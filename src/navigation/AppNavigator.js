import React, { useEffect } from 'react';
import { Platform, View, Animated, Easing, Text, TouchableHighlight } from 'react-native';
import { createAppContainer, createSwitchNavigator, NavigationActions } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerActions } from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import TabBarIcon from '../components/TabBarIcon';
import { translate } from '../components/Language';
import Colors from '../constants/Colors';
import AuthLoadingScreen from '../screens/Helper/AuthLoadingScreen';
import LoginScreen from '../screens/Login/UserloginScreen';
import RegisterScreen from '../screens/Login/RegisterScreen';
import LogoutScreen from '../screens/Login/LogoutScreen';
import ForgotpasswordScreen from '../screens/Login/ForgotpasswordScreen';
import ForgotpasswordverifyScreen from '../screens/Login/ForgotpasswordverifyScreen';
import NotificationListScreen from '../screens/Notification/NotificationListScreen';
import NotificationDetailScreen from '../screens/Notification/NotificationDetailScreen';
import { ScreenTitle, HeaderLeftGoBackOnPaymentPage, HeaderRightIcon, HeaderLeftIcon, HeaderLeftGoBackIcon, HeaderLeftGoBackOnPage, HeaderRightCarListIcon } from '../screens/Helper/HeaderIcon';
import CarinitialScreen from  '../screens/Rental/CarinitialScreen';
import DashboardScreen from '../screens/Dashboard/DashboardScreen';
import ComingSoonScreen from '../screens/Dashboard/ComingSoonScreen';
import MyAccountScreen from '../screens/Account/MyAccountScreen';
import UpdatePersonalInfoScreen from '../screens/Account/UpdatePersonalInfoScreen';
import AccountInfoScreen from '../screens/Account/AccountInfoScreen';
import NotificationSettingsScreen from '../screens/Account/NotificationSettingsScreen';
import LicenseInfo from '../screens/Account/LicenseInfo';
import Legal from '../screens/Account/Legal';
import TermsOfService from '../screens/Account/TermsOfService';
import PrivacyPolicy from '../screens/Account/PrivacyPolicy';
import ChatScreen from '../screens/Chat/ChatScreen';
import ChatDetailScreen from '../screens/Chat/ChatDetailScreen';
import BillingScreen from '../screens/Billing/BillingScreen';
import RentalScreen from '../screens/Rental/RentalScreen';
import RentalDocsScreen from '../screens/Rental/RentalDocsScreen';
import TransactionHistoryScreen from '../screens/Rental/TransactionHistoryScreen';
import SubscriptionScreen from '../screens/Subscriptions/SubscriptionScreen';
import TransactionFilterScreen from '../screens/Rental/TransactionFilterScreen';
import RentalDocsViewScreen from '../screens/Rental/RentalDocsViewScreen';
import ReturnSwitchConnectorCarScreen from '../screens/Returncar/ReturnSwitchConnectorCarScreen';
import ReschedReturnCarScreen from '../screens/Returncar/ReschedReturnCarScreen';
import BillingInvoiceScreen from '../screens/Billing/BillingInvoiceScreen';
import InvoiceListScreen from '../screens/Billing/InvoiceListScreen';
import TicketsAndTolls from '../screens/Billing/TicketsAndTollsScreen';
import FAQDetailScreen from '../screens/Support/FAQDetailScreen';
import SupportScreen from '../screens/Support/SupportScreen';
import { global_styles } from '../constants/Style';
import AccountPaymentScreen from '../screens/Account/AccountPaymentSettings';
import PaymentPaynow from '../screens/Billing/PaymentPaynow';
import SubscriptionPaymentPaynow from '../screens/Subscriptions/SubscriptionPaymentPaynow';
import ReferfriendScreen from '../screens/Referfriend/ReferfriendScreen';
import Chat from '../screens/Support/ChatScreen';
import Had_accident from '../screens/Support/Had_accident';
import AddBankDetail from  '../screens/BankInfo/AddBankDetail';
import ConfirmationScreen from  '../screens/BankInfo/ConfirmationSreen';
import CarScreen from    '../screens/Reservation/CarScreen';
import CardetailScreen from  '../screens/Reservation/CardetailScreen';
import Contract from  '../screens/Rental/Contract';
import ScheduleReservation from  '../screens/Reservation/ScheduleReservation';
import ConfirmationForReservation from  '../screens/Reservation/ConfirmationForReservation';
import CarOnschedule   from  '../screens/Reservation/ScheduleReservation';
import CarFilterScreen from   '../screens/Reservation/CarFilterScreen';
import  ReservationPay  from  '../screens/Reservation/reservationPay';
// import ConfirmationForReservation from   '../screens/Reservation/ConfirmationForReservation';
import { identity } from 'lodash';
// import CardetailScreen from  '../screens/Reservation/CardetailScreen';
//const NotificationBadge = withBadge(15)(BellIcon);
//<NotificationBadge style={{paddingRight:20}} onPress={() => navigation.navigate('NotificationList',{"backitem":navigation.state.routeName})} name="bell" size={30} />

/**
 * This class is a backbone of all the navigation stacks of an entire application.
 * This class handles all the navigations stacks.
 */

// Return a Car 
const ReturnSwitchConnectorCarScreenNavigator = createStackNavigator(
  {
    ReturnSwitchConnector: ReturnSwitchConnectorCarScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle('Returns'),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "RentalPage"),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Refer a friend Screen
const ReferfriendScreenNavigator = createStackNavigator(
  {
    Referfriend: ReferfriendScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle('Referral'),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "BillingPage"),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Payment Pay Now 
const PaymentPaynowScreenNavigator = createStackNavigator(
  {
    PaymentPaynow: PaymentPaynow,
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle('Make a Payment'),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "BillingPage"),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Subscription Payment Pay Now 
const SubscriptionPaymentPaynowScreenNavigator = createStackNavigator(
  {
    SubscriptionPaymentPaynow: SubscriptionPaymentPaynow,
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle('Make a Payment'),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "Subscription"),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);


// Resched return a Car 
const ReschedReturnCarScreenNavigator = createStackNavigator(
  {
    ReschedReturnCar: ReschedReturnCarScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle('Reschedule Return a Car'),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "ReturnSwitchConnector"),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

  //confir reservation stack...//

  const ConfirmationStack  =  createStackNavigator(
    {
      ConfirmationForReservationScreen: ConfirmationForReservation,
    },
    {
      defaultNavigationOptions: ({ navigation }) => {
        return {
          headerTitle: ScreenTitle('Confirmation Screeen'),
          // headerLeft: HeaderLeftGoBackOnPage(navigation, ""),
          headerRight: HeaderRightIcon(navigation),
          headerStyle: {
            backgroundColor: Colors.header,
          },
          headerTintColor: Colors.textcolor,
        };
      }
    }
  );

  const ReservationPaymentStack  =  createStackNavigator(
    {
      ReservationPay: ReservationPay,
    },
    {
      defaultNavigationOptions: ({ navigation }) => {
        return {
          headerTitle: ScreenTitle('Payment Screen'),
           headerLeft:null,
          // headerRight: HeaderRightIcon(navigation),
          headerStyle: {
            backgroundColor: Colors.header,
          },
          headerTintColor: Colors.textcolor,
        };
      }
    }
  );
  

// chat stack navigator
const ChatScreenStack = createStackNavigator(
  {
    // Chat: {
    //   screen: ChatScreen,
    // },
    ChatDetail: {
      screen: ChatDetailScreen,
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
       const { routeName } = navigation.state;
      return {
        headerTitle: translate("Chat"),
        headerTitle: () => {
          if (navigation.state.routeName == "ChatDetail") {
            return ScreenTitle(translate("Chat"));
          } else {
            return ScreenTitle(translate("Chat"));
          }
        },
        headerLeft: HeaderLeftGoBackOnPage(navigation, "SupportPage"),
        headerRight:null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Transaction history
const TransactionHistoryStackNavigator = createStackNavigator(
  {
    TransactionHistory: {
      screen: TransactionHistoryScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state;
      return {
        headerTitle: ScreenTitle("Transaction history"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "RentalPage"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);


// Subscription  history
const SubscriptionScreenStackNavigator = createStackNavigator(
  {
    Subscription: {
      screen: SubscriptionScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state;
      return {
        headerTitle: ScreenTitle("Subscriptions"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "MyAccount"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Transaction filter
const TransactionFilterStackNavigator = createStackNavigator(
  {
    TransactionFilter: {
      screen: TransactionFilterScreen
    }
    
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state;
      return {
        headerTitle: ScreenTitle("Filter"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "TransactionHistory"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// My Account stack navigator
const MyAccountStackNavigator = createStackNavigator(
  {
    MyAccount: {
      screen: MyAccountScreen
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Account"),
        headerLeft: null,
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

const MycarScreenStack = createStackNavigator(
  {
    CarinitialScreen: {
      screen:CarScreen 
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const parent = navigation.getParam('parent','Rental');
      return {
        headerTitle: ScreenTitle("Select Vehicle"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, parent),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

const  CarDetailStack =  createStackNavigator(
  {
    CardetailScreen:{
        screen:CardetailScreen
      },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
          
      return {
        headerTitle: ScreenTitle("Car Detail"),
        headerLeft: HeaderLeftGoBackOnPage(navigation,"CarinitialScreen"),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

const scheduleReservationStack = createStackNavigator(
  {
    CarOnschedule:{
      screen:CarOnschedule
    },
  },
  {
    defaultNavigationOptions: ({ navigation,route }) => {
      

      return {
        headerTitle: ScreenTitle("Create Reservation"),
        headerLeft: HeaderLeftGoBackOnPage(navigation,"CardetailScreen"),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Update Personal Info
const UpdatePersonalInfoStackNavigator = createStackNavigator(
  {
    UpdatePersonaInfo: {
      screen: UpdatePersonalInfoScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state;
      const parent = navigation.getParam('parent', 'MyAccount');
      return {
        headerTitle: ScreenTitle("Update your personal info"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, parent),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Update Account Info
const UpdateAccountInfoStackNavigator = createStackNavigator(
  {
    AccountInfo: {
      screen: AccountInfoScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state;
      return {
        headerTitle: ScreenTitle("Update your account info"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "MyAccount"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Notification settings
const NotificationSettingsStackNavigator = createStackNavigator(
  {
    NotificationSettings: {
      screen: NotificationSettingsScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state;
      return {
        headerTitle: ScreenTitle("Notification settings"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "MyAccount"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Update License Info
const UpdateLicenseInfoStackNavigator = createStackNavigator(
  {
    LicenseInfo: {
      screen: LicenseInfo
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Update your license info"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "MyAccount"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Legal
const LegalStackNavigator = createStackNavigator(
  {
    Legal: {
      screen: Legal
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Legal"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "MyAccount"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

//Had an Accident
const HadAccidentStackNavigator = createStackNavigator(
  {
    HadAccident: {
      screen: Had_accident
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Had an accident"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "Support"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);



// TermsOfService
const TermsStackNavigator = createStackNavigator(
  {
    TermsOfService: {
      screen: TermsOfService
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Terms of service"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "Legal"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Privacy Policy
const PrivacyStackNavigator = createStackNavigator(
  {
    PrivacyPolicy: {
      screen: PrivacyPolicy
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Privacy policy"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "Legal"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Account Payment
const AccountPaymentStackNavigator = createStackNavigator(
  {
    AccountPayment: {
      screen: AccountPaymentScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Update your credit card information"),
        headerLeft: HeaderLeftGoBackOnPaymentPage(navigation, "MyAccount"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Billing Invoice
const BillingInvoiceStackNavigator = createStackNavigator(
  {
    BillingInvoice: {
      screen: BillingInvoiceScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const title = navigation.getParam('title', '');
      const route = navigation.getParam('route', 'Billing');
      return {
        headerTitle: ScreenTitle(title),
        headerLeft: HeaderLeftGoBackOnPage(navigation, route),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Invoice stack navigator
const InvoiceStackNavigator = createStackNavigator(
  {
    InvoiceList: {
      screen: InvoiceListScreen
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Invoice"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "BillingPage"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Billing Invoice Detail
const TicketsAndTollsStackNavigator = createStackNavigator(
  {
    TicketsAndTolls: {
      screen: TicketsAndTolls
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Billing Invoice Detail"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "BillingInvoice"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Chat screen
const ChatStackNavigator = createStackNavigator(
  {
    Chat: {
      screen: Chat
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Chat"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "SupportPage"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Billing stack navigator
const BillingStackNavigator = createStackNavigator(
  {
    BillingPage: {
      screen: BillingScreen
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Billing"),
        headerLeft: null,
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
        backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Rental stack navigator

const ContractStackNavigator = createStackNavigator(
  {
    Contract: {
      screen: Contract
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Contract"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, 'Rental'),
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);


const RentalStackNavigator = createStackNavigator(
  {
    RentalPage: {
      screen: RentalScreen
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Your rental"),
        headerLeft: null,
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);


// FAQ Detail
const FAQDetailStackNavigator = createStackNavigator(
  {
    FAQDetail: {
      screen: FAQDetailScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const title = navigation.getParam('title', 'FAQ Detail');
      return {
        headerTitle: ScreenTitle(title),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "Support"),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Support stack navigator
const SupportStackNavigator = createStackNavigator(
  {
    SupportPage: {
      screen: SupportScreen
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle("Support"),
        headerLeft: null,
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Dashboard stack navigator
const DashboardStack = createStackNavigator(
  {
    DashboardPage: {
      screen: DashboardScreen,
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle(translate("dashboard")),
        headerLeft: null,
        headerRight: HeaderRightIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

const ComingSoonStackNavigator = createStackNavigator(
  {
    ComingSoon: {
      screen: ComingSoonScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: ScreenTitle('Documents'),
        headerLeft: HeaderLeftGoBackOnPage(navigation, 'DashboardPage'),
        headerRight: null,
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

const AddbankInfoStatckNavigation = createStackNavigator({
  
    AddBankDetail: {
      screen: AddBankDetail
    },
     ConfirmationScreen:{
       screen:ConfirmationScreen
     }
    }
  );


const RentalDocsStackNavigator = createStackNavigator(
  {
    RentalDocs: RentalDocsScreen,
    RentalDocsView: RentalDocsViewScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: () => {
          if (navigation.state.routeName == "RentalDocs") {
          } else {
            return ScreenTitle(navigation.state.params.name);
          }
        },
        headerLeft: () => {
          if (navigation.state.routeName == "RentalDocs")
            return HeaderLeftGoBackIcon(navigation);
          else
            return HeaderLeftGoBackIcon(navigation);
        },
        headerRight: () => {
          if (navigation.state.routeName == "RentalDocs")
            return HeaderRightIcon(navigation);
          else
            return null;
        },
        headerStyle: {
          backgroundColor: Colors.header,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.18,
          shadowRadius: 1.00,
          elevation: 1,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);


// Bottom bar tab navigator
const DashboardTabNavigator = createBottomTabNavigator(
  {
    Dashboard: {
      screen: DashboardStack,
    },
    Billing: {
      screen: BillingStackNavigator,
    },
    Rental: {
      screen: RentalStackNavigator,
    },
    Support: {
      screen: SupportStackNavigator,
    },
    MyAccount: {
      screen: MyAccountStackNavigator,
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state;
      return {
        tabBarLabel: " ",
        tabBarIcon: ({ focused }) => {
          if (routeName === 'MyAccount') {
            return <TabBarIcon type_name="profile" icon_val={2} focused={focused} name={'user'} />;
          } else if (routeName === 'Dashboard') {
            return <TabBarIcon type_name="account" icon_val={2} focused={focused} name={'view-dashboard-outline'} />;
          } else if (routeName === 'Billing') {
            return <TabBarIcon type_name="billing" icon_val={1} focused={focused} name={'library-books'} />;
          } else if (routeName === 'Rental') {
            return <TabBarIcon type_name="car" icon_val={1} focused={focused} name={Platform.OS === 'ios' ? 'car' : 'car'} />;
          } else if (routeName === 'Support') {
            return <TabBarIcon type_name="chat" icon_val={2} focused={focused} name={'hipchat'} />;
          } else if (routeName === 'Help') {
            return <TabBarIcon type_name="help" icon_val={2} focused={focused} name={'help-circle'} />;
          } else if (routeName === 'Chat') {
            return <TabBarIcon type_name="chat" icon_val={2} focused={focused} name={'hipchat'} />;
          }
        },
      }
    },
    initialRouteName: 'Dashboard',
    tabBarOptions: {
      labelStyle: {
        fontSize: 12,
        marginTop: 0
      },
      inactiveTintColor: 'transparent',
      activeTintColor: 'black',
      style: {
        fontSize: 24,
        borderColor: 'white',
        borderTopColor: '#f5f5f5',
        borderTopWidth: 1,
        marginVertical: Platform.OS === "ios" ? 12 : 5,
        marginTop: 0,
        paddingTop: 5,
        backgroundColor: 'white',
      }
    },
  },
);

// Dashboard stack navigator
const DashboardStackNavigator = createStackNavigator(
  {
    DashboardTabNavigator: DashboardTabNavigator,
  }, {
  headerMode: 'none'
}
);

/*
// Forgot password stack navigator
const ForgotPasswordStackNavigator = createStackNavigator(
  {
    Forgotpassword: ForgotpasswordScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: translate("forgot_password_menu"),
        headerLeft: null,
        headerStyle: {
          backgroundColor: Colors.header,
          shadowColor: Colors.textcolor,
          shadowOpacity: 0,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// Varify forgot password stack navigator
const ForgotPasswordVerifyStackNavigator = createStackNavigator(
  {
    Forgotpasswordverify: ForgotpasswordverifyScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: translate("reset_password"),
        headerLeft: HeaderLeftGoBackOnPage(navigation, "Forgotpassword"),
        headerStyle: {
          backgroundColor: Colors.header,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);  */

// Notification  stack navigator
const NotificationStackNavigator = createStackNavigator(
  {
    NotificationList: {
      screen: NotificationListScreen
    },
    NotificationDetail: {
      screen: NotificationDetailScreen
    },
  },
  {
    initialRouteName: 'NotificationList',
    headerMode: "screen",
    mode: Platform.OS === "ios" ? "modal" : "card",
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerTitle: (navigation.state.routeName == "NotificationList" ? 'Notifications' : 'Notification Detail'),
        headerLeft: HeaderLeftGoBackIcon(navigation),
        headerStyle: {
          backgroundColor: Colors.header,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.18,
          shadowRadius: 1.00,
          elevation: 1,
        },
        headerTintColor: Colors.textcolor,
      };
    }
  }
);

// App navigation drawer navigator
const AppDrawerNavigator = createDrawerNavigator({

  [translate("browse_cars")]: {
    screen: DashboardStackNavigator
  },
  'ComingSoon': {
    screen: ComingSoonStackNavigator,
  },
  'Notification': {
    screen: NotificationStackNavigator,
  },
  'RentalDocsSt': {
    screen: RentalDocsStackNavigator,
  },
  'Referafriend': {
    screen: ReferfriendScreenNavigator,
  },
  'TransactionHistory': {
    screen: TransactionHistoryStackNavigator,
  },
  'TransactionFilter': {
    screen: TransactionFilterStackNavigator,
  },

  'ReservationPay':{
      screen:ReservationPaymentStack
  },

    // 'CarScreen':{
    //   screen:CarReservarionStack,
    // },
    'CardetailScreen':{
     screen:CarDetailStack
    },
    // "CarFilterScreen":{
    //   screen:CarReservarionStack
    // },
  'SubscriptionScreen': {
    screen: SubscriptionScreenStackNavigator,
  },
  'AccountPayments': {
    screen: AccountPaymentStackNavigator,
  },
  'UpdateAccountInfo': {
    screen: UpdateAccountInfoStackNavigator,
  },
  'NotificationSettings': {
    screen: NotificationSettingsStackNavigator,
  },
  'UpdatePersonalInfo': {
    screen: UpdatePersonalInfoStackNavigator,
  },
  'UpdateLicenseInfo': {
    screen: UpdateLicenseInfoStackNavigator,
  },
  'Legal': {
    screen: LegalStackNavigator,
  },

 'ConfirmationForReservation':{
      screen: ConfirmationStack
 },
  'TermsOfService': {
    screen: TermsStackNavigator,
  },
  'HadAccidentStack': {
    screen: HadAccidentStackNavigator,
  },
  'PrivacyPolicy': {
    screen: PrivacyStackNavigator,
  },
  'Returncarwrapper': {
    screen: ReturnSwitchConnectorCarScreenNavigator,
  },
  'ChatScreenMenu': {
    screen: ChatScreenStack,
  },
  'ReschedReturnCarwrapper': {
    screen: ReschedReturnCarScreenNavigator,
  },
  'SubscriptionPaymentPaynowScree': {
    screen: SubscriptionPaymentPaynowScreenNavigator,
  },
  'PaymentPaynowScreen': {
    screen: PaymentPaynowScreenNavigator,
  },
  'FAQDetail': {
    screen: FAQDetailStackNavigator,
  },
  'Contract':{
     screen:ContractStackNavigator
  },
  'CarinitialScreen': {
       screen:MycarScreenStack
     },
  'BillingInvoice': {
    screen: BillingInvoiceStackNavigator,
  },
  // 'ScheduleReservation':{
  //   screen:'ScheduleReservation'
  // },
  'CarOnschedule':{
    screen:scheduleReservationStack
  },
  'InvoiceList': {
    screen: InvoiceStackNavigator
  },
  'TicketsAndTolls': {
    screen: TicketsAndTollsStackNavigator,
  },
  'Chat': {
    screen: ChatStackNavigator,
  },
  'AddBankDetail': {
    screen: AddbankInfoStatckNavigation,
  },
     'ConfirmationScreen':{
        screen:AddbankInfoStatckNavigation
     },
  [translate("logout")]: {
    screen: LogoutScreen
  }
}, {
  drawerType: "slide",
  defaultNavigationOptions: {
    drawerLockMode: 'locked-closed',
  }
});
/*Forgotpassword: {
  screen: ForgotPasswordStackNavigator,
},*/
const AuthStack = createStackNavigator(
  {
    SignIn: LoginScreen,
    SignUp: RegisterScreen,
    Forgotpassword: ForgotpasswordScreen,
    Forgotpasswordverify: ForgotpasswordverifyScreen,
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      gesturesEnabled: false,
    }
  });

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: {
        screen: AuthLoadingScreen,
        navigationOptions: {
          headerTitle: translate("Loading"),
        },
      },
      Auth: {
        screen: AuthStack,
      },
      App: {
        screen: AppDrawerNavigator,
      },
    },
    {
      initialRouteName: 'AuthLoading',
      headerMode: "screen",
      mode: Platform.OS === "ios" ? "modal" : "card",
    }
  )
);