import { head } from 'lodash';
import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  Image,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  TouchableOpacity,
  Platform
} from 'react-native';
const win = Dimensions.get('window');
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';

export default function ConfirmationScreen(props) {

    return(
        <View style={styles.main_container}>
           <View style={styles.container_waper}>
            <Image source={require('../../assets/images/under-construction-orange.png')} style={{height:300,width:300}}/>
            <View style={{marginTop:10}}>
            <Text style={[{textAlign:"center",padding:10,fontSize:18,justifyContent:"center",alignItems:"center",color:"rgba(0 0 0, 0.6)"}, global_styles.lato_regular]}>Thank you for submitting your 
banking details. Please allow 5-7 business days for the refund to reflect in your account.</Text>
            </View>
          
                <TouchableOpacity style={{marginTop:20}} >
                  <View style={global_styles.home_white_button_wrapper}>
                    <Text style={global_styles.button_style_white_text}>HOME</Text>
                  </View>
                </TouchableOpacity>
              
           </View>
        </View>
    )
}
const styles = StyleSheet.create({
    main_container: {
      backgroundColor: '#f9f9f4',
      flex: 1,
      alignItems:"center",
      padding:20
    },
    container_waper:{
     marginTop:20,
     alignItems:"center"
    }

  });