import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  Image,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  TouchableOpacity,
  Platform
} from 'react-native';
const win = Dimensions.get('window');
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import Colors from '../../constants/Colors';
import { TextInput } from 'react-native-gesture-handler';
import { translate } from '../../components/Language';
import RadioGroup from 'react-native-radio-button-group';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [{label: 'YES', value: 0}, {label: 'NO', value: 1}, {label: 'PARTIAL', value: 2},];
var radio_pickup=[{label: 'YES', value: 0}, {label: 'NO', value: 1}]
import CalendarPicker from 'react-native-calendar-picker';

// let platformStyle = Platform.OS === 'ios' ? styles.iosStyle : styles.androidStyle;
export default function AddBankDetail(props) {
     
    const [state,setState]=useState({
      // radio_props:[{label: 'YES', value: 0}, {label: 'NO', value: 1}, {label: 'PARTIAL', value: 2}],
      value2: 0,
      value2Index: 0,
    })
    const [value, setValue] = React.useState('first');


    const  gotoConfirmationScreen = () =>{
      props.navigation.navigate('ConfirmationScreen');
     }
    return(
        <View style={styles.main_container}>
            <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'} showsVerticalScrollIndicator={false} contentContainerStyle={(Platform.OS == "web") ? { flexGrow: 1, height: (win.height - 100) } : {}}>
            <View style={styles.container}>
               <View style={styles.container_style}>
               <Text style={[styles.note_text_style, global_styles.lato_regular]}>You have a deposit refund of $399. Would you like to apply this deposit to a future Buggy rental?</Text>
               <RadioForm
              formHorizontal={true}
              animation={true}
              labelHorizontal={true}>
              {radio_props.map((obj, i) => {
                var is_selected = state.value2Index == i;
                return (
                  <View key={i} style={styles.radioButtonWrap}>
                    <RadioButton
                      isSelected={is_selected}
                      obj={obj}
                      index={i}
                      labelHorizontal={true}
                     buttonColor={'#7c7c7c'}
                      labelColor={'#7c7c7c'}
                      buttonOuterColor={'#c4c4c4'}
                      buttonInnerColor={'#7c7c7c'}
                      buttonSize={10}
                      style={[i !== radio_props.length-1 && styles.radioStyle]}
                      onPress={(value, index) => {
                          console.log(value)
                        setState({value2:value})
                        setState({value2Index: index});
                      }}
                    />
                  </View>
                )
              })}
            </RadioForm>
            <View style={{marginTop:10,marginBottom:10}}>
            <Text style={[styles.note_text_style, global_styles.lato_regular]}>How much would you like to apply to your future rental deposit?</Text>
            </View>
               <View style={{flex:1,alignItems:"center"}}>
              <TextInput
                  style={[global_styles.text_amount, global_styles.lato_regular]}
                  placeholder={"$200.00"}
                //   value={first_name}
                //   onChangeText={(text) => updateTextInput(text, 'first_name')}
                />
                <Text style={[styles.note_text_style, global_styles.lato_regular]}>Would you like to schedule your future pickup now?</Text>
              </View>
              <RadioForm
              formHorizontal={true}
              animation={true}
              labelHorizontal={true}>
              {radio_pickup.map((obj, i) => {
                var is_selected = state.value2Index == i;
                return (
                  <View key={i} style={styles.radioButtonWrap}>
                    <RadioButton
                      isSelected={is_selected}
                      obj={obj}
                      index={i}
                      labelHorizontal={true}
                     buttonColor={'#7c7c7c'}
                      labelColor={'#7c7c7c'}
                      buttonOuterColor={'#c4c4c4'}
                      buttonInnerColor={'#7c7c7c'}
                      buttonSize={10}
                      style={[i !== radio_props.length-1 && styles.radioStyle]}
                      onPress={(value, index) => {
                          console.log(value)
                        setState({value2:value})
                        setState({value2Index: index});
                      }}
                    />
                  </View>
                )
              })}
            </RadioForm>
            <View style={{marginTop:10}}>
            <CalendarPicker
            containerStyle={{height:100}}
            width={200}
            dayShape="square"
            // todayBackgroundColor={"blue"}
          // onDateChange={this.onDateChange}
        />
        </View>
              </View>
            
              <View style={{marginTop:20}}>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("name_on_the_Account")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={"John haider"}
                //   value={first_name}
                //   onChangeText={(text) => updateTextInput(text, 'first_name')}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}></Text>
              </View>
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("routing_number")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={"122000234"}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}></Text>
              </View>
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("account_number")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={"*******453"}
                //   value={nick_name}
                //   onChangeText={(text) => updateTextInput(text, 'nick_name')}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}></Text>
              </View>

              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("re_enter_account_number")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={"*******453"}
                //   value={nick_name}
                //   onChangeText={(text) => updateTextInput(text, 'nick_name')}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}></Text>
              </View>
              <View style={{marginTop:10}}>
              <Text style={[styles.note_text_style, global_styles.lato_regular]}>Please allow 5-7 business days for the refund to reflect in your account.</Text>
              </View>
              <View style={global_styles.button_container}>
                <TouchableOpacity >
                  <View style={global_styles.button_style_white_wrapper}>
                    <Text style={global_styles.button_style_white_text}>{translate("cancel")}</Text>
                  </View>
                </TouchableOpacity>
                <View style={{ marginStart: 16 }} />
                <TouchableOpacity onPress={()=>gotoConfirmationScreen()} >
                  <View style={global_styles.bottom_style_orange_wrapper}>
                    <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate("save")}</Text>
                  </View>
                </TouchableOpacity>
              </View>
              </View>
              </KeyboardAwareScrollView>
            </View>
    )

}

// Stylesheet to design add bank detail screen
const styles = StyleSheet.create({
    main_container: {
      backgroundColor: '#f9f9f4',
      flex: 1
    },
    container: {
      margin: 22
    },
    labelStyleAcount: {
      marginTop: 4,
      fontSize: 12,
      color: '#000000',
      letterSpacing: 1,
      opacity: 0.6
    },
    button_container: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      marginTop: 22
    },
    error_label: {
      color: 'red',
      fontSize: 12,
    },
    iosStyle: {
      marginLeft: 0,
      marginTop: -8,
      paddingVertical: 12
    },
    androidStyle: {
      marginLeft: -10,
      marginTop: -13,
      paddingVertical: 12,
      paddingHorizontal: 10,
    },
    split_container: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    half_container_left: {
      flex: 1,
    },
    half_container_right: {
      flex: 1,
      marginStart: 16
    },
    note_text_style:{
        color:'rgba(0, 0, 0, 0.6)',
        fontSize: 14, 
    },
      container_style:{
          flex:1,
          padding:20,
        
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    backgroundColor:"white"  

      },
      radioStyle: {
        
        
        
      },
      radioButtonWrap: {
        justifyContent:"space-between",
         flex:1,
         marginTop:20
      },
     
  });