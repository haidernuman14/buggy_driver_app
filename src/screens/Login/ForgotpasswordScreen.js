import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Image,
  Platform,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  View,
  ActivityIndicator,
  TouchableHighlight
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useMutation } from '@apollo/react-hooks';
import { TextField } from 'react-native-material-textfield';
import { global_styles, modal_style } from '../../constants/Style';
import Colors from '../../constants/Colors';
import { translate } from '../../components/Language';
import { appRequestPasswordReset } from '../../graphql/forgotPassword';
import { CheckConnectivity } from '../Helper/NetInfo';
import Modal from "react-native-modal";
import Icon2 from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
/* ForgotPassword screen allows user to receive the OTP to reset their password 
const win = Dimensions.get('window');
User has to give their registered email id on which they will get the OTP */
const win = Dimensions.get('window');
//Initialisation of constants 
export default function ForgotpasswordScreen(props) {
  const [key, setKey] = useState(false);
  const [state, setState] = useState({
    email: '',
    phone: '',
    isModal: false,
    errors: {
      error_email: '',
      error_global: '',
      message_global: ''
    }
  })

  const [isLoading, setIsLoading] = useState(false);
  const { email, isModal, phone, errors } = state;

  const [ArequestPasswordReset] = useMutation(appRequestPasswordReset);  //Graph QL API calls.

  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value.trim() });
  }

  //Function calls asynchronous API calls to receive OTP
  const fun_forgot_username = async () => {

    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }

    const { email, phone } = state;
    let flag_val = 0;
    let error = { ...state, errors };
    error.error_global = '';
    error.message_global = '';
    if (phone == '') {
      error.error_email = translate("error_phone");
      setState({ ...state, errors: error });
      flag_val = 1;
      // console.log("A11-->",phone);
    } else if (isNaN(phone)) {
      flag_val = 1;
      error.error_email = translate("error_valid_phone"); 
      // console.log("A112-->",phone);
    } else if (!isNaN(phone) && (phone.length != 10 && phone.length != 12 ) ) {  
      flag_val = 1;
      error.error_email = translate("error_valid_phone");
      // console.log("A113-->",phone);
    } else {
      error.error_email = ""; 
    }

    if (flag_val == 1) {
      setState({ ...state, errors: error });
      return;
    }

    let phone1 = phone;
    if(!isNaN(phone) && phone.length > 8 && (phone.length == 10 || phone.length == 12 ) ) {
        if(phone.indexOf("+1")>=0){
          phone1 = phone;
        } else {
          phone1 = "+1"+phone; 
        }
    }

    let input = {
      phone: phone1
    }
    // console.log("input<><><>",input);
    //API calls once valid email is received from user input.
    setIsLoading(true);
    ArequestPasswordReset({
      variables: { input }
    }).then((response) => {
      setIsLoading(false);
      if (response.data.requestPasswordReset.ok) {
        // props.navigation.navigate('Forgotpasswordverify');
        error.message_global = translate("verification_code_sent");
        saveData()
        setState({ ...state, isModal: true, errors: error });
      } else {
        error.error_email = translate("phone_is_not_authenticated"); // Please show proper messahe that comes from API response
        setState({ ...state, errors: error });
      }
      return

    }).catch((error) => { // error handling in case of API thows error
      setIsLoading(false);
      error.error_email = translate("phone_is_not_authenticated");
      setState({ ...state, errors: error });
      return
    });

    return;

  }

  const saveData = async () => {
    await AsyncStorage.setItem('s_phone', phone);
  }

  const fun_switch = (val) => {
    let errors = {
      error_email: '',
    };
    setState({ ...state, errors: errors });
    props.navigation.navigate('SignIn');
  }

  return (
    <>
      {isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}

      <View style={styles.main_container}>
        <KeyboardAwareScrollView>
          <View style={styles.headContainer}>
            <Image style={styles.logo} resizeMode="contain" source={require('../../assets/images/logo.png')}></Image>
            <Text style={[styles.login_heading_text, global_styles.lato_regular]}>{translate("go_your_own_way")}</Text>
          </View>
          <View style={styles.wrapper}>
            <Text style={[styles.labelStyleContent, global_styles.lato_medium]}>
              ENTER YOUR PHONE NUMBER AND WE WILL{"\n"}
              SEND YOU A PIN TO RESET YOUR PASSWORD
                </Text>
            <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>PHONE NUMBER</Text>
            <View style={global_styles.inputWrap_username}>
              <TextInput
                style={[global_styles.textInput, global_styles.lato_regular]}
                placeholder={"6461234567"}
                keyboardType="phone-pad"
                value={phone}
                onChangeText={(text) => updateTextInput(text, 'phone')}
              />
            </View>

            <View style={[styles.error_message_wrapper, global_styles.lato_regular]}>
              <Text style={styles.error_message_text}>{errors.error_email}</Text>
            </View>
            <View style={styles.button_container}>
              <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_orange_wrapper]} onPress={() => { fun_forgot_username(); }} >
                <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate("send")}</Text>
              </TouchableOpacity>
              <Text></Text>
              <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_gray_wrapper]} onPress={() => { fun_switch() }} >
                <Text style={[global_styles.bottom_style_gray_text, global_styles.lato_medium]}>CANCEL</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal}  >
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => { setState({ ...state, isModal: false }); props.navigation.navigate("Forgotpasswordverify", { emailid: email }) }}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Icon2 name="ios-checkmark-circle-outline" style={modal_style.modal_check} />
              <View>
                <Text style={modal_style.modalText}> {errors.message_global} </Text>
                <View style={modal_style.modal_textbuttonWrap_center}>
                  <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => { setState({ ...state, isModal: false }); props.navigation.navigate("Forgotpasswordverify", { emailid: email }) }}>
                    <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    paddingHorizontal: 35,
    paddingTop: 22,
    paddingBottom: 12,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  button_container: {
    justifyContent: 'center',
    marginTop: 35
  },
  cancel_container: {
    width: 140,
    backgroundColor: '#dedbdb',
    padding: 8,
    borderWidth: 1,
    borderRadius: 8,
    alignItems: "center",
    marginTop: 10,
    alignSelf: "center",
    borderColor: '#dedbdb'
  },
  save_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    alignSelf: "center",
    borderColor: '#db9360'
  },
  save_text: {
    color: '#FFFFFF'
  },
  labelStyleContent: {
    fontSize: 11,
    color: '#000000',
    letterSpacing: 1,
    marginBottom: 20,
  },
  labelStyle: {
    marginTop: 4,
    fontSize: 12,
    color: '#000000',
    letterSpacing: 1,
    marginVertical: 4,
    marginTop: 8,
    opacity: 0.6
  },
  textInput_password_secure: {
    color: "#2dafd3",
    paddingTop: 2,
    paddingLeft: 5
  },
  error_msg: {
    color: "red",
    fontSize: 16,
  },
  icon_input: {
    width: 17,
    height: 20,
    marginVertical: 5
  },
  container: {
    flex: 1
  },
  screen_change_text: {
    color: "#f59b42",
    fontSize: 16,
    marginRight: 15,
    textDecorationLine: "underline",
    marginTop: 10
  },
  forgot_link: {
    flexDirection: 'row',
    marginVertical: 0,
    alignSelf: "flex-end",
    marginTop: 2
  },
  forgot_link_text: {
    color: "#2dafd3",
    fontSize: 13,
    marginTop: 5,
  },
  error_message_wrapper: {
    flexDirection: "row",
    alignItems: "flex-end",
    marginBottom: 0,
    paddingVertical: 0,
  },
  error_message_text: {
    color: "#f59b42",
    fontSize: 12,
    justifyContent: "flex-end",
  },
  headContainer: {
    marginVertical: 0,
    marginBottom: (Platform.OS === "ios")?45:15,   
    flex: 1,  
    alignItems: "center",
    marginTop:(Platform.OS === "ios")?70:40,
  },
  logo: {
    width: 250,
    height: 60,
  },
  login_heading: {
    flex: 1,
    alignItems: "center",
    marginTop: 0,
    marginBottom: 25,
  },
  login_heading_text: {
    fontSize: 19,
    color: "#393e5c",
    textAlign: "center",
  },
  login_sub_heading: { // Defined but not used please remove this.
    flex: 1,
    alignItems: "center",
    marginTop: 30,
  },
  login_sub_heading_text: { // Defined but not used please remove this.
    fontSize: 18,
    textAlign: "center",
    color: "#fff",
  },
  modal: {
    flex: 1,
  },
  wrapper: {
    borderRadius: 15,
    marginVertical: 30,
    minHeight: 160,
    alignContent: "flex-start"
  },
  inputWrap: { // Defined but not used please remove this.
    flexDirection: "row",
    marginBottom: 0,
    borderRadius: 7,
    borderColor: '#ccc',
    borderWidth: 1,
    backgroundColor: "#fff",
    width: "100%",
  },
  inputWrap_username: { // Defined but not used please remove this.
    flexDirection: "row",
    marginBottom: 0,
    borderRadius: 5,
  },
  inputWrapLast: {
    marginBottom: 0,
  },
  WrapBorder: { // Defined but not used please remove this.
    flexDirection: "row",
  },
  input_cotainer: {
    flex: 1,
    paddingHorizontal: 0,
    paddingVertical: 0,
    height: 48,
    color: "#d4d4d4",
    borderWidth: 1,
    borderColor: "#ccc",
    backgroundColor: "#FFF",
    borderRadius: 10
  },
  input: {
    flex: 2,
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: "#d4d4d4",
    backgroundColor: "#fff",
    borderRadius: 10
  },
  loginButton: {
    alignItems: "center",
    flexDirection: "row",
  },
  button: {
    color: "#FFF",
    backgroundColor: Colors.color0,
    fontSize: 20,
    flex: 2,
    justifyContent: "center",
    textAlign: "center",
    paddingVertical: 8,
    fontWeight: "bold",
    borderRadius: 5
  },

})
