import React, { useState, useEffect } from 'react';
import { Animated, Dimensions, TouchableOpacity, BackHandler, StyleSheet, ScrollView, ActivityIndicator, TouchableHighlight, View, Text, TextInput, Image, KeyboardAvoidingView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import * as BlinkIDReactNative from 'blinkid-react-native';
import RegsteptwoScreen from './RegsteptwoScreen';
import { global_styles, modal_style } from '../../constants/Style';
import Colors from '../../constants/Colors';
import licenseKey from '../../constants/Microblink';
import visionUrl from '../../constants/GoogleVision';
import { translate } from '../../components/Language';
import { useMutation } from '@apollo/react-hooks';
import { CheckConnectivity } from '../Helper/NetInfo';
import Icon2 from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-picker';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from "moment";
import { driverUploadDocument } from '../../graphql/driverUploadDocument';
import { driverLicenseExpirationDateUpdate } from '../../graphql/driverLicenseExpirationDateUpdate';
import { defaultNormalizedCacheFactory } from 'apollo-boost';
import Modal from "react-native-modal";

// Get screen domensions
const win = Dimensions.get('window');

export default function RegsteponeScreen(props) {
  const [AdriverUploadDocument] = useMutation(driverUploadDocument);
  const [AdriverLicenseExpirationDateUpdate] = useMutation(driverLicenseExpirationDateUpdate);
  const [state, setState] = useState({
    isRegTwo: false,
    licenseImageProgressMessage: '',
    user_id:props.user_id,
    first_name: '',
    last_name: '',
    tlcLicense: '',
    dmvLicense: '',
    tlcName: '',
    dmvName: '',
    tlc_date: '',
    dmv_date: '',
    flag: 0,
    dmv_license: {
      fileName: "",
      fileData: "",
    },
    tlc_license: {
      filePath: "",
      fileName: "",
      fileData: "",
      fileUri: "",
    },
    errors1: {
      error_dmv_license: '',
      error_tlc_license: '',
      error_global: '',
    },
    slideUp:{},
    animation: new Animated.Value(0)
  })

  const [isLoading, setIsLoding] = useState(false);
  const [showDMVMessage, setShowDMVMessage] = useState(false); 
  const [dmv_flag, setDmvFlag] = useState(0);
  const [tlc_flag, setTlcFlag] = useState(0);
  const { isRegTwo, flag, licenseImageProgressMessage, user_id, first_name, last_name, tlc_date, dmv_date, tlcLicense, dmvLicense, tlcName, dmvName, dmv_license, tlc_license, errors1, animation, slideUp } = state;
  console.log('i==uuu',user_id)
  useEffect(()=>{
    const slideUp = {
      transform: [
        {
          translateY: animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
          }),
        },
      ],
    };
    setState({...state,slideUp:slideUp})
  },[])
  useEffect(() => {
    if (flag == 1) {
      setIsregTwo(true)
    }
  }, [flag])

  useEffect(() => { 
    
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    }
    const navFocusListener = props.navigation.addListener('didFocus', () => {
      BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    });
    return () => {
      navFocusListener.remove();
    };
  }, []);

  const handleBackButton  = () => {
    return true;
  }

  const setIsregTwo = (flag) => {
    setIsLoding(false);
    setState({ ...state, isRegTwo: flag });
  }

  useEffect(() => {
    if (dmv_flag == 1 && tlc_flag == 1) {
      setState({ ...state, flag: 1 })
    }
  }, [tlc_flag, dmv_flag])

  const allowedFileType = (fileType) => {
    const allowed = ["jpg", "jpeg", "png"]
    let extension = "";
    if (fileType) {
      let split_ = fileType.split("/");
      if (split_.length > 1 && split_[1]) {
        extension = split_[1].toLowerCase()
      }
    }
    return allowed.includes(extension);
  }

  const fun_choose_dmv = async () => {
    try {
      var blinkIdCombinedRecognizer = new BlinkIDReactNative.BlinkIdCombinedRecognizer();
      blinkIdCombinedRecognizer.returnFullDocumentImage = true;      

      const scanningResults = await BlinkIDReactNative.BlinkID.scanWithCamera(
        new BlinkIDReactNative.BlinkIdOverlaySettings(),
        new BlinkIDReactNative.RecognizerCollection([blinkIdCombinedRecognizer]),
        licenseKey
      );

      if (scanningResults) {
        console.log('scanningResults==>',scanningResults);
        for (let i = 0; i < scanningResults.length; ++i) {
          handleDMVResult(scanningResults[i]);
        }
      }
    } catch (error) {
      console.log(error);
      let _error = { ...errors1 };
      _error.error_dmv_license = translate("error_dmv_license");
      setState({
        ...state,
        errors1: _error
      });
    }
  }

  const handleDMVResult = (result) => {
    if (result instanceof BlinkIDReactNative.BlinkIdCombinedRecognizerResult) {
      let blinkIdResult = result;
      //console.log('blinkIdResult',blinkIdResult);
      console.log('result', blinkIdResult.dateOfExpiry)
      let dmv_date = '';
      if (blinkIdResult.dateOfExpiry) {
        dmv_date = blinkIdResult.dateOfExpiry.day + "-" + blinkIdResult.dateOfExpiry.month + "-" + blinkIdResult.dateOfExpiry.year;
      }
      let resultString = {
        fileName: blinkIdResult.documentNumber + '.jpg',
        fileData: (blinkIdResult.fullDocumentFrontImage) ? (blinkIdResult.fullDocumentFrontImage) : ''
      }
      let dmv = blinkIdResult.documentNumber;
      let error = { ...errors1 };
        error.error_dmv_license = "";

      if(resultString.fileData=="" || resultString.fileData==null){
        setDmvFlag(1);
        //setShowDMVMessage(true);
      }
      
      setState({ ...state, dmvName: resultString.fileName, dmvLicense: dmv, dmv_date: dmv_date, dmv_license: resultString, errors1:error });
    }
  }
  const fun_choose_tlc = async () => {
    let options = {
      title: 'Select Image From',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      maxWidth: 500,
      maxHeight: 500
    };
    await ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        if (allowedFileType(response.type)) {
          let f_name = response.uri.substring(response.uri.lastIndexOf('/') + 1)
          let extension = f_name.split('.').pop();
          let _tlc_license = { ...tlc_license };
          _tlc_license.filePath = response;
          _tlc_license.fileData = response.data;
          _tlc_license.fileUri = response.uri;
          _tlc_license.fileName = 'tlc_scan.' + extension;

          let error = { ...errors1 };
          error.error_tlc_license = "";
          setState({
            ...state,
            tlc_license: _tlc_license,
            tlcName: 'tlc_scan.' + extension,
            errors1: error
          })
        } else {
          let error = { ...errors1 };
          error.error_tlc_license = "File type not accepted. Please upload a .jpg, .jpeg or .png file";
          setState({ ...state, errors1: error, tlc_license: {
            filePath: "",
            fileName: "",
            fileData: "",
            fileUri: "",
          } });
        }
      }
    });
  }
  const verifyTlcLicense = async (tlcLicenseNumber) => {
    //Verify TLC License from the NYC Open Data Website. Returns a single array containing license info object
    console.log('verifyTlcLicense')
    let nycOpenDataUrl = "https://data.cityofnewyork.us/resource/xjfq-wh2d.json?license_number=" + tlcLicenseNumber;
    let appToken = "5WBDkAQLj244SKLuDJmXDVDhT";
    let response = null;
    try {
      const res = await fetch(nycOpenDataUrl, {
        method: 'GET',
        data: {
          "$limit": 1,
          "$$app_token": appToken
        }
      });
      const data = await res.json();
      //Api returns an array
      if (data && data[0] && data[0].name) {
        let name = data[0].name;
        name = name.split(",");
        if (name && name.length > 0) {
          response = { "last_name": name[0], "first_name": name[1], "tlc_date": data[0].expiration_date };
        }
      }
    }
    catch (err) {
      // console.log(err)
    }
    return response;
  }
  const processDocument = async (document, documentName, input) => {
    let error = { ...errors1 };
    if (document) {
      try {
        setState({ ...state, licenseImageProgressMessage: "Please wait while we read and verify your " + documentName })
        let visionRequest = {
          "requests": [
            {
              "image": {
                "content": document
              },
              "features": [
                {
                  "type": "TEXT_DETECTION",
                  "maxResults": 4
                }
              ]
            }
          ]
        }
        const response = await fetch(visionUrl, {
          method: 'POST',
          headers: { 'Accept': 'application/json, text/plain, */*', },
          body: JSON.stringify(visionRequest)
        });
        const responseData = await response.json();
        console.log(responseData)
        if (responseData && responseData.responses[0] && responseData.responses[0].fullTextAnnotation && responseData.responses[0].fullTextAnnotation.text) {
          let fullText = responseData.responses[0].fullTextAnnotation.text;
          if (documentName === "TLC License") {
            if (fullText && fullText.toLowerCase().includes("license")) {//NYC TLC License has "LICENSE" text contained. Use to verify that a tlc license was infact returned
              let tlcLicensefromImage = fullText.split('NUMBER\n').pop().split('\n')[0];
              tlcLicensefromImage = tlcLicensefromImage.replace(/ /g, '');
              const verifyLicense = await verifyTlcLicense(tlcLicensefromImage);
              if (verifyLicense) {
                setState({ ...state, first_name: verifyLicense["first_name"], last_name: verifyLicense["last_name"], tlcLicense: tlcLicensefromImage, licenseImageProgressMessage: null, tlc_date: verifyLicense["tlc_date"] });
                fileUpload(input);
              } else {
                error.error_tlc_license = "Invalid TLC number. Please provide valid TLC license.";
                setIsLoding(false);
                setState({ ...state, errors1: error, tlc_license: {
                  filePath: "",
                  fileName: "",
                  fileData: "",
                  fileUri: "",
                }, licenseImageProgressMessage: null })
              }
            } else {
              error.error_tlc_license = "There seems to be some issue with your TLC number. Please try again after few mintues.";
              setIsLoding(false);
              setState({ ...state, errors1: error, tlc_license: {
                filePath: "",
                fileName: "",
                fileData: "",
                fileUri: "",
              }, licenseImageProgressMessage: null })
            }
          }
        } else {
          error.error_tlc_license = "Taking more time, then required, please try after some time.";
          setIsLoding(false);
          setState({ ...state, errors1: error, tlc_license: {
            filePath: "",
            fileName: "",
            fileData: "",
            fileUri: "",
          }, licenseImageProgressMessage: null })
        }
      } catch (error) {
        console.log('readError', error)
        error.error_tlc_license = "There seems to be some issue with your TLC document. Please try again after few mintues.";
        setIsLoding(false);
        setState({ ...state, errors1: error, tlc_license: {
          filePath: "",
          fileName: "",
          fileData: "",
          fileUri: "",
        }, licenseImageProgressMessage: null })
      }
    } else {
      error.error_tlc_license = "Please upload valid TLC License.";
      setIsLoding(false);
      setState({ ...state, errors1: error, tlc_license: {
        filePath: "",
        fileName: "",
        fileData: "",
        fileUri: "",
      } });
    }
  }
  const imageUploadHandler = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
    setIsLoding(true);
    let flag_val = 0;
    let error = { ...errors1 };

    if (tlc_license.fileData == '') {
      error.error_tlc_license = translate("error_tlc_license");
      flag_val = 1;
    } else {
      error.error_tlc_license = "";
    }

    if (dmv_license.fileName == '') {
      error.error_dmv_license = translate("error_dmv_license");
      flag_val = 1;
    } else {
      error.error_dmv_license = "";
    }

    if (flag_val == 1) {
      setIsLoding(false);
      setState({ ...state, errors1: error });
      return;
    } else {
      setState({ ...state, errors1: error });
    }

    // Upload DMV Image  
    if (user_id && dmv_license.fileData != '' && user_id != '') {
      let input = { file: dmv_license.fileData, driverId: user_id, documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZTo2", fileName: dmv_license.fileName }
      await fileUpload(input, 'DMV');
    }else  if (Platform.OS === 'ios'){
      setDmvFlag(1)
    }

    // Upload TLC Image
    if (user_id && tlc_license.fileData != '' && user_id != '') {
      let input = { file: tlc_license.fileData, driverId: user_id, documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZTo1", fileName: tlc_license.fileName }
      await processDocument(tlc_license.fileData, "TLC License", input);
    }
  }
  const setRegTwo = (value) => {
    setIsLoding(false)
    setState({ ...state, isRegTwo: value });
  }

  const setRegSkip = (value) => {
    setState({ ...state, isLoading: false, isRegTwo: value, tlcLicense: '', dmvLicense: '', tlc_date: '', dmv_date: '' });
  }

  const setRegTwoClose = (value) => {
    setState({ ...state, isRegTwo: value });
    if (props.reg) {
      props.navigation.navigate('App');
    } else {
      props.setRegOneClose(false);
    }
  }

  const fileUpload = async (input, type = 'TLC') => {

    if (type == 'TLC'){
      await AsyncStorage.setItem('TLC_Image', JSON.stringify(input) ); 
      setTlcFlag(1);
      setIsLoding(false);
    } else {
      await AsyncStorage.setItem('DMV_Image', JSON.stringify(input) ); 
      setDmvFlag(1);
      setIsLoding(false);
    }
/*
    console.log('input',input.driverId)
    AdriverUploadDocument({
      variables: { input }
    }).then(async (response) => {
      console.log("uploadDocument >>" + type, response.data.uploadDocument);
      console.log("uploadDocument >>" + type, response.data.uploadDocument.errors);
      if (response.data.uploadDocument.ok) {
        if (type == 'TLC')
          setTlcFlag(1)
        else
          setDmvFlag(1)
      } else {
        setIsLoding(false);
      }
    }) */
  }
  
  //console.log('tlc_expire',tlc_expire,'dmv_expire',dmv_expire)

  const rescan_dmv = () => {
    setShowDMVMessage(false); 
    setIsLoding(false); 
  }

  const dmv_upload_later = () => {
    setDmvFlag(1);
    setShowDMVMessage(false);
    setIsLoding(false); 
  }

  return (
    <>
      {isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}
      {!isRegTwo && <Animated.View style={[styles.Animated, slideUp]}>
        <ScrollView style={{ flex: 1 }}>
          <KeyboardAvoidingView behavior='padding'>
            <View style={styles.title}>
              <Text style={styles.titleText}>{translate("apply_drive")}</Text>
           {/*    <TouchableHighlight underlayColor="" onPress={() => {
                if (props.reg) {
                  props.navigation.navigate('App');
                } else {
                  props.setRegOneClose(false);
                }
              }}>
                <Text style={styles.titleClose}><Icon3 style={styles.close_text_icon} name='close' /></Text>
              </TouchableHighlight> */}
            </View>
            <View style={styles.wrap_images}>
              <Image width="20" style={styles.frame_image} source={require('../../assets/images/step1.png')} />
            </View>
            <View style={styles.wrapper}>
              {errors1.error_global != '' &&
                <View>
                  <Text style={styles.error_msg}>{errors1.error_global}</Text>
                </View>
              }
              <View style={styles.view_text_lable} >
                <Text style={styles.text_right_upload} >{translate("scan_or_upload_your_dmv_license")}:</Text>
                <TouchableHighlight underlayColor="" style={styles.text_right_wrap} onPress={() => { fun_choose_dmv(); }}>
                  <View style={{ position: "relative" }}>
                    <Text style={styles.text_field} >{dmvName}</Text>
                    <Icon2 style={styles.text_icon} name='upload' />
                  </View>
                </TouchableHighlight>
              </View>
              {errors1.error_dmv_license != '' &&
                <View>
                  <Text style={styles.error_msg}>{errors1.error_dmv_license}</Text>
                </View>
              }
              <View style={styles.view_text_lable}>
                <Text style={styles.text_right_upload} >{translate("scan_or_upload_your_tlc_license")}:</Text>
                <TouchableHighlight underlayColor="" style={styles.text_right_wrap} onPress={() => { fun_choose_tlc(); }}>
                  <View style={{ position: "relative" }}>
                    <Text style={styles.text_field} >{tlcName}</Text>
                    <Icon2 style={styles.text_icon} name='upload' />
                  </View>
                </TouchableHighlight>
              </View>

              {errors1.error_tlc_license != '' &&
                <View>
                  <Text style={styles.error_msg}>{errors1.error_tlc_license}</Text>
                </View>
              }
            </View>
            <View style={styles.textbuttonWrap}>
              <TouchableHighlight underlayColor="" style={styles.linkWrap} onPress={() => { setRegSkip(true); }}>
                <Text style={styles.lnk_right_text}>{translate("do_not_have_it")}</Text>
              </TouchableHighlight>
              <TouchableHighlight underlayColor="" style={styles.upload_document} onPress={() => { imageUploadHandler(); }}>
                <Text style={styles.upload_document_text}>{translate("upload_document")}</Text>
              </TouchableHighlight>
            </View>

            <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={showDMVMessage} >
              <View style={modal_style.modal}>
                <View style={modal_style.modal_wrapper}>
                    <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => { setShowDMVMessage(false) }}>
                      <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                    </TouchableOpacity>
                    <Text style={modal_style.modal_heading}>Success</Text>
                    <Text style={[modal_style.modalText]}>DMV license has not been scanned properly.</Text>
                    <View style={modal_style.modal_textbuttonWrap}>
                      <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => { rescan_dmv(); }  }>
                        <Text style={modal_style.upload_document_text_modal}>RESCAN</Text>
                      </TouchableHighlight>
                      <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => { dmv_upload_later(); } }>
                        <Text style={modal_style.upload_document_text_modal}>UPLOAD LATER</Text>
                      </TouchableHighlight>
                    </View>
                </View>
              </View>
            </Modal>

          </KeyboardAvoidingView>
        </ScrollView>
      </Animated.View>}
      {isRegTwo && <RegsteptwoScreen {...props} FirstName={first_name} LastName={last_name} tlcLicense={tlcLicense} TlcLicense={tlc_license} tlc_date={tlc_date} dmvLicense={dmvLicense} DmvLicense={dmv_license} dmv_date={dmv_date} setModalSuccess={props.setModalSuccess} setRegTwo={setRegTwo} setRegTwoClose={setRegTwoClose} reLogin={props.reLogin} email_mobile={props.email_mobile} />}
    </>
  );
}

const styles = StyleSheet.create({
  Animated: {
    flex: 8,
  },
  info_main: {
    flex: 1,
  },
  wrap_images: {
    paddingTop: 5,
    marginVertical: 0,
    resizeMode: "contain",
    alignItems: "center"
  },
  frame_image: {
    maxWidth: 310,
    resizeMode: "contain"
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: 25,
    paddingVertical: 20,
  },
  title: {
    flex: 1,
    flexDirection: "row",
    width: win.width,
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#9e9d9b",
  },
  titleText: {
    flex: 1,
    paddingLeft: 30,
    paddingBottom: 15,
    fontWeight: "700",
    fontSize: 20,
  },
  titleClose: {
    marginRight: 20,
    marginTop: -5,
    fontWeight: "bold",
    fontSize: 22,
  },
  inputWrap: {
    flexDirection: "row",
    marginBottom: 15,
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },
  close_text_icon: {
    color: "red",
    fontSize: 22,
    fontWeight: "bold"
  },
  WrapBorder: {
    flexDirection: "row",
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  textbuttonWrap: {
    paddingHorizontal: 25,
    paddingVertical: 0,
    flexDirection: "row",
  },
  linkWrap: {
    flex: 1,
    marginVertical: 10,
    justifyContent: "center"
  },
  lnk_right_text: {
    textDecorationLine: 'underline',
    color: "#8c8c8b",
    fontSize: 14,
  },
  error_msg: {
    marginTop: 0,
    marginLeft: 0,
    color: "red",
    fontSize: 14,
    marginBottom: 14
  },
  text_icon: {
    color: '#9e9d9b',
    fontSize: 22,
    position: "absolute",
    right: 0,
    top: 0,
    bottom: 0
  },
  text_right_wrap: {
    borderColor: "#9e9d9b",
    borderBottomWidth: 1,
    position: "relative",
    padding: 0,
    marginTop: 5
  },
  text_field: {
    width: "80%",
    padding: 5
  },
  text_right_upload: {
    fontSize: 15,
  },
  view_text_lable: {
    marginTop: 12
  },
  upload_document: {
    backgroundColor: "#db9360",
    paddingHorizontal: 12,
    borderRadius: 6,
    paddingVertical: 10,
    marginRight: 7,
  },
  upload_document_text: {
    color: "#FFFFFF",
    fontWeight: "bold",
    fontSize: 16,
  },
});
