import React, { useState, useEffect } from 'react'; //Unused import function "useEffect"
import {
  StyleSheet,
  Dimensions,
  TextInput,
  Platform,
  TouchableOpacity,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  View,
  Image,
  ActivityIndicator,
  TouchableHighlight
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useMutation } from '@apollo/react-hooks'; //To use Gqaph API calls
import { TextField } from 'react-native-material-textfield';
import { global_styles, modal_style } from '../../constants/Style';
import Colors from '../../constants/Colors';
import { translate } from '../../components/Language';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { appResetPassword } from '../../graphql/driverResetPassword';
import { CheckConnectivity } from '../Helper/NetInfo';
import Modal from "react-native-modal";
import Icon3 from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
const win = Dimensions.get('window');
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
/*  ForgotpasswordverifyScreen is intended to set a new password. */

//Initialisation of constants
export default function ForgotpasswordverifyScreen(props) {

  const [key, setKey] = useState(false);
  const [state, setState] = useState({
    phone: '',
    passwordreg: '',
    confirm_password: '',
    verifycode: '',
    isModal: false,
    hidePasswordReg: true,
    hidePassword1: true,
    errors: {
      error_phone: '',
      error_password: '',
      error_confirm_password: '',
      error_verifycode: '',
      error_global: '',
      message_global: translate("verification_code_sent")
    }
  })

  const [isLoading, setIsLoading] = useState(false);
  const { phone, isModal, passwordreg, hidePasswordReg, hidePassword1, confirm_password, verifycode, errors } = state;

  const [AresetPassword] = useMutation(appResetPassword); //Call GraphQL API to reset the password.

  const [user, setUser] = useState({});
  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value.trim() });
  }
  // Function checks the local validation and perform API call asynchronously and exaption handling.
  const fun_reset_password = async () => {

    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }

    const { phone, passwordreg, hidePasswordReg, hidePassword1, confirm_password, verifycode, errors } = state;
    let flag_val = 0;
    let error = { ...state, errors };

    const expressionEmail = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    // phone validation regex.
    //check phone validation
    if (phone == null || phone == "") {
      errors.error_phone = translate("error_phone");
      flag_val = 1;
    } else if (isNaN(phone)) {
      flag_val = 1;
      errors.error_phone = translate("error_valid_phone");
    } else if (!isNaN(phone) && (phone.length != 10 && phone.length != 12 ) ) {  
      flag_val = 1;
      errors.error_email = translate("error_valid_phone");
    } else {
      errors.error_phone = "";
    }
    //check password validation
    if (passwordreg == '') {
      errors.error_password = translate("error_password");
      flag_val = 1;
    } else if (passwordreg.length < 6) {
      errors.error_password = translate("error_password_char");
      flag_val = 1;
    } else {
      errors.error_password = "";
    }
    // check confirm password validation
    if (confirm_password == '') {
      errors.error_confirm_password = translate("error_confirm_password");
      flag_val = 1;
    } else if (confirm_password != passwordreg) {
      errors.error_confirm_password = translate("error_confirm_password_match");
      flag_val = 1;
    } else {
      errors.error_confirm_password = "";
    }
    //check validation for OTP
    if (verifycode == '') {
      errors.error_verifycode = translate("enter_verification_code");
      flag_val = 1;
    } else {
      errors.error_verifycode = "";
    }
    setState({ ...state, errors: errors });

    if (flag_val == 1) {
      return;
    }

    let phone1 = phone;
    if(!isNaN(phone) && phone.length > 8 && (phone.length == 10 || phone.length == 12 ) ) {
        if(phone.indexOf("+1")>=0){
          phone1 = phone;
        } else {
          phone1 = "+1"+phone; 
        }
    }

    let input = {
      phone: phone1,
      newPassword: passwordreg,
      confirmPassword: confirm_password,
      verificationCode: verifycode
    }
    // console.log("input--",input);
    setIsLoading(true);
    AresetPassword({
      variables: { input }
    }).then((response) => {
      setIsLoading(false);
      if (response.data.resetPassword.ok) {
        error.message_global = translate("password_reset_success");
        setState({ ...state, errors: error, isModal: true });
        setKey(!key);
      } else {
        error.error_verifycode = translate("phone_is_not_authenticated");
        setState({ ...state, errors: error });
        setKey(!key);
      }
      return
    }).catch((error) => {
      setIsLoading(false);
      error.error_verifycode = translate("phone_is_not_authenticated");
      setState({ ...state, errors: error });
      return
    });


    // props.navigation.navigate('SignIn');
    return;

  }


  const setPasswordVisibility = (field, value) => {
    setState({ ...state, [field]: !value });
  }

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    let phone = await AsyncStorage.getItem('s_phone');
    setState({ ...state, phone: phone });
  }

  const fun_switch = (val) => {
    let errors = {
      error_phone: '',
      error_password: '',
      error_confirm_password: '',
      error_verifycode: '',
      error_global: '',
      message_global: '',
    };
    setState({ ...state, errors: errors });
    props.navigation.navigate('SignIn');
  }
  // console.log("phone---", phone);

  return (
    <>
      {isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}

      <View style={styles.main_container}>
        <KeyboardAwareScrollView>
          <View style={styles.headContainer}>
            <Image style={styles.logo} resizeMode="contain" source={require('../../assets/images/logo.png')}></Image>
            <Text style={[styles.login_heading_text, global_styles.lato_regular]}>{translate("go_your_own_way")}</Text>
          </View>
          <View style={styles.wrapper}>
            <Text style={[styles.labelStyleContent, global_styles.lato_medium]}>
              ENTER YOUR CODE & RESET PASSWORD
                </Text>
            <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>PHONE NUMBER</Text>
            <View style={global_styles.inputWrap_username}>
              <TextInput
                style={[global_styles.textInput, global_styles.lato_regular]}
                placeholder={"6461234567"}
                keyboardType="number-pad"
                value={phone}
                onChangeText={(text) => updateTextInput(text, 'phone')}
              />
            </View>
            <View style={styles.error_message_wrapper}>
              <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_phone}</Text>
            </View>

            <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>PASSWORD</Text>
            <View style={[global_styles.inputWrap_password]}>
              <TextInput
                style={[global_styles.textInput_password, global_styles.lato_regular]}
                secureTextEntry={hidePasswordReg ? true : false}
                placeholder={translate("password")}
                value={passwordreg}
                onChangeText={(text) => updateTextInput(text, 'passwordreg')}
              />
              <TouchableOpacity underlayColor="" activeOpacity={0.8} style={global_styles.touachableButton} onPress={() => setPasswordVisibility('hidePasswordReg', hidePasswordReg)}>
                {(hidePasswordReg) ? <Text style={global_styles.textInput_password_secure} >Show</Text> : <Text style={global_styles.textInput_password_secure} >Hide</Text>}
              </TouchableOpacity>
            </View>
            <View style={styles.error_message_wrapper}>
              <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_password}</Text>
            </View>

            <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>CONFIRM PASSWORD</Text>
            <View style={[global_styles.inputWrap_password]}>
              <TextInput
                style={[global_styles.textInput_password, global_styles.lato_regular]}
                secureTextEntry={hidePassword1 ? true : false}
                placeholder={translate("confirm_password")}
                value={confirm_password}
                onChangeText={(text) => updateTextInput(text, 'confirm_password')}
              />
              <TouchableOpacity underlayColor="" activeOpacity={0.8} style={global_styles.touachableButton} onPress={() => setPasswordVisibility('hidePassword1', hidePassword1)}>
                {(hidePassword1) ? <Text style={global_styles.textInput_password_secure} >Show</Text> : <Text style={global_styles.textInput_password_secure} >Hide</Text>}
              </TouchableOpacity>
            </View>
            <View style={styles.error_message_wrapper}>
              <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_confirm_password}</Text>
            </View>

            <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>VERIFICATION CODE</Text>
            <View style={[global_styles.inputWrap_username]}>
              <TextInput
                style={[global_styles.textInput, global_styles.lato_regular]}
                placeholder={translate("verification_code")}
                value={verifycode}
                onChangeText={(text) => updateTextInput(text, 'verifycode')}
              />
            </View>
            <View style={styles.error_message_wrapper}>
              <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_verifycode}</Text>
            </View>

            <View style={styles.button_container}>
              <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_orange_wrapper]} onPress={() => { fun_reset_password(); }} >
                <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate("send")}</Text>
              </TouchableOpacity>
              <Text></Text>
              <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_gray_wrapper]} onPress={() => { fun_switch() }} >
                <Text style={[global_styles.bottom_style_gray_text, global_styles.lato_medium]}>CANCEL</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal} >
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => props.navigation.navigate("SignIn")}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Icon3 name="ios-checkmark-circle-outline" style={modal_style.modal_check} />
              <View>
                <Text style={modal_style.modalText}> {errors.message_global} </Text>
                <View style={modal_style.modal_textbuttonWrap_center}>
                  <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => props.navigation.navigate("SignIn")}>
                    <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </View>
        </Modal>

      </View>
    </>
  )

}



const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: 0,
    paddingBottom: 12,
  },
  button_container: {
    justifyContent: 'center',
    marginTop: 15
  },
  cancel_container: {
    width: 140,
    backgroundColor: '#dedbdb',
    padding: 8,
    borderWidth: 1,
    borderRadius: 8,
    alignItems: "center",
    marginTop: 10,
    alignSelf: "center",
    borderColor: '#dedbdb'
  },
  save_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    alignSelf: "center",
    borderColor: '#db9360'
  },
  save_text: {
    color: '#FFFFFF'
  },
  textInput: {
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 6,
    fontSize: 14,
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderColor: '#ccc',
    color: '#454545',
    width: "100%",
    backgroundColor: '#ffffff'
  },
  textInput_password: {
    borderRadius: 6,
    fontSize: 14,
    paddingHorizontal: 12,
    paddingVertical: 8,
    color: '#454545',
    width: "75%",
    backgroundColor: '#ffffff'
  },
  labelStyleContent: {
    fontSize: 12,
    color: '#000000',
    letterSpacing: 1,
    marginBottom: 10
  },
  labelStyle: {
    marginTop: 4,
    fontSize: 12,
    color: '#000000',
    letterSpacing: 1,
    marginVertical: 4,
    opacity: 0.6
  },
  textInput_password_secure: {
    color: "#2dafd3",
    paddingTop: Platform.OS === 'ios' ? 2 : 2,
    paddingLeft: 5
  },
  error_msg: {
    color: "red",
    fontSize: 16,
  },
  icon_input: {
    width: 17,
    height: 20,
    marginVertical: 5
  },
  container: {
    flex: 1
  },
  screen_change_text: {
    color: "#f59b42",
    fontSize: 16,
    marginRight: 15,
    textDecorationLine: "underline",
    marginTop: 10
  },
  forgot_link: {
    flexDirection: 'row',
    marginVertical: 0,
    alignSelf: "flex-end",
    marginTop: 2
  },
  forgot_link_text: {
    color: "#2dafd3",
    fontSize: 13,
    marginTop: 5,
  },
  error_message_wrapper: {
    flexDirection: "row",
    marginBottom: 0,
    paddingVertical: 0,
  },
  error_message_text: {
    color: "#f59b42",
    fontSize: 12,
    justifyContent: "flex-end",
  },
  headContainer: {
    marginVertical: 0,
    marginBottom: 15,
    marginTop: 30,
    flex: 1,
    alignItems: "center"
  },
  logo: {
    width: 250,
    height: 60,
  },
  login_heading: {
    flex: 1,
    alignItems: "center",
    marginTop: 0,
    marginBottom: 25,
  },
  login_heading_text: {
    fontSize: 19,
    color: "#393e5c",
    textAlign: "center",
  },
  login_sub_heading: { // Defined but not used please remove this.
    flex: 1,
    alignItems: "center",
    marginTop: 30,
  },
  login_sub_heading_text: { // Defined but not used please remove this.
    fontSize: 18,
    textAlign: "center",
    color: "#fff",
  },
  modal: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    marginVertical: 10,
    paddingVertical: 10,
    width: win.width - 80,
    borderRadius: 15,
    alignContent: "center",
    alignSelf: "center",
  },
  inputWrap: { // Defined but not used please remove this.
    flexDirection: "row",
    marginBottom: 0,
    borderRadius: 7,
    borderColor: '#ccc',
    borderWidth: 1,
    backgroundColor: "#fff",
    width: "100%",
  },
  inputWrap_username: { // Defined but not used please remove this.
    flexDirection: "row",
    marginBottom: 0,
    borderRadius: 5,
  },
  inputWrapLast: {
    marginBottom: 0,
  },
  WrapBorder: { // Defined but not used please remove this.
    flexDirection: "row",
  },
  input_cotainer: {
    flex: 1,
    paddingHorizontal: 0,
    paddingVertical: 0,
    height: 48,
    color: "#d4d4d4",
    borderWidth: 1,
    borderColor: "#ccc",
    backgroundColor: "#FFF",
    borderRadius: 10
  },
  input: {
    flex: 2,
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: "#d4d4d4",
    backgroundColor: "#fff",
    borderRadius: 10
  },
  loginButton: {
    alignItems: "center",
    flexDirection: "row",
  },
  button: {
    color: "#FFF",
    backgroundColor: Colors.color0,
    fontSize: 20,
    flex: 2,
    justifyContent: "center",
    textAlign: "center",
    paddingVertical: 8,
    fontWeight: "bold",
    borderRadius: 5
  },
  touachableButton: {
    paddingTop: Platform.OS === 'ios' ? 8 : 8,
    paddingHorizontal: 5,
    paddingRight: 0
  },

})

