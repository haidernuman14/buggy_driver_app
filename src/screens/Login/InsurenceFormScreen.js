import React, { Component } from 'react';
import { Animated, Dimensions, ScrollView, StyleSheet, TouchableHighlight, View, Text, TextInput, Image, KeyboardAvoidingView, Platform } from 'react-native';
import Pdf from 'react-native-pdf';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
let source = Platform.OS == 'ios' ? require("../../assets/images/Insurance_form.pdf") : { uri: 'bundle-assets://Insurance_form.pdf' }
const win = Dimensions.get('window');
export default class InsurenceFormScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animation: new Animated.Value(0)
        }
    }

    render() {
        const slideUp = {
            transform: [
                {
                    translateY: this.state.animation.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 1],
                    }),
                },
            ],
        };
        return (
            <>
                {
                    <Animated.View style={[styles.Animated, slideUp]}>
                        <View style={{ flex: 1 }}>
                            <View style={styles.title}>
                                <Text style={styles.titleText}>Insurance Form</Text>
                                <TouchableHighlight underlayColor="" onPress={() => {
                                    this.props.hidePDF();
                                }}>
                                    <Text style={styles.titleClose}>
                                        <Icon2 style={styles.close_text_icon} name='close' />
                                    </Text>
                                </TouchableHighlight>
                            </View>
                            <View style={styles.container}>
                                <Pdf
                                    source={source}
                                    style={styles.pdf} />
                            </View>
                        </View>
                    </Animated.View>}
            </>
        );
    }
}

const styles = StyleSheet.create({
    Animated: {
        flex: 20,
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex:1,
        width: win.width,
        height: win.height,
    },
    title: {
        flex: .07,
        flexDirection: "row",
        width: win.width,
        marginVertical: 10,
        alignContent: "center",
        justifyContent: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#9e9d9b",
    },
    titleText: {
        flex: 1,
        paddingLeft: 30,
        paddingBottom: 15,
        fontWeight: "700",
        fontSize: 20,
    },
    titleClose: {
        marginRight: 20,
        fontWeight: "bold",
        fontSize: 22,
    },
    close_text_icon: {
        color: "red",
        fontSize: 22,
        fontWeight: "bold"
    },
});