import React, { useState, useEffect } from 'react';
import { Animated, Dimensions, BackHandler, StyleSheet, ScrollView, ActivityIndicator, TouchableHighlight, View, Text, TextInput, Image, KeyboardAvoidingView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import * as BlinkIDReactNative from 'blinkid-react-native';
import { global_styles } from '../../constants/Style';
import Colors from '../../constants/Colors';
import licenseKey from '../../constants/Microblink';
import visionUrl from '../../constants/GoogleVision';
import { translate } from '../../components/Language';
import { useMutation } from '@apollo/react-hooks';
import client from 'graphql';
import { CheckConnectivity } from '../Helper/NetInfo';
import Icon2 from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-picker';
import moment from "moment";
import RegsteptwoexistScreen from './RegsteptwoexistScreen';
import { driverUploadDocument } from '../../graphql/driverUploadDocument';
import { driverLicenseExpirationDateUpdate } from '../../graphql/driverLicenseExpirationDateUpdate';

const win = Dimensions.get('window');

export default function RegsteponeexistScreen(props) {
    const [AdriverUploadDocument] = useMutation(driverUploadDocument);
    const [AdriverLicenseExpirationDateUpdate] = useMutation(driverLicenseExpirationDateUpdate);
    const [state, setState] = useState({
        user_id: props.user.id,
        isRegTwo: false,
        tlc_expire: props.tlc_expire,
        dmv_expire: props.dmv_expire,
        licenseImageProgressMessage: '',
        first_name: '',
        last_name: '',
        tlcLicense: '',
        dmvLicense: '',
        tlcName: '',
        dmvName: '',
        tlc_date: '',
        dmv_date: '',
        flag: 0,
        dmv_license: {
            fileName: "",
            fileData: "",
        },
        tlc_license: {
            filePath: "",
            fileName: "",
            fileData: "",
            fileUri: "",
        },
        errors: {
            error_dmv_license: '',
            error_tlc_license: '',
            error_global: '',
        },
        animation: new Animated.Value(0)
    })
    const [isLoading, setIsLoding] = useState(false);
    const [dmv_flag, setDmvFlag] = useState(0);
    const [tlc_flag, setTlcFlag] = useState(0);
    const [user, setUser] = useState(props.user);
    const { isRegTwo, flag, licenseImageProgressMessage, user_id, first_name, last_name, tlc_date, dmv_date, tlc_expire, dmv_expire, tlcLicense, dmvLicense, tlcName, dmvName, dmv_license, tlc_license, errors, animation } = state;

    useEffect(() => {
        if (flag == 1) {
            setIsregTwo(true)
        }
    }, [flag])
    const setIsregTwo = (flag) => {
        setIsLoding(false);
        setState({ ...state, isRegTwo: flag });
        if (flag == false) {

        }
    }
    useEffect(() => {
        if (tlc_expire != '' && dmv_expire != '') {
            console.log('dmv_flag', dmv_flag, 'tlc_flag', tlc_flag)
            if (dmv_flag == 1 && tlc_flag == 1) {
                setState({ ...state, flag: 1 })
            }
        } else if (dmv_expire != '' && dmv_flag == 1) {
            setState({ ...state, flag: 1 })
        } else if (tlc_expire != '' && tlc_flag == 1) {
            setState({ ...state, flag: 1 })
        }
    }, [tlc_flag, dmv_flag])

    useEffect(() => { 
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        }
        const navFocusListener = props.navigation.addListener('didFocus', () => {
            BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        });
        return () => {
            navFocusListener.remove();
        };
    }, []);

    const handleBackButton  = () => {
        return true;
    }
    
    const allowedFileType = (fileType) => {
        const allowed = ["jpg", "jpeg", "png"]
        let extension = "";
        if (fileType) {
            let split_ = fileType.split("/");
            if (split_.length > 1 && split_[1]) {
                extension = split_[1].toLowerCase()
            }
        }
        return allowed.includes(extension);
    }
    const fun_choose_dmv = async () => {
        try {
            var blinkIdCombinedRecognizer = new BlinkIDReactNative.BlinkIdCombinedRecognizer();
            blinkIdCombinedRecognizer.returnFullDocumentImage = true;      
      
            const scanningResults = await BlinkIDReactNative.BlinkID.scanWithCamera(
              new BlinkIDReactNative.BlinkIdOverlaySettings(),
              new BlinkIDReactNative.RecognizerCollection([blinkIdCombinedRecognizer]),
              licenseKey
            );

            if (scanningResults) {

                for (let i = 0; i < scanningResults.length; ++i) {
                    handleDMVResult(scanningResults[i]);
                }
            }
        } catch (error) {
            console.log(error);
            let _error = { ...errors };
            _error.error_tlc_license = "Please scan or upload valid TLC License.";
            setState({
                ...state,
                errors: _error
            });
        }
    }

    const handleDMVResult = (result) => {
        if (result instanceof BlinkIDReactNative.BlinkIdCombinedRecognizerResult) {
            let blinkIdResult = result;
            console.log('result', blinkIdResult.dateOfExpiry)
            let dmv_date = '';
            if (blinkIdResult.dateOfExpiry) {
                dmv_date = blinkIdResult.dateOfExpiry.day + "-" + blinkIdResult.dateOfExpiry.month + "-" + blinkIdResult.dateOfExpiry.year;
            }
            let resultString = {
                fileName: blinkIdResult.documentNumber + '.jpg',
                fileData: (blinkIdResult.fullDocumentFrontImage) ? (blinkIdResult.fullDocumentFrontImage) : ''
            }
            let dmv = blinkIdResult.documentNumber;
            if (user.dmvLicense != dmv) {
                let error = { ...errors };
                error.error_dmv_license = "This DMV is invalid. Scan the DMV license that has similar license number.";
                setState({ ...state, errors: error });
                return;
            }
            setState({ ...state, dmvName: resultString.fileName, dmvLicense: dmv, dmv_date: dmv_date, dmv_license: resultString }, () => {
                //console.log(dmv, resultString);
            });
        }
    }
    const fun_choose_tlc = async () => {
        let options = {
            title: 'Select Image From',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxWidth: 500,
            maxHeight: 500
        };
        await ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                if (allowedFileType(response.type)) {
                    let f_name = response.uri.substring(response.uri.lastIndexOf('/') + 1)
                    let _tlc_license = { ...tlc_license };
                    _tlc_license.filePath = response;
                    _tlc_license.fileData = response.data;
                    _tlc_license.fileUri = response.uri;
                    _tlc_license.fileName = f_name;

                    let error = { ...errors };
                    error.error_tlc_license = "";
                    error.error_dmv_license = "";
                    setState({
                        ...state,
                        tlc_license: _tlc_license,
                        tlcName: f_name,
                        errors: error
                    })
                } else {
                    let error = { ...errors };
                    error.error_tlc_license = "File type not accepted. Please upload a .jpg, .jpeg or .png file";
                    setState({ ...state, errors: error });
                }
            }
        });
    }
    const verifyTlcLicense = async (tlcLicenseNumber) => {
        //Verify TLC License from the NYC Open Data Website. Returns a single array containing license info object
        console.log('verifyTlcLicense')
        let nycOpenDataUrl = "https://data.cityofnewyork.us/resource/xjfq-wh2d.json?license_number=" + tlcLicenseNumber;
        let appToken = "5WBDkAQLj244SKLuDJmXDVDhT";
        let response = null;
        try {
            const res = await fetch(nycOpenDataUrl, {
                method: 'GET',
                data: {
                    "$limit": 1,
                    "$$app_token": appToken
                }
            });
            const data = await res.json();
            //Api returns an array
            if (data && data[0] && data[0].name) {
                let name = data[0].name;
                name = name.split(",");
                if (name && name.length > 0) {
                    response = { "last_name": name[0], "first_name": name[1], "tlc_date": data[0].expiration_date };
                }
            }
        }
        catch (err) {
            // console.log(err)
        }
        return response;
    }
    const processDocument = async (document, documentName, input) => {
        let error = { ...errors };
        if (document) {
            try {
                setState({ ...state, licenseImageProgressMessage: "Please wait while we read and verify your " + documentName })
                let visionRequest = {
                    "requests": [
                        {
                            "image": {
                                "content": document
                            },
                            "features": [
                                {
                                    "type": "TEXT_DETECTION",
                                    "maxResults": 4
                                }
                            ]
                        }
                    ]
                }
                const response = await fetch(visionUrl, {
                    method: 'POST',
                    headers: { 'Accept': 'application/json, text/plain, */*', },
                    body: JSON.stringify(visionRequest)
                });
                const responseData = await response.json();
                if (responseData && responseData.responses[0] && responseData.responses[0].fullTextAnnotation && responseData.responses[0].fullTextAnnotation.text) {
                    let fullText = responseData.responses[0].fullTextAnnotation.text;
                    if (documentName === "TLC License") {
                        if (fullText && fullText.toLowerCase().includes("license")) {//NYC TLC License has "LICENSE" text contained. Use to verify that a tlc license was infact returned
                            let tlcLicensefromImage = fullText.split('NUMBER\n').pop().split('\n')[0];
                            tlcLicensefromImage = tlcLicensefromImage.replace(/ /g, '');
                            // console.log(user.tlcLicense)
                            if (user.tlcLicense != tlcLicensefromImage) {
                                let error = { ...errors };
                                error.error_tlc_license = "This TLC is invalid. Select the TLC license that has similar license number.";
                                setState({ ...state, errors: error });
                                setIsLoding(false);
                                return;
                            }
                            const verifyLicense = await verifyTlcLicense(tlcLicensefromImage);
                            if (verifyLicense) {
                                setState({ ...state, tlcLicense: tlcLicensefromImage, licenseImageProgressMessage: null, tlc_date: verifyLicense["tlc_date"] })
                                fileUpload(input, verifyLicense["tlc_date"]);
                            } else {
                                error.error_tlc_license = "Please scan or upload valid TLC License.";
                                setIsLoding(false);
                                setState({ ...state, errors: error, licenseImageProgressMessage: null })
                            }
                        } else {
                            error.error_tlc_license = "Please scan or upload valid TLC License.";
                            setIsLoding(false);
                            setState({ ...state, errors: error, licenseImageProgressMessage: null })
                        }
                    }
                } else {
                    error.error_tlc_license = "Please scan or upload valid TLC License.";
                    setIsLoding(false);
                    setState({ ...state, errors: error, licenseImageProgressMessage: null })
                }
            } catch (error) {
                // console.log('readError', error)
                error.error_tlc_license = "Please scan or upload valid TLC License.";
                setIsLoding(false);
                setState({ ...state, errors: error, licenseImageProgressMessage: null })
            }
        } else {
            error.error_tlc_license = "Please scan or upload valid TLC License.";
            setIsLoding(false);
            setState({ ...state, errors: error });
        }
    }
    const imageUploadHandler = async () => {
        let netState = await CheckConnectivity();
        if (!netState) {
            props.screenProps.setNetModal(true)
            return;
        }
        setIsLoding(true);
        let flag_val = 0;
        let error = { ...errors };

        if (tlc_expire != '' && tlc_license.fileData == '') {
            error.error_tlc_license = translate("error_tlc_license");
            flag_val = 1;
        } else {
            error.error_tlc_license = "";
        }

        if (dmv_expire != '' && dmvLicense == '') {
            error.error_dmv_license = translate("error_dmv_license");
            flag_val = 1;
        } else {
            error.error_dmv_license = "";
        }

        if (flag_val == 1) {
            setIsLoding(false);
            setState({ ...state, errors: error });
            return;
        } else {
            setState({ ...state, errors: error });
        }

        // Upload DMV Image  
        if (dmv_expire != '' && user_id && dmv_license.fileData != '' && user_id != '') {
            let input = { file: dmv_license.fileData, driverId: user_id, documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZTo2", fileName: dmv_license.fileName }
            await fileUpload(input, dmv_date, 'DMV');
        }

        // Upload TLC Image
        if (tlc_expire != '' && user_id && tlc_license.fileData != '' && user_id != '') {
            let input = { file: tlc_license.fileData, driverId: user_id, documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZTo1", fileName: tlc_license.fileName }
            await processDocument(tlc_license.fileData, "TLC License", input);
        }
    }

    const fileUpload = (input, date, type = 'TLC') => {
        AdriverUploadDocument({
            variables: { input }
        }).then(async (response) => {
            // console.log("uploadDocument >>" + type, response.data.uploadDocument);
            if (response.data.uploadDocument.ok) {
                let input;
                if (type == 'TLC')
                    input = { id: user_id, tlcLicenseExpirationDate: moment(date).format('YYYY-MM-DD') }
                else
                    input = { id: user_id, dmvLicenseExpirationDate: moment(date, 'D-M-YYYY').format('YYYY-MM-DD') }
                await LicenseUpdate(input, type);
            } else {
                setIsLoding(false);
            }
        })
    }
    const LicenseUpdate = (input, type) => {
        console.log('input', input)
        AdriverLicenseExpirationDateUpdate({
            variables: { input }
        }).then((response) => {
            console.log("uploadDocument Sign >>", response.data.updateDriver);
            console.log('_______', response.data.updateDriver.ok);
            if (response.data.updateDriver.ok == true && response.data.updateDriver.driver) {
                let user_info = response.data.updateDriver.driver;
                user.tlcLicenseExpirationDate = user_info.tlcLicenseExpirationDate;
                user.dmvLicenseExpirationDate = user_info.dmvLicenseExpirationDate;
                AsyncStorage.setItem('user', JSON.stringify(user));
                if (type == 'TLC')
                    setTlcFlag(1)
                else
                    setDmvFlag(1)
                console.log('___yes___')
                return;
            } else {
                setIsLoding(false);
            }
        })
    }

    const slideUp = {
        transform: [
            {
                translateY: animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 1],
                }),
            },
        ],
    };
    //console.log('tlc_expire',tlc_expire,'dmv_expire',dmv_expire)
    return (
        <>
            {isLoading &&
                <View style={global_styles.activityIndicatorView}>
                    <ActivityIndicator size="large" style={{ padding: 60 }} />
                </View>}
            {!isRegTwo && <Animated.View style={[styles.Animated, slideUp]}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={styles.wrapper}>
                        {errors.error_global != '' &&
                            <View>
                                <Text style={styles.error_msg}>{errors.error_global}</Text>
                            </View>
                        }
                        {dmv_expire != '' && <View style={styles.view_text_lable} >
                            <Text style={styles.text_right_upload} >{translate("scan_or_upload_your_dmv_license")}:</Text>
                            <TouchableHighlight underlayColor="" style={styles.text_right_wrap} onPress={() => { fun_choose_dmv(); }}>
                                <View style={{ position: "relative" }}>
                                    <Text style={styles.text_field} >{dmvName}</Text>
                                    <Icon2 style={styles.text_icon} name='upload' />
                                </View>
                            </TouchableHighlight>
                        </View>}
                        {errors.error_dmv_license != '' &&
                            <View>
                                <Text style={styles.error_msg}>{errors.error_dmv_license}</Text>
                            </View>
                        }
                        {tlc_expire != '' && <View style={styles.view_text_lable}>
                            <Text style={styles.text_right_upload} >{translate("scan_or_upload_your_tlc_license")}:</Text>
                            <TouchableHighlight underlayColor="" style={styles.text_right_wrap} onPress={() => { fun_choose_tlc(); }}>
                                <View style={{ position: "relative" }}>
                                    <Text style={styles.text_field} >{tlcName}</Text>
                                    <Icon2 style={styles.text_icon} name='upload' />
                                </View>
                            </TouchableHighlight>
                        </View>}

                        {errors.error_tlc_license != '' &&
                            <View>
                                <Text style={styles.error_msg}>{errors.error_tlc_license}</Text>
                            </View>
                        }
                    </View>
                    <View style={styles.textbuttonWrap}>
                        <TouchableHighlight underlayColor="" style={styles.upload_document} onPress={() => { imageUploadHandler(); }}>
                            <Text style={styles.upload_document_text}>{translate("upload_document")}</Text>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            </Animated.View>}
            {isRegTwo && <RegsteptwoexistScreen {...props} stage={1} setIsregTwo={setIsregTwo} />}
        </>
    );
}

const styles = StyleSheet.create({
    Animated: {
        flex: 8,
    },
    info_main: {
        flex: 1,
    },
    wrap_images: {
        paddingTop: 5,
        marginVertical: 0,
        resizeMode: "contain",
        alignItems: "center"
    },
    frame_image: {
        maxWidth: 310,
        resizeMode: "contain"
    },
    wrapper: {
        flex: 1,
        paddingHorizontal: 25,
        paddingVertical: 20,
    },
    title: {
        flex: 1,
        flexDirection: "row",
        width: win.width,
        marginVertical: 10,
        alignContent: "center",
        justifyContent: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#9e9d9b",
    },
    titleText: {
        flex: 1,
        paddingLeft: 30,
        paddingBottom: 15,
        fontWeight: "700",
        fontSize: 20,
    },
    titleClose: {
        marginRight: 20,
        marginTop: -5,
        fontWeight: "bold",
        fontSize: 22,
    },
    inputWrap: {
        flexDirection: "row",
        marginBottom: 15,
    },
    input: {
        flex: 1,
        paddingHorizontal: 10,
    },
    close_text_icon: {
        color: "red",
        fontSize: 22,
        fontWeight: "bold"
    },
    WrapBorder: {
        flexDirection: "row",
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    textbuttonWrap: {
        paddingHorizontal: 25,
        alignItems: "flex-end",
        flex: 1,
        paddingBottom: 20
    },
    linkWrap: {
        flex: 1,
        marginVertical: 10,
        justifyContent: "center"
    },
    lnk_right_text: {
        textDecorationLine: 'underline',
        color: "#8c8c8b",
        fontSize: 14,
    },
    error_msg: {
        marginTop: 0,
        marginLeft: 0,
        color: "red",
        fontSize: 14,
        marginBottom: 14
    },
    text_icon: {
        color: '#9e9d9b',
        fontSize: 22,
        position: "absolute",
        right: 0,
        top: 0,
        bottom: 0
    },
    text_right_wrap: {
        borderColor: "#9e9d9b",
        borderBottomWidth: 1,
        position: "relative",
        padding: 0,
        marginTop: 5
    },
    text_field: {
        width: "80%",
        padding: 5
    },
    text_right_upload: {
        fontSize: 15,
    },
    view_text_lable: {
        marginTop: 12
    },
    upload_document: {
        backgroundColor: Colors.color0,
        paddingHorizontal: 12,
        borderRadius: 6,
        paddingVertical: 10,
        marginRight: 7,
    },
    upload_document_text: {
        color: "#FFFFFF",
        fontWeight: "bold",
        fontSize: 16,
    },
});
