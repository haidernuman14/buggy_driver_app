import React, { useState, useContext, useEffect } from 'react';
import { Animated, Platform, TextInput, Alert, StyleSheet, ScrollView, ActivityIndicator, Dimensions, View, Text, Image, TouchableOpacity, TouchableHighlight, KeyboardAvoidingView, Keyboard } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Modal from "react-native-modal";
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { useMutation,useQuery} from '@apollo/react-hooks';
import { driverLogin} from '../../graphql/driverLogin';
import { driverRegister } from '../../graphql/driverRegister';
import RNPickerSelect from 'react-native-picker-select';
import RegsteptwoexistScreen from './RegsteptwoexistScreen';
import { translate } from '../../components/Language';
import Colors from '../../constants/Colors';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { CheckConnectivity } from '../Helper/NetInfo';
import { langContext } from '../Helper/langContext';
import { notification_list } from '../../constants/notifications';
import { checkIfDriverExists } from '../../graphql/checkIfDriverExists';
import DeviceInfo from 'react-native-device-info';

import analytics from '@react-native-firebase/analytics';
const states = require("../../constants/statesCities.json");

const win = Dimensions.get('window');
/**
 * This class is an entry point of overall registration process.
 * This class collects Email or Phone or TLC as a username and Password from the user.
 * It passes all these information to the next step of registration process.
 */

export default function RegisterScreen(props) {
  const [state, setState] = useState({
    isLoading: false,
    isModal: false,
    isModalSuccess: false,
    isRegOne: false,
    driverLicense:'',
    email_mobile: '',
    passwordreg: '',
    radioPaymentTypeValue: "TLC LICENSE",
    confirm_password: '',
    hidePasswordReg: true,

    hidePassword1: true,
    activeDeviceId:DeviceInfo.getUniqueId(),
    activeDeviceModel:DeviceInfo.getModel(),
    activeDeviceOs:Platform.OS.toUpperCase()+" "+DeviceInfo.getSystemVersion(),
    errors: {
      error_email_mobile: '',
      error_password: '',
      error_confirm_password: '',
      error_driverLicense:'',
      error_global: ''
    },
    animation: new Animated.Value(1)
  })
  let radio_props = [
    { label: "TLC LICENSE", value: "TLC LICENSE" },
    { label: "DMV LICENSE", value: "DMV LICENSE" },
  ];
  const [user, setUser] = useState({});
  const [userFlag, setUserFlag] = useState("");
  const [isLoadingReg, setIsLoadingReg] = useState(false);
  const [username, setUsername] = useState("");
  const {isModal, isRegOne, email_mobile, hidePasswordReg, hidePassword1, passwordreg, confirm_password, errors,radioPaymentTypeValue,driverLicense,activeDeviceId,activeDeviceModel,activeDeviceOs} = state;
  const [driverLoginMutation] = useMutation(driverLogin);
  const [driverRegisterMutation] = useMutation(driverRegister);

  const { data: driverData, loading: driverDataLoading, error: driverDataError } = useQuery(
    checkIfDriverExists, {
        variables: {phone: username},
        fetchPolicy: "network-only",
        skip: (username == '')
  });


  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value });
  }

  const goBackLoginScreenHandler = (val) => {

    Keyboard.dismiss()

    let errors = {
      error_email_mobile: '',
      error_password: '',
      error_confirm_password: '',
      error_global: ''
    };
    setState({ ...state, errors: errors });
    props.navigation.navigate('SignIn');
  }

  useEffect(() => {
      if(driverDataError){
      setIsLoadingReg(false)
      Alert.alert("Error", "We couldn't able register your account. Please contact Buggy Costumer support.\n347-334-6313")
      }
  }, [driverDataError])
  

  // Register driver using "RegisterDriver" API
  const handle_registeration =async () => {

    Keyboard.dismiss()
    // handleCheckIfDriverExists()
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
    let flg_error = 0;

    if (email_mobile == '' || (email_mobile.indexOf("+1") < 0 && (!isNaN(email_mobile)) == false)) {
      errors.error_email_mobile = translate("error_email_mobile");
      flg_error = 1;
    } else if (!isNaN(email_mobile) && email_mobile.length < 6) {
      errors.error_email_mobile = "Please enter valid Phone Number";
      flg_error = 1;
    } else if (!isNaN(email_mobile) && email_mobile.length > 8 && (email_mobile.length != 10 && email_mobile.length != 12)) {
      errors.error_email_mobile = translate("error_valid_phone");
      flg_error = 1;
    } else {
      errors.error_email_mobile = "";
    }
    if (passwordreg == '') {
      errors.error_password = translate("error_password");
      flg_error = 1;
    } else if (passwordreg.length < 6) {
      errors.error_password = translate("error_password_char");
      flg_error = 1;
    } else {
      errors.error_password = "";
    }
    if (confirm_password == '') {
      errors.error_confirm_password = translate("error_confirm_password");
      flg_error = 1;
    } else if (confirm_password != passwordreg) {
      errors.error_confirm_password = translate("error_confirm_password_match");
      flg_error = 1;
    } else {
      errors.error_confirm_password = "";
    }

    if (flg_error == 1) {
      setIsLoadingReg(false);
      setState({ ...state, errors: errors, isLoading: false });
      return
    }
    let Flag = '';
    let username = email_mobile;
    if (isNaN(email_mobile)) {
      Flag = 1;
    }
      
    else if (email_mobile.length >= 6 && email_mobile.length < 8) {
      Flag = 2;
      time_tlc = email_mobile;
    } else {
      Flag = 3;
      username = (email_mobile.indexOf("+1") >= 0) ? email_mobile : '+1' + email_mobile;
    }
       
    setUserFlag(Flag);
    setUsername(username)
    setIsLoadingReg(true);
    setState({ ...state, passwordreg: passwordreg, driverLicense: driverLicense });
    // registerDriverInfo(username, passwordreg, driverLicense);

    
    return;
  }

  const setModalVisible = (value, flag = '') => {
    if (flag == 'yes') {
      setState({ ...state, isModal: value, isRegOne: true });
    } else if (flag == 'no') {
      props.navigation.navigate('App');
    } else {
      setState({ ...state, isModal: value });
    }
  }

  const setModalSuccess = async (value) => {
    setState({ ...state, isModalSuccess: value });
  }

  const setRegOne = (value) => {
    setState({ ...state, isRegOne: value });
    props.navigation.navigate('App');
  }

  const setPasswordVisibility = (field, value) => {
    setState({ ...state, [field]: !value });
  }

  const radioPaymentMethodChange = (value, index) => {
    setState({ ...state, radioPaymentTypeValue: value });


  }

  // Call "driverLogin" API on successful Registration to get a Token 
  const reLogin = async () => {
    const username = await AsyncStorage.getItem('java_name');
    driverLoginMutation({
      variables: {
        username: username,
        password: passwordreg
      }
    }).then(async (response1) => {
  
      await AsyncStorage.setItem('isLogin', 'true');
      await AsyncStorage.setItem('token', response1.data.driverLogin.token);
      await AsyncStorage.setItem('reg_user_id', 'reg_user_id');
      await AsyncStorage.setItem('user', JSON.stringify(response1.data.driverLogin.user));
      if (response1.data.driverLogin.user.driverLicense == '') {
        await AsyncStorage.setItem('allow_apply_now', 'yes');
      } else {
        await AsyncStorage.setItem('allow_apply_now', 'no');
      }
      let response = response1;

      if (!(response.data.driverLogin.user && response.data.driverLogin.user.hasOwnProperty('driverPtr') && response.data.driverLogin.user.driverPtr.hasOwnProperty('activeInsurance') && response.data.driverLogin.user.driverPtr.activeInsurance !== null)) {
        AsyncStorage.setItem('activeInsurance', 'true');
      } else {
        AsyncStorage.setItem('activeInsurance', 'false');
      }
      return;
    });
  }
  const registerDriverInfo = (email_mobile, password, driverLicense) => {
      if (email_mobile != "" && password != "") {
      driverRegisterMutation({
        variables: {
          tlcLicense:radioPaymentTypeValue=="TLC LICENSE"?driverLicense:'',
          dmvLicense:radioPaymentTypeValue=="DMV LICENSE"?driverLicense:'',
          emailOrPhone: email_mobile && email_mobile.indexOf("+1") >= 0 ? email_mobile : '+1' + email_mobile,
          password: password,
          activeDeviceId:activeDeviceId,
          activeDeviceModel:activeDeviceModel,
          activeDeviceOs:activeDeviceOs
        }
      }).then((response) => {
        if (response.data && response.data.registerDriver && response.data.registerDriver.ok) {
            driverLoginMutation({
              variables: {
                username: email_mobile,
                password: password
              }
            }).then(async (response1) => {
              if (response1.data && response1.data.driverLogin && response1.data.driverLogin.user) {
                setUser(response1.data.driverLogin.user);
                analytics().setUserId(response1.data.driverLogin.user.id);
                setIsLoadingReg(false);
                if (response1.data.driverLogin.user.driverPtr && response1.data.driverLogin.user.driverPtr.activeInsurance)
                  await AsyncStorage.setItem('activeInsurance', 'true');
                else
                  await AsyncStorage.setItem('activeInsurance', 'false');
                
                if (response1.data.driverLogin.user.currentAgreement)
                  AsyncStorage.setItem('is_null_currentAgreement', 'false');
                else
                  AsyncStorage.setItem('is_null_currentAgreement', 'true');
                
                await AsyncStorage.setItem('isLogin', 'true');
                await AsyncStorage.setItem('token', response1.data.driverLogin.token);
                await AsyncStorage.setItem('user', JSON.stringify(response1.data.driverLogin.user));
                setModalVisible(true)
              }
            }) 
        }
        else if(response.data && response.data.registerDriver && response.data.registerDriver.errors && response.data.registerDriver.errors.length>0 && response.data.registerDriver.errors[0].messages) {
       
          setIsLoadingReg(false);
          errors.error_global = response.data.registerDriver.errors[0].messages;
          setState({ ...state, isLoading: false, errors: errors });
          return
        }
      }).catch((error) => {
       
        AsyncStorage.clear();
        errors.error_global = "Something went wrong! Please contact admin.";
        setIsLoadingReg(false);
        setState({ ...state, isLoading: false, errors: errors });
        return
      });
    }
  }

  useEffect(()=>{
    if(driverData && driverData.checkIfDriverExists && driverData.checkIfDriverExists.driverExists == true && driverData.checkIfDriverExists.passwordExists == true){
      registerDriverInfo(username, passwordreg, driverLicense);
      setIsLoadingReg(false);
    }
    else if (driverData && driverData.checkIfDriverExists && driverData.checkIfDriverExists.driverExists == false && driverData.checkIfDriverExists.passwordExists == false) {
      setIsLoadingReg(false);
      setUsername("");
      // setSkipDriverEmail(false)
      Alert.alert("Error", "We couldn't found your record from our database. Please contact Buggy Costumer support.\n347-334-6313")
    }
     if (driverData && driverData.checkIfDriverExists && driverData.checkIfDriverExists.driverExists == true && driverData.checkIfDriverExists.passwordExists == false) {
      registerDriverInfo(username, passwordreg, driverLicense);
         setIsLoadingReg(false);
         setUsername("");
      // registerDriverInfo(email_mobile, passwordreg, driverLicense);
  }
  else
  {
    setIsLoadingReg(false);
         setUsername(""); 
  }

  },[driverData])



  return (
    <>
      {isLoadingReg &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}
      <LinearGradient start={{ x: 0, y: 0.2 }} end={{ x: 0, y: 0.5 }} colors={["#f9f9f4", "#f9f9f4"]} style={{ flex: 1, height: "100%" }}>
        <KeyboardAwareScrollView scrollEnabled={isRegOne ? false : true} keyboardShouldPersistTaps={'handled'}>
          <Animated.View style={[styles.modalView]}>
            <View style={styles.wrapper}>
              <View style={styles.headContainer}>
                <Image style={styles.logo} resizeMode="contain" source={require('../../assets/images/logo.png')}></Image>
                <Text style={styles.login_heading_text}>{translate("go_your_own_way")}</Text>
              </View>
              <View style={styles.formwrapper}>

                <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>PHONE NUMBER</Text>
                <View style={global_styles.inputWrap_username}>
                  <TextInput
                    style={[global_styles.textInput, global_styles.lato_regular]}
                    placeholder={"16461234567"}
                    value={email_mobile}
                    keyboardType="phone-pad"
                    maxLength={12}
                    onChangeText={(text) => updateTextInput(text, 'email_mobile')}
                  />
                </View>
                <View style={styles.error_message_wrapper}>
                  <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_email_mobile}  {errors.error_global != '' ? errors.error_global.toString() : ''}</Text>
                </View>

                <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>PASSWORD</Text>
                <View style={[global_styles.inputWrap_password]}>
                  <TextInput
                    style={[global_styles.textInput_password, global_styles.lato_regular]}
                    secureTextEntry={hidePasswordReg ? true : false}
                    placeholder={translate("password")}
                    value={passwordreg}
                    onChangeText={(text) => updateTextInput(text, 'passwordreg')}
                  />
                  <TouchableOpacity underlayColor="" activeOpacity={0.8} style={global_styles.touachableButton} onPress={() => setPasswordVisibility('hidePasswordReg', hidePasswordReg)}>
                    {(hidePasswordReg) ? <Text style={global_styles.textInput_password_secure} >Show</Text> : <Text style={global_styles.textInput_password_secure} >Hide</Text>}
                  </TouchableOpacity>
                </View>
                <View style={styles.error_message_wrapper}>
                  <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_password}</Text>
                </View>

                <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>{"CONFIRM PASSWORD"}</Text>
                <View style={[global_styles.inputWrap_password]}>
                  <TextInput
                    style={[global_styles.textInput_password, global_styles.lato_regular]}
                    secureTextEntry={hidePassword1 ? true : false}
                    placeholder={translate("confirm_password")}
                    value={confirm_password}
                    onChangeText={(text) => updateTextInput(text, 'confirm_password')}
                  />
                  <TouchableOpacity underlayColor="" activeOpacity={0.8} style={global_styles.touachableButton} onPress={() => setPasswordVisibility('hidePassword1', hidePassword1)}>
                    {(hidePassword1) ? <Text style={global_styles.textInput_password_secure} >Show</Text> : <Text style={global_styles.textInput_password_secure} >Hide</Text>}
                  </TouchableOpacity>
                </View>
                <View style={styles.error_message_wrapper}>
                  <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_confirm_password}</Text>
                </View>
                <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>{"LICENSE TYPE"}</Text>
                <View >
                      <RNPickerSelect
                                style={pickerSelectStyles}
                                placeholder={{}}
                                onValueChange={(value, index) => radioPaymentMethodChange(value, index)}
                                mode="dropdown"
                                value={radioPaymentTypeValue}
                                items={radio_props}
                                useNativeAndroidPickerStyle={false}
                                Icon={() => {
                                  return (
                                    <View
                                      style={pickerSelectStyles.icon}
                                    />
                                  );
                                }}
                              />
                     </View>
                        <View style={{marginTop:10}}>
                <View>
                  <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>{radioPaymentTypeValue=="TLC LICENSE" ? "TLC LICENSE":"DMV LICENSE"}</Text>
                  <View style={[global_styles.inputWrap_password]}>
               <TextInput
                 style={[global_styles.textInput_password, global_styles.lato_regular]}
                 keyboardType="number-pad"
                 placeholder={radioPaymentTypeValue=="TLC LICENSE"? translate("tlc_license"): translate("dmv_license")}
                 value={driverLicense}
                 onChangeText={(text) => updateTextInput(text, 'driverLicense')}
               />
               </View>
               <View style={styles.error_message_wrapper}>
                <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.errror_driverLicense}</Text>
                </View>
               </View>
               </View>
                <View style={styles.button_container}>
                  <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_orange_wrapper]} onPress={() => { handle_registeration() }} >
                    <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>SIGN UP</Text>
                  </TouchableOpacity>
                  <Text></Text>
                  <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_gray_wrapper]} onPress={() => { goBackLoginScreenHandler() }} >
                    <Text style={[global_styles.bottom_style_gray_text, global_styles.lato_medium]}>{translate("login")}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Animated.View>
        </KeyboardAwareScrollView>
      </LinearGradient>
      <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal} >
        <View style={modal_style.modal}>
          <View style={modal_style.modal_wrapper}>
            <Text style={modal_style.modal_heading}>Success</Text>
            <Text style={modal_style.modalText, { color: 'green' }}>If the information does not look correct, please contact customer support.</Text>
            <View style={modal_style.modal_textbuttonWrap_center}>
              <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => { setModalVisible(false, 'yes'); }}>
                <Text style={modal_style.upload_document_text_modal}>{"OK"}</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Modal>
      {isRegOne &&  <RegsteptwoexistScreen {...props} setModalSuccess={setModalSuccess} reLogin={reLogin} user={user} />}
    </>
  )
}

// Stylesheet to design Registration screen
const styles = StyleSheet.create({
  error_msg: {
    color: "red",
    fontSize: 16,
  },
  button_container: {
    justifyContent: 'center',
    marginTop: 22
  },
  inputWrap: { // Defined but not used please remove this.
    flexDirection: "row",
    marginBottom: 0,
    borderRadius: 7,
    borderColor: '#ccc',
    borderWidth: 1,
    backgroundColor: "#fff",
    width: "100%",
  },
  inputWrap_username: { // Defined but not used please remove this.
    flexDirection: "row",
    flex: 1,
    alignItems: "stretch",
    marginBottom: 0,
    borderRadius: 5,
    height: (Platform.OS === "ios") ? 44 : 0,
  },
  cancel_container: {
    width: 140,
    backgroundColor: '#dedbdb',
    padding: 8,
    borderWidth: 1,
    borderRadius: 8,
    alignItems: "center",
    marginTop: 10,
    alignSelf: "center",
    borderColor: '#dedbdb'
  },
  save_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    alignSelf: "center",
    borderColor: '#db9360'
  },
  save_text: {
    color: '#FFFFFF'
  },
  icon_input: {
    width: 17,
    height: 20,
    marginVertical: 5
  },
  container: {
    flex: 1
  },
  error_message_wrapper: {
    marginBottom: 0,
    paddingVertical: 0,
    marginHorizontal: 5,
    marginLeft: 0
  },
  error_message_text: {
    color: "#f59b42",
    fontSize: 12,
  },
  headContainer: {
    // marginHorizontal: 10,
    // minHeight: 150
    marginVertical: 0,
    marginBottom: 15,
    flex: 1,
    alignItems: "center",
    marginTop: (Platform.OS === "ios") ? 40 : 40,
    marginBottom: (Platform.OS === "ios") ? 70 : 0,
  },
  logo: {
    width: 250,
    height: 60,
    alignSelf: "center"
  },
  login_heading: {
    alignItems: "center",
    marginTop: 0,
  },
  login_heading_text: {
    fontSize: 19,
    color: "#393e5c",
    textAlign: "center",
  },
  login_sub_heading: {
    flex: 1,
    alignItems: "center",
    marginTop: 30,
  },
  login_sub_heading_text: {
    fontSize: 18,
    textAlign: "center",
    color: "#fff",
  },
  modalView: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingTop: 50,
    backgroundColor: '#f9f9f4',
  },
  formwrapper: {
    marginHorizontal: 40,
    marginVertical: 0,
    paddingHorizontal: 0,
    paddingVertical: 0,
    borderRadius: 15,
    marginTop: (Platform.OS === "ios") ? 10 : 20
  },
  input_cotainer: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 0,
    height: 48,
    color: "#FFF",
    backgroundColor: "#266272",
    borderRadius: 10
  },
  input: {
    flex: 2,
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: "#FFF",
    backgroundColor: "#266272",
    borderRadius: 10
  },
  button: {
    color: "#FFF",
    backgroundColor: Colors.color0,
    fontSize: 20,
    flex: 2,
    justifyContent: "center",
    textAlign: "center",
    paddingVertical: 8,
    fontWeight: "bold",
    borderRadius: 5
  },
  touachableButton: {
    paddingTop: Platform.OS === 'ios' ? 8 : 8,
    height: 30,
    width: 40
  },
  buttonPasswordImage: {
    fontSize: 18,
    height: 40,
    width: 40
  },
  registerWrap: {
    flexDirection: 'row',
    marginTop: 15,
    alignSelf: "center",
  },
  registerLinkText: {
    marginVertical: 0,
    fontSize: 16,
    color: "white",
    backgroundColor: "transparent",
  },
  registerLinkText1: {
    marginVertical: 0,
    fontSize: 16,
    color: "white",
    backgroundColor: "transparent",
    textDecorationLine: 'underline',
  }
})

