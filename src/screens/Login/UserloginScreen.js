import React, { useState, useContext, useEffect } from 'react';
import { Animated, Platform, TextInput, StyleSheet, Keyboard, SafeAreaView, ScrollView, ActivityIndicator, Dimensions, View, Text, Image, TouchableOpacity, KeyboardAvoidingView, TouchableHighlight } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { FilledTextField } from 'react-native-material-textfield';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { driverLogin } from '../../graphql/driverLogin';
import { translate } from '../../components/Language';
import Colors from '../../constants/Colors';
import { global_styles } from '../../constants/Style';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { CheckConnectivity } from '../Helper/NetInfo';
import { langContext } from '../Helper/langContext';
//import { all_DriverNotifications } from '../../graphql/driverAllDriverNotifications';
import { checkIfDriverExists } from '../../graphql/checkIfDriverExists';
import { getPaymentIds } from '../../constants/PaymentIDs';
import analytics from '@react-native-firebase/analytics';
const win = Dimensions.get('window');
import DeviceInfo from 'react-native-device-info';
import { stat } from 'react-native-fs';
console.disableYellowBox = true;
const bundleId  = DeviceInfo.getBuildNumber();
/** Login Screen is intended to allow user login into the application */

//Initialisation of constants with default values
export default function LoginScreen(props) {
  const [state, setState] = useState({
    username: '',
    password: '',
    hidePassword: true,
    activeDeviceId:"",
    activeDeviceModel:"",
    activeDeviceOs:"",
    errors: {
      error_username: '',
      error_password: '',
      error_global: ''
    },
    switchVal: true,
    animation: new Animated.Value(1)
  })
  const { username, hidePassword, password, animation, errors } = state;
  const [skipTLC, setSkipTLC] = useState(true);
  const [AdriverLogin] = useMutation(driverLogin);
  const { data: user_data, loading: user_loading, error: user_error } = useQuery(checkIfDriverExists, {
    variables: { tlcLicense: username },
    fetchPolicy: "network-only",
    skip: skipTLC
  })
  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value.trim() });
  }

  ///////////////////////  
  const [driverId, setDriverId] = useState("");
  const [isLoading, setLoader] = useState(false);
  const [landingPage, setLandingPage] = useState(true);

  const { setStatusColor, setNotificationCount } = useContext(langContext);
  useEffect(() => {
    setStatusColor(Colors.status_bar_color);
    // console.log(props.navigation.getParam('from', true))
    // setLandingPage(props.navigation.getParam('from', true))
  }, []);

  const fun_switch = (val) => {

    Keyboard.dismiss()

    let errors = {
      error_username: '',
      error_password: '',
      error_global: ''
    };
    setState({ ...state, errors: errors,username:'',password:'' });
    props.navigation.navigate('SignUp');
    setLandingPage(false)
  }

  const setPasswordVisibility = (field, value) => {
    setState({ ...state, [field]: !value });
  }

  //Call API to check TLC is valid or not.
  useEffect(() => {
    if (user_data && user_data.hasOwnProperty('checkIfDriverExists') && user_data.checkIfDriverExists && user_data.checkIfDriverExists.driverExists == true) {
      setSkipTLC(true);
      login('exist');
    } else if (user_data && user_data.hasOwnProperty('checkIfDriverExists') && user_data.checkIfDriverExists && user_data.checkIfDriverExists.driverExists == false) {
      console.log('skipTLC', skipTLC)
      setSkipTLC(true);
      console.log(user_data)
      checkTLC();
    }
  }, [user_data])

  const login = (flag) => {
    // console.log('login====>', username)
    // setState({...state,activeDeviceId:DeviceInfo.getUniqueId(),activeDeviceModel:DeviceInfo.getModel(),activeDeviceOs:Platform.OS.toUpperCase()+" "+DeviceInfo.getSystemVersion()})
    let login_user_phone = username;
    if (!isNaN(username) && username.length > 8 && (username.length == 10 || username.length == 12)) {
      if (username.indexOf("+1") >= 0) {
        login_user_phone = username;
      } else {
        login_user_phone = "+1" + username;
      }
    }
    AdriverLogin({
      variables: {
        username: login_user_phone,
        password: password,
      }
    }).then((response) => {
      AsyncStorage.setItem('java_name', username);
      AsyncStorage.setItem('java_set_val', password);
      setLoader(false);
      setState({ ...state, username: '', password: '' });
      AsyncStorage.setItem('isLogin', 'true');

      console.log("UID____>>>>", response.data.driverLogin.user.id);
      analytics().setUserId(response.data.driverLogin.user.id);

      let response1 = response;
      if (response1.data.driverLogin.user && response1.data.driverLogin.user.hasOwnProperty('driverPtr') && response1.data.driverLogin.user.driverPtr.hasOwnProperty('unreadNotificationsCount') && response1.data.driverLogin.user.driverPtr.unreadNotificationsCount) {
        let unread = response1.data.driverLogin.user.driverPtr.unreadNotificationsCount;
        if (unread > 99) {
          unread = "99+";
        }
        setNotificationCount(unread);
      }

      if (response1.data.driverLogin.user && response1.data.driverLogin.user.hasOwnProperty('driverPtr') && response1.data.driverLogin.user.driverPtr.hasOwnProperty('reservationDriver') && response1.data.driverLogin.user.driverPtr.reservationDriver.hasOwnProperty('edges') && response1.data.driverLogin.user.driverPtr.reservationDriver.edges.length > 0) {
        let list_reservation = response1.data.driverLogin.user.driverPtr.reservationDriver.edges;
        let reservation_flag = 0;
        list_reservation.map((item) => {
          if (item.node.status == "Open") {
            reservation_flag = 1;
          }
        });
        if (reservation_flag == 1) {
          AsyncStorage.setItem('isReservation', 'true');
        } else {
          AsyncStorage.setItem('isReservation', 'false');
        }
      } else {
        AsyncStorage.setItem('isReservation', 'false');
      }

      if (!(response.data.driverLogin.user && response.data.driverLogin.user.hasOwnProperty('driverPtr') && response.data.driverLogin.user.driverPtr.hasOwnProperty('activeInsurance') && response.data.driverLogin.user.driverPtr.activeInsurance !== null)) {
        AsyncStorage.setItem('activeInsurance', 'true');
      } else {
        AsyncStorage.setItem('activeInsurance', 'false');
      }

      if (response.data.driverLogin.user && response.data.driverLogin.user.hasOwnProperty('paymentMethod') && response.data.driverLogin.user.paymentMethod != "") {
        let payment_Method = response.data.driverLogin.user.paymentMethod;
        let p_value = 0;
        if (payment_Method == "Card") {
          p_value = getPaymentIds(props, "card");
        }
        if (payment_Method == "Zelle") {
          p_value = getPaymentIds(props, "zelle");
        }
        if (payment_Method == "Remote Cash") {
          p_value = getPaymentIds(props, "remotecash");
        }
        console.log("payment_Method::::", payment_Method);
        AsyncStorage.setItem('current_payment_id', p_value.toString());
      }

      if (response.data.driverLogin.user && response.data.driverLogin.user.hasOwnProperty('currentAgreement') && response.data.driverLogin.user.currentAgreement === null) {
        AsyncStorage.setItem('is_null_currentAgreement', 'false');
      } else {
        if (response.data.driverLogin.user && response.data.driverLogin.user.hasOwnProperty('currentAgreement') && response.data.driverLogin.user.currentAgreement != null) {
          AsyncStorage.setItem('currentAgreement', JSON.stringify(response.data.driverLogin.user.currentAgreement));
          AsyncStorage.setItem('current_car', response.data.driverLogin.user.currentAgreement.car.pk.toString());
          let cname = response.data.driverLogin.user.currentAgreement.car.year + " " + response.data.driverLogin.user.currentAgreement.car.color + " " + response.data.driverLogin.user.currentAgreement.car.model;

          AsyncStorage.setItem('current_car_name', cname);
          if (response.data.driverLogin.user.currentAgreement.hasOwnProperty('carreturn') && response.data.driverLogin.user.currentAgreement.carreturn != null && response.data.driverLogin.user.currentAgreement.carreturn.hasOwnProperty('returnType')) {
            if (response.data.driverLogin.user.currentAgreement.stage) {
              AsyncStorage.setItem('is_switched_car_stage', response.data.driverLogin.user.currentAgreement.stage);
            }
            if (response.data.driverLogin.user.currentAgreement.carreturn.returnType != null && response.data.driverLogin.user.currentAgreement.carreturn.returnType != "") {
              AsyncStorage.setItem('car_return_type', response.data.driverLogin.user.currentAgreement.carreturn.returnType);
            } else {
              AsyncStorage.setItem('car_return_type', '');
            }
          }
        }
        AsyncStorage.setItem('is_null_currentAgreement', 'true');
      }

      AsyncStorage.setItem('token', response.data.driverLogin.token);
      //if(response.data.driverLogin.user.dmvLicense=='' && response.data.driverLogin.user.tlcLicense ==''){
      if (response.data.driverLogin.user.dmvLicense == '') {
        AsyncStorage.setItem('reg_user_id', response.data.driverLogin.user.id);
      } else {
        AsyncStorage.setItem('reg_user_id', 'reg_user_id');
      }

      if (response.data.driverLogin.user.dmvLicense == '') {
        AsyncStorage.setItem('allow_apply_now', 'yes');
      } else {
        AsyncStorage.setItem('allow_apply_now', 'no');
      }

      AsyncStorage.setItem('user', JSON.stringify(response.data.driverLogin.user));

      if (response.data.driverLogin.user.id)
        setDriverId(response.data.driverLogin.user.id);

      props.navigation.navigate("App");

    }).catch((error) => {
      console.log(error)
      AsyncStorage.clear();
      setLoader(false);
      errors.error_global = translate("incorrect_login_details");
      setState({ ...state, errors: errors });
      return
    });
  }

  const  forgetPassword =()=>{
    setState({ ...state, errors: errors,username:'',password:'' });
    props.navigation.navigate('Forgotpassword');
    setLandingPage(false)
  }
  const checkTLC = async () => {
    let tlc_flag = await verifyTlcLicense(username);
    console.log('tlc_flag', tlc_flag)
    if (!tlc_flag) {
      errors.error_username = translate("error_valid_tlc");
      setState({ ...state, errors: errors });
      setLoader(false);
    } else {
      errors.error_username = "";
      setState({ ...state, errors: errors });
      login('tlc');
    }
  }
  const verifyTlcLicense = async (tlcLicenseNumber) => {
    //Verify TLC License from the NYC Open Data Website. Returns a single array containing license info object
    let nycOpenDataUrl = "https://data.cityofnewyork.us/resource/xjfq-wh2d.json?license_number=" + tlcLicenseNumber;
    let appToken = "5WBDkAQLj244SKLuDJmXDVDhT";
    const res = await fetch(nycOpenDataUrl, {
      method: 'GET',
      data: {
        "$limit": 1,
        "$$app_token": appToken
      }
    });
    const data = await res.json();
    //console.log('TLC',data);
    //Returns an array
    if (data && data[0] && data[0].name) {
      return true;
    }
    else {
      return false;
    }
  }
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.isModal = true;
      console.log(props.screenProps)
      return;
    }
  }
  const fun_login = async () => {
    
    // console.log("active_device_id",DeviceInfo.getUniqueId());
    // console.log("active_device_model",DeviceInfo.getModel());
    // console.log("active_device_os",Platform.OS.toUpperCase()+" "+DeviceInfo.getSystemVersion());
  
    Keyboard.dismiss()

    const net = checkNet();
    if (net) {
      setLoader(true);
      let flg_error = 0;
      let tlc_check = 0;
      const expression = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
      /* else if (isNaN(username) && expression.test(username) === false) {
        errors.error_username = "Please enter valid Phone Number";
        flg_error = 1;
      } */

      console.log("-------------+", username.indexOf("+1") + "+-" + (!isNaN(username)) + "--+" + username);
      if (username == '' || (username.indexOf("+1") < 0 && (!isNaN(username)) == false)) {
        errors.error_username = translate("error_username");
        flg_error = 1;
      }
      else if (!isNaN(username) && username.length < 6) {
        flg_error = 1;
        errors.error_username = "Please enter valid Phone Number";
      } else if (!isNaN(username) && username.length > 8 && (username.length != 10 && username.length != 12)) {
        flg_error = 1;
        errors.error_username = translate("error_valid_phone");
      } else if (!isNaN(username) && (username.length >= 6 && username.length < 8)) {
        flg_error = 1;
        errors.error_username = translate("error_valid_phone");
      } else {
        errors.error_username = "";
      }

      if (password == '') {
        errors.error_password = translate("error_password");
        flg_error = 1;
      } else {
        errors.error_password = "";
      }

      if (flg_error == 1) {
        errors.error_global = '';
        setLoader(false);
        setState({ ...state, errors: errors });
        return;
      } else {
        setState({ ...state, errors: errors });
        if (tlc_check == 1) {
          setSkipTLC(false);
        } else {
          login('direct');
        }
      }
    }
  }
  const userIcon = () => {
    return (
      <Image width="20" style={styles.icon_input} source={require("../../assets/images/username-icon.png")} />
    )
  }
  const passwordIcon = () => {
    return (
      <Image width="20" style={styles.icon_input} source={require("../../assets/images/password-icon.png")} />
    )
  }
  const passwordEye = () => {
    return (<TouchableOpacity underlayColor="" activeOpacity={0.8} style={styles.touachableButton} onPress={() => setPasswordVisibility('hidePassword', hidePassword)}>
      {(hidePassword) ? <Text style={{ color: "#454545" }} >Hide</Text> : <Text style={{ color: "#454545" }}>Show</Text>}
    </TouchableOpacity>)
  }
  //Remove this code  for different login desing screens
  const fun_screen_change = (screen1) => {
    if (screen1 == "screen1") {
      props.navigation.navigate("SignIn");
    } else if (screen1 == "screen2") {
      props.navigation.navigate("Userlogin2");
    } else if (screen1 == "screen3") {
      props.navigation.navigate("Userlogin3");
    }
  }

  const fun_login_page = () => {
   
    setLandingPage(false);
  }
  //Render the UI of login screen
  return (
    <>
      {isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}

      {landingPage ?
        <View style={styles.main_container}>
          <View style={[styles.headContainer]}>
            <Image style={styles.logo} resizeMode="contain" source={require('../../assets/images/logo.png')}></Image>
            <Text style={[styles.login_heading_text, global_styles.lato_regular]}>{translate("go_your_own_way")}</Text>
          </View>
          <View style={[styles.wrapper]}>
            <View style={[styles.button_container, { paddingTop: 20 }]}>
              <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_orange_wrapper]} onPress={() => { fun_login_page(); }} >
                <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate("login")}</Text>
              </TouchableOpacity>
              <Text></Text>
              <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_gray_wrapper]} onPress={() => { fun_switch() }} >
                <Text style={[global_styles.bottom_style_gray_text, global_styles.lato_medium]}>SIGN UP</Text>
               
              </TouchableOpacity>
            </View>
            
          </View>
          <View style={[styles.landing_page_img_wrap]}>
        
            <Image style={styles.landing_page_image} source={require('../../assets/images/Landing-entrance.gif')}></Image>
      
          </View>
          <Text style={{fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Medium",
                fontWeight: '400',
                width:"100%",
                textAlign:"center",
                fontStyle: 'normal'}}>App version: {bundleId}</Text>
        </View>
        
        :

        <View style={styles.main_container}>
          <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'}>
            <View style={[styles.headContainer]}>
              <Image style={styles.logo} resizeMode="contain" source={require('../../assets/images/logo.png')}></Image>
              <Text style={[styles.login_heading_text, global_styles.lato_regular]}>{translate("go_your_own_way")}</Text>
            </View>
            <View style={[styles.wrapper]}>

              <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>PHONE NUMBER</Text>
              <View style={global_styles.inputWrap_username}>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={"16461234567"}
                  keyboardType="phone-pad"
                  value={username}
                  onChangeText={(text) => updateTextInput(text, 'username')}
                  onSubmitEditing={Keyboard.dismiss}
                />
              </View>

              <View style={styles.error_message_wrapper}>
                <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_username}  {errors.error_global != '' ? errors.error_global.toString() : ''}</Text>
              </View>

              <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>PASSWORD</Text>
              <View style={[global_styles.inputWrap_password]}>
                <TextInput
                  style={[global_styles.textInput_password, global_styles.lato_regular]}
                  secureTextEntry={hidePassword ? true : false}
                  placeholder={translate("password")}
                  value={password}
                  onChangeText={(text) => updateTextInput(text, 'password')}
                  onSubmitEditing={Keyboard.dismiss}
                />
                <TouchableOpacity underlayColor="" activeOpacity={0.8} style={global_styles.touachableButton} onPress={() => setPasswordVisibility('hidePassword', hidePassword)}>
                  {(hidePassword) ? <Text style={global_styles.textInput_password_secure} >Show</Text> : <Text style={global_styles.textInput_password_secure} >Hide</Text>}
                </TouchableOpacity>
              </View>
              <TouchableHighlight underlayColor='' onPress={() => {(forgetPassword())}} >
              <View style={styles.forgot_link}>
                <Text style={styles.forgot_link_text}>Forgot Password?</Text>
              </View>
              </TouchableHighlight>
              <View style={[styles.error_message_wrapper, { marginTop: -20 }]}>
                <Text style={[styles.error_message_text, global_styles.lato_regular]}>{errors.error_password}</Text>
              </View>

              <View style={styles.button_container, { marginTop: 30 }}>
                <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_orange_wrapper]} onPress={() => { fun_login(); }} >
                  <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate("login")}</Text>
                </TouchableOpacity>
                <Text></Text>
                <TouchableOpacity style={[{ alignSelf: "center" }, global_styles.bottom_style_gray_wrapper]} onPress={() => { fun_switch() }} >
                  <Text style={[global_styles.bottom_style_gray_text, global_styles.lato_medium]}>SIGN UP</Text>
                </TouchableOpacity>
              </View>

            </View>
          </KeyboardAwareScrollView>
        </View>


      }

    </>
  )
}

//StyleSheet for login screen

const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingHorizontal: 35,
    paddingTop: 22,
    paddingBottom: 12,
  },
  landing_page_img_wrap: {
    marginVertical: 0,
    height: 200,
    alignContent: "flex-start",
    alignItems: "flex-start"
  },
  button_container: {
    justifyContent: 'center',
    marginTop: 10,
    borderColor: "#333"
  },
  cancel_container: {
    width: 140,
    backgroundColor: '#dedbdb',
    padding: 8,
    borderWidth: 1,
    borderRadius: 8,
    alignItems: "center",
    marginTop: 10,
    alignSelf: "center",
    borderColor: '#dedbdb'
  },
  save_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    alignSelf: "center",
    borderColor: '#db9360'
  },
  error_msg: {
    color: "red",
    fontSize: 16,
  },
  icon_input: {
    width: 17,
    height: 20,
    marginVertical: 5
  },
  container: {
    flex: 1
  },
  screen_change_text: {
    color: "#f59b42",
    fontSize: 16,
    marginRight: 15,
    textDecorationLine: "underline",
    marginTop: 10
  },
  forgot_link: {
    flexDirection: 'row',
    marginVertical: 0,
    alignSelf: "flex-end",
    marginTop: 2
  },
  forgot_link_text: {
    color: "#2dafd3",
    fontSize: 13,
    marginTop: 5,
  },
  error_message_wrapper: {
    flexDirection: "row",
    alignItems: "flex-end",
    marginBottom: 0,
    paddingVertical: 0,
    marginLeft: 0,
  },
  error_message_text: {
    color: "#f59b42",
    fontSize: 12,
    justifyContent: "flex-end",
  },
  headContainer: {
    marginVertical: 0,
    marginBottom: (Platform.OS === "ios") ? 40 : 15,
    flex: 1,
    alignItems: "center",
    marginTop: (Platform.OS === "ios") ? 70 : 40,
  },
  logo: {
    width: 250,
    height: 60,
  },
  landing_page_image: {
    alignSelf: "center",
    width: "100%",
    maxHeight: 220
  },
  login_heading: {
    flex: 1,
    alignItems: "center",
    marginTop: 0,
    marginBottom: 25,
  },
  login_heading_text: {
    fontSize: 19,
    color: "#393e5c",
    textAlign: "center",
  },
  login_sub_heading: { // Defined but not used please remove this.
    flex: 1,
    alignItems: "center",
    marginTop: 30,
  },
  login_sub_heading_text: { // Defined but not used please remove this.
    fontSize: 18,
    textAlign: "center",
    color: "#fff",
  },
  modal: {
    flex: 1,
  },
  wrapper: {
    borderRadius: 15,
    marginVertical: 30,
    minHeight: 160,
    marginTop: 80,
    alignContent: "flex-start"
  },
  inputWrapLast: {
    marginBottom: 0,
  },
  WrapBorder: { // Defined but not used please remove this.
    flexDirection: "row",
  },
  input_cotainer: {
    flex: 1,
    paddingHorizontal: 0,
    paddingVertical: 0,
    height: 48,
    color: "#d4d4d4",
    borderWidth: 1,
    borderColor: "#ccc",
    backgroundColor: "#FFF",
    borderRadius: 10
  },
  input: {
    flex: 2,
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: "#d4d4d4",
    backgroundColor: "#fff",
    borderRadius: 10
  },
  loginButton: {
    alignItems: "center",
    flexDirection: "row",
  },
  button: {
    color: "#FFF",
    backgroundColor: Colors.color0,
    fontSize: 20,
    flex: 2,
    justifyContent: "center",
    textAlign: "center",
    paddingVertical: 8,
    fontWeight: "bold",
    borderRadius: 5
  },

})
