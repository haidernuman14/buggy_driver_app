import React, { useEffect } from 'react';  //Unused import function "React"
import AsyncStorage from '@react-native-community/async-storage';

/** This screen is used in navigation panel */
export default function LogoutScreen(props) {
  useEffect(() => {
    signOutAsync()
  }, [])
  const signOutAsync = async () => {
    await AsyncStorage.clear(); //clear the current user session data
    props.navigation.navigate('SignIn');
  };
  return null;
}
