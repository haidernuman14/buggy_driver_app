import React, { Component } from 'react';
import { Animated, Dimensions, BackHandler, Picker, StyleSheet, ActivityIndicator, TouchableOpacity, TouchableHighlight, View, Text, TextInput, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AsyncStorage from '@react-native-community/async-storage';
import { useMutation } from '@apollo/react-hooks';
import { Mutation } from 'react-apollo';
import client from 'graphql';
import { driverCompleteRegister } from '../../graphql/driverCompleteRegister';
import { global_styles, modal_style } from '../../constants/Style';
import Colors from '../../constants/Colors';
import Modal from "react-native-modal";
import { translate } from '../../components/Language';
import { CheckConnectivity } from '../Helper/NetInfo';
import RadioForm from 'react-native-simple-radio-button';
import Icon3 from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-picker';;
import RegstepfourScreen from './RegstepfourScreen';
import { driverUploadDocument } from '../../graphql/driverUploadDocument';

// Get screen dimensions
const win = Dimensions.get('window');

/**
 * This class allows a driver to upload (three) more documents 
 * then redirects to next registration screen.
 * It uploads documents to Buggy server using driverUploadDocument API.
 */

export default class RegstepthreeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isModal: false,
      isRegFour: false,
      radioValue: 0,
      document_1: {
        type: "",
        name: "",
        size: "",
        uri: "",
      },
      document_2: {
        type: "",
        name: "",
        size: "",
        uri: "",
      },
      document_3: {
        type: "",
        name: "",
        size: "",
        uri: "",
      },
      document_1_name: '',
      document_2_name: '',
      document_3_name: '',
      errors: {
        error_document_1: '',
        error_document_2: '',
        error_document_3: '',
        error_global: '',
      },
      slideUp: {},
      animation: new Animated.Value(0)
    }
  }

  componentDidMount() {
    this.details();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton  = () => {
    return true;
  }

  updateTextInput = (value, field) => {
    this.setState({ [field]: value });
  }

  details = async () => {
    const screenHeight = win.height;
    const user = await AsyncStorage.getItem('user');
    let user_id = JSON.parse(user).id;
    this.setState({ user_id: user_id });
    let slideUpob = {
      transform: [
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0.01, 10],
            outputRange: [0, 0],
            extrapolate: "extend",
          }),
        },
      ],
    }
    this.setState({ slideUp: slideUpob });
  }

  fun_register_model = async () => {
    this.setState({ isModal: false, isRegFour: true });
  }

  setModalVisible = (value) => {
    this.setState({ ...this.state, isModal: value });
  }

  radioButtonChange = (value) => {
    this.setState({ ...this.state, radioValue: value });
  }

  allowedFileType = (fileType) => {
    const allowed = ["jpg", "jpeg", "png"]
    let extension = "";
    if (fileType) {
      let split_ = fileType.split("/");
      if (split_.length > 1 && split_[1]) {
        extension = split_[1].toLowerCase()
      }
    }
    return allowed.includes(extension);
  }

  fun_choose_document = async (doc_name) => {
    let options = {
      title: 'Select Image From',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      maxWidth: 500,
      maxHeight: 500
    };
    await ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        if (this.allowedFileType(response.type)) {
          let f_name = response.uri.substring(response.uri.lastIndexOf('/') + 1)
          let extension = f_name.split('.').pop();
          let _document_1 = { ...this.state.document_1 };
          if (doc_name == 'document_2') {
            _document_1 = { ...this.state.document_2 };
          } else if (doc_name == 'document_3') {
            _document_1 = { ...this.state.document_3 };
          }

          _document_1.filePath = response;
          _document_1.fileData = response.data;
          _document_1.fileUri = response.uri;
          _document_1.fileName = f_name;
          this.setState({
            [doc_name]: _document_1,
            [doc_name + "_name"]: doc_name + '.' + extension,
            isLoading: false
          });

          let error = { ...this.state.errors };
          error.error_document_1 = "";
          error.error_document_2 = "";
          error.error_document_3 = "";

          this.setState({ isLoading: false, errors: error })
        } else {

          let error = { ...this.state.errors };
          if (doc_name == "document_1")
            error.error_document_1 = "File type not accepted. Please upload a .jpg,.jpeg or .png file";
          else if (doc_name == "document_2")
            error.error_document_2 = "File type not accepted. Please upload a .jpg,.jpeg or .png file";
          else if (doc_name == "document_3")
            error.error_document_3 = "File type not accepted. Please upload a .jpg,.jpeg or .png file";

          this.setState({ isLoading: false, errors: error });
        }
      }
    });
  }

  imageUploadHandler = async (send, flag) => {

    const { user_id, document_1, document_2, radioValue, document_3, document_1_name, document_2_name, document_3_name } = this.state;

    let netState = await CheckConnectivity();
    if (!netState) {
      this.props.screenProps.setNetModal(true)
      return;
    }

    if (flag == "skip" || radioValue == 0) {
      this.setState({ isRegFour: true });
    } else {

      let flag_val = 0;
      let error = { ...this.state.errors };

      if (document_1.name == '') {
        error.error_document_1 = "Please upload document file.";
        flag_val = 1;
      } else {
        error.error_document_1 = "";
      }

      if (document_2.name == '') {
        error.error_document_2 = "Please upload document file.";
        flag_val = 1;
      } else {
        error.error_document_2 = "";
      }

      if (document_3.name == '') {
        error.error_document_3 = "Please upload document file.";
        flag_val = 1;
      } else {
        error.error_document_3 = "";
      }

      if (document_1_name == "" && document_2_name == "" && document_3_name == "") {

        this.setState({ isLoading: false, errors: error });
        return;

      } else {

        error.error_document_1 = "";
        error.error_document_2 = "";
        error.error_document_3 = "";
        this.setState({ errors: error, isLoading: true });


        console.log("input-->>", user_id, document_1.fileName);
        // Upload Document 1
        if (user_id && document_1_name != '' && user_id != '') {
          let input = { file: document_1.fileData, driverId: user_id, documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZToxMA==", fileName: document_1.fileName }
          // console.log("input-->>",input); 
          send({ variables: { input } });
        }

        // Upload Document 2
        if (user_id && document_2_name != '' && user_id != '') {
          let input = { file: document_2.fileData, driverId: user_id, documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZToxMA==", fileName: document_2.fileName }
          send({ variables: { input } });
        }

        // Upload Document 3
        if (user_id && document_3_name != '' && user_id != '') {
          let input = { file: document_3.fileData, driverId: user_id, documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZToxMA==", fileName: document_3.fileName }
          send({ variables: { input } });
        }
        //console.log("input-->>",input); 

      }

    }

  }

  getUploadDocumentResponse = async (data) => {

    console.log(" ==== upload document response ->", data);
    console.log(" ==== upload document response ->", data.uploadDocument);
    console.log("upload document response ->", data.uploadDocument.errors);

    let error = { ...this.state.errors };
    if (!data.uploadDocument.ok) {
      error.error_document_1 = "Image has been not uploaded. Please try again with proper image.";
      this.setState({ isLoading: false, errors: error });
    } else {
      this.setState({ isLoading: false, isModal: true });
    }

  }

  setRegFour = (value) => {
    this.setState({ isRegFour: value });
  }

  setRegFourClose = (value) => {
    this.setState({ isRegFour: value });
    this.props.setRegThreeClose(false);
  }

  render() {
    const { isLoading, isRegFour, isModal, user_id, radioValue, document_1, document_2, document_3, document_3_name, document_2_name, document_1_name, errors } = this.state;
    const screenHeight = Dimensions.get("window").height;

    var radio_props = [
      { label: translate("no") + '          ', value: 0 },
      { label: translate("yes") + '   ', value: 1 },
    ];

    return (<>
      {isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}
      {!isRegFour && <Mutation
        client={client}
        mutation={driverUploadDocument}
        onCompleted={(data) => this.getUploadDocumentResponse(data)}>
        {(AdriverUploadDocument, { loading, error }) => (
          <>
            <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal} >
              <View style={modal_style.modal}>
                <View style={modal_style.modal_wrapper}>
                  <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => { this.fun_register_model(); }}>
                    <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                  </TouchableOpacity>
                  <Text style={modal_style.modal_heading}>Success</Text> 
                  <Text style={modal_style.modalText}>{translate('document_upload_message')}</Text>
                  <View style={modal_style.modal_textbuttonWrap_center}>
                    <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => { this.fun_register_model(); }}>
                      <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </Modal>

            <Animated.View style={[styles.Animated, this.state.slideUp]}>
              <KeyboardAwareScrollView>
                <View style={styles.title}>
                  <Text style={styles.titleText}>{translate("apply_drive")}</Text>
                </View>
                <View style={styles.wrap_images}>
                  <Image width="20" style={styles.frame_image} source={require('../../assets/images/step3.png')} />
                </View>
                <View style={styles.wrapper}>
                  {errors.error_global != '' &&
                    <View>
                      <Text style={styles.error_msg}>{errors.error_global}</Text>
                    </View>
                  }

                  <View style={styles.inputWrap}>
                    <Text>{translate("do_you_have_other_documents")}</Text>
                    <RadioForm
                      animation={false}
                      radio_props={radio_props}
                      initial={0}
                      style={styles.radio_button}
                      formHorizontal={true}
                      onValueChange={(value) => this.radioButtonChange(value)}
                      onPress={(value) => this.radioButtonChange(value)}
                    />

                    {radioValue == 1 &&
                      <View style={styles.wrapper_upload_controll}>
                        {errors.error_global != '' &&
                          <View>
                            <Text style={styles.error_msg}>{errors.error_global}</Text>
                          </View>
                        }

                        <View style={styles.view_text_lable} >
                          <Text style={styles.text_right_upload} >{translate("you_can_upload")}</Text>
                          <TouchableHighlight underlayColor="" style={styles.text_right_wrap} onPress={() => { this.fun_choose_document("document_1"); }}>
                            <View style={{ position: "relative" }}>
                              <Text style={styles.text_field} >{document_1_name}</Text>
                              <Icon3 style={styles.text_icon} name='upload' />
                            </View>
                          </TouchableHighlight>
                          {errors.error_document_1 != '' &&
                            <View>
                              <Text style={styles.error_msg}>{errors.error_document_1}</Text>
                            </View>
                          }

                          <TouchableHighlight underlayColor="" style={styles.text_right_wrap} onPress={() => { this.fun_choose_document("document_2"); }}>
                            <View style={{ position: "relative" }}>
                              <Text style={styles.text_field} >{document_2_name}</Text>
                              <Icon3 style={styles.text_icon} name='upload' />
                            </View>
                          </TouchableHighlight>
                          {errors.error_document_2 != '' &&
                            <View>
                              <Text style={styles.error_msg}>{errors.error_document_2}</Text>
                            </View>
                          }

                          <TouchableHighlight underlayColor="" style={styles.text_right_wrap} onPress={() => { this.fun_choose_document("document_3"); }}>
                            <View style={{ position: "relative" }}>
                              <Text style={styles.text_field} >{document_3_name}</Text>
                              <Icon3 style={styles.text_icon} name='upload' />
                            </View>
                          </TouchableHighlight>
                          {errors.error_document_3 != '' &&
                            <View>
                              <Text style={styles.error_msg}>{errors.error_document_3}</Text>
                            </View>
                          }


                        </View>

                      </View>
                    }

                  </View>
                </View>
                <View style={styles.textbuttonWrap}>
                  <TouchableHighlight underlayColor="" style={styles.linkWrap} onPress={() => { this.imageUploadHandler(AdriverUploadDocument, "skip"); }} >
                    <Text style={styles.lnk_right_text}>{translate("skip")}</Text>
                  </TouchableHighlight>
                  <TouchableHighlight underlayColor="" style={styles.upload_document} onPress={() => { this.imageUploadHandler(AdriverUploadDocument, "submit"); }} >
                    <Text style={styles.upload_document_text}>{translate("submit")}</Text>
                  </TouchableHighlight>
                </View>
              </KeyboardAwareScrollView>
            </Animated.View>
          </>)}
      </Mutation>}
      {isRegFour && <RegstepfourScreen {...this.props} driverId={user_id} setModalSuccess={this.props.setModalSuccess} setRegFour={this.setRegFour} setRegFourClose={this.setRegFourClose} />}
    </>
    );
  }
}

// Stylesheet to design registration screen
const styles = StyleSheet.create({
  Animated: {
    flex: 8,
  },
  radio_button: {
    flex: 1,
    flexDirection: "row",
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "center",
  },
  info_main: {
    flex: 1,
  },
  close_text_icon: {
    color: "red",
    fontSize: 22,
    fontWeight: "bold"
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: 25,
    paddingVertical: 20,
    paddingBottom: 0,
  },
  wrap_images: {
    paddingTop: 10,
    marginVertical: 0,
    resizeMode: "contain",
    alignItems: "center"
  },
  frame_image: {
    maxWidth: 310,
    resizeMode: "contain"
  },
  title: {
    flex: 1,
    flexDirection: "row",
    width: win.width,
    marginVertical: 20,
    alignContent: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#9e9d9b",
  },
  title_wrap: {
    flex: 1,
    height: 80,
    marginBottom: 0,
    paddingBottom: 0,
    marginVertical: 0,
    paddingVertical: 0,
    width: win.width,
  },
  title_modal: {
    width: 50,
    height: 50,
    position: "absolute",
    right: 0,
    top: 0,
    flex: 1,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#000",
  },
  titleText: {
    flex: 1,
    paddingLeft: 30,
    paddingBottom: 15,
    fontWeight: "700",
    fontSize: 20,
  },
  titleClose: {
    marginRight: 20,
    marginTop: -0,
    fontWeight: "bold",
    fontSize: 22,
  },
  inputWrap: {
    marginBottom: 15,
  },
  input: {
    flex: 1,
  },
  input2: {
    borderWidth: 0,
    marginTop: 0,
    paddingTop: 0,
    marginVertical: 0,
    paddingVertical: 0,
    height: 40,
    borderBottomColor: 'transparent'
  },
  input_text_field_container: {
    borderWidth: 1,
    borderColor: "#333",
    marginTop: 5,
    paddingTop: 0,
    marginVertical: 0,
    paddingVertical: 0,
    height: 40,
  },
  WrapBorder: {
    flexDirection: "row",
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  textbuttonWrap: {
    paddingHorizontal: 30,
    paddingVertical: 0,
    flexDirection: "row",
  },
  error_msg: {
    marginTop: 0,
    marginLeft: 0,
    color: "red",
    fontSize: 16,
    marginBottom: 14
  },
  textbuttonLeftView: {
    flex: 1,
    width: 20,
    backgroundColor: 'transparent',
  },
  textbuttonRightView: {
    width: 100,
    position: "absolute",
    right: 20,
    top: 5,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    backgroundColor: 'transparent',
  },
  select_picker: {
    color: "#918e8e",
    borderWidth: 2,
    borderColor: "#333"
  },
  linkWrap: {
    flex: 1,
    marginVertical: 10,
    justifyContent: "center"
  },
  lnk_right_text: {
    textDecorationLine: 'underline',
    color: "#8c8c8b",
    fontSize: 14,
  },
  link_color: {
    textDecorationLine: 'underline',
    color: "#8c8c8b",
    fontSize: 14,
    height: 30,
  },
  view_text_lable: {
    marginTop: 15
  },
  upload_document: {
    backgroundColor: "#db9360",
    borderRadius: 5,
    paddingHorizontal: 10,
    justifyContent: "center",
    marginLeft: 70,
  },
  upload_document_text: {
    color: "#FFFFFF",
    fontWeight: "bold",
    fontSize: 14,
  },
  text_icon: {
    color: '#9e9d9b',
    fontSize: 22,
    position: "absolute",
    right: 0,
    top: 0,
    bottom: 0
  },
  text_right_wrap: {
    borderColor: "#9e9d9b",
    borderBottomWidth: 1,
    position: "relative",
    padding: 0,
    paddingTop: 10,
    marginTop: 5
  },
  text_field: {
    width: "80%",
    padding: 5
  },
});
