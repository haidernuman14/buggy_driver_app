import React, { useState, useEffect, useRef } from 'react';
import { Animated, BackHandler, ActivityIndicator, Image, TouchableOpacity, Dimensions, StyleSheet, Text, View, TouchableHighlight, ScrollView, Platform } from 'react-native';
import Modal from "react-native-modal";
import Colors from '../../constants/Colors';
import { translate } from '../../components/Language';
import SignatureCapture from 'react-native-signature-capture';
import InsurenceFormScreen from './InsurenceFormScreen'
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style } from '../../constants/Style';
import { driverUploadDocument } from '../../graphql/driverUploadDocument';
import { useMutation, useQuery } from '@apollo/react-hooks';

// Get dimensions of screen
const win = Dimensions.get('window');

/**
 * Get eSignature from user for insurance form and uplaod it to server.
 * It uses Signature pad where user can do signature with finger tips.  
 */
export default function RegstepfourScreen(props) {
  const [state, setState] = useState({
    show: false,
    isModal: false,
    animation: new Animated.Value(0),
    drag: false,
    isLoading: false,
    error: '',
  })
  const { show, isLoading, isModal, animation, drag, error } = state;
  const [AdriverUploadDocument] = useMutation(driverUploadDocument);
  const is_direct=(props.navigation.hasOwnProperty('state') && props.navigation.state.hasOwnProperty('params') && props.navigation.state.params && props.navigation.state.params.hasOwnProperty('signdirect') && props.navigation.state.params.signdirect)?true:false
  const [is_direct_sign, setIsDirectSign] = useState(is_direct)
  const d = props.hasOwnProperty('driverId') ? props.driverId : ((props.navigation.state.hasOwnProperty('params') && props.navigation.state.params && props.navigation.state.params.hasOwnProperty('driverId') && props.navigation.state.params.driverId) ? props.navigation.state.params.driverId : '');
  const [driverId, setdriverId] = useState(d)
  const inputEl = useRef('sign');

  useEffect(() => { 
    
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    }
    const navFocusListener = props.navigation.addListener('didFocus', () => {
      BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    });
    return () => {
      navFocusListener.remove();
    };
  }, []);

  const handleBackButton  = () => {
    return true;
  } 

  const screenHeight = win.height;
  const slideUp = {
    transform: [
      {
        translateY: animation.interpolate({
          inputRange: [0.01, 10],
          outputRange: [0, -80 * screenHeight],
          extrapolate: "extend",
        }),
      },
    ],
  }

  const showPDF = () => {
    setState({ ...state, show: true })
  }
  const hidePDF = () => {
    setState({ ...state, show: false })
  }

  // Save signature image
  const saveSign = () => {
    if (drag == true) {
      inputEl.current.saveImage();
      setState({ ...state, isLoading: true });
    } else {
      setState({ ...state, error: 'Please enter signature.' });
    }
  }

  // Reset signature
  const resetSign = () => {
    // console.log("did", driverId);
    inputEl.current.resetImage();
    setState({ ...state, drag: false, error: '' })
  }

  const _onSaveEvent = async (result) => {
    //result.encoded - for the base64 encoded png
    //result.pathName - for the file path name
    if (result.encoded && result.encoded != "") {
      imageUpload(result);
    } else {
      setState({ ...state, isLoading: false, error: 'Please enter signature.' });
    }
  }
  const _onDragEvent = () => {
    // This callback will be called when the user enters signature
    // console.log("dragged");
    setState({ ...state, drag: true, error: '' })
  }

  const imageUpload = (image) => {
    // console.log('driverId', driverId)
    if (driverId != '' && image.pathName != '' && image.encoded != '') {
      let input = { file: image.encoded, driverId: driverId, documentTypeId:(props.screenProps.build_type=="live")?"RHJpdmVyRG9jdW1lbnRUeXBlTm9kZToyMA==":"RHJpdmVyRG9jdW1lbnRUeXBlTm9kZToxOQ==", fileName: image.pathName }
      // console.log("input-->",input);
      AdriverUploadDocument({
        variables: { input }
      }).then((response) => {
        // console.log(is_direct_sign)
        // console.log("uploadDocument Sign >>", response.data.uploadDocument);
        if (response.data.uploadDocument.ok) {
          if (is_direct_sign) {
            setState({ ...state, isLoading: false, isModal: true, error: '' });
          } else {
            if (props.reg) {
              setState({ ...state, isLoading: false, isModal: true, error: '' });
            } else {
              AsyncStorage.setItem('reg_user_id', 'reg_user_id');
              setState({ ...state, isLoading: false, error: '' });
              props.setRegFourClose(false);
            }
          }
        } else {
          setState({ ...state, isLoading: false, error: 'Please write your valid signature.' });
        }
      }).catch((error) => {
        // console.log("uploadDocument Sign >>", error);
        setState({ ...state, isLoading: false, error: 'Please write your valid signature.' });
      });
    } else {
      setState({ ...state, isLoading: false });
    }
  }

  const fun_register_model = async () => {
    setState({ ...state, isModal: false });
    if (is_direct_sign) {
      props.navigation.navigate('AccountList');
    } else {
      await AsyncStorage.setItem('reg_user_id', 'reg_user_id');
      if (props.reg) {
        props.navigation.navigate('App')
      } else {
        props.setRegFourClose(false);
      }
    }
  }

  const slideUp1 = {
    transform: [
      {
        translateY: animation.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1],
          extrapolate: "extend",
        }),
      },
    ],
  };


  return (
    <>
      {isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}

        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal} >
        <View style={modal_style.modal}>
          <View style={modal_style.modal_wrapper}>
            <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => { fun_register_model(); }}>
              <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
            </TouchableOpacity>
            <Text style={modal_style.modal_heading}>Success</Text>
            <Text style={modal_style.modalText}>
              {is_direct_sign ? "Signature has been submitted successfully." : translate('registration_thank_you_message')}
            </Text>
            <View style={modal_style.modal_textbuttonWrap_center}>
              <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => { fun_register_model(); }}>
                <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Modal>

      {!show && <Animated.View style={[styles.Animated, (Platform.OS === 'ios' ? slideUp1 : slideUp1)]}>

        {is_direct_sign == false &&
          <View style={styles.title}>
            <Text style={styles.titleText}>{translate("apply_drive")}</Text>
          </View>}
        {is_direct_sign == false &&
          <View style={styles.wrap_images}>
            <Image width="20" style={styles.frame_image} source={require('../../assets/images/step4.png')} />
          </View>}

        <View style={styles.wrapper}>
          <View style={styles.Wrap}>
            <TouchableHighlight style={styles.LinkTextWrap} underlayColor='' onPress={() => showPDF()}>
              <Text style={styles.LinkText}>Click here</Text>
            </TouchableHighlight>
            <Text style={styles.LinkText1}>
              , to review the insurance form.
                    </Text>
          </View>
          <Text style={styles.Text}>Draw your signature below which will be used in this form.</Text>
          <View style={styles.borderView}>
            <SignatureCapture
              style={[styles.signature]}
              ref={inputEl}
              onSaveEvent={_onSaveEvent}
              onDragEvent={_onDragEvent}
              saveImageFileInExtStorage={false}
              showNativeButtons={false}
              showTitleLabel={false}
              showBorder={false}
              viewMode={"portrait"} />
          </View>
          <View style={styles.bottomView}>
            {error != '' &&
              <Text style={[styles.error_msg]}>{error}</Text>
            }
            <Text style={styles.Text1}>By tapping the 'ACCEPT SIGNATURE' button below, I agree that the signature above will be the legally binding electronic representation of my signature for this document.</Text>
            <View style={styles.buttonView}>
              <TouchableHighlight underlayColor='' style={styles.checkoutButton} onPress={() => { saveSign() }}>
                <Text style={styles.checkoutButtonText}>ACCEPT SIGNATURE</Text>
              </TouchableHighlight>
              <TouchableHighlight underlayColor='' style={styles.cancelButton} onPress={() => { resetSign() }}>
                <Text style={styles.cancelButtonText}>CLEAR SIGNATURE</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Animated.View>}
      {show && <InsurenceFormScreen hidePDF={hidePDF} />}
    </>
  );
}

const styles = StyleSheet.create({
  Animated: {
    flex: 8,
  },
  title: {
    flex: .08,
    flexDirection: "row",
    width: win.width,
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#9e9d9b",
  },
  titleText: {
    flex: 1,
    paddingLeft: 30,
    paddingBottom: 10,
    fontWeight: "700",
    fontSize: 20,
  },
  titleClose: {
    marginRight: 20,
    fontWeight: "bold",
    fontSize: 22,
  },
  close_text_icon: {
    color: "red",
    fontSize: 22,
    fontWeight: "bold"
  },
  wrapper: {
    flex: .92,
    paddingHorizontal: 25,
  },
  Wrap: {
    flexDirection: "row",
    marginTop: 10
  },
  LinkText1: {
    fontSize: 18,
  },
  LinkTextWrap: {
    flexDirection: "row-reverse",
  },
  LinkText: {
    fontSize: 18,
    textDecorationLine: 'underline',
  },
  wrap_images: {
    paddingTop: 10,
    marginVertical: 0,
    resizeMode: "contain",
    alignItems: "center"
  },
  frame_image: {
    maxWidth: 310,
    resizeMode: "contain"
  },
  Text: {
    marginTop: 10,
    fontSize: 18,
  },
  Text1: {
    marginTop: 10,
    fontSize: 14,
  },
  signature: {
    flex: 1,
  },
  borderView: {
    flex: .5,
    marginTop: 10,
    borderColor: '#000033',
    borderWidth: 1.2,
  },
  bottomView: {
    flex: .5,
  },
  buttonView: {
    marginTop: 10,
    flexDirection: "row-reverse",
  },
  checkoutButton: {
    backgroundColor: "#db9360",
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    color: "#db9360",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  error_msg: {
    marginTop: 10,
    marginLeft: 0,
    color: "red",
    fontSize: 14
  },
});