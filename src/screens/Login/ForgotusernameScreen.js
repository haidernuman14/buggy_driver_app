import React, { useState, useEffect } from 'react'; //Unused import function "useEffect"
import {
  StyleSheet,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  TouchableHighlight
} from 'react-native';
import { TextField, FilledTextField, OutlinedTextField } from 'react-native-material-textfield';//Unused import function "FilledTextField" and "OutlinedTextField"
import { global_styles, modal_style } from '../../constants/Style';
import Colors from '../../constants/Colors';
import { translate } from '../../components/Language';
import LinearGradient from 'react-native-linear-gradient';
import { useMutation } from '@apollo/react-hooks';
import { appRequestUsername } from '../../graphql/forgotUsername';
import { CheckConnectivity } from '../Helper/NetInfo';
import Modal from "react-native-modal";
import Icon2 from 'react-native-vector-icons/Ionicons';

/** This screen allows user to reset their username incase of forgotten */

//Initialisation of constants
export default function ForgotusernameScreen(props) {
  const [key, setKey] = useState(false); //Unused variable "setKey"
  const [state, setState] = useState({
    email: '',
    isModal: false,
    errors: {
      error_email: '',
      error_global: '',
      message_global: ''
    }
  })

  const [isLoading, setIsLoading] = useState(false);
  const { email, isModal, errors } = state;

  const [AforgotUsername] = useMutation(appRequestUsername); //Call GraphQL API to reset the username.


  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value.trim() });
  }
  //Function to check and call API asynchrnously to reset username
  const fun_forgot_username = async () => {

    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }

    const { email, phone } = state;
    let flag_val = 0;
    let error = { ...state, errors };
    error.error_global = '';
    error.message_global = '';

    const expressionEmail = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/; // email validation regex.
    //check email iput local validation
    if (email == '') {
      error.error_email = translate("error_email");
      setState({ ...state, errors: error });
      flag_val = 1;
    } else if (expressionEmail.test(email) === false) {
      error.error_email = translate("error_valid_email");
      setState({ ...state, errors: error });
      flag_val = 1;
    } else {
      error.error_email = "";
      setState({ ...state, errors: error });
    }

    if (flag_val == 1) {
      return;
    }

    let input = {
      email: email
    }
    setIsLoading(true);
    AforgotUsername({
      variables: { input }
    }).then((response) => {
      setIsLoading(false);
      if (response.data.requestUsername.ok) {
        error.message_global = translate("username_has_been_sent_successfully");
        setState({ ...state, isModal: true, errors: error });
      } else {
        error.error_email = translate("email_is_not_authenticated");
        setState({ ...state, errors: error });
      }
      return

    }).catch((error) => { // exceptional handling block
      setIsLoading(false);
      error.error_email = translate("email_is_not_authenticated");
      setState({ ...state, errors: error });
      return
    });
    return;
  }

  //Render the UI design of Forgot Password screen
  return (
    <>

      {isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}

      <ScrollView style={styles.container}>
        <KeyboardAvoidingView enabled style={{ flex: 1 }}>
          <View key={key} style={styles.info_main}>

            <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal}  >
              <View style={modal_style.modal}>
                <View style={modal_style.modal_wrapper}>
                  <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => { setState({ ...state, isModal: false }); }}>
                    <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                  </TouchableOpacity>
                  <Icon2 name="ios-checkmark-circle-outline" style={modal_style.modal_check} />
                  <View>
                    <Text style={modal_style.modalText}> {errors.message_global} </Text>
                    <View style={modal_style.modal_textbuttonWrap_center}>
                      <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => props.navigation.navigate("SignIn")}>
                        <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </View>
              </View>
            </Modal>

            <View style={styles.personal_detail_wrapper}>
              <Text style={[styles.personal_detail, global_styles.lato_regular]}>{translate("provide_your_registered_email")}</Text>
            </View>

            <View style={styles.profile_field} >
              <View style={styles.profile_field_value_wrapper} >
                <TextField
                  autoCapitalize='none'
                  lineWidth={1}
                  contentInset={{ top: 0, left: 0, right: 0, lable: 0, input: 8 }}
                  containerStyle={styles.input}
                  style={[styles.input, global_styles.lato_regular]}
                  placeholder={translate("Email")}
                  value={email}
                  onChangeText={(text) => updateTextInput(text, 'email')}
                  error={errors.error_email}
                />
              </View>
            </View>

            <View style={styles.loginButton}>
              <LinearGradient colors={[Colors.color0, Colors.color0]} start={{ x: 0.0, y: 0.0 }} end={{ x: 0, y: 0 }} style={{ flex: 1, borderRadius: 5 }}>
                <Text style={[styles.button, global_styles.lato_bold]} onPress={() => {
                  fun_forgot_username();
                }} >{translate("send")}</Text>
              </LinearGradient>
            </View>

          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </>
  );
}
//Stylesheet for forgotusername screen 
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  info_main: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    paddingVertical: 10,
    paddingBottom: 0,
  },
  error_global_text: { // Defined but not used please remove this.
    color: "red",
    fontSize: 16,
  },
  msg_global: {  // Defined but not used please remove this.
    paddingVertical: 10,
    marginTop: 20,
    alignItems: "center",
    borderColor: "#f1f1f1"
  },
  loginButton: {
    alignItems: "center",
    flexDirection: "row",
    width: 150,
    alignSelf: "center",
    marginTop: 12
  },
  button: {
    color: "#FFF",
    backgroundColor: Colors.color0,
    fontSize: 19,
    flex: 2,
    justifyContent: "center",
    textAlign: "center",
    paddingVertical: 8,
    fontWeight: "bold",
    borderRadius: 5
  },
  personal_detail_wrapper: {
    paddingVertical: '8%',
    paddingBottom: 10,
    marginHorizontal: 20,
  },
  personal_detail: {
    fontSize: 17,
  },
  profile_field: {
    marginVertical: 10,
    marginBottom: 0,
    marginHorizontal: 20,
  },
  input_cotainer: { // Defined but not used please remove this.
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 0,
    height: 40,
    color: "#FFF",
    backgroundColor: "#266272",
  },
  input: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    color: "#FFF",
    backgroundColor: "#f8f8f8"
  },
  profile_field_value_wrapper: {
    paddingHorizontal: 0,
    flexDirection: "row",
    marginBottom: 0,
  },
  input: {
    flex: 1,
    paddingHorizontal: 0,
  },
}); 