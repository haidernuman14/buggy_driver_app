import React, { Component } from 'react';
import {
  Animated, Dimensions, StyleSheet, Image, ActivityIndicator,
  TouchableHighlight, BackHandler, View, Text, Linking, Platform, TextInput,TouchableOpacity
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import { OutlinedTextField } from 'react-native-material-textfield';
import {compose, graphql, withApollo} from 'react-apollo';
import gql from 'graphql-tag';
import { driverCompleteRegister } from '../../graphql/driverCompleteRegisterExist';
import { global_styles,pickerSelectStyles } from '../../constants/Style';
import Colors from '../../constants/Colors';
import { translate } from '../../components/Language';
import { CheckConnectivity } from '../Helper/NetInfo';
import CalendarPicker from 'react-native-calendar-picker';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import moment from 'moment';
import { ScrollView } from 'react-native-gesture-handler';
import Ionicons from  'react-native-vector-icons/Ionicons';
import RNPickerSelect from 'react-native-picker-select';
import { zip } from 'lodash';
const states  = require("../../constants/statesCities.json");
const cities  = require("../../constants/cities.json");
import Modal from "react-native-modal";
import FontAwesome from  'react-native-vector-icons/FontAwesome';
const UpdateDriverProfile = gql`
mutation updateDriverMutation($input: UpdateDriverMutationInput!) {
  updateDriver(input:$input) {
    id
    firstName
    lastName
    nickName    
    email
    tlcLicense
    phone
    addressLine2
    streetAddress
    city
    state
    zipCode
    dmvLicense
    dateAdded
    driver {
      balance
    }
    success:ok
    ok
    errors {
      field
      messages
    }
  }
}`;

const win = Dimensions.get('window');
const baseHeight = 896;
const inputBoxVerticalPadding = win.height >= baseHeight ? 10 : 6;
 class RegsteptwoexistScreen extends Component {
 
  constructor(props) {
    super(props);
    const { user } = this.props
    let phone_val = user.phone;
    phone_val = phone_val.split("+1");
    if (phone_val.length == 1) {
      phone_val = user.phone;
    } else {
      phone_val = phone_val[1];
    }
    this.state = {
      isLoading: false,
      isRegThree: false,
      tlc_license: (user.tlcLicense) ? user.tlcLicense : '',
      calenderShow: false,
      calenderShow1: false,
      driverState:user.state,
      address_line_2:user.addressLine2,
      city:user.city,

      Cities:[],
      autoCompelete:false,
      tempvalue:[{value:"Haider",label:"haider2"}],
      id:user.id,
      zip:user.zipCode,
      userInfo:null,
      
      streetAddress:user.streetAddress,
      tlc_expiration_date: (user.tlcLicenseExpirationDate) ? moment(user.tlcLicenseExpirationDate).format('MM-DD-YYYY') : '',
      dmv_expiration_date: (user.dmvLicenseExpirationDate) ? moment(user.dmvLicenseExpirationDate).format('MM-DD-YYYY') : '',
      minDate: moment(),
      startDate: moment().format('YYYY-MM-DD'),
      first_name: (user.firstName && user.firstName != '') ? user.firstName : '',
      last_name: (user.lastName && user.lastName != '') ? user.lastName : '',
      userpass: '',
      dmv_license: (user.dmvLicense && user.dmvLicense != '') ? user.dmvLicense : '',
      email: (user.email != '') ? user.email : '',
      phone: (user.phone != '') ? phone_val : '',
      disable_flag: (user.tlcLicense == '' || user.dmvLicense == '') ? true : false,
      errors: {
        error_tlc_license: '',
        error_tlc_date: '',
        error_first_name: '',
        error_last_name: '',
        error_dmv_license: '',
        error_dmv_date: '',
        error_email: '',
        error_phone: '',
        error_global: ''
      },
      animation: new Animated.Value(0)
    }
      this.setUserDetail();
  }

  updateTextInput = (value, field) => {
    this.setState({ [field]: value.trim() });
  }
 radioSelectedState = (value, index) => {
 this.setState({driverState:value}); 
  this.getCities(value); 
}

getCities=(value)=>{
    let citiesArray = [];
     cities[value].map((item,index)=>{
      citiesArray.push({"label":item,"value":item})
     })
      this.setState({Cities:citiesArray})

}
radioSelectedCity=(value)=>{
  this.setState({city:value}); 
}

updateTextZip=(text)=>{
   this.setState({zip:text})
}
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton  = () => {
    return true;
  }


  verifyTlcLicense = async (tlcLicenseNumber) => {
    //Verify TLC License from the NYC Open Data Website. Returns a single array containing license info object
    let nycOpenDataUrl = "https://data.cityofnewyork.us/resource/xjfq-wh2d.json?license_number=" + tlcLicenseNumber;
    let appToken = "5WBDkAQLj244SKLuDJmXDVDhT";
    const res = await fetch(nycOpenDataUrl, {
      method: 'GET',
      data: {
        "$limit": 1,
        "$$app_token": appToken
      }
    });
    const data = await res.json();
    //console.log('TLC',data);
    //Returns an array
    if (data && data[0] && data[0].name) {
      let tlc_expiration_date = data[0].expiration_date;
      console.log('tlc_expiration_date', tlc_expiration_date)
      tlc_expiration_date = moment(tlc_expiration_date).format('MM-DD-YYYY');
      console.log(tlc_expiration_date)
      return tlc_expiration_date;
    }
    else {
      return false;
    }
  }

  getDataFromAutoComplete=(data,detail)=>{
    
    let postal_code = detail.address_components.filter(component => component.types.includes("postal_code"))[0];
    let city = detail.address_components.filter(component => component.types.includes("locality") || component.types.includes("sublocality_level_1"))[0];    
    let state = detail.address_components.filter(component => component.types.includes("administrative_area_level_1"))[0];
  
    if(city)
      this.state.Cities.push({"label":city.long_name,"value":city.long_name})
    if(state)
      this.setState({driverState:state.short_name})
    if(postal_code){
      this.setState({zip:postal_code.long_name})}

    this.setState({autoCompelete:false, streetAddress:data.description, city:city.long_name})      
  }
  fun_register_step_two = async () => {

    let netState = await CheckConnectivity();
    if (!netState) {
      this.props.screenProps.setNetModal(true)
      return;
    }
    this.props.reLogin();
    this.props.navigation.navigate('App');
    return;
  }

  updateTextInputAddress=(val)=>{
    this.setState({streetAddress:val})
  }
  updateCityText=(val)=>{
    this.setState({city:val})
  }
  setUserDetail = async () => {
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
      this.setState({userInfo:user})
   }
   updateAddressLine=(value,index)=>{
      this.setState({address_line_2:value})
   }   

  render() {
    let platformStyle = Platform.OS === 'ios' ? styles.iosStyle : styles.androidStyle;
    const { isLoading, isRegThree, disable_flag, calenderShow, calenderShow1, minDate, startDate, tlc_expiration_date, dmv_expiration_date, tlc_license, first_name, last_name, dmv_license, email, phone, errors,streetAddress,driverState,city,zip,address_line_2 } = this.state;
    const screenHeight = Dimensions.get("window").height;
    const slideUp = {
      transform: [
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolate: "extend",
          }),
        },
      ],
    };
    return (
      <>
        {!isRegThree &&
          <>
            {(isLoading) &&
              <View style={global_styles.activityIndicatorView}>
                <ActivityIndicator size="large" style={{ padding: 60 }} />
              </View>
            }

            <Animated.View style={[styles.Animated, slideUp]}>
              <KeyboardAwareScrollView>
                <View style={styles.title}>
                  <Text style={[styles.titleText, global_styles.lato_medium]}>Verify your details</Text>
                </View>
                <View style={styles.wrap_images}>
                </View>
                <View style={styles.wrapper}>
                  {/* <View style={{ paddingLeft: 10, flexDirection: "row" }}>
                    <Text>
                      <Text style={[styles.error_msg1, global_styles.lato_regular]}>If the information does not look correct, please contact customer support on the email: </Text>
                      <Text onPress={() => Linking.openURL("mailto:info@joinbuggy.com")} style={[global_styles.lato_regular, { color: "#0f6ad9", textDecorationLine: "underline" }]}>info@joinbuggy.com</Text>
                      <Text style={styles.error_msg1}>{"\n"}or call us: </Text>
                      <Text onPress={() => Linking.openURL("tel:+13473346313")} style={[global_styles.lato_regular, { color: "#0f6ad9", textDecorationLine: "underline" }]}>+13473346313</Text>
                    </Text>
                  </View> */}
                  <Text></Text>
                  {errors.error_global != '' &&
                    <View>
                      <Text style={styles.error_msg}>{errors.error_global}</Text>
                    </View>
                  }
                  <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginBottom:15}}> 
                    <Text style={[{color: 'rgba(0, 0, 0, 0.6)',}, global_styles.lato_medium]}>{"TLC# "+tlc_license}</Text>
                    <Text style={[{color: 'rgba(0, 0, 0, 0.6)',}, global_styles.lato_medium]}>{"DMV# "+dmv_license}</Text>
                    {/* <TextInput
                      editable={false}
                      style={[global_styles.textInput, global_styles.lato_regular]}
                      placeholder={translate("tlc_license")}
                      value={tlc_license}
                      onChangeText={(text) => this.updateTextInput(text, 'tlc_license')}
                    />
                    <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_tlc_license}</Text> */}
                  </View>
                  <View>
                    <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("first_name")}</Text>
                    <View style={[global_styles.textInput]}>
                    <TextInput
                      style={[global_styles.lato_regular],[global_styles.textinputContainer]}
                      placeholder={translate("place_holder_first_name")}
                      value={first_name}
                      onChangeText={(text) => this.updateTextInput(text, 'first_name')}
                    
                    />
                     <FontAwesome name="edit" style={{marginTop:8,fontSize:15,color:"grey"}}/>
                    </View>
                    <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_first_name}</Text>
                  </View>


                  <View>
                  
                    <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("last_name")}</Text>
                    <View style={[global_styles.textInput]}>
                      <TextInput
                      style={[global_styles.lato_regular],[global_styles.textinputContainer]}
                      placeholder={translate("place_holder_last_name")}
                      value={last_name}
                      onChangeText={(text) => this.updateTextInput(text, 'last_name')}
                    />
                    <FontAwesome name="edit" style={{marginTop:8,fontSize:15,color:"grey"}}/>
                    </View>
                    <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_last_name}</Text>
                  </View>
                  {/* <View>
                    <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("dmv_license_number")}</Text>
                    <TextInput
                      editable={false}
                      style={[global_styles.lato_regular]}
                      placeholder={translate("dmv_license")}
                      value={dmv_license}
                      onChangeText={(text) => this.updateTextInput(text, 'dmv_license')}
                    />
                    <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_dmv_license}</Text>
                  </View> */}
                  <View>
                  
                    <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("Email")}</Text>
               
                    <View style={[global_styles.textInput]}>
                    <TextInput
                      style={[global_styles.lato_regular],[global_styles.textinputContainer]}
                      placeholder={translate("place_holder_email_address")}
                      value={email}
                      onChangeText={(text) => this.updateTextInput(text, 'email')}
                    />
                     <FontAwesome name="edit" style={{marginTop:8,fontSize:15,color:"grey"}}/>
                     </View>
                    <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_email}</Text>
                  </View>
                  <View>
                 
                    <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("Phone")}</Text>
                   
                    <View style={[global_styles.textInput]}>
                    <TextInput
                      keyboardType="number-pad"
                      style={[global_styles.lato_regular],[global_styles.textinputContainer]}
                      placeholder={translate("place_holder_phone_number")}
                      value={phone}
                      onChangeText={(text) => this.updateTextInput(text, 'phone')}
                    />
                    <FontAwesome name="edit" style={{marginTop:8,fontSize:15,color:"grey"}}/>
                     </View>
                    <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_phone}</Text>
                  </View>

                  <View>
            
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("address")}</Text>
                
                
                 <TouchableOpacity onPress={()=>this.setState({autoCompelete:true})}>
                 <View style={[global_styles.textInput]}>
                <TextInput
                  style={[global_styles.lato_regular],[global_styles.textinputContainer]}
                  placeholder={translate("place_holder_address")}
                  value={this.state.streetAddress}
                  editable={false}
                  onChangeText={(text) => {this.updateTextInputAddress(text)}}
                />
                <FontAwesome name="edit" style={{marginTop:8,fontSize:15,color:"grey"}}/>
                </View>
                </TouchableOpacity>
                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_address}</Text>
              </View>
              <View>
                <View>
                
              <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("address_line_2")} (Apt, Suite, etc.)</Text>
              
              
              <View style={[global_styles.textInput]}>
                <TextInput
                  style={[global_styles.lato_regular],[global_styles.textinputContainer]}
                  placeholder={translate("place_holder_address_line_2")}
                  value={this.state.address_line_2}
                  onChangeText={(text) =>  this.updateAddressLine(text, 'address_line_2')}
                />
                 <FontAwesome name="edit" style={{marginTop:8,fontSize:15,color:"grey"}}/>
                 </View>
                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_address_line2}</Text>
              </View>

              <View>
               
              <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("zipcode")}</Text>
              <View style={[global_styles.textInput]}>
              <TextInput
                    style={[global_styles.lato_regular],[global_styles.textinputContainer]}
                    placeholder={translate("place_holder_zip_code")}
                    value={this.state.zip}
                    onChangeText={(text) => this.updateTextZip(text)}
                  />
                  <FontAwesome name="edit" style={{marginTop:8,fontSize:15,color:"grey"}}/>
                  </View>
               <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_zip}</Text>
              </View>
               
                
                  
              {/* <View style={styles.split_container}> */}
                {/* <View style={styles.half_container_left}>
                  <View>
                    <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("state")}</Text>
                    <View style={[platformStyle]}>
                      <RNPickerSelect
                        placeholder={{}}
                        onValueChange={(value, index) => this.radioSelectedState(value)}
                        mode="dropdown"
                        value={driverState}
                        defaultValue={driverState}
                        items={statelist}
                        style={pickerSelectStyles}
                        useNativeAndroidPickerStyle={false}
                        Icon={() => {
                          return (
                            <View
                              style={pickerSelectStyles.icon}
                            />
                          );
                        }}
                      />
                    </View>
                  </View>
                  </View> */}
                  {/* <View style={styles.half_container_right}>
                  <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("zipcode")}</Text>
                  <TextInput
                    style={[{ marginTop: 4,
                      borderWidth: 1,
                      borderRadius: 4,
                      fontSize: 14,
                      borderColor: '#d4d4d4',
                      color: 'rgba(0, 0, 0, 0.6)',
                      width: "100%",
                      backgroundColor: '#ffffff',
                      padding:10,
                    }, global_styles.lato_regular]}
                    placeholder={translate("place_holder_zip_code")}
                    value={this.state.zip}
                    onChangeText={(text) => this.updateTextZip(text)}
                  />
                  {/* <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_zip}</Text> */}
                {/* </View> */} 
                  {/* </View> */}
                   {/* <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("city")}</Text>
                <View style={{marginTop:4}}>
                <RNPickerSelect
                        placeholder={{}}
                        onValueChange={(value, index) => this.radioSelectedCity(value)}
                        mode="dropdown"
                        value={this.state.city}
                        defaultValue={this.state.city}
                        items={this.state.Cities}
                        style={pickerSelectStyles}
                        useNativeAndroidPickerStyle={false}
                        Icon={() => {
                          return (
                            <View
                              style={pickerSelectStyles.icon}
                            />
                          );
                        }}
                      />
                      </View>
                {/* <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_city}</Text> */}
              </View>
                </View> 
                <View style={styles.textbuttonWrap}>
                  <TouchableHighlight disabled={disable_flag} onPress={() =>  this._updateUserData()}>
                    <View style={global_styles.bottom_style_orange_wrapper}>
                      <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate('welcome')}</Text>
                    </View>
                  </TouchableHighlight>
                </View>
                
              </KeyboardAwareScrollView>
              <Modal backdropOpacity={0.5} backdropColor={"#c4c4c4"} isVisible={this.state.autoCompelete}>
              <View style={{ flex: 1,backgroundColor: '#ecf0f1'}}>
                <View style={{flexDirection:"row",alignItems:"center",backgroundColor:"white",height:"7%",alignItems:"center",}}>
                <TouchableOpacity onPress={()=>{this.setState({autoCompelete:false})}} >
                  <Ionicons name="arrow-back" style={{alignItems:"center",fontSize:24,marginRight:10,marginLeft:10}}/>

                </TouchableOpacity>
                <Text style={{fontSize:16,textAlign:"center",justifyContent:"center",alignItems:"center",width:"75%"}}>Address</Text>
                </View>
                <View style={{padding: 10,fontWeight:"bold",flex:1}}>
              <GooglePlacesAutocomplete
         placeholder={this.state.streetAddress}
         fetchDetails= {true}
       
         query={{
          key: "AIzaSyBbOdZTwL_yX5P0kTNHgBx0yv_v5nQW4Ak",
          components: "country:us",
          language: 'us',
          type:'address',
          types: '(cities)',
          
           // language of the results
        }}
        
        onPress={(data, details = null) => {this.getDataFromAutoComplete(data, details)}}
        onFail={(error) => console.error(error)}
        requestUrl={{
          url:
            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
          useOnPlatform: 'web',
        }} // this in only required for use on the web. See https://git.io/JflFv more for details.
      />
      </View>
    
      </View>
                </Modal>
              
            </Animated.View>
          </>
        }
      </>
    );
  }
  _updateUserData = async () => {
  
    let userInfo = this.state.userInfo;
    let input = {
      id:this.state.id,
      streetAddress: this.state.streetAddress,
      addressLine2:this.state.address_line_2,
      city: this.state.city,
      state: this.state.driverState,
      zipCode:this.state.zip,
      phone:"+1"+ this.state.phone,
      firstName: this.state.first_name,
      lastName: this.state.last_name,
      email: this.state.email,
    } 
       console.log(input)
   const data  = await this.props.UpdateDriverProfile({
      variables: {input}
    }).then((response) => {
      if (response.data.updateDriver.ok) {
          console.log("here i am"+response.data)
        let user_info = response.data.updateDriver;
          console.log("not"+user_info)
        userInfo.streetAddress = user_info.streetAddress;
        userInfo.addressLine2 = user_info.addressLine2;
        userInfo.city = user_info.city;
        userInfo.state = user_info.state;
        userInfo.zipCode = user_info.zipCode;
        userInfo.phone = user_info.phone;
        userInfo.email = user_info.email;
        userInfo.firstName = user_info.firstName;
        userInfo.lastName = user_info.lastName;
        AsyncStorage.setItem('user', JSON.stringify(userInfo));
        console.log("here user"+user_info)
          this.fun_register_step_two();
      }
      
    }).catch(error=>{
       console.log("error"+error)
    })
     if(data==undefined){
      this.fun_register_step_two();
     }
  }

 }

 

const styles = StyleSheet.create({
  Animated: {
    flex: 20,
  },
  info_main: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: 17,
    paddingVertical: 0,
  },
  close_text_icon: {
    color: "red",
    fontSize: 22,
    fontWeight: "bold"
  },
  wrap_images: {
    paddingTop: 10,
    marginVertical: 0,
    resizeMode: "contain",
    alignItems: "center",
  },
  frame_image: {
    maxWidth: 320,
    resizeMode: "contain"
  },
  title: {
    flex: 1,
    flexDirection: "row",
    width: win.width,
    marginVertical: 20,
    marginBottom: 10,
    alignContent: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#9e9d9b",
  },
  titleText: {
    flex: 1,
    paddingLeft: 115,
    paddingBottom: 15,
    fontSize: 18,
    width:"100%",
    alignItems:"center"
  },
  titleClose: {
    marginRight: 20,
    marginTop: -0,
    fontWeight: "bold",
    fontSize: 22,
  },
  split_container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputWrap: {
    flexDirection: "row",
    marginBottom: 4,
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },
  WrapBorder: {
    flexDirection: "row",
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  textbuttonWrap: {
    alignItems: "flex-end",
    flex: 1,
    margin: 16,
  },
  textbuttonRigthView: {
    alignItems: "flex-end",
    flex: 1,
  },
  textbuttonLeftView: {
    flex: 1,
    width: 20,
    backgroundColor: 'transparent',
  },
  upload_document_disable: {
    backgroundColor: "rgba(253, 141, 57,.5)",
    paddingHorizontal: 12,
    borderRadius: 6,
    paddingVertical: 10,
    marginRight: 7,
  },
  upload_document: {
    backgroundColor: Colors.color0,
    paddingHorizontal: 12,
    borderRadius: 6,
    paddingVertical: 10,
    marginRight: 7,
  },
  upload_document_text: {
    color: "#FFFFFF",
    fontWeight: "bold",
    fontSize: 16,
  },
  iosStyle: {
    marginLeft: 0,
    marginTop: -8,
    paddingVertical: 12
  },
  androidStyle: {
    marginLeft: -10,
    marginTop: -13,
    paddingVertical: 12,
    paddingHorizontal: 10,
  },
  textbuttonRightView: {
    flex: 1,
    width: 20,
    backgroundColor: 'transparent',
  },
  buttonText: {
    fontSize: 18,
    height: 30,
    textDecorationLine: 'underline',
    color: "blue",
  },
  calenderView: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    marginBottom: 12,
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "rgba(0, 0, 0, .38)",
    borderRadius: 5
  },
  calenderViewError: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    marginBottom: 12,
    flexDirection: "row",
    borderWidth: 2,
    borderColor: "rgb(213, 0, 0)",
    borderRadius: 5
  },
  calenderText: {
    flex: 1,
    fontSize: 16,
    color: "rgba(0, 0, 0, .38)",
    paddingVertical: 8,
  },
  calenderTextError: {
    flex: 1,
    fontSize: 16,
    color: "rgb(213, 0, 0)",
    paddingVertical: 8,
  },
  calenderDateView: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 8,
  },
  calenderDate: {
    flex: 1,
  },
  calenderDateText: {
    fontSize: 16,
  },
  calenderIcon: {
    flexDirection: "row-reverse"
  },
  calenderShow: {
    zIndex: 10,
    display: "flex"
  },
  calenderHide: {
    display: "none"
  },
  error_msg: {
    marginTop: 0,
    marginLeft: 0,
    color: "rgb(213, 0, 0)",
    fontSize: 16,
    marginBottom: 14
  },
  error_msg1: {
    color: "rgb(213, 0, 0)",
    fontSize: 16,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    height: 250,
    borderRadius: 15,
    borderWidth: 1,
  },
  modalTitle: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 22
  },
  modalText: {
    marginVertical: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  modalOk: {
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  modalOkText: {
    width: 150,
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: "#000",
    color: '#FFF',
    borderColor: "#FFF",
    borderWidth: 1,
  },
  error_label: {
    color: 'red',
    fontSize: 12,
  },
  half_container_left: {
    flex: 1,
    marginTop:20,
  },
  half_container_right: {
    flex: 1,
    marginStart: 16,
    marginTop:5,
    padding:0
  }
});


export default graphql(UpdateDriverProfile, {name: 'UpdateDriverProfile'})(RegsteptwoexistScreen)