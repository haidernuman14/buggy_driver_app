import React, { Component } from 'react';
import {
  Animated, Dimensions, StyleSheet, Image, ActivityIndicator,
  TouchableHighlight, View, Text, Platform, TextInput, BackHandler
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { OutlinedTextField } from 'react-native-material-textfield';
import AsyncStorage from '@react-native-community/async-storage';
import RegstepthreeScreen from './RegstepthreeScreen';
import { Mutation } from 'react-apollo';
import client from 'graphql';
import { driverCompleteRegister } from '../../graphql/driverCompleteRegister';
import { global_styles } from '../../constants/Style';
import Colors from '../../constants/Colors';
import { translate } from '../../components/Language';
import { CheckConnectivity } from '../Helper/NetInfo';
import CalendarPicker from 'react-native-calendar-picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import { driverUploadDocument } from '../../graphql/driverUploadDocument';

// Get screen dimensions
const win = Dimensions.get('window');

/**
 * This class confirms and collects License numbers, Names, Email id, Phone numbers
 * from the user.
 */
export default class RegsteptwoScreen extends Component {
  constructor(props) {
    super(props);
    const { FirstName, LastName, tlcLicense, dmvLicense, tlc_date, dmv_date } = this.props
    console.log('dmvLicense', dmvLicense)
    let email_mobile = (this.props.email_mobile) ? this.props.email_mobile : '';
    let tlc = (tlcLicense && tlcLicense != '') ? tlcLicense : '';
    if (email_mobile != '' && !isNaN(email_mobile) && (email_mobile.length >= 6 && email_mobile.length <= 8)) {
      tlc = (tlc == '') ? email_mobile : tlc;
      email_mobile = '';
    }
    this.state = {
      isLoading: false,
      isRegThree: false,
      user_id: '',
      tlc_license: tlc,
      calenderShow: false,
      calenderShow1: false,
      tlc_expiration_date: (tlc_date) ? moment(tlc_date).format('MM-DD-YYYY') : '',
      dmv_expiration_date: (dmv_date) ? ((moment(dmv_date, 'D-M-YYYY').isValid()) ? moment(dmv_date, 'D-M-YYYY').format('MM-DD-YYYY') : '') : '',
      minDate: moment(),
      startDate: moment().format('YYYY-MM-DD'),
      first_name: (FirstName && FirstName != '') ? FirstName : '',
      last_name: (LastName && LastName != '') ? LastName : '',
      userpass: '',
      dmv_license: (dmvLicense && dmvLicense != '') ? dmvLicense : '',
      email: (email_mobile != '' && isNaN(email_mobile)) ? email_mobile : '',
      phone: (email_mobile != '' && isNaN(email_mobile)) ? '' : email_mobile,
      errors: {
        error_tlc_license: '',
        error_tlc_date: '',
        error_first_name: '',
        error_last_name: '',
        error_dmv_license: '',
        error_dmv_date: '',
        error_email: '',
        error_phone: '',
        error_global: ''
      },
      object_upload_document_send: false,
      animation: new Animated.Value(0)
    }

    
  } 

  handleBackButton  = () => {
    return true;
  }

  componentDidMount() {
    this.details();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  updateTextInput = (value, field) => {
    this.setState({ [field]: value.trim() });
  }

  // Get user's id
  details = async () => {
    const user_id = await AsyncStorage.getItem('reg_user_id');
    this.setState({ user_id: user_id });
  }

  //Verify TLC License from the NYC Open Data Website. Returns a single array containing license info object
  verifyTlcLicense = async (tlcLicenseNumber) => {
    //Verify TLC License from the NYC Open Data Website. Returns a single array containing license info object
    let nycOpenDataUrl = "https://data.cityofnewyork.us/resource/xjfq-wh2d.json?license_number=" + tlcLicenseNumber;
    let appToken = "5WBDkAQLj244SKLuDJmXDVDhT";
    const res = await fetch(nycOpenDataUrl, {
      method: 'GET',
      data: {
        "$limit": 1,
        "$$app_token": appToken
      }
    });
    const data = await res.json();
    //console.log('TLC',data);
    //Returns an array
    if (data && data[0] && data[0].name) {
      let tlc_expiration_date = data[0].expiration_date;
      console.log('tlc_expiration_date', tlc_expiration_date)
      tlc_expiration_date = moment(tlc_expiration_date).format('MM-DD-YYYY');
      console.log(tlc_expiration_date)
      return tlc_expiration_date;
    }
    else {
      return false;
    }
  }

  // Validate registration data like TLC and DMV license numbers then do registration
  fun_register_step_two = async (send, upload_document_send) => {
    //his.setState({ isRegThree: true });
    //return;

    let netState = await CheckConnectivity();
    if (!netState) {
      this.props.screenProps.setNetModal(true)
      return;
    }
    const { user_id, tlc_license, first_name, last_name, dmv_license, email, phone, tlc_expiration_date, dmv_expiration_date } = this.state;
    this.setState({ isLoading: true });
    let flag_val = 0;
    let tlc_flag = '';
    let error = { ...this.state.errors };
    if (tlc_license == '') {
      error.error_tlc_license = translate("error_tlc_license_text");
      flag_val = 1;
    } else if (isNaN(tlc_license)) {
      error.error_tlc_license = translate("valid_tlc_number");
      flag_val = 1;
    } else if (tlc_license.length >= 6 && tlc_license.length <= 8) {
      tlc_flag = await this.verifyTlcLicense(tlc_license);
      if (!tlc_flag) {
        error.error_tlc_license = translate("valid_tlc_number");
        flag_val = 1;
      } else {
        error.error_tlc_license = "";
      }
    } else {
      error.error_tlc_license = translate("valid_tlc_number");
      flag_val = 1;
    }

    if (first_name == '') {
      error.error_first_name = translate("error_first_name");
      flag_val = 1;
    } else {
      error.error_first_name = "";
    }

    if (last_name == '') {
      error.error_last_name = translate("error_last_name");
      flag_val = 1;
    } else {
      error.error_last_name = "";
    }

    if (dmv_license == '') {
      error.error_dmv_license = translate("error_dmv_license_text");
      flag_val = 1;
    } else if (isNaN(dmv_license)) {
      error.error_dmv_license = translate("valid_dmv_number");
      flag_val = 1;
    } else if (!isNaN(dmv_license) && dmv_license.length > 10) {
      error.error_dmv_license = translate("valid_dmv_number");
      flag_val = 1;
    } else {
      error.error_dmv_license = "";
    }
    const expressionEmail = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    if (email == '') {
      error.error_email = translate("error_email");
      flag_val = 1;
    } else if (expressionEmail.test(email) === false) {
      error.error_email = translate("error_valid_email");
      flag_val = 1;
    } else {
      error.error_email = "";
    }
    const expressioPhone = /^(?:\d{10})$/;
    if (phone == '') {
      error.error_phone = translate("error_phone");
      flag_val = 1;
    } else if (expressioPhone.test(phone) === false) {
      error.error_phone = translate("error_valid_phone");
      flag_val = 1;
    } else {
      error.error_phone = "";
    }

    if (dmv_expiration_date == '') {
      error.error_dmv_date = translate("error_dmv_expiration_date");
      flag_val = 1;
    } else {
      error.error_dmv_date = "";
    }

    if (flag_val == 1) {
      this.setState({ isLoading: false, errors: error });
      return;
    } else {
      this.setState({ errors: error });
    }
    const { tlcLicense, dmvLicense, TlcLicense, DmvLicense } = this.props
    let dmvLicensePic = '';
    let tlcLicensePic = '';
    console.log('tlcLicenseExpirationDate:' + tlc_flag,
      'dmvLicenseExpirationDate:' + dmv_expiration_date)
    console.log("Reg request info", {
      firstName: first_name,
      lastName: last_name,
      tlcLicense: tlc_license,
      dmvLicense: dmv_license,
      email: email,
      phone: '+1' + phone,
      dmvLicensePic: dmvLicensePic,
      tlcLicensePic: tlcLicensePic,
      tlcLicenseExpirationDate: moment(tlc_flag, 'MM-DD-YYYY').format('YYYY-MM-DD'),
      dmvLicenseExpirationDate: moment(dmv_expiration_date, 'MM-DD-YYYY').format('YYYY-MM-DD')
    });
    send({
      variables: {
        firstName: first_name,
        lastName: last_name,
        tlcLicense: tlc_license,
        dmvLicense: dmv_license,
        email: email,
        phone: '+1' + phone,
        dmvLicensePic: dmvLicensePic,
        tlcLicensePic: tlcLicensePic,
        tlcLicenseExpirationDate: moment(tlc_flag, 'MM-DD-YYYY').format('YYYY-MM-DD'),
        dmvLicenseExpirationDate: moment(dmv_expiration_date, 'MM-DD-YYYY').format('YYYY-MM-DD')
      }
    });

    this.setState({ object_upload_document_send: upload_document_send });

    //this.upload_dmv_document(upload_document_send);
    //this.upload_tlc_document(upload_document_send);

    return;
  }

  // TLC image upload
  upload_dmv_document = async () => {
    let input = await AsyncStorage.getItem('TLC_Image');
    if (input) {
      input = JSON.parse(input);
      console.log("TLC_Image>>>>>>>>", input);
      this.state.object_upload_document_send({ variables: { input } });
    }
  }

  // DMV image upload
  upload_tlc_document = async () => {
    let input = await AsyncStorage.getItem('DMV_Image');
    if (input) {
      input = JSON.parse(input);
      console.log("DMV_Image>>>>>>>>", input);
      this.state.object_upload_document_send({ variables: { input } });
    }
  }

  success_tlc_upload = async (response) => {
    console.log("Upload document response data::::::>>>", response);
  }

  save_data = async (response) => {
    console.log("register response data, ", response, response.completeDriverProfile.errors);
    if (response.completeDriverProfile.ok) {

      this.upload_dmv_document();
      this.upload_tlc_document();

      const { tlc_license, phone, email, tlc_expiration_date, dmv_expiration_date } = this.state;

      let username = "";

      if (phone != "")
        username = phone;

      if (email != "")
        username = email;

      if (tlc_license != "")
        username = tlc_license;

      await AsyncStorage.setItem('java_name', username);
      await AsyncStorage.setItem('reg_user_id', 'reg_user_id');

      this.props.reLogin();
      if (!this.props.existFlag) {
        this.setState({ isRegThree: true });
      } else {
        this.props.setRegTwoClose(false);
      }
      return;
    } else if (response.completeDriverProfile.errors) {
      console.log('err_msg')
      let error = { ...this.state.errors };
      error.error_global = response.completeDriverProfile.errors[0].messages;
      this.setState({ isLoading: false, errors: error });
      return
    }
    else {
      let error = { ...this.state.errors };
      error.error_global = translate("Please enter all details");
      this.setState({ isLoading: false, errors: error });
      return
    }
  }

  setRegThree = (value) => {
    this.setState({ isRegThree: value });
  }

  setRegThreeClose = (value) => {
    this.setState({ isRegThree: value });
    this.props.setRegTwoClose(false);
  }

  openCalender = () => {
    this.setState({ calenderShow: !this.state.calenderShow })
  }

  onDateChange = (date) => {
    let setDate = moment(date).format('YYYY-MM-DD');
    let setDisplayDate = moment(date).format('MM-DD-YYYY');
    //console.log(setDate)
    this.setState({ dmv_expiration_date: setDisplayDate, startDate: setDate, calenderShow: false })
  }

  render() {
    const { tlc_date, dmv_date, existFlag } = this.props;
    const { isLoading, isRegThree, calenderShow, calenderShow1, minDate, startDate, tlc_expiration_date, dmv_expiration_date, tlc_license, first_name, last_name, dmv_license, email, phone, errors } = this.state;
    const screenHeight = Dimensions.get("window").height;
    const slideUp = {
      transform: [
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolate: "extend",
          }),
        },
      ],
    };
    return (
      <>
        {!isRegThree && <Mutation
          client={client}
          mutation={driverCompleteRegister}
          onCompleted={(data) => this.save_data(data)}>
          {(AdriverCompleteRegister, { loading, error }) => (
            <>
              {(isLoading || loading) &&
                <View style={global_styles.activityIndicatorView}>
                  <ActivityIndicator size="large" style={{ padding: 60 }} />
                </View>
              }

              <Mutation
                client={client}
                mutation={driverUploadDocument}
                onCompleted={(data) => this.success_tlc_upload(data)}>
                {(AdriverUploadDocument, { loading1, error1 }) => (<>

                  <Animated.View style={[styles.Animated, slideUp]}>
                    <KeyboardAwareScrollView>
                      <View style={styles.title}>
                        <Text style={[styles.titleText, global_styles.lato_medium]}>{translate("apply_drive")}</Text>
                        {/*!existFlag && <TouchableHighlight underlayColor="" onPress={() => { this.props.setRegTwoClose(false); }}><Text style={styles.titleClose}>
                      <Icon2 style={styles.close_text_icon} name='close' />
                    </Text>
                    </TouchableHighlight>*/}
                      </View>
                      <View style={styles.wrap_images}>
                        {!existFlag && <Image width="20" style={styles.frame_image} source={require('../../assets/images/step2.png')} />}
                      </View>
                      <View style={styles.wrapper}>
                        {error &&
                          <View>
                            <Text style={styles.error_msg}>{error.message}</Text>
                          </View>
                        }
                        {errors.error_global != '' &&
                          <View>
                            <Text style={styles.error_msg}>{errors.error_global}</Text>
                          </View>
                        }
                        <View style={{ marginTop: 8, }}>
                          <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("tlc_license_number")}</Text>
                          <TextInput
                            style={[global_styles.textInput, global_styles.lato_regular]}
                            placeholder={translate("tlc_license")}
                            value={tlc_license}
                            onChangeText={(text) => this.updateTextInput(text, 'tlc_license')}
                          />
                          <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_tlc_license}</Text>
                        </View>
                        <View>
                          <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("first_name")}</Text>
                          <TextInput
                            style={[global_styles.textInput, global_styles.lato_regular]}
                            placeholder={translate("place_holder_first_name")}
                            value={first_name}
                            onChangeText={(text) => this.updateTextInput(text, 'first_name')}
                          />
                          <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_first_name}</Text>
                        </View>
                        <View>
                          <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("last_name")}</Text>
                          <TextInput
                            style={[global_styles.textInput, global_styles.lato_regular]}
                            placeholder={translate("place_holder_last_name")}
                            value={last_name}
                            onChangeText={(text) => this.updateTextInput(text, 'last_name')}
                          />
                          <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_last_name}</Text>
                        </View>
                        <View>
                          <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("dmv_license_number")}</Text>
                          <TextInput
                            style={[global_styles.textInput, global_styles.lato_regular]}
                            placeholder={translate("dmv_license")}
                            value={dmv_license}
                            onChangeText={(text) => this.updateTextInput(text, 'dmv_license')}
                          />
                          <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_dmv_license}</Text>
                        </View>
                        <View>
                          <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("dmv_exp_date")}</Text>
                          <View style={styles.calenderView}>
                            <Text style={[styles.calenderText, global_styles.lato_regular]}>{translate('place_holder_dmv_exp')}</Text>
                            <View style={styles.calenderDateView}>
                              <TouchableHighlight underlayColor='' onPress={() => (dmv_date) ? '' : this.openCalender()} style={styles.calenderDate}><Text style={styles.calenderDateText}>{dmv_expiration_date}</Text></TouchableHighlight>
                              <TouchableHighlight underlayColor='' onPress={() => (dmv_date) ? '' : this.openCalender()} style={styles.calenderIcon}><Icon color={"#db9360"} size={20} name="calendar" /></TouchableHighlight>
                            </View>
                          </View>
                          {errors.error_dmv_date != '' &&
                            <View>
                              <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_dmv_date}</Text>
                            </View>
                          }
                          <View style={{ marginBottom: 12 }} />
                          <View style={calenderShow ? styles.calenderShow : styles.calenderHide}>
                            <CalendarPicker
                              minDate={minDate}
                              selectedStartDate={startDate}
                              onDateChange={(value) => this.onDateChange(value)}
                            />
                          </View>
                        </View>
                        <View>
                          <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("Email")}</Text>
                          <TextInput
                            style={[global_styles.textInput, global_styles.lato_regular]}
                            placeholder={translate("place_holder_email_address")}
                            value={email}
                            onChangeText={(text) => this.updateTextInput(text, 'email')}
                          />
                          <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_email}</Text>
                        </View>
                        <View>
                          <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("Phone")}</Text>
                          <TextInput
                            style={[global_styles.textInput, global_styles.lato_regular]}
                            value={phone}
                            placeholder={"6461234567"}
                            keyboardType="number-pad"
                            onChangeText={(text) => this.updateTextInput(text, 'phone')}
                          />
                          <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_phone}</Text>
                        </View>
                      </View>
                      <View style={styles.textbuttonWrap}>
                        <TouchableHighlight underlayColor="" onPress={() => { this.fun_register_step_two(AdriverCompleteRegister, AdriverUploadDocument); }}>
                          <View style={global_styles.bottom_style_orange_wrapper}>
                            <Text style={styles.upload_document_text}>{translate('next')}</Text>
                          </View>
                        </TouchableHighlight>
                      </View>
                    </KeyboardAwareScrollView>
                  </Animated.View>

                </>)}
              </Mutation>

            </>)}
        </Mutation>}
        {isRegThree && <RegstepthreeScreen {...this.props} setModalSuccess={this.props.setModalSuccess} setRegThree={this.setRegThree} setRegThreeClose={this.setRegThreeClose} />}
      </>
    );
  }
}

// Styesheet to design Registration step two screen
const styles = StyleSheet.create({
  Animated: {
    flex: 8,
  },
  info_main: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: 17,
    paddingVertical: 0,
  },
  close_text_icon: {
    color: "red",
    fontSize: 22,
    fontWeight: "bold"
  },
  wrap_images: {
    paddingTop: 10,
    marginVertical: 0,
    resizeMode: "contain",
    alignItems: "center",
  },
  frame_image: {
    maxWidth: 320,
    resizeMode: "contain"
  },
  title: {
    flex: 1,
    flexDirection: "row",
    width: win.width,
    marginVertical: 20,
    marginBottom: 10,
    alignContent: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#9e9d9b",
  },
  titleText: {
    flex: 1,
    paddingLeft: 30,
    paddingBottom: 15,
    fontWeight: "700",
    fontSize: 20,
  },
  titleClose: {
    marginRight: 20,
    marginTop: -0,
    fontWeight: "bold",
    fontSize: 22,
  },
  inputWrap: {
    flexDirection: "row",
    marginBottom: 4,
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },
  WrapBorder: {
    flexDirection: "row",
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  textbuttonWrap: {
    alignItems: "flex-end",
    flex: 1,
    margin: 16,
  },
  textbuttonRigthView: {
    alignItems: "flex-end",
    flex: 1,
  },
  textbuttonLeftView: {
    flex: 1,
    width: 20,
    backgroundColor: 'transparent',
  },
  upload_document: {
    backgroundColor: "#db9360",
    paddingHorizontal: 12,
    borderRadius: 6,
    paddingVertical: 10,
    marginRight: 7,
  },
  upload_document_text: {
    color: "#FFFFFF",
    fontWeight: "bold",
    fontSize: 16,
  },
  textbuttonRightView: {
    flex: 1,
    width: 20,
    backgroundColor: 'transparent',
  },
  buttonText: {
    fontSize: 18,
    height: 30,
    textDecorationLine: 'underline',
    color: "blue",
  },
  calenderView: {
    paddingVertical: 2,
    paddingHorizontal: 10,
    marginTop: 2,
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "#d4d4d4",
    borderRadius: 5
  },
  calenderViewError: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    marginBottom: 12,
    flexDirection: "row",
    borderWidth: 2,
    borderColor: "rgb(213, 0, 0)",
    borderRadius: 5
  },
  calenderText: {
    flex: 1,
    fontSize: 14,
    paddingVertical: 8,
    color: 'rgba(0, 0, 0, 0.4)',
  },
  calenderTextError: {
    flex: 1,
    fontSize: 16,
    color: "rgb(213, 0, 0)",
    paddingVertical: 8,
  },
  calenderDateView: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 8,
  },
  calenderDate: {
    flex: 1,
  },
  calenderDateText: {
    fontSize: 16,
  },
  calenderIcon: {
    flexDirection: "row-reverse"
  },
  calenderShow: {
    zIndex: 10,
    display: "flex"
  },
  calenderHide: {
    display: "none"
  },
  error_msg: {
    marginTop: 0,
    marginLeft: 0,
    color: "rgb(213, 0, 0)",
    fontSize: 16,
    marginBottom: 14
  },
  error_msg1: {
    marginTop: -10,
    marginLeft: 20,
    color: "rgb(213, 0, 0)",
    fontSize: 12,
    marginBottom: 8
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    height: 250,
    borderRadius: 15,
    borderWidth: 1,
  },
  modalTitle: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 22
  },
  modalText: {
    marginVertical: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  modalOk: {
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  modalOkText: {
    width: 150,
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: "#000",
    color: '#FFF',
    borderColor: "#FFF",
    borderWidth: 1,
  },
  error_label: {
    color: 'red',
    fontSize: 12,
    marginBottom: 4,
  },
});
