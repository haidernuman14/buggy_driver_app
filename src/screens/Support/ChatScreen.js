import React, { useState, useEffect, useContext } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    Image,
    ActivityIndicator,
    View,
    Keyboard,
    Dimensions,
    Platform,
    TouchableOpacity,
    SafeAreaView,
    FlatList,
    NativeSyntheticEvent,
    NativeScrollEvent,
    KeyboardAvoidingView
} from 'react-native';
import { useMutation, useQuery } from '@apollo/react-hooks';
import AsyncStorage from '@react-native-community/async-storage';
import Slack from 'react-native-slack-webhook';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import RNPickerSelect from 'react-native-picker-select';
import { TextInput } from 'react-native-gesture-handler';
import RNPusherPushNotifications from 'react-native-pusher-push-notifications';
import { getPusherToken } from '../../graphql/getPusherToken';
import { compose, graphql, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment';
import { Header } from 'react-navigation-stack';
import { set } from 'react-native-reanimated';
import { getAllDriverMessages } from '../../graphql/receiveAllMessages';
import { createDriverMessage } from '../../graphql/createMessage';
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { stat } from 'react-native-fs';
const webhookURL = 'https://hooks.slack.com/services/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';
const win = Dimensions.get('window');

const baseHeight = 896;
const chatBoxPadding = win.height >= baseHeight ? 12 : Platform.OS === 'ios' ? 8 : 0;
const chatBoxBottomMargin = win.height >= baseHeight ? 16 : 8;
const chatTextSize = win.height >= baseHeight ? 16 : 14;
const bottomIconsMargin = win.height >= baseHeight ? 16 : 0;
const messageBoxPadding = win.height >= baseHeight ? 16 : 12;

let inboxList = [
    { value: 'Select Reason', label: 'Select Reason' },
    { value: 'Accident', label: 'Accident' },
    { value: 'Maintenance', label: 'Maintenance' },
    { value: 'Billing and Payments', label: 'Billing and Payments' },
    { value: 'Switches', label: 'Switches' }
];

let inboxListTwo = [
    { value: 'Select Channel', label: 'Select Channel' },
    { value: 'Billing & Payments', label: 'Billing & Payments' },
    { value: 'Accident', label: 'Accident' },
    { value: 'Maintenance', label: 'Maintenance' },
    { value: 'Switches', label: 'Switches' }
]

let inboxIds = ['select', 'inb_1npz', 'todo', 'inb_1kpd', 'todo'];
let channelIds = ['select', '+13473346316', 'todo', '+13473346678', 'todo'];
let selectedInboxId = inboxIds[0]
let selectedChannelId = channelIds[0]
let nextPage = ''
let msgs = []


const marginStartPicker = Platform.OS === 'ios' ? 8 : 13
const marginEndPicker = Platform.OS === 'ios' ? 9 : 0


function ChatScreen(props) {
    let platformStyle = Platform.OS === 'ios' ? styles.iosStyle : styles.androidStyle;
    const [state, setState] = useState({
        isLoading: false,
        inboxId: inboxList[0].value,
        driverId: props.driverId,
    });

    const { isLoading, inboxId } = state;
    const [messages, setMessages] = useState([]);
    const [user, setUser] = useState({});
    const [phoneNumber, setDriverPhoneNumber] = useState("");
    const [new_message, setMessage] = useState("");
    const [reason, setreason] = useState("");
    const [profileImage, setProfileImage] = useState(require('../../assets/images/avatar.png'))

    const [CreateDriverMessage] = useMutation(createDriverMessage);
    const [AgetPusherToken] = useMutation(getPusherToken);
    const { data: data, isLoading: isloadingMessages, refetch } = useQuery(getAllDriverMessages, {
        variables: { driverId: state.driverId },
        fetchPolicy: "network-only",
    });


    const setInboxUpdate = (val) => {
        setreason(val)
    }

    const refetchWait = async () => {
        if (refetch) await refetch()
    }
    useEffect(() => {
        RNPusherPushNotifications.on('notification', (notification) => {
            refetchWait()
        });
    }, [data]);

    let cnt = 1



    // Setting up user detail


    function setUsers(userId, token, onError, onSuccess) {
        // Note that only Android devices will respond to success/error callbacks
        RNPusherPushNotifications.setUserId(
            userId,
            token,
            (statusCode, response) => {
                onError(statusCode, response);
            },
            () => {
                onSuccess('Set User ID Success');
            }
        );
    }

    const interests = [
        'debug-test',
        'debug-secondtest',
    ]

    function onPusherInitError(statusCode, response) {
       
    }

    function onPusherInitSuccess(response) {
      
    }

    async function initializePusher(token) {
        try {

            RNPusherPushNotifications.setInstanceId('d56f5e11-ae8d-49d6-b32d-de18db7d8f60');

            // Init interests after registration
            RNPusherPushNotifications.on('registered', () => {
                // This subscribes to general interests. Notifications triggered using
                // instances will be used for all users, not targeting a specific user.
                interests.forEach((interest) => {
                    subscribe(interest);
                    // console.log("Interest: ", interest)
                });
                // Uncomment below interest for testing
                // subscribe("hello")

                // setUser takes our logged in user's `id` attribute and appends `user-` as our
                // backend is setup with Pusher to setUsers using this naming convention (ie. user-214 would
                // be an example user for this function)
                RNPusherPushNotifications
                setUsers(`user-${driverId}`, token, onPusherInitError, onPusherInitSuccess)
            });

            // Setup notification listeners
            // RNPusherPushNotifications.on('notification', handleNotification);
            //   .on('notification', (notification) => {
            //     console.log("Notification received here dd: ", notification)
            //     refetch()
            //     handleNotification({ notification })
            //   });
        } catch (e) {
           
        }
    }

    function subscribe(interest) {
        // console.log('subscribing to interest... ', interest);
        // Note that only Android devices will respond to success/error callbacks
        RNPusherPushNotifications.subscribe(
            interest,
            (statusCode, response) => {
               
            },
            () => {
              
            }
        );
    }

    function handleNotification({ notification }) {
        // Handle notifications received while app is open
        const alert = Platform.OS === 'ios'
            ? notification.userInfo.aps.alert
            : notification;

        // console.log('alert: ', alert);

        // iOS app specific handling
        if (Platform.OS === 'ios') {
            switch (notification.appState) {
                case 'inactive':

                    // inactive: App came in foreground by clicking on notification.
                    //           Use notification.userInfo for redirecting to specific view controller
                    props.toNotification("inactive");
                    break;
                case 'background':


                    // background: App is in background and notification is received.
                    //             You can fetch required data here don't do anything with UI
                    props.toNotification("background");
                    break;
                case 'active':

                    // App is foreground and notification is received. Show an alert or something.
                    props.toNotification("active");
                    break;
                default:
                    break;
            }
        }

        props.toNotification("android");

    };

    useEffect(() => {
        if (data) {
            if (data.allDriverMessages && data.allDriverMessages.edges.length > 0) {
                let receiveMessages = []
                data.allDriverMessages.edges.map((item, index) => {
                    receiveMessages.push({ text: item.node.body, isInbound: item.node.isInbound })
                })
                setMessages(receiveMessages)
            }
        }
    }, [data])
    const sendMessage = async () => {
        Keyboard.dismiss()
        if (isLoading || new_message == '') {
            return
        }
        if (reason == 'Select Reason' || reason == '') {
            alert('Please select the reason to send a message.')
            return
        }
        let message = { text: new_message }
        CreateDriverMessage({
            variables: {
                driverId: state.driverId,
                body: message.text,
                isInbound: false,
                subject: reason
            }
        }).then((response) => {
            if (data) {
                if (response.data.createDriverMessage.ok == true) {
                    messages.unshift(message);
                    setMessage('')
                }
                else if (response.data.createDriverMessage.errors && response.data.createDriverMessage.errors.messages) {
                   
                }
            }
        }).catch((error) => { })
    }


    const onViewRef = React.useRef((current) => {
      
        if (current && current.viewableItems && current.viewableItems.length > 0) {
            current.viewableItems.map((item) => {
             
                if (msgs.length > 0 && item.index >= msgs.length - 1 && isLoading == false) {
                   
                }
            })
           
        }
    })
    const viewConfigRef = React.useRef({ viewAreaCoveragePercentThreshold: 50 })

    const renderItem = ({ item }) => {
        if (!item.isInbound) {
            return (<View style={styles.sent_message_container} >
                <Image style={styles.received_image} source={profileImage} />
                <View style={styles.sent_message_box}>
                    <Text style={[styles.chat_text, global_styles.lato_regular]}>{item.text}</Text>
                </View>
               
            </View >
            )
        } else {
            return (<View style={styles.received_message_container}>

                <Image style={styles.sent_image} source={require('../../assets/images/buggy_chat.png')} resizeMode="contain" />
                <View style={styles.received_message_box}>
                    <Text style={[styles.chat_text, global_styles.lato_regular]}>{item.text}</Text>
                    {/* <Text style={[styles.date_time, global_styles.lato_regular]}>{moment.unix(item.created_at).format('MM-DD-YYYY')}</Text> */}
                </View>
            </View>)
        }
    }



    const getScreenLayout = () => {
        return (<View style={styles.main_container}>
            <View >
                <View style={styles.saperator} />
                <Text style={[styles.title_info, global_styles.lato_medium]}>How can we help?</Text>
                <View style={styles.info_container}>
                    <Text style={[styles.info, global_styles.lato_regular]}>Chat with a member of the Buggy Team,</Text>
                    <Text style={[styles.info, global_styles.lato_regular]}>to get started choose a subject from the list below</Text>
                </View>
                <View style={styles.picker_container}>
                    <View style={[platformStyle]}>
                        <RNPickerSelect
                            placeholder={{}}
                            style={pickerSelectStyles}
                            onValueChange={(value) => setInboxUpdate(value)}
                            mode="dropdown"
                            defaultValue={reason}
                            value={reason}
                            useNativeAndroidPickerStyle={false}
                            items={inboxList}
                            Icon={() => {
                                return (
                                    <View
                                        style={pickerSelectStyles.icon}
                                    />
                                );
                            }}
                        />
                    </View>
                </View>
                <View style={styles.chat_text_box_container}>
                    <TouchableOpacity onPress={sendMessage}>
                        <Image style={styles.send_image} source={require('../../assets/images/send.png')} />
                    </TouchableOpacity>
                    <TextInput
                        placeholder={'Type your message here'}
                        style={[styles.chat_box, global_styles.lato_regular]}
                        value={new_message}
                        onChangeText={(text) => setMessage(text)}
                        onSubmitEditing={Keyboard.dismiss}
                    />
                </View>
                
            </View>
            <View style={styles.list_container}>
                <View style={styles.laoding_info_container}>
                    <Text style={[styles.all_messages, global_styles.lato_regular]}>All Messages</Text>
                    {isLoading && <ActivityIndicator style={styles.loading} size="small" />}
                </View>
                <FlatList
                    data={messages}
                    renderItem={renderItem}
                    // keyExtractor={item => item.id}
                    inverted={true}
                    showsVerticalScrollIndicator={false}
                // onViewableItemsChanged={onViewRef.current}
                // viewabilityConfig={viewConfigRef.current}

                />
            </View>
        </View>)
    }

    return (<>
        {Platform.OS === 'ios' ?
            <KeyboardAvoidingView keyboardVerticalOffset={Header.HEIGHT + 20} style={{ flex: 1 }} behavior="padding">{getScreenLayout()}</KeyboardAvoidingView>
            : getScreenLayout()
        }
    </>)

}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#F9F9F4',
        flex: 1,
        paddingHorizontal: 25,
        paddingTop: 16,
        paddingBottom: 12,
        flexDirection: 'column-reverse',
    },
    iosStyle: {
        marginLeft: 0,
        marginTop: -4,
        paddingVertical: 10
    },
    androidStyle: {
        marginLeft: -15,
        marginTop: -5,
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    title_info: {
        marginTop: 8,
        fontSize: 20,
        alignSelf: 'center',
        color: '#393E5C',
    },
    info_container: {
        marginTop: 8,
    },
    info: {
        alignSelf: 'center',
        color: '#393E5C',
    },
    picker_container: {
        marginTop: 8,
        marginStart: marginStartPicker,
        marginEnd: marginEndPicker,
    },
    laoding_info_container: {
        flexDirection: 'row',
        marginBottom: 8,
    },
    all_messages: {
        color: '#393E5C',
    },
    loading: {
        alignSelf: 'flex-end',
        marginStart: 8
    },
    list_container: {
        flex: 1,
    },
    chat_text_box_container: {
        borderColor: '#D4D4D4',
        borderWidth: 1,
        borderRadius: 8,
        backgroundColor: '#fff',
        marginTop: 16,
        marginStart: 8,
        marginEnd: 8,
        paddingEnd: 16,
        paddingStart: 16,
        paddingTop: chatBoxPadding,
        paddingBottom: chatBoxPadding,
        flexDirection: 'row-reverse',
        alignItems: 'center',
        marginBottom: chatBoxBottomMargin,
    },
    chat_box: {
        flex: 1,
        alignSelf: 'stretch',
        marginEnd: 4,
        color: '#393E5C',
        fontSize: 16,
    },
    send_image: {
        width: 24,
        height: 24,
        alignSelf: 'flex-end',
    },
    icons_container: {
        flexDirection: 'row',
        marginBottom: bottomIconsMargin,
        marginStart: 16,
        marginTop: 8,
    },
    icons: {
        width: 24,
        height: 24,
        marginEnd: 8,
        // alignSelf: 'baseline',
    },
    sent_message_container: {
        flexDirection: 'row',
        marginTop: 16,
        alignSelf: 'flex-start',
        flex: 1
    },
    sent_message_box: {
        marginStart: 20,
        marginLeft:10,
        marginEnd: 1,
        marginTop: 1,
        marginBottom: 1,
        backgroundColor: '#fff',
        borderRadius: 8,
        justifyContent: 'flex-end',
        padding: messageBoxPadding,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 1,
        elevation: 1,
        flexDirection: 'row-reverse',
        flexShrink: 1,
    },
    receive_message_box: {
        marginStart: 10,
        marginEnd: 1,
        flex: 1,
        marginTop: 1,
        marginBottom: 1,
        flexDirection: "row",
        alignSelf: "flex-end",
        alignItems: "flex-end",
        alignContent: "flex-end",
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: messageBoxPadding,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 1,
        elevation: 1,
    },
    sent_image: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        marginTop: 4,
        marginStart:10,
    },
    chat_text: {
        fontSize: chatTextSize,
        alignSelf: 'flex-start',
    },
    date_time: {
        alignSelf: 'flex-end',
        marginTop: 8,
        color: '#393E5C',
        fontSize: 12,
    },
    received_message_container: {
        flexDirection: 'row-reverse',
        marginTop: 16,
        // justifyContent:"space-evenly"
    },
    received_message_box: {
        alignSelf: 'flex-start',
        marginEnd: 1,
        marginTop: 1,
        marginBottom: 1,
        backgroundColor: '#E1A77E',
        borderRadius: 8,
        padding: messageBoxPadding,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        flexDirection: 'row-reverse',
        shadowOpacity: 0.25,
        shadowRadius: 1,
        elevation: 1,
        flexShrink: 1,
    },
    received_image: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        marginTop: 4,
        marginRight:10
        // marginStart: 16,
    },
    saperator: {
        backgroundColor: '#D4D4D4',
        height: 1,
        alignSelf: 'stretch',
        marginTop: 8,
    },
})

export default ChatScreen;