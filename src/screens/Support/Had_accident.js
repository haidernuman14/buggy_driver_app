import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  Image,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  TouchableOpacity,
  Linking,
  Platform,
  BackHandler,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/AntDesign';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { global_styles, modal_style, } from '../../constants/Style';
import LaunchNavigator from 'react-native-launch-navigator';
import ImagePicker from 'react-native-image-picker';
import { driverUploadDocument } from '../../graphql/driverUploadDocument';
import { useMutation, useQuery } from '@apollo/react-hooks';
import Modal from "react-native-modal";

/**
 * Map navigator plugin url
 * https://github.com/dpa99c/react-native-launch-navigator
 */

const win = Dimensions.get('window');

export default function Had_accident(props) {

  const [AdriverUploadDocument] = useMutation(driverUploadDocument);

  const [state, setState] = useState({
    faq_id: ''
  });

  const { faq_id } = state;
  const [isLoading, setLoading] = useState(false)
  const [user, setUser] = useState()
  const [isAccidentImageUploaded, setAccidentImageUploaded] = useState(false)

  const setFAQId = async () => {
    let FAQId = await AsyncStorage.getItem('FAQId');
    setState({ ...setState, faq_id: FAQId })
    let headerTitle = 'FAQ Detail'
    if (FAQId == 0)
      headerTitle = 'Had an accident'
    else if (FAQId == 1)
      headerTitle = 'Need a tow'
    else if (FAQId == 2)
      headerTitle = 'Billing questions'
    else if (FAQId == 3)
      headerTitle = 'Locations and hours'
    props.navigation.setParams({ title: headerTitle })
  }

  const setUserDetail = async () => {
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setUser(user)
  }

  // Set the driver id from the storage
  useEffect(() => {

    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setFAQId()
      setUserDetail()
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setFAQId()
      setUserDetail()
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const getDirectionoBrooklyn = () => {
    LaunchNavigator.navigate("445 Empire Blvd, Brooklyn, NY 11225")
      .then(() => console.log("Launched navigator"))
      .catch((err) => console.error("Error launching navigator: " + err));
  }

  const getDirectionoBronx = () => {
    LaunchNavigator.navigate("691 Burke Avenue, Bronx, NY 10467")
      .then(() => console.log("Launched navigator"))
      .catch((err) => console.error("Error launching navigator: " + err));
  }

  const allowedFileType = (fileType) => {
    const allowed = ["jpg", "jpeg", "png"]
    let extension = "";
    if (fileType) {
      let split_ = fileType.split("/");
      if (split_.length > 1 && split_[1]) {
        extension = split_[1].toLowerCase()
      }
    }
    return allowed.includes(extension);
  }

  const takeAccidentPicture = async () => {
    if (isLoading == true) {
      console.log('Upload accident picture is in progress...')
      return
    }
    console.log('Take accident picture...')
    let options = {
      title: 'Select Image From',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      maxWidth: 700,
      maxHeight: 700
    };
    await ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        if (allowedFileType(response.type)) {
          console.log('File response')
          console.log(response)
          let f_name = response.uri.substring(response.uri.lastIndexOf('/') + 1)
          let input = {
            file: response.data,
            driverId: user.id,
            documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZToyMQ==",
            fileName: f_name
          }
          uploadAccidentPicture(input)
        } else {
          console.log("File type not accepted. Please upload a .jpg, .jpeg or .png file")
        }
      }
    });
  }

  const uploadAccidentPicture = (input) => {
    setLoading(true)
    AdriverUploadDocument({
      variables: { input }
    }).then(async (response) => {
      // console.log("uploadDocument >>" + response.data);
      // console.log("uploadDocument >>" + response.data.uploadDocument);
      if (response.data.uploadDocument.errors && response.data.uploadDocument.errors[0]) {
        console.log("Error Array >>> " + response.data.uploadDocument.errors[0].messages)
      }
      if (response.data.uploadDocument.ok) {
        setLoading(false)
        console.log('Accident picture upload success')
        setAccidentImageUploaded(true)
      } else {
        setLoading(false)
        console.log('Accident picture upload failed')
      }
    })
  }

  return (<>
    <View style={styles.main_container}>
      <KeyboardAwareScrollView>
        <>
            {isLoading &&
              <View style={styles.upload_container}>
                <Text style={global_styles.lato_semibold}>Uploading an accident picture...</Text>
                <ActivityIndicator size="small" />
              </View>
            } 
            <View style={styles.billing_container}>
              <Text style={[styles.accident_title, global_styles.lato_semibold]}>If you were in an accident please complete the following steps:</Text>
              <View style={styles.saperator} />
              <View style={styles.faq_content_icon_text_container}>
                <Icon style={styles.vector_icon} name='warning' />
                <Text style={[styles.faq_content, global_styles.lato_regular]}>Call 911. It's important to report the accident and get a police report even if you are ok and don't need medical attention.</Text>
              </View>
              <View style={styles.faq_content_icon_text_container}>
                <Icon style={styles.vector_icon} name='warning' />
                <View style={{ backgroundColor: "#fff", flexDirection: 'column', flex: 1, }}>
                  <Text style={[styles.faq_content, global_styles.lato_regular]}>Please call Buggy and let us know at <Text onPress={() => Linking.openURL("tel:+13473346313")} style={[global_styles.lato_regular, { color: "#2DAFD3", marginStart: 16, fontSize: 17 }]}> 347-334-6313.</Text></Text>
                </View>
              </View>
              <View style={styles.faq_content_icon_text_container}>
                <Icon style={styles.vector_icon} name='warning' />
                <Text style={[styles.faq_content, global_styles.lato_regular]}>Please get the name, phone number and insurance information of the other party.</Text>
              </View>
              <View style={styles.faq_content_icon_text_container}>
                <Icon style={styles.vector_icon} name='warning' />
                <Text style={[styles.faq_content, global_styles.lato_regular]}>Take as many pictures of the accident and of all the cars involved as possible.{'\n'}Be sure to clearly show any damage.</Text>
              </View>
              <View style={styles.faq_content_icon_text_container}>
                <Icon style={styles.vector_icon} name='warning' />
                <Text style={[styles.faq_content, global_styles.lato_semibold]}>Please send all information and photos at <Text onPress={() => Linking.openURL("tel:+13473346313")} style={[global_styles.lato_regular, { color: "#2DAFD3", marginStart: 16, fontSize: 17 }]}>347-334-6313.</Text></Text>
              </View>
   
            </View> 
           
        </>
      </KeyboardAwareScrollView>

      
        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isAccidentImageUploaded} >
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap}  onPress={() => setAccidentImageUploaded(false)}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Text style={modal_style.modal_heading}>Success</Text>
              <Text style={global_styles.lato_semibold}>Accident image uploaded successfully.</Text>
              <View style={modal_style.modal_textbuttonWrap_center}>
                <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal}  onPress={() => setAccidentImageUploaded(false)}>
                  <Text style={modal_style.upload_document_text_modal}>OK</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>
               


    {/*   <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isAccidentImageUploaded} >
        <View style={modal_style.modal}>
          <View style={modal_style.modal_wrapper}>
            <Text style={global_styles.lato_semibold}>Accident image uploaded successfully.</Text>
            <TouchableOpacity onPress={() => setAccidentImageUploaded(false)}>
              <Text style={styles.modal_option_ok}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>*/}
    </View>
  </>);
}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 4,
    paddingTop: 10,
    paddingBottom: 12,
  },
  upload_container: {
    flexDirection: 'row',
    padding: 16,
    justifyContent: 'space-between',
  },
  faq_title: {
    fontSize: 18,
    marginBottom: 10,
    borderBottomColor: "#f4f4f4",
    borderBottomWidth: 1,
    paddingBottom: 15,
    paddingHorizontal: 15
  },
  faq_title_2: {
    fontSize: 18,
    borderBottomColor: "#f4f4f4",
    borderBottomWidth: 0,
    paddingLeft: 15,
    marginTop: 10,
    marginBottom: 0,
  },
  icon_input: {
    width: 62,
    height: 67,
    alignSelf: "flex-end"
  },
  tow_title: {
    fontSize: 18,
    fontWeight: 'bold',
    padding: 2,
    marginTop: 24
  },
  faq_text: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
    opacity: 0.6,
    marginStart: 1,
    marginEnd: 1,
    padding: 2,
    margin: 0,
  },
  faq_detail_container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#fff",
    margin: 5,
    paddingVertical: 20,
    flexWrap: "wrap",
  },
  billing_container: {
    marginBottom: 16,
    backgroundColor: '#fff',
  },
  accident_title: {
    fontSize: 18,
    margin: 16,
    textAlign: 'center',
  },
  billing_title: {
    fontSize: 18,
    margin: 16,
  },
  billing_content: {
    alignSelf: 'stretch',
    fontSize: 17,
    margin: 16,
    flexWrap: "wrap",
    flex: 1,
    color: '#33363b',
    opacity: 0.6
  },
  faq_content: {
    alignSelf: 'stretch',
    fontSize: 17,
    marginHorizontal: 16,
    flexWrap: "wrap",
    flex: 1,
    color: '#33363b',
    opacity: 0.6
  },
  faq_content_icon_text_container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignSelf: 'stretch',
    fontSize: 14,
    marginTop: 18,
    paddingHorizontal: 20,
  },
  faq_location_detail_container: {
    alignItems: 'center'
  },
  vector_icon: {
    fontSize: 24
  },
  tow_link_text: {
    color: "#2dafd3",
    fontSize: 18,

  },
  tow_container: {
    backgroundColor: "#fff",
    paddingHorizontal: 5,
    paddingBottom: 16,
    marginStart: 4,
    marginEnd: 4,
  },
  location_detail_container: {
    backgroundColor: '#f9f9f4'
  },
  get_directions: {
    // fontWeight: 'bold',
    color: '#2dafd3',
    fontSize: 18,

  },
  location_brooklyn_container: {
    backgroundColor: '#fff',
    paddingHorizontal: 112,
    paddingVertical: 10,
    paddingTop: 20,
    marginBottom: 0

  },
  brooklyn_image_container: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    paddingLeft: 5,
    marginHorizontal: 15,
    marginVertical: 6,
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: 'center',
  },
  image: {
    height: 130,
    width: 130,
  },
  item_image_left: {
    width: 30,
    paddingLeft: 15,
  },
  item_image_right: {
    width: 300,
    position: "absolute",
    marginLeft: 170,
    marginTop: 70,
  },
  location_text_line1: {
    fontSize: 17,
    color: '#393e5c',
  },
  saperator: {
    backgroundColor: '#f9f9f4',
    height: 1,
    alignSelf: 'stretch',
  },
  link_text: {
    color: 'rgb(45,175,211)',
  },
  modal_option_ok: {
    marginTop: 12,
    fontSize: 18,
    color: '#2DAFD3',
    alignSelf: 'flex-end',
  }
});