import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  Image,
  ActivityIndicator,
  View,
  TextInput,
  Keyboard,
  Dimensions,
  Platform,
  TouchableOpacity
} from 'react-native';
import analytics from '@react-native-firebase/analytics';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import moment from "moment";
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { CheckConnectivity } from '../Helper/NetInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SearchBar from 'react-native-search-bar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/Feather';

/**
 * This class displays dashboard information.
 * Displays Pending Dues information with PAY NOW option.
 * Provides information of current rented cars under CURRENT RENTAL section. 
 * Provides history of all the rented cars under RENTAL HISTORY section.
 */

const win = Dimensions.get('window');

export default function SupportScreen(props) {

  const [state, setState] = useState({
    isLoading: true,
    data: {},
    searchtext: "",
    support_faq: null
  });
  const [user, setUser] = useState({});
  const [driverId, setDriverId] = useState("");

  const [filterd_key, setFilterdKey] = useState([]);
  const { setStatusColor, support_faq } = useContext(langContext);
  const { searchtext, isLoading } = state; 

  // Check internet connection
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  // Setting up user detail
  const setuserid = async () => {
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setDriverId(user.id);
    setUser(user);
    let support_data = [{
      data: [
        {
          "key": "accident",
          "name": "Had an accident? Car damaged?",
          "content": "If you were in an accident please follow these steps, Call 911. It's important to report the accident and get a police report even if you are ok and don't need medical attention. Pelase call Buggy at 347-334-6313. Take as many pictures of the accident and the cars involved as possible. Be sure to clearly show all damage."
        },
        {
          "key": "billing",
          "name": "Billing or payment questions",
          "content": "Brooklyn, 445 EMPIRE BLVD, BROOKLYN, NY 11225, Updated Hours: Mon - Fri: 9:00am - 5:00pm, Sat & Sun: Closed, Bronx, 691 BURKE AVENUE, BRONX, NY 10467, Updated Hours: Mon - Fri: 9:00am - 5:00pm, Sat & Sun: Closed"
        },
        {
          "key": "tow",
          "name": "Need a tow?",
          "content": "Awaiting for content"
        },
        {
          "key": "tow",
          "name": "Buggy hours and locations",
          "content": "Awaiting for content"
        },
      ]
    }];
    setState({ ...state, isLoading:false, support_faq: support_data });
  }

  // Set the driver id from the storage
  useEffect(() => {

    checkNet();
    setStatusColor(Colors.account_status_bar_color);
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const showChatHistory = () => {

  }

  const showFAQDetail = async (faqId) => {
    if(faqId=='0'){
      await AsyncStorage.setItem('FAQId_Back_Accident', "");
      props.navigation.navigate('HadAccident')
    } else {
      await AsyncStorage.setItem('FAQId', faqId);
      props.navigation.navigate('FAQDetail')
    }
  }

  const showContactUS = () => {
    props.navigation.navigate('ChatDetail')
  }

  const updateTextInput = (value, field) => {

    let support_data = [{
      data: [
        {
          "key": "accident",
          "name": "Had an accident? Car damaged?",
          "content": "If you were in an accident please follow these steps, Call 911. It's important to report the accident and get a police report even if you are ok and don't need medical attention. Pelase call Buggy at 347-334-6313. Take as many pictures of the accident and the cars involved as possible. Be sure to clearly show all damage."
        },
        {
          "key": "billing",
          "name": "Billing or payment questions",
          "content": "Brooklyn, 445 EMPIRE BLVD, BROOKLYN, NY 11225, Updated Hours: Mon - Fri: 9:00am - 5:00pm, Sat & Sun: Closed, Bronx, 691 BURKE AVENUE, BRONX, NY 10467, Updated Hours: Mon - Fri: 9:00am - 5:00pm, Sat & Sun: Closed"
        },
        {
          "key": "tow",
          "name": "Need a tow?",
          "content": "Awaiting for content"
        },
        {
          "key": "location",
          "name": "Buggy hours and locations",
          "content": "Awaiting for content"
        },
      ]
    }];

    // console.log(support_data[0].data);
    let filterd_key1 = [];
    support_data[0].data.map((ind_data) => {
      let str_value = value.split(" ");
      str_value.map((str_ind) => {
        if (ind_data.name.indexOf(str_ind) > -1) {
          filterd_key1.push(ind_data);
        }
        if (ind_data.content.indexOf(str_ind) > -1) {
          filterd_key1.push(ind_data);
        }
      });
    });
    setFilterdKey(filterd_key1);
    setState({ ...state, [field]: value });
  }

  const checkStatus = (flag) => {
 
    if (  searchtext == "" ) {
      return true;
    }
    let f_g = false;
    filterd_key.map((str_ind) => {
      if (str_ind.key == flag) {
        f_g = true;
      }
    });
    return f_g;
  }
  return (
    <>
      {isLoading ?
        <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> :
        <View style={styles.main_container}>
          <View style={styles.heading_container}>
            <Text style={[styles.heading, global_styles.lato_medium]}>How can we help?</Text>
          </View>
          <View style={[styles.search_bar_container, global_styles.lato_regu]}>
            <Image width="30" style={styles.image_search_icon} source={require('../../assets/images/search-icon.png')} />
            <TextInput
              style={[styles.textInput, global_styles.lato_regular]}
              placeholder={"Search"}
              value={searchtext}
              onChangeText={(text) => updateTextInput(text, 'searchtext')}
            />
          </View>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={(Platform.OS == "web") ? { flexGrow: 1, height: (win.height - 100) } : {}}>
            <>
              <Text style={[styles.question_heading, global_styles.lato_medium]}>Frequently Asked Questions</Text>
              {(checkStatus("accident")) ?
                <TouchableOpacity onPress={(id) => showFAQDetail('0')}>
                  <View style={styles.card_container}>
                    <Image style={styles.image} source={require('../../assets/images/support/accident.png')} />
                    <Text style={[styles.card_text, global_styles.lato_medium]}>Had an accident? Car damaged?</Text>
                  </View>
                </TouchableOpacity> : <></>
              }
              {(checkStatus("billing")) ?
                <TouchableOpacity onPress={(id) => showFAQDetail('2')}>
                  <View style={styles.card_container}>
                    <Image style={styles.image} source={require('../../assets/images/support/billing.png')} />
                    <Text style={[styles.card_text, global_styles.lato_medium]}>Billing or payment questions</Text>
                  </View>
                </TouchableOpacity> : <></>
              }
              {(checkStatus("tow")) ?
                <TouchableOpacity onPress={(id) => showFAQDetail('1')}>
                  <View style={styles.card_container}>
                    <Image style={styles.image} source={require('../../assets/images/support/tow.png')} />
                    <Text style={[styles.card_text, global_styles.lato_medium]}>Need a tow?</Text>
                  </View>
                </TouchableOpacity> : <></>
              }
              {(checkStatus("location")) ?
                <TouchableOpacity onPress={(id) => showFAQDetail('3')}>
                  <View style={styles.card_container}>
                    <Image style={styles.image} source={require('../../assets/images/support/hours_location.png')} />
                    <Text style={[styles.card_text, global_styles.lato_medium]}>Buggy hours and locations</Text>
                  </View>
                </TouchableOpacity>
                : <></>
              }
              <Text style={[styles.connect_heading, global_styles.lato_medium]}>Need to get in touch?</Text>
              <Text style={[styles.connect_content, global_styles.lato_medium]}>Connect with a member of the Buggy Team</Text>
              {/* 
              <View style={styles.contact_detail_main_container}>
                <View style={styles.contact_detail_container}>
                  <Icon2 style={styles.vector_icon} name='phone' />
                  <Text style={[styles.contact_detail_text, global_styles.lato_medium]}>347-334-6313</Text>
                </View>
                <View style={styles.contact_detail_container}>
                  <Icon style={styles.vector_icon} name='email-outline' />
                  <Text style={[styles.contact_detail_text, global_styles.lato_medium]}>info@joinbuggy.com</Text>
                </View>
              </View>
              */}

              <View>
                <View style={[styles.contact_us_container, global_styles.lato_regular]}>
                  <TouchableOpacity onPress={showContactUS}>
                    <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>CONTACT US</Text>
                  </TouchableOpacity>
                </View>
              </View>
              {/* <View style={styles.chat_history_container}>
                <TouchableOpacity onPress={showChatHistory}>
                  <Text style={[styles.click_here, global_styles.lato_regular]}>Click here</Text>
                </TouchableOpacity>
                <Text style={[styles.clickHere_content, global_styles.lato_regular]}>to view your chat history.</Text>
              </View> */}
            </>
          </KeyboardAwareScrollView>
        </View>}
    </>
  );
}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: 22,
    paddingBottom: 12,
  },
  heading_container: {
    alignItems: 'center'
  },
  heading: {
    fontSize: 20,
    color: '#393e5c',
  },
  search_bar_container: {
    marginTop: 8,
    borderWidth: 1,
    borderColor: "#ccc",
    backgroundColor: "#fff",
    flexDirection: "row",
    borderRadius: 6,
    padding: 0,
    paddingBottom: 5,
  },
  textInput: {
    marginTop: 4,
    borderRadius: 6,
    fontSize: 16,
    paddingHorizontal: 12,
    paddingVertical: 6,
    color: '#454545',
    opacity: 0.8,
    backgroundColor: '#ffffff',
    alignSelf: 'stretch',
    flex: 1,
  },
  question_heading: {
    marginTop: 16,
    marginBottom: 8,
    fontSize: 16,
    color: '#000000'
  },
  card_container: {
    // height: 64,
    marginBottom: 16,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    padding: 8,
  },
  image: {
    width: 90,
    height: 75,
    resizeMode: "contain"
  },
  image_search_icon: {
    height: 30,
    marginTop: 10,
    marginLeft: 10,
    width: 30,
  },
  card_text: {
    marginStart: 8,
    fontSize: 15,
    flexWrap: "wrap"
  },
  connect_heading: {
    marginTop: 8,
    fontSize: 18,
    color: '#393E5C'
  },
  connect_content: {
    marginTop: 4,
    fontSize: 16,
    color: '#393E5C'
  },
  vector_icon: {
    fontSize: 18
  },
  contact_detail_main_container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 12
  },
  contact_detail_container: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 2
  },
  contact_detail_text: {
    marginStart: 4,
    fontSize: 12,
  },
  chat_history_container: {
    flexDirection: 'row',
    marginTop: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  click_here: {
    color: '#2dafd3',
    marginEnd: 4,
    fontSize: 16
  },
  clickHere_content: {
    fontSize: 16,
    color: '#393E5C'
  },
  contact_us_container: {
    minWidth: 120,
    padding: 8,
    borderWidth: 1,
    alignSelf: 'center',
    alignItems: 'center',
    borderColor: '#db9360',
    marginTop: 16,
    paddingHorizontal: 30,
    backgroundColor: '#db9360',
    paddingVertical: 12,
    borderRadius: 5,
    textAlign: "center"
  },
  contact_us_text: {
    color: '#FFFFFF',
  },
});