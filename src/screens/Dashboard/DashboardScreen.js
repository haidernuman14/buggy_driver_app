import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  Dimensions,
  Platform
} from 'react-native';
import analytics from '@react-native-firebase/analytics';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import moment from "moment";
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { Driver } from '../../graphql/driverBalanceCurrentRental';
import { CheckConnectivity } from '../Helper/NetInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-picker';
import Modal from "react-native-modal";
import CropImagePicker from 'react-native-image-crop-picker';
import { wrap } from 'lodash';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { driverUploadDocument } from '../../graphql/driverUploadDocument';
import { driverProfileImages } from '../../graphql/driverProfileImages';
import { getAppstoreAppMetadata } from "react-native-appstore-version-checker";

/**
 * This class displays dashboard information.
 * Displays Pending Dues information with PAY NOW option.
 * Provides information of current rented cars under CURRENT RENTAL section. 
 * Provides history of all the rented cars under RENTAL HISTORY section.
 * 
 * Crop Image Picker: https://github.com/ivpusic/react-native-image-crop-picker
 * 
 */

const win = Dimensions.get('window');

const baseHeight = 896;
const gridPadding = win.height >= baseHeight ? 16 : 8;
const gridHeight = win.height >= baseHeight ? 200 : 160;
const gridImageHeight = win.height >= baseHeight ? 160 : 120;

const profileImageSize = 128;
const profileImageBackSize = profileImageSize + 10;

let profileImagesArr = []

export default function DashboardScreen(props) {

  // console.log('Height: ' + win.height)

  const [AdriverUploadDocument] = useMutation(driverUploadDocument);

  const [state, setState] = useState({
    isLoading: false,
    isUpdateProfilePicture: false,
    imageData: "",
  });

  const [user, setUser] = useState({});
  const [driverId, setDriverId] = useState("");
  const { setStatusColor } = useContext(langContext);

  const [profileImage, setProfileImage] = useState(require('../../assets/images/avatar.png'));

  const [current_car, setCurrentCar] = useState("");
  const [current_car_name, setCurrentCarName] = useState("");
  const [tlc_length, setTlcLength] = useState(0);
  const [isGetProfilePicture, setGetProfilePicture] = useState(false)

  const { isLoading, isUpdateProfilePicture, imageData } = state;

  // Check internet connection
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  const { data: profileImages, loading: drivar_data_loading, error: drivar_data_error } = useQuery(driverProfileImages, {
    variables: { id: user.id },
    fetchPolicy: "network-only",
    skip: isGetProfilePicture == false
  });

  const { data: driver_data } = useQuery(Driver, {
    variables: { id: user.id },
    fetchPolicy: "network-only",
    skip: driverId == ''
  });
  const setProfiePic = async () => {
    setGetProfilePicture(false)
    if (profileImages) {
      if (profileImages && profileImages.hasOwnProperty('driver')) {
        if (profileImages.driver.driverdocumentSet.edges && profileImages.driver.driverdocumentSet.edges.length > 0) {
          profileImagesArr = []
          profileImages.driver.driverdocumentSet.edges.map((item) => {
            if (item.node.documentType.typeName == 'Signature') {
              profileImagesArr.push(item.node)
            }
          })
          if (profileImagesArr.length > 0) {
            profileImagesArr.sort((obj1, obj2) => new Date(moment.unix(obj1.dateAdded).format('YYYY-MM-DD')) - new Date(moment.unix(obj2.dateAdded).format('YYYY-MM-DD')))
            setProfileImage({ uri: profileImagesArr[profileImagesArr.length - 1].documentUrl });
            await AsyncStorage.setItem('profile_picture', profileImagesArr[profileImagesArr.length - 1].documentUrl);
          }
        }
      }
    }
  }



  useEffect(() => {
    setProfiePic()

    setStatusColor("#FFF");
  }, [profileImages]);

  // Setting up user detail
  const setuserid = async () => {

    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    let current_car1 = await AsyncStorage.getItem('current_car');
    setCurrentCar(current_car1);

    let current_car_name1 = await AsyncStorage.getItem('current_car_name');
    setCurrentCarName(current_car_name1);

    let path = await AsyncStorage.getItem('profile_picture');
    let imageSource = path == '' || path == null ? require('../../assets/images/avatar.png') : { uri: path };
    setProfileImage(imageSource);

    // user.phone = ''
    // user.dmvLicense = ''
    // user.tlcLicense = ''
    // user.email = 'vishal.vishal.vishal.vishal.vishal@gmail.com'
    // user.address = 'Hello ahdakjdhasjhdja hi ajjasbdjhadhjasbd hello jhasjdahjdasjhd'

    setDriverId(user.id);
    setUser(user);
    setTlcLength(user.tlcLicense.length);
    setGetProfilePicture(true)

    // console.log(user)

  }

  // Set the driver id from the storage
  useEffect(() => {

    checkNet();
    setStatusColor(Colors.account_status_bar_color);
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();
    });


    return () => {
      navFocusListener.remove();
    };
  }, []);

  const cropData = {
    offset: { x: 0, y: 0 },
    size: { width: 500, height: 500 },
    displaySize: { width: 700, height: 700 },
    resizeMode: 'contain' | 'cover' | 'stretch',
  };

  const allowedFileType = (fileType) => {
    const allowed = ["jpg", "jpeg", "png"]
    let extension = "";
    if (fileType) {
      let split_ = fileType.split("/");
      if (split_.length > 1 && split_[1]) {
        extension = split_[1].toLowerCase()
      }
    }
    return allowed.includes(extension);
  }

  const updateProfilePicture = () => {
    setState({ ...setState, isUpdateProfilePicture: true })
  }

  const getAppVersionForAndroid = () => {
    getAppstoreAppMetadata("com.joinbuggy.driverapp") //put any apps packageId here
      .then(metadata => {
        console.log(
          "clashofclans android app version on playstore",
          metadata.version,
          "published on",
          metadata.currentVersionReleaseDate
        );
      })
      .catch(err => {
        console.log("error occurred", err);
      });
  }

  const getAppVersionsForIos = () => {
    getAppstoreAppMetadata("1519736744") //put any apps id here
      .then(appVersion => {
        console.log(
          "clashofclans android app version on appstore",
          appVersion.version,
          "published on",
          appVersion.currentVersionReleaseDate
        );
      })
      .catch(err => {
        console.log("error occurred", err);
      });
  }



  const openGallery = () => {
    CropImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
    }).then(image => {

      let imageSource = image.path == '' ? require('../../assets/images/avatar.png') : { uri: image.path };
      setProfileImage(imageSource);
      setState({ ...setState, isUpdateProfilePicture: false, imageData: image.data })
      if (image.data && image.data != null) {
          console.log("image",image.filename)
        let input = {
          file: image.data,
          driverId: user.id,
          documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZToyMA==",
          fileName: image.filename
        }

        uploadProfilePicture(input)
      }
    });
  }

  const openCamera = () => {
    CropImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
    }).then(image => {

      let imageSource = image.path == '' ? require('../../assets/images/avatar.png') : { uri: image.path };
      setProfileImage(imageSource);
      setState({ ...setState, isUpdateProfilePicture: false, imageData: image.data })
      if (image.data && image.data != null) {
        let currentTimeStamp = Math.round((new Date()).getTime() / 1000)
        let name = currentTimeStamp + '.jpg'
        let input = {
          file: image.data,
          driverId: user.id,
          documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZToyMA==",
          fileName: name
        }

        uploadProfilePicture(input)
      }
    });
  }

  const uploadProfilePicture = (input) => {
    AdriverUploadDocument({
      variables: { input }
    }).then(async (response) => {

      if (response.data.uploadDocument.errors && response.data.uploadDocument.errors[0]) {

      }
      if (response.data.uploadDocument.ok) {
        setGetProfilePicture(true)

      } else {
        setGetProfilePicture(false)

      }
    })
  }

  const cancelSetProfilePicture = () => {
    setState({ ...setState, isUpdateProfilePicture: false })
  }

  const editProfile = async () => {
    await AsyncStorage.setItem('parent', "Dashboard");
    props.navigation.navigate('UpdatePersonaInfo')
  }

  const showBilling = () => {
    props.navigation.navigate('BillingPage')
  }

  const showRental = () => {
    props.navigation.navigate('RentalPage')
  }

  const carScreen = async () => {
    await AsyncStorage.setItem('parent', "Dashboard");
    props.navigation.push('CarinitialScreen',{userdata:user})
  }
  const showCarDocument = () => {
    // props.navigation.navigate('RentalDocs', { carId: current_car, flag: "current_rental", car_name: current_car_name, time: Date.now() });
    props.navigation.navigate('ComingSoon');
  }

  const showHelpSupport = () => {
    props.navigation.navigate('SupportPage')
  }

  let curHr = new Date().getHours();
  let message_day = "";
  if (curHr < 12) {
    message_day = "Good Morning";
  } else if (curHr < 18) {
    message_day = "Good Afternoon";
  } else {
    message_day = "Good Evening";
  }

  return (
    <>
      {isLoading ?
        <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> :
        <View style={styles.main_container}>


          {(driver_data && driver_data.driver && driver_data.driver.reservationDriver.edges.length == 0) || (driver_data &&  driver_data.driver.reservationDriver.edges.length > 0 && driver_data.driver.reservationDriver.edges[0].node.car == null)  ?
            <TouchableOpacity onPress={() => carScreen()}
              style={{ backgroundColor: "#db9360", marginTop: 16, padding: 10, borderRadius: 5, alignItems: "center", justifyContent: "center" }}>
              <Text style={[{ color: "white", textAlign: "center", fontSize: 16 }, global_styles.lato_regular_android]}>Ready to drive again? Choose your car now {">"}</Text>
            </TouchableOpacity>
           :null}
            
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}><>
            <View style={{ marginTop: 16, backgroundColor: '#f9f9f4', }}>
              <View style={styles.profile_image_container}>
                <View style={styles.profile_image_circle_back}>
                  <Image style={profileImagesArr.length > 0 ? styles.profile_image : styles.avatar} source={profileImage} />
                  <View style={styles.camera_container}>
                    <View style={styles.camera_back}>
                      <TouchableOpacity style={styles.camera_image} onPress={updateProfilePicture}><>
                        <Image source={require('../../assets/images/camera_white.png')} />
                      </></TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.profile_container}>
                <View style={styles.picture_text_container}>
                  <View style={styles.profile_details_container}>
                    <Text style={[styles.name_container, { flexWrap: "wrap", maxWidth: 180, textAlign: "right" }, global_styles.lato_semibold]}>{message_day + ' ' + user.firstName + '!'}</Text>
                    <Text style={[styles.first_last_name, global_styles.lato_regular]}>{user.firstName + ' ' + user.lastName}</Text>

                    <Text style={[styles.address_info, { flexWrap: "wrap", maxWidth: 180, textAlign: "right" }, global_styles.lato_regular]}>
                      {(!user.streetAddress || user.streetAddress == null) ? "Address: N/A" : user.streetAddress + ((user.addressLine2 != "" && user.addressLine2 != null) ? "\n" + user.addressLine2 : "")}
                    </Text>
                    {(user.city != "" && user.state != "" && user.zipCode != "") ? <>
                      <Text style={[styles.address_line_2, global_styles.lato_capital]}>
                        {user.city + ", " + user.state + " " + user.zipCode}
                      </Text></> : <></>}

                  </View>
                </View>
                <View style={[styles.phone_email_outer_contaier, { marginTop: 25 }]}>
                  <View style={styles.phone_contaier}>
                    <Icon2 style={styles.vector_icon} name='phone' />
                    <Text style={[styles.phone_email_info, global_styles.lato_regular]}>{(user.phone && user.phone != null && user.phone != "" && user.phone != "+NoneNone") ? ' ' + user.phone.substr(user.phone.indexOf(user.phone.substr(-10)), 3) + "-" + user.phone.substr(user.phone.indexOf(user.phone.substr(-7)), 3) + "-" + user.phone.substr(-4) : " N/A"}</Text>
                  </View>
                  <View style={styles.email_contaier}>
                    <Icon style={styles.vector_icon} name='email-outline' />
                    <Text numberOfLines={1} style={[styles.phone_email_info, global_styles.lato_regular]}>{(!user.email || user.email == null) ? " N/A" : ' ' + user.email}</Text>
                  </View>
                </View>
                <TouchableOpacity onPress={editProfile}>
                  <View style={styles.edit_details_container}>
                    <Text style={[styles.edit_details, global_styles.lato_medium]}>Edit Details</Text>
                  </View>
                </TouchableOpacity>
                <View style={styles.tlc_dmv_main_container}>
                  <View style={[styles.tlc_dmv_container, { marginRight: 5 }]}>
                    <Text style={[styles.tlc_dmv_info, global_styles.lato_semibold]}>TLC License:</Text>
                    <Text style={[styles.tlc_dmv_info, global_styles.lato_regular]}>{(!user.tlcLicense || (parseInt(tlc_length) > 10) ? " N/A" : ' ' + user.tlcLicense)}</Text>
                  </View>
                  <View style={styles.tlc_dmv_container}>
                    <Text style={[styles.tlc_dmv_info, global_styles.lato_semibold]}>DMV License:</Text>
                    <Text style={[styles.tlc_dmv_info, global_styles.lato_regular]}>{(!user.dmvLicense || user.dmvLicense == null) ? " N/A" : ' ' + user.dmvLicense}</Text>
                  </View>
                </View>
              </View>
              <View>
                <View style={styles.grid_row_container}>
                  <View style={styles.grid_box_left}>
                    <TouchableOpacity onPress={showBilling}><>
                      <Image style={styles.grid_image} source={require('../../assets/images/billing-3-sec.gif')} />
                      <Text style={[styles.grid_label, global_styles.lato_medium]}>BILLING & PAYMENTS</Text>
                    </></TouchableOpacity>
                  </View>
                  <View style={styles.grid_box_right}>
                    <TouchableOpacity onPress={showRental}><>
                      <Image style={styles.grid_image} source={require('../../assets/images/rental-3-sec.gif')} />
                      <Text style={[styles.grid_label, global_styles.lato_medium]}>YOUR RENTAL</Text>
                    </></TouchableOpacity>
                  </View>
                </View>
                <View style={styles.grid_row_container}>
                  <View style={styles.grid_box_left}>
                    <TouchableOpacity onPress={showCarDocument}><>
                      {/* <Image style={styles.grid_image} source={require('../../assets/images/documents-3-sec.gif')} />
                      <Text style={[styles.grid_label, global_styles.lato_medium]}>CAR DOCUMENTS</Text> */}
                      <Image style={styles.grid_image} source={require('../../assets/images/documents_s.gif')} />
                      <Text style={[styles.grid_label, global_styles.lato_medium]}>Documents</Text>
                    </></TouchableOpacity>
                  </View>
                  <View style={styles.grid_box_right}>
                    <TouchableOpacity onPress={showHelpSupport}><>
                      <Image style={styles.grid_image} source={require('../../assets/images/support-3-sec.gif')} />
                      <Text style={[styles.grid_label, global_styles.lato_medium]}>HELP & SUPPORT</Text>
                    </></TouchableOpacity>
                  </View>
                </View>
              </View>
            </View></></KeyboardAwareScrollView>
          <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isUpdateProfilePicture} >
            <View style={modal_style.modal}>
              <View style={modal_style.modal_wrapper}>
                <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={cancelSetProfilePicture}>
                  <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                </TouchableOpacity>
                <Text style={styles.modal_title}>Set profile picture from:</Text>
                <View style={styles.modal_options_container}>
                  <TouchableOpacity onPress={openGallery}>
                    <Text style={styles.modal_option}>Gallery</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={openCamera}>
                    <Text style={styles.modal_option}>Camera</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={cancelSetProfilePicture}>
                    <Text style={styles.modal_option_cancel}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      }
    </>
  );
}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 15,
    paddingBottom: 12,
  },
  name_container: {
    fontSize: 16,
    color: '#393e5c',
  },
  profile_outer: {
    borderRadius: 8,
  },
  profile_container: {
    backgroundColor: '#fff',
    padding: 12,
    borderRadius: 8,
    // overflow: 'visible',
  },
  picture_text_container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  profile_image_container: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    position: 'absolute',
    top: -12,
    left: 8,
    zIndex: 1,
  },
  profile_image_circle_back: {
    height: profileImageBackSize,
    width: profileImageBackSize,
    borderRadius: profileImageBackSize / 2,
    justifyContent: 'center',
    backgroundColor: '#f9f9f4',
  },
  profile_image: {
    height: profileImageSize,
    width: profileImageSize,
    borderRadius: profileImageSize / 2,
    alignSelf: 'center',
  },
  avatar: {
    height: profileImageSize,
    width: profileImageSize,
    borderRadius: profileImageSize / 2,
    alignSelf: 'center',
    opacity: 0.8,
  },
  camera_container: {
    height: 118,
    width: 130,
    flex: 1,
    justifyContent: 'flex-end',
    position: 'absolute',
  },
  camera_back: {
    flex: 1,
    backgroundColor: 'rgb(45,175,211)',
    width: 26,
    height: 26,
    borderRadius: 26 / 2,
    position: 'absolute',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  camera_image: {
    width: 22,
    height: 21,
    alignSelf: 'center',
    marginStart: 5,
    marginTop: 4,
  },
  profile_details_container: {
    flex: 2,
    alignSelf: 'stretch',
    alignItems: 'flex-end',
    marginStart: 8,
  },
  tlc_dmv_main_container: {
    flexDirection: 'row',
    flexWrap: "wrap",
    justifyContent: 'flex-end',
    marginTop: 16,
  },
  tlc_dmv_container: {
    flexDirection: 'row',
  },
  tlc_dmv_info: {
    fontSize: 14,
    color: '#393e5c',
  },
  edit_details_container: {
    marginTop: 4,
    flexDirection: 'row-reverse',
  },
  edit_details: {
    color: '#2dafd3',
    fontSize: 14,
  },
  grid_row_container: {
    flex: 1,
    flexDirection: 'row',
    height: gridHeight,
    marginTop: 16,
  },
  grid_box_left: {
    flex: 1,
    backgroundColor: '#fff',
    marginEnd: 8,
    justifyContent: 'flex-end',
    borderRadius: 8,
    padding: gridPadding,
  },
  grid_box_right: {
    flex: 1,
    backgroundColor: '#fff',
    marginStart: 8,
    justifyContent: 'flex-end',
    borderRadius: 8,
    padding: gridPadding,
  },
  grid_image: {
    alignSelf: 'center',
    marginBottom: 8,
    width: gridImageHeight,
    height: 120
  },
  grid_label: {
    alignSelf: 'center',
    fontSize: 13,
    color: '#222222',
  },
  first_last_name: {
    fontSize: 14,
    alignSelf: 'flex-end',
    marginTop: 24,
    color: '#393e5c',
  },
  address_info: {
    fontSize: 14,
    alignSelf: 'flex-end',
    color: '#393e5c',
    marginTop: 4,
    textAlign: "right",

  },
  address_line_2: {
    fontSize: 14,
    alignSelf: 'flex-end',
    color: '#393e5c',
    marginTop: 4,
    textAlign: "right",
  },
  phone_email_info: {
    fontSize: 14,
    color: '#393e5c',
    flexWrap: 'wrap',
    overflow: 'scroll',
    alignSelf: 'flex-end',
  },
  vector_icon: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#393e5c',
  },
  phone_email_outer_contaier: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: "wrap",
    marginTop: 18,
    justifyContent: 'flex-end',
  },
  phone_contaier: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    alignSelf: "center",
    marginRight: 10
  },
  email_contaier: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  modal_style: {
    backgroundColor: '#fff',
    padding: 24,
  },
  modal_title: {
    justifyContent: 'center',
    fontSize: 12,
  },
  modal_options_container: {

  },
  modal_option: {
    marginTop: 12,
    fontSize: 18,
  },
  modal_option_cancel: {
    marginTop: 12,
    fontSize: 18,
    color: '#ff0000',
    alignSelf: 'flex-end',
  }
});
