import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, Image, View, Text, Dimensions, FlatList, ActivityIndicator, TouchableOpacity, PermissionsAndroid, Platform } from 'react-native';
import { global_styles } from '../../constants/Style';
import { driverDocuments } from '../../graphql/allDocumentsType';
import { downloadFromDrive } from '../../graphql/downloadFromDrive';
import { useMutation, useQuery } from '@apollo/react-hooks';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";
import { set } from 'react-native-reanimated';
import moment from "moment";
import RNFetchBlob from 'rn-fetch-blob';
const { config, fs } = RNFetchBlob;
// import { TouchableOpacity } from 'react-native-gesture-handler';
import FileViewer from "react-native-file-viewer";
import { stat } from 'react-native-fs';
const win = Dimensions.get('window');
import { useNetInfo } from '@react-native-community/netinfo';
import { ScrollView } from 'react-native-gesture-handler';
const marginHorizontal = 32
const imageWidth = win.width - (marginHorizontal * 2)

export default function ComingSoonScreen(props) {

  const [state, setState] = useState({
    isLoading: true,
    isUpdateProfilePicture: false,
    imageData: "",
    showImage: null,
    selectedDocumentType: "Cars",
  });
  const { isLoading } = state;
  const [user, setUser] = useState({});
  const [driverId, setDriverId] = useState("");
  const [driverDoucments, setDriverDoucments] = useState([]);
  const [carDoucments, setCarDoucments] = useState([]);

  const { data: driver, loading: drivar_data_loading, error: drivar_data_error } =
    useQuery(driverDocuments, {
      variables: { id: driverId },
      fetchPolicy: "network-only",
    })


  ///query for get download file from google drive...//
  const [getDownloadFromDrive] = useMutation(downloadFromDrive)

  useEffect(() => {
    if (driver) {
      if (driver && driver.hasOwnProperty('driver')) {

        if (driver.driver.currentAgreement != null && driver.driver.currentAgreement.car.cardocumentSet.edges && driver.driver.currentAgreement.car.cardocumentSet.edges.length > 0) {
          let CardoucmentsData = [];
          driver.driver.currentAgreement.car.cardocumentSet.edges.map((item) => {
            let doucments = {
              doucmentType: item.node.documentType.typeName,
              doucmentUrl: item.node.documentUrl,
              doucmentTitle: item.node.name
            }
            if (item.node.makeVisible == true && item.node.documentType.visibleInDriverApp == true) {
              CardoucmentsData.push(doucments)

            }
          })
          setCarDoucments(CardoucmentsData);
        }
        if (driver.driver.driverdocumentSet.edges && driver.driver.driverdocumentSet.edges.length > 0) {
          let doucmentsData = [];
          driver.driver.driverdocumentSet.edges.map((item) => {

            let doucments = {
              doucmentType: item.node.documentType.typeName,
              doucmentUrl: item.node.documentUrl,
              doucmentTitle: item.node.name
            }
            if (item.node.makeVisible == true && item.node.documentType.visibleInDriverApp == true) {
              doucmentsData.push(doucments)

            }
          })
          setDriverDoucments(doucmentsData)
        }
        setState({ ...state, isLoading: false });
      }
    }

  }, [driver])

    ///start ....///
  const downloadfileFromGoogelDrive = (fileId, name) => {
    setState({ ...state, isLoading: true });
    let getfileId = getIdFromUrl(fileId)
    getDownloadFromDrive({
      variables: { input: { fileId: getfileId[0] } }
    }).then((response) => {
      setState({ ...state, isLoading: false });
      if (response.data.downloadFromDrive.fileData != null) {
        setState({ ...state, isLoading: true });
        RNFetchBlob
          .config({
            addAndroidDownloads: {
              useDownloadManager: true,
              notification: true,
              mime: getExtention(name) ? 'application/' + getExtention(name) : 'application/pdf',
              description: 'File downloaded.'
            }
          })
        let base64Str = response.data.downloadFromDrive.fileData;
        let pdfLocation = getExtention(name) ? RNFetchBlob.fs.dirs.DocumentDir + '/' + name : RNFetchBlob.fs.dirs.DocumentDir + '/' + name + ".pdf";
        RNFetchBlob.fs.writeFile(pdfLocation, base64Str, 'base64').then(res => {
          if (res > 0) {
            show_doc(pdfLocation);
          }
        }).catch((error => {  setState({ ...state, isLoading: false }); }))
      }
      else
        alert("File not exists Please Contact Buggy Team")
        
    }).catch((error) => {  setState({ ...state, isLoading: false }); })

  }

  const getExtention = filename => {
    //To get the file extension
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
  };
  ///get File Extension...///
  function getIdFromUrl(url) {
    return url.match(/[-\w]{25,}/);
  }

  const show_doc = async (pdfLocation) => {
    try {

      setState({ ...state, isLoading: false });
      await FileViewer.open(pdfLocation, { showOpenWithDialog: true, showAppsSuggestions: true });
    } catch (e) {
      console.warn(TAG, "An error occurred", JSON.stringify(e));
    }
  }
  const setuserid = async () => {
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setDriverId(user.id);
    setUser(user);
  }
  useEffect(() => {
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();
    }
    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();
    });
    return () => {
      navFocusListener.remove();
    };
  }, []);

  

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  const downloadImage = (image_URL) => {
    setState({ ...state, isLoading: true });
    fetch(image_URL)
      .then(response => response.json())
      .then(data => {
        if (data.error) {
          setState({ ...state, isLoading: false });
          alert("File not exists Please Contact Buggy Team")
        }
        else {
          downloadImagee(image_URL)
        }
      }).catch(error => {
        downloadImagee(image_URL)
      })
  }

  const downloadImagee = (image_URL) => {

    setState({ ...state, isLoading: true });
    RNFetchBlob
      .config({
        fileCache: true,
        appendExt: 'jpg'
      })
      .fetch('GET', image_URL)
      .then((resp) => {
        RNFetchBlob.fs.exists(resp.path())
          .then((exist) => {
            setState({ ...state, isLoading: false });

          })
          .catch(() => {
            setState({ ...state, isLoading: false });
          });

        if (Platform.OS === 'ios') {
          setState({ ...state, isLoading: false });
          RNFetchBlob.ios.openDocument(resp.path());
        } else {
          setState({ ...state, isLoading: false });
          RNFetchBlob.android.actionViewIntent('file://' + resp.path(), 'image/jpg');
        }
      })
      .catch((errorMessage, statusCode) => {
        setState({ ...state, isLoading: false });
      });
  }
  ////.......//////......Download permissionsAndroid for android side .....///../..//
  const checkPermission = async (image_url, fileName) => {

    //Function to check the platform
    //If iOS the start downloading
    //If Android then ask for runtime permission

    if (Platform.OS === 'ios') {
      AndroidImageDownload(image_url, fileName);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'This app needs access to your storage to download Photos',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //Once user grant the permission start downloading

          AndroidImageDownload(image_url, fileName);
        } else {
          //If permission denied then show alert 'Storage Permission Not Granted'
          alert('Storage Permission Not Granted');
        }
      } catch (err) {
        //To handle permission related issue

      }
    }
  };

  ////.......//////......Download image for android side .....///../..//

  const AndroidImageDownload = (image, fileName) => {
    setState({ ...state, isLoading: true });
    //Main function to download the image
    let date = new Date(); //To add the time suffix in filename
    //Image URL which we want to download
    let image_URL = image;
    //Getting the extention of the file
    let ext = getExtention(image_URL);
    ext = '.' + ext[0];
    //Get config and fs from RNFetchBlob
    //config: To pass the downloading related options
    //fs: To get the directory path in which we want our image to download
    const { config, fs } = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        //Related to the Android only
        title: fileName,
        useDownloadManager: true,
        notification: true,
        path:
          PictureDir +
          '/image_' + Math.floor(date.getTime() + date.getSeconds() / 2) + ext,
        description: 'Image',
      },
    };
    config(options)
      .fetch('GET', image_URL)
      .then(res => {
        setState({ ...state, isLoading: false });
        alert('Image Downloaded Successfully.');
      }).catch(error => {
        setState({ ...state, isLoading: false });

      })
  };

  downloadpfd = (url, title) => {
    setState({ ...state, isLoading: true });
    RNFetchBlob
      .config({
        fileCache: true,
        title: title,
        appendExt: 'pdf'
      })
      .fetch('GET', url)
      .then((resp) => {
        RNFetchBlob.fs.exists(resp.path())
          .then((exist) => {
            setState({ ...state, isLoading: false });
          })
          .catch(() => {
            setState({ ...state, isLoading: false });
          });

        if (Platform.OS === 'ios') {
          setState({ ...state, isLoading: false });
          RNFetchBlob.ios.openDocument(resp.path());
        } else {
          setState({ ...state, isLoading: false });
          RNFetchBlob.android.actionViewIntent('file://' + resp.path(), 'image/pdf');
        }
      })
      .catch((errorMessage, statusCode) => {
        setState({ ...state, isLoading: false });

      });
  }
  const renderItem = ({ item, index }) => {

    if (item.doucmentUrl.includes("drive.google.com")) {
      return (
        <View style={{ flex: 1, width: "100%", padding: 20, flexDirection: "row", justifyContent: "space-around", alignItems: "center", backgroundColor: "white", marginTop: 10 }}>
          <Text style={{ flex: 1 }}>{item.doucmentType}</Text>
          <TouchableOpacity
            onPress={() => {
              downloadfileFromGoogelDrive(item.doucmentUrl, item.doucmentTitle)
            }}
            style={{ borderRadius: 10, borderWidth: 0.5, borderColor: "grey", padding: 5 }}>
            <Text>Download</Text>
          </TouchableOpacity>
        </View>
      )
    }
    else {
      return (
        <View style={{ flex: 1, width: "100%", padding: 20, flexDirection: "row", justifyContent: "space-around", alignItems: "center", backgroundColor: "white", marginTop: 10 }}>
          <Text style={{ flex: 1 }}>{item.doucmentType}</Text>
          {item.doucmentTitle.includes('.pdf') ?
            <TouchableOpacity
              onPress={() => {
                if (Platform.OS === 'ios')
                  downloadpfd(item.doucmentUrl)
                else
                  checkPermission(item.doucmentUrl, item.doucmentTitle)
              }}
              style={{ borderRadius: 10, borderWidth: 0.5, borderColor: "grey", padding: 5 }}>
              <Text>Download</Text>
            </TouchableOpacity>
            : <TouchableOpacity
              onPress={() => {
                if (Platform.OS === 'ios')
                  downloadImage(item.doucmentUrl)
                else
                  checkPermission(item.doucmentUrl, item.doucmentTitle)
              }
              }
              style={{ borderRadius: 10, borderWidth: 0.5, borderColor: "grey", padding: 5 }}>
              <Text>Download</Text>
            </TouchableOpacity>}
        </View>
      )
    }
  }
  const renderCartItem = ({ item, index }) => {
    if (item.doucmentUrl.includes("drive.google.com")) {
      return (
        <View style={{ flex: 1, width: "100%", padding: 20, flexDirection: "row", justifyContent: "space-around", alignItems: "center", backgroundColor: "white", marginTop: 10 }}>
          <Text style={{ flex: 1 }}>{item.doucmentType}</Text>
          <TouchableOpacity
            onPress={() => {
              downloadfileFromGoogelDrive(item.doucmentUrl, item.doucmentTitle)
            }}
            style={{ borderRadius: 10, borderWidth: 0.5, borderColor: "grey", padding: 5 }}>
            <Text>Download</Text>
          </TouchableOpacity>
        </View>
      )
    }
    else {
      return (
        <View style={{ flex: 1, width: "100%", padding: 20, flexDirection: "row", justifyContent: "space-around", alignItems: "center", backgroundColor: "white", marginTop: 10 }}>
          <Text style={{ flex: 1 }}>{item.doucmentType}</Text>
          {item.doucmentTitle.includes('.pdf') ?
            <TouchableOpacity
              onPress={() => {
                if (Platform.OS == 'ios')
                  downloadpfd(item.doucmentUrl)
                else
                  checkPermission(item.doucmentUrl, item.doucmentTitle)
              }}
              style={{ borderRadius: 10, borderWidth: 0.5, borderColor: "grey", padding: 5 }}>
              <Text>Download</Text>
            </TouchableOpacity>
            : <TouchableOpacity
              onPress={() => {
                if (Platform.OS === 'ios')
                  downloadImage(item.doucmentUrl)
                else
                  checkPermission(item.doucmentUrl, item.doucmentTitle)
              }}
              style={{ borderRadius: 10, borderWidth: 0.5, borderColor: "grey", padding: 5 }}>
              <Text>Download</Text>
            </TouchableOpacity>}
        </View>
      )
    }
  }
  return (
    <>
      {isLoading ?
        <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> : <></>}
        <ScrollView style={{backgroundColor: '#f9f9f4',}}>
      <View style={styles.main_container}>
        <View style={{ marginTop: 10 }}>
            <Text style={{ fontSize: 17 }}>Car Documents</Text>
          <View>
            {carDoucments.length > 0 ?
              <FlatList
                data={carDoucments}
                renderItem={renderItem}
                style={{ marginTop: 10, }}
                keyExtractor={(item) => item.id}
              />
              : <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Text style={{ color: "grey" }}>No Records Found</Text>
              </View>
            }
          </View>
        </View>
        <View>
          <View style={{ marginTop: 10 }}>
            <Text style={{ fontSize: 17 }}>Driver Documents</Text>
            <View>
              {driverDoucments.length > 0 ?
                <FlatList
                  data={driverDoucments}
                  renderItem={renderCartItem}
                  style={{ marginTop: 10, }}
                  keyExtractor={(item) => item.id}
                />
                : <View style={{ justifyContent: "center", alignItems: "center" }}>
                  <Text style={{ color: "grey" }}>No Records Found</Text>
                </View>
              }
            </View>
          </View>
        </View>
      </View>
      </ScrollView>
    </>
  )
}
const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 15,
    paddingBottom: 12,
  },
  image_container: {
    alignItems: 'center',
  },
  image: {
    height: imageWidth,
    width: imageWidth,
    marginHorizontal: marginHorizontal,
    marginTop: -marginHorizontal,
  },
  text: {
    color: '#000',
    textAlign: 'center',
    fontSize: 18,
    marginTop: -marginHorizontal,
    marginHorizontal: 8,
  },
})
