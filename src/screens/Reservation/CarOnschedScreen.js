import React, { useState, useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { View, ScrollView, SafeAreaView, ActivityIndicator, Text, StyleSheet, TouchableHighlight, Linking } from 'react-native';
import Colors from '../constants/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import CalendarPicker from 'react-native-calendar-picker';
import TokenApi from '../onsched/token';
import ServicesApi from '../onsched/services/service';
import AvailabilityApi from '../onsched/appointments/availability';
import AvailabilityAddApi from '../onsched/appointments/appointmentAdd';
import AvailabilityBookApi from '../onsched/appointments/appointmentBook';
import CustomerAddApi from '../onsched/customers/add';
import CustomerGetApi from '../onsched/customers/get';
import { createReservation } from '../graphql/createReservation';
import moment from 'moment';
import { translate } from '../components/Language';
import Icon from 'react-native-vector-icons/FontAwesome'
import { global_styles, modal_style } from '../constants/Style';
import { CheckConnectivity } from './NetInfo';
import AppointmentAddApi from '../onsched/appointments/appointmentAdd';
import RadioForm from 'react-native-simple-radio-button';
import { WebView } from 'react-native-webview';
import Icon3 from 'react-native-vector-icons/AntDesign';
import Modal from "react-native-modal";

let date = new Date();
let day = date.getDay();
let minDay = (day == 6) ? 1 : 0;
let maxDay = (day == 5 || day == 6) ? 2 : 1;
export default function CarOnschedScreen(props) {
  const [AcreateReservation] = useMutation(createReservation);
  const [state, setState] = useState({
    apiMode: 'live',//sandbox
    isLoading: false,
    isTimeLoading: false,
    calenderShow: false,
    userInfo: '',
    error_time: '',
    error_payment: '',
    error_global: '',
    token: '',
    pk: '',
    onschedCustomerId: 0,
    minDate: moment().add(minDay, 'day'),
    maxDate: moment().add(maxDay, 'day'),
    displayDate: moment().add(minDay, 'day').format('MM-DD-YYYY'),
    startDate: moment().add(minDay, 'day').format('YYYY-MM-DD'),
    endDate: moment().add(minDay, 'day').format('YYYY-MM-DD'),
    selectedTime: '',
    startDateTime: '',
    endDateTime: '',
    serviceId: 1730,
    resourceId: 25144,
    serviceList: {},
    resourcesList: {},
    isAvailable: true,
    paymentType: '',
    paymentTypeIndex: '',
    paymentTypeIndexValue: 0,
    availabilityList: [],
    isCard:false,
    isRemoteCash:false,
    isZelle:false,
    isModalPayLater:false,
    paylater_method_error:"",
    isModalPayLaterSuccess:false,
    isVanila:false,
    isModalPopup:false,
  });
  const { apiMode, isModalPopup, isCard, isVanila, paymentTypeIndexValue, pk, isZelle, isRemoteCash,isModalPayLaterSuccess, paylater_method_error, isModalPayLater, paymentType, error_global, paymentTypeIndex, isLoading, isTimeLoading, calenderShow, userInfo, token, onschedCustomerId, serviceId, resourceId, isAvailable, selectedTime, startDateTime, endDateTime, minDate, maxDate, displayDate, startDate, endDate, serviceList, resourcesList, availabilityList, error_time, error_payment } = state;

  useEffect(() => {
    const status = checkNet();
    if (status) {
      detail();
    }
  }, [props.navigation.state])

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    //await AsyncStorage.setItem('onSchedToken', '');
    if (!netState) {
      props.screenProps.setNetModal(true)
      return false;
    }
    return true;
  }

  const detail = async () => {
    console.log('detail')
    const details = await AsyncStorage.getItem('user');
    let onsched_customer_id = await AsyncStorage.getItem('onsched_customer_id');
    let data = JSON.parse(details);
    let pk = JSON.parse(details).pk;
    if (onsched_customer_id == null) {
      onsched_customer_id = 0;
    }
    let _token = '';
    try {
      const result = await TokenApi(apiMode);
      console.log('result', (result.access_token)?true:false)
      _token = result.access_token;
    } catch (error) {
      console.log('TokenApiError', error);
    }
    setState({ ...state, pk: pk, token: _token, userInfo: data, onschedCustomerId: onsched_customer_id });
  }

  useEffect(() => {
    if (token != '') {
      AvailabilityFun('token')
    }
  }, [token])

  useEffect(() => {
    if (isTimeLoading == true) {
      AvailabilityFun('time')
    }
  }, [isTimeLoading]) 

  const openCalender = () => {
    setState({ ...state, calenderShow: !calenderShow })
  }

  const onDateChange = (date) => {
    let setDate = moment(date).format('YYYY-MM-DD');
    let setDisplayDate = moment(date).format('MM-DD-YYYY');
    //console.log(setDate)
    setState({ ...state, isTimeLoading: true, displayDate: setDisplayDate, startDate: setDate, endDate: setDate, selectedTime: '', calenderShow: false })
  }

  const AvailabilityFun = async (type) => {
    try {
      const result = await AvailabilityApi(apiMode, token, serviceId, startDate, endDate, resourceId);
      if (result.availableTimes.length > 0) {
        setState({ ...state, isLoading: false, isTimeLoading: false, availabilityList: result.availableTimes, isAvailable: true })
      } else {
        setState({ ...state, isLoading: false, isTimeLoading: false, availabilityList: [], isAvailable: false })
      }
    } catch (error) {
      console.log('AvailabilityApiError', error)
    }
  }

  const selectTime = (time, startDateTime, endDateTime) => {
    setState({ ...state, selectedTime: time, startDateTime: startDateTime, endDateTime: endDateTime, error_time: '' })
  } 

  const renderTime = () => {
    const availabilityTime0 = [];
    const availabilityTime1 = [];
    const availabilityTime2 = [];
    availabilityList.map((item, index) => {
      if (item.time >= 900 && item.time <= 1130) {
        availabilityTime0.push(item)
      } else if (item.time >= 1200 && item.time <= 1630) {
        availabilityTime1.push(item)
      } else {
        availabilityTime2.push(item)
      }
    })
    return (<>
      {availabilityTime0.length > 0 && <View key={0} style={styles.calenderTimeBoxView}>
        <Text style={styles.calenderTimeBoxText}>Morning</Text>
        <View style={styles.calenderTimeBoxWrapRow}>
          {availabilityTime0.map((item, index) => {
            return (
              <TouchableHighlight underlayColor='' style={selectedTime == item.displayTime ? styles.calenderTimeBoxRowSelected : styles.calenderTimeBoxRow} onPress={() => selectTime(item.displayTime, item.startDateTime, item.endDateTime)}>
                <Text style={selectedTime == item.displayTime ? styles.calenderTimeBoxRowTextSelected : styles.calenderTimeBoxRowText}>{item.displayTime}</Text>
              </TouchableHighlight>
            )
          })}
        </View>
      </View>}
      {availabilityTime1.length > 0 && <View key={1} style={styles.calenderTimeBoxView}>
        <Text style={styles.calenderTimeBoxText}>Afternoon</Text>
        <View style={styles.calenderTimeBoxWrapRow}>
          {availabilityTime1.map((item, index) => {
            return (
              <TouchableHighlight underlayColor='' style={selectedTime == item.displayTime ? styles.calenderTimeBoxRowSelected : styles.calenderTimeBoxRow} onPress={() => selectTime(item.displayTime, item.startDateTime, item.endDateTime)}>
                <Text style={selectedTime == item.displayTime ? styles.calenderTimeBoxRowTextSelected : styles.calenderTimeBoxRowText}>{item.displayTime}</Text>
              </TouchableHighlight>
            )
          })}
        </View>
      </View>}
      {availabilityTime2.length > 0 && <View key={2} style={styles.calenderTimeBoxView}>
        <Text style={styles.calenderTimeBoxText}>Evening</Text>
        <View style={styles.calenderTimeBoxWrapRow}>
          {availabilityTime2.map((item, index) => {
            return (
              <TouchableHighlight underlayColor='' style={selectedTime == item.displayTime ? styles.calenderTimeBoxRowSelected : styles.calenderTimeBoxRow} onPress={() => selectTime(item.displayTime, item.startDateTime, item.endDateTime)}>
                <Text style={selectedTime == item.displayTime ? styles.calenderTimeBoxRowTextSelected : styles.calenderTimeBoxRowText}>{item.displayTime}</Text>
              </TouchableHighlight>
            )
          })}
        </View>
      </View>}
    </>);
  }

  const checkoutFun = async () => {
    //props.navigation.navigate('CarPayment', { paymentype: paymentType, type: props.navigation.state.params.type,price: props.navigation.state.params.price });
    //return;
    let errortime = "";
    let errorpaymentmethod = "";
    let error_flag = 0;
    // if ( selectedTime == '') {
    //   errortime =  "Please choose time"; 
    //   error_flag = 1;
    // } else {
    //   errortime = "";
    // }
    if( paymentTypeIndexValue == "" ) {
      errorpaymentmethod = "Please choose payment method";
      error_flag = 1;
    } else {
      errorpaymentmethod = "";
    }

    if( error_flag == 0 ) {
      console.log("paymentTypeIndexValue--",paymentTypeIndexValue);
      if( paymentTypeIndexValue == 1 ) {
        reservationFun(2);
      } else {
        setState({ ...state, isModalPopup: true });
      }

    } else {
      setState({ ...state, error_time: errortime, error_payment:errorpaymentmethod  });
    }
    return;
    //development
   /* if( isModalPayLater && isZelle == false && isCard == false ) {
      setState({...state, paylater_method_error:"Please select payment method"});
      return;
    } else {
      setState({...state, paylater_method_error:""});
    }
    return; */

   // reservationFun(); // return;
    // props.navigation.navigate('CarPayment', { paymentype: paymentType, price: 399 })  
   // return;
    /*if (paymentType == '') {
      setState({ ...state, error_payment: "Please choose Payment Method" })
      return;
    } else {
      //reservationFun();
      props.navigation.navigate('CarPayment', { paymentype: paymentType, price: 399 })  
      return;
    }*/
    if (selectedTime == '' && false) {
      setState({ ...state, error_time: "Please choose time" })
    } else {
      if (paymentType == '') {
        setState({ ...state, error_payment: "Please choose Payment Method" })
      } else if( isModalPayLater && isZelle == false && isVanila == false ) {
        setState({...state, paylater_method_error:"Please select payment method"});
        return;
      } else {
        if( isModalPayLater ) { 
          setState({ ...state, isModalPayLater: true });
        } else { 
          reservationFun(2);
        }
        //props.navigation.navigate('CarPayment', { paymentype: paymentType, price: props.navigation.state.params.price })
        return;
        if (onschedCustomerId != 0) {
          if (startDateTime != '') {
            //AppointmentBookFun();
            AppointmentAddFun(onschedCustomerId)
          } else {
            console.log('startDateTime', startDateTime)
          }
        } else {
          let onsched_customer_id = 0;
          try {
            //'jigar@aceinfoway.com'
            //console.log('userInfo:email', userInfo.email)
            const customer = await CustomerGetApi(apiMode, token, userInfo.email);
            //console.log('customerGet', customer)
            if (customer.count > 0) {
              onsched_customer_id = customer.data[0].id;
              //AsyncStorage.setItem('onsched_customer_id',onsched_customer_id)
              setState({ ...state, onschedCustomerId: onsched_customer_id })
            } else {
              try {
                const customerAdd = await CustomerAddApi(apiMode, token, userInfo);
                //console.log('customerADD', customerAdd)
                onsched_customer_id = customerAdd.id;
                setState({ ...state, onschedCustomerId: onsched_customer_id })
              } catch (error) {
                console.log('CustomerAddApiError', error)
              }
            }
          } catch (error) {
            console.log('CustomerGetApiError', error)
          }
          if (onsched_customer_id != 0) {
            console.log(onsched_customer_id)
            if (startDateTime != '') {
              //AppointmentBookFun();
              AppointmentAddFun(onsched_customer_id)
            } else {
              console.log('startDateTime', startDateTime)
            }
          }
        }
      }
    }
  }
  /*discount:0,
        pickUpLocation:"",
        surcharge:0,
        deposit:399,
        notes:"",
        priceRange:props.navigation.state.params.car_info.weeklyCharge,
        sendMessage:"",
        returnDate:"",
        isSplit:false,*/
        
  const reservationFun = async (flag_redired) => {
       
   /* console.log({carId: props.navigation.state.params.car_info.id,
      driverId: userInfo.id,
      discount: 0,
      pickUpLocation:"Strauss",
      surcharge: 0.00,
      sendMessage: true,
      isSplit: false,
      pickupDate: "2020-04-15 09:00", //moment(startDateTime).format('YYYY-MM-DD HH:MM'),
      preferredCar: props.navigation.state.params.car_info.model}) */

        if( flag_redired == 2 ) {

          props.navigation.navigate('CarPayment',  { 
            paymentype: paymentType, 
            type: props.navigation.state.params.type,
            price: props.navigation.state.params.price,
            startDateTime: startDateTime,
            carId: props.navigation.state.params.car_info.id,
            driverId: userInfo.id,
            agreementType:props.navigation.state.params.type,
            pickupDate: moment(startDateTime).format('YYYY-MM-DD HH:MM'),
            preferredCar: props.navigation.state.params.car_info.model  
          });
          
        } else {

            let input = {
                carId: props.navigation.state.params.car_info.id,
                driverId: userInfo.id,
                discount: 0,
                pickUpLocation:"Strauss",
                surcharge: 0.00,
                sendMessage: false,
                isSplit: false,
                agreementType:props.navigation.state.params.type,
                pickupDate: moment(startDateTime).format('YYYY-MM-DD HH:MM'),
                preferredCar: props.navigation.state.params.car_info.model
            }; 
              
            setState({ ...state,  isLoading: true });

            console.log("return car val==>", input);
            AcreateReservation({
              variables: input
            }).then((response) => {
              console.log("return car reponse==>", response.data.createReservation);
              if (response.data.createReservation.ok) {
                AsyncStorage.setItem('isReservation', 'true');
                setState({ ...state, isLoading: false, isZelle: false, isVanila: false, isModalPayLaterSuccess:false }); 
                props.navigation.navigate('Questionnaire',{ ptype:(( paymentTypeIndexValue == 2 ) ? "Zelle" : "Remote Cache"), agreementType:props.navigation.state.params.type });

              } else if(response.data.createReservation.errors && response.data.createReservation.errors.length > 0) {
                if(response.data.createReservation.errors[0].messages && response.data.createReservation.errors[0].messages.length > 0){ 
                  console.log("reservation---",response.data.createReservation.errors);
                  setState({ ...state, isLoading: false, isModalPayLaterSuccess: false, error_global: response.data.createReservation.errors[0].messages[0] })
                } 
              } 
            }).catch((error) => {
              setState({ ...state, isLoading: false, isModalPayLaterSuccess: false, error_global: "Your reservation was not created. Please try again later." })
              console.log('AcreateReservation======', error)
            });

      }
  }

  const AppointmentAddFun = async (onsched_customer_id) => {
    try {
      const value = {
        serviceId: serviceId,
        resourceId: resourceId,
        customerId: onsched_customer_id,
        bookedBy: userInfo.email,
        startDateTime: startDateTime,
        endDateTime: endDateTime,
      }
      console.log('value', value)
      const result = await AppointmentAddApi(apiMode, token, value);
      AppointmentBookFun(result.id)
      console.log('AppointmentAddFun', result)
    } catch (error) {
      console.log('AppointmentAddFunError', error)
    }
  }

  const AppointmentBookFun = async (appointmentId) => {
    try {
      const result = await AvailabilityBookApi(apiMode, token, appointmentId, userInfo);
      console.log('AppointmentBookFun', result)
      props.navigation.navigate('CarPayment', { price: props.navigation.state.params.price })
    } catch (error) {
      console.log('AppointmentAddFunError', error)
    }
  }

  const cancelFun = () => {
    setState({ ...state, selectedTime: '', error_time: '', error_payment: "" })
    props.navigation.goBack()
  }

  let radio_props = [
    { label: "Pay now using Credit Card " + '          ', value: 1 },
    { label: "Pay later using Zelle " + '          ', value: 2 },
    { label: "Pay later using Remote Cash " + '          ', value: 5 },
  ];
  const radioTypeButtonChange = (value) => {

    
    setState({ ...state, paymentTypeIndexValue:value,error_payment: "" });

    /*console.log("value--->> ",value);
    if (selectedTime == '') {
      setState({ ...state, paymentTypeIndex:-1, error_time: "Please choose time" })
    } else{
      if (value == 1) { 
        setState({ ...state, isModalPayLater: false, isZelle: false, isCard: false, paymentTypeIndex: 0, paymentType: "creditcard" })
      } if (value == 2) { 
        setState({ ...state, paymentTypeIndex: -1, isZelle: true }); 
      }
    } */
  }

  let paylater_methods_props = [
    { label: "Zelle " + '   ', value: 2 }, 
    { label: "Remote Cash " + '   ', value: 5 },
  ];

  const radioPayLetterMethodsButtonChange = (value) => {
    if (value == 2) {
      setState({ ...state, isZelle: true });  
    } else if (value == 5) {
      setState({ ...state, isVanila: true }); 
    }
  }


  const zellevanilapayment_ok = () => { 
    if( selectedTime != '' ) {
      setState({...state,isVanila:false,isZelle:false,isModalPayLaterSuccess:true});
    } else {
      setState({...state,isVanila:false,isZelle:false,isModalPayLaterSuccess:false});
    }
  }
  
  const closePaylaterOption = () => {

    if( selectedTime != '' ) {
        reservationFun(1); 
    } else {
      setState({...state,isVanila:false,isZelle:false,isModalPayLaterSuccess:false});
    }

  }

  const hidemakePayment = () => {
    setState({ ...state, isModalPopup: false, isZelle: false, isVanila: false  });  
  }

  const makePayment = () => { 
    if( paymentTypeIndexValue == 2 ) {
      setState({ ...state,  isModalPopup: false, isZelle: true });
    } else if( paymentTypeIndexValue == 5 ) {
      setState({ ...state, isModalPopup: false, isVanila: true });
    } 
  }

  return (
    <>
      {isLoading ?
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> :
        <SafeAreaView style={styles.container}> 
        <ScrollView style={styles.container}> 
          <View style={styles.calenderView}>
            <Text style={styles.calenderText}>Choose Date</Text>
            <View style={styles.calenderDateView}>
              <TouchableHighlight underlayColor='' onPress={() => openCalender()} style={styles.calenderDate}><Text style={styles.calenderDateText}>{displayDate}</Text></TouchableHighlight>
              <TouchableHighlight underlayColor='' onPress={() => openCalender()} style={styles.calenderIcon}><Icon color={Colors.color0} size={20} name="calendar" /></TouchableHighlight>
            </View>
          </View>
          <View style={calenderShow ? styles.calenderShow : styles.calenderHide}>
            <CalendarPicker
              minDate={minDate}
              maxDate={maxDate}
              disabledDates={(date) => {
                //console.log('disabledDates', date.day())
                let type = date.day();
                //hide Saturday
                return (type == 6) ? date : '';
              }}
              selectedStartDate={startDate}
              onDateChange={(value) => onDateChange(value)}
            />
          </View>
          <View style={styles.calenderTimeView}>
            {error_global != '' && <Text style={styles.errorText}>{error_global}</Text>}
            <Text style={styles.calenderTimeText}>Choose Time</Text> 
            <View style={styles.calenderTimeBoxWrap}>
              {isTimeLoading ?
                <View>
                  <ActivityIndicator size="small" style={{ padding: 60 }} />
                </View> :
                (isAvailable ? renderTime() : <Text style={styles.noTimeText}>No Availability - No available time slots remaining</Text>)}
            </View>
            {error_time != '' && <Text style={styles.errorText}>{error_time}</Text>}
          </View>

          <View style={styles.calenderView}>
            <Text style={styles.calenderText}>Payment Methods</Text>
          </View>
          <Text style={[styles.security_deposit_message, global_styles.opensans_regular]} >You are going to pay ${props.navigation.state.params.price} amount as Security deposit.</Text>
          <View style={styles.payment_method_items}> 
            <RadioForm
              animation={false}
              radio_props={radio_props}
              initial={paymentTypeIndex}
              buttonColor={'#ccc'}
              selectedButtonColor={Colors.color0}
              style={styles.radio_button}
              formHorizontal={false}
              buttonSize={10}
              buttonOuterSize={20}
              buttonWrapStyle={{ marginLeft: 10, marginBottom: 10, }}
              onValueChange={(value) => radioTypeButtonChange(value)}
              onPress={(value) => radioTypeButtonChange(value)}
            />
             {error_payment != '' && <Text style={[styles.errorText, { marginBottom: 10 }]}>{error_payment}</Text>}
          </View>

          {isModalPayLater && <>
          <View style={styles.calenderView}>
              <Text style={styles.calenderText}>Pay Later Methods</Text>
          </View>
           <View style={styles.payment_method_items}>
            <RadioForm
              animation={false}
              radio_props={paylater_methods_props}
              initial={5}
              key={5}
              index={5}
              buttonColor={'#ccc'}
              selectedButtonColor={Colors.color0}
              style={styles.radio_button}
              formHorizontal={false}
              buttonSize={10}
              buttonOuterSize={20}
              buttonWrapStyle={{ marginLeft: 10, marginBottom: 10, }}
              onValueChange={(value) => radioPayLetterMethodsButtonChange(value)}
              onPress={(value) => radioPayLetterMethodsButtonChange(value)}
            /> 
            {paylater_method_error != '' && <Text style={[styles.errorText, { marginBottom: 10 }]}>{paylater_method_error}</Text>}
          </View></>}

          <Modal isVisible={(isVanila || isZelle || isModalPopup || isModalPayLaterSuccess)} >
            <View style={modal_style.modal}>
              {(isZelle || isVanila) && <>
                      {isZelle &&
                        <View style={styles.modal_pay_later_wrap}>
                          <Text style={[styles.pay_later_method_title, global_styles.opensans_semibold]}>Zelle</Text>
                          <View style={[styles.WrapText, styles.paddingBottom]}>
                            <Text style={[styles.pay_later_method_desc, global_styles.opensans_regular]}>
                              Our payment email address is </Text>
                            <TouchableHighlight underlayColor="" onPress={() => Linking.openURL('mailto:billing@joinbuggy.com')}>
                              <Text style={[styles.pay_later_method_desc_link, global_styles.opensans_regular]}>billing@joinbuggy.com</Text>
                            </TouchableHighlight>
                          </View>
                          <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.opensans_regular]}>
                            Be sure to include your TLC License number in the memo, before sending the payment.
                          </Text>
                          <View style={[styles.WrapText, styles.paddingBottom]}>
                            <Text style={[styles.pay_later_method_desc, global_styles.opensans_regular]}>
                              For more information on how it works: </Text>
                            <TouchableHighlight underlayColor="" style={{ display: "flex" }} onPress={() => Linking.openURL('https://goo.gl/YPuVs5')}><Text style={[styles.pay_later_method_desc_link, global_styles.opensans_regular]}>Click here</Text></TouchableHighlight>
                          </View>
                          <Text style={[styles.pay_later_method_desc, global_styles.opensans_regular]}>Please feel free to reach out to us if you need any assistance.
                              </Text>
                        </View>
                      }
                      {isVanila &&
                        <View style={styles.modal_pay_later_wrap}>
                          <Text style={[styles.pay_later_method_title, global_styles.opensans_semibold]}>Vanilla</Text>
                          <View style={[styles.webview_desc, styles.paddingBottom]}>
                            { ( pk && pk!="" ) && 
                              <WebView
                                source={{ uri: "https://www.achpsac.com/buggy/barcode/"+pk+"?C=B34*5" }}
                                style={{borderWidth:1, marginTop: 20 }} 
                              />
                            }
                          </View>
                          <View style={styles.WrapText}>
                            <TouchableHighlight underlayColor="" style={styles.LinkTextWrap} onPress={() => Linking.openURL('https://www.joinbuggy.com/remote-cash')}>
                              <Text style={[styles.pay_later_method_desc_link, global_styles.opensans_regular]}>Click here</Text>
                            </TouchableHighlight>
                            <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.opensans_regular]}> for more details on how it works.</Text>
                          </View>
                          <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.opensans_regular]}>
                            In brief, you can make payments for a $2 fee and pay at any participating stores.
                          </Text>
                          <Text style={[styles.pay_later_method_desc, global_styles.opensans_regular]}>
                            Please contact us if you have any questions. Your feedback is appreciated!
                          </Text>
                        </View>
                      }
                      <View style={modal_style.modal_textbuttonWrap}>
                        <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => zellevanilapayment_ok() }>
                          <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                        </TouchableHighlight>
                      </View> 
                      </>
              }
              {isModalPayLaterSuccess && <>
              
                <Icon3 name="checkcircleo" style={modal_style.modal_check} />
                <View>
                  <Text style={modal_style.modalText}>Reservation has been done.</Text>
                  <View style={modal_style.modal_textbuttonWrap}>
                    <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal}  onPress={() => closePaylaterOption()}>
                      <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                    </TouchableHighlight>
                  </View>
                </View>

              </>}

              {isModalPopup && <>
                  <View>
                    <Text style={modal_style.modalText}>{ ( paymentTypeIndexValue == 2 ) ? "You are paying $"+props.navigation.state.params.price+" for your security deposit using Zelle." : "You are paying $"+props.navigation.state.params.price+" for your security deposit using Remote Cash." }</Text>
                    <View style={modal_style.modal_textbuttonWrap}>
                      <TouchableHighlight underlayColor=""  style={modal_style.upload_document_modal}  onPress={() => makePayment(true)}>
                        <Text style={modal_style.upload_document_text_modal}>YES</Text>
                      </TouchableHighlight>
                      <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hidemakePayment(false)}>
                        <Text style={modal_style.upload_document_text_modal}>NO</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
              </>}


            </View>
          </Modal>
        
                
          <View style={styles.buttonView}>
            <TouchableHighlight underlayColor='' style={styles.checkoutButton} onPress={() => { checkoutFun() }}>
              <Text style={styles.checkoutButtonText}>CHECKOUT</Text>
            </TouchableHighlight>
            <TouchableHighlight underlayColor='' style={styles.cancelButton} onPress={() => { cancelFun() }}>
              <Text style={styles.cancelButtonText}>CANCEL</Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </SafeAreaView>
      }
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  calenderView: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    backgroundColor: '#EEE',
  },
  calenderText: {
    flex: 1,
    fontSize: 16,
    paddingVertical: 8,
  },
  calenderDateView: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 8,
    backgroundColor: "#FFF"
  },
  calenderDate: {
    flex: 1,
  },
  calenderDateText: {
    fontSize: 16,
  },
  calenderIcon: {
    flexDirection: "row-reverse"
  },
  calenderShow: {
    zIndex: 10,
    display: "flex"
  },
  calenderHide: {
    display: "none"
  },
  calenderTimeView: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 8,
  },
  calenderTimeText: {
    fontSize: 16,
  },
  errorText: {
    color: "red",
    paddingVertical: 5
  },
  noTimeText: {
    paddingVertical: 10
  },
  calenderTimeBoxWrap: {
    paddingVertical: 10,
  },
  calenderTimeBoxView: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  calenderTimeBoxText: {
    flex: 1,
    fontSize: 14,
    marginBottom: 4
  },
  calenderTimeBoxWrapRow: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  calenderTimeBoxRow: {
    width: "22%",
    textAlign: "center",
    marginVertical: "1.5%",
    marginHorizontal: "1.5%",
    borderWidth: 1,
    borderColor: "#CCCCCC",
  },
  calenderTimeBoxRowSelected: {
    width: "22%",
    textAlign: "center",
    marginVertical: "1.5%",
    marginHorizontal: "1.5%",
    borderWidth: 1,
    borderColor: Colors.color0,
  },
  calenderTimeBoxRowText: {
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    color: "#575757",
  },
  calenderTimeBoxRowTextSelected: {
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    color: Colors.color0,
  },
  buttonView: {
    flex: 1,
    flexDirection: "row-reverse",
    paddingHorizontal: 30,
  },
  checkoutButton: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    color: Colors.color0,
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  payment_method_items: {
    marginHorizontal: 15,
    marginVertical: 15,
  },
  WrapText: {
    flexDirection: "row",
  },
  LinkTextWrap: {
    flexDirection: "row-reverse",
  },
  paddingBottom: {
    paddingBottom: 10,
  },
  pay_later_method_title: {
    fontSize: 15,
    marginBottom: 15,
  },
  pay_later_method_desc: {
    fontSize: 12, 
  },
  webview_desc: { 
    height:200,
    borderWidth:1,
  },
  pay_later_method_desc_link: {
    fontSize: 12,
    color: "#0d73e0",
    textDecorationLine: "underline"
  },
  payment_amount_wrap:{
    paddingHorizontal:10,
    marginVertical:5
  },
  payment_amount:{
     marginVertical:5,
     flex:1,
     flexDirection:"row"
  },
  payment_amount_title:{
     fontSize:12,
     fontWeight:"bold"
  },
  payment_amount_text:{
    marginLeft:5,
    fontSize:12,
  },
  payment_amount_text_promocode:{
    fontSize:10,
    fontStyle:"italic",
    marginLeft:5,
    marginTop:2,
  },
  modal_pay_later_wrap: {
    paddingHorizontal: 0, 
    paddingTop: 20,
    marginBottom:10,
  },
  security_deposit_message:{
    paddingHorizontal:12,
    paddingVertical:12
  }
})
