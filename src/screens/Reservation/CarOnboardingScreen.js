import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, ActivityIndicator, SafeAreaView, StyleSheet, TouchableHighlight } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { CheckBox } from 'react-native-elements';
import RadioForm from 'react-native-simple-radio-button';
import Modal from "react-native-modal";
import { global_styles, modal_style } from '../constants/Style';
import { translate } from '../components/Language';
import Colors from '../constants/Colors';
import { CheckConnectivity } from './NetInfo';
import { runPickup } from '../graphql/runPickup';
import { driverDetail } from '../graphql/driver';
import AsyncStorage from '@react-native-community/async-storage';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { driverUpdatePayment } from '../graphql/driverUpdatePayment';

var radio_props = [
  { label: translate("yes"), value: 1 },
  { label: translate("no"), value: 2 },
];

export default function CarOnboardingScreen(props) {

  const [state, setState] = useState({
    isModal: false,
    isModalEzPassNo: false,
    check_boxes: [{
      id: 1,
      title: 'Uber',
      checked: false,
    },
    {
      id: 2,
      title: 'Lyft',
      checked: false,
    },
    {
      id: 3,
      title: 'VIA',
      checked: false,
    },
    {
      id: 4,
      title: 'Other',
      checked: false,
    }],
    radioValue: 0,
    radioValue1: 0,
    show_text_box: false,
    other_name: '',
    error_ez_pass:'',
    error_ez_pass_no:'',
    isLoading:false,
    error_company:'',
    error_other_name: '',
    error_global:''
  })
  const { isModal,isModalEzPassNo,isLoading, error_global, check_boxes, radioValue, radioValue1, show_text_box, other_name, error_ez_pass,error_ez_pass_no, error_company,error_other_name } = state;
  const [DriverUpdatePayment] = useMutation(driverUpdatePayment);
  const [RunPickup] = useMutation(runPickup);

  /* get reservation id */
  const [reservationId, setReservationId] = useState("");
  const [driverId, setDriverId] = useState("");
  const setuserid = async () => {
    const user = await AsyncStorage.getItem('user');
    console.log(JSON.parse(user).id);
    setDriverId(JSON.parse(user).id);
  }
  useEffect(() => {
    setuserid();
  }, []);
  const { data: drivar_data, loading: drivar_data_loading, error: drivar_data_error } = useQuery(driverDetail, {
    variables: { id: driverId },
    fetchPolicy: "network-only",
    skip: (driverId == '')
  });

  useEffect(() => {

    if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver && drivar_data.driver.reservationDriver) {
      if (drivar_data.driver.reservationDriver.hasOwnProperty('edges') && drivar_data.driver.reservationDriver.edges.length > 0) {
        let reservation_flag = '';
        drivar_data.driver.reservationDriver.edges.map((item) => {
          if (item.node.status == "Open") {
            reservation_flag = item.node.id;
          }
        });
        setReservationId(reservation_flag);
      }
    }

  }, [drivar_data]);
  /* end get reservation id */

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.isModal = true;
      console.log(props.screenProps)
      return;
    }
  }
  const onCheck = (id) => {
    checkNet();
    let changedIndex = check_boxes.findIndex((cb) => cb.id === id);
    check_boxes[changedIndex].checked = !check_boxes[changedIndex].checked;
    let show_text_box1 = false;
    if (id == 4 && check_boxes[changedIndex].checked)
      show_text_box1 = true;
    else if (id == 4 && !check_boxes[changedIndex].checked)
      show_text_box1 = false;
    else if (show_text_box)
      show_text_box1 = true;   
    setState({ ...state, check_boxes, show_text_box: show_text_box1,error_company:'' });
  }
  const radioButtonChange = (value) => {
    setState({ ...state, radioValue: value,error_ez_pass:'' });
  }
  const radioButtonChange1 = (value) => {
    setState({ ...state, radioValue1: value,error_ez_pass_no:'' });
  }
  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value,error_other_name:'' });
  }
  const nextFun = () => {
    let check = check_boxes.findIndex((cb) => cb.checked === true);
    
    if(check<0){
      setState({ ...state, error_company: 'Please provide which company do you drive for.' });
      return;
    }else if(show_text_box==true && other_name==''){      
      setState({ ...state, error_other_name: 'Please provide company name.' });
      return;
    }
    if( radioValue == 0 ) {
      setState({ ...state, error_ez_pass: 'Please provide your input for EZ Pass.' });
      return;
    } else {

      if (radioValue == 1) {
        setState({ ...state, isModal: true });
      } else {
        if( radioValue1 == 0 ) {
          setState({ ...state, error_ez_pass_no: 'Please select radio option.' });
          return;
        }   
        if (radioValue1 == 1) {

          setState({ ...state,isModalEzPassNo:true });
         
          //props.navigation.navigate('OnBoardingPassPayment', { frompage: "caronboarding", price: 45 })
        } else {
          setState({ ...state, isModal: true });
        }
      }

    }

  }

  const runPickupAPI = (redirectpage, param_query) => {

    if(redirectpage=="OnBoardingPassPayment"){

       setState({ ...state, isModal:false, isModalEzPassNo:false, isLoading: false });
       
       let checked_text = "";
       check_boxes.map((item_check)=>{
            if(item_check.checked && item_check.title != "Other"){
              checked_text += item_check.title+", "; 
            }
        });
        check_boxes.map((item_check)=>{
          if(item_check.checked && item_check.title == "Other"){
            checked_text += other_name+" "; 
          }
      });

      props.navigation.navigate("OnBoardingPassPayment", { price: 45, driverId:driverId, frompage: "caronboarding", preferredBase:checked_text, agreementType:props.navigation.state.params.agreementType, reservationId:reservationId, isDriverGettingEzpass:radioValue  });
      return;

    }
  
    if (reservationId != "") {

      AsyncStorage.setItem('isReservation', 'true');
      setState({ ...state, isModal:false, isModalEzPassNo:false, isLoading: true });
    
      let input = {
        paidDepositCash: 0,
        paidDepositCc: 0,
        isDriverGettingEzpass: (radioValue == 1 ? true : false),
        reservationId: reservationId,
        agreementType: props.navigation.state.params.agreementType,
        preferredPaymentMethod: "card"
      }
      console.log("input RunPickup---", input);
      RunPickup({
        variables: { input }
      }).then((response) => {
        console.log("response============>", response);
        if (!response.data.runPickup.ok) {
          console.log("Error response: ", response.data.runPickup.errors);
          let error_gob = "Car booking has not been completed successfully. Please try again.";
          if( response.data.runPickup.errors.length > 0 && response.data.runPickup.errors[0].messages && response.data.runPickup.errors[0].messages.length > 0){ 
            if(response.data.runPickup.errors[0].messages[0]!=""){
              error_gob = response.data.runPickup.errors[0].messages[0];
            }
          }
          setState({ ...state, error_global:error_gob, isLoading: false, isModalEzPassNo:false, isModal: false }); 
        } else {
          
          // update driver
          let checked_text = "";
           check_boxes.map((item_check)=>{
                if(item_check.checked && item_check.title != "Other"){
                  checked_text += item_check.title+", "; 
                }
            });
            check_boxes.map((item_check)=>{
              if(item_check.checked && item_check.title == "Other"){
                checked_text += other_name+" "; 
              }
          });

          if(driverId=="" || checked_text==""){
            setState({ ...state, isLoading: false, isModalEzPassNo:false, isModal: false });
            props.navigation.navigate(redirectpage, param_query);
            return;
          }

          let input = {
            id: driverId,
            preferredBase: checked_text
          } 
          console.log("3. DriverUpdate Input Parameter:", input);
          DriverUpdatePayment({
            variables: { input }
          }).then((response) => { 
              console.log("DriverUpdatePayment Response ============>",response.data);
              if(response.data.updateDriver.errors){ 
                console.log("DriverUpdatePayment ERROR  ============>",response.data.updateDriver.errors);
                if( response.data.updateDriver.errors.length > 0 && response.data.updateDriver.errors[0].messages && response.data.updateDriver.errors[0].messages.length > 0){ 
                    if(response.data.updateDriver.errors[0].messages[0]==""){
                       setState({ ...state, isLoading: false, isModalEzPassNo:false, isModal: false });
                       props.navigation.navigate(redirectpage, param_query);
                    } else {
                      setState({ ...state, isLoading:false,  error_global:response.data.updateDriver.errors[0].messages[0] });
                    }
                } else { 
                  setState({ ...state, isLoading: false, isModalEzPassNo:false, isModal: false });
                  props.navigation.navigate(redirectpage, param_query);
                }
              } else { 
                setState({ ...state, isLoading: false, isModalEzPassNo:false, isModal: false });
                props.navigation.navigate(redirectpage, param_query);
              } 
          }); 
          //////////////////////////////////////////////
 
        }
      }).catch((error) => { 
        console.log("error---",error);
        setState({ ...state,error_global:"Car booking has not been completed successfully, Please try again.", isModalEzPassNo:false, isLoading: false, isModal: false }); 
      });

    } else {
      setState({ ...state, error_global:"Car booking has not been completed successfully. Please try again", isModalEzPassNo:false, isLoading: false, isModal: false }); 
    }

  }

  const ok_model = () => {
    let params = {};
    if(isModalEzPassNo){
      runPickupAPI('OnBoardingPassPayment', { frompage: "caronboarding", price: 45 });
    } else {
      runPickupAPI('AccountList', params);
    }
  }
  return ( 
    <>{isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> }  
       <Modal isVisible={isModal || isModalEzPassNo}>
        <View style={modal_style.modal}>
          <View>
            {radioValue1 == 2 && <Text style={[modal_style.modalText]}>Please make sure to purchase one.</Text>}
            <Text style={[modal_style.modalText]}>Make sure to set your account on auto replenishment</Text>
            <Text style={[modal_style.modalText, { textAlign: "left" }]}>Please note: Buggy charges 5$ processing fee for each toll we paying on your behalf.</Text>
            <View style={modal_style.modal_textbuttonWrap}>
              <TouchableHighlight underlayColor='' style={modal_style.upload_document_modal} onPress={() => { ok_model(); }}>
                <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Modal>
        <SafeAreaView style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.info_main}>
            <View style={styles.heading_title}>
              <Text style={[styles.heading_title_text, global_styles.opensans_bold]}>Which Company do you drive for?</Text>
            </View>
            <View style={styles.content}>
              <View style={styles.content_item}>
                {
                  check_boxes.map((cb) => {
                    return (
                      <CheckBox
                        textStyle={[styles.check_boxe_text, global_styles.opensans_regular]}
                        key={cb.id}
                        title={cb.title}
                        checked={cb.checked}
                        onPress={() => onCheck(cb.id)}
                      />
                    )
                  })
                }
                {error_company != '' && <Text style={[styles.errorText, { marginBottom: 10 }]}>{error_company}</Text>}
                {show_text_box && <View style={styles.inputWrap}>
                  <TextField
                    lineWidth={1}
                    contentInset={{ top: 0, left: 0, right: 0, lable: 0, input: 8 }}
                    containerStyle={styles.input}
                    style={styles.input}
                    placeholder={"Enter Company Name"}
                    value={other_name}
                    onChangeText={(text) => updateTextInput(text, 'other_name')}
                    error={error_other_name}
                  />
                </View>}
              </View>
            </View>
            <View style={styles.heading_title}>
              <Text style={[styles.heading_title_text, global_styles.opensans_bold]}>Do you have an EZ Pass?</Text>
            </View>
            <View style={styles.content}>
              <RadioForm
                animation={false}
                radio_props={radio_props}
                initial={""}
                buttonSize={14}
                buttonColor={'#ccc'}
                selectedButtonColor={Colors.color0}
                radioStyle={[styles.content_item, styles.radio_item]}
                labelStyle={[styles.radio_text, global_styles.opensans_regular]}
                onValueChange={(value) => radioButtonChange(value)}
                onPress={(value) => radioButtonChange(value)}
              />
            </View>             
            {error_ez_pass != '' && <Text style={[styles.errorText, { marginBottom: 10 }]}>{error_ez_pass}</Text>}

            {radioValue == 2 && <><View style={styles.heading_title}>
              <Text style={[styles.heading_title_text, global_styles.opensans_bold]}>Would you like to buy one from Buggy for $45?</Text>
            </View>
              <View style={styles.content}>
                <RadioForm
                  animation={false}
                  radio_props={radio_props}
                  initial={""}
                  buttonSize={14}
                  buttonColor={'#ccc'}
                  selectedButtonColor={Colors.color0}
                  radioStyle={[styles.content_item, styles.radio_item]}
                  labelStyle={[styles.radio_text, global_styles.opensans_regular]}
                  onValueChange={(value) => radioButtonChange1(value)}
                  onPress={(value) => radioButtonChange1(value)}
                />
              </View>
              {error_ez_pass_no != '' && <Text style={[styles.errorText, { marginBottom: 10 }]}>{error_ez_pass_no}</Text>}
              </>}
          </View>
          
          {error_global != '' && <Text style={[styles.errorText, { marginBottom: 10 }]}>{error_global}</Text>}

          <View style={styles.buttonView}>
            <TouchableHighlight underlayColor='' style={styles.checkoutButton} onPress={() => { nextFun() }}>
              <Text style={styles.checkoutButtonText}>NEXT</Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </SafeAreaView>
      </> 
  );
}
const styles = StyleSheet.create({
  info_main: {
    marginVertical: 20,
  },
  heading_title: {
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: "#fafafa",
  },
  heading_title_text: {
    fontSize: 14
  },
  content: {
    marginVertical: 5,
    marginHorizontal: 20
  },
  content_item: {
    paddingVertical: 5, 
  },
  errorText: {
    color:"red",
    paddingHorizontal:15, 
    fontSize:12,
    marginVertical:5,
  },
  radio_item: {
    paddingHorizontal: 20,
  },
  check_boxe_text: {
    fontSize: 14,
  },
  radio_text: {
    fontSize: 14,
  },
  inputWrap: {
    flexDirection: "row",
    marginBottom: 4,
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },
  buttonView: {
    flex: 1,
    flexDirection: "row-reverse",
    paddingHorizontal: 30,
    paddingBottom: 30
  },
  checkoutButton: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    color: Colors.color0,
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  }
});