import React, { useState } from 'react';
import { ScrollView,  Keyboard, Text, KeyboardAvoidingView, SafeAreaView, TouchableWithoutFeedback } from 'react-native';
import {
  ActivityIndicator, View
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import StripeScreen from './StripeScreen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { createReservation } from '../graphql/createReservation';
import { useMutation } from '@apollo/react-hooks';
import { global_styles,modal_style } from '../constants/Style';

export default function CarPaymentScreen(props) {
  const [state, setState] = useState({
    payamount: props.navigation.state.params.price,
    from: "sched",
    error_global:"",
    isLoading: false,
    paymenttype:props.navigation.state.params.paymentype
  });

  const [AcreateReservation] = useMutation(createReservation);
  const { payamount, isLoading, error_global, paymenttype, from } = state;

  const navigateToScreen = (screename) => {
    if( screename == 'Questionnaire' ) {
      setState({ ...state, isLoading:true });
      // create reservation data 
      let input = {
        carId: props.navigation.state.params.carId,
        driverId: props.navigation.state.params.driverId,
        discount: 0,
        pickUpLocation:"Strauss",
        surcharge: 0.00,
        sendMessage: false,
        isSplit: false,
        agreementType: props.navigation.state.params.agreementType,
        pickupDate: props.navigation.state.params.pickupDate,
        preferredCar: props.navigation.state.params.preferredCar,
     }; 

     console.log("return car val==>", input);
      AcreateReservation({
        variables: input
      }).then((response) => {
        console.log("return car reponse==>", response.data.createReservation);
        if (response.data.createReservation.ok) {
          AsyncStorage.setItem('isReservation', 'true');
          setState({ ...state, isLoading:false });
          props.navigation.navigate("Questionnaire",{ptype:"CreditCard", agreementType:props.navigation.state.params.agreementType});
        } else if(response.data.createReservation.errors && response.data.createReservation.errors.length > 0) {
          if(response.data.createReservation.errors[0].messages && response.data.createReservation.errors[0].messages.length > 0){ 
            console.log("reservation---",response.data.createReservation.errors);
            setState({ ...state, isLoading:false, error_global: response.data.createReservation.errors[0].messages[0] })
          } 
        }  
      }).catch((error) => {
        setState({ ...state, isLoading:false,error_global: "Your reservation was not created. Please try again later." });
        console.log('AcreateReservation======', error); 
      }); 
      
    }
    else
      props.navigation.navigate((screename!="")?screename:"CarList");
  }
  const navigateToSuccess = () => {
    props.navigation.navigate("CarPaymentSuccess");
  }

  return (
    <>
      { isLoading ?
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>:
        <KeyboardAwareScrollView>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <>
            { error_global != "" && <Text style={{ color: "red", fontSize:14, paddingVertical:10, paddingHorizontal:15 }} > {error_global} </Text> }
              <StripeScreen {...props} paymenttype={paymenttype} navigateToSuccess={navigateToSuccess} navigateToScreen={navigateToScreen} navigation={props.navigation} from={from} amount={payamount} />
            </>
          </TouchableWithoutFeedback>
        </KeyboardAwareScrollView>
        }
    </>
  );
}
 