import React, { useState, useEffect, useContext } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { StyleSheet, View, Text,  TextInput, Button, Dimensions, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'; 
import { translate } from '../../components/Language';
import Icon2 from 'react-native-vector-icons/Feather';
import { global_styles } from '../../constants/Style';
import moment from 'moment';
import Colors from '../../constants/Colors';
import { CheckConnectivity } from '../Helper/NetInfo';
import RadioForm from 'react-native-simple-radio-button';
import ReservationPayScreen from './reservationPayScreen'; 
import { driverBalance } from '../../graphql/driverPayAmount';
import {useQuery } from '@apollo/react-hooks';
const win = Dimensions.get('window');

export default function ReservationPay(props) {
    const [user, setUser] = useState({});
    const [isLoading, setIsLoding] = useState(true);
    const [driverId, setDriverId] = useState("");
    const [initReload, setInitReload] = useState(false);

    const [state, setState] = useState({
        userInfo: {},
        amount: 0,
        payamount: 0,
        selected_radio_option:0,
    });

    const { userInfo, amount,selected_radio_option, payamount } = state

    // Check internet connection
    const checkNet = async () => {
        let netState = await CheckConnectivity();
        if (!netState) {
            props.screenProps.setNetModal(true)
            return;
        }
    }

    // Setting up user detail
    const setuserid = async () => {

        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);  
        setDriverId(user.id);
        setUser(user);
        setInitReload(!initReload);  
    }

    // Set the driver id from the storage
    useEffect(() => {

        checkNet();
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            setuserid();
        }

        const navFocusListener = props.navigation.addListener('didFocus', () => {
            setuserid();
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);


    
  ///////////// Current Charge ///////////////
  const { data: drivar_data, loading: drivar_data_loading, error: drivar_data_error } = useQuery(driverBalance, {
    variables: { id: driverId },
    fetchPolicy: "network-only",
    skip: driverId == ''
  }); 
  useEffect(() => {
    if (drivar_data) {

      if (drivar_data && drivar_data.hasOwnProperty('driver')) {

        let current_amount = 0; 
        if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentCharge != null && drivar_data.driver.currentCharge !=  "" && drivar_data.driver.currentCharge > 0) { 
            current_amount = drivar_data.driver.currentCharge;
        }
         if(console.log(props.navigation.state.params.price)){
            setState({ ...state, amount:current_amount});
            setIsLoding(false);
            setDriverId(""); 
         }
         else{
        setState({ ...state, amount:current_amount});
        setIsLoding(false);
        setDriverId("");
         }
        
      }
    }
  }, [drivar_data]);
  ///////////// End Current Charge //////////////


    let card_options = [
        { label: "Total outstanding amount " + '$' + amount, value: 0 },
        { label: "Other amount " + '          ', value: 1 },
    ];

    const radioChangeCard = (value, index) => {
        setState({ ...state, selected_radio_option: value });
        setInitReload(!initReload);
    } 

    // Navigate to Account List stack navigator
    const navigateToScreen = (screename) => {
         props.navigation.navigate((screename!="")?screename:"BillingPage");
    }
    // Navigate to Car Payment Screen
    const navigateToSuccess = () => {
        // console.log("Success payment");
        //setState({ ...state, payamount: "" });
        //props.navigation.navigate("CarPaymentSuccess");
    }
    const navigateToCoupon = () => {
        // props.navigation.navigate("Coupon");
    }
     
    return (
        <View style={styles.main_container}>
            <KeyboardAwareScrollView  >
                <>
                    {isLoading ? 
                        <ActivityIndicator size="large" style={{ padding: 60 }} />
                        :
                        <> 
                        <View style={styles.card_method_list}>
                          <View key={initReload} style={styles.container_payment_amount}>
                               <ReservationPayScreen initReload={initReload}  {...props} navigateToSuccess={navigateToSuccess} navigateToScreen={navigateToScreen} navigation={props.navigation} from={"dashboard"} amount={props.navigation.state.params.reservationDetail.deposit?parseInt(props.navigation.state.params.reservationDetail.deposit):amount} reservationDetail = {props.navigation.state.params.reservationDetail} />
                            </View>                   
                        </View>  
                     </>
                    }
                </>
            </KeyboardAwareScrollView>
        </View >
    );

}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1, 
    },
    card_method_list:{ 
       paddingHorizontal:0, 
       paddingBottom:100
    },
    
});
