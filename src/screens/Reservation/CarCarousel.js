import React, { useState, useEffect } from 'react';
import { View, Dimensions, Image, Text, StyleSheet, TouchableHighlight, FlatList } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Colors from '../../constants/Colors';
import _ from 'lodash';
import { CheckConnectivity } from '../Helper/NetInfo';
const win = Dimensions.get('window');
import Ionicons from 'react-native-vector-icons/Ionicons';
export default function CarCarousel(props) {
  const image = require('../../assets/images/2.jpeg');
  const [elRefs, setElRefs] = useState(0);
  const { item_mode, pageCount, item1, id } = props;
  const buttonColor = [styles.buttonColor0, styles.buttonColor1, styles.buttonColor2]
  const colorArr = [Colors.color0, Colors.color1, Colors.color2];
  const renderItem = ({ item, index }, pageCount, id) => {
    let image_src = item.node.genericImageLink;
    if (image_src == null) {
      image_src = image;
    } else {
      image_src = { uri: item.node.genericImageLink };
    }

    return (
      <View key={id + '_' + index} style={styles.item_wrap}>
        <View style={styles.image_item_wrap}>
          <View style={{
            flexDirection: "row", alignItems: "center", marginTop: 10,
            marginLeft: 10
          }}>
            <View style={{ alignItems: "center", flexDirection: "row", backgroundColor: "#fff", padding: 5, borderWidth: 0.5, borderColor: "#ECECEC", borderRadius: 5 }}>
              <Ionicons name="pricetag" style={{ fontSize: 15, marginRight: 5, color: "#DB9361" }} />
              <Text style={styles.car_uber}>Uber Promotions Available</Text>
            </View>
          </View>
          <View>
            {/* {pageCount > 1 && <Text style={styles.car_choose}>Choose from {pageCount} color</Text>} */}
            <Image style={styles.imageitem} source={image_src} />
          </View>
        </View>
        <View style={styles.item_right}>
          <TouchableHighlight underlayColor='' onPress={() => CarDetail(item.node.pk)}>
            <View style={{ flexDirection: "row", justifyContent: "space-between", flex: 1 }} >
              <Text style={styles.header_title}>{item.node.year} {item.node.model}</Text>
              <Text style={styles.car_price}>${item.node.weeklyCharge}/week</Text>
              <Text style={styles.car_type}></Text>
            </View>

          </TouchableHighlight>
        </View>
        <View style={styles.button_item_right}>
          <TouchableHighlight underlayColor='' style={styles.buttonColor0} onPress={() => CarDetail(item.node.pk)}>
            <Text style={[styles.car_see_detail]}>DETAILS</Text>
          </TouchableHighlight>
        </View>
      </View>

    );
  }
  const CarDetail = async (car_id) => {
        // let netState = await CheckConnectivity();
    // if (!netState) {
    //   props.screenProps.setNetModal(true)
    //   return;
    // }
    props.navigation.navigate('CardetailScreen', { car_id: car_id, time: Date.now() })
  }
  const borderLeft = [styles.borderLeft0, styles.borderLeft1, styles.borderLeft2]
  const dotColor = [Colors.color0, Colors.color1, Colors.color2]
  return (<><FlatList
    key={id}
    data={item1}
    style={{ marginTop: 10 }}
    // pagingEnabled={true}
    renderItem={({ item, index }) => {
      return renderItem({ item, index }, pageCount, id)
    }}
  // sliderWidth={win.width}
  // itemWidth={win.width}
  // containerCustomStyle={[styles.slider, borderLeft[item_mode]]}
  // contentContainerCustomStyle={styles.sliderContentContainer}
  // onSnapToItem={(index) => setElRefs(index)}
  />
    {/* <Pagination key={'p_' + id}
      dotsLength={pageCount}
      activeDotIndex={elRefs}
      containerStyle={styles.paginationContainer}
      dotColor={dotColor[item_mode]}
      dotContainerStyle={styles.paginationDotContainer}
      dotStyle={styles.paginationDot}
      inactiveDotColor={"#000"}
      inactiveDotOpacity={0.4}
      inactiveDotScale={0.6}
    /> */}
  </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9F3"
  },
  slider: {
    flex: 1,
    borderLeftWidth: 8,
    backgroundColor: "#FFFFFF",
    borderRadius: 8,
    marginVertical: 6,
    overflow: 'visible' // for custom animations
  },
  sliderContentContainer: {
    paddingVertical: 0 // for custom animation
  },
  borderLeft0: {
    borderLeftColor: Colors.color0,
  },
  borderLeft1: {
    borderLeftColor: Colors.color1,
  },
  borderLeft2: {
    borderLeftColor: Colors.color2,
  },
  paginationContainer: {
    marginTop: -57,
    marginBottom: -10,
  },
  paginationDotContainer: {
    marginHorizontal: 0,
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
  },
  item_wrap: {
    flex: 1,
    // flexDirection: 'row',
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.1,
    marginHorizontal: 20,
    backgroundColor: "white"

  },
  image_item_wrap: {
    flex: 1,
    backgroundColor: "#f9f9f4",
    height: 200,

  },
  item_right: {
    flex: 0.3,
    marginTop: 10,

  },
  imageitem: {
    width: "100%",
    height: 150,
    resizeMode: 'contain',
    marginTop: 5,
    marginBottom: 5

  },
  car_uber: {
    fontSize: 10,
    color: 'black',


  },
  car_choose: {
    fontSize: 12,
    color: '#707070',

  },
  car_price: {
    color: "#39AFD2",
    fontSize: 20,
    textAlign: "right",
  },
  header_title: {
    fontSize: 16,
    fontWeight: "bold",
    marginLeft: 10,
    flex: 0.89


  },
  button_item_right: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginTop: 10,
    marginBottom: 20

  },
  car_see_detail: {
    color: "#fff",
    paddingVertical: 8,
    paddingHorizontal: 16,
    justifyContent: "center",
    textAlign: "center",
    fontSize: 15,
    fontFamily: "lato-medium"
  },
  buttonColor0: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    backgroundColor: "#DB9360",
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 3,
    paddingBottom: 3
  },
  buttonColor1: {
    borderRadius: 5,
    backgroundColor: Colors.color1,
  },
  buttonColor2: {
    borderRadius: 5,
    backgroundColor: Colors.color2,
  },
});