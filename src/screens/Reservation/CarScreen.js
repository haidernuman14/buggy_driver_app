import React, { useEffect, useState } from 'react';
import { ActivityIndicator, View, Text, Dimensions, Image, FlatList, ScrollView, SafeAreaView, StyleSheet, TouchableHighlight, TouchableOpacity, Switch } from 'react-native';
import { useQuery } from '@apollo/react-hooks';
import RNPickerSelect from 'react-native-picker-select';
import { availableCars } from '../../graphql/availableCars';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { allAgreementTypes } from '../../graphql/allAgreementTypes';
import MultiSlider from "@ptomasroos/react-native-multi-slider"
import { SliderBox } from "react-native-image-slider-box";
import { global_styles } from '../../components/style';
// let sort_icon = require('../../assets/images/sort_icon.png');
let filter_icon = require('../../assets/images/Filter.png');
export default function CarScreen(props) {
  const [state, setState] = useState({
    driver : props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.userdata?props.navigation.state.params.userdata:null,
    isLoading: true,
    filterShow: false,
    multiSliderValue: [0, 3000],
    requiredDeposit: "",
  })
  const [carData, setCarData] = useState({
    allCars: [], //bos data
    availableCars : [], // 3 filters data
    imageEdges : [],
    filteredCars : [],
  })
  const [carFilters, setCarFilters] = useState({
    rentalTypes: [],
    carMakers: [],
    carYears: [],
    priceRanges: {min:0, max:0},
    carGroups: [],
    carLocations: [{ label: "445 Empire Blvd, Brooklyn, NY 11225", value: "Buggy Empire"},
    { label: "691 Burke Avenue, Bronx, NY 10467", value: "Bronx" }],
  })
  const [selectedFilters, setSelectedFilters] = useState({
    rentalType: "TLC Weekly",
    carMaker: [],
    carYear: [],
    carGroup: "",
    carLocation: "",
    priceRange: {min:0, max:0},
    sortBy: "asc",
  })
  const [carColors, setCarColors] = useState({
    colors: [{ label:"BK", value: "Black"},
    { label: "WHT", value: "White" },
    { label: "GY", value: "Grey" },
    { label: "BL", value: "Blue" },
    { label: "RD", value: "Red" },
    { label: "WHT", value: "White" },
    { label: "SIL", value: "Silver" },
    { label: "BK/C", value: "Black" },
    { label: "BR", value: "Brown" }
    ],
  })
  const { refetch } = useQuery(availableCars, {
    variables: {
      driverId: state && state.driver && state.driver.id ? state.driver.id : null,
      // year: selectedFilters.carYear ? parseInt(selectedFilters.carYear) : null,
      agreementType_Name_Iexact: selectedFilters.rentalType,
      first:5,
      },
    fetchPolicy: "network-only",
    onError: (error) => {console.log("error in all subs and trans",error), setState({...state, isLoading: false})},
    onCompleted: (data) => {
      let makerArray = []
      let yearArray = []
      let typeArray = []
      let availableCarsArray = []
      let priceRangeArray = {min:0, max:0};
      
      if(data && data.availableCars && data.availableCars.edges && data.availableCars.edges.length > 0){
        data.availableCars.edges.map((item, index) => {
          let carData = {node:null, minPrice:0, maxPrice:0}
          if (item.node.prices.edges.length > 0){
            // && item.node.isReady == true && item.node.carpicturesSet.edges.length > 0) {
            let maker = item.node.model.split(' ');
            if (!makerArray.includes(maker[0]))
              makerArray.push(maker[0])

            if (!yearArray.includes(item.node.year))
              yearArray.push(item.node.year)

            // if (!typeArray.includes({label: item.node.group, value: item.node.group}))
            //   typeArray.push({label: item.node.group, value: item.node.group})
            if (!(typeArray.find(obj => obj.label === item.node.group)) )
              typeArray.push({label: item.node.group, value: item.node.group})

            let carPrices = []
            item.node.prices.edges.map((item, index) => {
              carPrices.push(parseFloat(item.node.price))
            });
            
            carData.minPrice = Math.min(...carPrices)
            carData.maxPrice = Math.max(...carPrices)
            
            if(carData.minPrice < priceRangeArray.min || priceRangeArray.min == 0)
              priceRangeArray.min = carData.minPrice

            if(carData.minPrice > priceRangeArray.max || priceRangeArray.max == 0)
              priceRangeArray.max = carData.minPrice
            
            carData.node = item.node;
            availableCarsArray.push(carData)
        }
      })
      setCarFilters({...carFilters, carMakers: makerArray, carYears: yearArray, carGroups: typeArray, priceRanges: priceRangeArray })
      setSelectedFilters({...selectedFilters, priceRange: priceRangeArray})
      setCarData({...carData, allCars: data.availableCars, availableCars: availableCarsArray});
      }
      setState({...state, isLoading: false})
    }
  })
  const { } = useQuery(allAgreementTypes, {
    variables: { isActive: true },
    fetchPolicy: "network-only",
    onError: (error) => {setState({...state, isLoading: false})},
    onCompleted: (response) => {
      let agreementTypes = []
      if(response && response.allAgreementTypes && response.allAgreementTypes.edges.length > 0){
        response.allAgreementTypes.edges.map((item, index) => {
          if(item.node.name.includes("TLC") || item.node.name.includes("Empire Rental")){
            if (item.node.forTlc == true && state.driver && state.driver.isTlc == item.node.forTlc) {
              let agreementLabel = item.node.name.toLowerCase().includes("weekly") ? "Weekly" : item.node.name.toLowerCase().includes("weekend") ? "Weekend" : item.node.name.toLowerCase().includes("daily") ? "Daily" : item.node.name.toLowerCase().includes("monthly") ? "Monthly" : null;
              if(agreementLabel)
                agreementTypes.push({ label: agreementLabel.toUpperCase(), value: item.node.name })
            }
            else if (item.node.forTlc == false && state.driver && state.driver.isTlc == item.node.forTlc) {
              let agreementLabel = item.node.name.toLowerCase().includes("week") ? "Weekly" : item.node.name.toLowerCase().includes("daily") ? "Daily" : item.node.name.toLowerCase().includes("monthly") ? "Monthly" : null;
              if(agreementLabel)
                agreementTypes.push({ label: agreementLabel.toUpperCase(), value: item.node.name })
            }
          }
        })
        setCarFilters({...carFilters, rentalTypes: agreementTypes})
      }
    }
  })
  
  const clearCarFilter = () => {
    setSelectedFilters({...selectedFilters, carMaker: [], carYear: [], carGroup: "", carLocation: "", priceRange: {min:carFilters.priceRanges.min, max:carFilters.priceRanges.max}, sortBy: "asc"})
    setState({...state, filterShow: false})
  }

  // const applyCarFilter = ()=> {
  //   let filters =[]
  //   // console.log('availableCars',carData.availableCars.includes("nissan"));
  //   filters.push(carData.availableCars.includes("Hyundai Sonata"))
  //   console.log('filters',filters);

  // }

  useEffect (()=>{
    if(carData.availableCars && carData.availableCars.length > 0){
      let filteredCarsArray = []
      carData.availableCars.map((item, index)=>{
        let pass = false;
        if(selectedFilters.carMaker.length == 0 || (selectedFilters.carMaker.length > 0 && selectedFilters.carMaker.includes(item.node.model.split(" ")[0])) ){
          pass = true;
        }
        
        if(pass && (selectedFilters.carYear.length == 0 || (selectedFilters.carYear.length > 0 && selectedFilters.carYear.includes(item.node.year))) ){
          pass = true;
        } else {
          return true;
        }
        
        if(pass && (selectedFilters.carGroup == ""  || (selectedFilters.carGroup == item.node.group))){
          pass = true;
        } else {
          return true;
        }

        if(pass && (selectedFilters.carLocation == "" || selectedFilters.carLocation == item.node.location)){
          pass = true;
        } else {
          return true;
        }

        if(pass && (item.minPrice >= selectedFilters.priceRange.min && item.maxPrice <= selectedFilters.priceRange.max)){
          pass = true;
        } else {
          return true;
        }

        filteredCarsArray.push(item)
      })
      // console.log('filteredCarsArray',filteredCarsArray.sort(sortCarsData));
      if(selectedFilters.sortBy == "asc")
        setCarData({...carData, filteredCars: filteredCarsArray.sort(sortCarsData)});
      else if(selectedFilters.sortBy == "desc")
        setCarData({...carData, filteredCars: filteredCarsArray.sort(sortCarsData).reverse()});

    }
  },[selectedFilters])

  const sortCarsData =( a, b ) =>{
    if ( a.minPrice < b.minPrice ){
      return -1;
    }
    if ( a.minPrice > b.minPrice ){
      return 1;
    }
    return 0;
  }

  const handleFiltersSelection = (oldSelection = selectedFilters, fieldUpdate = "Year", fieldValue) => {
    if(fieldValue){
      if(fieldUpdate == "Year"){
        let years = selectedFilters.carYear;
        if (years && years.includes(fieldValue))
          years.splice(years.indexOf(fieldValue), 1);
        else
          years.push(fieldValue)
        setSelectedFilters({...selectedFilters, carYear: years})
      } else if(fieldUpdate == "Maker"){
        let makers = selectedFilters.carMaker;
        if (makers.includes(fieldValue))
          makers.splice(makers.indexOf(fieldValue), 1);
        else
          makers.push(fieldValue)
        setSelectedFilters({...selectedFilters, carMaker: makers})
      }
    }
  }

  const renderModelFilters = ({ item }) => (
    <TouchableOpacity onPress={() => handleFiltersSelection(selectedFilters, "Maker", item)}>
      <View style={{
        maxWidth: 
        Dimensions.get('window').width /4,
        // flex: 0.5,
        backgroundColor: '#fff',
        marginBottom: 10,
        alignItems: "center",
        flexDirection: "row",
        borderRadius: 4,
        // borderWidth:2,
        // borderColor:'black',
      }}>
        <View style={{ borderWidth: 1, height: 10, width: 10, borderColor: "#7c7c7c", alignItems: "center", justifyContent: "center", marginRight: 5, marginLeft: 5 }}>
          {selectedFilters.carMaker.includes(item) && <Ionicons name="checkmark" style={{ fontSize: 10, fontWeight: "bold" }} /> }
        </View>
        <View style={{ width: 100 }}>
          <Text style={{ fontSize: 14, fontFamily: Platform.OS === "ios" ? "Lato-Light" : "Lato-Regular", marginRight: 5, width: 'auto', color: "black" }} numberOfLines={3}>
            {item}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  )
  const renderYearFilters = ({ item }) => (
    <TouchableOpacity onPress={() => handleFiltersSelection(selectedFilters, "Year", item)}>
      <View style={{
        maxWidth: Dimensions.get('window').width / 2,
        flex: 0.5,
        backgroundColor: '#fff',
        marginBottom: 10,
        alignItems: "center",
        flexDirection: "row",
        borderRadius: 4
      }}>
        <View style={{ borderWidth: 1, height: 10, width: 10, borderColor: "#7c7c7c", alignItems: "center", justifyContent: "center", marginRight: 5, marginLeft: 5 }}>
          {selectedFilters.carYear.includes(item) && <Ionicons name="checkmark" style={{ fontSize: 10, fontWeight: "bold" }} /> }
        </View>
        <Text style={{ fontSize: 12, fontFamily: Platform.OS === "ios" ? "Lato-Light" : "Lato-Regular", color: "black" }}>
          {item}
        </Text>
      </View>
    </TouchableOpacity>
  )
  const renderCarNode = ({item}) => {
    let ImageArray = []
    if (item.node.carpicturesSet.edges.length > 0){
      item.node.carpicturesSet.edges.map((picture, index) => {
          if(picture.node.pictureUrl!="")
        ImageArray.push(picture.node.pictureUrl);
      })
    }
    return (
    <View style={styles.item_wrap}>
            <View style={styles.image_item_wrap}>
            {item.node.carpicturesSet.edges.length > 0 ?
                  <View>
                    <SliderBox
                      images={ImageArray && ImageArray.length > 5 ? ImageArray.slice(0,5) : ImageArray && ImageArray.length <= 5 ? ImageArray : null}
                      sliderBoxHeight={190}
                      onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                      dotColor="#1b4d7e"
                      inactiveDotColor="#90A4AE"
                      paginationBoxVerticalPadding={30}
                      autoplay={false}
                      circleLoop
                      resizeMethod={"resize"}
                      resizeMode={'cover'}
                      paginationBoxStyle={{
                        position: "absolute",
                        bottom: 0,
                        padding: 0,
                        alignItems: "center",
                        alignSelf: "center",
                        justifyContent: "center",
                        paddingVertical: 10
                      }}
                      dotStyle={{
                        width: 10,
                        height: 10,
                        numberOfLines:5,
                        borderRadius: 5,
                        marginHorizontal: 0,
                        padding: 0,
                        margin: 0,
                        backgroundColor: "rgba(128, 128, 128, 0.92)"
                      }}
                      ImageComponentStyle={{ marginRight: 40 }}
                      imageLoadingColor="#grey"
                    // parentWidth={200}
                    />
                  </View>
                  : <Image style={styles.imageitem} source={require('../../assets/images/no_image_found.png')} />}
                  </View>
                  <View style={{ marginTop:20,marginBottom: 20,flexDirection:"row"}}>
                  <View style={{flex:1, flexDirection:"column"}}>
                    <Text style={[styles.header_title, global_styles.lato_semibold]}>{item.node.year} {item.node.model}</Text>
                    {item.minPrice !== item.maxPrice ?
                    <View style={{ flexDirection: "row", marginLeft: 10 }}>
                      <Text style={[global_styles.lato_light,styles.car_price, { fontSize:20,textDecorationLine: 'line-through', color: "#7c7c7c" }]}>${item.maxPrice}{item.node.prices.edges[0].intervalUnit}/{item.node.prices.edges[0].node.interval > 1 ? item.node.prices.edges[0].node.interval + item.node.prices.edges[0].node.intervalUnit.toLowerCase() + "s" : item.node.prices.edges[0].node.intervalUnit.toLowerCase()}</Text>
                      <Text style={[global_styles.lato_semibold,styles.car_price, { fontSize:20, color: "#db9360", marginLeft: 5 }]}>${item.minPrice}{item.node.prices.edges[0].intervalUnit}/{item.node.prices.edges[0].node.interval > 1 ? item.node.prices.edges[0].node.interval + item.node.prices.edges[0].node.intervalUnit.toLowerCase() + "s" : item.node.prices.edges[0].node.intervalUnit.toLowerCase()}</Text>
                    </View>
                    : <Text style={[styles.car_price, { marginLeft: 10 }]}>${item.maxPrice}/{item.node.prices.edges[0].node.interval > 1 ? item.node.prices.edges[0].node.interval + item.node.prices.edges[0].node.intervalUnit.toLowerCase() + "s" : item.node.prices.edges[0].node.intervalUnit.toLowerCase()}</Text>}
                  </View>
                  <View>
                  <TouchableHighlight underlayColor='' style={styles.buttonColor0} onPress={() => props.navigation.push('CardetailScreen',{driver: state.driver, carPk:item.node.pk,agreementType: selectedFilters.rentalType, 
                Year: item.node.year, Model: item.node.model, Color: item.node.color, MaxValue: item.maxPrice, MinValue: item.minPrice, ImageArray: ImageArray ? ImageArray : null, Description: item.node.genericDescription,
                interval: item.node.prices.edges[0].node.interval, intervalUnit: item.node.prices.edges[0].node.intervalUnit, requiredDeposit: state.requiredDeposit})}>
                <Text style={[styles.car_see_detail]}>{"RESERVE >"}</Text>
              </TouchableHighlight>
                </View>
                  </View>              
              </View>
    );
  }
  const sortHandle = () => {
    if (selectedFilters.sortBy == "desc"){
      setSelectedFilters({...selectedFilters, sortBy:"asc"})
    }else if (selectedFilters.sortBy == "asc"){
      setSelectedFilters({...selectedFilters, sortBy:"desc"})
    }
  }
  const handleAgreementChange = (value) => {
    setSelectedFilters({...selectedFilters, rentalType: value})
    setState({...state, isLoading: true})
  }
  return (
        <>
        {state.isLoading ? <>
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <ActivityIndicator animating={true} color="#393e5c" size={50} />
        </View>
       </> : <>
        {!carData.availableCars ?
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text style={{ fontFamily: "Lato-regular" }}>No Cars Found</Text>
        </View>
        :
        <View style={{ flex: 1, backgroundColor: "#f9f9f4" }} >
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent:"space-between"}}>

            <View style={{ margin: 5, width: "35%", height: 50 }}>
              <RNPickerSelect
                style={pickerAgreementStyles}
                useNativeAndroidPickerStyle={false}
                placeholder={{label:selectedFilters.rentalType,value:selectedFilters.rentalType}}
                mode="dropdown"
                value={selectedFilters.rentalType}
                defaultValue={selectedFilters.rentalType}
                onValueChange={(value) => handleAgreementChange(value)}
                items={carFilters.rentalTypes}
                Icon={() => {
                  return (
                    <View
                      style={pickerAgreementStyles.icon}
                    />
                  );
                }}
              />
            </View>
            <TouchableOpacity
              onPress={() => { state.filterShow ? setState({...state, filterShow : false}) : setState({...state, filterShow : true}) }}
              style={{ backgroundColor: "#F9F9F3", paddingHorizontal:20, margin: 5, borderRadius: 8, backgroundColor: "white" }}>
              <View style={{ justifyContent: "center", alignItems: "center", height: 45, flexDirection: "row", }}>
                {/* <Ionicons name="options" style={{ fontSize: 25, color: "black" }} /> */}
                <Image style={{width: 24,height: 24,resizeMode: 'contain',}} source={filter_icon}/>
                <Text style={{ fontSize: 14, color:'#393e5c' }}> FILTER</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { sortHandle()}}
              style={{ backgroundColor: "#F9F9F3", paddingHorizontal:30, margin: 5, borderRadius: 8, backgroundColor: "white" }}>
              <View style={{ justifyContent: "center", alignItems: "center", height: 45, flexDirection: "row", }}>
              {selectedFilters.sortBy == "asc"?
                 <FontAwesome5 name="sort-amount-up-alt" style={{ fontSize: 25, color: "black" }} />:
                 <FontAwesome5 name="sort-amount-down" style={{ fontSize: 25, color: "black" }} />}
                <Text style={{ fontSize: 14, color:'#393e5c' }}> SORT</Text>
              </View>
            </TouchableOpacity>
          </View>
          {state.filterShow ?
            <View style={{ backgroundColor: "white", padding: 10, justifyContent: "space-around" }}>
              <View style={{ flexDirection: "row", marginTop: 10, justifyContent: "space-between", marginRight: 10, marginLeft: 10 }}>
                <View  >
                  <Text style={styles.filterTitle}>Model</Text>
                  <FlatList
                    data={carFilters.carMakers}
                    renderItem={renderModelFilters}
                    numColumns={2}
                    style={{ marginTop: 5 }}
                    // contentContainerStyle={{ justifyContent: "space-around" }}
                    keyExtractor={item => item.id}
                  />
                  <Text style={[styles.filterTitle,{marginTop:10}]}>Price Range</Text>

                  <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                    <Text style={{ fontFamily: "Lato-Medium", fontSize: 13 }}>${selectedFilters.priceRange.min == selectedFilters.priceRange.max ? selectedFilters.priceRange.min - 1 : selectedFilters.priceRange.min}</Text>
                    <View style={{ width: 10, borderWidth: 1, borderColor: "black", height: 0, marginLeft: 5, marginRight: 5 }} />
                    <Text style={{ fontFamily: "Lato-Medium", fontSize: 13 }}>${selectedFilters.priceRange.max}</Text>
                  </View>
                  <View style={{ marginLeft: 10 }}>
                    <MultiSlider
                      values={[state.multiSliderValue[0], state.multiSliderValue[1]]}
                      sliderLength={100}
                      markerStyle={{ size: "small", height: 20, width: 20, backgroundColor: "#393e5c" }}
                      trackStyle={{ color: "red" }}
                      // markerContainerStyle={{width:10,height:10,size:"small"}}
                      onValuesChange={(val) => setSelectedFilters({...selectedFilters, priceRange: {min:(val[0] ? val[0] : selectedFilters.priceRange.min), max:(val[1] ? val[1] : selectedFilters.priceRange.max)} })}
                      min={selectedFilters.priceRange.min == selectedFilters.priceRange.max ? selectedFilters.priceRange.min - 1 : selectedFilters.priceRange.min}
                      max={selectedFilters.priceRange.max}
                      step={1}
                      allowOverlap
                      snapped
                    />
                  </View>
                </View>
                <View style={{ width: 100, marginRight:20 }}>
                  <Text style={styles.filterTitle}>Year</Text>
                  <View>
                    <FlatList
                      data={carFilters.carYears}
                      renderItem={renderYearFilters}
                      numColumns={2}
                      style={{ marginTop: 5 }}
                      contentContainerStyle={{ justifyContent: "space-around" }}
                      keyExtractor={item => item.id}
                    />
                  </View>
                  <Text style={[styles.filterTitle,{marginTop:40}]}>Type</Text>
                  <RNPickerSelect
                    style={pickerSelectStyles}
                    useNativeAndroidPickerStyle={false}
                    mode="dropdown"
                    value={selectedFilters.carGroup}
                    placeholder={{ label: "Car Category", value: null }}
                    onValueChange={(value) => setSelectedFilters({...selectedFilters, carGroup: value })}
                    items={carFilters.carGroups}
                    Icon={() => {
                      return (
                        <View
                          style={pickerSelectStyles.icon}
                        />
                      );
                    }}
                  />
                </View>
              </View>
              <View style={{ margin: 10 }}>
                <Text style={styles.filterTitle}>Select Your Pickup Location</Text>
                <RNPickerSelect
                    // placeholder={"Select your pickup Location"}
                    style={pickerSelectStyles}
                    onValueChange={(value) => setSelectedFilters({...selectedFilters, carLocation: value })}
                    mode="dropdown"
                    value={selectedFilters.carLocation}
                    useNativeAndroidPickerStyle={false}
                    placeholder={{ label: "All", value: null }}
                    items={carFilters.carLocations}
                    Icon={() => {
                      return (
                        <View
                          style={pickerSelectStyles.icon}
                        />
                      );
                    }}
                  />
                {/* } */}
              </View>
              {/* <View style={{ flexDirection: "row", justifyContent: "flex-end", marginTop: 20, alignItems: "center" }}>
                <TouchableOpacity
                  onPress={() => clearCarFilter()}
                  style={{ paddingHorizontal: 20, paddingVertical: 5, borderWidth: 1, borderColor: "#393e5c", borderRadius: 5 }}>
                  <Text style={{ fontSize: 10, color: "#393e5c" }}>CLEAR ALL</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => applyCarFilter()}
                  style={{ paddingHorizontal: 25, paddingVertical: 7, borderColor: "black", marginLeft: 5, backgroundColor: "#393e5c", borderRadius: 5 }}>
                  <Text style={{ color: "white", fontSize: 10 }}>APPLY</Text>
                </TouchableOpacity>
              </View> */}
            </View>
            : null
            } 
            {carData.filteredCars.length>0?
            <FlatList
              data={carData.filteredCars ? carData.filteredCars : null}
              renderItem={renderCarNode}
              style={{ flex: 1, elevation: 2, shadowColor: '#000' }}
              keyExtractor={item => item.id}
              removeClippedSubview={true}
              maxToRenderPerBatch={3000}
            />           
            :
            <View style={{alignItems:"center",justifyContent:"center"}}>
            <Text style={{  fontFamily: 'Lato-Semibold',
      fontWeight: '600',
      fontStyle: 'normal',color:"#393e5c",fontSize:17}}>NO CAR FOND</Text>
            </View>
            }
            </View>
      }
      </>
    }
      </>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9F3"
  },
  slider: {
    flex: 1,
    borderLeftWidth: 8,
    backgroundColor: "#FFFFFF",
    borderRadius: 8,
    marginVertical: 6,
    overflow: 'visible' // for custom animations
  },
  sliderContentContainer: {
    paddingVertical: 0 // for custom animation
  },
  borderLeft0: {
    // borderLeftColor: Colors.color0,
  },
  borderLeft1: {
    // borderLeftColor: Colors.color1,
  },
  borderLeft2: {
    // borderLeftColor: Colors.color2,
  },
  paginationContainer: {
    marginTop: -57,
    marginBottom: -10,
  },
  paginationDotContainer: {
    marginHorizontal: 0,
  },
  calenderView: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    backgroundColor: '#EEE',
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
  },
  item_wrap: {
    flex: 1,
    margin: 10,
    elevation: 2,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.1,
    backgroundColor: "white"
  },
  image_item_wrap: {
    flex: 1,
    backgroundColor: "#f9f9f4",
    height: 200,
  },
  item_right: {
    flex: 0.3,
    marginTop: 10,
  },
  priceWrap: {
    marginVertical: 10,
    marginHorizontal: 10,
  },
  priceText: {
    marginVertical: 10,
    fontSize: 16,
  },
  priceSliderView: {
    flexDirection: "row"
  },
  priceSliderStartText: {
    // flex: 1,
    margin: 5
  },
  dotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    padding: 0,
    margin: 0,
    backgroundColor: "rgba(128, 128, 128, 0.92)"
  },
  priceSliderEndText: {
    margin: 5
    // flexDirection: "row-reverse"
  },
  priceSlider: {
    width: 180,
    height: 30
  },
  imageitem: {
    width: "100%",
    height: 150,
    resizeMode: 'contain',
    marginTop: 10,
    marginBottom: 5,
  },
  car_uber: {
    fontSize: 10,
    color: 'black',
  },
  car_choose: {
    fontSize: 12,
    color: '#707070',
  },
  car_price: {
    color: "#39AFD2",
    fontSize: 20,

  },
  header_title: {
    fontSize: 20,
    // fontWeight: "bold",
    marginLeft: 10,
    flex: 0.89,
    color:"#393e5c"
  },
  button_item_right: {
    flex: 0.3,
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginTop: 10,
    marginBottom: 20
  },
  car_see_detail: {
    color: "#fff",
    paddingVertical: 8,
    paddingHorizontal: 16,
    justifyContent: "center",
    textAlign: "center",
    fontSize: 14,
    fontFamily: "lato-medium"
  },
  buttonColor0: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    backgroundColor: "#DB9360",
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 3,
    paddingBottom: 3
  },
  buttonColor1: {
    borderRadius: 5,
    // backgroundColor: Colors.color1,
  },
  buttonColor2: {
    borderRadius: 5,
    // backgroundColor: Colors.color2,
  },
  filterTitle: {
    fontFamily: "Lato-Medium",
    fontSize: 18
  }
});
export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    paddingVertical: Platform.OS === "ios" ? 11 : 10,
    paddingHorizontal: 5,
    marginTop: 5,
    // marginHorizontal: 10,
    borderWidth: 1,
    borderColor: '#d4d4d4',
    backgroundColor: '#fff',

    borderRadius: 6,
    color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    paddingRight: 5,
    fontStyle: 'normal',
    fontSize: Platform.OS === "ios" ? 12 : 13,
  },
  placeholder: {
    color: 'black',
    fontSize: 12,
  },
  inputAndroid: {
    paddingHorizontal: 10,
    paddingRight: 0,
    paddingVertical: 6,
    marginHorizontal: 10,
    marginLeft: 0,
    marginTop: Platform.OS === "ios" ? 0 : 4,
    borderWidth: 1,
    borderColor: '#d4d4d4',
    backgroundColor: '#fff',
    borderRadius: 6,
    color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 13,
  },
  iconContainer: {
    top: Platform.OS === "ios" ? 22 : 22,
    right: Platform.OS === "ios" ? 10 : 10,
  },
  icon: {
    backgroundColor: 'transparent',
    borderTopWidth: 6,
    borderTopColor: 'gray',
    borderRightWidth: 6,
    borderRightColor: 'transparent',
    borderLeftWidth: 6,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
  }
});

export const pickerAgreementStyles = StyleSheet.create({
  inputIOS: {
    paddingVertical: Platform.OS === "ios" ? 15 : 10,
    paddingHorizontal: 35,

    marginTop: 5,
    // marginHorizontal: 10,
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: '#fff',
    letterSpacing:0.75,
    borderRadius: 6,
    color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    paddingRight: 5,
    fontStyle: 'normal',
    fontSize: Platform.OS === "ios" ? 14 : 13,
  },
  placeholder: {
    color: '#393e5c',
    // padding:20,
  },
  inputAndroid: {
    paddingHorizontal: 15,
    paddingRight: 0,
    paddingVertical: 15,
    marginHorizontal: 10,
    marginLeft: 0,
    marginTop: Platform.OS === "ios" ? 0 : 4,
    // borderWidth: 1,

    backgroundColor: '#fff',
    borderRadius: 6,
    // color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 13,
  },
  iconContainer: {
    top: Platform.OS === "ios" ? 25 : 22,
    right: Platform.OS === "ios" ? 5 : 10,
  },
  icon: {
    backgroundColor: 'transparent',
    borderTopWidth: 6,
    borderTopColor: 'gray',
    borderRightWidth: 6,
    borderRightColor: 'transparent',
    borderLeftWidth: 6,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
  }
});