import React, { useState, useEffect } from 'react';
import { Platform, View, ScrollView, SafeAreaView, ActivityIndicator, Text, Image, StyleSheet, TouchableHighlight, TouchableOpacity } from 'react-native';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import LaunchNavigator from 'react-native-launch-navigator';
import moment from "moment";


export default function ConfirmationForReservation(props) {
    const [openReservationDetail,setOpenReservation] = useState("");

    useEffect(()=>{
      
        getCheckOpenReservation()

    },[])
    
    const getDirectionoBrooklyn = () => {
        LaunchNavigator.navigate("445 Empire Blvd, Brooklyn, NY 11225")
          .then(() => console.log("Launched navigator"))
          .catch((err) => console.error("Error launching navigator: " + err));
      }

    const getCheckOpenReservation = async () => {
        console.log(props.navigation.state.params.reservationDetail)
        setOpenReservation(props.navigation.state.params.reservationDetail)
      }
    

    return (
        <ScrollView>
         {openReservationDetail!=""?
        <View style={{ flex: 1, backgroundColor: "#F9F9F3" }}>
            <View style={{ flex: 1, alignItems: "center", padding: 20 }}>
                <Image source={require('../../assets/images/landing_page_image.png')} style={{ width: 300, height: 200, resizeMode: "contain",marginTop:30 }} />
    <Text style={{ color: "#646462",textAlign:"center",fontFamily:"Lato-Medium",fontSize:17,marginTop:20}}>{`Your booking is confirmed. Your details are as follows:`}</Text>
                {/* <Text style={{ color: "#646462" }}>as Fellows:</Text> */}
                <View style={{ flexDirection: "row", marginTop: 20,alignItems:"center",justifyContent:"center" }}>
                <Text style={{color: "#646462",fontFamily:"Lato-Bold",fontSize:19 }}>Car: </Text>
    <Text style={{ color: "#646462",fontFamily:"Lato-Medium",fontSize:18}}>{openReservationDetail.preferredCar}</Text>
                </View>
                <View style={{ flexDirection: "row", marginTop:5,alignItems:"center" }}>
                    <Text style={{color: "#646462",fontFamily:"Lato-Bold",fontSize:19 }}>Price: </Text>
               <Text style={{ color: "#646462",fontFamily:"Lato-Medium",fontSize:18}}>${openReservationDetail.priceRange} / {openReservationDetail.agreementType.name}</Text>
                </View>
                <View style={{marginTop: 20,alignItems:"center" }}>
                     {console.log("check date and time....",openReservationDetail)}
                    <Text style={{color: "#646462",fontFamily:"Lato-Bold",fontSize:19 }}>Pickedup Date and Time:</Text>
    <Text style={{ color: "#646462",fontFamily:"Lato-Medium",fontSize:18,marginTop:5}}>{moment.utc(openReservationDetail.pickupDate).format("LLL")}</Text>
                </View>
                <View style={{ flexDirection: "row", marginTop: 20}}>
                <Text style={{color: "#646462",fontFamily:"Lato-Bold",fontSize:19 }}>Location: </Text>
                <Text style={{ color: "#646462",fontFamily:"Lato-Medium",fontSize:17,}}>{openReservationDetail.pickUpLocation.includes("Empire")? "445 Empire Blvd,\nBrooklyn, NY 11225.":"691 Burke Avenue, Bronx, NY 10467"}</Text>
                </View>

                <Text style={{ color: "#646462" ,marginTop:20,textAlign:"center",fontFamily:"Lato-Medium",fontSize:18}}>You will receive an email shortly with your rental contract.</Text>
                <TouchableOpacity
                onPress={()=>{getDirectionoBrooklyn()}}
                underlayColor="" activeOpacity={0.8} style={{
                    backgroundColor: "#fff",
                    borderColor: "#d4d4d4",
                    borderWidth: 1,
                    width: 150,
                    marginTop: 30,
                    alignItems:"center",
                    justifyContent:"center",
                    height:44,
                    borderRadius: 4
                }}>
                    <Text style={[{
                        color: "#393e5c",
                        fontSize: 15,
                        alignSelf: "center",
                        fontFamily:"Lato-Medium",
                        flexDirection: "row",
                        paddingVertical: 7,
                        paddingHorizontal: 8,
                    }, global_styles.lato_regular]}>DIRECTIONS</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                 onPress={()=>{props.navigation.push('RentalPage')}}
                 underlayColor="" activeOpacity={0.8} style={{
                    backgroundColor: "#db9360",
                    width: 150,
                    marginTop: 20,
                    alignItems:"center",
                    justifyContent:"center",
                    height:44,
                    borderRadius: 4
                }}>
                    <Text style={[{
                       color: "white",
                       fontSize: 15,
                       alignSelf: "center",
                       fontFamily:"Lato-Medium",
                       flexDirection: "row",
                       paddingVertical: 7,
                       paddingHorizontal: 8,
                       
                    }, global_styles.lato_regular]}>HOME</Text>
                </TouchableOpacity>
            </View>
        </View>:null}
        </ScrollView>
 
    )
}