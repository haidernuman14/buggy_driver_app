import React, { useState, useEffect } from 'react';
import { useMutation, useApolloClient,useLazyQuery } from '@apollo/react-hooks';
import { View, ScrollView, SafeAreaView, ActivityIndicator, Text, StyleSheet, TouchableHighlight, Linking, TouchableOpacity, Image, Alert } from 'react-native';
import Colors from '../../constants/Colors';
import CalendarPicker from 'react-native-calendar-picker';
import { createReservation } from '../../graphql/createReservation';
import moment from 'moment';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import RNPickerSelect from 'react-native-picker-select';
import { CheckConnectivity } from '../Helper/NetInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { checkOpenReservation } from '../../graphql/checkOpenReservation';
import { updateReservation } from '../../graphql/updateReservation';
import { createReservationPrice } from '../../graphql/createReservationPrice';
import { allBusinessHours } from '../../graphql/allBusinessHours';


global.date = new Date();
let day = date.getDay();
let minDay = (day == 6) ? 1 : 0;
let maxDay = (day == 5 || day == 6) ? 2 : 1;
export default function CarOnschedScreen(props) {
  const [timeSlot, setTimeSlot] = useState([])
  const client = useApolloClient();
  const [AcreateReservation] = useMutation(createReservation);
  const [AupdateReservation] = useMutation(updateReservation);
  const [AcreateReservationPrice] = useMutation(createReservationPrice)
  // const [timeSlot, setTimeSlot] = useState([
  //   { label: "9:00", value: "9:00" },
  //   { label: "9:15", value: "9:15" },
  //   { label: "9:30", value: "9:30" },
  //   { label: "9:45", value: "9:45" },
  //   { label: "10:00", value: "10:00" },
  //   { label: "10:15", value: "10:15" },
  //   { label: "10:30", value: "10:30" },
  //   { label: "10:45", value: "10:45" },
  //   { label: "11:00", value: "11:00" },
  //   { label: "11:15", value: "11:15" },
  //   { label: "11:30", value: "11:30" },
  //   { label: "11:45", value: "11:45" },
  //   { label: "12:00", value: "12:00" },
  //   { label: "12:15", value: "12:15" },
  //   { label: "12:30", value: "12:30" },
  //   { label: "12:45", value: "12:45" },
  //   { label: "1:00", value: "01:00" },
  //   { label: "01:15", value: "01:15" },
  //   { label: "01:30", value: "01:30" },
  //   { label: "01:45", value: "01:45" },
  //   { label: "02:00", value: "02:00" },
  //   { label: "02:15", value: "02:15" },
  //   { label: "02:30", value: "02:30" },
  //   { label: "02:45", value: "02:45" },
  //   { label: "03:00", value: "03:00" },
  //   { label: "03:15", value: "03:15" },
  //   { label: "03:30", value: "03:30" },
  //   { label: "03:45", value: "03:45" },
  //   { label: "04:00", value: "04:00" },
  //   { label: "04:15", value: "04:15" },
  //   { label: "04:30", value: "04:30" },
  //   { label: "04:45", value: "04:45" },
  //   { label: "05:00", value: "05:00" },
  // ]);

  const [selectedTimeSlot, setSelectedTimeSlot] = useState("");
  const [date, setDate] = useState(new Date(1598051730000));
  const [pickUpTime, setPickUpTime] = useState();
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [state, setState] = useState({
    isLoading: false,
    isTimeLoading: false,
    minDate: moment().add(minDay, 'day'),
    displayDate: moment().add(minDay, 'day').format('MM-DD-YYYY'),
    startDate: moment().add(minDay, 'day').format('YYYY-MM-DD'),
    endDate: moment().add(minDay, 'day').format('YYYY-MM-DD'),
    selectedTime: '',
    availabilityList: [],
    reservationId: "",
    timeSlot:"",
    the_date: '',
    the_Day: "",
  });
  const { isLoading, minDate, startDate } = state;
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    //await AsyncStorage.setItem('onSchedToken', '');
    if (!netState) {
      props.screenProps.setNetModal(true)
      return false;
    }
    return true;
  }

  useEffect(() => {

    setUserId()

  }, [])

  ///......set user Id  function....../
  const setUserId = async () => {
    const driverData = await AsyncStorage.getItem('user');
    let driverid = JSON.parse(driverData).id;
    getCheckOpenReservation(driverid)
  }

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setPickUpTime(moment(currentDate).format('HH:MM'))
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    console.log("here")
    showMode('time');
  };

  //......checkOpentReservation function....
  const getCheckOpenReservation = (driverId) => {
    client.query({
      query: checkOpenReservation,
      variables: { id: driverId }
    }).then((res) => {
      if (res.data && res.data.driver && res.data.driver.reservationDriver && res.data.driver.reservationDriver.edges.length > 0 && res.data.driver.reservationDriver.edges[0].node.car == null) {
        setState({ ...state, reservationId: res.data.driver.reservationDriver.edges[0].node.id })
        createReservationPriceHandler(res.data.driver.reservationDriver.edges[0].node.id)
      }
    }).catch((error) => {
      console.log("checkOpenReservation", error)
    })
  }

  const createReservationPriceHandler = (reservationId) => {
    AcreateReservationPrice({
      variables: {
        input: {
          reservation: reservationId,
          price: props.navigation.state.params.price,
          interval: props.navigation.state.params.interval,
          intervalUnit: capitalizeFirstLetter(props.navigation.state.params.intervalUnit),
          maximumPrice: 0,
          minimumPrice: 0,
        }
      }
    }).then((res) => {

    }).catch((error) => {
      console.log(error)
    })
  }

  const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }


  const setStateUpdate = (val) => {
    console.log(val)
    setSelectedPickUpLocation(val)
  }
  const onDateChange = (date) => {
    let setDate = moment(date).format('YYYY-MM-DD');
    let day = moment(date).format('dddd');
  
    setState({ ...state, the_date: setDate, the_Day: day })
    callAllBusinessHours();
  }

  const [callAllBusinessHours] = useLazyQuery(allBusinessHours, {
    fetchPolicy: "network-only",
      onError: (error) => { console.log("error in all subs and trans", error), setState({ ...state, isLoading: false }) },
      onCompleted: (data) => {
        data.allBusinessHours.edges.map((item) => {
          if (item.node.dayOfWeekDisplay == state.the_Day) {
              let x = {
                slotInterval: 15,
                openTime: item.node.openTime,
                closeTime: item.node.closeTime
              };
              let startTime = moment(x.openTime, "HH:mm");
              let endTime = moment(x.closeTime, "HH:mm");
              let allTimes = [];
              while (startTime < endTime) {
                  allTimes.push({ label: startTime.format("HH:mm"), value: startTime.format("HH:mm") });
                  startTime.add(x.slotInterval, 'minutes');
              }
              setTimeSlot(allTimes)
              console.log("all times slots",allTimes);
      }
    })
  }
})
  const handlePayLater = async () => {
    
    setState({ ...state, isLoading: true })
    const driverData = await AsyncStorage.getItem('user');;
    const driverID = JSON.parse(driverData).id;
    if (!(state.the_date)) {
      setState({ ...state, isLoading: false })
      Alert.alert("Error", "Please Select the Date")
    }
    else{
      if(!selectedTimeSlot){
            setState({ ...state, isLoading: false,timeSlot:"Please Select Time Slot." })   
          }
      else {
        setState({ ...state,timeSlot:"",isLoading:true }) 
        if (state.reservationId == "")
      createReservationHandler(driverID , state.the_date + " " +selectedTimeSlot)
    else if (state.reservationId != "")
      updateReservationHandler(driverID,state.the_date + " " + selectedTimeSlot)
          }
    }
  }

  const createReservationHandler = (driverId,pickUpDate) => {
    console.log("carId:",props.navigation.state.params.carId)
    console.log("driverId:",driverId,)
    console.log("carId:",props.navigation.state.params.pickUpLocation,)
    console.log("carId:",props.navigation.state.params.agreementType,)
    console.log("carId:",props.navigation.state.params.price)
    console.log("carId:",pickUpDate,)
    console.log("carId:",props.navigation.state.params.carId,)
    AcreateReservation({
      variables: {
        input: {
          carId: props.navigation.state.params.carId,
          driverId: driverId,
          pickUpLocation:props.navigation.state.params.pickUpLocation,
          agreementType:props.navigation.state.params.agreementType,
          pickupDate: pickUpDate,
          preferredCar:props.navigation.state.params.model,
          priceRange: props.navigation.state.params.price,
          reservationPrices: [
            {
              price: props.navigation.state.params.price,
              interval: props.navigation.state.params.interval,
              intervalUnit: capitalizeFirstLetter(props.navigation.state.params.intervalUnit),
              maximumPrice: 0,
              minimumPrice: 0,
            }
          ]
        }
      }
    }).then((response) => {

      if (response.data.createReservation.ok) {
       console.log(response.data.createReservation.reservation)
        setState({ ...state, isLoading: false })
        props.navigation.push('ReservationPay',{reservationDetail:response.data.createReservation.reservation})
      }
      else if (response.data.createReservation.errors && response.data.createReservation.errors.length > 0 && response.data.createReservation.errors[0].messages && response.data.createReservation.errors[0].messages.length > 0) {
        setState({ ...state, isLoading: false })
        console.log(response.data.createReservation.errors)
        Alert.alert("Error", "An open reservation Exits for this account.")
      }

    }).catch((error) => {
      setState({ ...state, isLoading: false, isModalPayLaterSuccess: false, error_global: "Your reservation was not created. Please try again later." })
      console.log("Error in catch", error)
    });
  }

  const updateReservationHandler = (driverId,pickUpDate) => {
      console.log("state.reservationId",state.reservationId)
    console.log("update reservartion.....")
    AupdateReservation({
      variables: {
        input: {
          reservationId: state.reservationId,
          carId: props.navigation.state.params.carId,
          driverId: driverId,
          pickUpLocation: props.navigation.state.params.pickUpLocation,
          agreementType: props.navigation.state.params.agreementType,
          pickupDate: pickUpDate,
          preferredCar: props.navigation.state.params.model,
          priceRange: props.navigation.state.params.price,
        }
      }
    }).then((response) => {
      if (response.data.updateReservation.ok) {
          console.log("res data......",response.data.updateReservation.reservation)
        setState({ ...state, isLoading: false })
        props.navigation.push('ReservationPay', {reservationDetail:response.data.updateReservation.reservation})
      }
      else if (response.data.updateReservation.errors && response.data.updateReservation.errors.length > 0 && response.data.updateReservation.errors[0].messages && response.data.updateReservation.errors[0].messages.length > 0) {
        setState({ ...state, isLoading: false })
        Alert.alert("Error", "An open reservation Exits for this account.")

      }
    }).catch((error) => {
      setState({ ...state, isLoading: false, isModalPayLaterSuccess: false, error_global: "Your reservation was not created. Please try again later." })
      console.log("Error in catch", error)
    });
  }

  return (
    <>
      {isLoading ?
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> :
        <ScrollView style={styles.container}>
          <View style={{ flex: 1 }}>
            <Text style={[{ marginTop: 20, fontSize: 18, alignItems: "center", width: "100%", textAlign: "center" }, global_styles.lato_regular_android]}>Choose A Date To Schedule Your Reservation</Text>
            <View style={{ marginTop: 30 }}>
              <CalendarPicker
                minDate={global.date}
                maxDate={global.date}
                startFromMonday={true}
                selectedDayColor="#1b4d7e"
                selectedDayTextColor="white"
                onDateChange={(value) => onDateChange(value)}
              />
            </View>
          </View>

          <TouchableOpacity style={{ marginLeft: 10, marginTop: 10 }} onPress={showTimepicker}>
            <Text style={{ fontFamily: "Lato-Medium" }}>
              Select your Pickup Time
             </Text>

            <View style={{ width: "50%", height: 50 }}>
              <RNPickerSelect
                style={pickerAgreementStyles}
                useNativeAndroidPickerStyle={false}
                placeholder={{ label: "CHOOSE TIME SLOT" }}
                mode="dropdown"
                value={selectedTimeSlot}
                // defaultValue={agreementType}
                onValueChange={(value) => setSelectedTimeSlot(value)}
                items={timeSlot}
                Icon={() => {
                  return (
                    <View
                      style={pickerAgreementStyles.icon}
                    />
                  );
                }}
              />
              {!selectedTimeSlot?
              <Text style={{ color: "red",marginTop:10 }}>{state.timeSlot}</Text>
              :null}
            </View>

          </TouchableOpacity>

          <View style={styles.buttonView}>
            <TouchableHighlight style={styles.checkoutButton} onPress={() => handlePayLater()}>
              <Text style={[styles.checkoutButtonText, global_styles.lato_regular_android]}>PAY DEPOSIT {'>'}</Text>
            </TouchableHighlight>
          </View>
          <View>

          </View>

        </ScrollView>
      }
    </>
  );

}

export const pickerAgreementStyles = StyleSheet.create({
  inputIOS: {
    paddingVertical: Platform.OS === "ios" ? 15 : 10,
    paddingHorizontal: 5,
    backgroundColor: "white",
    marginTop: 5,
    // marginHorizontal: 10,
    borderWidth: 1,

    borderColor: '#979797',
    backgroundColor: '#fff',

    borderRadius: 6,
    color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    paddingRight: 5,
    fontStyle: 'normal',
    fontSize: Platform.OS === "ios" ? 14 : 13,
  },
  placeholder: {
    color: 'black',
    fontSize: 12,
  },
  inputAndroid: {
    paddingHorizontal: 8,
    paddingRight: 0,
    paddingVertical: 6,
    marginHorizontal: 10,
    marginLeft: 0,
    marginTop: Platform.OS === "ios" ? 0 : 4,
    borderWidth: 1,
    backgroundColor: '#fff',
    borderRadius: 6,
    color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 13,
  },
  iconContainer: {
    top: Platform.OS === "ios" ? 25 : 22,
    right: Platform.OS === "ios" ? 5 : 10,
  },
  icon: {
    backgroundColor: 'transparent',
    borderTopWidth: 6,
    borderTopColor: 'black',
    borderRightWidth: 6,
    borderRightColor: 'transparent',
    borderLeftWidth: 6,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
  }
});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f9f9f4',
  },
  calenderView: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    backgroundColor: '#EEE',
    justifyContent: "center"
  },
  calenderText: {
    flex: 1,
    fontSize: 16,
    paddingVertical: 8,
  },
  calenderDateView: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 8,
    backgroundColor: "#FFF"
  },
  calenderDate: {
    flex: 1,
  },
  calenderDateText: {
    fontSize: 16,
  },
  calenderIcon: {
    flexDirection: "row-reverse"
  },
  calenderShow: {
    zIndex: 10,
    display: "flex"
  },
  calenderHide: {
    display: "none"
  },
  calenderTimeView: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 8,
  },
  calenderTimeText: {
    fontSize: 16,
  },
  errorText: {
    color: "red",
    paddingVertical: 5
  },
  noTimeText: {
    paddingVertical: 10
  },
  calenderTimeBoxWrap: {
    paddingVertical: 10,
  },
  calenderTimeBoxView: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  calenderTimeBoxText: {
    flex: 1,
    fontSize: 14,
    marginBottom: 4
  },
  calenderTimeBoxWrapRow: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  calenderTimeBoxRow: {
    width: "22%",
    textAlign: "center",
    marginVertical: "1.5%",
    marginHorizontal: "1.5%",
    borderWidth: 1,
    borderColor: "#CCCCCC",
  },
  calenderTimeBoxRowSelected: {
    width: "22%",
    textAlign: "center",
    marginVertical: "1.5%",
    marginHorizontal: "1.5%",
    borderWidth: 1,
    borderColor: Colors.color0,
  },
  calenderTimeBoxRowText: {
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    color: "#575757",
  },
  calenderTimeBoxRowTextSelected: {
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    color: Colors.color0,
  },
  buttonView: {
    flex: 1,
    flexDirection: "row-reverse",
    margin: 20,
  },
  checkoutButton: {
    backgroundColor: "#E1A77E",
    paddingVertical: 20,
    paddingHorizontal: 30,
    borderRadius: 8,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    alignSelf: "center",
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 15,
    fontWeight: "500",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    color: Colors.color0,
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  payment_method_items: {
    marginHorizontal: 15,
    marginVertical: 15,
  },
  WrapText: {
    flexDirection: "row",
  },
  LinkTextWrap: {
    flexDirection: "row-reverse",
  },
  paddingBottom: {
    paddingBottom: 10,
  },
  pay_later_method_title: {
    fontSize: 15,
    marginBottom: 15,
  },
  pay_later_method_desc: {
    fontSize: 12,
  },
  webview_desc: {
    height: 200,
    borderWidth: 1,
  },
  pay_later_method_desc_link: {
    fontSize: 12,
    color: "#0d73e0",
    textDecorationLine: "underline"
  },
  payment_amount_wrap: {
    paddingHorizontal: 10,
    marginVertical: 5
  },
  payment_amount: {
    marginVertical: 5,
    flex: 1,
    flexDirection: "row"
  },
  payment_amount_title: {
    fontSize: 12,
    fontWeight: "bold"
  },
  payment_amount_text: {
    marginLeft: 5,
    fontSize: 12,
  },
  payment_amount_text_promocode: {
    fontSize: 10,
    fontStyle: "italic",
    marginLeft: 5,
    marginTop: 2,
  },
  modal_pay_later_wrap: {
    paddingHorizontal: 0,
    paddingTop: 20,
    marginBottom: 10,
  },
  security_deposit_message: {
    paddingHorizontal: 12,
    paddingVertical: 12
  }
})
