
import React, { useState } from 'react';
import {
  TouchableOpacity,
  SafeAreaView,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import { SliderBox } from "react-native-image-slider-box";
import { global_styles } from '../../components/style';
export default function CardetailScreen(props){
  const [state, SetState] = useState({
     driver : props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.driver ? props.navigation.state.params.driver : null,
     requiredDeposit:props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.requiredDeposit ? props.navigation.state.params.requiredDeposit : null,
     carPk:props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.carPk ? props.navigation.state.params.carPk : null,
     carYear:props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.Year ? props.navigation.state.params.Year : null,
     carModel: props && props.navigation && props.navigation.state && props.navigation.state.params &&  props.navigation.state.params.Model ? props.navigation.state.params.Model : null,
     carColor:props && props.navigation && props.navigation.state && props.navigation.state.params &&  props.navigation.state.params.Color ? props.navigation.state.params.Color : null,
     maxPrice: props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.MaxValue ? props.navigation.state.params.MaxValue : null,
     minPrice: props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.MinValue ? props.navigation.state.params.MinValue : null,
     carDescription: props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.Description ? props.navigation.state.params.Description : null,
     carImages: props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.ImageArray ? props.navigation.state.params.ImageArray : null,
     interval:  props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.interval ? props.navigation.state.params.interval : null,
     intervalUnit:props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.intervalUnit ? props.navigation.state.params.intervalUnit : null,
     agreementType:props && props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.agreementType ? props.navigation.state.params.agreementType : null
  })

  const handleNavigationOnSchedule = () => {
    props.navigation.push('CarOnschedule', {
      driver: state.driver, price: state.minPrice, carPk: state.carPk, model: state.carModel, interval: state.interval,
      intervalUnit: state.intervalUnit, requiredDeposit: state.requiredDeposit, agreementType: state.agreementType
    })
  }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        <View style={{ marginTop: 5, }}>
          {state.carImages && state.carImages.length > 0 ?
            <SliderBox
              images={state.carImages}
              sliderBoxHeight={300}
              dotColor="#1b4d7e"
              dotStyle={{}}
              inactiveDotColor="#90A4AE"
              imageLoadingColor={"grey"}
              disableOnPress={false}
              autoplay={true}
              resizeMode="cover"
            />
            : <Image style={styles.imageitem} source={require('../../assets/images/no_image_found.png')} />}
        </View>
        <View style={{marginLeft:10, }}>
          <View style={{ margin: 10, justifyContent: 'center', }}>
            <Text style={[{ fontSize: 30, color: "#393e5c" },global_styles.lato_semibold]}>{state.carYear} {state.carModel} {'\n'}
            {state.carColor}</Text>
          </View>
          {state.maxPrice !== state.minPrice ?
            <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 10 }}>
              {state.intervalUnit && state.interval?
              <View style={{flexDirection:"row"}}>
              <Text style={[styles.car_price, { textDecorationLine: 'line-through', color: "#7c7c7c" }]}>${state.maxPrice}/{state.interval > 1 ? state.interval + state.intervalUnit.toLowerCase() : state.intervalUnit.toLowerCase()}</Text>
              <Text style={[styles.car_price, { color: "#db9360", marginLeft: 10 }]}>${state.minPrice}/{state.interval > 1 ? state.interval + state.intervalUnit.toLowerCase() : state.intervalUnit.toLowerCase()}</Text>
              </View>:null}
            </View>
            :
            state.intervalUnit && state.interval? 
            <Text style={[styles.car_price, { marginLeft: 10 }]}>${state.maxPrice}/{state.interval > 1 ? state.interval + state.intervalUnit.toLowerCase() : state.intervalUnit.toLowerCase()}</Text>:null}
          <View style={{ flexDirection: "column", marginTop: 10 }}>
            <View style={{ margin: 10,alignItems:"center",flexDirection:"row" }}>
              <Text style={{ fontWeight: "bold", fontSize: 15 }}>Other Features</Text></View>
            <View style={{ marginRight: 10, marginLeft: 10, marginBottom: 10 }}>
              <View>
                <Text style={{ fontSize: 12, fontFamily: "Lato-Regular", letterSpacing: 0.75 }}>{state.carDescription}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.button_item_right}>
          <TouchableOpacity underlayColor='' style={styles.buttonColor0} onPress={() =>
            handleNavigationOnSchedule()
          }>
            <Text style={[styles.car_see_detail]}>{"RESERVE NOW >"}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  car_price: {
    color: "#39AFD2",
    fontSize: 20,
    fontFamily: "Lato-Regular"
  },
  imageitem: {
    width: "100%",
    height: 150,
    resizeMode: 'contain',
    fontFamily: "Lato-Regular",
    marginTop: 10,
    marginBottom: 5
  },
  buttonColor0: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    backgroundColor: "#DB9360",
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10
  },
  button_item_right: {
    // flex: 0.3,
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginBottom: 20,
    marginTop: 15
  },
  car_see_detail: {
    color: "#fff",
    paddingVertical: 8,
    paddingHorizontal: 16,
    justifyContent: "center",
    textAlign: "center",
    fontSize: 15,
    fontFamily: "lato-medium"
  },
})