import React, { useEffect, useState } from 'react'; //Unused import function "useEffect"   
import {
  StyleSheet,
  ScrollView, //Unused import function "ScrollView"
  Text,
  Image,
  View,
  TouchableOpacity,
  Platform,
  TouchableHighlight
} from 'react-native';
import { translate } from '../../components/Language';
import moment from "moment";
import { global_styles } from '../../constants/Style';
import CarVector from 'react-native-vector-icons/FontAwesome5';
import Feather from  'react-native-vector-icons/Feather';

/**This class is intended to present the card layout on a dashboard screen to show the details of current rental car */
export default function ReservationPopupCurrentCar(props) {

  const [state, setState] = useState({
    isLoading: false,
    show: false
  });
  const { isLoading, show } = state;
  const [carImageUrl, setCarImageUrl] = useState('')

  useEffect(() => {
    console.log("current ag,.... ", props.currentAgreement)
    setCarImageUrl(props.currentAgreement.car.genericImageLink)
  }, [])

  const toggleListRental = () => {
    setState({ ...state, show: !show });
  }

  const handleNavigationOnSchedule = () => {
   
   props.navigation.push('CarOnschedule',{updateTime:"timeUpda"})
  }
  //Render the UI of current car rental details  
  return (
    <>
      {/* {props.hasOwnProperty('currentAgreement') ? */}
        <>
          <View style={styles.billing_list_item_wrap} >
                 
            <View style={styles.billing_list_item} >
              <View style={styles.billing_list_title_left} >
                <View style={styles.car_image_back}>
                  {/* <Image style={styles.car_image} source={{ uri: props.currentAgreement.car.genericImageLink }} /> */}
                  {carImageUrl != null && carImageUrl != '' && carImageUrl
                    ? <Image style={styles.car_image} source={{ uri: carImageUrl }} />
                    : <CarVector style={styles.vector_car_image} name='car-alt' />
                  }
                </View>
              </View>
              <View style={[styles.billing_list_price_right, { flexWrap: "wrap", maxWidth: 180, alignSelf: "flex-end", textAlign: "right" }]} >
                <Text style={[styles.billing_list_title, { textAlign: "right" }, global_styles.lato_medium]}>{props.currentAgreement.car.year} {props.currentAgreement.car.color} {props.currentAgreement.car.model}</Text>
                <Text style={[styles.billing_list_price_right_text, global_styles.lato_medium]}>{props.currentAgreement.car.color}</Text>
                <Text style={[styles.billing_list_price_right_text, global_styles.lato_medium]}>${props.currentAgreement.priceRange}/{props.currentAgreement.agreementType.name}</Text>
                 <View style={{flexDirection:"row"}}>
                  
                <Text style={[styles.rental_date, global_styles.lato_regular]}>{translate("car_pickupdate")}:{moment(props.currentAgreement.pickupDate).format("LLL")}</Text>
                 </View>
                <Text style={[styles.rental_date, global_styles.lato_regular]}>{"Pickup Location:"} {"445 Empire Blvd, Brooklyn, NY 11225"}</Text>
                
              </View>
            </View>


          </View>
        </>
        {/* : <View style={global_styles.activityIndicatorView}>
          <Text></Text>
        </View>} */}
    </>
  );
}

//StyleSheet of CurrentCar Rental History
const styles = StyleSheet.create({
  head_title: {
    color: '#000',
    fontSize: 15,
    paddingLeft: 0,
    justifyContent: "flex-start",
    paddingTop: 5,
    marginBottom: 0,
    paddingBottom: 5,
    backgroundColor: "#fff",
    borderColor: "#e6e6e6"
  },
  rental_date: {
    fontSize: 12,
    paddingTop: 5,
    color: "#393E5C"
  },
  billing_list_price_right_text: {
    fontSize: 14,
    marginBottom: 8,
    color: "#393E5C"
  },
  car_image_back: {
    width: 110,
    height: 110,
    borderRadius: 110 / 2,
    marginTop: 5,
    resizeMode: "contain",
    marginTop: 5,
    backgroundColor: '#F9F9F4',
    justifyContent: 'center',
    flex: 1,
  },
  car_image: {
    width: 90,
    resizeMode: "contain",
    height: 90,
    alignSelf: 'center',
  },
  vector_car_image: {
    fontSize: 56,
    alignSelf: 'center',
    color: '#ccc',
  },
  billing_list_item_wrap: {
    backgroundColor: '#FFFFFF',
    paddingStart: 15,
    paddingEnd: Platform.OS == 'ios' ? 0 : 15,
    paddingVertical: 5,
    zIndex: 10,
    alignItems:"center",marginRight:15
  },
  billing_list_item: {
    backgroundColor: '#fff',
    flexDirection: "row",
    width: "100%",
    alignItems:"center"
  },
  billing_list_title_left: {
    padding: 0,
    width: 100,
  },
  billing_list_price_right: {
    width: 400,
    position: "absolute",
    alignItems: "flex-end",
    right: 0,
    borderColor: "#333",
  },
  billing_list_title: {
    color: '#393E5C',
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 10,
  },
});
