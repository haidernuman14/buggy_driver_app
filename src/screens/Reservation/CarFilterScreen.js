import React, { useState, useEffect } from 'react';
import { ActivityIndicator, Dimensions, View, Text, SafeAreaView, StyleSheet, TouchableHighlight } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { useQuery } from '@apollo/react-hooks';
import { allCarModels } from '../../graphql/allCarModels';
import { allCarModelGroupTypes } from '../../graphql/allCarModelGroupTypes';
import Slider from '@react-native-community/slider';
import RNPickerSelect from 'react-native-picker-select';
import { global_styles, pickerSelectStyles } from '../../constants/Style';
import Colors from '../../constants/Colors';
const win = Dimensions.get('window');
function CarFilterScreen(props) {
  const [state, setState] = useState({
    isLoading: false,
    isFilter: false,
    shouldExecute: false,
    shouldExecute1: false,
    model_data: [],
    car_type_data: [],
    model: '',
    year: '',
    price: 0,
    _sort: '',
    car_type: ''
  });
  const { isLoading, isFilter, shouldExecute, shouldExecute1, model_data, car_type_data, model, year, price, car_type, _sort, errors } = state;
  const { data, loading, error } = useQuery(allCarModels, {
    variables: {
      isAvailable: true,
    },
    skip: !shouldExecute
  });
  const { data: data_car_type, loading: loading_car_type, error: error_car_type } = useQuery(allCarModelGroupTypes, {
    skip: !shouldExecute1
  });
  useEffect(() => {
    getDetail();
  }, [props.navigation.state])

  const getDetail = async () => {
    let filter = await AsyncStorage.getItem('filter');
    if (filter) {
      filter = JSON.parse(filter);
      setState({ ...state, isLoading: true, shouldExecute: true, isFilter: true, price: filter.price, year: filter.year, model: filter.model, _sort: filter.sort, car_type: filter.car_type })
    } else {
      setState({ ...state, isLoading: true, shouldExecute: true, })
    }
  }
  useEffect(() => {
    if (loading || error) {
      console.log('loading', loading)
      console.log(error)
      return;
    }
    if (data) {
      getData(data)
    }
  }, [data])

  useEffect(() => {
    if (loading_car_type || error_car_type) {
      console.log('loading_car_type', loading_car_type)
      console.log(error_car_type)
      return;
    }
    if (data_car_type) {
      getCarTypeData(data_car_type)
    }
  }, [data_car_type])

  const getData = async (data) => {
    const model_data = data.allCarModels.edges;
    let items = [];
    for (let i = 0; i < model_data.length; i++) {
      items.push({ label: model_data[i].node.name, value: model_data[i].node.pk });
    }
    items.sort(function (a, b) {
      if (a.label < b.label) { return -1; }
      if (a.label > b.label) { return 1; }
      return 0;
    })
    setState({ ...state, shouldExecute1: true, model_data: items });
  }
  const getCarTypeData = async (data_car_type) => {
    const car_type_data = data_car_type.allCarModelGroupTypes.edges;
    let items = [];
    for (let i = 0; i < car_type_data.length; i++) {
      items.push({ label: car_type_data[i].node.name, value: car_type_data[i].node.name });
    }
    items.sort(function (a, b) {
      if (a.label < b.label) { return -1; }
      if (a.label > b.label) { return 1; }
      return 0;
    })
    setState({ ...state, isLoading: false, car_type_data: items });
  }

  const years = () => {
    let years = [];
    const year = (new Date()).getFullYear();
    for (let i = 2010; i < year; i++) {
      years.push({ label: i.toString(), value: i });
    }
    return years;
  }
  const setValue = (key, value) => {
    setState({ ...state, [key]: value })
  }
  const SortView = () => {
    return (<>
      <View style={styles.sortWrap}>
        <Text style={styles.Title}>Sort By</Text>
        <View style={styles.cardWrap}>
          <TouchableHighlight underlayColor="" style={_sort == 'weekly_charge' ? styles.cardViewActive : styles.cardView} onPress={() => setValue('_sort', _sort == 'weekly_charge' ? '' : 'weekly_charge')}>
            <View style={{ flexDirection: "row" }}>
              <View>
                <Text style={styles.bold}>$ </Text>
              </View>
              <View>
                <Text>Price</Text>
                <Text>Low to High</Text>
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="" style={_sort == '-weekly_charge' ? styles.cardViewActive : styles.cardView} onPress={() => setValue('_sort', _sort == '-weekly_charge' ? '' : '-weekly_charge')}>
            <View style={{ flexDirection: "row" }}>
              <View>
                <Text style={styles.bold}>$ </Text>
              </View>
              <View>
                <Text>Price</Text>
                <Text>High to Low</Text>
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="" style={_sort == '-year' ? styles.cardViewActive : styles.cardView} onPress={() => setValue('_sort', _sort == '-year' ? '' : '-year')}>
            <View style={{ flexDirection: "row" }}>
              <View>
                <Text style={styles.bold}>Y </Text>
              </View>
              <View>
                <Text>Manufacturing</Text>
                <Text>Year</Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    </>
    );
  }
  const filterFun = async () => {
    await AsyncStorage.setItem('filter', JSON.stringify({ 'price': price, 'year': year, 'model': model, 'sort': _sort, 'car_type': car_type }));
    props.navigation.navigate('CarScreen', {
      sort: _sort,
      car_type: car_type,
      price: price,
      year: year,
      model: model,
      reset: false
    });
  }
  const resetFun = async () => {
    console.log('isFilter', isFilter)
    if (isFilter) {
      console.log('reset')
      await AsyncStorage.removeItem('filter');
      props.navigation.navigate('CarList', {
        price: '',
        year: '',
        model: '',
        sort: '',
        car_type: '',
        reset: true
      });
    } else {
      console.log('back')
      props.navigation.goBack();
    }
  }
  return (<>
    {
      isLoading ?
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> :
        <SafeAreaView style={styles.container}>
          <View style={styles.viewWraper}>
            {SortView()}
            <View style={styles.inputWrap}>
              <Text style={styles.Title}>Make & Model</Text>
              <View style={styles.inputView}>
                <View style={styles.inputView1}>
                  <RNPickerSelect
                    style={pickerSelectStyles}
                    useNativeAndroidPickerStyle={false}
                    mode="dropdown"
                    value={model}
                    placeholder={{ label: "Model", value: null }}
                    onValueChange={(value) => setValue('model', value)}
                    items={model_data}
                    Icon={() => {
                      return (
                        <View
                          style={pickerSelectStyles.icon}
                        />
                      );
                    }}
                  />
                </View>
                <View style={styles.inputView2}>
                  <RNPickerSelect
                    style={pickerSelectStyles}
                    useNativeAndroidPickerStyle={false}
                    mode="dropdown"
                    value={year}
                    placeholder={{ label: "Year", value: null }}
                    onValueChange={(value) => setValue('year', value)}
                    items={years()}
                    Icon={() => {
                      return (
                        <View
                          style={pickerSelectStyles.icon}
                        />
                      );
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={styles.categoryWrap}>
              <Text style={styles.Title}>Car Type</Text>
              <View>
                <RNPickerSelect
                  style={pickerSelectStyles}
                  useNativeAndroidPickerStyle={false}
                  mode="dropdown"
                  value={car_type}
                  placeholder={{ label: "Car Category", value: null }}
                  onValueChange={(value) => setValue('car_type', value)}
                  items={car_type_data}
                  Icon={() => {
                    return (
                      <View
                        style={pickerSelectStyles.icon}
                      />
                    );
                  }}
                />
              </View>
            </View>
            <View style={styles.priceWrap}>
              <Text style={styles.priceText}>Price: {(price != '') ? 'Up to $' + price + '/week' : ''}</Text>
              <View style={styles.priceSliderView}>
                <Text style={styles.priceSliderStartText}>$0</Text>
                <Text style={styles.priceSliderEndText}>$600</Text>
              </View>
              <Slider
                style={styles.priceSlider}
                onValueChange={(value) => setValue('price', value)}
                value={price}
                step={1}
                minimumValue={0}
                maximumValue={600}
                minimumTrackTintColor={Colors.color0}
                maximumTrackTintColor="#DDDDDD"
              />
            </View>

            <View style={styles.buttonView}>
              <TouchableHighlight underlayColor='' style={styles.checkoutButton} onPress={() => { filterFun() }}>
                <Text style={styles.checkoutButtonText}>APPLY</Text>
              </TouchableHighlight>
              <TouchableHighlight underlayColor='' style={styles.cancelButton} onPress={() => { resetFun() }}>
                <Text style={styles.cancelButtonText}>RESET</Text>
              </TouchableHighlight>
            </View>
          </View>
        </SafeAreaView>
    }
  </>
  );
}
export default CarFilterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewWraper: {
    flex: 1
  },
  sortWrap: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: "#EEEEEE",
  },
  Title: {
    fontSize: 16,
    marginBottom: 10
  },
  cardWrap: {
    flexDirection: "row",
    marginHorizontal: 5,
  },
  cardView: {
    marginHorizontal: 3,
    paddingHorizontal: 4,
    paddingVertical: 6,
    borderWidth: 1,
    borderColor: "#CCCCCC",
    borderRadius: 10
  },
  cardViewActive: {
    marginHorizontal: 3,
    paddingHorizontal: 4,
    paddingVertical: 6,
    borderWidth: 1,
    borderColor: Colors.color0,
    borderRadius: 10
  },
  bold: {
    fontSize: 18,
    fontWeight: "700"
  },
  inputWrap: {
    marginTop: 20,
    marginVertical: 10,
    marginHorizontal: 20,
  },
  inputView: {
    flexDirection: 'row',
  },
  inputView1: {
    flex: .7,
  },
  inputView2: {
    flex: .3,
  },
  categoryWrap: {
    marginVertical: 10,
    marginHorizontal: 20,
  },
  priceWrap: {
    marginVertical: 10,
    marginHorizontal: 20,
  },
  priceText: {
    marginVertical: 20,
    fontSize: 16,
  },
  priceSliderView: {
    flexDirection: "row"
  },
  priceSliderStartText: {
    flex: 1,
  },
  priceSliderEndText: {
    flexDirection: "row-reverse"
  },
  priceSlider: {
    width: win.width - 80,
    height: 40
  },
  buttonView: {
    marginTop: 20,
    flexDirection: "row-reverse",
    paddingHorizontal: 30,
  },
  checkoutButton: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    color: Colors.cancelText,
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  }
});