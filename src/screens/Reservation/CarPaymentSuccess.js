import React from 'react';
import { View, Text, Image, StyleSheet, ScrollView } from 'react-native';
import { translate } from '../components/Language';
import { global_styles } from '../constants/Style';
import Icon from 'react-native-vector-icons/FontAwesome'; 

export default function CarPaymentSuccessScreen(props) {
  return (
    <>
      <ScrollView>
        <View style={styles.main_wrapper}>
          <Text style={[styles.refer_driver_title, global_styles.opensans_semibold]}>Thank you!</Text>
          <Text style={[styles.refer_driver_sub_desc, global_styles.opensans_regular]}>Pay later option has been saved in your account.</Text>
        </View>
      </ScrollView>
    </>
  );
}



const styles = StyleSheet.create({
  main_wrapper: {
    backgroundColor: '#fff'
  },
  refer_driver_title: {
    fontSize: 15,
    color: "#454545",
    marginVertical: 18,
    paddingHorizontal: 20,
    marginBottom: 8
  },
  refer_driver_sub_desc: {
    fontSize: 12,
    color: "#454545",
    paddingHorizontal: 20,
    marginBottom: 7,
  },
  highlight_box: {
    marginHorizontal: 20,
    padding: 20,
    marginVertical: 7,
    justifyContent: "center",
    flex: 1,
    alignItems: "center",
    textAlign: "center",
    backgroundColor: "#f1f1f1",
    borderRadius: 5,
  },
  highlight_box_title: {
    textAlign: "center",
    fontSize: 16,
    marginBottom: 10,
    color: "#454545"
  },
  address_icon: {
    textAlign: "center",
    fontSize: 22,
    marginVertical: 10,
    color: "#454545"
  },
  highlight_box_desc: {
    textAlign: "center",
    fontSize: 12,
    color: "#454545"
  },
});