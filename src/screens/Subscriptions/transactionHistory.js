import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  Platform,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';
import moment from "moment";
import { global_styles } from '../../constants/Style';
import { useQuery } from '@apollo/react-hooks';
import { allTransactions } from '../../graphql/allTransactions';
import { allSubscriptions } from '../../graphql/allSubscriptionTypesTransactions';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Octicons';
import { FlatList } from 'react-native-gesture-handler';

const win = Dimensions.get('window');

export default function TransactionHistoryScreen(props) {

  //console.log('props.driverId', props.driverId)

  const [state, setState] = useState({
    isLoading: true,
    driverId: props.driverId,
    fresh_flag: true,
    data_transaction_list: [],
    data_transaction_pageinfo: {},
    amount_type: props.amount_type,
    start_date: props.start_date,
    end_date: props.end_date,
    cat_name: props.cat_name
  });

  const { amount_type, start_date, end_date, cat_name, isLoading, driverId, fresh_flag, data_transaction_list, data_transaction_pageinfo } = state;

  const [isNextPageLoading, setNextPageLoading] = useState(false);
  //console.log("props.cat_name>>>>>>>",props.cat_name,cat_name);
  ///////// Get Transaction ///////////
  let input_data = {};
  /*
  if (props.amount_type == "credit") {
    if (props.start_date != "" && props.end_date != "" && props.start_date != null && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = {
          amount_Gte: 0,
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = {
          amount_Gte: 0,
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 6
        };
      }
    } else if (props.start_date != "" && (props.end_date == "" || props.end_date == null) && props.start_date != null ) {
      if (props.cat_name != "") {
        input_data = {
          amount_Gte: 0,
          dueDate_Gte: props.start_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = {
          amount_Gte: 0,
          dueDate_Gte: props.start_date,
          driverId: driverId,
          first: 6
        };
      }
    } else if ((props.start_date == "" || props.start_date==null) && props.end_date != "" && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = {
          amount_Gte: 0,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = {
          amount_Gte: 0,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 6
        };
      }
    } else {
      if (props.cat_name != "") {
        input_data = {
          amount_Gte: 0,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = {
          amount_Gte: 0,
          driverId: driverId,
          first: 6
        };
      }
    }
  } else if (props.amount_type == "debit") {
    if (props.start_date != "" && props.end_date != "" && props.start_date != null && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = {
          amount_Lte: 0,
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = {
          amount_Lte: 0,
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 6
        };
      }
    } else if (props.start_date != "" && (props.end_date == "" || props.end_date == null) && props.start_date != null ) {
      if (props.cat_name != "") {
        input_data = {
          amount_Lte: 0,
          dueDate_Gte: props.start_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = {
          amount_Lte: 0,
          dueDate_Gte: props.start_date,
          driverId: driverId,
          first: 6
        };
      }
    } else if ((props.start_date == "" || props.start_date==null) && props.end_date != "" && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = {
          amount_Lte: 0,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = {
          amount_Lte: 0,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 6
        };
      }
    } else {
      if (props.cat_name != "") {
        input_data = {
          amount_Lte: 0,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = {
          amount_Lte: 0,
          driverId: driverId,
          first: 6
        };
      }
    }
  }else  {
    if (props.start_date != "" && props.end_date != "" && props.start_date != null && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = { 
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = { 
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 6
        };
      }
    } else if (props.start_date != "" && (props.end_date == "" || props.end_date == null) && props.start_date != null ) {
      if (props.cat_name != "") {
        input_data = { 
          dueDate_Gte: props.start_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = { 
          dueDate_Gte: props.start_date,
          driverId: driverId,
          first: 6
        };
      }
    } else if ((props.start_date == "" || props.start_date==null) && props.end_date != "" && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = { 
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = { 
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 6
        };
      }
    } else {
      if (props.cat_name != "") {
        input_data = { 
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 6
        };
      } else {
        input_data = { 
          driverId: driverId,
          first: 6
        };
      }
    }
  }*/
  input_data = { 
    driverId: driverId,
    first: 6
  };
  const { data: data_transaction, loading: loding_transaction, error: error_transaction, fetchMore } = useQuery(allSubscriptions, {
    variables: input_data,
    fetchPolicy: "network-only",
  });

  useEffect(() => {
    if (loding_transaction) {
      console.log('loding_transaction', loding_transaction)
    }
    if (data_transaction && data_transaction.allSubscriptions.edges.length > 0) { 
      if (fresh_flag) {
        setState({ ...state, isLoading: false, data_transaction_list: [...data_transaction.allSubscriptions.edges], data_transaction_pageinfo: data_transaction.allSubscriptions.pageInfo })
      } else {
        setState({ ...state, isLoading: false, data_transaction_list: [...data_transaction_list, ...data_transaction.allSubscriptions.edges], data_transaction_pageinfo: data_transaction.allSubscriptions.pageInfo })
      } 
    } else if (data_transaction && data_transaction.allSubscriptions.edges.length == 0) {
      setState({ ...state, isLoading: false })
    }
  }, [data_transaction])

  const handelMoreTransaction = (id) => {
    setState({ ...state, fresh_flag: false })
    fetchMore({
      variables: {
        driverId: driverId,
        first: 6,
        after: id
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        setNextPageLoading(false)
        return fetchMoreResult;
      }
    });
  }

  const loadMoreData = () => {
    if (data_transaction_pageinfo.hasNextPage != true || isNextPageLoading) { 
      return
    } 
    setNextPageLoading(true)
    handelMoreTransaction(data_transaction_pageinfo.endCursor)
  } 

  return (
    <><View><>
      <View style={styles.main_container}>
        <View style={styles.container}>
          <View style={styles.top_container}>
            <View style={[styles.loading_filter ]}>
                <Text style={[styles.heading_title, global_styles.lato_semibold]}>Transactions</Text>
            </View>
            {isLoading ?
              <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
                <ActivityIndicator size="large" style={{ padding: 60 }} />
              </View> : <>

                {data_transaction_list && data_transaction_list.length > 0 ?

                  <>
                   {data_transaction_list && data_transaction_list.length > 0 ?
                     <>
                      {data_transaction_list.map((item) => <>
                        {item.node.subscriptionPayments.edges.map((itm) =>
                          <View style={styles.list_item_container}>
                            <Text style={[itm.node.transaction.amount < 0 ? styles.amount_text : styles.amount_text_orange, global_styles.lato_regular]}>${itm.node.transaction.amount.toFixed(2)}</Text>
                            <View style={styles.list_item}>
                              <Text style={[styles.title_text, global_styles.lato_medium]}>{itm.node.transaction.chargeTypeDisplay}</Text>
                              <Text style={[styles.date_text, global_styles.lato_regular]}>{moment(itm.node.transaction.dueDate).format("MM/DD/YYYY")}</Text>
                            </View>
                          </View>)} 
                      </>)}
                      <View style={styles.loading_container}>
                        {isNextPageLoading && <ActivityIndicator size="small" />}
                      </View>
                      {data_transaction_pageinfo.hasNextPage == true ?
                        <View style={[global_styles.button_container, { justifyContent: "center", marginBottom: 20 }]}>
                          <TouchableOpacity onPress={loadMoreData}>
                            <View style={[global_styles.button_style_white_wrapper, { borderColor: '#ccc' }]}>
                              <Text style={[global_styles.button_style_white_text, { color: '#393e5c' }]}>SHOW MORE</Text>
                            </View>
                          </TouchableOpacity>
                        </View> : <></>}
                      </>:<></>
                    }
                  </>

                  : <View style={[  styles.padding_hr, global_styles.lato_regular]}><Text style={global_styles.lato_regular}>No transaction history exists.</Text></View>
                }
              </>}
          </View>
        </View>
      </View>
    </>
    </View></>
  );
}

const styles = StyleSheet.create({
  no_container: {
    flex: 1,
    alignItems: "center",
    marginTop:10,
    justifyContent: "center"
  },
  padding_hr:{
    marginLeft:18
  },
  main_container: { 
    flex: 1,
  },
  heading_title:{
    fontSize:18,
    color:"#000", 
  },
  container: {
    flex: 1,
    flexDirection: 'column-reverse',
  },
  top_container: {
    flex: 1,
  },
  bottom_container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  show_more_container: {
    paddingVertical: 8,
    paddingHorizontal: 32,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    margin: 16
  },
  loading_filter: {
    flexDirection: 'row',
    marginVertical:10,
    marginTop:20,
    paddingHorizontal:15,
    justifyContent: 'flex-start'
  }, 
  vector_icon: {
    fontSize: 18,
  },
  filter_image: {
    width: 24,
    height: 24,
  },
  filter: {
    marginStart: 8,
    fontSize: 16,
    color: '#393e5c',
  },
  list_item_container: {
    flexDirection: 'row-reverse', 
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8, 
  },
  list_item: {
    flex: 1,
  }, 
  title_text: {
    fontSize: 14,
    color: '#000',
    opacity: 0.6,
  },
  date_text: {
    fontSize: 13,
    marginTop: 4,
    color: '#000',
    opacity: 0.6,
  },
  amount_text: {
    fontSize: 16,
    color: '#393e5c',
  },
  amount_text_orange: {
    fontSize: 16,
    color: '#db9360'
  },
  loading_container: {
    flex: 1,
    justifyContent: 'center',
  },
  loading_indicator: {
    alignSelf: 'flex-start',
    marginBottom: 8,
  },
});
