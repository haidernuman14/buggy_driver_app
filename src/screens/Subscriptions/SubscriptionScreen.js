import React, { useState, useEffect, useContext } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    TouchableOpacity,
    ActivityIndicator,
    View,
    Keyboard, 
    Dimensions,
    Platform,
    Image,
    SafeAreaView,
} from 'react-native';
import { useMutation, useQuery } from '@apollo/react-hooks';
import analytics from '@react-native-firebase/analytics';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style } from '../../constants/Style';
import moment from "moment";
import Colors from '../../constants/Colors';
import { CheckConnectivity } from '../Helper/NetInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Octicons';
import { allTransactions } from '../../graphql/allTransactions';
import TransactionHistory from './transactionHistory'; 
import { updateSubscription } from '../../graphql/updateSubscription';
import Modal from "react-native-modal";
import { allSubscriptions } from '../../graphql/driverSubscription';

export default function SubscriptionScreen(props) {

    const [isLoading, setIsLoading] = useState(false);
    const [driverId, setDriverId] = useState('');
    const [user_object, setUser] = useState({});
    const [reloadDriverId, setReloadDriverId] = useState('');
    const [reload, setReload] = useState(false);
    const [reloadSubInfo, setReloadSubInfo] = useState(false);
    const [UpdateSubscription] = useMutation(updateSubscription);
    const [cancel_sub_id, setCancelSubId] = useState("");
    const [cancelError, setCancelError] = useState(false);
    const [state, setState] = useState({
        amount_type: "",
        start_date:"",
        end_date:"",
        cat_name:"",
        cancel_popup:false,
        confirm_cancel_popup:false,
        renew_popup:false,
        confirm_renew_popup:false,
        subscriptionInfo:null,
        data_allSubscriptions:null,
    });
    const {subscriptionInfo, data_allSubscriptions, renew_popup, confirm_renew_popup, confirm_cancel_popup, cancel_popup, amount_type, start_date, end_date, cat_name } = state;
 
    const checkNet = async () => {
        let netState = await CheckConnectivity();
        if (!netState) {
            props.screenProps.setNetModal(true)
            return;
        }
    }

    useEffect(() => {
        checkNet();
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
           // console.log('isFocused')
            setuserid();
        }

        const navFocusListener = props.navigation.addListener('didFocus', () => {
          //  console.log('didFocus')
            setuserid();
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);

    const { data: drivar_data, loading: drivar_data_loading, error: drivar_data_error } = useQuery(allSubscriptions, {
        variables: { driverId: reloadDriverId },
        fetchPolicy: "network-only",
        skip: reloadDriverId == ''
      });
    //   console.log("reloadDriverId-->>", reloadDriverId);
      useEffect(() => {
        if (drivar_data) {
            if (drivar_data && drivar_data.hasOwnProperty('allSubscriptions')) {  
                setReloadDriverId("");
                setReloadSubInfo(!reloadSubInfo);
                setState({ ...state, data_allSubscriptions:drivar_data.allSubscriptions});
            }
        }
     }); 

    const setuserid = async () => {
        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);
        setUser(user);
        setDriverId("");
        setDriverId(user.id);
        setReloadDriverId(user.id);

        let transaction_f = await AsyncStorage.getItem('transaction_f');
        if(transaction_f=="yes"){
            setReload(true);
        } else {
            setReload(false);
        }
        /////
        let amount_type = await AsyncStorage.getItem('amount_type');
        let start_date = await AsyncStorage.getItem('start_date');
        let end_date = await AsyncStorage.getItem('end_date');
        let cat_name1 = await AsyncStorage.getItem('cat_name'); 

        let c1 = JSON.parse(cat_name1);  
        if(cat_name1==null || cat_name1 == "" ||  c1.length==0 ){
            let new_cat_array = [5,45];
            cat_name1 = JSON.stringify(new_cat_array);
        }   

        let sub_info_1 = null; 
        if (user && user.hasOwnProperty('driverPtr') && user.driverPtr != null && user.driverPtr.hasOwnProperty('subscriptionSet') && user.driverPtr.subscriptionSet != null && user.driverPtr.subscriptionSet.edges.length > 0) {
            sub_info_1 =  user.driverPtr.subscriptionSet;
        }
        setState({ ...state, subscriptionInfo:sub_info_1, amount_type:amount_type, start_date:start_date, end_date:end_date, cat_name:cat_name1 });
    }

    const cancelSubscription = async ( amount, id ) => { 
        setCancelSubId(id);
        setState({ ...state, confirm_cancel_popup:true, cancel_popup:false}); 
    }
    
    const performCancelSubscription = async () => {
        let input = { 
            subscriptionId: cancel_sub_id,
            status: 0
          }
        //   console.log("updateSubscription Cancel Input Parameter:>>", input);
          setState({ ...state, confirm_cancel_popup:false});  
          setIsLoading(true);
          UpdateSubscription({
            variables: { input }
          }).then((response) => {
             
            if(response.data.updateSubscription.ok ) {
                setIsLoading(false);
                setCancelError(true);
                setReloadDriverId(user_object.id);
                setState({ ...state, confirm_cancel_popup:false, cancel_popup:true});  
            }
        });
    }

   /* const paymentMethod = async (amount,id) => { 
        await AsyncStorage.setItem('subscription_pay_amount', amount+"");
        await AsyncStorage.setItem('subscription_pay_id',id);
        props.navigation.navigate('SubscriptionPaymentPaynow');
    } */ 

    const renewSubscription = async ( amount, id ) => { 
        setCancelSubId(id);
        setState({ ...state, renew_popup:false, confirm_renew_popup:true}); 
    }

    const performRenewSubscription = async () => {
        let input = { 
            subscriptionId: cancel_sub_id,
            status: 1
          }
        //   console.log("updateSubscription Renew Input Parameter:>>", input);
          setState({ ...state, confirm_renew_popup:false });  
          setIsLoading(true);
          UpdateSubscription({
            variables: { input }
          }).then((response) => { 
            if(response.data.updateSubscription.ok ) {
                setIsLoading(false);
                setReloadDriverId(user_object.id);
                showTransactions();
                setState({ ...state, confirm_renew_popup:false, renew_popup:true});  
            }
        });
    }
    
    const showTransactions = async () => {
        setCancelError(false);
    }
    
    return (
        <>
             {isLoading?<View style={global_styles.activityIndicatorView}><ActivityIndicator size="large" style={{ padding: 60 }} /></View>:<></>} 
                    
                <SafeAreaView key={reload} style={styles.main_container}>
                    <View>
                    <ScrollView>
                      
                        <View  style={[styles.head_wrap ]}>
                            {cancelError?<Text style={[styles.heading_title,{fontSize:16}, global_styles.lato_semibold]}>Your subscription has been cancelled.</Text>:<Text style={[styles.heading_title, global_styles.lato_semibold]}>Current Subscriptions</Text>}
   
   
                            {(data_allSubscriptions!=null && data_allSubscriptions.edges.length > 0) ? <>
                              {data_allSubscriptions.edges.map((item)=> <>
                                <View style={[styles.sub_content_wrap ]}>
                                    <View style={[styles.sub_content_wrap_left ]}>
                                            <Image width="50" style={styles.icon_input} source={require("../../assets/images/subscription-left-icon.png")} />
                                    </View>
                                    <View style={[styles.sub_content_wrap_right ]}>
                                        <Text style={[styles.sub_content_heading, global_styles.lato_semibold]}>
                                            {item.node.subscriptionType.name} 
                                        </Text>
                                        <Text style={[styles.sub_content_status, global_styles.lato_regular]}>Status: {item.node.statusDisplay}</Text>
                                        <Text style={[styles.sub_content_date, global_styles.lato_regular]}>Date Added: {moment.utc(item.node.dateAdded).format("MMMM DD, yyyy")}</Text>
                                        {(item.node.statusDisplay!="Active")?<Text style={[styles.sub_content_date,{marginTop:5}, global_styles.lato_regular]}>Date Cancelled: {moment.utc(item.node.dateModified).format("MMMM DD, yyyy")}</Text>:<></>}
                                   </View> 
                                </View>
                                <View style={[(item.node.statusDisplay!="Active" || (!cancelError))?styles.view_car_btn_2:styles.view_car_btn]}>
                                    {(item.node.statusDisplay=="Active")?
                                    <TouchableOpacity underlayColor="" activeOpacity={0.8} style={[styles.cardocumentbutton, styles.cardocumentbutton_payment]} onPress={()=> cancelSubscription(item.node.subscriptionType.amount,item.node.id)}  >
                                        <Text style={[styles.buttonText, {width: 92, textAlign:"center" }, global_styles.lato_medium]}>CANCEL</Text>
                                    </TouchableOpacity>:
                                    <>{/* <TouchableOpacity underlayColor="" activeOpacity={0.8} style={[styles.cardocumentbutton, styles.cardocumentbutton_payment]} onPress={()=> renewSubscription(item.node.subscriptionType.amount,item.node.id)}  >
                                        <Text style={[styles.buttonText, {width: 80}, global_styles.lato_medium]}>RENEW</Text>
                                     </TouchableOpacity>*/}</>}
                                     {cancelError?
                                        <TouchableOpacity underlayColor="" activeOpacity={0.8} style={styles.cardocumentbutton} onPress={()=>showTransactions()}  >
                                            <Text style={[styles.buttonText, {width: 180, paddingHorizontal: 18,}, global_styles.lato_medium]}>SHOW TRANSACTIONS</Text>
                                        </TouchableOpacity>
                                    :<>{/*(item.node.statusDisplay=="Active")?<TouchableOpacity underlayColor="" activeOpacity={0.8} style={styles.cardocumentbutton} onPress={()=>paymentMethod(item.node.subscriptionType.amount,item.node.id)}  >
                                        <Text style={[styles.buttonText, {width: 180, paddingHorizontal: 28,}, global_styles.lato_medium]}>PAYMENT METHOD</Text>
                                     </TouchableOpacity>:<></>*/}</>}
                                </View>
                            </>)}
                        </>:<View style={{marginTop:8}}><Text style={global_styles.lato_regular}>No current subscription found.</Text></View>}
                                
                            
                        </View>
                        {(cancelError == false && driverId != '') ? <TransactionHistory key={reload} cat_name={cat_name} end_date={end_date} start_date={start_date} amount_type={amount_type} {...props} driverId={driverId} /> : null}
                    </ScrollView>

                    <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={(cancel_popup || confirm_cancel_popup )} >
                        <View style={modal_style.modal}> 
                            {cancel_popup && <>
                            
                            <View style={modal_style.modal_wrapper}>
                                <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => setState({...state, cancel_popup:false})}>
                                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                                </TouchableOpacity>
                                <Text style={modal_style.modal_heading}>Success</Text>
                                <Text style={modal_style.modalText}>Your subscription has been cancelled successfully.</Text>
                                <View style={modal_style.modal_textbuttonWrap_center}>
                                <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal}  onPress={() => setState({...state, cancel_popup:false})}>
                                    <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                                </TouchableOpacity>
                                </View>
                            </View>

                            </>}

                            {confirm_cancel_popup && <>
                            <View style={modal_style.modal_wrapper}>
                                    <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => setState({...state, confirm_cancel_popup:false})}>
                                    <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                                    </TouchableOpacity>
                                    <Text style={modal_style.modal_heading}>Warning!</Text>
                                    <Text style={modal_style.modalText}>If you cancel your CDW subscription, should you get into an accident or the car gets damaged, you will be responsible for the full deductible of $1000.</Text>
                                    <View style={modal_style.modal_textbuttonWrap}>
                                    <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={() => setState({...state, confirm_cancel_popup:false}) }>
                                        <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity underlayColor=""  style={modal_style.upload_document_modal}  onPress={() => performCancelSubscription()}>
                                        <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                                    </TouchableOpacity> 
                                    </View>
                                </View>
                            </>}


                        </View>
                        </Modal>


                    <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={(renew_popup || confirm_renew_popup )} >
                        <View style={modal_style.modal}> 
                            {renew_popup && <>
                            
                            <View style={modal_style.modal_wrapper}>
                                <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => setState({...state, renew_popup:false})}>
                                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                                </TouchableOpacity>
                                <Text style={modal_style.modal_heading}>Success</Text>
                                <Text style={modal_style.modalText}>Your subscription has been renewed successfully.</Text>
                                <View style={modal_style.modal_textbuttonWrap_center}>
                                <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal}  onPress={() => setState({...state, renew_popup:false})}>
                                    <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                                </TouchableOpacity>
                                </View>
                            </View>

                            </>}

                            {confirm_renew_popup && <>
                            <View style={modal_style.modal_wrapper}>
                                    <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => setState({...state, confirm_renew_popup:false})}>
                                    <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                                    </TouchableOpacity>
                                    <Text style={modal_style.modal_heading}>Warning!</Text>
                                    <Text style={modal_style.modalText}>Are you sure to renew subscription?</Text>
                                    <View style={modal_style.modal_textbuttonWrap}>
                                    <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={() => setState({...state, confirm_renew_popup:false}) }>
                                        <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity underlayColor=""  style={modal_style.upload_document_modal}  onPress={() => performRenewSubscription()}>
                                        <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                                    </TouchableOpacity> 
                                    </View>
                                </View>
                            </>}


                        </View>
                        </Modal>

                    </View>
                </SafeAreaView>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    view_car_btn: {
        flexDirection: "row",
        flex: 1,
        marginTop:13,  
        marginLeft:15,
        alignItems:"center",
        alignSelf:"center",
    },
    view_car_btn_2: {
        flexDirection: "row",
        flex: 1,
        marginTop:13,  
        marginLeft:15,
        alignItems:"flex-start",
        alignSelf:"flex-start",
    },
    cardocumentbutton: { 
        backgroundColor: "#fff",
        borderColor: "#DB9360",
        borderWidth: 1,
        marginRight:20,
        height: 35,
        borderRadius: 4,
    },
    buttonText: {
        color: "#393e5c",
        fontSize: 13,
        alignSelf: "center",
        flexDirection: "row",
        paddingVertical: 8,
        paddingHorizontal: 15,
    },
    sub_content_heading:{
        fontSize:16,
        color:'rgba(51, 54, 59, 0.6)'
    },
    sub_content_status:{
        fontSize:15,
        marginVertical:6,
        color:'rgba(51, 54, 59, 0.6)'
    },
    sub_content_date:{
        fontSize:15,
        color:'rgba(51, 54, 59, 0.6)'
    },
    sub_content_wrap:{
        flexDirection:"row",
        marginTop:15,
    },
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
    },
    sub_content_wrap_left:{
        width:80,
    },
    sub_content_wrap_right:{
        marginLeft:10
    },
    icon_input: {
        width: 80,
        height: 80,
        resizeMode: "contain"
    },
    head_wrap: { 
        marginVertical:10,
        marginTop:20,
        paddingHorizontal:15, 
    }, 
    heading_title:{
        fontSize:18,
        color:"#000", 
    },
    container: {
        flex: 1,
        flexDirection: 'column-reverse',
    },
    top_container: {
        flex: 1,
    },
    bottom_container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    show_more_container: {
        paddingVertical: 8,
        paddingHorizontal: 32,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 8,
        margin: 16
    },
    filter_container: {
        backgroundColor: '#fff',
        margin: 16,
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 8,
    },
    vector_icon: {
        fontSize: 18,
    },
    filter: {
        marginStart: 8,
        fontSize: 16,
    },
    list_item_container: {
        flexDirection: 'row-reverse',
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 8,
    },
    list_item: {
        flex: 1,
    },
    saperator: {
        backgroundColor: '#ccc',
        height: 0.5,
        alignSelf: 'stretch'
    },
    title_text: {
        fontSize: 14,
    },
    date_text: {
        fontSize: 12,
        marginTop: 4,
    },
    amount_text: {
        fontSize: 14,
        color: '#393e5c'
    },
    amount_text_orange: {
        fontSize: 14,
        color: '#db9360'
    },
})