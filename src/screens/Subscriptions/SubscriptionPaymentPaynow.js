import React, { useState, useEffect, useContext } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { StyleSheet, View, Text, Image,  TextInput, Button, Dimensions, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'; 
import { translate } from '../../components/Language';
import Icon2 from 'react-native-vector-icons/Feather';
import { global_styles,modal_style } from '../../constants/Style';
import moment from 'moment';
import Colors from '../../constants/Colors';
import { CheckConnectivity } from '../Helper/NetInfo';
import RadioForm from 'react-native-simple-radio-button';
import StripeScreen from '../Payment/StripeScreen'; 
import { driverBalance } from '../../graphql/driverPayAmount';
import { useMutation, useQuery } from '@apollo/react-hooks';
const win = Dimensions.get('window'); 
import { updateSubscription } from '../../graphql/updateSubscription.js';
import Modal from "react-native-modal";
export default function SubscriptionPaymentPaynow(props) {

    const [user, setUser] = useState({});
    const [isLoading, setIsLoding] = useState(true);
    const [driverId, setDriverId] = useState("");
    const [initReload, setInitReload] = useState(false);
    const [UpdateSubscription] = useMutation(updateSubscription);

    const [state, setState] = useState({
        userInfo: {},
        amount: 0,
        payamount: 0,
        success_subscription_popup:false,
        subscription_pay_id:"",
        selected_radio_option:0,
    });

    const { userInfo, subscription_pay_id,success_subscription_popup, amount,selected_radio_option, payamount } = state

    // Check internet connection
    const checkNet = async () => {
        let netState = await CheckConnectivity();
        if (!netState) {
            props.screenProps.setNetModal(true)
            return;
        }
    }

    // Setting up user detail
    const setuserid = async () => {

        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);  
        
        setUser(user);
        setInitReload(!initReload); 
        
        let subscription_pay_amount = await AsyncStorage.getItem('subscription_pay_amount'); 
        let subscription_pay_id1  = await AsyncStorage.getItem('subscription_pay_id'); 
        if(subscription_pay_amount != null && subscription_pay_amount != "" && subscription_pay_id1 != null){  
            setIsLoding(false);
            setState({ ...state, amount:parseFloat(subscription_pay_amount), subscription_pay_id:subscription_pay_id1});
        } 

    }

    // Set the driver id from the storage
    useEffect(() => {

        checkNet();
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            setuserid();
        }

        const navFocusListener = props.navigation.addListener('didFocus', () => {
            setuserid();
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);


    
  ///////////// Current Charge ///////////////
  const { data: drivar_data, loading: drivar_data_loading, error: drivar_data_error } = useQuery(driverBalance, {
    variables: { id: driverId },
    fetchPolicy: "network-only",
    skip: driverId == ''
  }); 
  useEffect(() => {
    if (drivar_data) {

      if (drivar_data && drivar_data.hasOwnProperty('driver')) {

        let current_amount = 0; 
        if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentCharge != null && drivar_data.driver.currentCharge !=  "" && drivar_data.driver.currentCharge > 0) { 
            current_amount = drivar_data.driver.currentCharge;
        }
        setState({ ...state, amount:current_amount});
        setIsLoding(false);
        setDriverId("");
        
      }
    }
  }, [drivar_data]);
  ///////////// End Current Charge //////////////
 

    // Navigate to Account List stack navigator
    const navigateToScreen = (screename) => {
         props.navigation.navigate((screename!="")?screename:"BillingPage");
    }
    // Navigate to Car Payment Screen
    const navigateToSuccess = (card_val) => {
       
         let input = { 
            subscriptionId: subscription_pay_id,
            paymentMethod: card_val,
            status: 1
          }
        //   console.log("updateSubscription Input Parameter:>>", input);

          UpdateSubscription({
            variables: { input }
          }).then((response) => {
             
            if(response.data.updateSubscription.ok ) {
                setState({ ...state, success_subscription_popup:true});  
            }
        });
        
        // console.log("Success payment");
        //setState({ ...state, payamount: "" });
        //props.navigation.navigate("CarPaymentSuccess");
    }
    const navigateScreenSubsciption = () => {
        setState({ ...state, success_subscription_popup:false});  
        props.navigation.navigate("Subscription");     
    }
     
    return (
        <View style={styles.main_container}>
            <KeyboardAwareScrollView  >
                <>
                    {isLoading ? 
                        <ActivityIndicator size="large" style={{ padding: 60 }} />
                        :
                        <> 
                        <View style={styles.card_method_list}>
                          <View key={initReload} style={styles.container_payment_amount}>
                               <StripeScreen initReload={initReload}  {...props} navigateToSuccess={navigateToSuccess} navigateToScreen={navigateToScreen} navigation={props.navigation} from={"dashboard"} amount={amount} />
                            </View>                   
                        </View>  
                     </>
                    }
                </>
                <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={success_subscription_popup} >
                    <View style={modal_style.modal}>
                        <View style={modal_style.modal_wrapper}>
                        <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => navigateScreenSubsciption()}>
                            <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                        </TouchableOpacity>
                        <Text style={modal_style.modal_heading}>Success</Text>
                        <Text style={modal_style.modalText}>Your payment was successful for your subscription.</Text>
                        <View style={modal_style.modal_textbuttonWrap_center}>
                            <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={() => navigateScreenSubsciption()}>
                            <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                            </TouchableOpacity>
                        </View>
                        </View>
                    </View>
                    </Modal>
            </KeyboardAwareScrollView>
        </View >
    );

}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1, 
    },
    card_method_list:{ 
       paddingHorizontal:0, 
       paddingBottom:100
    },
    
});
