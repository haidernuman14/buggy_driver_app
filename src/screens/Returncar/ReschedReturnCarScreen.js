import React, { useState, useEffect } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { View, Dimensions, Image, TouchableOpacity, ScrollView, Keyboard, Platform, TouchableWithoutFeedback, ActivityIndicator, Text, StyleSheet, TouchableHighlight } from 'react-native';
import Colors from '../../constants/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import CalendarPicker from 'react-native-calendar-picker';
import moment from 'moment';
import translate from '../../components/Language';
import Icon from 'react-native-vector-icons/FontAwesome'
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import { CheckConnectivity } from '../Helper/NetInfo';
import { TextField } from 'react-native-material-textfield';
import { changeReturn } from '../../graphql/changeReturn';
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { driverReturnCarDate } from '../../graphql/driverReturnCarDate';

let date = new Date();
let day = date.getDay();
let minDay = (day == 6) ? 1 : 0;
let maxDay = 20000000000000000000000000000000000;
const win = Dimensions.get('window');
export default function ReschedReturnCarScreen(props) {
  const [state, setState] = useState({
    apiMode: 'live',//sandbox
    isLoading: false,
    isTimeLoading: false,
    calenderShow: false,
    futureCalenderShow: false,
    userInfo: '',
    error_global: '',
    token: '',
    minDate: moment().add(minDay, 'day'),
    minFutureDate: moment().add((minDay + 1), 'day'),
    maxDate: moment().add(maxDay, 'day'),
    displayDate: 'MM/DD/YYYY', //moment().add(minDay, 'day').format('MM-DD-YYYY'),
    startDate: '',// moment().add(minDay, 'day').format('YYYY-MM-DD'),
    endDate: moment().add(minDay, 'day').format('YYYY-MM-DD'),
    reasonforreturn: 0,
    pick_up_future: 0,
    isCard: false,
    isRemoteCash: false,
    isZelle: false,
    isModalPayLater: false,
    error_paylater_method: "",
    error_return_reason: "",
    error_pick_up_future: "",
    error_payment_method: "",
    error_elsewheretext_other_detail: "",
    isPaymentMethod: false,
    rentalId: "",
    elsewheretext: "",
    success_rental: false,
    user_id: "",
    isModalPopup: false,
    is_rental_car: "",
    futureDate: moment().add((minDay + 1), 'day').format('YYYY-MM-DD'),
    displayFutureDate: moment().add((minDay + 1), 'day').format('MM-DD-YYYY'),
    return_car_stage: "",
    show_date_validation_popup: false,
    currentAgreementPrice: '0',
    error_start_date: "",
  });
  const { error_start_date, currentAgreementPrice, show_date_validation_popup, user_id, minFutureDate, is_rental_car, return_car_stage, isModalPopup, displayFutureDate, futureCalenderShow, futureDate, success_rental, rentalId, elsewheretext, error_elsewheretext_other_detail, isPaymentMethod, error_payment_method, error_return_reason, error_pick_up_future, reasonforreturn, pick_up_future, error_global, isLoading, calenderShow, minDate, maxDate, displayDate, startDate } = state;

  const [ChangeReturn] = useMutation(changeReturn);

  const [driverId, setDriverId] = useState("");
  const [returncarDriverId, setReturnCarDriverId] = useState("");
  const [carReturnId, setCarReturnId] = useState("");

  const [key, setKey] = useState(false);


  useEffect(() => {
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      udetail();
    }
    const navFocusListener = props.navigation.addListener('didFocus', () => {
      udetail();
    });
    return () => {
      navFocusListener.remove();
    };
  }, []);


  const udetail = async () => {
    const rental_car_id = await AsyncStorage.getItem('rental_car_id');
    const is_rental_car = await AsyncStorage.getItem('is_rentalnull');
    const return_carstage = await AsyncStorage.getItem('return_car_stage');

    let user = await AsyncStorage.getItem('user');

    if (user) {
      user = JSON.parse(user);
      setDriverId(user.id);
      setReturnCarDriverId(user.id);
    }
    // console.log("is_rental_car))))",is_rental_car);
    // console.log("rental_car_id))))",rental_car_id);
    // console.log("return_carstage))))",return_carstage);

    let carAmount = await AsyncStorage.getItem('carAmount');
    if (rental_car_id && is_rental_car && return_carstage) {
      setState({ ...state, currentAgreementPrice: (carAmount && carAmount != null) ? carAmount : '0', return_car_stage: return_carstage, rentalId: rental_car_id, is_rental_car: (is_rental_car == "yes") ? true : false });
    }
  }


  ///////// Get Return Car ///////////
  const { data: data_returncar, loading: drivar_returncar_loading, error: drivar_returncar_error } = useQuery(driverReturnCarDate, {
    variables: { id: returncarDriverId },
    fetchPolicy: "network-only",
    skip: returncarDriverId == ''
  });

  useEffect(() => {
    if (data_returncar && data_returncar.hasOwnProperty('driver')) {
      // console.log("return_reason --->", data_returncar.driver);
      if (data_returncar && data_returncar.hasOwnProperty('driver') && data_returncar.driver.currentAgreement != null) {
        if (data_returncar.driver.currentAgreement.hasOwnProperty('carreturn') && data_returncar.driver.currentAgreement.carreturn != null) {
          let return_car_id = data_returncar.driver.currentAgreement.carreturn.id;
          setCarReturnId(return_car_id);
          setReturnCarDriverId("");
        }
      }
      setReturnCarDriverId("");
    }
  }, [data_returncar]);



  //if (return_car_driver_id)
  //console.log("return_car_driver_id is ", return_car_driver_id);
  //////////// End return car /////////////////

  /*

 useEffect(() => {
   const status = checkNet();
   if (user_id == "")
     udetail();  
 }, [Math.random()]); */


  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return false;
    }
    return true;
  }


  const openCalender = () => {
    setState({ ...state, calenderShow: !calenderShow })
  }

  const openFutureCalender = () => {
    setState({ ...state, futureCalenderShow: !futureCalenderShow })
  }

  const onDateChange = (date) => {
    let setDate = moment(date).format('YYYY-MM-DD');
    let setDisplayDate = moment(date).format('MM-DD-YYYY');
    //console.log(setDate)
    setState({ ...state, error_start_date: "", show_date_validation_popup: (moment().diff(setDate, 'days') >= -5 ? true : false), futureDate: moment(date).add(1, 'day').format('YYYY-MM-DD'), displayFutureDate: moment(date).add(1, 'day').format('MM-DD-YYYY'), minFutureDate: moment(date).add(1, 'day'), isTimeLoading: true, displayDate: setDisplayDate, startDate: setDate, endDate: setDate, calenderShow: false })
  }
  /*
  const onFutureDateChange = (date) => {
    let setDate = moment(date).format('YYYY-MM-DD'); 
    let setDisplayDate = moment(date).format('MM-DD-YYYY');
    
    setState({ ...state, displayFutureDate: setDisplayDate, futureDate:setDate, futureCalenderShow:false });
  }*/


  const checkoutFun = async () => {

    setState({ ...state, isLoading: true });

    let input = {
      "carReturnId": carReturnId,
      "rental": rentalId,
      "scheduledDate": startDate + "T09:30"
    }

    // console.log("input2---",input);

    ChangeReturn({
      variables: { input }
    }).then((response) => {
      if (response.data.changeReturn.ok) {
        setState({ ...state, isLoading: false, return_car_stage: "A_30", is_rental_car: false, isModalPopup: false, success_rental: true });
        AsyncStorage.setItem('return_car_stage', "A_30");
        AsyncStorage.setItem('is_rentalnull', "yes");
        AsyncStorage.setItem('return_date', startDate);
      } else {
        let error_m = "";
        if (response.data.changeReturn.errors.length > 0) {
          if (response.data.changeReturn.errors[0].messages.length > 0) {
            error_m = response.data.changeReturn.errors[0].messages[0];
          }
        }
        setState({ ...state, error_global: error_m, isLoading: false, isModalPopup: false, success_rental: false });
      }
    }).catch((error) => {
      setState({ ...state, isLoading: false });
      // console.log("er---",error);
    });


  }


  /*
  const setReasonForReturn = async (val) => {
    setState({ ...state, error_return_reason:"", reasonforreturn:val, pick_up_future:0, isPaymentMethod:false,error_elsewheretext_other_detail:"" });
  }
  
  let reason_for_return_options = [
    { label: "Select Reason for Return" + '   ', value: "" },
    { label: "Vacation " + '    ', value: 1 },
    { label: "Renting Elsewhere" + '   ', value: 2 },
    { label: "Requested by Buggy" + '   ', value: 3 },
  ];
  
  let pick_up_future_options = [
    { label: "Select option for future pickup" + '   ', value: "" },
    { label: "Yes" + '   ', value: 1 },
    { label: "No" + '   ', value: 2 }, 
  ]; 

  const setDriverPickUpInFuture = async (val) => {
    let paymentmethod = false;
    if( val == 1 ) {
      paymentmethod = true;
    }
    setState({ ...state, isPaymentMethod: paymentmethod, pick_up_future:val });
  } 
  */

  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value });
  }

  const closePopup = () => {
    setState({ ...state, isLoading: false, is_rental_car: false, success_rental: false });
    props.navigation.navigate("ReturnSwitchConnector");

  }

  const hidemakePayment = () => {
    setState({ ...state, isModalPopup: false, success_rental: false });
  }

  const openconfirm = () => {
    let future_error = "";
    let paym_error = "";
    let paylater_method = "";
    let elsewheretext_other_detail = "";

    let is_err = 0;
    let is_paid = 0;
    let reason_error = "";
    let error_startdate = "";

    if (startDate == "") {
      is_err = 1;
      error_startdate = "Please choose a return date.";
    } else {
      error_startdate = "";
    }

    setState({ ...state, error_start_date: error_startdate, error_elsewheretext_other_detail: elsewheretext_other_detail, error_paylater_method: paylater_method, error_payment_method: paym_error, error_return_reason: reason_error, error_pick_up_future: future_error });

    if (is_err == 0) {
      setState({ ...state, isModalPopup: true, success_rental: false });
    }
  }
  //let platformStyle = Platform.OS === 'ios' ? styles.iosStyle : styles.androidStyle;
  /*
  console.log("return_car_stage>>",return_car_stage);
  const goBacktoDashboard = () => {
    //console.log(props)
    props.navigation.navigate('AccountList');
  }
  console.log("is_rental_car---",is_rental_car); */

  const hideReturnDateValidatePopup = () => {
    setState({ ...state, displayDate: 'MM-DD-YYYY', startDate: '', show_date_validation_popup: false, calenderShow: true });
  }

  const selectYesValidatePopup = () => {
    setState({ ...state, error_start_date: "", show_date_validation_popup: false });
  }

  return (
    <View style={styles.main_container}>
      <KeyboardAwareScrollView contentContainerStyle={(Platform.OS == "web") ? { flexGrow: 1, height: (win.height - 100) } : { flex: 1 }} >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <ScrollView keyboardShouldPersistTaps="always">
            <>

              <View style={styles.main_header_content}></View>
              <View style={[styles.container, styles.container_header]}>
                {isLoading || carReturnId == "" ?
                  <View style={[styles.calenderView, { paddingVertical: 200 }]}>
                    <View style={global_styles.activityIndicatorView}>
                      <ActivityIndicator size="small" style={{ marginTop: 0 }} />
                    </View>
                  </View> :
                  <View>
                    {(rentalId != "" && carReturnId != "") ?
                      <>
                        <Text style={[global_styles.labelStyle, global_styles.lato_regular]}>CHOOSE DATE</Text>
                        <View style={global_styles.inputWrap_username}>
                          <View style={{ flexDirection: "row", backgroundColor: "#FFF", borderWidth: 1, borderRadius: 5, borderColor: "#d4d4d4", paddingHorizontal: 8, paddingVertical: 8, paddingBottom: 5, flex: 1, width: "100%", paddingLeft: 10 }}>
                            <TouchableHighlight underlayColor='' onPress={() => openCalender()} style={styles.calenderDate}><Text style={[styles.calenderDateText, global_styles.lato_regular]}>{displayDate}</Text></TouchableHighlight>
                            <TouchableHighlight underlayColor='' onPress={() => openCalender()} style={styles.calenderIcon}><Image width={25} style={styles.cal_image} source={require('../../assets/images/cal-icon.png')} /></TouchableHighlight>
                          </View>
                        </View>

                        <View style={calenderShow ? styles.calenderShow : styles.calenderHide}>
                          <CalendarPicker
                            previousTitleStyle={{ marginLeft: 15 }}
                            nextTitleStyle={{ marginRight: 15 }}
                            dayLabelsWrapper={{ marginLeft: 20 }}
                            minDate={minDate}
                            maxDate={maxDate}
                            disabledDates={(date) => {
                              let type = date.day();
                              return (type == 6 || type == 0) ? date : '';
                            }}
                            selectedStartDate={startDate}
                            onDateChange={(value) => onDateChange(value)}
                          />
                        </View>
                        <Text style={[styles.errorText, global_styles.lato_regular]}>{error_start_date}</Text>

                        <View style={styles.buttonView}>
                          <TouchableOpacity style={[global_styles.bottom_style_orange_wrapper]} onPress={() => { openconfirm(); }} >
                            <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>SUBMIT</Text>
                          </TouchableOpacity>
                        </View>

                        {error_global != '' && <Text style={styles.errorText}>{error_global}</Text>}

                        <Modal isVisible={(success_rental || isModalPopup)} >
                          <View style={modal_style.modal}>

                            {isModalPopup && <>
                              <View style={modal_style.modal_wrapper}>
                                <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hidemakePayment(false)}>
                                  <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                                </TouchableOpacity>
                                <Text style={modal_style.modal_heading}>Warning!</Text>
                                <Text style={[modal_style.modalText, global_styles.lato_regular]}>Would you like to confirm your return?</Text>
                                <View style={modal_style.modal_textbuttonWrap}>
                                  <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hidemakePayment(false)}>
                                    <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Go Back</Text>
                                  </TouchableHighlight>
                                  <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => checkoutFun()}>
                                    <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Accept</Text>
                                  </TouchableHighlight>
                                </View>
                              </View>
                            </>}

                            {success_rental && <>
                              <View style={modal_style.modal_wrapper}>
                                <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => closePopup()}>
                                  <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                                </TouchableOpacity>
                                <Text style={modal_style.modal_heading}>Success</Text>
                                <Text style={[modal_style.modalText, global_styles.lato_regular]}>Congratulations, your return schedule details has been sent successfully.</Text>
                                <View style={modal_style.modal_textbuttonWrap_center}>
                                  <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => closePopup()}>
                                    <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>OK</Text>
                                  </TouchableHighlight>
                                </View>
                              </View>
                            </>
                            }
                          </View>
                        </Modal>

                      </> : <>
                        {(return_car_stage == "") && <ActivityIndicator size="small" style={{ padding: 60 }} />}

                        {(rentalId == null) && <View style={[styles.list_container, { padding: 14 }]}>
                          <Text style={[{ paddingHorizontal: 15, fontSize: 14, paddingVertical: 20 }, global_styles.lato_regular]} >There is no car reserved in your account.</Text>
                        </View>}

                      </>}
                  </View>
                }

              </View>

              <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={(show_date_validation_popup)} >
                <View style={modal_style.modal}>
                  <View style={modal_style.modal_wrapper}>
                    <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hideReturnDateValidatePopup()}>
                      <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                    </TouchableOpacity>
                    <Text style={modal_style.modal_heading}>Warning!</Text>
                    <Text style={[modal_style.modalText, global_styles.lato_regular]}>
                      Buggy has a 'one week notice' return policy. If you return on this date you will be charged an early return fee of ${currentAgreementPrice}. To avoid this fee please go back and choose a later date. To continue with this date and pay the fee click accept.
                            </Text>
                    <View style={modal_style.modal_textbuttonWrap}>

                      <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hideReturnDateValidatePopup()}>
                        <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Go Back</Text>
                      </TouchableHighlight>
                      <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => selectYesValidatePopup()}>
                        <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Accept</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </View>
              </Modal>
            </>
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAwareScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  main_wrapper: {
  },
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 25,
    paddingTop: 22,
    paddingBottom: 12,
  },
  container: {
    flex: 1,
  },
  cal_image: {
    width: 25,
    resizeMode: "contain",
    marginTop: -3,
    alignItems: "center",
  },
  save_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    marginStart: 16,
    borderColor: '#db9360'
  },
  save_text: {
    color: '#FFFFFF',
  },
  container: {
    flex: 1,
  },
  calenderView: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    paddingBottom: 5,
  },
  calenderText: {
    fontSize: 14,
    color: "#474747",
    lineHeight: 20,
  },
  calenderDateView: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 8,
    backgroundColor: "#FFF"
  },
  list_container: {
    marginVertical: 0,
    marginHorizontal: 15,
    paddingBottom: 10,
    marginBottom: 0,
  },
  calenderDate: {
    flex: 1,
  },
  calenderDateText: {
    fontSize: 14,
    marginTop: 3
  },
  calenderIcon: {
    flexDirection: "row-reverse"
  },
  calenderShow: {
    zIndex: 10,
    display: "flex"
  },
  calenderHide: {
    display: "none"
  },
  calenderTimeView: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 8,
  },
  calenderTimeText: {
    fontSize: 16,
  },
  errorText: {
    color: "red",
    paddingVertical: 5,
    paddingHorizontal: 0
  },
  noTimeText: {
    paddingVertical: 10
  },
  calenderTimeBoxWrap: {
    paddingVertical: 10,
  },
  calenderTimeBoxView: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  calenderTimeBoxText: {
    flex: 1,
    fontSize: 14,
    marginBottom: 4
  },
  calenderTimeBoxWrapRow: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  calenderTimeBoxRow: {
    width: "22%",
    textAlign: "center",
    marginVertical: "1.5%",
    marginHorizontal: "1.5%",
    borderWidth: 1,
    borderColor: "#CCCCCC",
  },
  calenderTimeBoxRowSelected: {
    width: "22%",
    textAlign: "center",
    marginVertical: "1.5%",
    marginHorizontal: "1.5%",
    borderWidth: 1,
    borderColor: Colors.color0,
  },
  calenderTimeBoxRowText: {
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    color: "#575757",
  },
  calenderTimeBoxRowTextSelected: {
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    color: Colors.color0,
  },
  buttonView: {
    flex: 1,
    alignItems: "flex-end",
    marginTop: 0
  },
  checkoutButton: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    color: Colors.color0,
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  payment_method_items: {
    marginHorizontal: 15,
    marginVertical: 5,
  },
  iosStyle: {
    flex: 1,
    marginLeft: 0,
    marginTop: 0,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  androidStyle: {
    flex: 1,
    flexDirection: "row",
    marginLeft: -10,
    marginTop: -5,
    paddingVertical: 10,
    paddingHorizontal: 10,
  }
})
