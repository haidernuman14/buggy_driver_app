import React, { useState, useEffect } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { View, TextInput, Image, Platform, ActivityIndicator, Text, StyleSheet, TouchableOpacity, TouchableHighlight } from 'react-native';
import Colors from '../../constants/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import CalendarPicker from 'react-native-calendar-picker';
import moment from 'moment';
import translate from '../../components/Language';
import Icon from 'react-native-vector-icons/FontAwesome'
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import { CheckConnectivity } from '../Helper/NetInfo';
import RNPickerSelect from 'react-native-picker-select';
import { TextField } from 'react-native-material-textfield';
import { scheduleReturnForDriver } from '../../graphql/scheduleReturnForDriver';
import Modal from "react-native-modal";

let date = new Date();
let day = date.getDay();
let minDay = (day == 6) ? 1 : 0;
let maxDay = 20000000000000000000000000000000000;
export default function ReturnCarScreen(props) {
  const [state, setState] = useState({
    isLoading: false,
    calenderShow: true,
    futureCalenderShow: false,
    error_global: '',
    token: '',
    minDate: moment().add(minDay, 'day'),
    minFutureDate: moment().add((minDay + 1), 'day'),
    maxDate: moment().add(maxDay, 'day'),
    displayDate: 'MM/DD/YYYY',// moment().add(minDay, 'day').format('MM-DD-YYYY'),
    startDate: '',// moment().add(minDay, 'day').format('YYYY-MM-DD'),
    endDate: moment().add(minDay, 'day').format('YYYY-MM-DD'),
    reasonforreturn: 0,
    pick_up_future: 0,
    isModalPayLater: false,
    error_paylater_method: "",
    error_return_reason: "",
    error_pick_up_future: "",
    error_payment_method: "",
    error_elsewheretext_other_detail: "",
    error_other_reason: "",
    isPaymentMethod: false,
    rentalId: (props.rentalId) ? props.rentalId : "",
    elsewheretext: "",
    success_rental: false,
    reasonforreturn_index: "",
    user_id: "",
    other_reason: "",
    error_start_date: "",
    isModalPopup: false,
    is_rental_car: (props.is_rental_car) ? props.is_rental_car : "",
    futureDate: '', // moment().add((minDay+1), 'day').format('YYYY-MM-DD'),
    displayFutureDate: 'MM/DD/YYYY',//moment().add((minDay+1), 'day').format('MM-DD-YYYY'),
    return_car_stage: (props.return_carstage) ? props.return_carstage : "",
    show_date_validation_popup: false,
    currentAgreementPrice: '',
  });
  const { user_id, currentAgreementPrice, minFutureDate, error_start_date, show_date_validation_popup, error_other_reason, other_reason, reasonforreturn_index, is_rental_car, return_car_stage, isModalPopup, displayFutureDate, futureCalenderShow, futureDate, success_rental, rentalId, elsewheretext, error_elsewheretext_other_detail, isPaymentMethod, error_payment_method, error_return_reason, error_pick_up_future, reasonforreturn, pick_up_future, error_global, isLoading, calenderShow, minDate, maxDate, displayDate, startDate } = state;

  const [ScheduleReturnForDriver] = useMutation(scheduleReturnForDriver);

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return false;
    }
    return true;
  }

  useEffect(() => {
    checkNet();
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      // console.log("F=1");
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      // console.log("F=2");
      setuserid();
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const setuserid = async () => {
    let currentAgreementDetail = await AsyncStorage.getItem('currentAgreement');
    if (currentAgreementDetail) {
      let c_details = JSON.parse(currentAgreementDetail);
      if (c_details != null) {
        setState({ ...state, currentAgreementPrice: c_details.weeklyCharge });
      }
    }
  }


  const openCalender = () => {
    setState({ ...state, calenderShow: !calenderShow })
  }

  const openFutureCalender = () => {
    setState({ ...state, futureCalenderShow: !futureCalenderShow })
  }

  const onDateChange = (date) => {
    let setDate = moment(date).format('YYYY-MM-DD');
    let setDisplayDate = moment(date).format('MM-DD-YYYY');
    //futureDate:moment(date).add(1,'day').format('YYYY-MM-DD'), displayFutureDate: moment(date).add(1,'day').format('MM-DD-YYYY') ,
    setState({ ...state, error_start_date: "", futureDate: "", displayFutureDate: 'MM/DD/YYYY', show_date_validation_popup: (moment().diff(setDate, 'days') >= -5 ? true : false), minFutureDate: moment(date).add(1, 'day'), displayDate: setDisplayDate, startDate: setDate, endDate: setDate, calenderShow: false, error_global: '' })
  }

  const onFutureDateChange = (date) => {
    let setDate = moment(date).format('YYYY-MM-DD');
    let setDisplayDate = moment(date).format('MM-DD-YYYY');

    setState({ ...state, displayFutureDate: setDisplayDate, futureDate: setDate, futureCalenderShow: false });
  }


  const checkoutFun = async () => {

    setState({ ...state, isLoading: true });


    let input = {
      "rentalId": rentalId,
      "returnReason": (reasonforreturn == 1 ? "Vacation" : (reasonforreturn == 3 ? "Requested by Buggy" : (reasonforreturn == 2 ? escape(elsewheretext) : (reasonforreturn == 5 ? escape(other_reason) : getvaluefromarray(reasonforreturn))))),
      "returnDate": startDate,
      "ignoreUnscheduledWarning": true
    }

    if (pick_up_future == 1) {
      input = {
        "rentalId": rentalId,
        "returnReason": (reasonforreturn == 1 ? "Vacation" : (reasonforreturn == 3 ? "Requested by Buggy" : (reasonforreturn == 2 ? escape(elsewheretext) : (reasonforreturn == 5 ? escape(other_reason) : getvaluefromarray(reasonforreturn))))),
        "returnDate": startDate,
        "ignoreUnscheduledWarning": true,
        "futurePickupDate": futureDate
      }
    }

    console.log("input2---", input);

    ScheduleReturnForDriver({
      variables: { input }
    }).then((response) => {
      // console.log(response.data.scheduleReturnForDriver);
      if (response.data.scheduleReturnForDriver.ok) {

        AsyncStorage.setItem('return_date', startDate);
        setState({ ...state, isLoading: false, return_car_stage: "A_30", is_rental_car: false, isModalPopup: false, success_rental: true });
        AsyncStorage.setItem('is_rentalnull', 'no');
        AsyncStorage.setItem('return_car_stage', "A_30");
        // AsyncStorage.setItem('rental_car_id', "");
        props.refreshPage(0, 1);
      } else {
        let error_m = "";
        if (response.data.scheduleReturnForDriver.errors.length > 0) {
          if (response.data.scheduleReturnForDriver.errors[0].messages.length > 0) {
            error_m = response.data.scheduleReturnForDriver.errors[0].messages[0];
          }
        }
        setState({ ...state, error_global: error_m, isLoading: false, isModalPopup: false, success_rental: false });
      }
    }).catch((error) => {
      setState({ ...state, isLoading: false });
      // console.log("er---", error);
    });


  }

  const getvaluefromarray = (val) => {

    let return_options = [
      { label: "Select", value: "" },
      { label: "Vacation", value: 1 },
      { label: "Too Expensive", value: 10 },
      { label: "Renting Elsewhere", value: 2 },
      { label: "I got a better job", value: 18 },
      { label: "My car is out of the shop", value: 13 },
      { label: "COVID-19", value: 6 },
      { label: "Emergency", value: 19 },
      { label: "Other", value: 5 },
    ];

    let final_v = "";
    return_options.map((item) => {
      if (item.value == val) {
        final_v = item.label;
      }
    });
    return final_v;
  }

  const setReasonForReturn = async (val, index) => {
    setState({ ...state, reasonforreturn: val, reasonforreturn_index: index, pick_up_future: 0, isPaymentMethod: false, error_return_reason: "", error_elsewheretext_other_detail: "" });
  }

  let reason_for_return_options = [
    { label: "Select" + '   ', value: "" },
    { label: "Vacation                " + '    ', value: 1 },
    { label: "Too Expensive                " + '    ', value: 10 },
    { label: "Renting Elsewhere       " + '   ', value: 2 },
    { label: "I got a better job                " + '    ', value: 18 },
    { label: "My car is out of the shop                " + '    ', value: 13 },
    { label: "COVID-19                " + '    ', value: 6 },
    { label: "Emergency               " + '    ', value: 19 },
    { label: "Other        " + '               ', value: 5 },
  ];

  let pick_up_future_options = [
    { label: "Select option for future pickup" + '   ', value: "" },
    { label: "Yes" + '   ', value: 1 },
    { label: "No" + '   ', value: 2 },
  ];


  const setDriverPickUpInFuture = async (val) => {
    let paymentmethod = false;
    if (val == 1) {
      paymentmethod = true;
    }
    setState({ ...state, error_pick_up_future: "", isPaymentMethod: paymentmethod, pick_up_future: val });
  }


  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value });
  }

  const closePopup = () => {
    setState({ ...state, isLoading: false, is_rental_car: false, success_rental: false });
    //props.navigation.navigate('ReturnCar');  
  }

  const hidemakePayment = () => {
    setState({ ...state, isModalPopup: false, success_rental: false });
  }

  const openconfirm = () => {
    let future_error = "";
    let paym_error = "";
    let paylater_method = "";
    let elsewheretext_other_detail = "";

    let is_err = 0;
    let is_paid = 0;
    let reason_error = "";
    let error_startdate = "";

    if (startDate == "") {
      is_err = 1;
      error_startdate = "Please choose a return date.";
    } else {
      error_startdate = "";
    }

    if (reasonforreturn == 0 || reasonforreturn == "") {
      is_err = 1;
      reason_error = "Please select the reason for return car.";
    } else {
      reason_error = "";
    }

    if (reasonforreturn == 2 && (elsewheretext == "" || elsewheretext == 0)) {
      is_err = 1;
      elsewheretext_other_detail = "Please enter other details.";
    } else {
      elsewheretext_other_detail = "";
    }
    let error_other_reason = "";
    if (reasonforreturn == 5 && (other_reason == "" || other_reason == 0)) {
      is_err = 1;
      error_other_reason = "Please enter other reason.";
    } else {
      error_other_reason = "";
    }

    if ((reasonforreturn == 1 || reasonforreturn == 19) && (pick_up_future == 0 || pick_up_future == "")) {
      is_err = 1;
      future_error = "Please select yes/no for pick up in future.";
    } else {
      future_error = "";
    }

    setState({ ...state, error_start_date: error_startdate, error_other_reason: error_other_reason, error_elsewheretext_other_detail: elsewheretext_other_detail, error_paylater_method: paylater_method, error_payment_method: paym_error, error_return_reason: reason_error, error_pick_up_future: future_error });

    if (is_err == 0) {
      setState({ ...state, isModalPopup: true, success_rental: false });
    }
  }
  let platformStyle = Platform.OS === 'ios' ? styles.iosStyle : styles.androidStyle;

  // console.log("return_car_stage>>", return_car_stage);
  const goBacktoDashboard = () => {
    //console.log(props)
    props.navigation.navigate('AccountList');
  }

  const hideReturnDateValidatePopup = () => {
    setState({ ...state, displayDate: 'MM-DD-YYYY', startDate: '', show_date_validation_popup: false, calenderShow: true });
  }

  const selectYesValidatePopup = () => {
    setState({ ...state, error_start_date: "", show_date_validation_popup: false });
  }

  // console.log("is_rental_car==))))))>", is_rental_car);
  return (
    <>
      {isLoading ?
        <View style={[styles.calenderView, { paddingVertical: 200 }]}>
          <View style={global_styles.activityIndicatorView}>
            <ActivityIndicator size="small" style={{ marginTop: 0 }} />
          </View>
        </View> :
        <View>
          {is_rental_car ?
            <>
              <View style={[styles.return_car_head]}>
                <Text style={[styles.return_car_head_text, global_styles.lato_semibold]} >Choose the date to return your car</Text>
              </View>

              <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>REASON FOR RETURN</Text>
              <View style={[global_styles.inputWrap_username, { flexDirection: "column" }]}>
                <RNPickerSelect
                  style={pickerSelectStyles}
                  placeholder={{}}
                  onValueChange={(value, index) => setReasonForReturn(value, index)}
                  mode="dropdown"
                  defaultValue={reasonforreturn}
                  value={reasonforreturn}
                  useNativeAndroidPickerStyle={false}
                  items={reason_for_return_options}
                  Icon={() => {
                    return (
                      <View
                        style={pickerSelectStyles.icon}
                      />
                    );
                  }}
                />
              </View>
              <Text style={[styles.errorText, global_styles.lato_regular]}>{error_return_reason}</Text>

              <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>RETURN DATE</Text>
              <View style={global_styles.inputWrap_username}>
                <View style={{ flexDirection: "row", backgroundColor: "#FFF", borderWidth: 1, borderRadius: 5, borderColor: "#d4d4d4", paddingHorizontal: 8, paddingVertical: 8, paddingBottom: 5, flex: 1, width: "100%", paddingLeft: 10 }}>
                  <TouchableHighlight underlayColor='' onPress={() => openCalender()} style={styles.calenderDate}><Text style={[styles.calenderDateText, { paddingTop: 5 }, global_styles.lato_regular]}>{displayDate}</Text></TouchableHighlight>
                  <TouchableHighlight underlayColor='' onPress={() => openCalender()} style={styles.calenderIcon}><Image width={25} style={styles.cal_image} source={require('../../assets/images/cal-icon.png')} /></TouchableHighlight>
                </View>

              </View>

              <View style={calenderShow ? styles.calenderShow : styles.calenderHide}>
                <CalendarPicker
                  previousTitleStyle={{ marginLeft: 15 }}
                  nextTitleStyle={{ marginRight: 15 }}
                  dayLabelsWrapper={{ paddingLeft: 20 }}
                  minDate={minDate}
                  maxDate={maxDate}
                  disabledDates={(date) => {
                    let type = date.day();
                    return (type == 6 || type == 0) ? date : '';
                  }}
                  selectedStartDate={startDate}
                  onDateChange={(value) => onDateChange(value)}
                />
              </View>
              <Text style={[styles.errorText, global_styles.lato_regular]}>{error_start_date}</Text>

              {reasonforreturn == 2 && <>
                <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>WOULD YOU LIKE TO TELL US WHERE YOU RENTING FROM?</Text>
                <View style={[global_styles.inputWrap_username]}>
                  <TextInput
                    style={[global_styles.textInput, global_styles.lato_regular]}
                    placeholder={""}
                    value={elsewheretext}
                    onChangeText={(text) => updateTextInput(text, 'elsewheretext')}
                  />
                </View>
                <Text style={[styles.errorText, global_styles.lato_regular]}>{error_elsewheretext_other_detail}</Text>
              </>
              }

              {reasonforreturn == 5 && <>
                <Text style={[global_styles.labelStyle, global_styles.lato_medium]}>OTHER REASON</Text>
                <View style={[global_styles.inputWrap_username]}>
                  <TextInput
                    style={[global_styles.textInput, global_styles.lato_regular]}
                    placeholder={""}
                    value={other_reason}
                    onChangeText={(text) => updateTextInput(text, 'other_reason')}
                  />
                </View>
                <Text style={[styles.errorText, global_styles.lato_regular]}>{error_other_reason}</Text>
              </>
              }



              {(reasonforreturn == 1 || reasonforreturn == 19) && <><Text style={[global_styles.labelStyle, global_styles.lato_medium]}>DO YOU WANT TO PICKUP AT A LATER DATE?</Text>
                <View style={[global_styles.inputWrap_username, { flexDirection: "column" }]}>
                  <RNPickerSelect
                    style={pickerSelectStyles}
                    placeholder={{}}
                    onValueChange={(value) => setDriverPickUpInFuture(value)}
                    mode="dropdown"
                    value={pick_up_future}
                    defaultValue={pick_up_future}
                    useNativeAndroidPickerStyle={false}
                    items={pick_up_future_options}
                    Icon={() => {
                      return (
                        <View
                          style={pickerSelectStyles.icon}
                        />
                      );
                    }}
                  />
                </View>
                <Text style={[styles.errorText, global_styles.lato_regular]}>{error_pick_up_future}</Text>
              </>
              }
              {isPaymentMethod && <>
                <Text style={[global_styles.labelStyle, global_styles.lato_regular]}>PICKUP DATE</Text>
                <View style={[global_styles.inputWrap_username]}>
                  <View style={{ flexDirection: "row", backgroundColor: "#FFF", borderWidth: 1, borderRadius: 5, borderColor: "#d4d4d4", paddingHorizontal: 8, paddingVertical: 8, paddingBottom: 5, flex: 1, width: "100%", paddingLeft: 10 }}>
                    <TouchableHighlight underlayColor='' onPress={() => openFutureCalender()} style={styles.calenderDate}><Text style={[styles.calenderDateText, { paddingTop: 5 }, global_styles.lato_regular]}>{displayFutureDate}</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='' onPress={() => openFutureCalender()} style={styles.calenderIcon}><Image width={25} style={styles.cal_image} source={require('../../assets/images/cal-icon.png')} /></TouchableHighlight>
                  </View>
                </View>
                <Text style={[styles.errorText, global_styles.lato_regular]}>{error_payment_method}</Text>

                <View style={futureCalenderShow ? styles.calenderShow : styles.calenderHide}>
                  <CalendarPicker
                    previousTitleStyle={{ marginLeft: 15 }}
                    nextTitleStyle={{ marginRight: 15 }}
                    dayLabelsWrapper={{ marginLeft: 20 }}
                    minDate={minFutureDate}
                    maxDate={maxDate}
                    disabledDates={(date) => {
                      //console.log('disabledDates', date.day())
                      let type = date.day();
                      //hide Saturday
                      return (type == 6 || type == 0) ? date : '';
                    }}
                    selectedStartDate={futureDate}
                    onDateChange={(value) => onFutureDateChange(value)}
                  />
                </View>
              </>}

              <View style={styles.buttonView}>
                <TouchableOpacity style={[global_styles.bottom_style_orange_wrapper]} onPress={() => { openconfirm(); }} >
                  <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>SUBMIT</Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.errorText}>{error_global}</Text>

              <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={(success_rental || isModalPopup)} >
                <View style={modal_style.modal}>

                  {isModalPopup && <>
                    <View style={modal_style.modal_wrapper}>
                      <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hidemakePayment(false)}>
                        <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                      </TouchableOpacity>
                      <Text style={modal_style.modal_heading}>Confirmation</Text>
                      <Text style={[modal_style.modalText, global_styles.lato_regular]}>Would you like to confirm your return?</Text>
                      <View style={modal_style.modal_textbuttonWrap}>
                        <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hidemakePayment(false)}>
                          <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Go Back</Text>
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => checkoutFun()}>
                          <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Accept</Text>
                        </TouchableHighlight>
                      </View>
                    </View>
                  </>}

                  {success_rental && <>
                    <View style={modal_style.modal_wrapper}>
                      <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => closePopup()}>
                        <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                      </TouchableOpacity>
                      <Text style={[modal_style.modalText, global_styles.lato_regular]}>Congratulations, your return schedule details has been sent successfully.</Text>
                      <View style={modal_style.modal_textbuttonWrap_single}>
                        <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => closePopup()}>
                          <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>OK</Text>
                        </TouchableHighlight>
                      </View>
                    </View>
                  </>
                  }
                </View>
              </Modal>



            </> : <>
              {(return_car_stage == "") && <ActivityIndicator size="small" style={{ padding: 60 }} />}

              {(rentalId == null) && <View style={[styles.list_container, { padding: 14 }]}>
                <Text style={[{ paddingHorizontal: 15, fontSize: 14, paddingVertical: 20 }, global_styles.lato_regular]} >There is no car reserved in your account.</Text>
              </View>}

              {(is_rental_car == false && return_car_stage == "A_30") && <View style={[styles.list_container, { padding: 20 }]}>
                <Text style={[styles.banner_billing_pending, global_styles.lato_regular]} >Your return a car is under process.</Text>
                <View style={[styles.sub_container_notice_text, { flex: 1, flexDirection: "row" }]} >
                  <Text style={[styles.banner_billing_pending, global_styles.lato_regular]}>Please confirm return from </Text>
                  <TouchableHighlight onPress={() => goBacktoDashboard()} underlayColor="">
                    <Text style={[global_styles.lato_regular, { color: "orange", textDecorationLine: "underline" }]}>dashboard</Text>
                  </TouchableHighlight>
                  <Text style={global_styles.lato_regular}>.</Text>
                </View>
              </View>}
              {(is_rental_car == false && return_car_stage == "A_35") && <View style={[styles.list_container, { padding: 14 }]}>
                <Text style={[{ paddingHorizontal: 15, fontSize: 14, paddingVertical: 20 }, global_styles.lato_regular]} >Your return a car process for the Current Rental is successfully processed.</Text>
              </View>}
            </>}
        </View>
      }

      <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={(show_date_validation_popup)} >
        <View style={modal_style.modal}>

          <View style={modal_style.modal_wrapper}>
            <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hideReturnDateValidatePopup()}>
              <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
            </TouchableOpacity>
            <Text style={modal_style.modal_heading}>Warning!</Text>
            <Text style={[modal_style.modalText, global_styles.lato_regular]}>
              Buggy has a 'one week notice' return policy. If you return on this date you will be charged an early return fee of ${props.carAmount}. To avoid this fee please go back and choose a later date. To continue with this date and pay the fee click accept.
            </Text>
            <View style={modal_style.modal_textbuttonWrap}>
              <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hideReturnDateValidatePopup()}>
                <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Go Back</Text>
              </TouchableHighlight>
              <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => selectYesValidatePopup()}>
                <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Accept</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  return_car_head_text: {
    fontSize: 16,
    marginBottom: 12
  },
  calenderView: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    paddingBottom: 5,
  },
  calenderText: {
    fontSize: 12,
    color: "#474747",
    lineHeight: 20,
    letterSpacing: 1
  },
  calenderDateView: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 8,
    backgroundColor: "#FFFFFF"
  },
  list_container: {
    marginVertical: 0,
    marginHorizontal: 15,
    paddingBottom: 10,
    marginBottom: 0,
  },
  calenderDate: {
    flex: 1,
  },
  calenderDateText: {
    fontSize: 14,
  },
  calenderIcon: {
    flexDirection: "row-reverse"
  },
  calenderShow: {
    zIndex: 10,
    display: "flex"
  },
  calenderHide: {
    display: "none"
  },
  calenderTimeView: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 8,
  },
  calenderTimeText: {
    fontSize: 16,
  },
  errorText: {
    color: "red",
    fontSize: 14,
    paddingHorizontal: 0
  },
  noTimeText: {
    paddingVertical: 10
  },
  calenderTimeBoxWrap: {
    paddingVertical: 10,
  },
  calenderTimeBoxView: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  calenderTimeBoxText: {
    flex: 1,
    fontSize: 14,
    marginBottom: 4
  },
  calenderTimeBoxWrapRow: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  calenderTimeBoxRow: {
    width: "22%",
    textAlign: "center",
    marginVertical: "1.5%",
    marginHorizontal: "1.5%",
    borderWidth: 1,
    borderColor: "#CCCCCC",
  },
  calenderTimeBoxRowSelected: {
    width: "22%",
    textAlign: "center",
    marginVertical: "1.5%",
    marginHorizontal: "1.5%",
    borderWidth: 1,
    borderColor: Colors.color0,
  },
  calenderTimeBoxRowText: {
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    color: "#575757",
  },
  calenderTimeBoxRowTextSelected: {
    fontSize: 14,
    lineHeight: 26,
    textAlign: "center",
    color: Colors.color0,
  },
  buttonView: {
    flex: 1,
    alignItems: "flex-end",
    marginTop: 20
  },
  checkoutButton: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    color: Colors.color0,
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  payment_method_items: {
    marginVertical: 5,
  },
  iosStyle: {
    flex: 1,
    marginLeft: 0,
    marginTop: 0,
    paddingVertical: 10,
    paddingHorizontal: 10,
    paddingTop: 0,
  },
  androidStyle: {
    flex: 1,
    flexDirection: "row",
    marginLeft: -10,
    marginTop: -5,
    paddingVertical: 10,
    paddingTop: 0,
    paddingHorizontal: 10,
  },
  textInput: {
    marginTop: 0,
    borderWidth: 1,
    borderRadius: 6,
    fontSize: 14,
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderColor: '#d4d4d4',
    color: '#000000',
    opacity: 0.8,
    backgroundColor: '#ffffff'
  },
  save_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    marginStart: 16,
    borderColor: '#db9360'
  },
  cal_image: {
    width: 25,
    resizeMode: "contain",
    marginTop: -3,
    alignItems: "center",
  },
  save_text: {
    color: '#FFFFFF',
  },
})
