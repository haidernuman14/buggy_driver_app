import React, { useState, useEffect } from 'react';
import { View, Text, Keyboard, Platform, ScrollView, ActivityIndicator, TouchableWithoutFeedback, TouchableOpacity, Image, StyleSheet, Linking, TouchableHighlight } from 'react-native';
import { translate } from '../../components/Language';
import ReturnCarScreen from './ReturnCarScreen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RadioForm from 'react-native-simple-radio-button';
import Colors from '../../constants/Colors';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { allCarModels } from '../../graphql/allCarModels';
import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import { cancelReturn } from '../../graphql/cancelReturn';
import { confirmReturn } from '../../graphql/confirmReturn';
import Modal from "react-native-modal";
import { driverBalance } from '../../graphql/driverBalance';
import { driverReturnCarDate } from '../../graphql/driverReturnCarDate';

export default function ReturnSwitchConnectorCarScreen(props) {

  const [state, setState] = useState({
    typeIndex: 0,
    typeValue: 1,
    isLoading: true,
    rentalId: '',
    is_rental_car: false,
    return_carstage: '',
    model_data: {},
    loaded_from_storage: false,
    pickupDate: "",
    return_date: "",
    isModalPopup: false,
    modelName: '',
    success_rental: false,
  });
  const { isLoading, isModalPopup, modelName, success_rental, return_date, pickupDate, loaded_from_storage, rentalId, is_rental_car, return_carstage, model_data, typeValue, typeIndex } = state;

  const [CancelReturn] = useMutation(cancelReturn);
  const [ConfirmReturn] = useMutation(confirmReturn);
  const [driverId, setDriverId] = useState("");
  const [key, setKey] = useState(false);

  const [returncarDriverId, setReturnCarDriverId] = useState("");
  const [balanceDriverId, setBalanceCarDriverId] = useState("");
  const [carAmount, setCarAmount] = useState(0);

  const setuserid = async () => {

    const user = await AsyncStorage.getItem('user');
    if (user) {
      setDriverId(JSON.parse(user).id);
      setReturnCarDriverId(JSON.parse(user).id);
    }

    /*
    const return_date1 = await AsyncStorage.getItem('return_date');
    const rental_car_id = await AsyncStorage.getItem('rental_car_id'); 
    const is_rental_car = await AsyncStorage.getItem('is_rentalnull'); 
    const return_carstage = await AsyncStorage.getItem('return_car_stage'); 
    const pickupDate1 = await AsyncStorage.getItem('pickupDate'); 

    let pickupDate_1 = "";
    if( pickupDate1 ) {
      pickupDate_1 = pickupDate1;
    }

    console.log("is_rental_car---------->>->",is_rental_car);
    if(rental_car_id && rental_car_id != "" &&  return_carstage &&  return_carstage != "") { 
      setState({ ...state, return_date:return_date1, pickupDate: pickupDate_1, loaded_from_storage:true, typeIndex:0, typeValue:1, rentalId: rental_car_id, isLoading:false, is_rental_car:(is_rental_car=="yes")?true:false, return_carstage: return_carstage }); 
      setKey(!key);
      setDriverId(JSON.parse(user).id); 
      setBalanceCarDriverId(JSON.parse(user).id);
    } else {
      setState({ ...state, loaded_from_storage:true });
    }
    */

  }


  ///////// Get Return Car ///////////
  const { data: data_returncar, loading: drivar_returncar_loading, error: drivar_returncar_error } = useQuery(driverReturnCarDate, {
    variables: { id: returncarDriverId },
    fetchPolicy: "network-only",
    skip: returncarDriverId == ''
  });
  


  useEffect(() => {
    if (data_returncar && data_returncar.hasOwnProperty('driver')) {
      if (data_returncar && data_returncar.hasOwnProperty('driver') && data_returncar.driver.currentAgreement != null) {

        let return_date = "";
        setCarAmount(data_returncar.driver.currentAgreement.car.weeklyCharge);
        if (data_returncar.driver.currentAgreement.hasOwnProperty('carreturn') && data_returncar.driver.currentAgreement.carreturn != null) {
          return_date = data_returncar.driver.currentAgreement.carreturn.scheduledDate;
          AsyncStorage.setItem('return_date', return_date);
          setReturnCarDriverId("");
          setDriverId("");
          setKey(!key);
        } else {
          setReturnCarDriverId("");
          setDriverId("");
          setKey(!key);
        }
        //setState({ ...state, return_date:return_date,loaded_from_storage:true});  

        // Direct data store in state========================================
        let rental_id_agreement = "";
        let pickupDate = "";
        let drivar_data = data_returncar;
        AsyncStorage.setItem('rental_car_id', "");
        if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null) {
          rental_id_agreement = drivar_data.driver.currentAgreement.id;
          AsyncStorage.setItem('rental_car_id', drivar_data.driver.currentAgreement.id);
        }

        let is_rentalnull = "no";
        let return_car_stage = "";
        AsyncStorage.setItem('return_car_stage', "");
        AsyncStorage.setItem('pickupDate', "");
        if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null) {
          AsyncStorage.setItem('return_car_stage', drivar_data.driver.currentAgreement.stage);

          if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null && drivar_data.driver.currentAgreement.hasOwnProperty('startDate') && drivar_data.driver.currentAgreement.startDate != "") {
            pickupDate = drivar_data.driver.currentAgreement.startDate;
          }

        }
        if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null) {
          if (drivar_data.driver.currentAgreement.stage != "A_30" && drivar_data.driver.currentAgreement.stage != "A_35") {
            is_rentalnull = "yes";
          }
          return_car_stage = drivar_data.driver.currentAgreement.stage;
          AsyncStorage.setItem('return_car_stage', return_car_stage);
        } else {
          is_rentalnull = "no";
        }
        AsyncStorage.setItem('is_rentalnull', is_rentalnull);

        setState({ ...state, return_date: return_date, loaded_from_storage: true, pickupDate: pickupDate, typeIndex: 0, typeValue: 1, rentalId: rental_id_agreement, isLoading: false, is_rental_car: (is_rentalnull == "yes") ? true : false, return_carstage: return_car_stage });
        //===================================//===================================

      } else {
        setReturnCarDriverId("");
        setDriverId("");
        setState({ ...state, isLoading: false, return_date: "", loaded_from_storage: true });
        setKey(!key);
      }
    }
  }, [data_returncar]);
  //////////// End return car /////////////////


  ///////////// get driver content ///////////////
  /*   const { data: drivar_data, loading: drivar_data_loading, error: drivar_data_error } = useQuery(driverBalance, {
       variables: { id: balanceDriverId },
       fetchPolicy: "network-only",
       skip: balanceDriverId == ''
     });
     console.log("driver_node_id>>", balanceDriverId);
     useEffect(() => {
       if (drivar_data) {
   
         if (drivar_data && drivar_data.hasOwnProperty('driver')) {  
   
           // set rental id
           let rental_id_agreement = "";
           let pickupDate = "";
           AsyncStorage.setItem('rental_car_id', "");
           if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null) {
             rental_id_agreement = drivar_data.driver.currentAgreement.id;
             AsyncStorage.setItem('rental_car_id', drivar_data.driver.currentAgreement.id); 
           }  
   
           let is_rentalnull = "no";
           let return_date = "";
           let return_car_stage = "";
           AsyncStorage.setItem('return_car_stage', "");
           AsyncStorage.setItem('pickupDate', "");
           if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null) {
             AsyncStorage.setItem('return_car_stage', drivar_data.driver.currentAgreement.stage);
   
             if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null && drivar_data.driver.currentAgreement.hasOwnProperty('startDate') && drivar_data.driver.currentAgreement.startDate != "") {
               pickupDate = drivar_data.driver.currentAgreement.startDate;
             }
   
           }
           if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null) {
             if (drivar_data.driver.currentAgreement.stage != "A_30" && drivar_data.driver.currentAgreement.stage != "A_35") {
               is_rentalnull = "yes";
             }
             return_car_stage = drivar_data.driver.currentAgreement.stage;
             AsyncStorage.setItem('return_car_stage', return_car_stage);
           } else {
             is_rentalnull = "no";
           }
           AsyncStorage.setItem('is_rentalnull', is_rentalnull);
 
           setReturnCarDriverId(driverId);
           setBalanceCarDriverId("");
           setState({ ...state, pickupDate: pickupDate, typeIndex:0, typeValue:1, rentalId: rental_id_agreement, isLoading:false, is_rental_car:(is_rentalnull=="yes")?true:false, return_carstage: return_car_stage }); 
      
 
         }
       }
     }, [drivar_data]); */
  ///////////// End custom content //////////////

  const refreshPage = async (tindex, tvalue) => {

    const user = await AsyncStorage.getItem('user');
    const rental_car_id = await AsyncStorage.getItem('rental_car_id');
    const is_rental_car = await AsyncStorage.getItem('is_rentalnull');
    const return_carstage = await AsyncStorage.getItem('return_car_stage');
    const return_date1 = await AsyncStorage.getItem('return_date');
    if (rental_car_id && rental_car_id != "" && return_carstage && return_carstage != "") {
      setState({ ...state, return_date: return_date1, loaded_from_storage: true, typeIndex: tindex, typeValue: tvalue, rentalId: rental_car_id, isLoading: false, is_rental_car: (is_rental_car == "yes") ? true : false, return_carstage: return_carstage });
      setKey(!key);
      setReturnCarDriverId(JSON.parse(user).id);
    } else {
      setState({ ...state, loaded_from_storage: true });
      setKey(!key);
    }

  }

  useEffect(() => {
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();
    }
    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();
    });
    return () => {
      navFocusListener.remove();
    };
  }, []);


  const { data, loading, error } = useQuery(allCarModels, {
    variables: {
      isAvailable: true,
    },
    skip: driverId == ''
  });

  useEffect(() => {
    if (data && !loading) {
      if (data && data.allCarModels) {
        getData(data);
      }
    }
  }, [data]);


  const getData = async (data) => {
    const model_data = data.allCarModels.edges;
    let items = [];
    for (let i = 0; i < model_data.length; i++) {
      items.push({ label: model_data[i].node.name, value: model_data[i].node.name });
    }
    items.sort(function (a, b) {
      if (a.label < b.label) { return -1; }
      if (a.label > b.label) { return 1; }
      return 0;
    })
    setState({ ...state, isLoading: false, model_data: items });
    setKey(!key);
    //setDriverId("");
  }


  if (isLoading || drivar_returncar_loading) {
    return <View style={global_styles.activityIndicatorView}><ActivityIndicator size="small" style={{ padding: 60 }} /></View>
  }



  const setCancelReturn = () => {

    if (rentalId != "") {
      setState({ ...state, modelName: "Loading", isLoading: true });
      let input = {
        "rentalId": rentalId,
      }
      CancelReturn({
        variables: { input }
      }).then((response) => {
        if (response.data.cancelReturn.ok) {
          AsyncStorage.setItem('return_car_stage', "A_10");
          AsyncStorage.setItem('is_rentalnull', "yes");
          setState({ ...state, modelName: "SuccessCancelReturn", is_rental_car: true, return_carstage: "A_10", isLoading: false });
          setKey(!key);
        }
      });

    }
  }
  const setConfirmCancelReturn = () => {
    setState({ ...state, modelName: "CancelConfirmReturn" });
  }

  const setReschedReturn = async () => {
    await AsyncStorage.setItem('carAmount', carAmount + "");
    props.navigation.navigate('ReschedReturnCar', { fromrequest: "ReturnConnect" });
  }

  const showReturnCarPopup = () => {
    setState({ ...state, isModalPopup: true, success_rental: false });
  }


  const checkoutFun = async () => {

    setState({ ...state, isLoading: true });
    if (rentalId != "") {
      let input = {
        "rentalId": rentalId,
        "confirmReturn": true
      }
      //console.log("ConfirmReturn input---", input);
      ConfirmReturn({
        variables: { input }
      }).then((response) => {
        if (response.data.confirmReturn.ok) {
          AsyncStorage.setItem('return_car_stage', "A_35");
          //AsyncStorage.setItem('rental_car_id', "");
          setState({ ...state, return_carstage: "A_35", isModalPopup: false, success_rental: true, isLoading: false });
        }
      });
    }
  }

  const hideReturnCarPopup = () => {
    setState({ ...state, isModalPopup: false, success_rental: false });
  }

  const closePopup = () => {
    setState({ ...state, isModalPopup: false, success_rental: false });
  }

  return (
    <>
      {isLoading ?
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="small" style={{ padding: 60 }} />
        </View> :
        <View style={styles.main_container}>
          <KeyboardAwareScrollView>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
              <ScrollView keyboardShouldPersistTaps="always">
                <>
                  <View style={styles.main_header_content}></View>

                  <View style={[styles.container, styles.container_header]}>

                    {(pickupDate != "" && pickupDate != null && moment().diff(pickupDate, 'days') < 0) ?
                      <>
                        <View style={[styles.list_container, { paddingHorizontal: 10, borderBottomWidth: 0 }]}>
                          <Text style={[{ paddingHorizontal: 15, fontSize: 16, paddingVertical: 40 }, global_styles.lato_semibold]} >
                            You have upcoming pickup. Return a car process is allowed, once the pickup process is completed.
                            </Text>
                        </View>
                      </> : <>
                        {((return_carstage == "A_20") && loaded_from_storage == true) ?
                          <View style={[styles.list_container, { paddingHorizontal: 10, borderBottomWidth: 0 }]}>
                            <Text style={[{ paddingHorizontal: 0, flex: 1, flexDirection: "row", fontSize: 16, paddingVertical: 40 }, global_styles.lato_semibold]} >
                              {'You have already submitted a request to exchange vehicles. If you would still like to return the car, please contact us at \n'}
                              <Text onPress={() => Linking.openURL("tel:+13473346313")} style={[global_styles.lato_regular, { color: "#2DAFD3" }]}>347 334 6313</Text>
                            </Text>
                          </View> : <>
                            {((return_carstage == "A_30" || return_carstage == "A_35") && loaded_from_storage == true) ?

                              <>
                                {return_carstage == "A_30" &&
                                  <View style={[styles.list_container, { paddingHorizontal: 10, borderBottomWidth: 0 }]}>
                                    {/* 
                                        <Text style={{paddingHorizontal:15, fontSize:14, paddingVertical:40}} >Your return a car process for the Current Rental is successfully processed.</Text>
                                    */}

                                    <View style={styles.sub_container_notice} >
                                      <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", width: "100%", flex: 1 }, global_styles.lato_semibold]}>
                                        You are scheduled to return your car on {moment.utc(return_date.split("T")[0]).format("dddd, MMMM DD, yyyy")} at {moment.utc(return_date).format("HH:mm")}
                                      </Text>
                                      <View style={styles.content_link_container}>
                                        <TouchableHighlight onPress={() => showReturnCarPopup()} underlayColor="" >
                                          <Text style={[styles.banner_billing_pending, { color: "#2dafd3" }, global_styles.lato_semibold]}>Click here</Text>
                                        </TouchableHighlight>
                                        <Text style={[styles.banner_billing_pending, global_styles.lato_semibold]}> to confirm your return.</Text>
                                      </View>
                                      <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", width: "100%", flex: 1 }, global_styles.lato_semibold]}>
                                        To reschedule or cancel your return, use the buttons below.
                                      </Text>
                                      <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", width: "100%", flex: 1 }, global_styles.lato_semibold]}>
                                        {"\n"}Where do I return the car?
                                      </Text>
                                      <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", fontSize: 14, width: "100%", }, global_styles.lato_regular]}>
                                        {/* Please make sure the inside and outside of the vehicle is completely clean, and has a full tank of gas, to avoid additional fees.{"\n"}{"\n"} */}
                                        Please bring your vehicle to 389 Empire Blv, Brookly NY,11225,and wait for Buggy representative to assist your further.
                                        {"\n"}
                                      </Text>
                                       <View style={{justifyContent:"space-between",flexDirection:"row"}}>
                                         <View />
                                        <Image source={require('../../assets/images/2.jpeg')} style={{height:200,width:"100%",marginBottom:10,}} />
                                        </View>
                                      <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", width: "100%", flex: 1 }, global_styles.lato_semibold]}>
                                        {"\n"}How to avoid fees?
                                      </Text>
                                      <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", fontSize: 14, width: "100%", flex: 1 }, global_styles.lato_regular]}>
                                        Please make sure the inside and outside of the vehicle is completely clean, and has a full tank of gas, to avoid additional fees.{"\n"}
                                        {/* Then please drop the car off in the parking lot of the Associated Supermarket at 975 Nostrand Ave, Brooklyn 11225.  Drive your car all the way to the end of the lot where our Representatives will take the keys from you.
                                        {"\n"} */}
                                      </Text>
                                     
                                      <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", width: "100%", flex: 1 }, global_styles.lato_semibold]}>How do I close my account?</Text>
                                      <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", fontSize: 14, width: "100%", flex: 1 }, global_styles.lato_regular]}>After parking the car, please stop by the office at 445 Empire Blvd on the second floor to have a representative close your account.</Text>

                                      <View style={styles.sub_container_notice_text} >

                                        <TouchableHighlight onPress={() => setConfirmCancelReturn()} underlayColor="" style={[global_styles.bottom_style_gray_wrapper, { paddingHorizontal: 12, paddingVertical: 9 }]}>
                                          <Text style={[global_styles.bottom_style_gray_text, global_styles.lato_regular]}>
                                            CANCEL RETURN
                                            </Text>
                                        </TouchableHighlight>

                                        <TouchableHighlight onPress={() => setReschedReturn()} underlayColor="" style={[global_styles.bottom_style_orange_wrapper, { paddingHorizontal: 12, paddingVertical: 9, marginLeft: 15 }]}>
                                          <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_regular]}>
                                            RESCHEDULE
                                            </Text>
                                        </TouchableHighlight>
                                      </View>
                                    </View>

                                  </View>
                                }
                                {return_carstage == "A_35" &&
                                  <View style={styles.sub_container_notice} >
                                    <Text style={[styles.banner_billing_pending, global_styles.lato_semibold]}>
                                      Your scheduled return is confirmed.
                                      </Text>
                                    <Text style={[styles.banner_billing_pending, { marginTop: 15 }, global_styles.lato_semibold]}>
                                      You are scheduled to return your car on {moment.utc(return_date.split("T")[0]).format("dddd, MMMM DD, yyyy")} at {moment.utc(return_date).format("HH:mm")}
                                    </Text>

                                    <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", width: "100%", flex: 1, marginTop: 16 }, global_styles.lato_semibold]}>
                                      To reschedule or cancel your return, use the buttons below.
                                      </Text>

                                    <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", width: "100%", flex: 1 }, global_styles.lato_semibold]}>
                                      {"\n"}Where do I return the car?
                                      </Text>
                                    <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", fontSize: 14, width: "100%", flex: 1 }, global_styles.lato_regular]}>
                                      Please make sure the inside and outside of the vehicle is completely clean, and has a full tank of gas, to avoid additional fees.{"\n"}{"\n"}
                                        Then please drop the car off in the parking lot of the Associated Supermarket at 975 Nostrand Ave, Brooklyn 11225.  Drive your car all the way to the end of the lot where our Representatives will take the keys from you.
                                        {"\n"}
                                    </Text>
                                    <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", width: "100%", flex: 1 }, global_styles.lato_semibold]}>How do I close my account?</Text>
                                    <Text style={[styles.banner_billing_pending, { flexWrap: "wrap", fontSize: 14, width: "100%", flex: 1 }, global_styles.lato_regular]}>After parking the car, please stop by the office at 445 Empire Blvd on the second floor to have a representative close your account.</Text>

                                    <View style={styles.sub_container_notice_text} >

                                      <TouchableHighlight onPress={() => setConfirmCancelReturn()} underlayColor="" style={[global_styles.bottom_style_gray_wrapper, { paddingHorizontal: 12, paddingVertical: 9 }]}>
                                        <Text style={[global_styles.bottom_style_gray_text, global_styles.lato_regular]}>
                                          CANCEL RETURN
                                            </Text>
                                      </TouchableHighlight>

                                      <TouchableHighlight onPress={() => setReschedReturn()} underlayColor="" style={[global_styles.bottom_style_orange_wrapper, { paddingHorizontal: 12, paddingVertical: 9, marginLeft: 15 }]}>
                                        <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_regular]}>
                                          RESCHEDULE
                                            </Text>
                                      </TouchableHighlight>
                                    </View>
                                  </View>
                                }
                              </>

                              :
                              <>
                                {(rentalId != "" && loaded_from_storage == true) ?
                                  <View key={key} style={styles.container} >
                                    <ReturnCarScreen refreshPage={refreshPage} carAmount={carAmount} rentalId={rentalId} is_rental_car={is_rental_car} return_carstage={return_carstage} {...props} />
                                  </View> :
                                  <>
                                    {(loaded_from_storage == true) &&
                                      <View style={[styles.list_container, { paddingHorizontal: 10, borderBottomWidth: 0 }]}>
                                        <Text style={{ paddingHorizontal: 15, fontSize: 14, paddingVertical: 40 }} >There is no car reserved in your account.</Text>
                                      </View>
                                    }
                                  </>
                                }
                              </>
                            }
                          </>
                        }
                      </>
                    }

                  </View>


                  <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={(modelName)} >
                    <View style={modal_style.modal}>
                      {modelName == "CancelConfirmReturn" && <>
                        <View style={modal_style.modal_wrapper}>
                          <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => setState({ ...state, modelName: "" })}>
                            <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                          </TouchableOpacity>
                          <Text style={modal_style.modal_heading}>Confirmation</Text>
                          <Text style={modal_style.modalText}>Are you sure you want to cancel this return?</Text>
                          <View style={modal_style.modal_textbuttonWrap}>
                            <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => setState({ ...state, modelName: "" })}>
                              <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => setCancelReturn()}>
                              <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                            </TouchableHighlight>
                          </View>
                        </View>
                      </>}
                      {modelName == "SuccessCancelReturn" && <>
                        <View style={modal_style.modal_wrapper}>
                          <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => setState({ ...state, modelName: "" })}>
                            <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                          </TouchableOpacity>
                          <Text style={modal_style.modal_heading}>Success</Text>
                          <Text style={modal_style.modalText}>Return a car request has been cancelled successfully.</Text>
                          <View style={modal_style.modal_textbuttonWrap_center}>
                            <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => setState({ ...state, modelName: "" })}>
                              <Text style={modal_style.upload_document_text_modal}>OK</Text>
                            </TouchableHighlight>
                          </View>
                        </View>
                      </>}

                      {modelName == "Loading" && <>
                        <View>
                          <View style={modal_style.modal_textbuttonWrap}>
                            <ActivityIndicator size="small" style={{ padding: 60 }} />
                          </View>
                        </View>
                      </>
                      }

                    </View>
                  </Modal>

                  <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={(success_rental || isModalPopup)} >
                    <View style={modal_style.modal}>
                      {isModalPopup && <>
                        <View style={modal_style.modal_wrapper}>
                          <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hideReturnCarPopup(false)}>
                            <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                          </TouchableOpacity>
                          <Text style={modal_style.modal_heading}>Confirmation</Text>
                          <Text style={modal_style.modalText}>Would you like to confirm your car return?</Text>
                          <View style={modal_style.modal_textbuttonWrap}>
                            <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hideReturnCarPopup(false)}>
                              <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => checkoutFun()}>
                              <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                            </TouchableHighlight>
                          </View>
                        </View>
                      </>}

                      {success_rental && <>
                        <View style={modal_style.modal_wrapper}>
                          <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => closePopup()}>
                            <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                          </TouchableOpacity>
                          <Text style={modal_style.modal_heading}>Success</Text>
                          <Text style={modal_style.modalText}>Congratulations, your return schedule details has been sent successfully.</Text>
                          <View style={modal_style.modal_textbuttonWrap_center}>
                            <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => closePopup()}>
                              <Text style={modal_style.upload_document_text_modal}>OK</Text>
                            </TouchableHighlight>
                          </View>
                        </View>
                      </>
                      }
                    </View>
                  </Modal>

                </>
              </ScrollView>
            </TouchableWithoutFeedback>
          </KeyboardAwareScrollView>
        </View>
      }
    </>
  );
}


const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 25,
    paddingTop: 22,
    paddingBottom: 12,
  },
  content_link_container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginVertical: 20,
    alignItems: 'flex-start',
  },
  container: {
    flex: 1,
  },
  container_header: {
  },
  sub_container_notice: {
    paddingVertical: 10,
    zIndex: 10,
    marginBottom: 10,
  },
  sub_container_notice_text: {
    flex: 1,
    paddingRight: 15,
    flexDirection: 'row',
    marginTop: 35,
    justifyContent: 'flex-end'
  },
  banner_billing_pending: {
    color: '#000',
    fontSize: 16,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 14,
    justifyContent: "center"
  },
  textbuttonSubmit: {
    backgroundColor: '#db9360',
    padding: 8,
    borderRadius: 6,
    alignItems: "center",
  },
});