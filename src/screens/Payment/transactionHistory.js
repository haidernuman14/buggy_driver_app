import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  Platform,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';
import moment from "moment";
import { global_styles } from '../../constants/Style';
import { useQuery } from '@apollo/react-hooks';
import { allTransactions } from '../../graphql/allTransactions';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Octicons';
import { FlatList } from 'react-native-gesture-handler';


const win = Dimensions.get('window');

export default function TransactionHistoryScreen(props) {

  //console.log('props.driverId', props.driverId)

  const [state, setState] = useState({
    isLoading: true,
    driverId: props.driverId,
    fresh_flag: true,
    data_transaction_list: [],
    data_transaction_pageinfo: {},
    amount_type: props.amount_type,
    start_date: props.start_date,
    end_date: props.end_date,
    cat_name: props.cat_name
  });

  const { amount_type, start_date, end_date, cat_name, isLoading, driverId, fresh_flag, data_transaction_list, data_transaction_pageinfo } = state;

  const [isNextPageLoading, setNextPageLoading] = useState(false);
  //console.log("props.cat_name>>>>>>>",props.cat_name,cat_name);
  ///////// Get Transaction ///////////
  let input_data = {};
  if (props.amount_type == "credit") {
    if (props.start_date != "" && props.end_date != "" && props.start_date != null && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = {
          amount_Gte: 0,
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = {
          amount_Gte: 0,
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 7
        };
      }
    } else if (props.start_date != "" && (props.end_date == "" || props.end_date == null) && props.start_date != null ) {
      if (props.cat_name != "") {
        input_data = {
          amount_Gte: 0,
          dueDate_Gte: props.start_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = {
          amount_Gte: 0,
          dueDate_Gte: props.start_date,
          driverId: driverId,
          first: 7
        };
      }
    } else if ((props.start_date == "" || props.start_date==null) && props.end_date != "" && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = {
          amount_Gte: 0,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = {
          amount_Gte: 0,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 7
        };
      }
    } else {
      if (props.cat_name != "") {
        input_data = {
          amount_Gte: 0,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = {
          amount_Gte: 0,
          driverId: driverId,
          first: 7
        };
      }
    }
  } else if (props.amount_type == "debit") {
    if (props.start_date != "" && props.end_date != "" && props.start_date != null && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = {
          amount_Lte: 0,
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = {
          amount_Lte: 0,
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 7
        };
      }
    } else if (props.start_date != "" && (props.end_date == "" || props.end_date == null) && props.start_date != null ) {
      if (props.cat_name != "") {
        input_data = {
          amount_Lte: 0,
          dueDate_Gte: props.start_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = {
          amount_Lte: 0,
          dueDate_Gte: props.start_date,
          driverId: driverId,
          first: 7
        };
      }
    } else if ((props.start_date == "" || props.start_date==null) && props.end_date != "" && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = {
          amount_Lte: 0,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = {
          amount_Lte: 0,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 7
        };
      }
    } else {
      if (props.cat_name != "") {
        input_data = {
          amount_Lte: 0,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = {
          amount_Lte: 0,
          driverId: driverId,
          first: 7
        };
      }
    }
  }else  {
    if (props.start_date != "" && props.end_date != "" && props.start_date != null && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = { 
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = { 
          dueDate_Gte: props.start_date,
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 7
        };
      }
    } else if (props.start_date != "" && (props.end_date == "" || props.end_date == null) && props.start_date != null ) {
      if (props.cat_name != "") {
        input_data = { 
          dueDate_Gte: props.start_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = { 
          dueDate_Gte: props.start_date,
          driverId: driverId,
          first: 7
        };
      }
    } else if ((props.start_date == "" || props.start_date==null) && props.end_date != "" && props.end_date != null) {
      if (props.cat_name != "") {
        input_data = { 
          dueDate_Lte: props.end_date,
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = { 
          dueDate_Lte: props.end_date,
          driverId: driverId,
          first: 7
        };
      }
    } else {
      if (props.cat_name != "") {
        input_data = { 
          driverId: driverId,
          chargeTypeIntIn: JSON.parse(props.cat_name),
          first: 7
        };
      } else {
        input_data = { 
          driverId: driverId,
          first: 7
        };
      }
    }
  }


  // console.log("input_data>>>>{}>", input_data);
  const { data: data_transaction, loading: loding_transaction, error: error_transaction, fetchMore } = useQuery(allTransactions, {
    variables: input_data,
    fetchPolicy: "network-only",
  });

  useEffect(() => {
    if (loding_transaction) {
      // console.log('loding_transaction', loding_transaction)
    }
    if (data_transaction && data_transaction.allTransactions.edges.length > 0) {
    //  console.log('ddddddddd', driverId)
   //   console.log('data_transaction_page', data_transaction.allTransactions.pageInfo)
      if (fresh_flag) {
        setState({ ...state, isLoading: false, data_transaction_list: [...data_transaction.allTransactions.edges], data_transaction_pageinfo: data_transaction.allTransactions.pageInfo })
      } else {
        setState({ ...state, isLoading: false, data_transaction_list: [...data_transaction_list, ...data_transaction.allTransactions.edges], data_transaction_pageinfo: data_transaction.allTransactions.pageInfo })
      }
      //setState({ ...state, isLoading:false, data_transaction_list: [], data_transaction_pageinfo: {} })
    } else if (data_transaction && data_transaction.allTransactions.edges.length == 0) {
      setState({ ...state, isLoading: false })
    }
  }, [data_transaction])

  const handelMoreTransaction = (id) => {
    setState({ ...state, fresh_flag: false })
    fetchMore({
      variables: {
        driverId: driverId,
        first: 7,
        after: id
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        setNextPageLoading(false)
        return fetchMoreResult;
      }
    });
  }

  const loadMoreData = () => {
    if (data_transaction_pageinfo.hasNextPage != true || isNextPageLoading) {
    //  console.log('Load more already running...')
      return
    }
   // console.log('Load more called...')
    setNextPageLoading(true)
    handelMoreTransaction(data_transaction_pageinfo.endCursor)
  }

  // console.log(data_transaction_list)
 // console.log("data_transaction_pageinfo.hasNextPage---->", data_transaction_pageinfo.hasNextPage);
  const renderItem = ({ item }) => {
    return (
      <View>
        <View style={styles.list_item_container}>
          <Text style={[item.node.amount < 0 ? styles.amount_text : styles.amount_text_orange, global_styles.lato_regular]}>{item.node.amount}</Text>
          <View style={styles.list_item}>
            <Text style={[styles.title_text, global_styles.lato_medium]}>{item.node.chargeTypeDisplay}</Text>
            <Text style={[styles.date_text, global_styles.lato_regular]}>{moment(item.node.dueDate).format("MM-DD-YYYY")}</Text>
          </View>
        </View>
        <View style={styles.saperator} />
      </View>
    );
  };

  return (
    <><ScrollView><>
      <View style={styles.main_container}>
        <View style={styles.container}>
          <View style={styles.top_container}>
            <View style={[styles.loading_filter, { backgroundColor: "#f9f9f4" }]}>
              <View style={styles.filter_outer_container}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TransactionFilter')}>
                  <View style={styles.filter_container}>
                    <Image style={styles.filter_image} source={require('../../assets/images/Filter.png')} />
                    <Text style={styles.filter}>FILTER</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            {isLoading ?
              <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
                <ActivityIndicator size="large" style={{ padding: 60 }} />
              </View> : <>

                {data_transaction_list && data_transaction_list.length > 0 ?

                  <>

                    {data_transaction_list.map((item) => <View style={styles.list_item_container}>
                      <Text style={[item.node.amount < 0 ? styles.amount_text : styles.amount_text_orange, global_styles.lato_regular]}>${item.node.amount.toFixed(2)}</Text>
                      <View style={styles.list_item}>
                        <Text style={[styles.title_text, global_styles.lato_medium]}>{item.node.chargeTypeDisplay}</Text>
                        <Text style={[styles.date_text, global_styles.lato_regular]}>{moment(item.node.dueDate).format("MM/DD/YYYY")}</Text>
                      </View>
                    </View>)}
                    <View style={styles.loading_container}>
                      {isNextPageLoading && <ActivityIndicator size="small" />}
                    </View>
                    {data_transaction_pageinfo.hasNextPage == true ?
                      <View style={[global_styles.button_container, { justifyContent: "center", marginBottom: 20 }]}>
                        <TouchableOpacity onPress={loadMoreData}>
                          <View style={[global_styles.button_style_white_wrapper, { borderColor: '#ccc' }]}>
                            <Text style={[global_styles.button_style_white_text, { color: '#393e5c' }]}>SHOW MORE</Text>
                          </View>
                        </TouchableOpacity>
                      </View> : <></>}

                  </>

                  : <View style={[styles.no_container, styles.padding_hr, global_styles.lato_regular]}><Text style={global_styles.lato_regular}>No transaction history exists.</Text></View>
                }
              </>}
          </View>
        </View>
      </View>
    </>
    </ScrollView></>
  );
}

const styles = StyleSheet.create({
  no_container: {
    flex: 1,
    alignItems: "center",
    marginTop:10,
    justifyContent: "center"
  },
  main_container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'column-reverse',
  },
  top_container: {
    flex: 1,
  },
  bottom_container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  show_more_container: {
    paddingVertical: 8,
    paddingHorizontal: 32,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    margin: 16
  },
  loading_filter: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  filter_outer_container: {
    flex: 1,
  },
  filter_container: {
    width: 120,
    backgroundColor: '#fff',
    margin: 16,
    paddingHorizontal: 16,
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    justifyContent: 'center',
  },
  vector_icon: {
    fontSize: 18,
  },
  filter_image: {
    width: 24,
    height: 24,
  },
  filter: {
    marginStart: 8,
    fontSize: 16,
    color: '#393e5c',
  },
  list_item_container: {
    flexDirection: 'row-reverse',
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderBottomColor: "#f5f5f5",
    borderBottomWidth: 1
  },
  list_item: {
    flex: 1,
  },
  saperator: {
    backgroundColor: '#ccc',
    height: 0.5,
    alignSelf: 'stretch'
  },
  title_text: {
    fontSize: 16,
    color: '#000',
    opacity: 0.6,
  },
  date_text: {
    fontSize: 14,
    marginTop: 4,
    color: '#000',
    opacity: 0.6,
  },
  amount_text: {
    fontSize: 16,
    color: '#393e5c',
  },
  amount_text_orange: {
    fontSize: 16,
    color: '#db9360'
  },
  loading_container: {
    flex: 1,
    justifyContent: 'center',
  },
  loading_indicator: {
    alignSelf: 'flex-start',
    marginBottom: 8,
  },
});
