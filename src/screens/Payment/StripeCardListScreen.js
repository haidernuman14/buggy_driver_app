import React, { useState, useEffect } from 'react';
import {
  ActivityIndicator, Text, KeyboardAvoidingView, View, Dimensions, Image, FlatList, SafeAreaView, StyleSheet, TouchableHighlight,
  Platform, ScrollView, TouchableOpacity
} from 'react-native';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { translate } from '../../components/Language';
import { useMutation } from "@apollo/react-hooks";
import { global_styles, modal_style } from '../../constants/Style';
import { GetStripeCards } from '../../graphql/driverAllCards';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Icon2 from 'react-native-vector-icons/EvilIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { CheckConnectivity } from '../Helper/NetInfo';
import Colors from '../../constants/Colors';
import { driverDeleteCard } from '../../graphql/driverDeleteCard';
import Modal from "react-native-modal";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { changeDefaultCard } from '../../graphql/changeDefaultCard';

/*
StripeCardListScreen lists the Credit cards of logged in user using "getStripeCards" API.
 */

export default function StripeCardListScreen(props) {

  const [state, setState] = useState({
    isModal: false,
    deleteCardItem: {},
    card_index:0,
    card_list: {},
    isModalPopup:false,
    ConfirmDefaultCard:false,
    SuccessConfirmDefaultCard:false
  });
  const [driverId, setDriverId] = useState("");
  const [activeId, setActiveId] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const {SuccessConfirmDefaultCard, card_index, ConfirmDefaultCard, isModal, card_list, deleteCardItem, isModalPopup } = state;
  const [DriverDeleteCard] = useMutation(driverDeleteCard);
  const [ChangeDefaultCard] = useMutation(changeDefaultCard);

  // Set up user id
  const setuserid = async () => {
    const user = await AsyncStorage.getItem('user');
    setDriverId(JSON.parse(user).id);
  }

  // Check internet connectivity
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  const { data, loading, error: error_cards } = useQuery(GetStripeCards, {
    variables: { driverId: driverId },
    fetchPolicy: "network-only",
    skip: (driverId == '')
  });

  useEffect(() => {
    checkNet();
    setuserid();

    if (data) {
      setIsLoading(false);
      if (data && data.hasOwnProperty("getStripeCards") && data.getStripeCards && data.getStripeCards.hasOwnProperty("cards") && data.getStripeCards.cards.length > 0) {
        let card_list1 = [];
        let ik = 0;
        let active_index = 0;
        let first_card = "";
        data.getStripeCards.cards.map((carditem) => { 
          if(carditem.default) {
            setActiveId(ik);
            active_index = ik;  
            first_card = {   brand:carditem.brand, card: data.getStripeCards.cards[active_index].id, number: "**** **** **** " + data.getStripeCards.cards[active_index].last4 };
          }
          card_list1.push({ brand:carditem.brand, label: (carditem.brand == "MasterCard" ? "Master" : carditem.brand) + " Card **** " + carditem.last4, value: carditem.id });
          ik = ik + 1;
        });

        if(first_card==""){
          first_card = {  brand:carditem.brand, card: data.getStripeCards.cards[active_index].id, number: "**** **** **** " + data.getStripeCards.cards[active_index].last4 };
        } 
        // console.log("card_list1>>>>>>>>>>",card_list1);
        props.setCardList(card_list1);
        setState({ ...state, deleteCardItem: first_card, card_list: card_list1 });
        
        props.radioCardChange(first_card.card);
      }
    }
  }, [data]);

  if (loading) {
    return <View><ActivityIndicator size="small" style={{ padding: 60 }} /></View>
  }

  const setListCards = (cards, removeditem) => {
    let card_list1 = [];
    cards.getStripeCards.cards.map((carditem) => {
      if (carditem.value != removeditem)
        card_list1.push({ label: "**** **** **** " + carditem.last4, brand:carditem.brand, value: carditem.id });
    });
    setState({ ...state, card_list: card_list1 });
  }

  const hideModal = (modalstatus) => {
    if (modalstatus) {
      checkNet();
      setIsLoading(true);
      let input = {
        driver: driverId,
        sourceId: deleteCardItem.card,
      }
      props.setloading(true);
      DriverDeleteCard({
        variables: { input }
      }).then((response) => {
        props.setloading(false);
        setIsLoading(false);
        if (response.data) {
          // console.log(response.data.removeCard.errors);
          //setListCards(card_list,deleteCardItem.card);
          if (response.data.removeCard.ok) {
            props.setReloadCards();
          }
          props.setReloadCards();
        }
      });
    }

    setState({ ...state, isModal: false });
  }

  const radioChangeCard = (value, index) => {
    
    let card_number = "";
    if (card_list.length > 0) {
      card_list.map((carditem) => {
        if (carditem.value == value) {
          card_number = carditem.label
        }
      });
    }
    // console.log("card_number>>",card_number);
    if (card_number != "") {
      setState({ ...state, deleteCardItem: { card: value, number: card_number } });
    }
    setActiveId(index);
    props.radioCardChange(value);
  }

  const deleteCard = () => {
    checkNet();
    setState({ ...state, isModal: true });
  }
  const makeConfirmPayment = (value, card_number) => {
    setIsLoading(false);
    setState({ ...state,isModalPopup: true , deleteCardItem: { card: value, number: card_number } });  
    props.radioCardChange(value); 
  }

  const makePayment = () => {
    setIsLoading(false);
    checkNet();
    setState({ ...state, isModalPopup: false }); 
    props.paybyCard();
  }

  const hidemakePayment = () => {
    setIsLoading(false);
    setState({ ...state, isModalPopup: false });  
  }

  const hidedefaultcard = () => {
    setIsLoading(false);
    setState({ ...state, ConfirmDefaultCard: false });  
  }

  const confirmdefaultcard = () => {
    setIsLoading(false);
    setState({ ...state, ConfirmDefaultCard: true });  
  }

  const hidesuccesspopup = () => {
    setIsLoading(false);
    setState({ ...state, SuccessConfirmDefaultCard: false });  
  }

  const setDefaultCard = () => {
    
      let status = checkNet();
      if(status){
        setIsLoading(true);
        let input = {
          driver: driverId,
          sourceId: deleteCardItem.card,
        } 
        ChangeDefaultCard({
          variables: { input }
        }).then((response) => { 
            setIsLoading(false); 
            // console.log("changeDefaultCard",response.data.changeDefaultCard); 
            if (response.data && response.data.changeDefaultCard.ok) {
              setState({ ...state, ConfirmDefaultCard: false, SuccessConfirmDefaultCard:true });
            } else {
              setState({ ...state, ConfirmDefaultCard: false, SuccessConfirmDefaultCard:false });
            }  
        });   
      }
  }
  // console.log("from_screen",props.from_screen);

  const setPaymentCard = (paymentcard,ind) => {
    props.radioCardChange(paymentcard);
    setState({ ...state, card_index:ind });
  }
  return (
    <>
      {!isLoading ? <ScrollView keyboardShouldPersistTaps="always" >

      <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModalPopup} >
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
             <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hidemakePayment(false)}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Text style={modal_style.modal_heading}>Confirmation</Text>
              <Text style={modal_style.modalText}>You are going to make this payment using card {deleteCardItem.number}. Are you sure?</Text>
              <View style={modal_style.modal_textbuttonWrap}>
                <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hidemakePayment(false)}>
                  <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                </TouchableHighlight>
                <TouchableHighlight underlayColor=""  style={modal_style.upload_document_modal}  onPress={() => makePayment(true)}>
                  <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                </TouchableHighlight> 
              </View>
            </View>
          </View>
        </Modal>

        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={ConfirmDefaultCard || SuccessConfirmDefaultCard} >
          <View style={modal_style.modal}>
            {ConfirmDefaultCard && 
                <View style={modal_style.modal_wrapper}>
                   <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hidemakePayment(false)}>
                      <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                    </TouchableOpacity>
                    <Text style={modal_style.modal_heading}>Confirmation</Text>
                  <Text style={modal_style.modalText}>You are going to set default card {"\n"} {deleteCardItem.number}. Are you sure?</Text>
                  <View style={modal_style.modal_textbuttonWrap}>
                    <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hidedefaultcard(false)}>
                      <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor=""  style={modal_style.upload_document_modal}  onPress={() => setDefaultCard(true)}>
                      <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                    </TouchableHighlight> 
                  </View>
                </View>
            }
            {SuccessConfirmDefaultCard && 
               <View style={modal_style.modal_wrapper}>
                  <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap}  onPress={() => hidesuccesspopup(false)}>
                    <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                  </TouchableOpacity>
                  <Text style={modal_style.modal_heading}>Success</Text>
                  <Text style={modal_style.modalText}>Default card has been set successfully to {deleteCardItem.number}.</Text>
                  <View style={modal_style.modal_textbuttonWrap_center}>
                    <TouchableHighlight underlayColor=""  style={modal_style.upload_document_modal}  onPress={() => hidesuccesspopup(false)}>
                      <Text style={modal_style.upload_document_text_modal}>OK</Text>
                    </TouchableHighlight> 
                  </View>
              </View>
            }
          </View>
        </Modal> 

        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal} >
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hideModal(false)}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Text style={modal_style.modal_heading}>Confirmation</Text>
              <Text style={modal_style.modalText}>Are you sure to delete card?{"\n"} {deleteCardItem.number}</Text>
              <View style={modal_style.modal_textbuttonWrap}> 
                <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hideModal(false)}>
                  <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                </TouchableHighlight>
                <TouchableHighlight underlayColor=""  style={modal_style.upload_document_modal}  onPress={() => hideModal(true)}>
                  <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>

        <KeyboardAvoidingView behavior="position"  >
          <View style={styles.container}> 

                

            <View style={styles.card_list_container}>
              {card_list && card_list.length > 0 ?
              
                <View key={isModal} style={styles.card_line_item} >

                  {card_list.map((cardlistitem, index) =>  
                      <>  
                        <TouchableOpacity style={styles.line_zelle} underlayColor="" onPress={() => setPaymentCard(cardlistitem.value,index)}> 
                          <View style={[styles.line_zelle_icon,{paddingVertical:13}]}>
                            {(cardlistitem.brand=="MasterCard")?
                            <Image  style={{width:45, height:40,resizeMode:"contain"}} source={require('../../assets/images/master-card.png')} />:
                            (cardlistitem.brand=="Visa")?
                            <Image  style={{width:50,resizeMode:"contain"}} source={require('../../assets/images/visa-card.png')} />:<Image  style={{width:50, height:50, borderRadius:40, resizeMode:"contain"}} source={require('../../assets/images/Amex.png')} />
                            } 
                         </View> 
                          <View style={styles.line_zelle_content} > 
                                <Text style={[styles.line_zelle_text, global_styles.lato_regular]}>{cardlistitem.label}</Text>
                          <Text style={[styles.line_zelle_text_small, global_styles.lato_regular]}>No service fee</Text> 
                          </View> 
                          {(card_index==index) &&
                            <TouchableHighlight underlayColor="" style={[styles.addbutton_payment]} onPress={() => makeConfirmPayment(cardlistitem.value,cardlistitem.label)} >
                              <Text style={styles.buttonText_payment}>PAY</Text>
                            </TouchableHighlight>}
                            
                      </TouchableOpacity> 
                      </> 
                  )} 

                  

                </View> : <Text style={[styles.no_card_found, global_styles.lato_regular]}>No card found.</Text>
              }
            </View> 

          </View>
        </KeyboardAvoidingView>

      </ScrollView> : <View><ActivityIndicator size="small" style={{ padding: 40 }} /></View>}
      {props.isLoading && !isLoading && <View><ActivityIndicator size="small" style={{ padding: 40 }} /></View>}
    </>
  );
}

// Stylesheet to create Car List UI
const styles = StyleSheet.create({
  container: {
    flex: 1, 
  },
  button_stripe: {
    fontSize: 14,
    marginRight: 5,
    marginTop: 1,
    position: "absolute",
    left: 0,
  },
  line_zelle_icon:{
    backgroundColor:"#f9f9f4",
    borderRadius:70,
    marginVertical:5,
    padding:8,
    paddingVertical:12,
    justifyContent:"center",
    alignContent:"center",
    alignItems:"center",
  },
  line_zelle:{
    flexDirection:"row",
    borderTopColor:"#d4f4f4",
    borderTopWidth:1,
    paddingTop:4,
    paddingHorizontal:15,
    backgroundColor:"#fff"
  },
  line_zelle_content:{
     marginHorizontal:15,
     paddingVertical:10
  },  
  line_zelle_text:{
    fontSize:16,
    color:"#858b94"
  },
  line_zelle_text_small:{
    fontSize:14,
    color:"#858b94"
  },
  radio_button: {
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "center",
  },
  no_card_found: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    fontSize: 12
  },
  button_card: {
    fontSize: 18,
  },
  buttonTextAddPaymentMethod: {
    fontSize: 12
  },
  delete_button_card: {
    position: "absolute",
    right: 4,
    top: 0,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    width: 40
  },
  delete_button_card_icon: {
    fontSize: 25
  },
  add_card_icon: {
    fontSize: 20,
  },
  payment_view_fields: {
    paddingHorizontal: 15,
    marginBottom: 20
  },
  payment_add_cards_text: {
    fontSize: 13,
  },
  payment_view_field_text: {
    fontSize: 14,
    position: "absolute",
    right: 15,
    top: 15
  },
  payment_method_wrap: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 10,
    backgroundColor: '#EEE'
  },
  card_text: {
    borderColor: "#ccc",
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 10,
    marginHorizontal: 10,
  },
  payment_method_title: {
    fontSize: 12
  },
  payment_method_items: {
    marginHorizontal: 15,
    marginTop: 15
  },
  payment_method_text: {
    fontSize: 13,
    paddingLeft: 20,
  },
  payment_add_cards: {
    marginTop: 20,
  },
  add_payment_card: {
    marginVertical: 10,
    marginHorizontal: 0,
    borderBottomWidth: 1,
    borderColor: "#f5f5f5",
    flex: 1,
    flexDirection: "row"
  },
  add_payment_card_link_wrap: {
    paddingTop: 8,
  },
  card_line_item: { 
    paddingHorizontal: 0,
    marginVertical: 0,
    marginHorizontal: 0,
    paddingBottom: 0,
    marginTop: 0,  
    width: "100%"
  },
  addbutton_payment: {
    paddingVertical: 0,
    paddingHorizontal: 20,
    marginHorizontal: 0,
    marginRight: 5,
    marginBottom: 10,
    backgroundColor: "#FFF",
    borderRadius: 5,
    borderWidth:1,
    borderColor:"#DB935F",
    position:"absolute", 
    top:"35%", 
    right:5,
    color:"#393e5c"
  },
  addbutton: {
    paddingVertical: 0,
    paddingHorizontal: 10,
    marginHorizontal: 0,
    marginRight: 5,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: Colors.color0,
    borderRadius: 5,
  },
  buttonText: {
    paddingVertical: 6,
    textAlign: "center",
    color:"#393e5c",
    fontSize: 12,
  },
  buttonText_payment: {
    paddingVertical: 6,
    textAlign: "center",
    fontSize: 14,
    color: "#DB935F"
  },
  modal_check: {
    color: "red",
    fontSize: 35,
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 7
  },
  message_global_text: {
    fontSize: 20,
    marginBottom: 0,
    paddingHorizontal:25,
    color: "#454545",
    textAlign: "center"
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    height: 180,
    borderRadius: 5,
    borderWidth: 1,
  },
  modalText: {
    marginVertical: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  modal_textbuttonWrap: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  upload_document_modal: {
    justifyContent: "center",
    marginLeft: 0,
    paddingHorizontal: 50,
    borderRadius: 6,
    paddingVertical: 8,
    marginRight: 7,
    backgroundColor: Colors.color0,
  },
  upload_document_text_modal: {
    color: "#FFFFFF",
    fontWeight: "bold",
    fontSize: 16,    
  },
  security_deposit_message:{
    paddingHorizontal:12,
    paddingVertical:12
  }
});