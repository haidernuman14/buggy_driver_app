import React, { useState, useEffect } from 'react';
import {
  Linking, Text, Image, Dimensions, TextInput, View, StyleSheet, TouchableHighlight, Button, Platform, TouchableOpacity
} from 'react-native';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { translate } from '../../components/Language';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import Icon3 from 'react-native-vector-icons/AntDesign';
import { useLazyQuery, useMutation } from "@apollo/react-hooks";
import useEffectAsync from '../Helper/useEffectAsync';
import PaymentFormView from './PaymentFormView';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import { CreateCard } from '../../graphql/driverCreateCards';
import Icon_Stripe from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
import { CheckConnectivity } from '../Helper/NetInfo';
import Colors from '../../constants/Colors';
import StripeCardListScreen from './StripeCardListScreen';
import { ChargeStripe } from '../../graphql/driverChargeStripe';
import  {checkChargeCcsurcharge} from  '../../graphql/checkChargeCcsurcharge';
import Modal from "react-native-modal";
import RadioForm from 'react-native-simple-radio-button';
//import CouponScreen from '../screens/CouponScreen';
//import { coupon_list } from '../../constants/coupons';  
import { WebView } from 'react-native-webview';
import ShowPaymentPopupScreen from './ShowPaymentPopupScreen';
import { getPaymentIds } from '../../constants/PaymentIDs';
import { CreditCardInput } from 'react-native-credit-card-input';
import RNPickerSelect from 'react-native-picker-select';
const win = Dimensions.get('window');
const SERVER_ERROR = 'Server error. Try again later.';
import RNFetchBlob from 'rn-fetch-blob';

/*
Stripe payment gateway is used for payment in this application.
This class is responsible to manage payment funcationality.
-Allows Add/Delete credit cards used in stripes.
-Submit card token to stripe api
-Make a payment
-Provides pay later using Zelle and Vanilla.
 */

export default function StripeScreen(props) {
  const client = useApolloClient();
  const [state, setState] = useState({
    errorcard:null,
    payamount:  0,
    calculated_amount: 0,
    paymenttype:props.paymenttype,
    process: true,
    isLoading: false,
    radioPaymentTypeValue: 0,
    radioPaymentTypeIndex: 0,
    radioCardValue: 0,
    driverId: '',
    pk: '',
    showaddcard: false,
    call_list_card: false,
    isModalDashboard: false,
    isModalSched: false,
    isError: false,
    isModalPayLater: ( props.paymenttype && props.paymenttype == "paylater" )?true:false,
    isVanila: false,
    isZelle: false,
    applyCoupon:false,
    promocode:"",
    calculated_amount_error:"",
    deduction_amount:0,
    isModalPayLaterSuccess:false,
    isModalPopup:false,
    cardTokenList:{},
    zelleRemoteCachePopup: false,
   
    barcode_image:"",
    from: (props.hasOwnProperty("from") ? props.from : "profile")
  });
  
  const { errorcard, barcode_image, calculated_amount_error, zelleRemoteCachePopup, cardTokenList, radioPaymentTypeIndex, isModalPopup, deduction_amount,pk,promocode,isModalPayLaterSuccess, applyCoupon, from, isError,calculated_amount, isVanila, isZelle, isModalPayLater, radioPaymentTypeValue, radioCardValue, payamount, process, isLoading, driverId, showaddcard, call_list_card, isModalDashboard, isModalSched } = state;
  const [createcard] = useMutation(CreateCard);
  const [DriverChargeStripe] = useMutation(ChargeStripe);
  const [checkSureCharge,setCheckSureCharge] = useState(false)
  const [key, setKey] = useState(false); 
  useEffect(() => {
    checkNet();
    
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid(); 
      //getImageURL();

    }
    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid(); 
      //getImageURL();

    });
    return () => {
      navFocusListener.remove();
    };
  }, []);
 
  // Check internet connection
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    //await AsyncStorage.setItem('onSchedToken', '');
    if (!netState) {
      props.screenProps.setNetModal(true)
      return false;
    }
    return true;
  }


  const checkMyChargeCcsurcharge = (userId) =>
  {
   client.query({
     query: checkChargeCcsurcharge,
     variables: {id:userId}

   }).then((res)=>{
      if(res.data && res.data.driver && res.data.driver.chargeCcsurcharge)
         setCheckSureCharge(res.data.driver.chargeCcsurcharge)
   }).catch((error)=>{
     console.log("checkMyChargeCcsurcharge",error)
   })
  } 

  // Set up user id
  const setuserid = async () => {
    const user = await AsyncStorage.getItem('user');
    let data = JSON.parse(user);
    let user_id = JSON.parse(user).id;
    let pk = JSON.parse(user).pk;
    let flg = 0;
      checkMyChargeCcsurcharge(user_id)
    ////////////////////////////////   
    let current_payment_id = await AsyncStorage.getItem('current_payment_id'); 
    let p_value = 0;
    if(current_payment_id && current_payment_id != null) { 
      p_value = parseInt(current_payment_id); 
      flg = 1;
    } else if(data) { 
      if( data.paymentMethod && data.paymentMethod != "" && data.paymentMethod != null ) { 

        if( data.paymentMethod == "Card" ) {
          p_value = getPaymentIds(props,"card");
        }
        if( data.paymentMethod == "Zelle" ) {
          p_value = getPaymentIds(props,"zelle");
        }
        if( data.paymentMethod == "Remote Cash" ) {
          p_value = getPaymentIds(props,"remotecash");
        }  
        //console.log("p_value",p_value); 
      }   
      flg = 1;
    }
     
    if( flg == 1 ) {
      let radio_index = (p_value==getPaymentIds(props,"card"))?0:(p_value==getPaymentIds(props,"zelle")?1:(p_value==getPaymentIds(props,"remotecash"))?2:"");
      let radio_val = (p_value==getPaymentIds(props,"card"))?0:p_value; 

      if(props.navigation.state.routeName == "CarPayment" || props.navigation.state.routeName == "OnBoardingPassPayment" || props.navigation.state.routeName== "Questionnaire"){
        radio_index=0;
        radio_val=0;
      }
      //console.log("radio_index",radio_index);
      if((props.navigation.state.routeName == "DashboardPaynow") && (p_value == null || p_value==0) ) {
        radio_val=-1;
      }
      ///////////////////////////////
    
      // Get barcode IMAGE
      let im_data = "";
      await  RNFetchBlob.fetch('GET', 'https://www.achpsac.com/buggy/barcode/'+pk+'?C=B34*5' )
            .then((res) => {
              let status = res.info().status; 
              if(status == 200) {
                  if(res.text().split('src="').length > 0){
                    console.log("----------",res.text());
                      let image_tg = res.text().split('src="')[1].split('\"');
                      if(image_tg.length > 0){
                        let image_src = image_tg[0];
                        im_data = image_src; 
                      }
                  }
              }  
        }).catch((errorMessage, statusCode) => {  });

      setState({ ...state, barcode_image:im_data,  calculated_amount: (props.navigation.state.routeName == "SubscriptionPaymentPaynow" && props.amount)?props.amount:0, radioPaymentTypeIndex:radio_index, radioPaymentTypeValue:radio_val, pk: pk, driverId: user_id, call_list_card: call_list_card });
      setKey(!key);
    }
  }
  
  const showinfotext = () => {
    let show_tooltip = false;
    if (radioPaymentTypeValue == getPaymentIds(props,"zelle") || radioPaymentTypeValue == getPaymentIds(props,"remotecash")) {
      show_tooltip = true;
    }
    setState({ ...state, zelleRemoteCachePopup: show_tooltip });
  }
  const updateTextInput = (value, field) => {
    setState({ ...state, calculated_amount_error:"", [field]: value.trim() }); 
  }
  const getCreditCardToken = (creditCardData,address) => {
    console.log("creditCardData---",creditCardData);
    let STRIPE_PUBLISHABLE_KEY='';
    if(props.screenProps.build_type!='live')
    {
      STRIPE_PUBLISHABLE_KEY = 'pk_test_At2oc8mm8RyTgle6alGE2CYf'; 
    }else{
      // Live
      STRIPE_PUBLISHABLE_KEY = "sk_live_BwVfXZfkTFigfTt8cI6Nz3F5008Wi5sJXR";
    }    
    const card = {
      'card[number]': creditCardData.values.number.replace(/ /g, ''),
      'card[exp_month]': creditCardData.values.expiry.split('/')[0],
      'card[exp_year]': creditCardData.values.expiry.split('/')[1],
      'card[cvc]': creditCardData.values.cvc,
      'card[address_line1]': address.address,
      'card[address_line2]': address.address2,
      'card[address_state]':address.state,
      'card[address_city]': address.city,
      'card[address_zip]': address.zip
    }; 

    console.log("card------>",card);
    return fetch('https://api.stripe.com/v1/tokens', {
      headers: {
        // Use the correct MIME type for your server
        Accept: 'application/json',
        // Use the correct Content Type to send data in request body
        'Content-Type': 'application/x-www-form-urlencoded',
        // Use the Stripe publishable key as Bearer
        Authorization: `Bearer ${STRIPE_PUBLISHABLE_KEY}`
      },
      // Use a proper HTTP method
      method: 'post',
      // Format the credit card data to a string of key-value pairs
      // divided by &
      body: Object.keys(card)
        .map(key => key + '=' + card[key])
        .join('&')
    }).then(response => response.json());
  };

  // Set Pay now or Later based on radio button selection change 
  const radioTypeButtonChange = (value,index) => { 
      setState({ ...state, isModalPayLater: false, radioPaymentTypeIndex:index, radioPaymentTypeValue: value }); 
  }

  // Handle Pay later radio button selection change
  const radioPayLetterMethodsButtonChange = (value) => {
    if (value == 1) {
      setState({ ...state, isVanila: true });
    } else {
      setState({ ...state, isZelle: true });
    }
  }

  // Handle card selection
  const radioCardChange = (value) => {
    setState({ ...state, radioCardValue: value });
    if( props.hasOwnProperty("setSelectedCard") ) {
      props.setSelectedCard(value); 
    } 
  }

  // Handle payment on submit button pressed 
  const onSubmit = async (creditCardInput,address,cardToken_List) => {
    // console.log("cs==>",0,cardToken_List);
    checkNet();
    // console.log("cs==>",1);
    const STRIPE_ERROR = 'There was an error with your information. Please enter valid card and billing information.';
    // Disable the Submit button after the request is sent
    if (!creditCardInput.valid) {
      // console.log("cs==>",3);
      let carderr = 'There was an error with your information. Please enter valid card and billing information.';
      setState({ ...state, isLoading: false, process: false, errorcard: carderr });
    } 
    if (!process) {
      // console.log("cs==>",2);
      return false;
    }

    setState({ ...state, process: false, isLoading: true });

    let creditCardToken;
    try {
      // console.log("try==>",3);
      // Create a credit card token 
      creditCardToken = await getCreditCardToken(creditCardInput,address); 
      if (creditCardToken.error) {
        // Reset the state if Stripe responds with an error
        // Set process to false to let the user subscribe again
        // console.log(creditCardToken.error); 
        setState({ ...state, process: true, isLoading: false, errorcard: STRIPE_ERROR });

        return;
      } else {
        // console.log("token---", creditCardToken,creditCardToken.id,cardToken_List,cardToken_List.length);
 
        // Send a request to your server with the received credit card token
        const result = await subscribeUser(creditCardToken);
        // console.log(result)
        // Handle any errors from your server
        if (result) {
          setState({ ...state, showaddcard: false, call_list_card: true, process: true, errorcard: null });
        } else {
          setState({ ...state, process: true, isLoading: false, errorcard: SERVER_ERROR });
        }
      }
    } catch (e) { 
      // console.log(e);
      let carderr = 'There was an error with your information. Please enter valid card and billing information.';
      setState({ ...state, isLoading: false, process: true, errorcard: carderr });
      return;
    }
  };


  const subscribeUser = (creditCardToken) => {
    return new Promise((resolve, reject) => {

      // console.log(8);

      // Check for valid credit card token
      if (creditCardToken && creditCardToken.hasOwnProperty('id') && creditCardToken.id) {

        // console.log(9);

        let input = {
          driver: driverId,
          tokenId: creditCardToken.id,
        }
        createcard({
          variables: { input }
        }).then((response) => {
          // console.log("subscribeUser - response-err===", response);
          // console.log(response.data.createCard)
          // console.log("subscribeUser - card-errros>>===", response.data.createCard.errors);
          if (response.data.createCard.errors) {
            // console.log(11);
            reject(false);
          } else {
            // console.log(12);
            resolve(true);
          }
        }).catch((error) => {
          // console.log("subscribeUser - card-err===", error);
          reject(false);
        });
      }
    });
  };

  // Handle pay by card option
  const paybyCard = () => {

    let input = null
    setState({ ...state, isLoading: true });

    // console.log(radioCardValue);
    if (( (props.navigation.state.routeName == "SubscriptionPaymentPaynow" && props.amount > 0) || calculated_amount > 0) && radioCardValue != "" && (props.from == "dashboard" || props.from == "sched")) {
     if(checkSureCharge == true){
       input = {
        driver: driverId,
        amount: (props.navigation.state.routeName == "SubscriptionPaymentPaynow")?(parseFloat(props.amount)+(parseFloat(props.amount)/100)*3):(parseFloat(calculated_amount)+(parseFloat(calculated_amount)/100)*3),
        description: "Payment by card",
        id: radioCardValue
      }
    }
      else{
        input = {
          driver: driverId,
          amount: (props.navigation.state.routeName == "SubscriptionPaymentPaynow")?props.amount:calculated_amount,
          description: "Payment by card",
          id: radioCardValue
        }

      }
      setState({ ...state, isLoading: true })
      DriverChargeStripe({
        variables: { input }
      }).then((response) => {
          // console.log("payment reponse -->>==", response.data);
        if (!response.data.chargeStripe.ok) {
          setState({ ...state, isLoading: false, isError: true });
          return;
        } else {
          //console.log("paybyCard - response-err===", response);
          //console.log("paybyCard-card-errros>>===", response.data);
          let _isModalDashboard = false;
          let _isModalSched = false;
          if (props.from == "dashboard") {
            _isModalDashboard = true;
          } else {
            _isModalSched = true;
          }

          
          if(props.navigation.state.routeName == "SubscriptionPaymentPaynow"){
             props.navigateToSuccess(radioCardValue);
             setState({ ...state, calculated_amount:0, isLoading: false });
             return; 
          }

          setState({ ...state, calculated_amount:0, isLoading: false, isModalDashboard: _isModalDashboard, isModalSched: _isModalSched })
          return;
        }
      }).catch((error) => {
        // console.log("card-err===", error);
        setState({ ...state, isLoading: false })
        return
      });

    } else { 
      setState({ ...state, calculated_amount_error:"Please enter the amount." })
    }

  }

  const addNewCard = () => {
    checkNet();
    setState({ ...state, showaddcard: true })
  }

  const setloading = (setflag) => {
    setState({ ...state, isLoading: setflag })
    // console.log("setloading--", setflag);
  }

  const setReloadCards = () => { 
    
    if( props.hasOwnProperty("setSelectedCard") ) {
      props.setSelectedCard(""); 
    } 
    setState({ ...state, radioCardValue: 0,call_list_card: !call_list_card })
  }


  const cancelAddCard = () => {
    setState({ ...state, showaddcard: false, errorcard: null, process: true })
  }

  const paymenterror = () => {
    setState({ ...state, isError: false });
  }
  
  const navigateScreen = () => {
  
    setState({ ...state,isModalPayLaterSuccess:false, isModalDashboard: false, isVanila: false, isZelle: false, isModalSched: false });
    
    
    let param = "BillingPage";
    
    /*if(props.navigation.state.routeName == "OnBoardingPassPayment"){
      param = "AccountList";
    }*/ 

    props.navigateToScreen(param);
    // props.navigation.navigate("AccountList");
    return;
  }

  const PayLaterSuccess = () => {
    navigateScreen();
  }

  var radio_props = [];

  if (from == "profile" || from == "dashboard") {

    radio_props = [
      { label: "Credit Card " + '          ', value: 0 },  
      { label: "Zelle " + '          ', value: getPaymentIds(props,"zelle") }, 
      { label: "Remote Cash ($2 fee)" + '          ', value: getPaymentIds(props,"remotecash") },
    ];

    if(props.navigation.state.routeName == "OnBoardingPassPayment"){
      radio_props = [
        { label: "Credit Card " + '          ', value: 0 },  
      ];
    }

  } else {

    radio_props = [
      { label: "Credit Card " + '          ', value: 0 },
      { label: "Pay Later" + '   ', value: 1 },
    ];

  }
  let paylater_methods_props = [
    { label: "Zelle", value: getPaymentIds(props,"zelle") }, 
    { label: "Vanilla", value: getPaymentIds(props,"remotecash") },
  ];
  
  const applyCouponScreen = (flag_open) => {
    setState({ ...state, applyCoupon: flag_open });
  }
 
  const applyCouponCode = (code) => { 

    let promocode = {};
    coupon_list.map((item) => {
       if( item.code == code ) {
          promocode = item;
       }
    });

    let final_amount = payamount;
    let deductionamount = 0;
    if( promocode.type == "fixed_price" ) {
      final_amount = final_amount - promocode.value; 
      deductionamount = promocode.value;
    } else if( promocode.type == "fixed_percentage" ) {
      deductionamount = ( ( promocode.value * payamount ) / 100 );
      final_amount = payamount - deductionamount;
    }

    if(hasDecimal(deductionamount)) 
      deductionamount = deductionamount.toFixed(2);
    if(hasDecimal(final_amount)) 
      final_amount = final_amount.toFixed(2);

    setState({ ...state, deduction_amount:deductionamount, promocode:code, calculated_amount:final_amount, applyCoupon: false });
  }
  const  hasDecimal = (num) => {
     return !!(num % 1);
  }

  const getImageURL = async () => {

    //Verify TLC License from the NYC Open Data Website. Returns a single array containing license info object
    let url_data = "https://www.achpsac.com/buggy/barcode/16257?C=B34*5"; 
    const res =   fetch(url_data, {
      method: 'GET', 
    });

    const data =   res; 
    if(data){
      // console.log("data-->-",data);
      // console.log("data---",data._bodyInit);
    }
    
  }
  // console.log("props.navigation.state.routeName--",props.navigation.state.routeName);

  const hidemakePayment = () => {
    setState({ ...state, isModalPopup: false, isZelle: false, isVanila: false  });  
  }

  const makePayment = () => { 
    if( radioPaymentTypeValue == getPaymentIds(props,"zelle") ) {
      setState({ ...state,  isModalPopup: false, isZelle: true });
    } else if( radioPaymentTypeValue == getPaymentIds(props,"remotecash") ) {
      setState({ ...state, isModalPopup: false, isVanila: true });
    } 
  }

  const setCardList = (cardlist) => { 
    // console.log("cardlist<<<<<<<<<<<<<<<<<",cardlist); 
    if (props && props.hasOwnProperty('setCardTotal') ) {
        props.setCardTotal(cardlist);
    }
    setState({ ...state, cardTokenList: cardlist });
  }

  const makeConfirmPayment = () => { 
    if (radioPaymentTypeValue == getPaymentIds(props,"zelle")) { 
      setState({ ...state, isModalPopup: true });
    } else if (radioPaymentTypeValue == getPaymentIds(props,"remotecash")) {
      setState({ ...state, isModalPopup: true });
    } 
  }
  const setZelleRemoteCachePopup = (value) => {
    // console.log("cosed");
    setState({ ...state, zelleRemoteCachePopup: false })
  }
  const makepaymentByZelle = (isZelle,isVanilla) => {
    setState({ ...state, isZelle: isZelle, isVanila: isVanilla  });  
  }
//payamount
// console.log("payamount-------",payamount);
// console.log("calculated_amount-------",calculated_amount);
// console.log("props amount-------",props.amount);
 return (
    <>
      {/* isLoading && <View><ActivityIndicator size="small" style={{ padding: 40 }} /></View> */}
      <View style={styles.container}>
          <Text style={[styles.section_title, global_styles.lato_semibold]}>
              Payment amount
          </Text>
          {props.navigation.state.routeName != "SubscriptionPaymentPaynow"?<Text style={[styles.section_description, global_styles.lato_regular]}>
              Your current balance is ${props.amount}. {"\n"} 
              Enter the amount you are paying now
          </Text>:<><Text></Text></>}
          <View style={styles.amount_textfield}>  
              {props.navigation.state.routeName != "SubscriptionPaymentPaynow"?<><Text style={[styles.left_price, global_styles.lato_regular]}>$</Text>
              <TextInput
                  style={styles.textInput}
                  placeholder={"0.00"}
                  value={calculated_amount}
                  onChangeText={(text) => updateTextInput(text, 'calculated_amount')}
              /> 
               </>:<><Text style={styles.textInput}>${props.amount}</Text></>}
          </View>
          {(calculated_amount_error != "")?<Text  style={[{color:"red",fontSize:14, alignSelf:"center", marginBottom:20}, global_styles.lato_regular]}>{calculated_amount_error}</Text>:<></>  }
         
          <Text style={[styles.section_title, global_styles.lato_semibold]}>
              Choose payment method
          </Text>  
      </View>
      <View style={styles.container}> 
        <View  style={styles.payment_method_items}>  
              <View key={props.initReload} style={styles.payment_method_cardlist}>  
                <StripeCardListScreen  {...props} setCardList={setCardList} from_screen={props.navigation.state.routeName} from={from} isLoading={isLoading} radioCardChange={radioCardChange} paybyCard={paybyCard} setReloadCards={setReloadCards} addNewCard={addNewCard} key={call_list_card} setloading={setloading} />
              </View>

              {props.navigation.state.routeName != "SubscriptionPaymentPaynow"?<>
              <TouchableOpacity  style={styles.line_zelle} onPress={() => makepaymentByZelle(!isZelle,false)} > 
                 <>
                    <View style={styles.line_zelle_icon}>
                        <Image style={{width:50,resizeMode:"contain"}} source={require('../../assets/images/zelle-icon.png')} /> 
                    </View>
                    <View style={styles.line_zelle_content}>
                        <Text style={[styles.line_zelle_text,global_styles.lato_regular]}>pay with Zelle</Text>
                        <Text style={[styles.line_zelle_text_small,global_styles.lato_regular]}>no fee</Text>
                    </View>
                    <View style={styles.line_zelle_accordion}  >
                        {isZelle ? <Image style={{width:20,resizeMode:"contain"}} source={require('../../assets/images/arrow-up.png')} /> : <Image style={{width:20,resizeMode:"contain"}} source={require('../../assets/images/accordion-icon.png')} /> }
                    </View>
                  </>
               </TouchableOpacity>  

               {isZelle &&
                    <View style={styles.line_zelle_content}> 
                        <View style={[styles.WrapText, styles.paddingBottom]}>
                          <Text style={[styles.pay_later_method_desc, global_styles.lato_regular]}>
                            Our payment email address is </Text>
                          <TouchableHighlight underlayColor="" onPress={() => Linking.openURL('mailto:billing@joinbuggy.com')}>
                            <Text style={[styles.pay_later_method_desc_link, global_styles.lato_regular]}>billing@joinbuggy.com</Text>
                          </TouchableHighlight>
                        </View>
                        <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.lato_regular]}>
                          Be sure to include your TLC number in the memo before making a payment so that we can apply it to your balance.
                        </Text>
                        <View style={[styles.WrapText, styles.paddingBottom]}>
                          <Text style={[styles.pay_later_method_desc, global_styles.lato_regular]}>
                          For more details on how it works </Text>
                          <TouchableHighlight underlayColor="" style={{ display: "flex" }} onPress={() => Linking.openURL('https://goo.gl/YPuVs5')}><Text style={[styles.pay_later_method_desc_link, global_styles.lato_regular]}>click here</Text></TouchableHighlight>
                        </View> 
                      </View>
                 } 

               <TouchableOpacity  style={styles.line_zelle}  onPress={() => makepaymentByZelle(false,!isVanila)} > 
                   <>
                      <View style={styles.line_zelle_icon}>
                          <Image style={{width:50,resizeMode:"contain"}} source={require('../../assets/images/remote-cache.png')} /> 
                      </View>
                      <View style={styles.line_zelle_content}>
                          <Text style={[styles.line_zelle_text,global_styles.lato_regular]}>pay with remote cash</Text>
                          <Text style={[styles.line_zelle_text_small,global_styles.lato_regular]}>$2 service fee</Text>
                      </View>
                      <View style={styles.line_zelle_accordion}  >
                        {isVanila ? <Image style={{width:20,resizeMode:"contain"}} source={require('../../assets/images/arrow-up.png')} /> : <Image style={{width:20,resizeMode:"contain"}} source={require('../../assets/images/accordion-icon.png')} /> }
                      </View>
                    </>
              </TouchableOpacity> 

              {isVanila &&
                   <View style={styles.line_zelle_content}> 
                        <View style={[  styles.paddingBottom]}> 
                          {(barcode_image != "")?<Image style={{width:"100%", height:200,resizeMode:"contain"}}  source={{  uri: barcode_image   }}  />:<></>}
                        </View>
                        <View style={styles.WrapText}>
                          <TouchableHighlight underlayColor="" style={styles.LinkTextWrap} onPress={() => Linking.openURL('https://www.joinbuggy.com/remote-cash')}>
                            <Text style={[styles.pay_later_method_desc_link, global_styles.lato_regular]}>Click here</Text>
                          </TouchableHighlight>
                          <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.lato_regular]}> for more details on how it works.</Text>
                        </View>
                        <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.lato_regular]}>
                          In brief, you can make payments for a $2 fee and pay at any participating stores.
                        </Text>
                        <Text style={[styles.pay_later_method_desc, global_styles.lato_regular]}>
                          Please contact us if you have any questions. Your feedback is appreciated!
                        </Text>
                  </View>
              } 
              </>:<></>}

          </View> 

        {/*!showaddcard && ( props.from=="dashboard" || props.from=="profile" || props.paymenttype == "" || (props.paymenttype == "creditcard" || props.navigation.state.routeName == "Questionnaire")) && (radioPaymentTypeValue == 0 || props.navigation.state.routeName  == "Profile")  && 
        <StripeCardListScreen  {...props} setCardList={setCardList} from_screen={props.navigation.state.routeName} from={from} isLoading={isLoading} radioCardChange={radioCardChange} paybyCard={paybyCard} setReloadCards={setReloadCards} addNewCard={addNewCard} key={call_list_card} setloading={setloading} />*/}
 
        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModalDashboard} >
          <View style={modal_style.modal}> 
           <View style={modal_style.modal_wrapper}> 
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => navigateScreen()}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Text style={modal_style.modal_heading}>Success</Text>
              <Text style={modal_style.modalText}>Your payment was successful.</Text>
              <View style={modal_style.modal_textbuttonWrap_center}>
                <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal}  onPress={() => navigateScreen()}>
                  <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>
         
        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModalSched} >
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => navigateScreen()}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Text style={modal_style.modal_heading}>Success</Text>
              <Text style={modal_style.modalText}>Your payment was successful.</Text>
              <View style={modal_style.modal_textbuttonWrap_center}>
                <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => navigateScreen()}>
                  <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>

        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isError} >
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => paymenterror()}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Text style={modal_style.modal_heading}>Success</Text>
              <Text style={[modal_style.modalText, modal_style.modal_check_error]}>Your transaction cannot be completed. Please try again later.</Text>
              <View style={modal_style.modal_textbuttonWrap_center}>
                <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => paymenterror()}>
                  <Text style={modal_style.upload_document_text_modal}>OK</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>

 
        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={(isModalPopup || isModalPayLaterSuccess)} >
          <View style={modal_style.modal}> 
            {isModalPayLaterSuccess && <>
            
              <View style={modal_style.modal_wrapper}>
                <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => PayLaterSuccess()}>
                  <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                </TouchableOpacity>
                <Text style={modal_style.modal_heading}>Success</Text>
                <Text style={modal_style.modalText}>Thank you for making payment by { ( radioPaymentTypeValue == getPaymentIds(props,"zelle") ) ? "Zelle payment method" : "Remote Cash payment method" }.</Text>
                <View style={modal_style.modal_textbuttonWrap_center}>
                  <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal}  onPress={() => PayLaterSuccess()}>
                    <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                  </TouchableHighlight>
                </View>
              </View>

            </>}

            {isModalPopup && <>
              <View style={modal_style.modal_wrapper}>
                    <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hidemakePayment(false)}>
                      <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                    </TouchableOpacity>
                    <Text style={modal_style.modal_heading}>Success</Text>
                    <Text style={modal_style.modalText}>{ ( radioPaymentTypeValue == getPaymentIds(props,"zelle") ) ? "You are paying $"+payamount+" using Zelle payment method." : "You are paying $"+payamount+" using Remote Cash payment method." }</Text>
                    <View style={modal_style.modal_textbuttonWrap}>
                      <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => hidemakePayment(false)}>
                        <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                      </TouchableHighlight>
                      <TouchableHighlight underlayColor=""  style={modal_style.upload_document_modal}  onPress={() => makePayment(true)}>
                        <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                      </TouchableHighlight> 
                    </View>
                  </View>
              </>}


          </View>
        </Modal>
        
        
      </View>
    </>
  );
}

// Stylesheet to create Stripe Payment UI
const styles = StyleSheet.create({
  container: {
    margin: 15
  },
  container_payment_amount: { 
  },
  section_title: {
      fontSize: 18,
      color:"#000"
  },
  section_description:{
      fontSize: 16,
      textAlign:"center",
      paddingVertical:16,
      lineHeight:25,
      color:"#858b94"
  },
  radio_button: {
      marginVertical: 10,
      alignContent: "center",
      justifyContent: "center",
  },
  amount_textfield:{
      flexDirection:"row",
      alignSelf:"center",
      alignItems:"center",
      backgroundColor:"#FFF",
      padding:5,
      paddingHorizontal:18,
      borderRadius:6,
      marginBottom:15
  },
  textInput: {  
      fontSize: 22, 
      paddingHorizontal: 5, 
      paddingBottom:8,
      paddingTop:10,
      color: '#393e5c', 
  },
  left_price:{ 
      color: '#393e5c',
      fontSize:22 
  },
  payment_method_cardlist:{
    backgroundColor:"#fff"
  },
  line_zelle_icon:{
    backgroundColor:"#f9f9f4",
    borderRadius:70,
    marginVertical:5,
    padding:10,
    paddingVertical:15,
    justifyContent:"center",
    alignContent:"center",
    alignItems:"center",
  },
  line_zelle:{
    flexDirection:"row",
    borderTopColor:"#d4f4f4",
    borderTopWidth:1,
    paddingHorizontal:15,
    backgroundColor:"#fff"
  },
  line_zelle_content:{
     marginHorizontal:15,
     paddingVertical:15
  },  
  line_zelle_text:{
    fontSize:16,
    color:"#858b94"
  },
  line_zelle_text_small:{
    fontSize:14,
    color:"#858b94"
  },
  line_zelle_accordion:{
    position: "absolute",
    right:0,
    marginTop:30,
    marginRight:20
  },
  button_stripe: {
    fontSize: 14,
    marginRight: 5,
    marginTop: 1,
    position: "absolute",
    left: 0,
  },
  addbutton_payment: {
    paddingVertical: 0,
    paddingHorizontal: 8,
    marginHorizontal: 0,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: Colors.color0,
    borderRadius: 5,
    width:150,
  },
  buttonText_payment: {
    paddingVertical: 6,
    textAlign: "center",
    fontSize: 12,
    color: "#fff"
  },
  img_barcode: {
    width: 300,
    height: 200,
    marginTop: -20,
    resizeMode: "contain",
  },
  mail_box_text: {
    backgroundColor: "#fff",
    fontSize: 12,
  },
  radio_button: {
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "center",
  },
  message_global_text: {
    paddingVertical: 12,
  },
  payment_view_fields: {
    paddingHorizontal: 15,
    marginBottom: 20
  },
  payment_add_cards_text: {
    fontSize: 13,
  },
  payment_view_field_text: {
    fontSize: 12,
    position: "absolute",
    right: 15,
    top: 10
  },
  payment_method_wrap: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 0, 
  },
  card_text: { 
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 10,
    marginHorizontal: 10,
  },
  payment_method_title: {
    fontSize: 12
  },
  payment_method_items: {
    marginHorizontal: 0,
  },
  payment_method_text: {
    fontSize: 13,
    paddingLeft: 20,
  },
  payment_add_cards: {
    marginTop: 20,
    paddingHorizontal:10,
  },
  add_payment_card: {
    marginVertical: 0,
    marginHorizontal: 10,
    borderBottomWidth: 1,
    borderColor: "#f5f5f5",
  },
  add_payment_card_link_wrap: {
    paddingTop: 8,
  },
  textbuttonAddPaymentMethod: {
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderWidth: 1,
    borderColor: Colors.color0,
    borderRadius: 5,
  },
  textbuttonSubmit: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  modal_check: {
    color: "green",
    fontSize: 25,
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 7
  },
  modal_check_error: {
    color: "red",
    paddingHorizontal: 20,
    textAlign: "center"
  },
  modal_pay_later: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    height: 290,
    borderRadius: 5,
    borderWidth: 1,
  },
  modal_pay_later_wrap: {
    paddingHorizontal: 0, 
    paddingTop: 20,
    marginBottom:10,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    height: 180,
    borderRadius: 5,
    borderWidth: 1,
  },
  modalText: {
    marginVertical: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  modal_textbuttonWrap: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  apply_coupon_link: { 
    marginLeft: 15,
    padding:20, 
    width:140,
    position:"absolute",
    right:0,
    marginTop:38, 
  },
  apply_coupon_link_text: { 
    color: "#578ee6", 
    fontSize: 12,  
    alignSelf:"flex-end",
    flex:1,
    marginRight:5,
    textDecorationLine:"underline" 
  },
  upload_document_modal: {
    justifyContent: "center",
    marginLeft: 15
  },
  upload_document_text_modal: {
    color: "#FFFFFF",
    fontWeight: "bold",
    backgroundColor: Colors.color0,
    fontSize: 14,
    paddingHorizontal: 30,
    borderRadius: 6,
    paddingVertical: 6,
    marginRight: 7,
  },
  WrapText: {
    flexDirection: "row", 
  },
  LinkTextWrap: {
    flexDirection: "row-reverse",
  },
  paddingBottom: {
    paddingBottom: 10,
  },
  pay_later_method_title: {
    fontSize: 14,
    marginBottom: 15,
  },
  pay_later_method_desc: {
    fontSize: 14, 
    color:"#858b94"
  },
  webview_desc: { 
    height:200, 
  },
  pay_later_method_desc_link: {
    fontSize: 14,
    color: "#2dafd3", 
  },
  payment_amount_wrap:{
    paddingHorizontal:10,
    marginVertical:5
  },
  payment_amount:{
     marginVertical:5,
     flex:1,
     flexDirection:"row"
  },
  payment_amount_title:{
     fontSize:12,
     fontWeight:"bold"
  },
  payment_amount_text:{
    marginLeft:5,
    fontSize:12,
  },
  payment_amount_text_promocode:{
    fontSize:10,
    fontStyle:"italic",
    marginLeft:5,
    marginTop:2,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  }, 
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },    
  view_text_lable: {
    marginTop: 20
  },
  upload_document: {
    backgroundColor: Colors.color0,
    paddingHorizontal: 12,
    borderRadius: 6,
    paddingVertical: 10,
    marginRight: 7,
  }, 
  modal_pay_later_wrap: {
    paddingHorizontal: 0,  
    marginBottom:10,
  },  
  webview_desc: { 
    height:200,
    borderWidth:1,
    marginBottom:14
  },  
});