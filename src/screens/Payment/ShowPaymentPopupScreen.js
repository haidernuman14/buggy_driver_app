import React, { useState, useEffect } from 'react';
import { Animated, Linking,Dimensions, StyleSheet, ScrollView, ActivityIndicator, TouchableHighlight, View, Text, TextInput, Image, KeyboardAvoidingView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'; 
import { global_styles } from '../../constants/Style';
import Colors from '../../constants/Colors';
import { translate } from '../../components/Language'; 
import { CheckConnectivity } from '../Helper/NetInfo';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from "moment"; 
import { WebView } from 'react-native-webview';
import { getPaymentIds } from '../../constants/PaymentIDs';

const win = Dimensions.get('window');
 
export default function ShowPaymentPopupScreen(props) {
   
  const [state, setState] = useState({ 
    user_id:props.user_id,
    slideUp:{},
    animation: new Animated.Value(0)
  })

  const [isLoading, setIsLoding] = useState(false);
  const {  user_id, animation, slideUp } = state;
  useEffect(()=>{
    const slideUp = {
      transform: [
        {
          translateY: animation.interpolate({
            inputRange: [0, 100],
            outputRange: [0, 100],
            extrapolate: "extend",
          }),
        },
      ],
    };
    setState({...state,slideUp:slideUp})
  },[])  

  
  const hidePopup = () => {
    let show_tooltip = false; 
  }


  return (
    <> 
      {isLoading &&
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>}
       <Animated.View style={[styles.Animated, slideUp]}> 
          <KeyboardAvoidingView behavior='padding'>
            <View style={styles.title}>
              <Text style={styles.titleText}></Text>
              <TouchableHighlight style={{width:50, height:50}} underlayColor="" onPress={() => props.setZelleRemoteCachePopup(false)  }>
                <Text style={styles.titleClose}><Icon3 style={styles.close_text_icon} name='close' /></Text>
              </TouchableHighlight>
            </View> 
            <View style={styles.wrapper}>  



              <View style={styles.view_text_lable} >


                {props.payment_type == getPaymentIds(props,"zelle") &&
                    <View style={styles.modal_pay_later_wrap}>
                      <Text style={[styles.pay_later_method_title, global_styles.lato_semibold]}>Zelle</Text>
                      <View style={[styles.WrapText, styles.paddingBottom]}>
                        <Text style={[styles.pay_later_method_desc, global_styles.lato_regular]}>
                          Our payment email address is </Text> 
                      </View>
                      <TouchableHighlight underlayColor="" onPress={() => Linking.openURL('mailto:billing@joinbuggy.com')}>
                          <Text style={[styles.pay_later_method_desc_link, global_styles.lato_regular]}>billing@joinbuggy.com</Text>
                        </TouchableHighlight>
                      <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.lato_regular]}>
                      {"\n"}Be sure to include your TLC License number in the memo, before sending the payment. {"\n"}
                      </Text>
                      <View style={[styles.WrapText, styles.paddingBottom]}>
                        <Text style={[styles.pay_later_method_desc, global_styles.lato_regular]}>
                          For more information on how it works: </Text>
                        <TouchableHighlight underlayColor="" style={{ display: "flex" }} onPress={() => Linking.openURL('https://goo.gl/YPuVs5')}><Text style={[styles.pay_later_method_desc_link, global_styles.lato_regular]}>Click here</Text></TouchableHighlight>
                      </View>
                      <Text style={[styles.pay_later_method_desc, global_styles.lato_regular]}>Please feel free to reach out to us if you need any assistance.
                          </Text>
                    </View>
              }  

              {props.payment_type == getPaymentIds(props,"remotecash") &&
                    <View style={styles.modal_pay_later_wrap}>
                      <Text style={[styles.pay_later_method_title, global_styles.lato_semibold]}>Vanilla</Text>
                      <View style={[styles.webview_desc, styles.paddingBottom]}>
                        { ( user_id && user_id!="" ) && 
                          <WebView
                            source={{ uri: "https://www.achpsac.com/buggy/barcode/"+user_id+"?C=B34*5" }}
                            style={{borderWidth:1, marginTop: 20 }} 
                          />
                        }
                      </View>
                      <View style={styles.WrapText}>
                        <TouchableHighlight underlayColor="" style={styles.LinkTextWrap} onPress={() => Linking.openURL('https://www.joinbuggy.com/remote-cash')}>
                          <Text style={[styles.pay_later_method_desc_link, global_styles.lato_regular]}>Click here</Text>
                        </TouchableHighlight>
                      <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.lato_regular]}> for more details on how it works. {"\n"}</Text>
                      </View>
                      <Text style={[styles.pay_later_method_desc, styles.paddingBottom, global_styles.lato_regular]}>
                        In brief, you can make payments for a $2 fee and pay at any participating stores. {"\n"}
                      </Text>
                      <Text style={[styles.pay_later_method_desc, global_styles.lato_regular]}>
                        Please contact us if you have any questions. Your feedback is appreciated!
                      </Text>
                    </View>
              }

              </View> 




            </View> 
            
            </KeyboardAvoidingView> 
      </Animated.View>
    </>
  );
}

const styles = StyleSheet.create({
    
  wrapper: { 
    paddingHorizontal: 25, 
    paddingBottom:50,
  },  
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },   
  text_icon: {
    color: '#9e9d9b',
    fontSize: 22,
    position: "absolute",
    right: 0,
    top: 0,
    bottom: 0
  }, 
  view_text_lable: {
    marginTop: 0
  },
  upload_document: {
    backgroundColor: Colors.color0,
    paddingHorizontal: 12,
    borderRadius: 6,
    paddingVertical: 10,
    marginRight: 7,
  }, 
  modal_pay_later_wrap: {
    paddingHorizontal: 0,  
    marginBottom:10,
  },
  pay_later_method_title: {
    fontSize: 17,
    marginBottom: 15,
  },
  pay_later_method_desc: {
    fontSize: 13, 
  },
  webview_desc: { 
    height:200,
    borderWidth:1,
    marginBottom:10
  },
  pay_later_method_desc_link: {
    fontSize: 13,
    color: "#0d73e0",
    textDecorationLine: "underline"
  },
  WrapText: {
    flexDirection: "row",
  },
  LinkTextWrap: {
    flexDirection: "row-reverse",
  },
});
