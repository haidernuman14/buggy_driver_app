import React, { useState, useEffect } from 'react';
import {
  ActivityIndicator, Text, KeyboardAvoidingView, View,  TouchableOpacity, Image, StyleSheet, TouchableHighlight, ScrollView
} from 'react-native'; 
import { global_styles, modal_style } from '../../constants/Style'; 
import AsyncStorage from '@react-native-community/async-storage';
import { CheckConnectivity } from '../Helper/NetInfo';
import { coupon_list } from '../../constants/coupons';  
import RadioForm from 'react-native-simple-radio-button';
import { TextField } from 'react-native-material-textfield';
import Colors from '../../constants/Colors';
import Modal from "react-native-modal";
import Icon3 from 'react-native-vector-icons/AntDesign';
import { translate } from '../../components/Language';

export default function CouponScreen(props) {

  const [state, setState] = useState({ 
    selected_coupon:{},
    couponcode:"",
    key: false,
    couponcode_error:"",
    isModal: false,
    deduction_amount:0,
    payamount: props.payamount,
  }); 
  const [isLoading, setIsLoading] = useState(false);
  const { key, couponcode, deduction_amount,payamount, isModal, couponcode_error } = state; 
  const [activeId, setActiveId] = useState(0);
  const [radio_coupon_list, setRadioCouponList] = useState();
 

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  } 

  useEffect(() => {
    checkNet();
    
    // console.log("coupon_list--",coupon_list);

    let coupon_list1 = [];
    if (coupon_list && coupon_list.length > 0) {
      coupon_list.map((item) => {
        coupon_list1.push({ label:item.title, value:item.id });
      });
      // console.log("coupon_list1--",coupon_list1);
      setRadioCouponList(coupon_list1); 
      
    } 
    
  }, []);  

  
  const radioChangeCoupon = (value, index) => {
    let current_item = "";
    if (coupon_list && coupon_list.length > 0) {
      coupon_list.map((item) => {
        if (item.id == value) {
          current_item = item
        }
      });
    }
    if (current_item != "") { 
      setState({ ...state, key:!key, couponcode:current_item.code,selected_coupon: current_item });
      if(couponcode != "")
        console.log(current_item.title);
    }
    setActiveId(index); 
  }

  const updateTextInput = (value) => {
  
    setState({ ...state, couponcode: value });
  }

  const applyCouponValidate = () => { 
    let promocode = [];
    coupon_list.map((item) => {
       if( item.code == couponcode ) {
          promocode = item;
       }
    });
    if(promocode.length <= 0){
      setState({ ...state, couponcode_error: "Please enter valid coupon code." });
    } else {

      let final_amount = payamount;
      let deductionamount = 0;
      if( promocode.type == "fixed_price" ) {
        final_amount = final_amount - promocode.value; 
        deductionamount = promocode.value;
      } else if( promocode.type == "fixed_percentage" ) {
        deductionamount = ( ( promocode.value * payamount ) / 100 ); 
      } 
      if(hasDecimal(deductionamount)) 
        deductionamount = deductionamount.toFixed(2);
      setState({ ...state, deduction_amount:deductionamount, isModal: true });
    }
  }
  const  hasDecimal = (num) => {
      return !!(num % 1);
  }
  const applyCoupon = () => { 
    let promocode = [];
    coupon_list.map((item) => {
       if( item.code == couponcode ) {
          promocode = item;
       }
    });
    if(promocode.length <= 0){
      setState({ ...state, couponcode_error: "Please enter valid coupon code." });
    } else {
      props.applyCoupon(couponcode);
    }
  }
 
  return (
    <>
      <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal}  >
        <View style={modal_style.modal}>
          
          <View style={modal_style.modal_wrapper}>
            <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => applyCoupon(couponcode)}>
              <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
            </TouchableOpacity>
             <Icon3 name="checkcircleo"  style={modal_style.modal_check} /> 
            <Text style={[modal_style.modalText,global_styles.lato_semibold]}>"{couponcode}" applied</Text>
            <Text style={modal_style.modalTextAmount}>${deduction_amount}</Text>
            <Text style={modal_style.modalText}>saving with this coupon</Text>
            <View style={modal_style.modal_textbuttonWrap_center}>
              <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => applyCoupon(couponcode)}>
                <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Modal>

       <ScrollView keyboardShouldPersistTaps="always" > 
        <KeyboardAvoidingView behavior="position"  >
          <View style={styles.container}>

          {/* <View style={styles.payment_method_title_wrap}>
              <Text style={[styles.payment_method_title]} >Apply Coupon Code</Text>
            </View> */}  
            <View style={styles.card_list_container}>

              <View key={key} style={styles.add_payment_coupon}> 
                      <TextField
                          fontSize={12}
                          labelFontSize={12}
                          lineWidth={1}
                          contentInset={{ top: 0, left: 0, right: 0, lable: 0, input: 5 }}
                          containerStyle={styles.input_cotainer}
                          style={styles.input}
                          placeholder={"Coupon Code"}
                          value={couponcode}
                          defaultValue={couponcode}
                          onChangeText={(text) => updateTextInput(text)}
                          error={couponcode_error}
                        />
              </View>

              <View style={styles.textbuttonSubmit_Wrapper}>
                <TouchableHighlight underlayColor='' style={styles.textbuttonSubmit} onPress={() => applyCouponValidate() } >
                  <Text style={styles.buttonText}>Apply Coupon</Text>
                </TouchableHighlight>
                <TouchableHighlight underlayColor='' style={styles.textbuttonCancel} onPress={() => props.applyCouponScreen(false)} >
                  <Text style={styles.buttonTextCancel}>{translate("cancel")}</Text>
                </TouchableHighlight>
              </View> 

              {radio_coupon_list && radio_coupon_list.length > 0 ?
                <View  style={styles.c_line_item} >
                  <RadioForm
                    animation={false}
                    radio_props={radio_coupon_list}
                    initial={""}
                    buttonColor={'#ccc'}
                    selectedButtonColor={Colors.color0}
                    style={styles.radio_button}
                    labelStyle={{ fontSize: 13, color: '#333', marginTop: -3 }}
                    formHorizontal={false}
                    buttonSize={10}
                    buttonOuterSize={20}
                    buttonWrapStyle={{ marginLeft: 10, borderWidth: 1, marginBottom: 10, }}
                    onValueChange={(value, index) => radioChangeCoupon(value, index)}
                    onPress={(value, index) => radioChangeCoupon(value, index)}
                  />
                </View> : <Text style={[styles.no_item_found, global_styles.lato_regular]}></Text>
              }
            </View> 

          </View>
        </KeyboardAvoidingView>

      </ScrollView>   
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  input_cotainer: {
    flex: 1,
    paddingHorizontal: 0,
    paddingVertical: 0,
    marginLeft:4,
    height: 40, 
    color: "#FFF",
    borderColor: '#f5f5f5', 
    backgroundColor: "#FFF",
    fontSize: 14,
  },
  input: {
    flex: 1,
    paddingHorizontal: 0,
    paddingVertical: 2,
    borderColor: '#f5f5f5',
    fontSize: 14,
    borderBottomWidth: 0,
    color: "#000",
    backgroundColor: "#fff"
  },
  button_stripe: {
    fontSize: 14,
    marginRight: 5,
    marginTop: 1,
    position: "absolute",
    left: 0,
  },
  radio_button: {
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "center",
  },
  no_item_found: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    fontSize: 12
  },   
  payment_method_title_wrap: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 10,
    backgroundColor: '#EEE'
  }, 
  payment_method_title: {
    fontSize: 12
  },   
  add_payment_coupon: {
    marginVertical: 10,
    marginHorizontal: 10,
    borderBottomWidth: 1,
    borderColor: "#f5f5f5",
    flex: 1,
    flexDirection: "row"
  }, 
  c_line_item: {
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginVertical: 5,
    marginHorizontal: 10,
    paddingBottom: 0,
    marginTop: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    width: "94%"
  },    
  textbuttonSubmit_Wrapper: {
    flex: 1,
    flexDirection: "row-reverse",  
  },
  textbuttonAddPaymentMethod: {
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderWidth: 1,
    borderColor: Colors.color0,
    borderRadius: 5,
  },
  textbuttonSubmit: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 10,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  textbuttonCancel: {
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  }, 
  buttonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  buttonTextCancel: {
    color: Colors.cancelText,
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
});