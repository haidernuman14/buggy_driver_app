import React, { useState, useEffect } from 'react';
import { StyleSheet, ScrollView, TextInput, Keyboard,Platform, Text, ActivityIndicator, View, Button, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import { CreditCardInput } from 'react-native-credit-card-input';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'; 
import RNPickerSelect from 'react-native-picker-select';
import Colors from '../../constants/Colors';
import { global_styles,modal_style,pickerSelectStyles } from '../../constants/Style';
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Renders the payment form and handles the credit card data
 * using the CreditCardInput component.
 */
export default function PaymentFormView(props) { 

 /* constructor(props) {
    super(props);
    this.state = { cardData: { valid: false } };
  } */

  const [state, setState] = useState({
    cardData: { valid: false},
    state_list:[],
    address: '',
    city:'',
    driverstate:'',
    zip:'',
    name:'',
    address2:'',
    errors: {
      error_address: '',
      error_address2: '',
      error_city: '',
      error_state: '',
      error_name: '',
      error_zip: ''
    }
  });

  
  let statelist = [ 
    { value: 0, label: "Select State" },
    { value:'AL',label:'Alabama'},
    { value:'AK',label:'Alaska'},
    { value:'AZ',label:'Arizona'},
    { value:'AR',label:'Arkansas'},
    { value:'CA',label:'California'},
    { value:'CO',label:'Colorado'},
    { value:'CT',label:'Connecticut'},
    { value:'DE',label:'Delaware'},
    { value:'DC',label:'District Of Columbia'},
    { value:'FL',label:'Florida'},
    { value:'GA',label:'Georgia'},
    { value:'HI',label:'Hawaii'},
    { value:'ID',label:'Idaho'},
    { value:'IL',label:'Illinois'},
    { value:'IN',label:'Indiana'},
    { value:'IA',label:'Iowa'},
    { value:'KS',label:'Kansas'},
    { value:'KY',label:'Kentucky'},
    { value:'LA',label:'Louisiana'},
    { value:'ME',label:'Maine'},
    { value:'MD',label:'Maryland'},
    { value:'MA',label:'Massachusetts'},
    { value:'MI',label:'Michigan'},
    { value:'MN',label:'Minnesota'},
    { value:'MS',label:'Mississippi'},
    { value:'MO',label:'Missouri'},
    { value:'MT',label:'Montana'},
    { value:'NE',label:'Nebraska'},
    { value:'NV',label:'Nevada'},
    { value:'NH',label:'New Hampshire'},
    { value:'NJ',label:'New Jersey'},
    { value:'NM',label:'New Mexico'},
    { value:'NY',label:'New York'},
    { value:'NC',label:'North Carolina'},
    { value:'ND',label:'North Dakota'},
    { value:'OH',label:'Ohio'},
    { value:'OK',label:'Oklahoma'},
    { value:'OR',label:'Oregon'},
    { value:'PA',label:'Pennsylvania'},
    { value:'RI',label:'Rhode Island'},
    { value:'SC',label:'South Carolina'},
    { value:'SD',label:'South Dakota'},
    { value:'TN',label:'Tennessee'},
    { value:'TX',label:'Texas'},
    { value:'UT',label:'Utah'},
    { value:'VT',label:'Vermont'},
    { value:'VA',label:'Virginia'},
    { value:'WA',label:'Washington'},
    { value:'WV',label:'West Virginia'},
    { value:'WI',label:'Wisconsin'},
    { value:'WY',label:'Wyoming'}, 
  ];

    const { cardData, name, address2, state_list, address, city, driverstate, zip, errors } = state;
    const { onSubmit, cancelAddCard, isLoading, error } = props; 

    const updateTextInput = (value, field) => {
      setState({ ...state, [field]: value });
    }
      
    const setStateUpdate = (val) => {
      setState({ ...state, driverstate: val });
    }

    const validate_info = async () => {
      // console.log("cardData.valid",cardData.valid);
      if(cardData.valid){  

          let error = { ...state, errors };
          let flag_val = 0;

          if (name == '' || name == '--') {
            error.error_name = "Please enter name on card.";
            flag_val = 1;
          }  else {
            error.error_name = "";
          } 

       /*   if (address == '' || address == '--') {
            error.error_address = "Please enter your Address.";
            flag_val = 1;
          } else if (address.length > 50) {
            error.error_address = "Please enter valid Address.";
            flag_val = 1;
          } else {
            error.error_address = "";
          } 
  

          if (city == '' || city == '--') {
            error.error_city = "Please enter your City.";
            flag_val = 1;
          } else if (city.length > 15) {
            error.error_city = "Please enter valid City.";
            flag_val = 1;
          } else {
            error.error_city = "";
          } 

          if (driverstate == '' || driverstate == '--') {
            error.error_state = "Please select your State.";
            flag_val = 1;
          } else {
            error.error_state = "";
          } */
      
          const expressioZip = /^(?:\d{5})$/;
          if (zip == '') {
            error.error_zip = "Please enter your Zip Code.";
            flag_val = 1;
          } else if (expressioZip.test(zip) === false) {
            error.error_zip =  "Please enter valid Zip Code.";
            flag_val = 1;
          } else {
            error.error_zip = "";
          }

          if (flag_val == 1) {
            // console.log("flag_val == 1");
            setState({ ...state,  errors: error }); 
            return;
          } else {
            // console.log("cardData ==",cardData);
            onSubmit(cardData,{address:address, address2:address2, city:city, zip:zip, state:driverstate},props.cardTokenList);
          }
        } else {
          //console.log("isvalid ==",isvalid);
          onSubmit({isvalid:false},{address:"", address2:"", city:"", zip:"", state:""},props.cardTokenList);
        }
    }
    let platformStyle = Platform.OS === 'ios' ? styles.iosStyle : styles.androidStyle;

    const CancelPaymentRedirect = async () => {
      let update_card_1 = await AsyncStorage.getItem('update_card');
      if( update_card_1 && update_card_1 == "yes" ){
        await AsyncStorage.setItem('update_card', '');
        props.navigation.navigate('Billing');
      } else {
        props.navigation.navigate('MyAccount');
      }
    }

    return (
      <>
        <ScrollView keyboardShouldPersistTaps="always" style={{ flex: 1 }}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View style={{ flex: 1 }}>
               <View style={styles.info_main}>

                  <View style={styles.edit_basic_main}>
                  {error && (
                    <View style={styles.alertWrapper}>
                      <View style={styles.alertTextWrapper}>
                        <Text style={styles.alertText}>{error}</Text>
                      </View>
                    </View>
                  )}
                  
                   <CreditCardInput placeholders={{ number: "1234 5678 1234 5678", expiry: "MM/YY", cvc: "CVV" }} labels={{ number: "CARD NUMBER", expiry: "EXPIRATION", cvc: "CVV" }} inputStyle={[global_styles.textInput,{marginBottom:10}]}  labelStyle={[global_styles.labelStyle, global_styles.lato_regular]}  onChange={(cardData) => setState({ ...state, cardData:cardData })} />
 
                    <Text style={[global_styles.labelStyle, global_styles.lato_regular]}>NAME ON CARD</Text> 
                    <View style={styles.profile_field_value_wrapper} > 
                       <TextInput
                           style={[global_styles.textInput,{height: (Platform.OS === "ios")?40:42, flex:1, alignItems:'stretch',}, global_styles.lato_regular]}
                            placeholder={"Name On Card"}  
                            onChangeText={(text) => updateTextInput(text, 'name')}
                        /> 
                    </View>      
                    <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_name}</Text>

                      <Text style={[global_styles.labelStyle, global_styles.lato_regular]}>BILLING ZIP CODE</Text> 
                      <View style={styles.profile_field_value_wrapper} >
                             <TextInput
                                style={[global_styles.textInput,{height: (Platform.OS === "ios")?40:42, flex:1, alignItems:'stretch',}, global_styles.lato_regular]}
                                placeholder={"Billing Zip Code"}  
                                onChangeText={(text) => updateTextInput(text, 'zip')}
                            />    
                      </View>    
                      <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_zip}</Text>
                  </View>
              </View>      

              <View style={global_styles.button_container}>
                  <TouchableOpacity onPress={() => CancelPaymentRedirect()}> 
                      <View style={global_styles.button_style_white_wrapper}>
                          <Text style={global_styles.button_style_white_text}>CANCEL</Text>
                      </View>
                  </TouchableOpacity>
                  <View style={{ marginStart: 16 }} />
                  <TouchableOpacity onPress={() => { validate_info(cardData) }}>
                      <View style={global_styles.bottom_style_orange_wrapper}>
                          <Text  style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>ADD CARD</Text>
                      </View>
                  </TouchableOpacity>
              </View>       
                
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </>
    );
   
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  error_label: {
    color: 'red',
    fontSize: 12,  
  },
  save_container: {
      width: 140,
      backgroundColor: '#db9360',
      padding: 8,
      borderWidth: 1,
      borderRadius: 6,
      alignItems: "center",
      marginStart: 16,
      borderColor: '#db9360'
  },
  cancel_container: {
      width: 140,
      backgroundColor: '#dedbdb',
      padding: 8,
      borderWidth: 1,
      borderRadius: 6,
      alignItems: "center",
      borderColor: '#dedbdb'
  },
  button_container: {
      flexDirection: 'row', 
      marginTop:10,
  },
  save_text: {
    color: '#FFFFFF',
    fontWeight: 'bold'
  },
  alertIconWrapper: {
    padding: 8,
    flex: 0.1,
  },
  info_main: {
    flex: 1,
    paddingTop: 15,
    paddingHorizontal: 0,
  },
  section_title: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 10,
    backgroundColor: '#EEE'
  },
  error_msg: {
    color: "red",
    fontSize: 12, 
    paddingVertical: 0,
    paddingTop: 0
  },
  section_title_license: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 10,
    backgroundColor: '#EEE'
  },
  section_title_payment: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 10,
    backgroundColor: '#EEE'
  }, 
  section_title_text: {
    fontSize: 12, 
  },
  edit_basic_main: {
    paddingVertical: 0, 
    marginHorizontal: 0, 
  },
  personal_detail_field_label: {
    color: 'black',
    fontSize: 12, 
  },
  profile_field_value_wrapper: {
    paddingHorizontal: 0, 
    marginBottom: 0,
    paddingTop: 0,
  },
  input: {
    flex: 1,
    paddingHorizontal: 0,
  },
  input_cotainer: { 
      marginTop: 4,
      borderWidth: 1,
      borderRadius: 6,
      fontSize: 14,
      paddingHorizontal: 12, 
      borderColor: '#efefed',
      marginRight:10,
      color: '#242424',
      marginBottom:10,
      backgroundColor: '#ffffff' 
  }, 
  input_cotainer_out_input: { 
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 6,
    fontSize: 14,
    paddingHorizontal: 12, 
    paddingVertical: 5,
    borderColor: '#efefed',
    marginRight:10,
    color: '#242424',
    marginBottom:10,
    backgroundColor: '#ffffff' 
  }, 
  profile_field_left: {
    flex: 1,
    flexDirection: "row",
    marginHorizontal: 0,
    marginBottom: 10
  },
  profile_field_1: {
    flex: 0.5,
    alignSelf: "flex-start",
    marginRight: 5,
  },
  profile_field_2: {
    flex: 0.5,
    alignSelf: "flex-start",
    marginRight: 5,
  },
  alertText: {
    color: '#c22',
    fontSize: 12,
    fontWeight: '400'
  },
  alertWrapper: {
    backgroundColor: '#ecb7b7',
    borderRadius: 5,
    paddingVertical: 8,
    paddingHorizontal: 14, 
    marginBottom: 10,
    marginHorizontal: 0
  },
  buttonView: {
    marginVertical: 30,
    flexDirection: "row-reverse",
    paddingHorizontal: 10,
  },
  checkoutButton: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    width: 130,
    alignSelf: "center",
    borderRadius: 5,
  },
  buttonActivityIndicator: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    paddingLeft:60,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    width: 50,
    alignSelf: "flex-end",
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    width: 80, 
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    color: Colors.color0,
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  iosStyle: {
    flex:1,
    marginLeft:0,
    marginTop:0,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  androidStyle: {
    flex:1,
    flexDirection:"row",
    marginLeft:-15,
    marginTop:-5,
    paddingVertical: 10,
    paddingHorizontal: 10,
  }
});
