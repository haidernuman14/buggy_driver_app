import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";
import { global_styles } from '../../constants/Style';
import { useQuery } from '@apollo/react-hooks';
import { CheckConnectivity } from '../Helper/NetInfo';
import TransactionHistory from './transactionHistory';

export default function TransactionHistoryScreen(props) {

  const [isLoading, setIsLoading] = useState(false);
  const [driverId, setDriverId] = useState('');
  const [reload, setReload] = useState(false);

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }
  
  useEffect(() => {
    checkNet();
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
    
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
     
      setuserid();
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const setuserid = async () => {
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setDriverId("");
    setDriverId(user.id);
    setReload(!reload);
  }
  
  return (
    <>
      {isLoading ?
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> :
        <ScrollView style={{marginBottom:72}}>      
          <SafeAreaView key={reload} style={styles.container}>     
          
           {driverId!=''?<TransactionHistory {...props} driverId={driverId} />:null}
           
        </SafeAreaView></ScrollView>}
    </>
  );
}
const styles = StyleSheet.create({
  container:{
    flex:1,   
  }
});