import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, TouchableHighlight } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import Modal from "react-native-modal";
import { translate } from '../../components/Language';
import { global_styles, modal_style } from '../../constants/Style';
import Colors from '../../constants/Colors';
import Icon2 from 'react-native-vector-icons/AntDesign';

/**
 * Utility class to check internet connection.
 */
export const CheckConnectivity = async () => {
    const data = await NetInfo.fetch();
    return data.isConnected;
};

// UI to display alert when internet connection is not available.
export default function NetChecker(props) {
    return (
        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={props.isModal} >
            <View style={modal_style.modal}>
                 <View style={modal_style.modal_wrapper}>
                    <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => props.setModal(false)}>
                        <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                    </TouchableOpacity>
                    <Icon2 name="exclamationcircleo" style={[modal_style.modal_check, modal_style.modal_check_red]} />
                    <View>
                        <Text style={modal_style.modalText}>{translate("app_is_now_Offline")}</Text>
                        <View style={modal_style.modal_textbuttonWrap_center}>
                            <TouchableHighlight  style={modal_style.upload_document_modal} onPress={() => props.setModal(false)}>
                                <Text style={modal_style.upload_document_text_modal}>{translate('ok')}</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            </View>
        </Modal>
    );
}; 