import { useEffect } from 'react';

/** This react native life cycle class used to check the app state and based on that actions may take place. */

export default function useEffectAsync(effect, inputs) {
    useEffect(() => {
        effect();
    }, inputs);
}