import React from 'react';
import {
  ActivityIndicator,
  Alert,
  StatusBar,
  Platform,
  StyleSheet,
  View,
} from 'react-native';
import { global_styles } from '../../constants/Style';
import AsyncStorage from '@react-native-community/async-storage';
import { check, request, PERMISSIONS, RESULTS, checkNotifications, requestNotifications, openSettings } from 'react-native-permissions';
import { CheckConnectivity } from '../Helper/NetInfo';
import PusherNotification from './PusherNotification';

/* 
This screen checks for the authentication.
If user has logged in, it redirects to Dashboard screen. Else, Login screen.

It also checks for the Notification and Location services permissions.
Asks user if permissions are not allowed or the app starts for the first time.
*/

class AuthLoadingScreen extends React.Component {

  async componentDidMount() {
    const isLogin = await AsyncStorage.getItem('isLogin');
    if (Platform.OS === 'ios' && isLogin != true) {
      // Check notification permission for iOS device. Ask for notification permission if not allowed.
      await checkNotifications().then(({ status, settings }) => {
        
        switch (status) {
          case RESULTS.UNAVAILABLE:
            this.requestNotificationsFun().then(statuses => console.log('u', statuses));
            break;
          case RESULTS.DENIED:
            this.requestNotificationsFun().then(statuses => console.log('d', statuses));
            break;
          case RESULTS.GRANTED:
           
            break;
          case RESULTS.BLOCKED:
            this.requestNotificationsFun().then(statuses => console.log('b', statuses));
            break;
        }
     
      });
    }
    if (isLogin != true) {
      // Check location permission. Ask for location permission if not allowed.
      // await this.checkLocation()
    }
    this._bootstrapAsync(isLogin);
  }

  checkLocation = () => {
    this.checkAll().then(result => {
        console.log(result)
      switch (result.locationStatus) {
        case RESULTS.UNAVAILABLE:
          this.requestAll().then(statuses => {
          
            switch (statuses) {
              case RESULTS.DENIED:
                this.openSettingsFun()
                break;
              case RESULTS.GRANTED:
             
                break;
              case RESULTS.BLOCKED:
                this.openSettingsFun()
                break;
              default:
                this.openSettingsFun()
                break;
            }
          });
          break;
        case RESULTS.DENIED:
          this.requestAll().then(statuses => {
            switch (statuses.locationStatus) {
              case RESULTS.DENIED:
                this.openSettingsFun()
                break;
              case RESULTS.GRANTED:
                break;
              case RESULTS.BLOCKED:
                this.openSettingsFun()
                break;
              default:
                this.openSettingsFun()
                break;
            }
          });
          break;
        case RESULTS.GRANTED:
          break;
        case RESULTS.BLOCKED:
          this.openSettingsFun()
          break;
        default:
          this.openSettingsFun()
          break;
      }
    })
      .catch(error => {
   
      });
  }
  openSettingsFun = () => {
    Alert.alert(
      'Location Permission',
      "App needs access to your phone's location.",
      [
        {
          text: 'Cancel',
          onPress: () => {
         
          },
          style: 'cancel',
        },
        { text: 'Ok', onPress: () => openSettings().catch(() => console.warn('cannot open settings')) },
      ],
    );
  }

  checkAll = async () => {
    const locationStatus = await check(Platform.select({
      android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
    }));
    return { locationStatus };
  }

  requestNotificationsFun = () => {
    return requestNotifications(['alert', 'sound'])
  }

  requestAll = async () => {
    const locationStatus = await request(Platform.select({
      android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
    }));
    return { locationStatus };
  }


  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async (isLogin) => {
    let netState = await CheckConnectivity();
    if (!netState) {
      this.props.screenProps.setNetModal(true)
      return;
    }

      
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(isLogin ? 'App' : 'SignIn');
  };

  redirect_to_notification_list = (flag) => {
      // console.log(flag)
  // this.props.navigation.navigate('Chat');
  }

  // Render any loading content that you like here
  render() {
    return (
      <View style={global_styles.activityIndicatorView, { backgroundColor: "#FFFFFF" }}>
        <PusherNotification toNotification={this.redirect_to_notification_list} props={this.props} navigation={this.props.navigation} />
        <ActivityIndicator size="large" style={{ padding: 60 }} />
        {Platform.OS === 'ios' ? <StatusBar barStyle="default" /> :
          <StatusBar
            barStyle={"dark-content"}
            // dark-content, light-content and default
            hidden={false}
            //To hide statusBar
            backgroundColor={"#f9f9f4"}
            //Background color of statusBar only works for Android
            translucent={false}
            //allowing light, but not detailed shapes
            networkActivityIndicatorVisible={true}
          />}
      </View>
    );
  }
}
export default AuthLoadingScreen;
