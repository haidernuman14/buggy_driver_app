import React, { useState, useRef, useEffect } from 'react';
import { CheckConnectivity } from '../Helper/NetInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { getPusherToken,Alert } from '../../graphql/getPusherToken';
import { useMutation } from '@apollo/react-hooks';
import RNPusherPushNotifications from 'react-native-pusher-push-notifications';
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from '@react-native-community/push-notification-ios';

import { Platform } from 'react-native';
import { not } from 'react-native-reanimated';


export default function PusherNotification(props) {
  const [driverId, setDriverId] = useState("");
  const [AgetPusherToken] = useMutation(getPusherToken);


  useEffect(() => {

    if (driverId == "")
      getUser();
    else
      getData();

  }, [driverId]);

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }
     ///......go to chatScreen ....//
     function navigationChat(){
      
      props.navigation.navigate('ChatDetail')
     }

//      useEffect(() => {

//      PushNotification.configure({
//   // (optional) Called when Token is generated (iOS and Android)
//   onRegister: function (token) {
//     console.log("TOKEN:", token);
//   },

//   // (required) Called when a remote is received or opened, or local notification is opened
//   onNotification: function (notification) {
//     console.log("NOTIFICATION:", notification);

//     // process the notification

//     // (required) Called when a remote is received or opened, or local notification is opened
//     notification.finish(PushNotificationIOS.FetchResult.NoData);
//   },

//   // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
//   onAction: function (notification) {
//     console.log("ACTION:", notification.action);
//     console.log("NOTIFICATION:", notification);

//     // process the action
//   },

//   // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
//   onRegistrationError: function(err) {
//     console.error(err.message, err);
//   },

//   // IOS ONLY (optional): default: all - Permissions to register.
//   permissions: {
//     alert: true,
//     badge: true,
//     sound: true,
//   },

//   // Should the initial notification be popped automatically
//   // default: true
//   popInitialNotification: true,

//   /**
//    * (optional) default: true
//    * - Specified if permissions (ios) and token (android and ios) will requested or not,
//    * - if not, you must call PushNotificationsHandler.requestPermissions() later
//    * - if you are not using remote notification or do not have Firebase installed, use this:
//    *     requestPermissions: Platform.OS === 'ios'
//    */
//   requestPermissions: true,
// })})

  const  sendMessage=(body,title)=>{
      PushNotification.localNotification({
        /* Android Only Properties */
        title:title, // (optional)
        message:body, // (required)
        invokeApp: true,
       
      });
    }
  const getUser = async () => {
    const user = await AsyncStorage.getItem('user');
    if (user)
      setDriverId(JSON.parse(user).id);
  }


  const getData = async () => {

    let input = {
      driverId: driverId
    }

    AgetPusherToken({
      variables: { input }
    }).then((response) => {
      //console.log("token", response);
      if (response.data.getPusherToken.ok) {
        initializePusher(response.data.getPusherToken.token);
      }
    });

  }

  


  useEffect(() => {
    PushNotification.configure({
    onNotification: function(notification) {
          if(Platform.OS==="android"){
            if(notification.userInteraction){
              navigationChat()
            }
            
          }
          else if(Platform.OS==="ios")
          {
         const { data } = notification;
         if(data.actionIdentifier){
          navigationChat()
        }
      }
    },
    onAction: function (notification) {
    
  
      // process the action
    }
  
});
  
  });

  


  const interests = [
    'debug-test',
    'debug-secondtest',
  ]

  function onPusherInitError(statusCode, response) {
 
  }

  function onPusherInitSuccess(response) {
   
  }

  async function initializePusher(token) {
    try {

      RNPusherPushNotifications.setInstanceId('d56f5e11-ae8d-49d6-b32d-de18db7d8f60');

      // Init interests after registration
      RNPusherPushNotifications.on('registered', () => {
        // This subscribes to general interests. Notifications triggered using
        // instances will be used for all users, not targeting a specific user.
        interests.forEach((interest) => {
          subscribe(interest);
          // console.log("Interest: ", interest)
        });
        // Uncomment below interest for testing
        // subscribe("hello")

        // setUser takes our logged in user's `id` attribute and appends `user-` as our
        // backend is setup with Pusher to setUsers using this naming convention (ie. user-214 would
        // be an example user for this function)
    
        setUser(`user-${driverId}`, token, onPusherInitError, onPusherInitSuccess)
      });

      // Setup notification listeners
      // RNPusherPushNotifications.on('notification', handleNotification);
      RNPusherPushNotifications.on('notification', (notification) => {
        sendMessage(notification.body,notification.title)
        handleNotification({ notification })
      });
    } catch (e) {
      console.log('error getting pusher token: ', e)
    }
  }

  

  function subscribe(interest) {
    // console.log('subscribing to interest... ', interest);
    // Note that only Android devices will respond to success/error callbacks
    RNPusherPushNotifications.subscribe(
      interest,
      (statusCode, response) => {
      
      },
      () => {
    
      }
    );
  }

  //   openNotification=(notification)=>{
  //       console.log
  //   PushNotification.configure({
  //     onNotification: function(notification) {
  //         const { data } = notification;
  
    
  //     }
  // });
  // }

  function setUser(userId, token, onError, onSuccess) {
    // Note that only Android devices will respond to success/error callbacks
    RNPusherPushNotifications.setUserId(
      userId,
      token,
      (statusCode, response) => {
        onError(statusCode, response);
      },
      () => {
        onSuccess('Set User ID Success');
      }
    );
  }

  function handleNotification({ notification }) {
    // Handle notifications received while app is open
    const alert = Platform.OS === 'ios'
      ? notification.userInfo.aps.alert
      : notification;

    // console.log('alert: ', alert);

    // iOS app specific handling
    if (Platform.OS === 'ios') {
      switch (notification.appState) {
        case 'inactive':
          // onPress(notification);
          // inactive: App came in foreground by clicking on notification.
          //           Use notification.userInfo for redirecting to specific view controller
          props.toNotification("inactive");
          break;
        case 'background':
             
          // background: App is in background and notification is received.
          //             You can fetch required data here don't do anything with UI
          props.toNotification("background");
          break;
        case 'active':
          
          // App is foreground and notification is received. Show an alert or something.
          props.toNotification("active");
          break;
        default:
          break;
      }
    }

    props.toNotification("android");

  };

  return null;
}
