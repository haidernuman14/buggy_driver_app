import React, { useState,useEffect } from 'react';
import { TouchableHighlight, Text, Keyboard } from 'react-native';
import { global_styles } from '../../constants/Style'; 
import BellIcon from 'react-native-vector-icons/EvilIcons'; 
import BackIcon from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../constants/Colors';
import { Badge } from 'react-native-elements'
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/AntDesign'; 


import AsyncStorage from '@react-native-community/async-storage'; 
import { driverNotificationCounts } from '../../graphql/driverNotificationCounts';
import { useMutation, useQuery } from '@apollo/react-hooks';
import moment from 'moment';
import { from } from 'apollo-boost';
const Notification_icon = (props) => { 

  const navigation = props.navigation; 
  const [notificationcount, setNotificationCount] = useState(0); 

  const [state, setState] = useState({  
    driverId:"", 
  });
  const {driverId} = state;

  useEffect(() => {
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      details();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => { 
       details();
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const { data: drivar_notification, loading: drivar_data_loading, error: drivar_data_error } = useQuery(driverNotificationCounts, {
    variables: { id: driverId },
    fetchPolicy: "network-only",
    skip: (driverId == '')
  });

  const details = async () => { 
    setState({...state,driverId:"" }); 
    const notificationids = await AsyncStorage.getItem('nids');
    let notificationlist = JSON.parse(notificationids); 
    let unread = 0;
    if( notificationlist ){
      notificationlist.map((dataitem,key) => { 
        if(!dataitem.isRead){ 
          unread = unread + 1;
        }  
      });
    } 
   
    if( unread > 9 ){
      unread = "9+";
    }
    setNotificationCount(unread); 

    const user = await AsyncStorage.getItem('user'); 
    if(user) {
      setState({...state, driverId:JSON.parse(user).id });
    }

  } 
  useEffect(() => { 
    if (drivar_notification) {
 
      if ( drivar_notification && drivar_notification.hasOwnProperty('driver')   && drivar_notification.driver.hasOwnProperty('unreadNotificationsCount') ) {
          
            let nt_list = [];
            let unread = drivar_notification.driver.unreadNotificationsCount; 
            if( unread > 9 ){
              unread = "9+";
            }
            setNotificationCount(unread);
       
            setState({...state,driverId:"" });  

      }

    }
  }, [drivar_notification]);


  return ( 
    <>
    <TouchableHighlight underlayColor=''  onPress={() => navigation.navigate('NotificationList',{"backitem":navigation.state.routeName})} >
        <><BellIcon
          style={global_styles.header_bell_icon} 
          name="bell"
          size={30}
        />
       {(notificationcount > 0 || notificationcount =="9+") && <Badge
          status="error"
          value={notificationcount}
          textStyle={global_styles.header_notificationText}
          containerStyle={global_styles.header_notificationContainer}
        />
        }
        </>
      </TouchableHighlight> 
    </>
  );   
} 

const filter_icon = (navigation) => {
  return ( <TouchableHighlight style={{ marginRight: 0, marginVertical: 5 }} underlayColor='' onPress={() => navigation.navigate('CarFilter')}>
          <Icon2 name='filter' size={25} style={{ color: Colors.textcolor }} />
        </TouchableHighlight>  );   
} 

const goback_icon_press = async (navigation,page1) => {
  
  let page = page1;
  let FAQId_Back_Accident =  await AsyncStorage.getItem('FAQId_Back_Accident'); 
  // console.log("FAQId_Back_Accident--",FAQId_Back_Accident);
  if( FAQId_Back_Accident && FAQId_Back_Accident == "yes" && page1=="Support" ){
      // console.log("FAQId_Back_Accident",FAQId_Back_Accident);
      await AsyncStorage.setItem('FAQId_Back_Accident', "");
      await AsyncStorage.setItem('FAQId', '1');
      navigation.navigate('FAQDetail');
      return;
  } 

  if( page == "" ) {
    navigation.goBack(null);
  } else {
    ((page != "")?navigation.navigate(page):((navigation.state.params && navigation.state.params.backitem)?navigation.navigate(navigation.state.params.backitem):navigation.goBack(null)));
  }
}


const goback_icon = (navigation,page) => {
     return ( <TouchableHighlight underlayColor='' onPress={() => goback_icon_press(navigation,page)} >
        <BackIcon
          style={{ paddingLeft: 20, color: Colors.textcolor }}
          name="arrow-back"
          size={30}
        />
      </TouchableHighlight> );   
}  

const drawer_icon = (navigation) => {
  return null;
  return (  <TouchableHighlight underlayColor='' onPressIn={Keyboard.dismiss } onPress={() => navigation.openDrawer()}>
          <Icon
            style={{ paddingLeft: 15, color: Colors.textcolor }} 
            name="md-menu"
            size={30}
          />
        </TouchableHighlight> );      
} 

export const HeaderLeftIcon = (navigation) => {
  return drawer_icon(navigation); 
}
export const HeaderLeftGoBackIcon = (navigation) => {
  return goback_icon(navigation,"");
} 
export const HeaderLeftGoBackOnPage = (navigation,page) => {
  return goback_icon(navigation,page);
} 

export const HeaderLeftGoBackOnPaymentPage = (navigation,page) => {
 
  return goback_icon_payment(navigation,page);
} 



const goback_icon_press_payment = async (navigation,page1) => {

  let page = page1;
  let update_card_1 =  await AsyncStorage.getItem('update_card'); 
  if( update_card_1 && update_card_1 == "yes" ){
     await AsyncStorage.setItem('update_card', '');
      page = "Billing";
  } else {
    page = "MyAccount"; 
  }
  // console.log("update_card_1---",update_card_1,page);


  if( page == "" ) {
    navigation.goBack(null);
  } else {
    ((page != "")?navigation.navigate(page):((navigation.state.params && navigation.state.params.backitem)?navigation.navigate(navigation.state.params.backitem):navigation.goBack(null)));
  }
}
const goback_icon_payment = (navigation,page) => {
    return ( <TouchableHighlight underlayColor='' onPress={() => goback_icon_press_payment(navigation,page)} >
        <BackIcon
          style={{ paddingLeft: 20, color: Colors.textcolor }}
          name="arrow-back"
          size={30}
        />
      </TouchableHighlight> );   
}  


export const HeaderRightIcon = (navigation) => {
  return <Notification_icon navigation={navigation} />;   
}

export const ScreenTitle = (title) => {
  return <Text style={[global_styles.lato_medium, { fontSize: 17, paddingLeft: 10, color: '#393e5c', }]}>{title}</Text>;   
}

export const HeaderRightCarListIcon = (navigation) => {
  return (<>
    {filter_icon(navigation)} 
    <Notification_icon navigation={navigation} />
  </>);   
}