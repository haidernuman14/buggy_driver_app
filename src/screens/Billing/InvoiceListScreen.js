import React, { useState, useEffect, useContext } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    Image,
    ActivityIndicator,
    View,
    Keyboard,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Dimensions,
    TouchableOpacity,
    Platform
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import moment from "moment";
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { CheckConnectivity } from '../Helper/NetInfo';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { allInvoicesWithGroups } from '../../graphql/allInvoicesWithGroups'
import { global_styles, modal_style } from '../../constants/Style';
import AsyncStorage from '@react-native-community/async-storage';

const currentBill = 'Current Bill'
const paidBill = 'Paid'

export default function InvoiceListScreen(props) {

    const [state, setState] = useState({
        invoiceList: [],
        pageInfo: {},
    })

    const { invoiceList, pageInfo } = state

    const [user, setUser] = useState({});
    const [isLoadInvoice, setLoadInvoice] = useState(false)
    const [freshFlag, setFreshFlag] = useState(true)
    const [isNextPageLoading, setNextPageLoading] = useState(false);
    const { setStatusColor } = useContext(langContext);
    const [isAllPaid, setAllPaid] = useState(true)

    // Check internet connection
    const checkNet = async () => {
        let netState = await CheckConnectivity();
        if (!netState) {
            props.screenProps.setNetModal(true)
            return;
        }
    }

    const { data: invoices, loading: isInvoicesLoading, error: invoiceError, fetchMore } = useQuery(allInvoicesWithGroups, {
        variables: { driverId: user.id },
        fetchPolicy: "network-only",
        skip: isLoadInvoice == false
    });

    const handleMoreInvoices = (id) => {
        setFreshFlag(false)
        fetchMore({
            variables: {
                driverId: user.id,
                first: 7,
                after: id
            },
            updateQuery: (prev, { fetchMoreResult }) => {
                setNextPageLoading(false)
                return fetchMoreResult;
            }
        });
    }

    const loadMoreData = () => {
        if (pageInfo.hasNextPage != true || isNextPageLoading) {
            return
        }
        setNextPageLoading(true)
        handleMoreInvoices(pageInfo.endCursor)
    }

    useEffect(() => {
        setLoadInvoice(false)
        if (invoices && invoices.hasOwnProperty('allInvoices') && invoices.allInvoices.hasOwnProperty('edges')) {
            // console.log(invoices.allInvoices.edges)
            if (invoices.allInvoices.edges.length > 0) {
                let invoiceItems = []
                let cnt = 0
                let foundCurrentBill = false
                invoices.allInvoices.edges.map((item) => {
                    if (cnt >= 4)
                        return
                    cnt++
                    let isCurrentBill = false
                    let sd = moment(item.node.startDate).format('MMMM DD, YYYY');
                    let ed = moment(item.node.endDate).format('MMMM DD, YYYY');
                    let currentDate = moment().format('MMMM DD, YYYY');
                    if (moment(currentDate).valueOf() >= moment(sd).valueOf()
                        && moment(currentDate).valueOf() <= moment(ed).valueOf()) {
                        isCurrentBill = true
                        foundCurrentBill = true
                        if (item.node.totalBalance > 0) {
                            setAllPaid(false)
                        }
                    }
                    let amount = isCurrentBill ? item.node.totalBalance : item.node.invoiceItemsTotal
                    let data = {
                        id: item.node.id,
                        startDate: sd,
                        endDate: ed,
                        totalBalance: item.node.totalBalance,
                        totalAmount: amount >= 0 ? '$' + amount.toFixed(2) : '-$' + (amount * -1).toFixed(2),
                        chargeType: isCurrentBill ? currentBill : paidBill,
                        data: item.node
                    }
                    invoiceItems.push(data)
                })
                invoiceItems.sort((a, b) => new Date(a.date) - new Date(b.date))
                if (!foundCurrentBill && invoiceItems.length > 0) {
                    if (invoiceItems[0].totalBalance > 0) {
                        invoiceItems[0].chargeType = currentBill
                        invoiceItems[0].totalAmount = '$' + invoiceItems[0].totalBalance.toFixed(2)
                        setAllPaid(false)
                    }
                }
                if (freshFlag) {
                    setState({ ...state, invoiceList: invoiceItems, pageInfo: invoices.allInvoices.pageInfo })
                    // setInvoiceList(invoiceItems)
                } else {
                    setState({ ...state, invoiceList: [...invoiceList, invoiceItems], pageInfo: invoices.allInvoices.pageInfo })
                }
                // console.log('Invoice items...', invoiceItems)
            }
        }
        // console.log(invoices)
    }, [invoices])

    const setuserid = async () => {
        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);
        setUser(user);
        setLoadInvoice(true)
    }

    useEffect(() => {

        checkNet();

        setStatusColor(Colors.account_status_bar_color);
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            setuserid();
        }

        const navFocusListener = props.navigation.addListener('didFocus', () => {
            setuserid();
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);

    const showInvoice = async (index) => {
        AsyncStorage.setItem('SelectedInvoice', (index).toString())
        AsyncStorage.setItem('Invoices', JSON.stringify(invoiceList))
        AsyncStorage.setItem('Route', 'InvoiceList')
        props.navigation.navigate('BillingInvoice')
    }

    return (
        <View style={styles.main_container}><>
            {isInvoicesLoading ?
                <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
                    <ActivityIndicator size="large" style={{ padding: 60 }} />
                </View> :
                <View style={styles.container}>
                    {!isAllPaid && <TouchableOpacity style={styles.pay_container} onPress={() => props.navigation.navigate('PaymentPaynow')}>
                        <View style={global_styles.bottom_style_orange_wrapper}>
                            <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>PAY NOW</Text>
                        </View>
                    </TouchableOpacity>}
                    <KeyboardAwareScrollView contentContainerStyle={(Platform.OS == "web") ? { flexGrow: 1, height: (win.height - 100) } : {}}>
                        {invoiceList != null && invoiceList.length > 0 &&
                            invoiceList.map((item, index) =>
                                (<TouchableOpacity key={index} onPress={() => showInvoice(index)}>
                                    <View style={styles.statement_list_item} >
                                        <View style={styles.statement_list_item_icon} >
                                            <Image width="50" style={styles.icon_input} source={item.chargeType == 'Current Bill' ? require("../../assets/images/list-icon-hover.png") : require("../../assets/images/list-icon.png")} />
                                        </View>
                                        <Text style={[styles.statement_list_item_text, global_styles.lato_semibold]}>{item.endDate + ' - ' + item.chargeType}</Text>
                                        <Text style={[styles.statement_list_item_price, global_styles.lato_semibold]}>{item.totalAmount}</Text>
                                    </View>
                                </TouchableOpacity>)
                            )
                        }
                        {pageInfo.hasNextPage == true &&
                            <View style={[global_styles.button_container, { justifyContent: "center", marginBottom: 20 }]}>
                                <TouchableOpacity onPress={loadMoreData}>
                                    <View style={[global_styles.button_style_white_wrapper, { borderColor: '#ccc' }]}>
                                        <Text style={[global_styles.button_style_white_text, { color: '#393e5c' }]}>SHOW MORE</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }
                    </KeyboardAwareScrollView></View>}
        </></View>
    )

}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#fff',
        flex: 1,
        margin: 16,
    },
    container: {
        flex: 1,
        flexDirection: 'column-reverse',
    },
    list_item_container: {
        backgroundColor: '#fff',
    },
    statement_list_item: {
        flexDirection: "row",
        marginVertical: 8,
        borderBottomColor: "#f5f5f5",
        borderBottomWidth: 1,
        paddingBottom: 10,
        marginTop: 5,
    },
    icon_input: {
        width: 25,
        height: 25,
    },
    statement_list_item_text: {
        marginLeft: 10,
        color: "#0B0B0C"
    },
    statement_list_item_price: {
        position: "absolute",
        color: "#0B0B0C",
        right: 0
    },
    pay_container: {
        alignSelf: 'flex-end',
        margin: 16,
    },
    pay_text: {
        color: '#FFFFFF',
        fontSize: 13,
    },
})

