import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  Image,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  TouchableOpacity,
  Platform
} from 'react-native';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import moment from "moment";
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { CheckConnectivity } from '../Helper/NetInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RNPickerSelect from 'react-native-picker-select';
import { getPaymentIds, getPaymentMethodNames } from '../../constants/PaymentIDs';
import ShowPaymentPopupScreen from './ShowPaymentPopupScreen';
import { driverUpdatePayment } from '../../graphql/driverUpdatePayment';
import { allInvoicesWithGroups } from '../../graphql/allInvoicesWithGroups'
import { allTransactions } from '../../graphql/allTransactions';
import AntDesign from 'react-native-vector-icons/AntDesign';

import Modal from "react-native-modal";
import StripeCardListScreen from '../Account/AccountCardListScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { from } from 'apollo-boost';
import RNFetchBlob from 'rn-fetch-blob';
import { getDataFromTree } from 'react-apollo';
/**
 * This class displays dashboard information.
 * Displays Pending Dues information with PAY NOW option.
 * Provides information of current rented cars under CURRENT RENTAL section. 
 * Provides history of all the rented cars under RENTAL HISTORY section.
 */
var receiptDataItem = []
const win = Dimensions.get('window');
const width = Dimensions.get('window').width

const currentBill = 'Current Bill'
const paidBill = 'Paid'

export default function BillingScreen(props) {

  const [DriverUpdatePayment] = useMutation(driverUpdatePayment);

  const [state, setState] = useState({
    isLoading: true,
    data: {},
    transitionData: {},
    user_id: "",
    fresh_flag: true,
    data_transaction_list: [],
    newPaymentMethod: "",
    radioPaymentTypeValue: "",
    zelleRemoteCachePopup: false,
    show_infoquestion_text: false,
    error_other_payment_method: "",
    currentPaymentMethodLabel: "",
    isModalPaymentMethodChange: false,
    currentPaymentMethodID: "",
    data_transaction_pageinfo: {},
  });
  const [isNextPageLoading, setNextPageLoading] = useState(false);

  const { isLoading, user_id, newPaymentMethod, isModalPaymentMethodChange, fresh_flag, radioPaymentTypeValue, zelleRemoteCachePopup, currentPaymentMethodID, data_transaction_list, data_transaction_pageinfo } = state;

  const [user, setUser] = useState({});
  const [driverId, setDriverId] = useState("");
  const { setStatusColor } = useContext(langContext);
  const [pk, setPk] = useState("");
  const [initReload, setInitReload] = useState(false);
  const [mainReload, setMainReload] = useState(true);
  const [initPaymentID, setInitPaymentID] = useState(0);
  const [initReloadDropdown, setInitReloadDropdown] = useState(false);
  const [isLoadInvoice, setLoadInvoice] = useState(false)
  const [isLoadReceipt, setLoadReceipt] = useState(false)
  const [invoiceList, setInvoiceList] = useState([])
  const [receiptList, setReceiptList] = useState([])
  const [pageInfo, setPageInfo] = useState({})
  const [isAllPaid, setAllPaid] = useState(true)
  const [cardListLabel, setCardListLabel] = useState('Credit Card on File')
  const [showNoInvoiceFound, setShowNoInvoiceFound] = useState(false)
  const [showNoReceiptFound, setShowNoReceiptFound] = useState(false)
  const [barcode_image, setBarcodeImage] = useState("")


  // Check internet connection
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  const { data: invoices, loading: isInvoicesLoading, error: invoiceError } = useQuery(allInvoicesWithGroups, {
    variables: { driverId: user.id },
    fetchPolicy: "network-only",
    skip: isLoadInvoice == false
  });
  const { data: data_transaction, loading: loding_transaction, error: error_transaction, refetch: refetchTransactions, fetchMore } = useQuery(allTransactions, {
    variables: {
      driverId: user.id,
      amount_Lt: 0,
      first: 2,
      orderBy: ["-due_date"]
    },
    fetchPolicy: "network-only",
    // skip: isLoadReceipt == false
  });


  useEffect(() => {
    setLoadInvoice(false)

    if (!(isInvoicesLoading || invoiceError))
      setState({ ...state, isLoading: false });

    if (invoices && invoices.hasOwnProperty('allInvoices') && invoices.allInvoices.hasOwnProperty('edges')) {
      if (invoices.allInvoices.edges.length > 0) {
        let invoiceItems = []
        let cnt = 0
        invoices.allInvoices.edges.map((item) => {
          if (cnt >= 4)
            return
          cnt++
          let isCurrentBill = false
          let sd = moment(item.node.startDate).format('MMMM DD, YYYY');
          let ed = moment(item.node.endDate).format('MMMM DD, YYYY');
          let currentDate = moment().format('MMMM DD, YYYY');

          if (moment(currentDate).valueOf() >= moment(sd).valueOf()
            && moment(currentDate).valueOf() <= moment(ed).valueOf()) {
            isCurrentBill = true
            if (item.node.totalBalance > 0) {
              setAllPaid(false)
            }
          }

          let amount = isCurrentBill ? item.node.totalBalance : item.node.invoiceItemsTotal
          let data = {
            id: item.node.id,
            startDate: sd,
            endDate: ed,
            previousBalance: item.node.previousBalance,
            totalAmount: amount <= 0 ? '$' + (amount * -1).toFixed(2) : '$' + amount.toFixed(2),
            chargeType: isCurrentBill ? currentBill : paidBill,
            data: item.node
          }

          if (isCurrentBill)
            invoiceItems.push(data)
        })

        setShowNoInvoiceFound(invoiceItems.length > 0 ? false : true)
        setInvoiceList(invoiceItems)
        setPageInfo(invoices.allInvoices.pageInfo)
      } else {
        setShowNoInvoiceFound(true)
        // setShowNoReceiptFound(true)
      }
    }
  }, [invoices])




  useEffect(() => {
    // setLoadReceipt(false)
    if (data_transaction && data_transaction.allTransactions.edges.length > 0) {
      if (fresh_flag) {
        refetchTransactions();
        setState({ ...state, isLoading: false, data_transaction_list: [data_transaction.allTransactions.edges], data_transaction_pageinfo: data_transaction.allTransactions.pageInfo })
        if (data_transaction && data_transaction.allTransactions && data_transaction.allTransactions.edges) {
          if (data_transaction.allTransactions.edges.length > 0) {
            receiptDataItem = []
            data_transaction.allTransactions.edges.map((item) => {
              let sd = moment(item.node.dueDate).format('MMMM DD, YYYY');
              let amount = item.node.amount ? item.node.amount : item.node.item.node.amount
              let receiptData = {
                id: item.node.id,
                dueDate: sd,
                chargeType: item.node.chargeTypeDisplay,
                paid: "Paid",
                totalAmount: amount <= 0 ? '$' + (amount * -1).toFixed(2) : '$' + amount.toFixed(2),
              }
              receiptDataItem.push(receiptData)
            })
            setReceiptList(receiptDataItem)
          }
        }

      }
      else {
        setState({ ...state, isLoading: false, data_transaction_list: [...data_transaction_list, ...data_transaction.allTransactions.edges], data_transaction_pageinfo: data_transaction.allTransactions.pageInfo })
        if (data_transaction && data_transaction.hasOwnProperty('allTransactions') && data_transaction.allTransactions.hasOwnProperty('edges')) {
          if (data_transaction.allTransactions.edges.length > 0) {
            data_transaction.allTransactions.edges.map((item) => {
              let sd = moment(item.node.dueDate).format('MMMM DD, YYYY');
              let amount = item.node.amount ? item.node.amount : item.node.item.node.amount
              let receiptData = {
                id: item.node.id,
                dueDate: sd,
                chargeType: item.node.chargeTypeDisplay,
                paid: "Paid",
                totalAmount: amount <= 0 ? '$' + (amount * -1).toFixed(2) : '$' + amount.toFixed(2),
              }
              receiptDataItem.push(receiptData)
            })
            setReceiptList(receiptDataItem)
          }
        }
      }

    }
    else if (data_transaction && data_transaction.allTransactions.edges.length == 0) {
      setState({ ...state, isLoading: false })
    }


    //setState({ ...state, isLoading:false, data_transaction_list: [], data_transaction_pageinfo: {} })

  }, [data_transaction, invoices])
  // Setting up user detail
  const setuserid = async () => {

    setInvoiceList([])
    setReceiptList([])
    setMainReload(true);
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setDriverId(user.id);
    setUser(user);
    let userinfo = user;
    let CurrentPaymentMethodID = "";
    let CurrentPaymentMethodLabel = "";

    if (userinfo.paymentMethod && userinfo.paymentMethod != "" && userinfo.paymentMethod != null) {
      if (userinfo.paymentMethod == "Card") {
        CurrentPaymentMethodID = getPaymentIds(props, "card");
        CurrentPaymentMethodLabel = "Card";
      }
      if (userinfo.paymentMethod == "Zelle") {
        CurrentPaymentMethodID = getPaymentIds(props, "zelle");
        CurrentPaymentMethodLabel = "Zelle";
      }
      if (userinfo.paymentMethod == "Remote Cash") {
        CurrentPaymentMethodID = getPaymentIds(props, "remotecash");
        CurrentPaymentMethodLabel = "Remote Cash";
      }
    }

    let current_payment_id = await AsyncStorage.getItem('current_payment_id');

    if (user) {
      setPk(user.pk);
    }

    setLoadInvoice(true);
    setLoadReceipt(true);


    setState({ ...state, user_id: user.id, zelleRemoteCachePopup: false, newPaymentMethod: CurrentPaymentMethodLabel, radioPaymentTypeValue: current_payment_id });
    setInitPaymentID(CurrentPaymentMethodID);

    let storage_val = await AsyncStorage.getItem('lastFlagBilling');
    if (storage_val) {
      setInitReload((storage_val == "yes") ? true : false);
      AsyncStorage.setItem('lastFlagBilling', (storage_val == "yes") ? "no" : "yes");
    } else {
      AsyncStorage.setItem('lastFlagBilling', "yes");
    }


    setMainReload(false);

    // Get barcode IMAGE
    let im_data = "";
    await RNFetchBlob.fetch('GET', 'https://www.achpsac.com/buggy/barcode/' + user.pk + '?C=B34*5')
      .then((res) => {
        let status = res.info().status;
        if (status == 200) {
          if (res.text().split('src="').length > 0) {
            let image_tg = res.text().split('src="')[1].split('\"');
            if (image_tg.length > 0) {
              let image_src = image_tg[0];
              im_data = image_src;
              setBarcodeImage(im_data);
            }
          }
        }
      }).catch((errorMessage, statusCode) => { });

  }
  const loadMoreData = () => {
    console.log("we all are herrrreeeeeeeeeee")
    if (data_transaction_pageinfo.hasNextPage != true || isNextPageLoading) {

      return
    }

    else {
      setNextPageLoading(true)
      handelMoreTransaction(data_transaction_pageinfo.endCursor)
    }

  }
  const handelMoreTransaction = (id) => {
    setState({ ...state, fresh_flag: false })
    fetchMore({
      variables: {
        driverId: user.id,
        first: 2,
        after: id,
        orderBy: ["-due_date"]
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        setNextPageLoading(false)
        return fetchMoreResult;
      }
    });
  }
  // Set the driver id from the storage
  useEffect(() => {

    checkNet();
    setStatusColor(Colors.account_status_bar_color);
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const radioPaymentMethodChange = (value, index) => {

    let NewPaymentMethod = (value == getPaymentIds(props, "card")) ? "Credit Card" : ((value == getPaymentIds(props, "zelle")) ? "Zelle" : ((value == getPaymentIds(props, "remotecash")) ? "Remote Cash" : ""));

    let show_tooltip = false;
    if (value == getPaymentIds(props, "zelle") || value == getPaymentIds(props, "remotecash")) {
      show_tooltip = true;
    }
    let boolOpenPaymentMethodModal = true;
    if (value == initPaymentID) {
      boolOpenPaymentMethodModal = false;
      setInitPaymentID(0);
    } else {
      setState({ ...state, zelleRemoteCachePopup: false, show_infoquestion_text: show_tooltip, error_other_payment_method: "", newPaymentMethod: NewPaymentMethod, currentPaymentMethodID: value, currentPaymentMethodLabel: NewPaymentMethod, radioPaymentTypeValue: value });
    }
  }
  const setZelleRemoteCachePopup = (value) => {
    setState({ ...state, zelleRemoteCachePopup: false })
  }

  let radio_props = [
    { label: "Credit Card " + '          ', value: getPaymentIds(props, "card") },
    { label: "Zelle (no fee)" + '          ', value: getPaymentIds(props, "zelle") },
    { label: "Remote Cash ($2 fee)" + '          ', value: getPaymentIds(props, "remotecash") },
  ];

  const showinfotext = () => {
    let show_tooltip = false;

    if (parseInt(radioPaymentTypeValue) == getPaymentIds(props, "zelle") || parseInt(radioPaymentTypeValue) == getPaymentIds(props, "remotecash") || parseInt(initPaymentID) == getPaymentIds(props, "zelle") || parseInt(initPaymentID) == getPaymentIds(props, "remotecash")) {
      show_tooltip = true;
    }
    setState({ ...state, zelleRemoteCachePopup: show_tooltip });
  }

  const setDefaultPayment = () => {

    if (user_id != "" && radioPaymentTypeValue > 0) {
      setState({ ...state, isLoading: true, isModalPaymentMethodChange: false });
      let input = {
        id: user_id,
        paymentMethod: radioPaymentTypeValue
      }
      DriverUpdatePayment({
        variables: { input }
      }).then(async (response) => {
        // console.log("DriverUpdatePayment Response ============>", response.data);
        if (response.data.updateDriver.errors) {
          // console.log("DriverUpdatePayment ERROR  ============>", response.data.updateDriver.errors);
          if (response.data.updateDriver.errors.length > 0 && response.data.updateDriver.errors[0].messages && response.data.updateDriver.errors[0].messages.length > 0) {
            if (response.data.updateDriver.errors[0].messages[0] == "") {
              await AsyncStorage.setItem('current_payment_id', currentPaymentMethodID.toString());
              setState({ ...state, radioPaymentTypeValue: currentPaymentMethodID, show_infoquestion_text: false, isLoading: false, isModalPaymentMethodChange: false, error_other_payment_method: "" });
              setInitReloadDropdown(!initReloadDropdown);
            } else {
              setState({ ...state, radioPaymentTypeValue: currentPaymentMethodID, isLoading: false, isModalPaymentMethodChange: false, error_other_payment_method: response.data.updateDriver.errors[0].messages[0] });
              setInitReloadDropdown(!initReloadDropdown);
            }
          } else {
            let error_other_paymentmethod = "You have already selected this payment method. Please select other payment method.";
            setState({ ...state, isLoading: false, isModalPaymentMethodChange: false, error_other_payment_method: error_other_paymentmethod });
            setInitReloadDropdown(!initReloadDropdown);
          }
        } else {
          await AsyncStorage.setItem('current_payment_id', currentPaymentMethodID.toString());
          setState({ ...state, radioPaymentTypeValue: currentPaymentMethodID, show_infoquestion_text: false, isLoading: false, isModalPaymentMethodChange: false, error_other_payment_method: "" });
          setInitReloadDropdown(!initReloadDropdown);
        }
      });
    } else {
      setState({ ...state, isLoading: false, isModalPaymentMethodChange: false, error_other_payment_method: "Please select payment method." });
    }

  }

  const setCardList = (cardList) => {
    setCardListLabel(cardList.length <= 1 ? 'Credit Card on File' : 'Credit Cards on File')
  }

  const radioCardChange = (card) => {
  }

  const showInvoice = async (index) => {
    const arr = [...invoiceList, ...receiptList]
    AsyncStorage.setItem('SelectedInvoice', (index).toString())
    AsyncStorage.setItem('Invoices', JSON.stringify(arr))
    AsyncStorage.setItem('Route', 'Billing')
    props.navigation.navigate('BillingInvoice')
  }
  const showTicket = async (index, status) => {
    const arr = [...invoiceList, ...receiptList]
    AsyncStorage.setItem('SelectedInvoice', (invoiceList.length > 0 ? index + 1 : index).toString())
    AsyncStorage.setItem('Invoices', JSON.stringify(arr))
    AsyncStorage.setItem('Route', 'Billing')
    props.navigation.navigate('BillingInvoice')
  }

  const updateCard = () => {
    AsyncStorage.setItem('update_card', 'yes');
    props.navigation.navigate('AccountPayment');
  }

  const gotoAddBankDetailScreen = () => {
    props.navigation.navigate('AddBankDetail');
  }

  return (<>
    {(mainReload) ?
      <View  >
        <ActivityIndicator size="large" style={{ padding: 60 }} />
      </View> :
      <View style={styles.main_container}><ScrollView>

        {(isLoading || isLoadInvoice) ?
          <View  >
            <ActivityIndicator size="large" style={{ padding: 60 }} />
          </View> : <></>}
        <View style={{ flexGrow: 1, flexDirection: 'column-reverse' }}>
          <View>
            <View style={[styles.billing_details_wrap]}>
              <View style={styles.billing_details_icon}>
                <Image width="38" style={styles.icon_input_2} source={require("../../assets/images/heart-icon.png")} />
              </View>
              <View style={styles.billing_details}>
                <Text style={[styles.billing_details_title, global_styles.lato_semibold]} >Share the Buggy Love</Text>
                <Text style={[styles.billing_details_title_text, global_styles.lato_regular]} >Refer a friend and you can earn up to $300</Text>
                <TouchableOpacity underlayColor="" style={styles.addbutton} onPress={() => { props.navigation.navigate('Referfriend') }}>
                  <Text style={[styles.buttonText, { marginBottom: 16 }, global_styles.lato_regular]}>Details</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ flexGrow: 1 }}>
            <KeyboardAwareScrollView style={{ flexGrow: 1 }}>

              <View style={styles.wrapper_info_main} >
                <View style={styles.statement_title} >

                  {(isLoading == false && isLoadInvoice == false) ? <>
                    <View style={styles.statement_wrapper} >
                      <Text style={[styles.statement_title_text, global_styles.lato_semibold]}>Invoice</Text>
                      <View style={styles.statement_list} >
                        {invoiceList != null && invoiceList.length > 0 &&
                          invoiceList.map((item, index) =>
                            (<TouchableOpacity key={index} onPress={() => showInvoice(index)}>
                              <View style={styles.statement_list_item} >
                                <View style={styles.statement_list_item_icon}>
                                  <Image width="50" style={styles.icon_input} source={item.chargeType == 'Current Bill' ? require("../../assets/images/list-icon-hover.png") : require("../../assets/images/list-icon.png")} />
                                </View>
                                <Text style={[styles.statement_list_item_text, global_styles.lato_semibold]}>{item.endDate + ' - ' + item.chargeType}</Text>
                                <Text style={[styles.statement_list_item_price, global_styles.lato_semibold]}>{item.totalAmount}</Text>
                              </View>
                            </TouchableOpacity>)
                          )
                        }
                        {showNoInvoiceFound && <Text style={[{ marginBottom: 16, }, global_styles.lato_regular]}>No invoice found.</Text>}
                        <Text style={[styles.statement_title_text, global_styles.lato_semibold]}>Receipts</Text>
                        {receiptList == null && <Text style={[global_styles.lato_regular]}>No receipt found.</Text>}
                        {receiptList != null && receiptList.length > 0 &&
                          receiptList.map((item, index) =>
                            (<TouchableOpacity key={index} onPress={() => showTicket(index, item.chargeType)}>
                              <View style={styles.statement_list_item} >
                                <View style={styles.statement_list_item_icon} >
                                  <Image width="50" style={styles.icon_input} source={item.chargeType == 'Current Bill' ? require("../../assets/images/list-icon-hover.png") : require("../../assets/images/list-icon.png")} />
                                </View>
                                <Text style={[styles.statement_list_item_text, global_styles.lato_semibold]}>{item.dueDate + ' - ' + item.paid}</Text>
                                <Text style={[styles.statement_list_item_price, global_styles.lato_semibold]}>{item.totalAmount}</Text>
                              </View>
                            </TouchableOpacity>)
                          )
                        }

                        {data_transaction_list != null && data_transaction_list.length > 0 ? <View style={styles.button_container}>
                          {pageInfo.hasNextPage == true &&
                            <TouchableOpacity onPress={() => { props.navigation.navigate('InvoiceList') }}>
                              <View style={styles.cancel_container}>
                                <Text style={[styles.cancel_container_text, global_styles.lato_regular]}>SHOW MORE</Text>
                              </View>
                            </TouchableOpacity>}

                          {data_transaction_pageinfo.hasNextPage == true ?
                            <TouchableOpacity onPress={loadMoreData}>
                              <View style={styles.cancel_container}>
                                <Text style={[styles.cancel_container_text, global_styles.lato_regular]}>SHOW MORE</Text>
                              </View>
                            </TouchableOpacity> : null}

                          {!isAllPaid && <TouchableOpacity onPress={() => { props.navigation.navigate('PaymentPaynow') }}>
                            <View style={styles.save_container}>
                              <Text style={[styles.save_text, global_styles.lato_regular]}>PAY NOW</Text>
                            </View>
                          </TouchableOpacity>}

                        </View> : <View style={styles.button_container}>
                            {!isAllPaid && <TouchableOpacity onPress={() => { props.navigation.navigate('PaymentPaynow') }}>
                              <View style={styles.save_container}>
                                <Text style={[styles.save_text, global_styles.lato_regular]}>PAY NOW</Text>
                              </View>
                            </TouchableOpacity>}
                          </View>}
                      </View>
                    </View>
                    <View style={styles.statement_contaner} >
                      <Text style={[styles.statement_title_text, global_styles.lato_semibold]}>Preferred Payment Method</Text>
                      <View style={styles.statement_list} >


                        {(user_id == "" || radioPaymentTypeValue == "") ? <></> : <View key={initReloadDropdown} style={[styles.profile_field_value_wrapper, styles.pickerIos, { flex: 1 }]} >

                          <View style={{ width: 170 }} >
                            {(user_id == "" || radioPaymentTypeValue == "") ? <></> :
                              <RNPickerSelect
                                style={pickerSelectStyles}
                                placeholder={{}}
                                onValueChange={(value, index) => radioPaymentMethodChange(value, index)}
                                mode="dropdown"
                                value={parseInt(radioPaymentTypeValue)}
                                items={radio_props}
                                useNativeAndroidPickerStyle={false}
                                Icon={() => {
                                  return (
                                    <View
                                      style={pickerSelectStyles.icon}
                                    />
                                  );
                                }}
                              />}
                          </View>

                          {(parseInt(radioPaymentTypeValue) == getPaymentIds(props, "zelle") || parseInt(radioPaymentTypeValue) == getPaymentIds(props, "remotecash")) &&
                            <TouchableHighlight underlayColor='' style={{ width: 30, height: 30, marginTop: 12, marginLeft: 5, borderRadius: 20 }} onPress={() => { showinfotext(); }}>
                              <Image width="22" style={styles.icon_input_question} source={require("../../assets/images/q_icon_notification.png")} />
                            </TouchableHighlight>
                          }
                          <TouchableOpacity underlayColor="" style={styles.addbutton2} onPress={() => setState({ ...state, isModalPaymentMethodChange: true })}>
                            <Text style={styles.buttonText}>Set as preferred</Text>
                          </TouchableOpacity>
                        </View>}

                      </View>
                    </View>
                  </> : <></>}

                  <View style={styles.statement_contaner} >
                    <Text style={[styles.statement_title_text, global_styles.lato_semibold]}>{cardListLabel}</Text>
                    <View key={initReload} style={styles.statement_card_list} >
                      <View style={{ flexDirection: 'row-reverse' }}>
                        <TouchableOpacity underlayColor="" style={styles.addbutton} onPress={() => updateCard()}>
                          <Text style={styles.update_cards}>Update cards</Text>
                        </TouchableOpacity>
                        <View style={{ alignSelf: 'stretch', flex: 1 }}>
                          <StripeCardListScreen  {...props} initReload={initReload} setCardList={setCardList} isLoading={isLoading} radioCardChange={radioCardChange} />
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </KeyboardAwareScrollView>
          </View>
        </View>


        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModalPaymentMethodChange} >
          <>
            <View style={modal_style.modal}>
              <View style={modal_style.modal_wrapper}>
                <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => setState({ ...state, isModalPaymentMethodChange: false })}>
                  <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                </TouchableOpacity>
                <Text style={modal_style.modal_heading}>Confirmation</Text>
                <Text style={modal_style.modalText}>Do you want to set {(newPaymentMethod == "Card") ? "Credit Card" : newPaymentMethod} as default payment method?</Text>
                <View style={modal_style.modal_textbuttonWrap}>
                  <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={() => setState({ ...state, isModalPaymentMethodChange: false })}>
                    <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Go Back</Text>
                  </TouchableOpacity>
                  <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={() => setDefaultPayment()}>
                    <Text style={[modal_style.upload_document_text_modal, global_styles.lato_regular]}>Accept</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </>
        </Modal>
      </ScrollView>
        <View key={zelleRemoteCachePopup}>
          {zelleRemoteCachePopup &&
            <ShowPaymentPopupScreen {...props} barcode_image={barcode_image} payment_type={(radioPaymentTypeValue != null && radioPaymentTypeValue != "") ? radioPaymentTypeValue : initPaymentID} user_id={pk} setZelleRemoteCachePopup={setZelleRemoteCachePopup} />
          }
        </View>
      </View >
    }
  </>
  );
}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1
  },
  icon_input: {
    width: 25,
    height: 25,
  },
  icon_input_2: {
    width: 38,
    height: 35,
  },
  icon_input_question: {
    width: 23,
    height: 23,
  },
  billing_details_icon: {
    marginRight: 16,
    marginTop: 2,
  },
  billing_details_title: {
    fontSize: 16,
    color: "#393E5C"
  },
  billing_details_title_text: {
    fontSize: 14,
    color: "#393E5C"
  },

  billing_details_wrap: {
    marginTop: 0,
    borderTopColor: "#f5f5f5",
    borderTopWidth: 1,
    flexDirection: "row",
    paddingHorizontal: 32,
    paddingTop: 16,
    width: "100%",
  },
  statement_title_text: {
    marginVertical: 0,
    marginBottom: 10,
    marginTop: 0,
    fontSize: 16,
  },
  statement_card_list: {
    marginLeft: -15,
  },
  statement_wrapper: {
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 15,
    paddingTop: 25
  },
  statement_title: {
    borderBottomWidth: 0,
    borderBottomColor: "#ccc",
    padding: 10,
  },
  statement_contaner: {
    borderBottomWidth: 0,
    borderBottomColor: "#ccc",
    padding: 20,
    paddingLeft: 20,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 0,
    paddingVertical: 16,
    marginTop: 16,
  },
  statement_list_item: {
    flexDirection: "row",
    marginVertical: 8,
    // borderBottomColor: "#f5f5f5",
    // borderBottomWidth: 1,
    paddingBottom: 10,
    marginTop: 5,
  },
  wrapper_info_main: {
    flex: 1,
    backgroundColor: '#f9f9f4',
  },
  statement_list_item_text: {
    marginLeft: 10,
    color: "#0B0B0C"
  },
  statement_list_item_price: {
    position: "absolute",
    color: "#0B0B0C",
    right: 0
  },
  save_container: {
    width: 130,
    backgroundColor: '#db9360',
    padding: 9,
    borderRadius: 6,
    alignItems: "center",
    marginStart: 16,
  },
  addbutton2: {
    paddingVertical: 5,
    paddingRight: 0,
    marginBottom: 10,
    borderRadius: 5,
    marginLeft: 1,
    position: "absolute",
    right: 0,
    top: 8
  },
  addbutton: {
    paddingVertical: 5,
    paddingRight: 0,
    marginBottom: 10,
    borderRadius: 5,
    marginLeft: 1,
    alignSelf: "flex-end",
  },
  buttonText: {
    fontSize: 13,
    color: "#2dafd3"
  },
  update_cards: {
    fontSize: 13,
    color: "#2dafd3",
    marginTop: 8,
  },
  cancel_container: {
    width: 130,
    backgroundColor: '#fff',
    padding: 9,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    borderColor: '#ccc',
  },
  cancel_container_text: {
    color: "#333333",
    fontSize: 13
  },
  button_container: {
    flexDirection: 'row',
    flex: 1,
    marginTop: 10,
    justifyContent: 'flex-end'
  },
  save_text: {
    color: '#FFFFFF',
    fontSize: 13,
    fontWeight: 'bold'
  },
  profile_field_value_wrapper: {
    paddingHorizontal: 0,
    flexDirection: "row",
    marginBottom: 0,
    paddingTop: 0,
  },
  pickerIos: {
    marginLeft: Platform.OS == 'ios' ? -10 : 0,
    marginTop: Platform.OS == 'ios' ? 5 : 0,
  },
  refunPopUpCotainer: {
    flex: 1,
    padding: 10,
    backgroundColor: "#393e5c",
    marginLeft: 15, marginRight: 15, marginTop: 10, borderRadius: 5,
    flexDirection: "row", alignItems: "center"
  },
  refundPopUpText: {
    color: "white",
    fontSize: 15,
    flex: 1

  }
});