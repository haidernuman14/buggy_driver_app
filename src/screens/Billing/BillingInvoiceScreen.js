import React, { useState, useEffect, useContext } from 'react';
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    Text,
    ActivityIndicator,
    View,
    TouchableOpacity,
    Button,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { property } from 'lodash';
import { ScreenTitle } from '../Helper/HeaderIcon';
import moment from "moment";
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { CheckConnectivity } from '../Helper/NetInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, } from '../../constants/Style';
import { useQuery } from '@apollo/react-hooks';
import { getInvoicePdf } from '../../graphql/getInvoicePdf';
import { getRecepitPdf } from '../../graphql/getReceiptPdf';
import RNFetchBlob from 'rn-fetch-blob';
import FileViewer from "react-native-file-viewer";
const win = Dimensions.get('window');

let currentIndex = 0
let invoiceArray = []
let isPdfDisplayed = false
var tempBuggyFee = ''
var tempToll = ''
function BillingInvoiceScreen(props) {

    const [state, setState] = useState({
        isLoading: false,
    });
    const { isLoading } = state;

    const [user, setUser] = useState({});
    const [invoice, setInvoice] = useState({})
    const [isDownloadInvoice, setDownloadInvoice] = useState(false)
    const [isDownloadRecepit, setDownloadRecepit] = useState(false)
    const [showPdf,setPdf] =  useState()
    const { setStatusColor } = useContext(langContext);
    const [invoiceID, setInvoiceID] = useState("")
    const [receiptID,setReceiptID]  = useState("")
    const [invoiceTitle, setInvoiceTitle] = useState("")

    // Check internet connection
    const checkNet = async () => {
        let netState = await CheckConnectivity();
        if (!netState) {
            props.screenProps.setNetModal(true)
            return;
        }
    }

    const setInvoiceData = async () => {
        setInvoice({})
        let index = parseInt(await AsyncStorage.getItem('SelectedInvoice'));
        
        invoiceArray = JSON.parse(await AsyncStorage.getItem('Invoices'));
           
        if (invoiceArray && invoiceArray.length > 0) {
            let routeName = await AsyncStorage.getItem('Route');
            let data = invoiceArray[index]
                if(data && data!=null && data.paid){
                    let recepitData = data
                    currentIndex = index
                    // console.log(recepitData.totalAmount)
                    recepitData.totalAmount = recepitData.totalAmount
                   props.navigation.setParams({ title:recepitData.dueDate, route: routeName })
                    setInvoice(recepitData)
                }
           else if (data && data != null) {
                let invoiceData = data
                props.navigation.setParams({ title: moment(invoiceData.startDate).format('MMMM DD') + ' - ' + invoiceData.endDate, route: routeName })
                currentIndex = index
                invoiceData.previousBalance = parseFloat(invoiceData.previousBalance)

                setInvoice(invoiceData)
            }
        }
    }

    const setuserid = async () => {
        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);
        setUser(user);
    }

    useEffect(() => {
        checkNet();
        setStatusColor(Colors.account_status_bar_color);
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            setuserid();
            setInvoiceData()
        }
        const navFocusListener = props.navigation.addListener('didFocus', () => {
            setuserid();
            setInvoiceData()
        });
        return () => {
            navFocusListener.remove();
        };
    }, [])
   
    const { data: pdfBase64, loading: isDownloadingPdf, error: downloadError } = useQuery(getInvoicePdf, {
        variables: { invoiceId: invoiceID },
        fetchPolicy: "network-only",
        skip: (isDownloadInvoice == false && invoiceID == "" && invoiceTitle == "")
    });

    const { data: PdfRecepitBase64, loading: isRecepitDownloadingPdf, error: downloadRecepitError } = useQuery(getRecepitPdf, {
        variables: { transactionId: receiptID},
        fetchPolicy: "network-only",
     
       
    });
          useEffect(() => {        
              
            // console.log('Download PDF useEffect called...')
          
        
        setDownloadRecepit(false)
       
        if(PdfRecepitBase64 && PdfRecepitBase64.hasOwnProperty('getRecieptPdf')){
            RNFetchBlob
                .config({
                    addAndroidDownloads: {
                        useDownloadManager: true,
                        notification: true,
                        mime: 'application/pdf',
                        description: 'File downloaded.'
                    }
                })
                let base64Str = PdfRecepitBase64.getRecieptPdf;
                let pdfLocation = RNFetchBlob.fs.dirs.DocumentDir + '/' +"Receipt Pdf"+ ".pdf";
                RNFetchBlob.fs.writeFile(pdfLocation, base64Str, 'base64').then(res => {
                    if (res > 0) {
                        if (!isPdfDisplayed) {
                            setPdf(PdfRecepitBase64)
                            isPdfDisplayed = true
                            show_doc(pdfLocation);
                        } else {
                            // console.log('Invoice already displayed')
                        }
                    }
                }).catch((error=>{console.log(error)}))
             
        }
        else{
            
        }
    } 
    ,[PdfRecepitBase64]
    )


useEffect(() => {
 if (pdfBase64 && pdfBase64.hasOwnProperty('getInvoicePdf')) {
            // console.log(pdfBase64.getInvoicePdf)
            RNFetchBlob
                .config({
                    addAndroidDownloads: {
                        useDownloadManager: true,
                        notification: true,
                        mime: 'application/pdf',
                        description: 'File downloaded.'
                    }
                })
                
            let base64Str = pdfBase64.getInvoicePdf;
            let pdfLocation = RNFetchBlob.fs.dirs.DocumentDir + '/' + invoiceTitle + ".pdf";
            // RNFetchBlob.fs.writeFile(pdfLocation, RNFetchBlob.base64.encode(base64Str), 'base64');
            RNFetchBlob.fs.writeFile(pdfLocation, base64Str, 'base64').then(res => {
                if (res > 0) {
                    if (!isPdfDisplayed) {
                        isPdfDisplayed = true
                        show_doc(pdfLocation);
                    } else {
                        // console.log('Invoice already displayed')
                    }
                }
            });
           
        }
    } 
    ,[pdfBase64]
    )
    const show_doc = async (pdfLocation) => {
        try {
            setInvoiceID("");
            setReceiptID("");
           // setInvoiceTitle("");
            setState({ ...state, isLoading: false });
            await FileViewer.open(pdfLocation, { showOpenWithDialog: true, showAppsSuggestions: true });
        } catch (e) {
            console.warn(TAG, "An error occurred", JSON.stringify(e));
        }
    }

    const seePreviousInvoice = async () => {
        if (invoiceArray && invoiceArray.length > 0 && currentIndex < invoiceArray.length - 1) {
            currentIndex = currentIndex + 1
            let invoiceData = invoiceArray[currentIndex]
            let routeName = await AsyncStorage.getItem('Route');
            props.navigation.setParams({ title: moment(invoiceData.startDate).format('MMMM DD') + ' - ' + invoiceData.endDate, route: routeName })
            invoiceData.previousBalance = parseInt(invoiceData.previousBalance)
            setInvoice(invoiceData)
        }
    }

    const showTicketDetails = async (chargeType) => {
        
        AsyncStorage.setItem('duration', moment(invoice.startDate).format('MMMM DD') + ' - ' + invoice.endDate)
        AsyncStorage.setItem('startDate', invoice.startDate)
        AsyncStorage.setItem('endDate', invoice.endDate)
        AsyncStorage.setItem('chargeType',chargeType.toString())
        props.navigation.navigate('TicketsAndTolls')
    }

    const payNow = () => {
        props.navigation.navigate('PaymentPaynow')
    }

    const download = (iid, title) => {
        isPdfDisplayed = false
        if (invoice && invoice != null) {
            setInvoiceID(iid);
           // setReceiptID(iid);
            setInvoiceTitle(title.replace(" ", "_").replace(" ", "_").replace(" ", "_").replace(" ", "_").replace(", ", "_").replace(",", "_").replace("__", "_").replace("__", "_"));
            setState({ ...state, isLoading: true });
            setDownloadInvoice(true)
        }
    }

    const downloadRecepit=(iid) =>{
         console.log(iid)
        isPdfDisplayed = false
        if(invoice && invoice != null){
             console.log(iid)
            setReceiptID(iid); 
            setState({ ...state, isLoading: true });
            //setState({ ...state, isLoading: true });
            setDownloadRecepit(true)
        }

    }


    return (
        <>
            {isLoading ?
                <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
                    <ActivityIndicator size="large" style={{ padding: 60 }} />
                </View> : <></>}
            <View style={styles.main_container}>
                {invoice && <KeyboardAwareScrollView showsVerticalScrollIndicator={false}><>
                      {invoice.paid?
                       <Text style={[styles.info, global_styles.lato_regular]}>{"Here's a receipt for your payment \n" +"made on "+invoice.dueDate}</Text>
                      :<Text style={[styles.info, global_styles.lato_regular]}>{"Here's  invoice for the week of \n" + moment(invoice.startDate).format('MMMM DD') + ' - ' + invoice.endDate}</Text>
                    }
                    <View style={styles.container}>
                        <View style={styles.due_container}>
                            <Text style={[styles.due_label, global_styles.lato_regular]}>Total due:</Text>
                            <Text style={[styles.due_amount, global_styles.lato_regular]}>{invoice.totalAmount}</Text>
                        </View>
                        <View style={styles.invoice_detail_container}>
                            {invoice != null && invoice.data != null && invoice.data.invoiceItemsGroup != null && invoice.data.invoiceItemsGroup.length > 0 &&
                                invoice.data.invoiceItemsGroup.map((item,index) =>{
                                       if (item.totalCharge >= 0 && item.chargeTypeDisplay=="Buggy Ticket Charge") {
                                        tempBuggyFee = (item.totalCharge >= 0 ? '+$' + item.totalCharge + ' Buggy Fee' : '-$' + (item.totalCharge * -1))
                                    }
                                    return(
                                    <View>
                                        <View style={{ height: 1, backgroundColor: '#f9f9f4' }} />
                                        <View style={styles.charge_container}>
                                            <View style={styles.charge_detail_amount_container}>
                                                {item.chargeType==9||tempBuggyFee==""?
                                                <Text style={[styles.charge_text, global_styles.lato_regular]}>{item.chargeTypeDisplay} & Buggy Fee</Text>:
                                                <Text style={[styles.charge_text, global_styles.lato_regular]}>{item.chargeTypeDisplay}</Text>}
                                                {item.chargeType==9||tempBuggyFee==""?
                                          <Text style={[styles.tool_charge_ammount, global_styles.lato_regular]}>{item.totalCharge >= 0 ? '$' + item.totalCharge.toFixed(2)+tempBuggyFee : '-$' + (item.totalCharge * -1).toFixed(2)+tempBuggyFee}</Text>
                                        :null}
                                          </View>
                                            {item.notes != '' && item.notes != '' && <Text style={[styles.notes, global_styles.lato_regular]}>{item.notes}</Text>}
                                            {(item.chargeType == 8 || item.chargeType == 9)
                                                && <TouchableOpacity onPress={() => showTicketDetails(item.chargeType)}>
                                                    <Text style={styles.see_detail}>See Details</Text>
                                                </TouchableOpacity>}
                                         
                                        </View>
                                    </View>)
                                })
                            }
                            {invoice != null &&
                                <View>
                                    <View style={{ height: 1, backgroundColor: '#f9f9f4' }} />
                                    {invoice.previousBalance > 0 && <View style={styles.charge_container}>
                                        <View style={styles.charge_detail_amount_container}>
                                            <Text style={[styles.charge_text, global_styles.lato_regular]}>Previous Balance</Text>
                                          
                                            <Text style={[styles.charge_text, global_styles.lato_regular]}>{"$"+invoice.previousBalance}</Text>
                                        </View>
                                        {/* <TouchableOpacity onPress={() => seePreviousInvoice()}>
                                            <Text style={styles.see_detail}>See Details</Text>
                                        </TouchableOpacity> */}
                                    </View>}
                                    <View style={{ height: 1, backgroundColor: '#f9f9f4' }} />
                                     {invoice.paid?
                                    <View style={styles.charge_detail_amount_container}>
                                        <Text style={[styles.total_label, global_styles.lato_medium]}>Payment Method</Text>
                                        <Text style={[styles.total_label, global_styles.lato_medium]}>{invoice.chargeType}</Text>
                                    </View>:  
                                    <View style={styles.charge_detail_amount_container}>
                                        <Text style={[styles.total_label, global_styles.lato_medium]}>Total due:</Text>
                                        <Text style={[styles.total_label, global_styles.lato_medium]}>{invoice.totalAmount}</Text>
                                    </View>
                                     }
                                </View>
                                }
                                
                        </View>
                        
                    </View>
                    {invoice.paid?
                                    <View style={[styles.charge_detail_dueDate]}>
                                    <Text style={[styles.total_label, global_styles.lato_medium]}>Payment Date</Text>
                                    <Text style={[styles.total_label, global_styles.lato_medium]}>{invoice.dueDate}</Text>
                                </View>
                                    :null}
                    <View style={styles.button_container}>
                        {(invoice.chargeType == 'Current Bill' && (invoice.totalAmount && parseFloat(invoice.totalAmount.replace('$',''))>0) ) && <TouchableOpacity style={{ marginTop: 16, width: 160 }} onPress={() => payNow()}>
                            <View style={global_styles.bottom_style_orange_wrapper}>
                                <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>PAY NOW</Text>
                            </View>
                        </TouchableOpacity>}
                        {invoice != null && <TouchableOpacity style={{ marginTop: 16, width: 160 }} onPress={() =>{
                            if(invoice.paid)
                            downloadRecepit(invoice.id)
                            else
                            download(invoice.id, moment(invoice.startDate).format('MMMM DD') + '_to_' + invoice.endDate)}}>
                            <View style={global_styles.button_style_white_wrapper}>
                                <Text style={global_styles.button_style_white_text}>DOWNLOAD</Text>
                            </View>
                        </TouchableOpacity>}
                    </View>
                </></KeyboardAwareScrollView>}
            </View ></>
    );

}

export default BillingInvoiceScreen;

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
    },
    info: {
        alignSelf: 'center',
        fontSize: 16,
        color: '#000',
        margin: 16,
        textAlign: 'center',
    },
    container: {
        backgroundColor: '#fff',
        marginTop: 8,
        paddingVertical: 16,
    },
    due_container: {
        alignItems: 'center',
        marginBottom: 16,
    },
    due_label: {
        fontSize: 12,
        color: 'rgba(0,0,0,0.6)',
    },
    due_amount: {
        fontSize: 32,
        color: '#000',
    },
    invoice_detail_container: {
        paddingBottom: 8,
    },
    charge_container: {
        paddingVertical: 16,
        paddingHorizontal: 24,
    },
    charge_detail_amount_container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    charge_detail_dueDate: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop:5
    },
    charge_text: {
        fontSize: 18,
        color: 'rgba(51,54,59,0.6)',
        
    },
    tool_charge_ammount:{
        fontSize: 15,
        color: 'rgba(51,54,59,0.6)',
        
          
    },
    notes: {
        fontSize: 12,
        marginEnd: 56,
        color: 'rgba(51,54,59,0.6)'
    },
    see_detail: {
        color: '#56ADF7',
    },
    total_label: {
        paddingVertical: 16,
        paddingHorizontal: 24,
        fontSize: 18,
        color: 'rgba(51,54,59,0.8)'
    },
    button_container: {
        justifyContent: 'flex-end',
        marginVertical: 22,
        marginHorizontal: 16,
        alignItems: 'flex-end',
    },
    pay_now_container: {
        width: 140,
        backgroundColor: '#dedbdb',
        padding: 8,
        borderWidth: 1,
        borderRadius: 6,
        alignItems: "center",
        borderColor: '#dedbdb',
        marginTop: 16,
    },
    download_container: {
        width: 140,
        backgroundColor: '#fff',
        padding: 8,
        borderWidth: 1,
        borderRadius: 6,
        alignItems: "center",
        borderColor: '#dedbdb',
        marginTop: 16,
    },
})