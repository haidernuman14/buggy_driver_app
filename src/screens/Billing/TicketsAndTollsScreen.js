import React, { useState, useEffect, useContext } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    View,
    TouchableOpacity,
    Button,
    Linking,
    ActivityIndicator,
    Platform,
    Dimensions,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { allTransactionsTicketsAndTolls } from '../../graphql/allTransactionsTicketsAndTolls'
import { useQuery } from '@apollo/react-hooks';
import { ScreenTitle } from '../Helper/HeaderIcon';
import { global_styles } from '../../constants/Style';
import AsyncStorage from '@react-native-community/async-storage';
import {Tooltip} from 'react-native-elements';

import moment from "moment";

const CHARGE_TICKET = 8
const CHARGE_TOLL = 9
var tempBuggyFee = ''
const win = Dimensions.get('window');

function TicketsAndTollsScreen(props) {

    const [user, setUser] = useState({});
    const [duration, setDuration] = useState()
    const [chargeType, setChargeType] = useState()
    const [startDate, setStartDate] = useState()
    const [endDate, setEndDate] = useState()
    const [tickets, setTickets] = useState([])
    const [tolls, setTolls] = useState([])
    const [violationNumbers, setViolationNumbers] = useState([])
    const [vInfo, setViInfo] = useState([])
    const [isLoadTicketAndTolls, setLoadTicketAndTolls] = useState(false)
    // const [tempBuggyFee,setTempBuggyFee]=useState({});
    // const [tempToll,setTempToll]=useState({});

    const { data: ticketsAndTolls, loading: isTicketsAndTollsLoading, error: ticketsAndTollsError } = useQuery(allTransactionsTicketsAndTolls, {
        variables: {
            driverId: user.id,
            chargeTypeIntIn: [CHARGE_TICKET, CHARGE_TOLL],
            dueDate_Gte: startDate,
            dueDate_Lte: endDate,
        },
        fetchPolicy: "network-only",
        skip: isLoadTicketAndTolls == false
    });

    useEffect(() => {
        // console.log(user.id)
        // console.log('Tickets & tolls use effect called...')
        setLoadTicketAndTolls(false)
        if (ticketsAndTolls && ticketsAndTolls.hasOwnProperty('allTransactions')
            && ticketsAndTolls.allTransactions.hasOwnProperty('edges')) {
            const tickets = []
            const tolls = []
            const vNumbers = []
            const vInfo = []
            // console.log(ticketsAndTolls)
            ticketsAndTolls.allTransactions.edges.map((item, index) => {

                if (item.node.notes != '' && item.node.notes.indexOf('Date') > -1) {
                    let strings = item.node.notes.split("Date")
                    if (strings.length > 1) {
                        let dateString = strings[1]
                        let date = dateString.substring(1, 20)
                        if (item.node.chargeType == 'A_9') {
                            item.node.dueDate = moment(date).format('MM/DD/YYYY HH:mm:ss')
                        } else {
                            item.node.dueDate = date
                        }

                    }
                }

                let info = 'N/A';
                if (item.node.notes != '' && item.node.notes.indexOf('Date') > -1) {
                    let strings = item.node.notes.split("Date")
                    if (strings.length > 1) {
                        let dateString = strings[1]
                        let date = dateString.substring(1, 20)
                        let temp = item.node.notes.replace(strings[0] + "Date " + date + ' - ', '')
                        info = temp.substring(0, temp.lastIndexOf('-'))
                    }
                }
                vInfo.push(info)
                let violationNumber = 'N/A'
                let checkString  =  item.node.notes
                if (checkString!= '' && checkString.indexOf('Violation #') > -1) {
                    let strings = checkString.split("Violation ")
                    if (strings.length > 1) {
                      let violationNum = strings[1]
                        let number = violationNum.substring(0, violationNum.lastIndexOf('-'))
                        violationNumber = number.substring(1);
                    }
                }
                 vNumbers.push(violationNumber)
                if (item.node.confirmationId == null)
                    tickets.push(item.node)
                else
                    tolls.push(item.node)
            })
            setTickets(tickets)
            setTolls(tolls)
            setViolationNumbers(vNumbers)
            setViInfo(vInfo)
        }
    }, [ticketsAndTolls])

    const setData = async () => {

        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);
        setUser(user);

        let duration = await AsyncStorage.getItem('duration')
        let startDate = await AsyncStorage.getItem('startDate')
        let endDate = await AsyncStorage.getItem('endDate')
        let chargeType = await AsyncStorage.getItem('chargeType')

        setDuration(duration)
        setStartDate(moment.utc(startDate).format("YYYY-MM-DD"))
        setEndDate(moment.utc(endDate).format("YYYY-MM-DD"))
        // console.log("Start date :"+moment.utc(startDate).format("YYYY-MM-DD")+"Last Date :"+moment.utc(endDate).format("YYYY-MM-DD"))
        setChargeType(chargeType)

        setLoadTicketAndTolls(true)

    }

    useEffect(() => {
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            setData()
        }
        const navFocusListener = props.navigation.addListener('didFocus', () => {
            setData()
        });
        return () => {
            navFocusListener.remove();
        };
    }, [])

    return (

        <View style={styles.main_container}>
            {isLoadTicketAndTolls ?
                <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
                    <ActivityIndicator size="large" style={{ padding: 60 }} />
                </View> :
                <KeyboardAwareScrollView showsVerticalScrollIndicator={false}><>
                    {chargeType == "8" ?
                        <View>
                            <Text style={[styles.title, global_styles.lato_semibold]}>{'Tickets & Violations Fees for week of\n' + duration}</Text>
                            <View style={styles.label_container}>
                                <Text style={[styles.label_left, global_styles.lato_semibold]}>Event Date</Text>
                                <Text style={[styles.label_center, global_styles.lato_semibold]}>Violation Number</Text>
                                <Text style={[styles.label_right, global_styles.lato_semibold]}>Buggy Fee</Text>
                            </View>
                            
                            {tickets.length > 0 && tickets.map((item, index) => (
                                <View style={index % 2 == 0 ? styles.content_container_with_no_back_color : styles.content_container_with_back_color}>
                                    <Text style={[styles.content_left, global_styles.lato_regular]}>{moment(item.dueDate).format('MM/DD/yyyy')}</Text>
                                    <Text style={[styles.content_center, global_styles.lato_regular]}>{violationNumbers[index]}</Text>
                                    <Text style={[styles.content_right, global_styles.lato_regular]}>{(item.amount >= 0 ? '$' + item.amount : '-$' + (item.amount * -1))}</Text>
                                </View>
                            ))}
                            {tickets.length <= 0?
                              <View style={{justifyContent:"center",flex:1,alignItems:"center"}}>
                              <Text style={[styles.info, global_styles.lato_regular]}>Sorry no records found</Text>
                              </View>
                              :null}
                            <Text style={[styles.info, global_styles.lato_regular]}>To search for your violation
                <Text style={[styles.link_text, global_styles.lato_regular]} onPress={() => { }}> click here.</Text> To pay the violation click the violation number above.</Text>
                            <View style={{ marginTop: 16 }} />
                        </View>
                        :
                        <View>
                            <Text style={[styles.title, global_styles.lato_semibold]}>{'Toll Fees for week of\n' + duration} </Text>
                            <View style={styles.label_container}>
                                <Text style={[styles.label_left, global_styles.lato_semibold]}>Toll Date</Text>
                                <Text style={[styles.label_center, global_styles.lato_semibold]}>Location</Text>
                                <Text style={[styles.label_right, global_styles.lato_semibold]}>Amount</Text>
                            </View>
                            {tolls && tolls.length > 0 && tolls.map((item, index) => {
                                if (index % 2 == 0) {
                                    tempBuggyFee = (item.amount >= 0 ? '$' + item.amount + ' Buggy Fee' : '-$' + (item.amount * -1))
                                    tempToll = item.checkNo
                                    return
                                }
                                else {
                                    if (tempToll == item.checkNo) {
                                        return (
                                            <View style={index % 2 == 0 ? styles.content_container_with_no_back_color : styles.content_container_with_back_color}>
                                                <Text style={[styles.content_left, global_styles.lato_regular]}>{item.dueDate}</Text>
                                                <Text style={[styles.content_center, global_styles.lato_regular]}>{vInfo[index]}</Text>
                                                <Text style={[styles.content_right, global_styles.lato_regular]}>{(item.amount >= 0 ? '$' + item.amount + " + " + tempBuggyFee : '-$' + (item.amount * -1))}</Text>

                                            </View>
                                        )
                                    }
                                }
                            }
                            )
                            }
                            {tolls.length <= 0?
                            <View style={{justifyContent:"center",flex:1,alignItems:"center"}}>
                            <Text style={[styles.info, global_styles.lato_regular]}>Sorry no records found</Text>
                            </View>
                            :null}
                        </View>
                          
                        }
                </></KeyboardAwareScrollView>}
        </View>
    );

}

TicketsAndTollsScreen.navigationOptions = ({ navigation }) => {
    return ({
        headerTitle: ScreenTitle("Tickets & violations details"),
    })
};
export default TicketsAndTollsScreen;

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
        paddingBottom: 16,
    },
    title: {
        fontSize: 16,
        margin: 16,
        textAlign: 'center',
    },
    label_container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#fff',
        paddingHorizontal: 16,
        paddingVertical: 12,
    },
    label_left: {
        flex: 0.6,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 15,
        textAlign: 'left',
    },
    label_center: {
        flex: 1,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 15,
        textAlign: 'center',
    },
    label_right: {
        flex: 0.5,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 15,
        textAlign: 'center',
    },
    content_container_with_no_back_color: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
    content_container_with_back_color: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#fff',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
    content_left: {
        flex: 0.6,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 14,
        textAlign: 'left',
    },
    content_center: {
        flex: 1,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 14,
        textAlign: 'center',
    },
    content_right: {
        flex: 0.5,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 14,
        textAlign: 'center',
    },
    info: {
        margin: 16,
        color: '#000',
        fontSize: 14,
    },
    toll_label_left: {
        flex: 1,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 15,
        textAlign: 'left',
    },
    toll_label_right: {
        flex: 1,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 15,
        textAlign: 'center',
    },
    toll_content_left: {
        flex: 1,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 14,
        textAlign: 'left',
    },
    toll_content_right: {
        flex: 1,
        color: 'rgba(0,0,0,0.6)',
        fontSize: 14,
        textAlign: 'right',
    },
    link_text: {
        color: 'rgb(45,175,211)',
    },
});