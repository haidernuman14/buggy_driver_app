import React, { useState,useEffect } from 'react';
import { TouchableHighlight, Text, Keyboard } from 'react-native';
import { global_styles } from '../../constants/Style'; 
import BellIcon from 'react-native-vector-icons/EvilIcons';  
import { Badge } from 'react-native-elements' 
import AsyncStorage from '@react-native-community/async-storage'; 
import { driverNotificationCounts } from '../../graphql/driverNotificationCounts';
import {  useQuery } from '@apollo/react-hooks';  
// Get the notification count 

//Render the UI of screen
const NotificationCount = (props) => { 
//export default const NotificationCount(props) {
  let navigation = props.navigation; 
  const [notificationcount, setNotificationCount] = useState(0); 

  const [state, setState] = useState({  
    driverId:"", 
  });
  const {driverId} = state;

  useEffect(() => {
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      details();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => { 
       details();
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const { data: drivar_notification, loading: drivar_data_loading, error: drivar_data_error } = useQuery(driverNotificationCounts, {
    variables: { id: driverId },
    fetchPolicy: "network-only",
    skip: (driverId == '')
  });

  const details = async () => { 

    console.log("test-->notification");
    setState({...state,driverId:"" }); 
    const notificationids = await AsyncStorage.getItem('nids');
    let notificationlist = JSON.parse(notificationids); 
    let unread = 0;
    if( notificationlist ){
      notificationlist.map((dataitem,key) => { 
        if(!dataitem.isRead){ 
          unread = unread + 1;
        }  
      });
    } 
   
    if( unread > 99 ){
      unread = "99+";
    }
    setNotificationCount(unread); 

    const user = await AsyncStorage.getItem('user'); 
    if(user) {
      setState({...state, driverId:JSON.parse(user).id });
    }

  } 
  useEffect(() => { 
    if (drivar_notification) {
 
      if ( drivar_notification && drivar_notification.hasOwnProperty('driver')   && drivar_notification.driver.hasOwnProperty('unreadNotificationsCount') ) {
          
            let nt_list = [];
            let unread = drivar_notification.driver.unreadNotificationsCount; 
            if( unread > 99 ){
              unread = "99+";
            }
            setNotificationCount(unread);
            console.log("notification_count------",unread); 
            setState({...state,driverId:"" });  

      }

    }
  }, [drivar_notification]);


  return ( 
      <> <BellIcon
            style={global_styles.header_bell_icon} 
            name="bell"
            size={30}
          />
          { (notificationcount != "" || notificationcount >0) &&<Text  onPress={() => props.navigation.navigate('NotificationList',{"backitem":props.navigation.state.routeName})} style={{backgroundColor:"red", position:"absolute", right:18, top:13, fontSize:10, fontWeight:"bold", color:"white", padding:3, borderRadius:15}} >{notificationcount}</Text> }
      </>
  );   
} 

export default NotificationCount;