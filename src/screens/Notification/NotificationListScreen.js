import React, {  useEffect, useState } from 'react';
import { View, Text, Image, Dimensions, Platform, StyleSheet, ScrollView,TouchableHighlight,ActivityIndicator } from 'react-native';
import { global_styles } from '../../constants/Style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BellIcon from 'react-native-vector-icons/EvilIcons'; 
import AsyncStorage from '@react-native-community/async-storage';
import { all_DriverNotifications } from '../../graphql/driverAllDriverNotifications';
import { CheckConnectivity } from '../Helper/NetInfo'; 
import { useMutation, useQuery } from '@apollo/react-hooks';
import moment from 'moment';

/**
 * This class displays Notification list 
 */
const win = Dimensions.get('window');
function NotificationListScreen(props) {

  const [notificationlist, setNotificationlist] = useState(false);
  const [state, setState] = useState({ 
    isLoading: false,
    driverId:"",
    notification_list:{},
  });
  const {isLoading, notification_list,driverId} = state;

  useEffect(() => {
    checkNet();

    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      getNotificationDetails();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      getNotificationDetails(); 
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true);
      return;
    }
  } 

  const { data: drivar_notification, loading: drivar_data_loading, error: drivar_data_error } = useQuery(all_DriverNotifications, {
    variables: { driverId: driverId },
    fetchPolicy: "network-only",
    skip: (driverId == '')
  });

  
  useEffect(() => {
    if (drivar_notification) {
 
      let nt_list = [];
      let readcount = 0;
      if (drivar_notification.allDriverNotifications && drivar_notification.allDriverNotifications.edges.length > 0) {
        drivar_notification.allDriverNotifications.edges.map((ntlistitem) => {
          let listitem = {
            id: ntlistitem.node.pk,
            sid: ntlistitem.node.id,
            status: 0,
            title: ntlistitem.node.title,
            isRead: ntlistitem.node.isRead,
            description: ntlistitem.node.message,
            date: moment(ntlistitem.node.dateAdded).format('MM-DD-YYYY')
          }
          console.log("ntlistitem.node.isRead>>",ntlistitem.node.isRead,(!ntlistitem.node.isRead));

          nt_list.push(listitem);
          if(!ntlistitem.node.isRead)
            readcount = readcount+1; 
        });
      }

      AsyncStorage.setItem('nids', JSON.stringify(nt_list));  
      setState({...state,driverId:"", isLoading:false });
      setNotificationlist(nt_list); 

    }
  }, [drivar_notification]);


  // Get notification detail
  const getNotificationDetails = async () => {
   /* const notificationids = await AsyncStorage.getItem('nids');
    let obj_no = JSON.parse(notificationids); 
    setNotificationlist(obj_no); */

    const user = await AsyncStorage.getItem('user'); 
    if(user) {
      setState({...state, isLoading:true, driverId:JSON.parse(user).id });
    }
  }
 

  // Manage notification unread count and redirect to the NotificationDetailScreen
  const setReadNotificationFlag = async (id) => { 
      let sid = "";
      if( notificationlist ){
        notificationlist.map((dataitem,key) => { 
          if(id==dataitem.id){ 
            sid = dataitem.sid;
            notificationlist[key].status  = 1; 
          }  
        });
      }  
      setNotificationlist(notificationlist);
      setStorageNotification(notificationlist);
      props.navigation.navigate("NotificationDetail",{nid:sid});
  }

  // Storing notification id
  const setStorageNotification = async(notificationlist1) => {
    await AsyncStorage.setItem('nids', JSON.stringify(notificationlist1) );
  }

  return (
    <>
       {isLoading ?
        <View style={[global_styles.activityIndicatorView,{	top: (Platform.OS=="web")?(win.height/2)-100:0 }]}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>: <ScrollView contentContainerStyle={(Platform.OS=="web")?{ flexGrow: 1, height:(win.height-67) }:{}}>
        <View style={styles.main_wrapper}>
          <View style={styles.notification_wrap}>
              { notificationlist && notificationlist.length > 0 ?
                <>
                { notificationlist.map((dataitem) =>
                  <View style={[styles.notification_item, ((dataitem.isRead)?styles.notification_item_green:styles.notification_item_red)]}>
                    <BellIcon style={styles.bell_icon} name='bell' />
                      <Text style={[styles.notification_datetime, global_styles.lato_semibold]}>{dataitem.date}</Text>
                      <TouchableHighlight onPress={() => setReadNotificationFlag(dataitem.id) }  underlayColor="">
                        <Text style={[styles.notification_title, ((dataitem.isRead)?global_styles.lato_regular:global_styles.lato_semibold), (Platform.OS=="web" && dataitem.isRead != true)?{ fontWeight:"bold" }:{} ]}>{dataitem.title}</Text>              
                      </TouchableHighlight> 
                      {
                        (Platform.OS=="web")? 
                        <img src={(dataitem.isRead==true)?require('../../assets/images/green.jpg'):require('../../assets/images/rightside.jpg')}  width={8} height={52} style={{opacity:0.4, position:"absolute", right:0, top:0}} />
                        :
                        <Image source={(dataitem.isRead==true)?require('../../assets/images/green.jpg'):require('../../assets/images/rightside.jpg')} style={(dataitem.isRead==true)?styles.notification_right_image_green:styles.notification_right_image_red} />
                      }   
                  </View>)
                }
              </>:<Text style={styles.notification_empty} >No notification found.</Text>  } 
          </View>
        </View>
      </ScrollView> }
    </>
  );
}

// Stylesheet to design Notification List
const styles = StyleSheet.create({
  main_wrapper: {
    backgroundColor: '#fafafa'
  },
  notification_empty: {
    marginHorizontal: 20,
    marginVertical:20
  },
  notification_wrap: {
    marginHorizontal: 0,
    marginBottom: 5
  },
  notification_item: {
    backgroundColor: '#fff',
    paddingHorizontal: 13,
    paddingVertical: 12,
    marginVertical: 10,
    marginHorizontal: 10,
    marginBottom: 0,
    zIndex: 10,
    shadowColor: "#000",
    zIndex: 10,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.29,
    shadowRadius: 1.0,
    elevation: 1, 
    borderRadius: 2,
    borderLeftWidth: 0,
  },
  notification_item_red: {
    borderColor: "#db1a44"
  },
  notification_item_green: {
    borderColor: "#0bba3a"
  },
  notification_title: {
    fontSize: 12,
    marginLeft: 37,
    color: "#454545",
  },
  notification_desc:{
    fontSize: 11,
    marginLeft: 37,
    color: "#454545",
  },
  bell_icon: {
    color: "#adadad",
    position: "absolute",
    left: 10,
    fontSize: 25,
    top: 13,
    borderRadius:50,
    borderWidth:1,
    borderColor:"#adadad",
    padding:2,
    paddingVertical:5,
    textAlign:"center"
  },
  notification_datetime: {
    color: "#adadad",
    fontSize: 10,
    marginLeft:37,
  },
  notification_right_image_red: {
    position: "absolute",
    right: 0,
    fontSize: 10,
    top: 5,
    opacity: 0.4
  },
  notification_right_image_green: {
    position: "absolute",
    right: 0,
    fontSize: 10,
    top: 5,
    opacity: 0.2
  }
});

export default NotificationListScreen;