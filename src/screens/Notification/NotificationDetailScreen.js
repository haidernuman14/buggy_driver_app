import React, {  useEffect, useState } from 'react';
import { View, Text,  Dimensions, Platform, Image,ActivityIndicator, StyleSheet, ScrollView } from 'react-native';
import { global_styles } from '../../constants/Style';
import BellIcon from 'react-native-vector-icons/EvilIcons'; 
import AsyncStorage from '@react-native-community/async-storage';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { markNotificationRead } from '../../graphql/markNotificationRead';
import { CheckConnectivity } from '../Helper/NetInfo';
import { HtmlText } from "@e-mine/react-native-html-text"; 

/**
 * Displays notification detail. 
 */
const win = Dimensions.get('window');
function NotificationDetailScreen(props) {

  const [state, setState] = useState({ 
    isLoading: false,
    driverId:""
  });
  const {isLoading, driverId} = state;
  const [AmarkNotificationRead] = useMutation(markNotificationRead);

  const [notification, setNotification] = useState({});
  useEffect(() => {
    checkNet();

    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      getNotificationDetails();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      getNotificationDetails(); 
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const getNotificationDetails = async () => {
    const notificationids = await AsyncStorage.getItem('nids');
    let obj_no = JSON.parse(notificationids); 
    let id = props.navigation.state.params.nid;  

    const user = await AsyncStorage.getItem('user'); 
    if(user) {

      if( obj_no ) {
        obj_no.map((dataitem,key) => { 
          if(id==dataitem.sid){ 
            setNotification(dataitem);  
          }  
        });
      }
      setState({...state, isLoading:true});

      let input = { 
        notificationId:id, 
      }
      console.log("input>>",input);
      AmarkNotificationRead({
        variables: { input }
      }).then((response) => {
          console.log("response============>",response,response.data.markNotificationRead.errors);
          if( response.data.markNotificationRead.ok ) {
            setState({...state, isLoading:false, driverId:JSON.parse(user).id });
            /*if( obj_no ) {
              obj_no.map((dataitem,key) => { 
                if(id==dataitem.sid){ 
                  setNotification(dataitem); 
                  setState({...state, isLoading:false, driverId:JSON.parse(user).id });
                }  
              });
            }  */
          }  
      }); 

    }
    
  }

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true);
      return;
    }
  } 

  return (
    <>
      {isLoading ?
        <View  style={[global_styles.activityIndicatorView,{	top: (Platform.OS=="web")?(win.height/2)-100:0 }]}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>:
        <ScrollView contentContainerStyle={(Platform.OS=="web")?{ flexGrow: 1, height:(win.height-130) }:{}}>
          <View style={styles.main_wrapper}>
            <View style={styles.notification_wrap}>
              <View style={[styles.notification_item, styles.notification_item_red]}>
                <BellIcon style={styles.bell_icon} name='bell' />
                { notification && notification.id != "" && <>
                    <Text style={[styles.notification_datetime, global_styles.lato_semibold]}>{notification.date}</Text>
                    <Text style={[styles.notification_title, global_styles.lato_semibold]}>{notification.title}</Text>
                    {/* <Text style={[styles.notification_desc, global_styles.lato_regular]}>{notification.description}</Text> */}
                    <HtmlText style={[styles.notification_desc, global_styles.lato_regular]}>{notification.description}</HtmlText>
                    </>
                }
                 {
                    (Platform.OS=="web")? 
                    <img src={require('../../assets/images/green.jpg')}  width={8} height={52} style={{opacity:0.4, position:"absolute", right:0, top:0}} />
                    :
                    <Image source={require('../../assets/images/green.jpg')} style={styles.notification_right_image_red} />
                 }
              </View>
            </View>
          </View>
        </ScrollView>}
    </>
  );
}

export default NotificationDetailScreen;

// Stylesheet to design notification detail screen
const styles = StyleSheet.create({
  main_wrapper: {
    backgroundColor: '#fff', 
  },
  notification_wrap: {
    marginHorizontal: 0,
    marginBottom: 5
  },
  notification_item: {
    backgroundColor: '#fff',
    paddingHorizontal: 13,
    paddingVertical: 12,
    marginVertical: 10,
    marginHorizontal: 10,
    marginBottom: 0,
    zIndex: 10,
    shadowColor: "#000",
    zIndex: 10,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.29,
    shadowRadius: 1.0,
    elevation: 1, 
    borderRadius: 2,
    borderLeftWidth: 0,
  },
  notification_item_red: {
    borderColor: "#db1a44"
  },
  notification_item_green: {
    borderColor: "#0bba3a"
  },
  notification_title: {
    fontSize: 12,
    marginLeft: 37,
    color: "#454545",
  },
  notification_desc:{
    fontSize: 11,
    marginLeft: 37,
    marginTop:5,
    color: "#454545",
  },
  bell_icon: {
    color: "#adadad",
    position: "absolute",
    left: 10,
    fontSize: 25,
    top: 13,
    borderRadius:50,
    borderWidth:1,
    borderColor:"#adadad",
    padding:2,
    paddingVertical:5,
    textAlign:"center"
  },
  notification_datetime: {
    color: "#adadad",
    fontSize: 10,
    marginLeft:37,
  },
  notification_right_image_red: {
    position: "absolute",
    right: 0,
    fontSize: 10,
    top: 5,
    opacity: 0.5
  },
  notification_right_image_green: {
    position: "absolute",
    right: 0,
    fontSize: 10,
    top: 5,
    opacity: 0.2
  }
});


