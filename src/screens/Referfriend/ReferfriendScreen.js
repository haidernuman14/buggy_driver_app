import React, { useState, useEffect } from 'react';
import {
  Platform, StyleSheet, SafeAreaView, ScrollView, View, Text, Image, Linking, TouchableHighlight, TouchableOpacity
} from 'react-native';
import { translate } from '../../components/Language';
import { global_styles } from '../../constants/Style';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const image = require('../../assets/images/Sharing.png');

export default function ReferfriendScreen(props) {

  return (
    <View style={styles.main_container}>
      <Text style={[styles.heading, global_styles.lato_medium]}>Refer a friend and earn!</Text>
      <KeyboardAwareScrollView>
        <Text style={[styles.content, global_styles.lato_regular]}>Refer new drivers to Buggy and you can earn up to $300.</Text>
        <Image style={styles.image} source={image} />
        <Text style={[styles.step, global_styles.lato_regular]}>Step: 1</Text>
        <Text style={[styles.step, global_styles.lato_regular]}>Refer a friend to Buggy. Have them message us at (347) 334-6317 to ask for an application or click
        <Text style={[styles.link_text, global_styles.lato_regular]} onPress={() => { Linking.openURL('https://www.joinbuggy.com/') }}> here</Text> to fill out an application.</Text>
        <Text style={[styles.step, global_styles.lato_regular]}>Step: 2</Text>
        <Text style={[styles.step, global_styles.lato_regular]}>When they pickup a car, have them let us know that you referred them.</Text>
        <Text style={[styles.step, global_styles.lato_regular]}>Step: 3</Text>
        <Text style={[styles.step, global_styles.lato_regular]}>Earn up to $300 in rebates towards your rental fee. Earn $50 per week for the first 6 weeks that your friend is in their car.</Text>
        <TouchableOpacity onPress={() => { Linking.openURL('https://www.joinbuggy.com/referral') }}>
          <View style={styles.terms_container}>
            <Text style={[styles.terms_text, global_styles.lato_medium]}>TERMS</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </View>
  );
}

// Stylesheet to design Refer a friend
const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    padding: 16,
  },
  heading: {
    fontSize: 20,
    marginTop: 16,
    marginBottom: 16,
  },
  content: {
    color: '#393e5c',
    fontSize: 14,
  },
  image: {
    width: 160,
    height: 160 * 0.81,
    alignSelf: 'center',
    marginTop: 32,
  },
  step: {
    color: '#393e5c',
    marginTop: 16,
    fontSize: 14,
  },
  terms_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    alignSelf: 'center',
    marginStart: 16,
    borderColor: '#db9360',
    marginTop: 32,
  },
  terms_text: {
    color: '#FFFFFF',
  },
  link_text: {
    color: 'rgb(45,175,211)',
  },
});