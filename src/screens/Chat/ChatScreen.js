import React, { useState, useEffect } from 'react';
import { ActivityIndicator, View, Text, Dimensions, Platform, ScrollView, SafeAreaView, StyleSheet, TouchableHighlight } from 'react-native';
import Colors from '../../constants/Colors';
import Icon from 'react-native-vector-icons/AntDesign';
import { CheckConnectivity } from '../Helper/NetInfo';
import AsyncStorage from '@react-native-community/async-storage';
import { translate } from '../../components/Language';
import firstApi from '../../components/chat/firstApi';
import listApi from '../../components/chat/listApi';
import messageApi from '../../components/chat/messageApi';
import { global_styles } from '../../constants/Style';
import moment from 'moment';
import Textarea from 'react-native-textarea';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import createConversationApi from '../../components/chat/createConversationApi';
import createContactApi from '../../components/chat/createContactApi';

const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZXMiOlsic2hhcmVkOioiLCJwcml2YXRlOioiXSwiaXNzIjoiZnJvbnQiLCJzdWIiOiJidWdneSJ9.6mtL6cA4QhZT50LQfeZWh0000-MzuqPayxP2OVGqao0";
export default function ChatScreen(props) {
  const [state, setState] = useState({
    isLoading: true,
    chatList: [],
    error_message:"",
    chat_message:"",
    linebreaknumber:0,
  });
  const { isLoading, linebreaknumber, error_message, chat_message, chatList } = state;
  const [phone, setPhone] = useState("9176356733");
  const [chatloader, setChatLoader] = useState(false);
  useEffect(() => {
    setUser();
  }, []);
  
  const setUser = async () => {
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setPhone(user.phone);
  }

  
/*
  useEffect(() => {
    /*const isFocused = props.navigation.isFocused();
    if (isFocused) {
      if (phone == '')
        setUser();
      else
        getData();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      if (phone == '')
        setUser();
      else
        getData();
    });

    return () => {
      navFocusListener.remove();
    };* /
    
    if (phone != '')
      getData();
    else 
      setState({ ...state, chatList: [], isLoading: false });
    
  }, [phone]); */
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return false;
    }
    return true;
  }

  useEffect(() => {
     const isFocused = props.navigation.isFocused();
    if (isFocused) { 
        getData();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => { 
        getData();
    });

    return () => {
      navFocusListener.remove();
    }; 
     
    
  }, []); 
/*
  useEffect(() => {
    const status = checkNet();
    if (status) { 
      getData();
    }
  }, [props.navigation.state]) */

  const getData = async () => {
    console.log("here")
  console.log(data)
      
  }
  const chatDetail = (url) => {

    let phone1 = phone; 
   // phone1 = '9176356733';

    if (url)
      props.navigation.navigate('ChatDetail', { url: url, phone:phone1 })
    else
      console.log('url', url)
  }
  const win = Dimensions.get('window');

  
  const updateTextInput = (value, field) => {
    let v_length = value.split("\n").length;
    if(v_length==0 || v_length==1 || value.trim() == "\n" ){
      v_length = 0;
    } 
    setState({ ...state, [field]: value,  error_message:"",  linebreaknumber: v_length });
  }
  const submitChatMessage = async () => {
    
    if(chat_message==""){
      setState({ ...state, error_message:"Please write your message." });
      return;
    }   
    setChatLoader(true);
    let data_url = "https://api2.frontapp.com/channels/cha_1l27/messages";
    const result = await createConversationApi(token, data_url, "cha_1l27", chat_message, phone ); 
    if(result){ 
      setTimeout(()=>{
        getData(); 
      }, 3000); 
    }
    console.log("result--",result);

  }
  const getReturnData = () => {  
      let a_chattext_len  = 2; 
      let chattextlength1 = chat_message.length;
      if(chattextlength1<=32) {
        a_chattext_len = 2;
      } else if(chattextlength1>32 && chattextlength1<65){
        a_chattext_len = 3;
      } else if(chattextlength1>65){
        a_chattext_len = 4;
      }

      a_chattext_len = linebreaknumber + a_chattext_len;
      if(a_chattext_len>3){
        a_chattext_len = 4;
      } 
      return {
        height: a_chattext_len*20
      }  
  }
  return (<>
    {isLoading ?
      <View style={[global_styles.activityIndicatorView,{	top: (Platform.OS=="web")?(win.height/2)-100:0 }]}>
        <ActivityIndicator size="large" style={{ padding: 60 }} />
      </View> :
      <SafeAreaView style={styles.container}> 
            <>
              {(chatList.length > 0) ?
                <KeyboardAwareScrollView >
                  <View style={{paddingHorizontal:20}}>
                        {chatList.map((item, index) => {
                        //  console.log("-------------->",item.recipient);
                          return (<View key={index} style={styles.chatView}>
                            <View style={styles.leftView}>
                              <View style={styles.chatHeader}>
                                <Text style={[styles.chatHeaderText,global_styles.lato_regular]}>{moment.unix(item.last_message.created_at).format('MM-DD-YYYY')}</Text>
                                <View style={styles.chatHeaderView}>
                                  <Text style={styles.chatHeaderText1}>{phone}</Text>
                                </View>
                              </View>
                              <Text style={[styles.chatMessageText,global_styles.lato_regular]}>{item.last_message.blurb}</Text>
                            </View>
                            <View style={styles.rightView}>
                              <TouchableHighlight underlayColor={{}} onPress={() => { chatDetail(item._links.related.messages) }}>
                                <Icon name={'right'} size={24} />
                              </TouchableHighlight>
                            </View>
                          </View>)
                        })}

                     
                  </View>
                </KeyboardAwareScrollView> : <ScrollView >
                  <View style={{paddingHorizontal:20, marginTop:20}}>
                    <Text style={styles.noCarText}>There are no messages found.</Text> 
                  </View>
                </ScrollView>}

                <View style={styles.bottomTextContainer}>  
                    <View style={styles.inputWrap}> 
                            <Textarea
                              containerStyle={[style_textarea.textareaContainer,getReturnData()]}
                                style={style_textarea.textarea}
                                placeholder={""} 
                                value={chat_message}
                                defaultValue={chat_message}
                                onChangeText={(text) =>  updateTextInput(text, 'chat_message')}  
                                placeholder={''}
                                placeholderTextColor={'#c7c7c7'}
                                underlineColorAndroid={'transparent'}
                              /> 
                         
                              <TouchableHighlight underlayColor='' onPress={submitChatMessage} style={styles.textbuttonSubmit} >
                                <Text style={styles.buttonText}>SEND</Text>
                              </TouchableHighlight>  
                          {chatloader && <ActivityIndicator size="small" style={{ padding: 10, alignSelf:"center" }} /> } 
                  </View>
                  {error_message != "" && <Text style={{color:"red",fontSize:14}}>{error_message}</Text> }
              </View>
            </> 
      </SafeAreaView>}
  </>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  noView: { 
  },
  noCarText: {
    fontSize: 16,
    marginBottom: 10
  },
  chatView: {
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderBottomColor: '#CCC',
    borderBottomWidth: 1
  },
  leftView: {
    flex: .9
  },
  rightView: {
    flex: .1,
    justifyContent:"center",
    alignItems:"center"
  },
  chatHeader: {
    flexDirection: "row",
  },
  chatHeaderText: {
    flex: .5,
    fontSize: 12,
  },
  chatHeaderView: {
    flex: .5,
    flexDirection: "row-reverse"
  },
  chatMessageText: {
    fontSize: 14,
    marginVertical: 5
  },
  bottomTextContainer:{
    width:"100%", 
    paddingVertical:10, 
    borderTopWidth:1,
    paddingHorizontal:10,
    borderTopColor:"#ccc"
  },
  inputWrap: { 
    marginTop:0,  
    flexDirection: "row",  
  },
  chatHeading:{
    marginBottom:5,
    marginTop:20
  },
  textbuttonSubmit_Wrapper: {
    flex: 1,
    flexDirection: "row-reverse", 
    marginTop:10,
  },
  textbuttonSubmit: {
    backgroundColor: Colors.color0,
    paddingVertical: 10,
    paddingHorizontal: 15, 
    marginHorizontal: 0,
    marginTop: 0,
    marginLeft:5,
    maxHeight:37,
    borderRadius: 5,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
});



const style_textarea = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'center',
    alignItems: 'center',
  },
  textareaContainer: {
    width: 240,
    height: 39,
    textAlignVertical: 'top', 
    padding: 1,
    fontSize:14,
    color:"#545454",  
    paddingHorizontal:8,
    borderWidth:1, 
    borderRadius:6,
    borderColor:"#ccc", 
  },
  textarea: {
    textAlignVertical: 'top', 
    fontSize: 14,
    color: '#333',
  },
});