import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Text,
  Platform,
  KeyboardAvoidingView,
  View,
  TouchableOpacity
} from 'react-native';
import { Header } from 'react-navigation-stack';

import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";
import { global_styles } from '../../constants/Style';
import { useQuery } from '@apollo/react-hooks';
import { CheckConnectivity } from '../Helper/NetInfo';
import Chat from '../Support/ChatScreen';

export default function ChatDetailScreen(props) {
  const [isLoading, setIsLoading] = useState(false);
  const [driverId, setDriverId] = useState('');
  const [reload, setReload] = useState(false);

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }
  
  useEffect(() => {
    checkNet();
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
    
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
     
      setuserid();
    });

    return () => {
        console.log("remove")
      navFocusListener.remove();
    };
  }, []);

  const setuserid = async () => {
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setDriverId("");
    setDriverId(user.id);
    setReload(!reload);
  }
  return (
    <>
      {isLoading ?
        <View >
          <ActivityIndicator/>
        </View> :
            
          <View  style={styles.container}>     
          
           {driverId!=''?<Chat {...props} driverId={driverId} />:null}
           
        </View>}
    </>
  );
}

const styles = StyleSheet.create({
  container:{
    flex:1,   
  }
});