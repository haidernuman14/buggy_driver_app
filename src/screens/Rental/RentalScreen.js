import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableOpacity,
  Dimensions,
  Image,
  Platform
} from 'react-native';
import { useMutation, useQuery } from '@apollo/react-hooks';
import analytics from '@react-native-firebase/analytics';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import moment from "moment";
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { CheckConnectivity } from '../Helper/NetInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import TransactionHistoryPopupCurrentCar from './transactionHistoryPopupCurrentCar';
import { Driver } from '../../graphql/driverBalanceCurrentRental';
import TransactionHistoryPopupRental from './transactionHistoryPopupRental';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ReservationPopupCurrentCar from '../Reservation/ReservationPopupCurrentCar';
import LaunchNavigator from 'react-native-launch-navigator';

/**
 * This class displays dashboard information.
 * Displays Pending Dues information with PAY NOW option.
 * Provides information of current rented cars under CURRENT RENTAL section. 
 * Provides history of all the rented cars under RENTAL HISTORY section.
 */
const win = Dimensions.get('window');
export default function RentalScreen(props) {

  const [state, setState] = useState({
    isLoading: true,
    currentAgreement: null,
    car_name: "",
    reloadRentalHistory: false,
    // openReservation: false

  });
  const [user, setUser] = useState({});
  const [driverId, setDriverId] = useState("");
  const [openReservation, setOpenReservation] = useState(false)
  const [openReservationCarInfo, setOpenReservationCarInfo] = useState(null)
  const { setStatusColor } = useContext(langContext);
  const { isLoading, car_name, currentAgreement, reloadRentalHistory } = state;

  // Check internet connection
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  // Setting up user detail
  const setuserid = async () => {

    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setDriverId(user.id);
    setUser(user);
    setState({ ...state, reloadRentalHistory: !reloadRentalHistory });
  }

  // Set the driver id from the storage
  useEffect(() => {

    checkNet();
    setStatusColor(Colors.account_status_bar_color);
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();
    });
    return () => {
      navFocusListener.remove();
    };
  }, []);

  const getDirectionoBrooklyn = () => {
    LaunchNavigator.navigate("445 Empire Blvd, Brooklyn, NY 11225")
      .then(() => console.log("Launched navigator"))
      .catch((err) => console.error("Error launching navigator: " + err));
  }
  const getDirectionoBronx = () => {
    LaunchNavigator.navigate("691 Burke Avenue, Bronx, NY 10467")
      .then(() => console.log("Launched navigator"))
      .catch((err) => console.error("Error launching navigator: " + err));
  }
  ///////////// Current Rental ///////////////
  const { data: drivar_data, loading: drivar_data_loading, error: drivar_data_error } = useQuery(Driver, {
    variables: { id: driverId },
    fetchPolicy: "network-only",
    skip: driverId == ''
  });

  const gotoCarScreen = async () => {
    await AsyncStorage.setItem('parent', "Rental");
    props.navigation.navigate('CarinitialScreen')
  }

  useEffect(() => {
    if (drivar_data_error) {
      // console.log("problemmmm.......")
    }

    //  console.log("DDDDDDDDDdrivar_data useEffect....")
    if (drivar_data) {
      // console.log("DDDDDDDDDdrivar_data", drivar_data);
      if (drivar_data && drivar_data.hasOwnProperty('driver')) {
        // console.log("DDDDDDDDDdrivar_data<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>", drivar_data);
        let c_agreement = "";
        AsyncStorage.setItem('rental_car_id', "");
        if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.currentAgreement != null) {

          AsyncStorage.setItem('rental_car_id', drivar_data.driver.currentAgreement.id);
          c_agreement = drivar_data.driver.currentAgreement;
        }
        else if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.reservationDriver.edges.length > 0 && drivar_data.driver.reservationDriver.edges[0].node.car != null) {
            console.log(drivar_data.driver.reservationDriver.edges)
          setOpenReservationCarInfo(drivar_data.driver.reservationDriver.edges)
          setOpenReservation(true)
        }
        if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.reservationDriver.edges.length > 0 && drivar_data.driver.reservationDriver.edges[0].node.car == null) {
          setOpenReservationCarInfo(drivar_data.driver.reservationDriver.edges)
          setOpenReservation(false)
        }
        else if (drivar_data && drivar_data.hasOwnProperty('driver') && drivar_data.driver.reservationDriver && drivar_data.driver.reservationDriver.edges.length == 0) {
          setOpenReservationCarInfo(drivar_data.driver.reservationDriver.edges)
          setOpenReservation(false)
        }

        else {
          let c_agreement = null;
          AsyncStorage.setItem('currentAgreement', JSON.stringify(c_agreement));
        }

        setDriverId("");
        let cname = "";
        if (c_agreement)
          cname = c_agreement.car.year + " " + c_agreement.car.color + " " + c_agreement.car.model;

        setState({ ...state, isLoading: false, currentAgreement: c_agreement, car_name: cname });

      }
    }
  }, [drivar_data]);
  ///////////// End Current Rental //////////////

  // console.log("currentAgreement<><>", currentAgreement);
  return (
    <View style={styles.main_container}>
      <>
        {isLoading ?
          <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
            <ActivityIndicator size="large" style={{ padding: 60 }} />
          </View> :
          <KeyboardAwareScrollView style={styles.info_main} >
            <>
              <View style={styles.currental_rental_container}>
                {(currentAgreement && currentAgreement != null && currentAgreement != "") ?
                  <Text style={[styles.head_title, global_styles.lato_medium]}>
                    {translate("current_rental")}
                  </Text>
                  : null}
                {(currentAgreement && currentAgreement != null && currentAgreement != "") ?
                  <TransactionHistoryPopupCurrentCar navigation={props.navigation} uid={user.id} currentAgreement={currentAgreement} navigation={props.navigation} />
                  : null}

                {(openReservationCarInfo && openReservation == false && currentAgreement == "" && openReservationCarInfo.length == 0) || (openReservationCarInfo &&  openReservationCarInfo.length > 0 && openReservation == false && currentAgreement == "" )?
                  <TouchableOpacity onPress={() => gotoCarScreen()}
                    style={{backgroundColor: "#1B4D7E", marginTop: 16, padding: 10, borderRadius: 5, alignItems: "center", justifyContent: "center" }}>
                    <Text style={[{ color: "white", textAlign: "center", fontSize: 16 }, global_styles.lato_regular_android]}>Ready to drive again?{"\n"}Choose your car now</Text>
                  </TouchableOpacity>:null}
                { openReservationCarInfo && openReservation == true && currentAgreement == "" && openReservationCarInfo[0].node.car != null ?
                  <View>
                    <Text style={[styles.head_title, global_styles.lato_medium]}>
                      Current Reservation
                  </Text>
                    <ReservationPopupCurrentCar navigation={props.navigation} uid={user.id} currentAgreement={openReservationCarInfo[0].node} navigation={props.navigation} />
                    <View style={styles.view_car_btn}>
                      <TouchableOpacity underlayColor="" activeOpacity={0.8} style={styles.cardocumentbutton} onPress={() => getDirectionoBrooklyn()}  >
                        <Text style={[styles.buttonText, global_styles.lato_regular]}>DIRECTIONS</Text>
                      </TouchableOpacity>
                    </View>

                  </View> :
                  null}
                {(currentAgreement) ?
                  <View style={styles.view_car_btn}>
                    <TouchableOpacity underlayColor="" activeOpacity={0.8} style={styles.cardocumentbutton} onPress={() => props.navigation.navigate('TransactionHistory')}  >
                      <Text style={[styles.buttonText, global_styles.lato_regular]}>TRANSACTIONS</Text>
                    </TouchableOpacity>
                  </View> : <></>}
              </View>

              {(currentAgreement && currentAgreement != null && currentAgreement != "") ?
                <View style={[styles.return_car_wrap]}>

                  <View style={styles.return_car}>
                    <TouchableOpacity underlayColor="" activeOpacity={0.8} style={{
                      backgroundColor: "#fff",
                      borderColor: "#1B4D7E",
                      padding: 5,
                      borderWidth: 1,
                      borderRadius: 4,
                    }}
                      onPress={() => props.navigation.navigate('Returncarwrapper')} >
                      <View style={{ flexDirection: "row", }}>
                        <Image width="36" style={{ height: 40, width: 40, resizeMode: "contain" }} source={require("../../assets/images/needtoreturnacar.png")} />
                        <Text style={[{ width: 100, marginLeft: 10, marginRight: 10, fontSize: 16 }, global_styles.lato_regular]}>SCHEDULE A RETURN</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity underlayColor="" activeOpacity={0.8}
                    style={{
                      backgroundColor: "#db9360",
                      padding: 5,
                      marginLeft: 5,
                      borderRadius: 4,
                      shadowColor: '#000',
                      shadowOffset: { width: 0, height: 1 },
                      shadowOpacity: 0.2,
                    }}

                    onPress={() => props.navigation.navigate('CarScreen')}  >
                    <View style={{ flexDirection: "row", flex: 1 }}>
                      <Image width="36" style={{ height: 40, width: 40, resizeMode: "contain", tintColor: "white" }} source={require("../../assets/images/needtoreturnacar.png")} />
                      <Text style={[{ width: 100, marginLeft: 10, marginRight: 10, fontSize: 16, textAlign: "right", color: "white" }, global_styles.lato_regular]}>SWITCH YOUR CAR</Text>
                    </View>
                  </TouchableOpacity>
                </View> : <></>}

              <View style={styles.rental_screen} key={reloadRentalHistory} >
                <Text style={[styles.head_title, global_styles.lato_medium]}>
                  {translate("rental_history")}
                </Text>
                <View style={{ borderTopWidth: 1, borderTopColor: "#f5f5f5", flex: 1 }}>
                  <TransactionHistoryPopupRental navigation={props.navigation} driverId={user.id} />
                </View>
              </View>

            </>
          </KeyboardAwareScrollView>}


      </>
    </View>
  );
}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1
  },
  info_main: {
    backgroundColor: "#f9f9f4",
  },
  icon_input_2: {
    width: 36,
    resizeMode: "contain"
  },
  return_car: {

    marginRight: 10
  },
  currental_rental_container: {
    backgroundColor: "#fff",
    paddingVertical: 10,
    paddingTop: 0,
    margin: 10
  },
  return_car_title: {
    fontSize: 16,
    color: "#393E5C"
  },
  return_car_title_text: {
    fontSize: 14,
    color: "#393E5C"
  },
  return_car_link_text: {
    color: "#2dafd3",
  },
  rental_screen: {
    margin: 10,
    marginTop: 0,
    marginBottom: 0,
    backgroundColor: "#fff"
  },
  return_car_wrap: {
    marginTop: 0,

    flexDirection: "row",
    justifyContent: "center",
    flex: 1,
    paddingVertical: 10,
    marginRight: 20,
    marginLeft: 20,
    paddingTop: 0,
    margin: 10
  },
  view_car_btn: {
    flexDirection: "row",
    flex: 1,
    justifyContent: 'flex-end',
    marginRight:15

  },
  cardocumentbutton: {

    backgroundColor: "#fff",
    borderColor: "grey",
    borderWidth: 1,
    borderRadius: 4,
  },
  cardocumentbutton_payment: {
    borderColor: "#db9360"
  },
  cardocumentbutton_tickets: {
    borderColor: "#db9360"
  },
  buttonText: {
    color: "#393e5c",
    fontSize: 14,
    alignSelf: "center",
    flexDirection: "row",
    paddingVertical: 7,
    paddingHorizontal: 8,
  },
  head_title: {
    color: '#000',
    fontSize: 16,
    paddingTop: 25,
    padding: 15,
    paddingBottom: 10,
    marginTop: 0,
    justifyContent: "flex-start",
    marginBottom: 0,
    backgroundColor: "#fff",
  },
});
