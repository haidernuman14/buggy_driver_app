import React, { useState, useEffect, useContext } from 'react';
import {
    StyleSheet,
    Text,
    ActivityIndicator,
    View,
    TouchableOpacity,
    Dimensions,
    Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { pickerSelectStyles, global_styles } from '../../constants/Style';
import RadionOff from 'react-native-vector-icons/Ionicons';
import RadionOn from 'react-native-vector-icons/MaterialIcons';
import Calendar from 'react-native-vector-icons/Feather';
import CheckboxChecked from 'react-native-vector-icons/Ionicons';
import CheckboxUnChecked from 'react-native-vector-icons/MaterialCommunityIcons';
import CalendarPicker from 'react-native-calendar-picker';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

let date = new Date();
let day = date.getDay();
let minDay = (day == 6) ? 1 : 0;
let maxDay = day;

export default function TransactionFilterScreen(props) {

    const [state, setState] = useState({
        isCredits: false,
        isDebits: true,
        isWeeklyRental: true,
        isTicketsTolls: false,
        isCreditCardFees: false,
        isDamage: false,
        isCleaning: false,
        startDate: 'MM/DD/YYYY',
        startDateDB: '',
        endDateDB: '',
        endDate: 'MM/DD/YYYY',
        minDate: moment().add(minDay, 'day'),
        maxDate: moment().add(maxDay, 'day'),
        isStartCalendar: false,
        isEndCalendar: false,
    });

    const { endDateDB, startDateDB, isCredits, isDebits, isWeeklyRental, isTicketsTolls, isCreditCardFees, isDamage, isCleaning, minDate, maxDate, isStartCalendar, isEndCalendar, startDate, endDate } = state

    const onStartDateChange = (date) => {
        let setDate = moment(date).format('YYYY-MM-DD');
        let setDisplayDate = moment(date).format('MM/DD/YYYY');
        setState({ ...state, startDateDB: setDate, startDate: setDisplayDate, isStartCalendar: false })
    }

    const onEndDateChange = (date) => {
        let setDate = moment(date).format('YYYY-MM-DD');
        // console.log("setDate<><>", setDate);
        let setDisplayDate = moment(date).format('MM/DD/YYYY');
        setState({ ...state, endDateDB: setDate, endDate: setDisplayDate, isEndCalendar: false })
    }

    useEffect(() => {

        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            onConstPrev();
        }

        const navFocusListener = props.navigation.addListener('didFocus', () => {
            onConstPrev();
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);

    const onConstPrev = async () => {


        //   await AsyncStorage.setItem('amount_type','');
        // await AsyncStorage.setItem('start_date','');
        // await AsyncStorage.setItem('end_date','');
        //  await AsyncStorage.setItem('cat_name',''); 

        let debitStatus = true
        let creditStatus = false

        let amount_type = await AsyncStorage.getItem('amount_type');
        debitStatus = await AsyncStorage.getItem('is_debit') == 1;
        creditStatus = await AsyncStorage.getItem('is_credit') == 1;
        let start_date = await AsyncStorage.getItem('start_date');
        let end_date = await AsyncStorage.getItem('end_date');
        let cat_name = await AsyncStorage.getItem('cat_name');
        let cname = JSON.parse(cat_name);

        let isWeeklyRental1 = false;
        let isTicketsTolls1 = false;
        let isCreditCardFees1 = false;
        let isCleaning1 = false;
        let isDamage1 = false;
        cname.map((ind) => {


            if (ind == 0) {
                isWeeklyRental1 = true;
            }
            if (ind == 2) {
                isTicketsTolls1 = true;
            }
            if (ind == 13) {
                isCreditCardFees1 = true
            }
            if (ind == 4) {
                isDamage1 = true
            }
            if (ind == 14) {
                isCleaning1 = true
            }

        });


        let start_date_format = "MM/DD/YYYY";
        if (start_date != "" && start_date != null) {
            start_date_format = moment.utc(start_date).format("MM/DD/YYYY");
        }

        let end_date_format = "MM/DD/YYYY";
        if (end_date != "" && end_date != null) {
            end_date_format = moment.utc(end_date).format("MM/DD/YYYY");
        }

        setState({ ...state, isWeeklyRental: isWeeklyRental1, isTicketsTolls: isTicketsTolls1, isCreditCardFees: isCreditCardFees1, isDamage: isDamage1, isCleaning: isCleaning1, isCredits: creditStatus, isDebits: debitStatus, endDateDB: end_date, endDate: end_date_format, startDateDB: start_date, startDate: start_date_format })

    }

    const clear = async () => {

        let cat_name = [];


        let amount_type1 = "";
        let startDateDB1 = "";
        let endDateDB1 = "";


        await AsyncStorage.setItem('amount_type', amount_type1);
        await AsyncStorage.setItem('start_date', startDateDB1);
        await AsyncStorage.setItem('end_date', endDateDB1);
        await AsyncStorage.setItem('cat_name', JSON.stringify(cat_name));

        let transaction_f = await AsyncStorage.getItem('transaction_f');
        if (transaction_f == "yes") {
            AsyncStorage.setItem('transaction_f', "no");
        } else {
            AsyncStorage.setItem('transaction_f', "yes");
        }

        props.navigation.navigate('TransactionHistory');


    }

    const onSaveFilter = async () => {

        let cat_name = [];
        if (isWeeklyRental) {
            cat_name.push(0);
        }
        if (isTicketsTolls) {
            cat_name.push(2);
        }
        if (isCreditCardFees) {
            cat_name.push(13);
        }
        if (isDamage) {
            cat_name.push(4);
        }
        if (isCleaning) {
            cat_name.push(14);
        }

        let amount_type1 = ''
        if (isDebits && isCredits) {
            amount_type1 = ''
        } else if (isDebits) {
            amount_type1 = 'debit'
        } else if (isCredits) {
            amount_type1 = 'credit'
        }
        // let amount_type1 = (isDebits) ? "debit" : "credit";
        let startDateDB1 = (startDateDB != "" && startDateDB != null) ? startDateDB : "";
        let endDateDB1 = (endDateDB != "" && endDateDB != null) ? endDateDB : "";


        await AsyncStorage.setItem('amount_type', amount_type1);
        await AsyncStorage.setItem('is_debit', isDebits ? '1' : '0');
        await AsyncStorage.setItem('is_credit', isCredits ? '1' : '0');
        await AsyncStorage.setItem('start_date', startDateDB1);
        await AsyncStorage.setItem('end_date', endDateDB1);
        await AsyncStorage.setItem('cat_name', JSON.stringify(cat_name));

        let transaction_f = await AsyncStorage.getItem('transaction_f');
        if (transaction_f == "yes") {
            AsyncStorage.setItem('transaction_f', "no");
        } else {
            AsyncStorage.setItem('transaction_f', "yes");
        }

        props.navigation.navigate('TransactionHistory');

    }
    return (
        <View style={styles.main_container}>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}><>
                <View style={{ marginHorizontal: 22, marginTop: 22 }}>
                    <Text style={[styles.title, global_styles.lato_medium]}>Transaction Type</Text>
                    <View style={styles.radio_container}>
                        <TouchableOpacity onPress={() => setState({ ...state, isCredits: !isCredits })}>
                            <View style={styles.radio_item_container}>
                                {isCredits ? <CheckboxChecked style={styles.vector_icon} name='checkbox-outline' /> : <CheckboxUnChecked style={styles.vector_icon} name='checkbox-blank-outline' />}
                                {/* {isCredits ? <RadionOn style={styles.vector_icon} name='radio-button-checked' /> : <RadionOff style={styles.vector_icon} name='radio-button-off' />} */}
                                <View style={{ marginStart: 8 }}><Text style={[styles.item, global_styles.lato_regular]}>Credits</Text></View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setState({ ...state, isDebits: !isDebits })}>
                            <View style={styles.radio_item_container}>
                                <View style={{ marginStart: 16 }}>{isDebits ? <CheckboxChecked style={styles.vector_icon} name='checkbox-outline' /> : <CheckboxUnChecked style={styles.vector_icon} name='checkbox-blank-outline' />}</View>
                                {/* <View style={{ marginStart: 16 }}>{isDebits ? <RadionOn style={styles.vector_icon} name='radio-button-checked' /> : <RadionOff style={styles.vector_icon} name='radio-button-off' />}</View> */}
                                <View style={{ marginStart: 8 }}><Text style={[styles.item, global_styles.lato_regular]}>Debits</Text></View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ marginTop: 32, marginStart: 22, }}><Text style={[styles.title, global_styles.lato_medium]}>Date Range</Text></View>
                <View style={styles.date_range_container}>
                    <Text style={[styles.date_label, global_styles.lato_regular]}>START DATE</Text>
                    <TouchableOpacity onPress={() => { setState({ ...state, isStartCalendar: !isStartCalendar, isEndCalendar: false }) }}>
                        <View style={styles.date_container}>
                            <Text style={[styles.date, global_styles.lato_regular]}>{startDate}</Text>
                            <Calendar style={styles.vector_icon} name='calendar' />
                        </View>
                    </TouchableOpacity>
                    {isStartCalendar && <View style={styles.calendar_container}>
                        <CalendarPicker
                            // minDate={minDate}
                            maxDate={maxDate}
                            disabledDates={(date) => {
                                let type = date.day();
                                return (type == 6) ? date : '';
                            }}
                            // selectedStartDate={startDate}
                            onDateChange={(value) => onStartDateChange(value)}
                        />
                    </View>}
                    <View style={{ marginTop: 16 }}><Text style={[styles.date_label, global_styles.lato_regular]}>END DATE</Text></View>
                    <TouchableOpacity onPress={() => { setState({ ...state, isEndCalendar: !isEndCalendar, isStartCalendar: false }) }}>
                        <View style={styles.date_container}>
                            <Text style={[styles.date, global_styles.lato_regular]}>{endDate}</Text>
                            <Calendar style={styles.vector_icon} name='calendar' />
                        </View>
                    </TouchableOpacity>
                    {isEndCalendar && <View style={styles.calendar_container}>
                        <CalendarPicker
                            // minDate={minDate}
                            maxDate={maxDate}
                            disabledDates={(date) => {
                                let type = date.day();
                                return (type == 6) ? date : '';
                            }}
                            // selectedStartDate={startDate}
                            onDateChange={(value) => onEndDateChange(value)}
                        />
                    </View>}
                </View>
                <View style={{ padding: 22, }}>
                    <View style={{ marginTop: 32 }}><Text style={[styles.title, global_styles.lato_medium]}>Categories</Text></View>
                    <View style={{ marginTop: 16 }}></View>
                    <TouchableOpacity onPress={() => setState({ ...state, isWeeklyRental: !isWeeklyRental })}>
                        <View style={styles.checkboxes_container}>
                            {isWeeklyRental ? <CheckboxChecked style={styles.vector_icon} name='checkbox-outline' /> : <CheckboxUnChecked style={styles.vector_icon} name='checkbox-blank-outline' />}
                            <Text style={[styles.checkbox_title, global_styles.lato_regular]}>Weekly Rental</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setState({ ...state, isTicketsTolls: !isTicketsTolls })}>
                        <View style={styles.checkboxes_container}>
                            {isTicketsTolls ? <CheckboxChecked style={styles.vector_icon} name='checkbox-outline' /> : <CheckboxUnChecked style={styles.vector_icon} name='checkbox-blank-outline' />}
                            <Text style={[styles.checkbox_title, global_styles.lato_regular]}>Tickets & Tolls</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setState({ ...state, isCreditCardFees: !isCreditCardFees })}>
                        <View style={styles.checkboxes_container}>
                            {isCreditCardFees ? <CheckboxChecked style={styles.vector_icon} name='checkbox-outline' /> : <CheckboxUnChecked style={styles.vector_icon} name='checkbox-blank-outline' />}
                            <Text style={[styles.checkbox_title, global_styles.lato_regular]}>Credit Card Fees</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setState({ ...state, isDamage: !isDamage })}>
                        <View style={styles.checkboxes_container}>
                            {isDamage ? <CheckboxChecked style={styles.vector_icon} name='checkbox-outline' /> : <CheckboxUnChecked style={styles.vector_icon} name='checkbox-blank-outline' />}
                            <Text style={[styles.checkbox_title, global_styles.lato_regular]}>Damage</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setState({ ...state, isCleaning: !isCleaning })}>
                        <View style={styles.checkboxes_container}>
                            {isCleaning ? <CheckboxChecked style={styles.vector_icon} name='checkbox-outline' /> : <CheckboxUnChecked style={styles.vector_icon} name='checkbox-blank-outline' />}
                            <Text style={[styles.checkbox_title, global_styles.lato_regular]}>Cleaning</Text>
                        </View>
                    </TouchableOpacity>

                    <View style={global_styles.button_container}>
                        <TouchableOpacity onPress={clear}>
                            <View style={global_styles.button_style_white_wrapper}>
                                <Text style={global_styles.button_style_white_text}>CLEAR & SAVE</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{ marginStart: 16 }} />
                        <TouchableOpacity onPress={onSaveFilter}>
                            <View style={global_styles.bottom_style_orange_wrapper}>
                                <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>SAVE</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
            </></KeyboardAwareScrollView>
        </View>
    )

}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
    },
    title: {
        fontSize: 18,
        color: '#000',
    },
    item: {
        fontSize: 16,
        color: '#000',
        opacity: 0.6,
    },
    radio_container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 16,
    },
    radio_item_container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    date_label: {
        fontSize: 14,
        color: '#000',
        opacity: 0.6,
        alignSelf: 'stretch',
        marginStart: 22,
    },
    date: {
        fontSize: 16,
        color: '#000',
        opacity: 0.6,
    },
    date_range_container: {
        marginTop: 16,
    },
    date_container: {
        backgroundColor: '#fff',
        borderColor: 'rgb(212, 212, 212)',
        borderRadius: 8,
        borderWidth: 1,
        paddingVertical: 8,
        paddingHorizontal: 12,
        flex: 1,
        alignSelf: 'stretch',
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 22,
    },
    checkboxes_container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginStart: 8,
        marginTop: 12,
    },
    checkbox_title: {
        marginStart: 12,
        fontSize: 16,
        color: '#000',
        opacity: 0.6,
    },
    vector_icon: {
        fontSize: 26,
    },
    save_container: {
        alignSelf: 'flex-end',
        marginTop: 32,
        backgroundColor: 'rgb(219, 147, 96)',
        width: 140,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
    },
    save: {
        color: '#fff',
        fontSize: 16,
    },
    calendar_container: {
        marginTop: 8,
    },
})