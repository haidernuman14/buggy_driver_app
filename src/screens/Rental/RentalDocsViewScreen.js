import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  SafeAreaView
} from 'react-native';
import { global_styles } from '../../constants/Style';
//import FileViewer from 'react-native-file-viewer';
import { WebView } from 'react-native-webview';

export default function RentalDocsViewScreen(props) {

  const [path, setPath] = useState('');
  const [web_loader, setWeb_loader] = useState(true);

  useEffect(() => {
    if (props.navigation.state.params.path) {
      setPath(props.navigation.state.params.path)
    }
  }, [props.navigation.state])
  
  const hideSpinner = () => {
    setWeb_loader(false)
  }
  return (
    <SafeAreaView style={{flex:1}}>
      {web_loader && <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View>
        }
      {path!='' && <WebView onLoad={() => hideSpinner()}
      source={{ uri: path }} />}
    </SafeAreaView>
  );
}