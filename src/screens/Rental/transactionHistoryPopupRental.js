import React, { useEffect, useState } from 'react'; //Unused import function "useEffect"
import {
  ActivityIndicator,
  StyleSheet,
  ScrollView, //Unused import function "ScrollView"
  Text,
  Image, //Unused import function "Image"
  View,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import moment from "moment";
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { global_styles } from '../../constants/Style';
import { DriverAllRentals } from '../../graphql/driverAllRentals';
import { useMutation, useQuery } from '@apollo/react-hooks';

/**This class is used to design the dashboard cards layout for rental history of rented cars*/
export default function TransactionHistoryPopupRental(props) {

  const [state, setState] = useState({
    isLoading: true,
    show: false,
    rental_data: {},
    rental_data_pageinfo: {},
    load_more_loader: false,
    fresh_flag: true,
  });
  const { isLoading, fresh_flag, load_more_loader, show, rental_data_pageinfo, rental_data } = state;
  //get data of the selected item

  const { data: drivar_data, loading: drivar_data_loading, error: drivar_data_error, fetchMore } = useQuery(DriverAllRentals, {
    variables: { orderBy: ["-start_date"], driverId: props.driverId, first: 3 },
    fetchPolicy: "network-only"
  });

  useEffect(() => {
    if (drivar_data) {
      if (drivar_data && drivar_data.hasOwnProperty('allRentals') && drivar_data.allRentals.edges.length > 0) {
        if (fresh_flag) {
          setState({ ...state, isLoading: false, load_more_loader: false, rental_data_pageinfo: drivar_data.allRentals.pageInfo, rental_data: drivar_data.allRentals.edges });
        } else {
          setState({ ...state, isLoading: false, load_more_loader: false, rental_data_pageinfo: drivar_data.allRentals.pageInfo, rental_data: [...rental_data, ...drivar_data.allRentals.edges] });
        }
      } else {
        setState({ ...state, isLoading: false });
      }
    }
  }, [drivar_data]);

  const handelMoreTransaction = (id) => {
    setState({ ...state, fresh_flag: false, load_more_loader: true });
    fetchMore({
      variables: {
        driverId: props.driverId,
        first: 3,
        after: id
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        return fetchMoreResult;
      }
    });
  }
  // console.log("rental_data_pageinfo.hasNextPage---->", rental_data_pageinfo.hasNextPage);
  return (
    <>
      {isLoading ?
        <View style={global_styles.activityIndicatorView}>
          <ActivityIndicator size="small" style={{ padding: 100, paddingBottom: 200 }} />
        </View> :
        <View >

          {rental_data && rental_data.length > 0 ? <>

            {/* rental_data.map((rentalitem) =>    <> 
                <View style={styles.billing_list_item_wrap} >
                  <View style={styles.billing_list_item} >
                    <View style={styles.billing_list_title_left_icon}> 
                      <Image width="50" style={styles.icon_input} source={require("../../assets/images/car-icon.png")} />
                    </View>
                    <View style={styles.billing_list_title_left} >
                      <TouchableHighlight underlayColor="" onPress={() => props.navigation.navigate('RentalDocs', { carId: rentalitem.node.car.pk, car_name: rentalitem.node.car.year+" "+rentalitem.node.car.color+" "+rentalitem.node.car.model, flag:"rental", time: Date.now() }) } >
                        <Text style={[styles.billing_list_title, global_styles.lato_semibold]}>{rentalitem.node.car.year} {rentalitem.node.car.color} {rentalitem.node.car.model}</Text>
                      </TouchableHighlight>
                      <View style={styles.view_detail}>
                          <Text style={[styles.heading_item_date, global_styles.lato_regular]}>Start Date: {moment(rentalitem.node.startDate).format("MMM DD, YYYY")}</Text>
                          <Text style={[styles.heading_item_date, global_styles.lato_regular]}>End Date: {(moment(rentalitem.node.endDate).isValid())?moment(rentalitem.node.endDate).format("MMM DD, YYYY"):'-'}</Text>
                      </View>
                    </View>
                    <View style={styles.billing_list_price_right} >
                      <Text style={[styles.billing_list_price_right_text, global_styles.lato_semibold]}>${rentalitem.node.weeklyCharge}/Week</Text>
                    </View>
                  </View> 
                </View>  
              </>)
           */}
            {rental_data.map((rentalitem) => <>
              <View style={styles.billing_list_item_wrap} >
                <View style={styles.billing_list_item} >
                  <View style={styles.billing_list_title_left_icon}>
                    <Image width="50" style={styles.icon_input} source={require("../../assets/images/car-icon.png")} />
                  </View>
                  <View style={styles.billing_list_title_left} >
                    {/* <TouchableHighlight underlayColor="" onPress={() => props.navigation.navigate('RentalDocs', { carId: rentalitem.node.car.pk, car_name: rentalitem.node.car.year + " " + rentalitem.node.car.color + " " + rentalitem.node.car.model, flag: "rental", time: Date.now() })} > */}
                    <TouchableHighlight underlayColor=""  >
                      <Text style={[styles.billing_list_title, global_styles.lato_semibold]}>{rentalitem.node.car.year} {rentalitem.node.car.color} {rentalitem.node.car.model}</Text>
                    </TouchableHighlight>
                    <View style={styles.view_detail}>
                      {/* <TouchableHighlight underlayColor="" onPress={() => props.navigation.navigate('RentalDocs', { carId: rentalitem.node.car.pk, car_name: rentalitem.node.car.year+" "+rentalitem.node.car.color+" "+rentalitem.node.car.model, flag:"rental", time: Date.now() }) } > */}
                      <TouchableHighlight underlayColor="" >
                        <>
                          <Text style={[styles.heading_item_date, global_styles.lato_regular]}>Start Date: {moment(rentalitem.node.startDate).format("MMM DD, YYYY")}</Text>
                          <Text style={[styles.heading_item_date, global_styles.lato_regular]}>End Date: {(moment(rentalitem.node.endDate).isValid()) ? moment(rentalitem.node.endDate).format("MMM DD, YYYY") : '-'}</Text>
                        </>
                      </TouchableHighlight>
                    </View>
                  </View>
                  <View style={styles.billing_list_price_right} >
                    <Text style={[styles.billing_list_price_right_text, global_styles.lato_semibold]}>${rentalitem.node.weeklyCharge}/Week</Text>
                  </View>
                </View>
              </View>
            </>)
            }
            {rental_data_pageinfo.hasNextPage == true &&
              <View style={styles.view_loadmore_detail}>
                <TouchableOpacity underlayColor="" activeOpacity={0.8} style={styles.textbuttonSubmit} onPress={() => handelMoreTransaction(rental_data_pageinfo.endCursor)}  >
                  <Text style={[styles.buttonText, global_styles.lato_regular]}>SHOW MORE</Text>
                </TouchableOpacity>
              </View>
            }
            {load_more_loader ? <View><ActivityIndicator size="small" style={{ padding: 0, marginTop: -20 }} /></View> : <></>}
          </> : <View style={[styles.padding_hr]}><Text style={global_styles.lato_regular}>No Rental history exists.</Text></View>
          }
        </View>
      }
    </>
  );
}

//StyleSheet for Rental History card layout
const styles = StyleSheet.create({
  hide: {
    display: "none"
  },
  padding_hr: {
    paddingHorizontal: 15,
    paddingVertical: 10
  },
  view_loadmore_detail: {
    alignItems: "flex-end",
    marginVertical: 15
  },
  icon_input: {
    width: 50,
    height: 49,
  },
  row_item: {
    borderBottomWidth: 1,
    borderColor: "#f1f1f1",
    paddingVertical: 8,
    borderRadius: 5,
  },
  list_item_price: {
    position: "absolute",
    right: 26,
    top: 10,
    fontSize: 12,
    color: "#454545"
  },
  list_item_date: {
    marginTop: 5,
    fontSize: 12,
    color: "#7d7d7d",
  },
  heading_item_title: {
    fontSize: 14,
    color: "#454545"
  },
  heading_item_date: {
    fontSize: 12,
    color: "#393E5C"
  },
  billing_list_price_right_text: {
    color: "#393E5C"
  },
  view_detail: {
    paddingTop: 0,
    marginBottom: 5,
    marginLeft: 2
  },
  row_wrapper: {
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  sub_item_heading: {
    paddingBottom: 10,
    marginTop: 20,
    paddingHorizontal: 15,
  },
  banner_billing_link: { // Defined but not used please remove this.
    color: '#3d82eb',
    fontSize: 14,
    textDecorationLine: "underline",
  },
  lato_bold: {  // Defined but not used please remove this.
    fontFamily: 'OpenSans-Bold',
    fontWeight: '700',
    fontStyle: 'normal'
  },
  lato_regular: {
    fontFamily: 'OpenSans',
    fontWeight: '400',
    fontStyle: 'normal'
  },
  lato_semibold: {
    fontFamily: 'OpenSans-SemiBold',
    fontWeight: '600',
    fontStyle: 'normal'
  },
  billing_list_item_wrap: {
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginVertical: 10,
    marginBottom: 0,
    paddingLeft: 10,
    borderTopWidth: 0,
    borderTopColor: "#f5f5f5",
    borderBottomWidth: 1,
    paddingVertical: 15,
    paddingTop: 10,
    borderBottomColor: "#f5f5f5",
    zIndex: 10,
  },
  billing_list_item: {
    backgroundColor: '#fff',
    flexDirection: "row",
    alignContent: "flex-start",
  },
  billing_list_title_left_icon: {
    width: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
  },
  billing_list_title_left: {
    width: "60%",
    marginLeft: 17,
  },
  billing_list_price_right: {
    position: "absolute",
    right: 0,
    top: 0,
  },
  billing_list_title: {
    color: '#393E5C',
    fontSize: 14,
    fontWeight: "bold",
  },
  textbuttonSubmit: {
    marginVertical: 10,
    minWidth: 120,
    marginRight: 10,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#ccc"
  },
  buttonText: {
    color: "#454545",
    fontSize: 13,
    alignSelf: "center",
    flexDirection: "row",
    paddingVertical: 8,
    borderRadius: 4,
    paddingHorizontal: 20,
  },
  button_view_plus: {
    position: "absolute",
    right: 10,
    top: 20,
    fontSize: 18,
    color: "#7d7d7d",
  },
  button_view_icon: {
    fontSize: 18,
    marginRight: 5,
    color: "#7d7d7d",
    justifyContent: "center"
  },
});
