import React, { useEffect, useState } from 'react';
import {
  Platform,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
  TouchableHighlight,
  Text,
  Image,
  Linking,
  View,
  Dimensions
} from 'react-native';
import { translate } from '../../components/Language';
import Colors from '../../constants/Colors';
import { CheckConnectivity } from '../Helper/NetInfo';
import { global_styles } from '../../constants/Style';
import { useQuery } from '@apollo/react-hooks';
import { carDocument } from '../../graphql/carDocument';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScreenTitle } from '../Helper/HeaderIcon';

const win = Dimensions.get('window');
function RentalDocsScreen(props) {

  const [state, setState] = useState({
    isLoading: true,
    list: [],
  });
  const [skip, setSkip] = useState(true); 
  const [carid, setCarID] = useState(0); 
  const { isLoading, list } = state;
  const { data, loading, error } = useQuery(carDocument, {
    variables: { id: carid },// 2146}, //
    skip: (skip && carid==0) 
  });
  useEffect(() => {
    checkNet();
    if (props.navigation.state) {
      if(props.navigation.state.params.carId != null && props.navigation.state.params.carId != "" && props.navigation.state.params.carId != 0 && props.navigation.state.params.carId != "0"){
        setCarID(props.navigation.state.params.carId);
      } else {
        setState({ ...state, isLoading: false })
      }
      
      setSkip(false);
    }
  }, [props.navigation.state]);

  // console.log(carid,skip);

  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }
  useEffect(() => {
    if (data && data.hasOwnProperty('car') && data.car != null && data.car.hasOwnProperty('cardocumentSet') && data.car.cardocumentSet != null && data.car.cardocumentSet.edges.length > 0) {
      setState({ ...state, isLoading: false, list: data.car.cardocumentSet.edges })
      setSkip(true);
    } else if ((data &&  data.hasOwnProperty('car') && data.car != null && data.car.hasOwnProperty('cardocumentSet') && data.car.cardocumentSet ==null) || (data &&  data.hasOwnProperty('car') && data.car != null && data.car.hasOwnProperty('cardocumentSet') && (data.car.cardocumentSet.edges.length ==null || data.car.cardocumentSet.edges.length ==0)) ) {
      setState({ ...state, isLoading: false })
      setSkip(true);
    }   
  }, [data])

  const openDocument = (url, name) => {
    let path = url.replace("&export=download", "");
    props.navigation.navigate('RentalDocsView', { path: path, name: name });
    return;
  }
  const getDocumentImage = (dtype) => {
     
    if(dtype == "REGISTRATION"){
        return require("../../assets/images/tthumb2.png");
    }
    if(dtype == "DMV_STICKER"){
      return require("../../assets/images/dmv-inspection-sticker.png");
    } 
    if(dtype == "CERT"){
      return require("../../assets/images/certificate-of-liability-dr.png");
    }
    if(dtype == "CERT" || dtype == "FH1"){
      return require("../../assets/images/document_icon_fh1.png");
    }
    if(dtype == "DIAMOND"){
      return require("../../assets/images/diamond-sticker.png");
    }
    return require("../../assets/images/tthumb2.png");
  } 
  return (
    <View style={styles.info_main} >
      <> 
        {isLoading ?
          <View style={global_styles.activityIndicatorView}>
            <ActivityIndicator size="large" style={{ padding: 60 }} />
          </View> : <>
          <ScrollView  > 

              {
                props.navigation.state.params.car_name != null ?
                  <>
                    <View style={[styles.carname_wrapper]}>
                      <Text style={[styles.carname_text, global_styles.lato_medium]} >Documents for {props.navigation.state.params.car_name}</Text>
                    </View>
                    {list && list.length > 0 ? 
                        <View style={[styles.padding_hr]}>
                          <View style={styles.view_more_transactions} >
                            <View style={styles.view_more_transactions_border} >
                              <View style={(list.length==1)?styles.row_wrapper_1:styles.row_wrapper}>
                                {list.map((dataitem) =>
                                      <TouchableHighlight underlayColor={{}} style={styles.item_box} onPress={() => openDocument(dataitem.node.documentUrl, dataitem.node.name)}>
                                      <>
                                          <Image width="150" style={styles.icon_input_thumb} source={getDocumentImage(dataitem.node.documentType.typeName)} />
                                        </>
                                      </TouchableHighlight> 
                                  )} 
                              </View>
                            </View>
                          </View>
                        </View>
                        :
                      <View style={[styles.no_container, styles.padding_hr]}><Text style={global_styles.lato_regular}>No document exists.</Text></View>
                    }
                </>:
                <View style={[styles.no_container, styles.padding_hr]}><Text style={global_styles.lato_regular}>No car is booked in your account.</Text></View>
              }
              
              </ScrollView>
      {/* 
              <View style={[styles.rating_section_wrap]}>
                  <View style={styles.rating_section_icon}>
                      <Icon size={36}  name="star-outline" />
                  </View>
                  <View style={styles.rating_section}>
                    <Text style={[styles.enjoying_buggy, global_styles.lato_semibold]} >Enjoying Buggy</Text>
                    <TouchableHighlight underlayColor={{}} onPress={() => Linking.openURL(Platform.OS === 'ios'?"https://apps.apple.com/us/app/id1519736744":"https://play.google.com/store/apps/details?id=com.joinbuggy.driverapp") }>
                      <Text style={[styles.rate_us_on_google, global_styles.lato_regular]} >Rate us on Google</Text>
                    </TouchableHighlight> 
                  </View> 
                </View>
        */}
          </>}
      </>
    </View>
  );
}

RentalDocsScreen.navigationOptions = ({ navigation }) => { 
  return ({
    headerTitle:(navigation.state.params.flag=="rental")?ScreenTitle('Past Documents'):ScreenTitle('Current Documents'),
  })
};
export default RentalDocsScreen;


const styles = StyleSheet.create({
  hide: {
    display: "none"
  },
  icon_input_thumb:{
      width:155,
      height:223
  },
  info_main:{ 
    backgroundColor: '#f9f9f4',
    flex: 1
  },
  enjoying_buggy:{
    fontSize:14,
    fontWeight:"bold"
  },
  carname_wrapper:{
     paddingVertical:15,
     paddingLeft:20,
  },
  carname_text:{
    fontSize:16, 
    marginTop:10
  },
  rating_section:{
    marginLeft:7,
  },
  rating_section_wrap:{
     marginTop:0,
     alignItems:"center",
     paddingTop:12,
     borderTopColor:"#ccc",
     borderTopWidth:1,
     paddingBottom:15,
     flexDirection:"row",
     justifyContent:"center",
     width:"100%", 
     minHeight:40,
     position:"absolute",
     backgroundColor:"#fff",
     bottom:0
  },
  title: {
    flexDirection: "row",
    width: win.width,
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "space-around", 
  },
  item_box:{
    width:155,
    height:224, 
    backgroundColor:"#f5f5f5",
    marginRight:8,
    marginBottom:8
  },
  titleText: {
    flex: 1,
    paddingLeft: 30,
    paddingBottom: 15,
    fontWeight: "bold",
    fontSize: 18,
  },
  titleClose: {
    marginRight: 20,
    marginTop: 5,
    fontWeight: "bold",
    fontSize: 22,
  },
  close_text_icon: {
    color: "red",
    fontSize: 22,
    fontWeight: "bold"
  },
  checkoutButton: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  checkoutButtonText: {
    color: "#454545",
    fontSize: 16,
    fontWeight: "bold",
    alignSelf: "center",
    flexDirection:"column", 
    marginTop:"35%"
  },
  row_item: { 
    borderColor: "#f1f1f1",
    paddingVertical: 12,
    borderRadius: 5,
  },
  list_item_price: {
    position: "absolute",
    right: 10,
    top: 3,
    fontSize: 12,
    color: "#454545"
  },
  list_item_date: {
    marginTop: 5,
    fontSize: 12,
    color: "#7d7d7d",
  },
  heading_item_title: {
    fontSize: 14,
    color: "#454545"
  },
  heading_item_date: {
    fontSize: 12,
    color: "#7d7d7d"
  },
  list_item_title: {
    fontSize: 12,
    color: "#454545"
  },
  no_container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop:20
  },
  padding_hr: {
    paddingHorizontal: 15, 
  },
  view_detail: {
    paddingHorizontal: 10,
  },
  row_wrapper: {
    flexDirection:"row", 
    flexWrap: 'wrap',  
  },
  row_wrapper_1:{
    flexDirection:"row",
    alignContent: "flex-start",
    alignItems:"flex-start", 
    paddingLeft:10,  
  },
  view_more_transactions: {
  },
  view_more_transactions_border: {
    paddingTop: 10, 
    zIndex: 10,
  },
  sub_item_heading: {
    borderBottomWidth: 1,
    paddingBottom: 10, 
    borderColor: "#dbdbdb",
    paddingHorizontal: 15,
  },
  banner_billing_link: {
    color: '#3d82eb',
    fontSize: 14,
    textDecorationLine: "underline",
  },
  lato_bold: {
    fontFamily: 'OpenSans-Bold',
    fontWeight: '700',
    fontStyle: 'normal'
  },
  lato_regular: {
    fontFamily: 'OpenSans',
    fontWeight: '400',
    fontStyle: 'normal'
  },
  lato_semibold: {
    fontFamily: 'OpenSans-SemiBold',
    fontWeight: '600',
    fontStyle: 'normal'
  },
  billing_list_item_wrap: {
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginVertical: 10,
    marginBottom: 0,
    zIndex: 10,
    shadowColor: "#000",
    zIndex: 10,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.29,
    shadowRadius: 1.65,
    elevation: 2,
    borderRadius: 8,
  },
  billing_list_item: {
    backgroundColor: '#fff',
    flexDirection: "row",
    alignContent: "flex-start",
  },
  billing_list_title_left: {
    fontWeight: "bold",
    width: "68%",
  },
  billing_list_price_right: {
    position: "absolute",
    right: 10,
    top: 0,
  },
  billing_list_title: {
    color: '#000',
    fontSize: 14,
    fontWeight: "bold",
  },
  textbuttonSubmit: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    paddingLeft: 0,
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center"
  },
  buttonText: {
    color: "#7d7d7d",
    fontSize: 13,
  },
  button_view_plus: {
    position: "absolute",
    right: 10,
    top: 20,
    fontSize: 18,
    color: "#7d7d7d",
  },
  button_view_icon: {
    fontSize: 18,
    marginRight: 5,
    marginTop: 1,
    color: "#7d7d7d",
    justifyContent: "center"
  },
});
