import React, { useState, useEffect, useContext } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    ActivityIndicator,
    View,
    Keyboard,
    TouchableOpacity,
    Dimensions,
    Platform,
    SafeAreaView,
} from 'react-native';
import { useMutation, useQuery } from '@apollo/react-hooks';
import analytics from '@react-native-firebase/analytics';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style } from '../../constants/Style';
import moment from "moment";
import Colors from '../../constants/Colors';
import { CheckConnectivity } from '../Helper/NetInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Octicons';
import { allTransactions } from '../../graphql/allTransactions';
import TransactionHistory from '../Payment/transactionHistory';
import CarScreen  from     '../../screens/Reservation/CarScreen'

export default function CarinitialScreen(props) {

    const [isLoading, setIsLoading] = useState(false);
    const [driverId, setDriverId] = useState('');
    const [reload, setReload] = useState(false);

    const [state, setState] = useState({
        amount_type: "",
        start_date:"",
        end_date:"",
        cat_name:"",
    });
    const {  amount_type, start_date, end_date, cat_name } = state;
 
    const checkNet = async () => {
        let netState = await CheckConnectivity();
        if (!netState) {
            props.screenProps.setNetModal(true)
            return;
        }
    }

    useEffect(() => {
        checkNet();
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
           // console.log('isFocused')
            setuserid();
        }

        const navFocusListener = props.navigation.addListener('didFocus', () => {
          //  console.log('didFocus')
            setuserid();
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);

    const setuserid = async () => {
        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);
        setDriverId("");
        setDriverId(user.id);
    }
    return (
        <>
            {isLoading ?
                <View style={global_styles.activityIndicatorView}>
                    <ActivityIndicator size="large" style={{ padding: 60 }} />
                </View> :
                <SafeAreaView key={reload} style={styles.container}>
                    {driverId != '' ? <CarScreen key={reload} cat_name={cat_name} end_date={end_date} start_date={start_date} amount_type={amount_type} {...props} driverId={driverId} /> : null}
                </SafeAreaView>}
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
    },
    container: {
        flex: 1,
        flexDirection: 'column-reverse',
    },
    top_container: {
        flex: 1,
    },
    bottom_container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    show_more_container: {
        paddingVertical: 8,
        paddingHorizontal: 32,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 8,
        margin: 16
    },
    filter_container: {
        backgroundColor: '#fff',
        margin: 16,
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 8,
    },
    vector_icon: {
        fontSize: 18,
    },
    filter: {
        marginStart: 8,
        fontSize: 16,
    },
    list_item_container: {
        flexDirection: 'row-reverse',
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 8,
    },
    list_item: {
        flex: 1,
    },
    saperator: {
        backgroundColor: '#ccc',
        height: 0.5,
        alignSelf: 'stretch'
    },
    title_text: {
        fontSize: 14,
    },
    date_text: {
        fontSize: 12,
        marginTop: 4,
    },
    amount_text: {
        fontSize: 14,
        color: '#393e5c'
    },
    amount_text_orange: {
        fontSize: 14,
        color: '#db9360'
    },
})