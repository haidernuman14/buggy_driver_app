import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, View, Text, Dimensions, Platform, TouchableOpacity } from 'react-native';
import { global_styles } from '../../constants/Style';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { translate } from '../../components/Language';

// https://www.joinbuggy.com/privacy-policy/

export default function PrivacyPolicy(props) {

    return (
        <View style={styles.main_container}>            
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}><>
                <View style={styles.container}>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("privacy_policy")}</Text>
                </View>
            </></KeyboardAwareScrollView>
        </View>
    )

}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
        paddingHorizontal: 8,
        paddingTop: 16,
        paddingBottom: 12,
    },
    container: {
        backgroundColor: '#fff',
        padding: 16,
    },
    heading: {
        fontSize: 20,
        color: '#393e5c',
        alignSelf: 'center',
        marginBottom: 16,
    },
    title: {
        fontSize: 18,
        marginTop: 12,
    },
    content: {
        marginTop: 4,
        color: '#393e5c',
        fontSize: 14,
    },
})