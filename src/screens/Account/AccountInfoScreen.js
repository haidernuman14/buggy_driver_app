import React, { useState, useEffect, useContext } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { StyleSheet, ActivityIndicator, View, Text, TextInput, Button, Image, TouchableOpacity } from 'react-native';
import { translate } from '../../components/Language';
import { SetDriverPassword } from '../../graphql/setDriverAccountPassword';
import { driverLogin } from '../../graphql/driverLogin';
import { CheckConnectivity } from '../Helper/NetInfo';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import { useMutation } from "@apollo/react-hooks";

export default function AccountInfoScreen(props) {

    const [state, setState] = useState({
        isLoading: false,
        emailId: '',
        newPassword: '',
        oldPassword: '',
        confirmPassword: '',
        phone:'',
        errors: {
            error_email: '',
            error_old_pass: '',
            error_new_pass: '',
            error_confirm_pass: ''
        }
    });
    const [driverid, setDriverId] = useState("");
    const [regModel, setRegModel] = useState(false);
    const [setDriverPassword] = useMutation(SetDriverPassword);
    const [AdriverLogin] = useMutation(driverLogin);
    const { isLoading, oldPassword, emailId, newPassword, confirmPassword, errors,phone } = state;

    const updateTextInput = (value, field) => {
        setState({ ...state, [field]: value.trim() });
    }

    const checkNet = async () => {
        let netState = await CheckConnectivity();
        //await AsyncStorage.setItem('onSchedToken', '');
        if (!netState) {
            props.screenProps.setNetModal(true)
            return false;
        }
        return true;
    }
    const setuserid = async () => {
        const user = await AsyncStorage.getItem('user');
        let user_id = JSON.parse(user).id;
        // console.log("Email-->", JSON.parse(user).email);
        let phoneNumber = JSON.parse(user).phone
        // console.log(phone)
        setDriverId(user_id);
        setState({ ...state, newPassword: "", confirmPassword: "",phone:phoneNumber })
    }

 const  checkOldPassword=()=>{
    setState({ ...state, isLoading: true });

    let flag = 1
    let err = { ...state, errors };

    if (oldPassword == '') {
        err.error_old_pass = translate('error_old_password')
        flag = 0
    } else {
        err.error_old_pass = ''
        
       
    }
}

    useEffect(() => {
        checkNet();

        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            setuserid();
        }
        const navFocusListener = props.navigation.addListener('didFocus', () => {
            setuserid();
        });
        return () => {
            navFocusListener.remove();
        };
    }, []);

    const save = () => {
      
           
        setState({ ...state, isLoading: true });

        let flag = 1
        let err = { ...state, errors };
        // let err = { ...errors }

        /*   if (emailId == '') {
               err.error_email = translate('error_email')
               flag = 0
           } else {
               err.error_email = ''
           }
   */

        if (oldPassword == '') {
            err.error_old_pass = translate('error_old_password')
            flag = 0
        } else {
            err.error_old_pass = ''
            AdriverLogin({
                variables: {
                  username:phone,
                  password: oldPassword
                }
              }).then((response) => {
                    
              }).catch((error=>{
                err.error_old_pass = translate('error_old_password')
                flag = 0
              }))
        }

        if (newPassword == '') {
            err.error_new_pass = translate('error_new_password')
            flag = 0
        } else {
            err.error_new_pass = ''
        }

        if (confirmPassword == '') {
            err.error_confirm_pass = translate('error_confirm_password_account')
            flag = 0
        } else if (newPassword != confirmPassword) {
            err.error_confirm_pass = translate('error_password_not_matched')
            flag = 0
        }
        else {
            err.error_confirm_pass = ''
        }

        if (flag == 0) {
            setState({ ...state, errors: err, isLoading: false })
            return
        }

         

        err.error_email = ''
        err.error_new_pass = ''
        err.error_confirm_pass = '',
        err.error_old_pass = '',
        
        

        setState({ ...state, isLoading: true });

        let input = {
            driverId: driverid,
            newPassword: newPassword,
            confirmPassword: confirmPassword,
        }

        // console.log("input--->",input);
        // return;
        //API calls once valid email is received from user input.
        AdriverLogin({
            variables: {
              username:phone,
              password: oldPassword
            }
          }).then((response) => {
            updatePassword(input)

          }).catch((error=>{
            // setState({ ...state, isLoading: false });
            err.error_old_pass = "Old Password is incorrect"
            setState({ ...state, isLoading: false, errors: err });
          
          }))

        

    }

    updatePassword=(input)=>{
       

        setDriverPassword({
            variables: { input }
        }).then((response) => {

            if (response.data.setDriverPassword.ok) {
                // props.navigation.navigate('Forgotpasswordverify'); 
                setRegModel(true);
                setState({ ...state, isLoading: false, errors: err });
            } else {
                error.error_confirm_pass = translate("email_verification_code_is_not_authenticated"); // Please show proper messahe that comes from API response
                setState({ ...state, isLoading: false, errors: err });
            }
            return

        }).catch((error) => { // error handling in case of API thows error
            setIsLoading(false);
            error.error_confirm_pass = translate("email_verification_code_is_not_authenticated");
            setState({ ...state, errors: err, isLoading: false });
            return
        });

        return;
    }

    const cancel = () => {
        setRegModel(false);
        props.navigation.goBack(null)
    }

    return (
        <View style={styles.main_container}>
            <KeyboardAwareScrollView  >
                <>
                    <View style={styles.container}>
                        <View>
                            <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>OLD PASSWORD</Text>
                            <TextInput
                                style={[global_styles.textInput, global_styles.lato_regular]}
                                secureTextEntry={true}
                                placeholder={translate("place_holder_old_password")}
                                value={oldPassword}
                                onChangeText={(text) => updateTextInput(text, 'oldPassword')}
                            />
                            <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_old_pass}</Text>
                        </View>
                        <View>
                            <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("new_password")}</Text>
                            <TextInput
                                style={[global_styles.textInput, global_styles.lato_regular]}
                                secureTextEntry={true}
                                placeholder={translate("place_holder_new_password")}
                                value={newPassword}
                                onChangeText={(text) => updateTextInput(text, 'newPassword')}
                            />
                            <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_new_pass}</Text>
                        </View>
                        <View>
                            <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("confirm_new_password")}</Text>
                            <TextInput
                                style={[global_styles.textInput, global_styles.lato_regular]}
                                secureTextEntry={true}
                                placeholder={translate("place_holder_confirm_new_password")}
                                value={confirmPassword}
                                onChangeText={(text) => updateTextInput(text, 'confirmPassword')}
                            />
                            <Text style={[styles.error_label, global_styles.lato_medium]}>{errors.error_confirm_pass}</Text>
                        </View>
                        <View style={global_styles.button_container}>
                            <TouchableOpacity onPress={cancel}>
                                <View style={global_styles.button_style_white_wrapper}>
                                    <Text style={global_styles.button_style_white_text}>{translate("cancel")}</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{ marginStart: 16 }} />
                            <TouchableOpacity onPress={save}>
                                <View style={global_styles.bottom_style_orange_wrapper}>
                                    <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate("save")}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {isLoading ?
                        <View style={[global_styles.activityIndicatorView]}>
                            <ActivityIndicator size="large" style={{ padding: 60 }} />
                        </View> : <></>}
                </>
            </KeyboardAwareScrollView>
            <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={regModel} >
                <View style={modal_style.modal}>
                    <View style={modal_style.modal_wrapper}>
                        <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={cancel}>
                            <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                        </TouchableOpacity>
                        <Text style={modal_style.modal_heading}>Success</Text>
                        <Text style={modal_style.modalText}>{translate("password_reset_success")}</Text>
                        <View style={modal_style.modal_textbuttonWrap_center}>
                            <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={cancel}>
                                <Text style={modal_style.upload_document_text_modal}>OK</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        </View >
    );

}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1
    },
    container: {
        margin: 22
    },
    labelStyleAcount: {
        marginTop: 4,
        fontSize: 12,
        color: '#000000',
        letterSpacing: 1,
        opacity: 0.6
    },
    button_container: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 22
    },
    error_label: {
        color: 'red',
        fontSize: 12,
    },
});