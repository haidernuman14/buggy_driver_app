import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, View, Text, Dimensions, Platform, TouchableOpacity } from 'react-native';
import { global_styles } from '../../constants/Style';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { translate } from '../../components/Language';

// https://www.joinbuggy.com/terms-of-service/

export default function TermsOfService(props) {

    return (
        <View style={styles.main_container}>
            <Text style={[styles.heading, global_styles.lato_medium]}>Buggy TLC  Car Rentals Terms of Service</Text>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}><>
                <View style={styles.container}>
                    <Text style={[styles.title, global_styles.lato_medium]}>1. Terms</Text>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("terms")}</Text>
                    <Text style={[styles.title, global_styles.lato_medium]}>2. Use License</Text>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("use_license")}</Text>
                    <Text style={[styles.title, global_styles.lato_medium]}>3. Disclaimer</Text>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("disclaimer")}</Text>
                    <Text style={[styles.title, global_styles.lato_medium]}>4. Limitations</Text>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("limitation")}</Text>
                    <Text style={[styles.title, global_styles.lato_medium]}>5. Accuracy of materials</Text>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("accuracy")}</Text>
                    <Text style={[styles.title, global_styles.lato_medium]}>6. Links</Text>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("links")}</Text>
                    <Text style={[styles.title, global_styles.lato_medium]}>7. Modifications</Text>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("use_license")}</Text>
                    <Text style={[styles.title, global_styles.lato_medium]}>8. Governing Law</Text>
                    <Text style={[styles.content, global_styles.lato_regular]}>{translate("governing")}</Text>
                </View>
            </></KeyboardAwareScrollView>
        </View>
    )

}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
        paddingHorizontal: 8,
        paddingTop: 16,
        paddingBottom: 12,
    },
    container: {
        backgroundColor: '#fff',
        padding: 16,
    },
    heading: {
        fontSize: 20,
        color: '#393e5c',
        alignSelf: 'center',
        marginBottom: 16,
    },
    title: {
        fontSize: 18,
        marginTop: 12,
    },
    content: {
        marginTop: 4,
        color: '#393e5c',
        fontSize: 14,
    },
})