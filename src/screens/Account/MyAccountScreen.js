import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Linking,
  Text,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  TouchableOpacity,
  Image,
  Platform
} from 'react-native';
import analytics from '@react-native-firebase/analytics';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles,modal_style} from '../../constants/Style';
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { CheckConnectivity } from '../Helper/NetInfo';
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


const win = Dimensions.get('window');
export default function MyAccountScreen(props) {

  const [state, setState] = useState({
    isLoading: false,
    data: {},
  });
  const [user, setUser] = useState({});
  const [driverId, setDriverId] = useState("");
  const { setStatusColor } = useContext(langContext);
  const  [isModal,setModal]=useState(false)

  const { isLoading} = state;

  // Check internet connection
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  // Setting up user detail
  const setuserid = async () => {

    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setDriverId(user.id);
    setUser(user);
  }

  // Set the driver id from the storage
  useEffect(() => {

    checkNet();
    setStatusColor(Colors.account_status_bar_color);
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();
    });

    return () => {
      navFocusListener.remove();
    };
  }, []);

  const showUpdateAccount = async () => {
    await AsyncStorage.setItem('parent', "MyAccount");
    props.navigation.navigate('UpdatePersonaInfo')
  }

  const Logout = async () => {
    await AsyncStorage.clear();
    setModal(false)
    props.navigation.navigate('SignIn');
  }

  return (
    <>
      {isLoading ?
        <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> :
        <KeyboardAwareScrollView contentContainerStyle={(Platform.OS == "web") ? { flexGrow: 1, height: (win.height - 100) } : {}}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <>
              <View style={styles.info_main} >
                <View style={styles.item_wrap} >
                  <TouchableHighlight underlayColor="" style={{ width: '100%' }} onPress={showUpdateAccount}>
                    <>
                      <View style={styles.item_image_left} >
                        <Image width="70" style={styles.icon_input} source={require("../../assets/images/myaccount/personal_info.png")} />
                      </View>
                      <View style={styles.item_image_right} >
                        <Text style={[styles.item_heading_text, global_styles.lato_medium]}>
                          Personal Info
                          </Text>
                        <Text style={[styles.item_content_text, global_styles.lato_semibold]}>
                          Name, Address, Phone
                          </Text>
                      </View>
                    </>
                  </TouchableHighlight>
                </View>


                <View style={styles.item_wrap} >
                  <TouchableHighlight underlayColor="" style={{ width: '100%' }} onPress={() => props.navigation.navigate('AccountInfo')}>
                    <>
                      <View style={styles.item_image_left} >
                        <Image width="50" style={styles.icon_input} source={require("../../assets/images/myaccount/account_info.png")} />
                      </View>
                      <View style={styles.item_image_right} >
                        <Text style={[styles.item_heading_text, global_styles.lato_regular]}>
                          Account Info
                          </Text>
                        <Text style={[styles.item_content_text, global_styles.lato_semibold]}>
                          Username & Password
                          </Text>
                      </View>
                    </>
                  </TouchableHighlight>
                </View>

                <View style={styles.item_wrap} >
                  <TouchableHighlight underlayColor="" style={{ width: '100%' }} onPress={() => props.navigation.navigate('AccountPayment')}>
                    <>
                      <View style={styles.item_image_left} >
                        <Image width="50" style={styles.icon_input} source={require("../../assets/images/myaccount/payment_settings.png")} />
                      </View>
                      <View style={styles.item_image_right} >
                        <Text style={[styles.item_heading_text, global_styles.lato_regular]}>
                          Payment Settings
                                </Text>
                        <Text style={[styles.item_content_text, global_styles.lato_semibold]}>
                          Update Credit Cards
                                </Text>
                      </View>
                    </>
                  </TouchableHighlight>
                </View>

                <View style={styles.item_wrap} >
                  <TouchableHighlight underlayColor="" style={{ width: '100%' }} onPress={() => props.navigation.navigate('LicenseInfo')}>
                    <>
                      <View style={styles.item_image_left} >
                        <Image width="50" style={styles.icon_input} source={require("../../assets/images/myaccount/license_info.png")} />
                      </View>
                      <View style={styles.item_image_right} >
                        <Text style={[styles.item_heading_text, global_styles.lato_regular]}>
                          License Info
                                    </Text>
                        <Text style={[styles.item_content_text, global_styles.lato_semibold]}>
                          DMV & TLC Licenses
                                    </Text>
                      </View>
                    </>
                  </TouchableHighlight>
                </View>

                <View style={styles.item_wrap} >
                  <TouchableHighlight underlayColor="" style={{ width: '100%' }} onPress={() => props.navigation.navigate('Subscription')}>
                    <>
                      <View style={styles.item_image_left} >
                        <Image width="50" style={styles.icon_input} source={require("../../assets/images/myaccount/subscriptions.png")} />
                      </View>
                      <View style={styles.item_image_right} >
                        <Text style={[styles.item_heading_text, global_styles.lato_regular]}>
                          Subscriptions
                                  </Text>
                        <Text style={[styles.item_content_text, global_styles.lato_semibold]}>
                          CDW Details and Settings
                                  </Text>
                      </View>
                    </>
                  </TouchableHighlight>
                </View>

                <View style={styles.item_wrap} >
                  <TouchableHighlight underlayColor="" style={{ width: '100%' }} onPress={() => props.navigation.navigate('NotificationSettings')}>
                    <>
                      <View style={styles.item_image_left} >
                        <Image width="50" style={styles.icon_input} source={require("../../assets/images/myaccount/message_rafiki.png")} />
                      </View>
                      <View style={styles.item_image_right} >
                        <Text style={[styles.item_heading_text, global_styles.lato_regular]}>
                          Notifications
                                    </Text>
                        <Text style={[styles.item_content_text, global_styles.lato_semibold]}>
                          Customize Your Notification
                                    </Text>
                      </View>
                    </>
                  </TouchableHighlight>
                </View>

                <View style={styles.item_wrap} >
                  <TouchableHighlight underlayColor="" style={{ width: '100%' }} onPress={() => props.navigation.navigate('Legal')}>
                    <>
                      <View style={styles.item_image_left} >
                        <Image width="50" style={styles.icon_input} source={require("../../assets/images/myaccount/pr-policy-2.png")} />
                      </View>
                      <View style={styles.item_image_right} >
                        <Text style={[styles.item_heading_text, global_styles.lato_regular]}>
                          Legal
                        </Text>
                        <Text style={[styles.item_content_text, global_styles.lato_semibold]}>Legal</Text>
                      </View>
                    </>
                  </TouchableHighlight>
                </View>

                {/* <View style={styles.item_wrap} >
                  <TouchableHighlight underlayColor="" style={{ width: '100%' }} onPress={() => { Linking.openURL('https://www.joinbuggy.com/privacy-policy/') }}>
                    <>
                      <View style={styles.item_image_left} >
                        <Image width="50" style={styles.icon_input} source={require("../../assets/images/myaccount/pr-policy-2.png")} />
                      </View>
                      <View style={styles.item_image_right} >
                        <Text style={[styles.item_heading_text, {paddingLeft:5}, global_styles.lato_regular]}>
                            Privacy Policy
                        </Text>
                        <Text style={[styles.item_content_text, {paddingLeft:5}, global_styles.lato_semibold]}>Privacy Policy</Text>
                      </View>
                    </>
                  </TouchableHighlight>
                </View> */}
                <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal}  >
              <View style={modal_style.modal}>
                <View style={modal_style.modal_wrapper}>
                  <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => {setModal(false)}}>
                    <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
                  </TouchableOpacity>
                  <Text style={modal_style.modal_heading}>Logout Confirmation</Text>
                  <Text style={modal_style.modalText}>Are you sure you want to logout?</Text>
                  <View style={{paddingVertical: 10,
    justifyContent:"space-between",
    flexDirection: 'row',
    alignItems: 'center',}}>
                    <TouchableHighlight underlayColor="" style={modal_style.upload_document_modal} onPress={() => {setModal(false)}}>
                      <Text style={modal_style.upload_document_text_modal}>Cancel</Text>
                    </TouchableHighlight>
                    <TouchableHighlight 
                    onPress={Logout}
                    underlayColor="" style={modal_style.upload_document_modal}>
                      <Text style={modal_style.upload_document_text_modal}>{translate("logout")}</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </Modal>

                <View style={styles.button_container}>
                  <TouchableHighlight underlayColor="" onPress={()=>{setModal(true)}}>
                    <View style={styles.save_container}>
                      <Text style={[styles.save_text, global_styles.lato_regular]}>LOGOUT</Text>
                    </View>
                  </TouchableHighlight>
                </View>
                <Text>{"\n"}</Text>
              </View>
            </>
            
          </TouchableWithoutFeedback>
        </KeyboardAwareScrollView>}
    </>
  );
}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
  info_main: {
    backgroundColor: "#f9f9f4",
    flex: 1,
    paddingTop: 10,
  },
  item_heading_text: {
    fontSize: 18,
    color: "#000000",
    opacity: 0.87,
  },
  item_content_text: {
    fontSize: 14,
    fontWeight: "bold",
    flexWrap: "wrap",
    width: 180,
    color: "#33363b",
    opacity: 0.6
  },
  save_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 10,
    borderRadius: 6,
    alignItems: "center",
    marginStart: 16,
  },
  button_container: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 20,
    marginRight: 25
  },
  save_text: {
    color: '#FFFFFF',
    fontSize: 14,
  },
  item_wrap: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    paddingLeft: 5,
    marginHorizontal: 15,
    marginVertical: 6,
    flex: 1,
    backgroundColor: "#fff",
    borderLeftWidth: 0,
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: 'center',
  },
  item_image_left: {
    width: 80,
  },
  item_image_right: {
    width: 300,
    position: "absolute",
    marginLeft: 80,
    marginTop: 15,
  },
  icon_input: {
    width: 80,
    height: 80,
    resizeMode: "contain"
  },
  logout: {
    padding: 5,
    backgroundColor: Colors.color0,
    width: 120,
    marginTop: 15,
    marginRight: 25,
    alignItems: "center",
    alignSelf: "flex-end"
  }
});