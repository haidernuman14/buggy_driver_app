import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, ActivityIndicator, View, Text, Image, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { pickerSelectStyles, global_styles } from '../../constants/Style';
import { allNotificationTypes } from '../../graphql/allNotificationTypes';
import { allDriverNotificationTypeConfigurations } from '../../graphql/allDriverNotificationTypeConfigurations';
import { createDriverNotificationTypeConfiguration } from '../../graphql/createDriverNotificationTypeConfiguration';
import { UpdateDriverNotificationTypeConfiguration } from '../../graphql/updateDriverNotificationTypeConfiguration';
import { useMutation, useQuery } from '@apollo/react-hooks';
import CheckboxChecked from 'react-native-vector-icons/Ionicons';
import CheckboxUnChecked from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { CheckConnectivity } from '../Helper/NetInfo';

export default function NotificationSettingsScreen(props) {

    const [state, setState] = useState({
        isPauseAll: true,
        isScheduledMaintenance: true,
        isPaymentDue: true,
        isPaymentMade: true,
        isAppointments: true,
        isBuggyOfficeCommunicationNotification: true,
        isGetNotificationsByText: true,
        isMaintenanceEmail: true,
        isBillingEmails: true,
        iSupportEmails: true,
        isNewsEmails: true,
        allNotificationTypesData:null,
        isBuggyOfficeCommunicationEmail: true,
        push_notification:null,
        email_sms_notification:null,
        existing_driver_notification_settings:null,
        driver_push_notification:null,
        driver_email_sms_notification:null,
    });
    const [isLoading, setIsLoading] = useState(true);
    const [driverId, setDriverId] = useState("");
    const [subDriverId, setSubDriverId] = useState("");
    const [user, setUser] = useState({});

    const [CreateDriverNotificationTypeConfiguration] = useMutation(createDriverNotificationTypeConfiguration);
    const [AupdateDriverNotificationTypeConfiguration] = useMutation(UpdateDriverNotificationTypeConfiguration);
    
    // Check internet connection
    const checkNet = async () => {
        let netState = await CheckConnectivity();
        if (!netState) {
            props.screenProps.setNetModal(true)
            return;
        }
    }

    // Setting up user detail
    const setuserid = async () => { 
        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);  
        setDriverId(user.id);
        setUser(user); 
    }

    // Set the driver id from the storage
    useEffect(() => {

        checkNet();
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            setuserid();
        }

        const navFocusListener = props.navigation.addListener('didFocus', () => {
            setuserid();
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);


    const { isPauseAll, driver_push_notification,  driver_email_sms_notification, existing_driver_notification_settings, push_notification, email_sms_notification, allNotificationTypesData, isScheduledMaintenance, isPaymentDue, isPaymentMade, isAppointments,
        isBuggyOfficeCommunicationNotification, isGetNotificationsByText, isMaintenanceEmail, isBillingEmails,
        isSupportEmails, isNewsEmails, isBuggyOfficeCommunicationEmail } = state

    
    // Notification default data
    const { data: allDriverNotificationTypeConfigurations_Settings, loading: loading1, error: error1 } = useQuery(allDriverNotificationTypeConfigurations, {
        variables: { driverId: subDriverId },
        fetchPolicy: "network-only",
        skip: subDriverId == ''
      });
    useEffect(() => {
        if (allDriverNotificationTypeConfigurations_Settings) {  
            let push_notification_1 = [];
            let email_sms_notification_1 = [];
            let allNotificationTypes_type = [];
            if (allDriverNotificationTypeConfigurations_Settings && allDriverNotificationTypeConfigurations_Settings.hasOwnProperty('allDriverNotificationTypeConfigurations')) { 
            
                if(allDriverNotificationTypeConfigurations_Settings.allDriverNotificationTypeConfigurations != null){
                 
                // Push Notifications
                allDriverNotificationTypeConfigurations_Settings.allDriverNotificationTypeConfigurations.edges.map((ind)=>{
                    
                    if(ind.node.deliveryMethods.indexOf("Device")>=0){
                        let p_item = {
                            "id": ind.node.id,
                            "name":  ind.node.notificationType.name,
                            "type":  "Device",
                            "deliveryMethods": eval(ind.node.deliveryMethods)
                        };
                        push_notification_1.push(p_item);
                    }

                });

               // console.log("---?????", allDriverNotificationTypeConfigurations_Settings.allDriverNotificationTypeConfigurations.edges);
        
                 // SMS/EMAIL Notifications
                 allDriverNotificationTypeConfigurations_Settings.allDriverNotificationTypeConfigurations.edges.map((ind)=>{
                     
                    if((ind.node.deliveryMethods.indexOf("Email")>=0 || ind.node.deliveryMethods.indexOf("Sms")>=0)){
                        
                        let p_item = {};
                        if(ind.node.deliveryMethods.indexOf("Email")>=0 && ind.node.deliveryMethods.indexOf("Sms")>=0 ) {
                            p_item = {
                                "id": ind.node.id,
                                "name":  ind.node.notificationType.name,
                                "type":  "Email",
                                "deliveryMethods": eval(ind.node.deliveryMethods)
                            };
                            email_sms_notification_1.push(p_item);  
                            p_item = {
                                "id": ind.node.id,
                                "name":  ind.node.notificationType.name,
                                "type":  "Sms",
                                "deliveryMethods": eval(ind.node.deliveryMethods)
                            };
                            email_sms_notification_1.push(p_item);  
                            
                        } else if(ind.node.deliveryMethods.indexOf("Sms")>=0 ){
                            p_item = {
                                 "id": ind.node.id,
                                 "name":  ind.node.notificationType.name,
                                 "type":  "Sms",
                                 "deliveryMethods": eval(ind.node.deliveryMethods)
                             };
                             email_sms_notification_1.push(p_item);   
                        } else if(ind.node.deliveryMethods.indexOf("Email")>=0 ){
                            p_item = {
                                 "id": ind.node.id,
                                 "name":  ind.node.notificationType.name,
                                 "type":  "Email",
                                 "deliveryMethods": eval(ind.node.deliveryMethods)
                             };
                             email_sms_notification_1.push(p_item);   
                         }   
                        console.log(ind.node.deliveryMethods);
                    }

                });

                allDriverNotificationTypeConfigurations_Settings.allDriverNotificationTypeConfigurations.edges.map((ind)=>{
                    console.log("item.deliveryMethods-=-=----",ind.node.deliveryMethods,eval(ind.node.deliveryMethods).length);
                    if(ind.node.deliveryMethods == "[]" ){
                        // console.log(55);
                        let p_item1 = {
                            "id": ind.node.id,
                            "name":  ind.node.notificationType.name,
                            "type":  "",
                            "deliveryMethods": eval(ind.node.deliveryMethods)
                        };
                        email_sms_notification_1.push(p_item1);   
                    } 

                });
                 
               }

               setSubDriverId("");
               setIsLoading(false); 
               setState({ ...state, existing_driver_notification_settings:allDriverNotificationTypeConfigurations_Settings,  driver_email_sms_notification:email_sms_notification_1, driver_push_notification:push_notification_1});
            }

         }
    }, [allDriverNotificationTypeConfigurations_Settings]); 
  
    ///////////// Current Charge ///////////////
    const { data: all_notification_data, loading: loading, error: error } = useQuery(allNotificationTypes, { 
        fetchPolicy: "network-only",
        skip: driverId == ''
      }); 
    useEffect(() => {
        if (all_notification_data) { 
            if (all_notification_data && all_notification_data.hasOwnProperty('allNotificationTypes')) { 
            //    console.log("-=-=-=->>>",all_notification_data.allNotificationTypes); 
                let push_notification_1 = [];
                let email_sms_notification_1 = [];
                let allNotificationTypes_type = [];
              
               if(all_notification_data.allNotificationTypes != null){
             
                // Push Notifications
                all_notification_data.allNotificationTypes.edges.map((ind)=>{
                    
                    if(ind.node.deliveryMethods.indexOf("Device")>=0 && ind.node.isActive == true){
                        let p_item = {
                            "id": ind.node.id,
                            "name":  ind.node.name,
                            "type":  "Device",
                        };
                        push_notification_1.push(p_item);
                    }

                });

                 // SMS/EMAIL Notifications
                 all_notification_data.allNotificationTypes.edges.map((ind)=>{
                    
                    if((ind.node.deliveryMethods.indexOf("Email")>=0 || ind.node.deliveryMethods.indexOf("Sms")>=0) && ind.node.isActive == true){
                        let p_item = {};
                        if(ind.node.deliveryMethods.indexOf("Email")>=0 && ind.node.deliveryMethods.indexOf("Sms")>=0 ) {
                            p_item = {
                                "id": ind.node.id,
                                "name":  ind.node.name,
                                "type":  "Email",
                                "deliveryMethods": eval(ind.node.deliveryMethods)
                            };
                            email_sms_notification_1.push(p_item);   

                        } else if(ind.node.deliveryMethods.indexOf("Sms")>=0 ){
                            p_item = {
                                 "id": ind.node.id,
                                 "name":  ind.node.name,
                                 "type":  "Sms",
                                 "deliveryMethods": eval(ind.node.deliveryMethods)
                             };
                             email_sms_notification_1.push(p_item);   
                        } else if(ind.node.deliveryMethods.indexOf("Email")>=0 ){
                            p_item = {
                                 "id": ind.node.id,
                                 "name":  ind.node.name,
                                 "type":  "Email",
                                 "deliveryMethods": eval(ind.node.deliveryMethods)
                             };
                             email_sms_notification_1.push(p_item);   
                         }
 
                    }

                });
                
               }
 
               if(driverId)
                   setSubDriverId(driverId);
               setState({ ...state, email_sms_notification:email_sms_notification_1, push_notification:push_notification_1, allNotificationTypes:allNotificationTypes_type});
             
            }
        }
    }, [all_notification_data]);
    ///////////// End Current Charge //////////////
   
   const createDriverNotificationTypeConfigurationCall = (deliveryMethods,notificationTypeId) => {
    
    setIsLoading(true); 

    let input = {
        driverId: user.id,
        notificationTypeId: notificationTypeId,
        deliveryMethods:deliveryMethods 
    } 
    // console.log("CreateDriverNotificationTypeConfiguration Input--", input); 

    CreateDriverNotificationTypeConfiguration({
      variables: { input }
    }).then((response) => {
        // console.log(response.data.createDriverNotificationTypeConfiguration,"payment reponse -->>==", response.data.createDriverNotificationTypeConfiguration.errors);
        if (response.data.createDriverNotificationTypeConfiguration.ok) {
            setSubDriverId(driverId); 
            return;
        }  else {
            setIsLoading(false); 
            return;
        }
    }).catch((error) => {
        setIsLoading(false); 
        return;
    }); 

   }

    const updateDriverNotificationTypeConfigurationCall = (deliveryMethods,driverNotificationTypeConfigurationId) => {
        
        setIsLoading(true); 
  
        let input = { 
            driverNotificationTypeConfigurationId: driverNotificationTypeConfigurationId, 
            deliveryMethods:deliveryMethods
        }   
        console.log("input-=-",input);
        AupdateDriverNotificationTypeConfiguration({
            variables: { input }
        }).then((response) => {
            console.log("payment reponse -->>==", response.data, response.data.updateDriverNotificationTypeConfiguration.errors);
            if (response.data.updateDriverNotificationTypeConfiguration.ok) {
                setSubDriverId(driverId); 
                return;
            } else {
                setIsLoading(false); 
                return;
            }
        }).catch((error) => {
            setIsLoading(false); 
            return;
        }); 

    } 

    const pushNotificationOnOff = (deliveryMethods,notificationTypeId) => {
        
    }

    const isSelectedPushNotification = (push_notification_item) => {
        let is_selected_item = false;
        if(driver_push_notification != null ){
            driver_push_notification.map((item)=> {
                //console.log(item.notificationType.name+"----------"+push_notification_item.name);
                if(item.name == push_notification_item.name){
                    is_selected_item = true;
                }
            }); 
        }
        return is_selected_item;
    }

    const isSelectedEmailSmsNotification = (email_sms_notification_item,type) => {
        let is_selected_item = false;
       // console.log("driver_email_sms_notification--::::---",driver_email_sms_notification,existing_driver_notification_settings);
        if(driver_email_sms_notification != null ){
            driver_email_sms_notification.map((item)=> {
                //console.log(item.notificationType.name+"-----<>-----"+email_sms_notification_item.name);
                if(item.name == email_sms_notification_item.name && item.deliveryMethods.includes(type) ){
                    is_selected_item = true;
                }
            }); 
        }
        return is_selected_item;
    }

    const saveSelectedCheckboxPushNotification = (push_notification_item,type) => {
        let is_selected_item = false;
        let methods_selected = [];
        let id_of_selected_item = "";
      
        if(driver_email_sms_notification != null ){
            driver_email_sms_notification.map((item)=> { 
                console.log("email>>>",item.name);
                if(item.name == push_notification_item.name  ){
                    is_selected_item = true;
                    id_of_selected_item = item.id
                    methods_selected = item.deliveryMethods;  
                } 
            }); 
        }
        if(driver_push_notification != null ){
            driver_push_notification.map((item)=> { 
                console.log("push>>>",item.name);
                if(item.name == push_notification_item.name){
                    is_selected_item = true;
                    id_of_selected_item = item.id
                    methods_selected = item.deliveryMethods;
                }
               
               // methods_selected = (item.deliveryMethods == "[]")?"":item.deliveryMethods.replace("['", "").replace("']", "").split("', '");  
            }); 
        }
         console.log("methods_selected_before////",methods_selected);
         if(methods_selected.length>0){
            if(methods_selected.includes(type)){
                let index_of_device = methods_selected.indexOf(type);
                methods_selected.splice(index_of_device, 1);
            } else {
                methods_selected.push(type);
            } 
        } else {
            methods_selected.push(type);
        }
        console.log("methods_selected_After",methods_selected,is_selected_item,"--",id_of_selected_item,"==",push_notification_item);
      // return;
        if(is_selected_item){ 
            updateDriverNotificationTypeConfigurationCall(methods_selected,id_of_selected_item);
        } else {
            
            createDriverNotificationTypeConfigurationCall(methods_selected,push_notification_item.id);
        } 

        return is_selected_item;
    }

    const saveSelectedCheckboxEmailSMSNotification = (push_notification_item,type) => {
        let is_selected_item = false;
        let methods_selected = [];
        let id_of_selected_item = "";
      
        
        if(driver_push_notification != null ){
            driver_push_notification.map((item)=> { 
                console.log("push>>>",item.name);
                if(item.name == push_notification_item.name){
                    is_selected_item = true;
                    id_of_selected_item = item.id
                    methods_selected = item.deliveryMethods;   
                } 
            }); 
        }

        if(driver_email_sms_notification != null ){
            driver_email_sms_notification.map((item)=> { 
                console.log("email>>>",item.name,"*",push_notification_item.name,"*",item.type,"*",type,"==",item.deliveryMethods,"==",item.deliveryMethods.includes(type) );
                if(item.name == push_notification_item.name   ){ // && item.deliveryMethods.includes(type)
                    is_selected_item = true;
                    id_of_selected_item = item.id
                    methods_selected = item.deliveryMethods;  
                } 
            }); 
        }

         console.log("methods_selected_before////",methods_selected);
         if(methods_selected.length>0){
            if(methods_selected.includes(type)){
                let index_of_device = methods_selected.indexOf(type);
                methods_selected.splice(index_of_device, 1);
            } else {
                methods_selected.push(type);
            } 
        } else {
            methods_selected.push(type);
        } 
        console.log("methods_selected_After",methods_selected,is_selected_item,"--",id_of_selected_item,"==",push_notification_item);
       // return;
        if(is_selected_item){ 
            updateDriverNotificationTypeConfigurationCall(methods_selected,id_of_selected_item);
        } else { 
            createDriverNotificationTypeConfigurationCall(methods_selected,push_notification_item.id);
        } 

        return is_selected_item;
    }
 
    return (
        <View style={styles.main_container}>
            {isLoading ? <View style={[global_styles.activityIndicatorView]}>
                <ActivityIndicator size="large" style={{ padding: 60 }} />
                </View> :<></>}
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}><>
                <Text style={[styles.title,{ marginBottom:20}, global_styles.lato_semibold]}>Choose how you get notifications</Text>
                <Text style={[styles.title, global_styles.lato_semibold]}>Push Notifications</Text>

                {existing_driver_notification_settings != null?<>
                <View style={styles.setting_group}> 

                    {(push_notification!=null)?<>
                        {push_notification.map((pushitem)=> 
                            <>
                                <View style={[styles.setting_item_container,{marginVertical:0, marginTop:0}]}>
                                    <Text style={[styles.item_text, {marginTop:6} ,global_styles.lato_regular]}>{pushitem.name}</Text>
                                    <TouchableOpacity onPress={() => saveSelectedCheckboxPushNotification(pushitem,"Device")  }>
                                        <Image style={styles.switch_button} source={isSelectedPushNotification(pushitem) ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                                    </TouchableOpacity>
                                </View>
                            </> 
                        )}</>:<></>}
                    
                     
                </View>
                <View style={styles.setting_title_container}>
                     <Text style={[styles.setting_title_2, global_styles.lato_semibold]}>Email and Text (SMS)</Text>
                     <View style={styles.checkboxes_container}>
                        <View style={[styles.checkboxes_item,{marginLeft:0}]}> 
                            <Image style={[styles.switch_title_button,{marginRight:-5}]} source={require('../../assets/images/notification_email_icon.png')} />
                        </View>
                        <View style={styles.checkboxes_item}>
                            <Image style={[styles.switch_title_button,{height:25}]} source={require('../../assets/images/notification_sms_icon.png')} />
                        </View> 
                    </View>
                </View>
                <View style={styles.setting_group}>

                {(email_sms_notification!=null)?<>
                    {email_sms_notification.map((emailitem)=>
                        <>
                            <View style={styles.setting_item_container}>
                                <Text style={[styles.item_text, global_styles.lato_regular]}>{emailitem.name}</Text>
                                <View style={styles.checkboxes_container}>
                                    <View style={[styles.checkboxes_item,{marginLeft:0}]}> 
                                      <TouchableOpacity onPress={() => saveSelectedCheckboxEmailSMSNotification(emailitem,"Email")  }>
                                           {isSelectedEmailSmsNotification(emailitem,"Email")?<Image style={styles.setting_checkbox_button} source={require('../../assets/images/notification_checkbox_check.png')} /> : <Image style={styles.setting_checkbox_button} source={require('../../assets/images/notification_checkbox_uncheck.png')} /> }
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.checkboxes_item}>
                                      <TouchableOpacity onPress={() => saveSelectedCheckboxEmailSMSNotification(emailitem,"Sms")  }>
                                           {isSelectedEmailSmsNotification(emailitem,"Sms")?<Image style={styles.setting_checkbox_button} source={require('../../assets/images/notification_checkbox_check.png')} /> : <Image style={styles.setting_checkbox_button} source={require('../../assets/images/notification_checkbox_uncheck.png')} /> }
                                        </TouchableOpacity>
                                    </View> 
                                </View>    
                            </View>
                        </>
                    )}</>:<></>} 
                </View>
                </>:<View style={[global_styles.activityIndicatorView]}></View>
                }
            </></KeyboardAwareScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
        padding: 22,
    },
    checkboxes_container:{
        flexDirection:"row",   
        alignContent:"flex-end",
        alignSelf:"flex-end",
        justifyContent: 'flex-end', 
    },
    checkboxes_item:{
        marginLeft:40
    },
    vector_icon: {
        fontSize: 26,
    },
    title: {
        fontSize: 18,
        color: '#000',
    },
    title_2: {
        fontSize: 18,
        color: '#000',
        marginTop: 64,
    },
    item_text: {
        fontSize: 16,
        color: '#000',
    },
    setting_group: {
        marginTop: 16,
    },
    setting_item_container: {
        flexDirection: 'row',
        justifyContent: 'space-between', 
        marginTop: 10,
        marginVertical:5,
        marginLeft:10
    },
    switch_button: {
        width: 70,
        height: 70 * 0.52,
        marginTop: 4,
    },
    setting_title_container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 35, 
    },
    setting_title_2: {
        fontSize: 18,
        color: '#000', 
    },
    switch_title_button:{
        width:25,
        height:19
    },
    setting_checkbox_button:{
        width:22,
        height:22
    }
})