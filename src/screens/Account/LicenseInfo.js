import React, { useState, useEffect, useContext } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { StyleSheet, View, Text, TextInput, Button, Dimensions, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { useMutation } from '@apollo/react-hooks';
import { translate } from '../../components/Language';
import Icon2 from 'react-native-vector-icons/Feather';
import * as BlinkIDReactNative from 'blinkid-react-native';
import licenseKey from '../../constants/Microblink';
import visionUrl from '../../constants/GoogleVision';
import ImagePicker from 'react-native-image-picker';
import { global_styles } from '../../constants/Style';
import moment from 'moment';
import { driverUploadDocument } from '../../graphql/driverUploadDocument';
import { driverLicenseExpirationDateUpdate } from '../../graphql/driverLicenseExpirationDateUpdate';

const win = Dimensions.get('window');

export default function LicenseInfo(props) {

    const [AdriverUploadDocument] = useMutation(driverUploadDocument);
    const [AdriverLicenseExpirationDateUpdate] = useMutation(driverLicenseExpirationDateUpdate);

    const [dmv_flag, setDmvFlag] = useState(0);
    const [tlc_flag, setTlcFlag] = useState(0);
    const [isLoading, setIsLoding] = useState(false);
    const [tlc_length, setTlcLength] = useState(0);

    const [tlcLicenseFile, setTlcFileData] = useState({
        filePath: "",
        fileName: "",
        fileData: "",
        fileUri: "",
    });

    const [dmvLicenseFile, setDmvFileData] = useState({
        fileName: "",
        fileData: ""
    });

    const [state, setState] = useState({
        userInfo: {},
        dmvLicenseNumber: '',
        dmvExpDate: '',
        tlcLicenseNumber: '',
        tlcExpDate: '',
        errors: {
            error_dmv_license: '',
            error_dmv_exp_date: '',
            error_tlc_license: '',
            error_tlc_exp_date: ''
        }
    });

    const { userInfo, dmvLicenseNumber, dmvExpDate, tlcLicenseNumber, tlcExpDate, errors } = state

    const setUserDetail = async () => {
        let user = await AsyncStorage.getItem('user');
        user = JSON.parse(user);
        setTlcLength(user.tlcLicense.length);
        setState({
            ...state, userInfo: user,
            dmvLicenseNumber: user.dmvLicense == '' || user.dmvLicense == null ? translate('place_holder_dmv_license') : user.dmvLicense,
            dmvExpDate: user.dmvLicenseExpirationDate == '' || user.dmvLicenseExpirationDate == null ? translate('place_holder_dmv_exp') : user.dmvLicenseExpirationDate,
            tlcLicenseNumber: user.tlcLicense == '' || user.tlcLicense == null ? translate('place_holder_tlc_license') : user.tlcLicense,
            tlcExpDate: user.tlcLicenseExpirationDate == '' || user.tlcLicenseExpirationDate == null ? translate('place_holder_tlc_exp') : user.tlcLicenseExpirationDate
        })
        // console.log(user.tlcLicenseExpirationDate)
    }

    // Set user details from the storage
    useEffect(() => {
        const isFocused = props.navigation.isFocused();
        if (isFocused) {
            setUserDetail();
        }

        const navFocusListener = props.navigation.addListener('didFocus', () => {
            setUserDetail();
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);

    const updateTextInput = (value, field) => {
        setState({ ...state, [field]: value.trim() });
    }

    const scanDMV = async () => {
        try {
            var blinkIdCombinedRecognizer = new BlinkIDReactNative.BlinkIdCombinedRecognizer();
            blinkIdCombinedRecognizer.returnFullDocumentImage = true;

            const scanningResults = await BlinkIDReactNative.BlinkID.scanWithCamera(
                new BlinkIDReactNative.BlinkIdOverlaySettings(),
                new BlinkIDReactNative.RecognizerCollection([blinkIdCombinedRecognizer]),
                licenseKey
            );

            if (scanningResults) {
                // console.log('scanningResults==>', scanningResults);
                for (let i = 0; i < scanningResults.length; ++i) {
                    handleDMVResult(scanningResults[i]);
                }
            }
        } catch (error) {
            // console.log(error);
            let _error = { ...errors };
            _error.error_dmv_license = translate("error_dmv_license");
            setState({
                ...state,
                errors: _error
            });
        }
    }

    const handleDMVResult = (result) => {
        if (result instanceof BlinkIDReactNative.BlinkIdCombinedRecognizerResult) {
            let blinkIdResult = result;
            //console.log('blinkIdResult',blinkIdResult);
            // console.log('result', blinkIdResult.dateOfExpiry)
            let expDate = '';
            if (blinkIdResult.dateOfExpiry) {
                expDate = blinkIdResult.dateOfExpiry.month + "-" + blinkIdResult.dateOfExpiry.day + "-" + blinkIdResult.dateOfExpiry.year;
            }
            let resultString = {
                fileName: blinkIdResult.documentNumber + '.jpg',
                fileData: (blinkIdResult.fullDocumentFrontImage) ? (blinkIdResult.fullDocumentFrontImage) : ''
            }
            let dmvLicense = blinkIdResult.documentNumber;
            let error = { ...errors };
            error.error_dmv_license = "";
            error.error_dmv_exp_date = "";

            if (resultString.fileData == "" || resultString.fileData == null) {
                setDmvFlag(1);
                //setShowDMVMessage(true);
            }
            setDmvFileData(resultString)
            setState({ ...state, dmvLicenseNumber: dmvLicense, dmvExpDate: expDate, errors: error });
        }
    }

    const allowedFileType = (fileType) => {
        const allowed = ["jpg", "jpeg", "png"]
        let extension = "";
        if (fileType) {
            let split_ = fileType.split("/");
            if (split_.length > 1 && split_[1]) {
                extension = split_[1].toLowerCase()
            }
        }
        return allowed.includes(extension);
    }

    const scanTLC = async () => {
        let options = {
            title: 'Select Image From',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxWidth: 500,
            maxHeight: 500
        };
        await ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                // console.log('User cancelled image picker');
            } else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
            } else {
                if (allowedFileType(response.type)) {
                    // console.log('File response')
                    // console.log(response)
                    let f_name = response.uri.substring(response.uri.lastIndexOf('/') + 1)
                    let extension = f_name.split('.').pop();
                    let _tlc_license = tlcLicenseFile;
                    _tlc_license.filePath = response;
                    _tlc_license.fileData = response.data;
                    _tlc_license.fileUri = response.uri;
                    _tlc_license.fileName = 'tlc_scan.' + extension;

                    let error = { ...errors };
                    error.error_tlc_license = "";

                    setTlcFileData(_tlc_license)

                    setState({ ...state, errors: error })

                    let input = { file: _tlc_license.fileData, driverId: userInfo.id, documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZTo1", fileName: _tlc_license.fileName }
                    processDocument(_tlc_license.fileData, "TLC License", input)
                } else {
                    let error = { ...errors };
                    error.error_tlc_license = "File type not accepted. Please upload a .jpg, .jpeg or .png file";
                    setTlcFileData(
                        {
                            filePath: "",
                            fileName: "",
                            fileData: "",
                            fileUri: "",
                        }
                    )
                    setState({ ...state, errors: error });
                }
            }
        });
    }

    const processDocument = async (document, documentName, input) => {
        let error = { ...errors };
        setIsLoding(true)
        // console.log('Processing document...')
        if (document) {
            try {
                let visionRequest = {
                    "requests": [
                        {
                            "image": {
                                "content": document
                            },
                            "features": [
                                {
                                    "type": "TEXT_DETECTION",
                                    "maxResults": 4
                                }
                            ]
                        }
                    ]
                }
                const response = await fetch(visionUrl, {
                    method: 'POST',
                    headers: { 'Accept': 'application/json, text/plain, */*', },
                    body: JSON.stringify(visionRequest)
                });
                const responseData = await response.json();
                if (responseData && responseData.responses[0] && responseData.responses[0].fullTextAnnotation && responseData.responses[0].fullTextAnnotation.text) {
                    let fullText = responseData.responses[0].fullTextAnnotation.text;
                    if (documentName === "TLC License") {
                        if (fullText && fullText.toLowerCase().includes("license")) {//NYC TLC License has "LICENSE" text contained. Use to verify that a tlc license was infact returned
                            let tlcLicensefromImage = fullText.split('NUMBER\n').pop().split('\n')[0];
                            tlcLicensefromImage = tlcLicensefromImage.replace(/ /g, '');
                            const verifyLicense = await verifyTlcLicense(tlcLicensefromImage);
                            if (verifyLicense) {
                                // console.log('TLC license success...')
                                setIsLoding(false)
                                setState({ ...state, tlcLicenseNumber: tlcLicensefromImage, tlcExpDate: verifyLicense["tlc_date"] ? moment(verifyLicense["tlc_date"]).format('MM-DD-YYYY') : '' })
                            } else {
                                // console.log('Invalid TLC license...2')
                                error.error_tlc_license = "Please scan or upload valid TLC License.";
                                setIsLoding(false);
                                setState({ ...state, errors: error })
                            }
                        } else {
                            // console.log('Invalid TLC license...3')
                            error.error_tlc_license = "Please scan or upload valid TLC License.";
                            setIsLoding(false);
                            setState({ ...state, errors: error })
                        }
                    }
                } else {
                    // console.log('Invalid TLC license...4')
                    error.error_tlc_license = "Please scan or upload valid TLC License.";
                    setIsLoding(false);
                    setState({ ...state, errors: error })
                }
            } catch (error) {
                // console.log('Invalid TLC license...5')
                // console.log('readError', error)
                error.error_tlc_license = "Please scan or upload valid TLC License.";
                setIsLoding(false);
                setState({ ...state, errors: error })
            }
        } else {
            // console.log('Invalid TLC license...6')
            error.error_tlc_license = "Please scan or upload valid TLC License.";
            setIsLoding(false);
            setState({ ...state, errors: error });
        }
    }

    const verifyTlcLicense = async (tlcLicenseNumber) => {
        //Verify TLC License from the NYC Open Data Website. Returns a single array containing license info object
        // console.log('verifyTlcLicense')
        let nycOpenDataUrl = "https://data.cityofnewyork.us/resource/xjfq-wh2d.json?license_number=" + tlcLicenseNumber;
        let appToken = "5WBDkAQLj244SKLuDJmXDVDhT";
        let response = null;
        try {
            const res = await fetch(nycOpenDataUrl, {
                method: 'GET',
                data: {
                    "$limit": 1,
                    "$$app_token": appToken
                }
            });
            const data = await res.json();
            //Api returns an array
            if (data && data[0] && data[0].name) {
                let name = data[0].name;
                name = name.split(",");
                if (name && name.length > 0) {
                    response = { "last_name": name[0], "first_name": name[1], "tlc_date": data[0].expiration_date };
                }
            }
        }
        catch (err) {
            // console.log(err)
        }
        return response;
    }

    const fileUpload = (input, date, type = 'TLC') => {
        AdriverUploadDocument({
            variables: { input }
        }).then(async (response) => {
            // console.log("uploadDocument >>" + type, response.data.uploadDocument);
            // console.log("Error Array >>> " + response.data.uploadDocument.errors[0].messages)
            if (response.data.uploadDocument.ok) {
                let input;
                if (type == 'TLC')
                    input = { id: userInfo.id, tlcLicenseExpirationDate: moment(date, 'M-D-YYYY').format('YYYY-MM-DD') }
                else
                    input = { id: userInfo.id, dmvLicenseExpirationDate: moment(date, 'M-D-YYYY').format('YYYY-MM-DD') }
                await LicenseUpdate(input, type);
            } else {
                setIsLoding(false);
            }
        })
    }

    const LicenseUpdate = (input, type) => {
        // console.log('input', input)
        AdriverLicenseExpirationDateUpdate({
            variables: { input }
        }).then((response) => {
            // console.log("uploadDocument Sign >>", response.data.updateDriver);
            // console.log('_______', response.data.updateDriver.ok);
            if (response.data.updateDriver.ok == true && response.data.updateDriver.driver) {
                let user_info = response.data.updateDriver.driver;
                userInfo.tlcLicenseExpirationDate = user_info.tlcLicenseExpirationDate;
                userInfo.dmvLicenseExpirationDate = user_info.dmvLicenseExpirationDate;
                AsyncStorage.setItem('user', JSON.stringify(userInfo));
                setUserDetail()
                if (type == 'TLC') {
                    setIsLoding(false)
                } else {
                    setDmvFlag(1)
                }
                // console.log('___yes___')
                return;
            } else {
                // console.log('___no___')
                if (response.data.updateDriver.errors
                    && response.data.updateDriver.errors[0].field == "<class 'rest_framework.fields.SkipField'>") {
                    // console.log('not updated due to same license')
                } else {

                }
                if (type == 'TLC') {
                    setIsLoding(false)
                } else {

                }
            }
        })
    }

    const save = async () => {

        // console.log('TLC license number: ', tlcLicenseNumber)

        setIsLoding(true);

        let flag_val = 0;
        let error = { ...errors };

        if (tlcLicenseFile.fileData == '' || tlcLicenseNumber == '' || translate('place_holder_tlc_license')) {
            error.error_tlc_license = translate("error_tlc_license");
            flag_val = 1;
        } {
            error.error_tlc_license = "";
        }

        if (tlcExpDate == translate('place_holder_tlc_exp') || tlcExpDate == '') {
            error.error_tlc_exp_date = translate("error_tlc_license_exp");
            flag_val = 1;
        } else {
            error.error_tlc_exp_date = '';
        }

        if (dmvLicenseNumber == '' || translate('place_holder_dmv_license')) {
            error.error_dmv_license = translate("error_dmv_license");
            flag_val = 1;
        } {
            error.error_dmv_license = "";
        }

        if (dmvExpDate == translate('place_holder_dmv_exp') || dmvExpDate == '') {
            error.error_dmv_exp_date = translate("error_dmv_license_exp");
            flag_val = 1;
        } else {
            error.error_dmv_exp_date = '';
        }


        // if ((tlcExpDate != translate('place_holder_tlc_exp') || tlcExpDate != '') && tlcLicenseFile.fileData == '') {
        //     error.error_tlc_license = translate("error_tlc_license");
        //     flag_val = 1;
        // } else {
        //     error.error_tlc_license = "";
        // }

        // if ((dmvExpDate != translate('place_holder_dmv_exp') || dmvExpDate != '') || dmvLicenseNumber == '') {
        //     error.error_dmv_license = translate("error_dmv_license");
        //     flag_val = 1;
        // } else {
        //     error.error_dmv_license = "";
        // }

        // if (tlcExpDate == translate('place_holder_tlc_exp') || tlcExpDate == '') {
        //     error.error_tlc_exp_date = translate("error_tlc_license_exp");
        //     flag_val = 1;
        // } else {
        //     error.error_tlc_exp_date = '';
        // }

        // if (dmvExpDate == translate('place_holder_dmv_exp') || dmvExpDate == '') {
        //     error.error_dmv_exp_date = translate("error_dmv_license_exp");
        //     flag_val = 1;
        // } else {
        //     error.error_dmv_exp_date = '';
        // }

        if (flag_val == 1) {
            setIsLoding(false);
            setState({ ...state, errors: error });
            return;
        } else {
            setState({ ...state, errors: error });
        }

        if (dmvLicenseNumber == user.dmvLicense && dmvExpDate == user.dmvLicenseExpirationDate
            && tlcLicenseNumber == user.tlcLicense && tlcExpDate == user.tlcLicenseExpirationDate) {
            setIsLoding(false);
            return
        }

        if (dmvExpDate != '' && dmvLicenseFile.fileData != '' && userInfo.id != '') {
            // console.log('DMV ready to upload')
            let input = {
                file: dmvLicenseFile.fileData,
                driverId: userInfo.id,
                documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZTo2",
                fileName: dmvLicenseFile.fileName
            }
            await fileUpload(input, dmvExpDate, 'DMV');
        }

        if (tlcExpDate != '' && tlcLicenseFile.fileData && userInfo.id != '') {
            // console.log('TLC ready to upload')
            let input = {
                file: tlcLicenseFile.fileData,
                driverId: userInfo.id,
                documentTypeId: "RHJpdmVyRG9jdW1lbnRUeXBlTm9kZTo1",
                fileName: tlcLicenseFile.fileName
            }
            await fileUpload(input, tlcExpDate);
        }

        // setIsLoding(false)

    }

    const cancel = () => {
        props.navigation.goBack(null)
    }

    return (
        <View style={styles.main_container}>
            <KeyboardAwareScrollView  >
                <>
                    {isLoading ?
                        // <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
                        <ActivityIndicator size="large" style={{ padding: 60 }} />
                        // </View>
                        :
                        <View style={styles.container}>
                            <View>
                                <Text style={[styles.labelStyleAcount, global_styles.lato_medium]}>{translate("dmv_license_number")}</Text>
                                <TouchableOpacity >
                                    <View style={styles.license_info_container}>
                                        <Text style={[styles.license_info, global_styles.lato_regular]}>{dmvLicenseNumber}</Text>
                                        <Icon2 style={styles.text_icon} name='upload' />
                                    </View>
                                </TouchableOpacity>
                                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_dmv_license}</Text>
                            </View>
                            <View>
                                <Text style={[styles.labelStyleAcount, global_styles.lato_medium]}>{translate("dmv_exp_date")}</Text>
                                <View style={styles.license_info_container}>
                                    <Text style={[styles.license_info, global_styles.lato_regular]}>{dmvExpDate}</Text>
                                </View>
                                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_dmv_exp_date}</Text>
                            </View>
                            <View>
                                <Text style={[styles.labelStyleAcount, global_styles.lato_medium]}>{translate("tlc_license_number")}</Text>
                                <TouchableOpacity >
                                    <View style={styles.license_info_container}>
                                        <Text style={[styles.license_info, global_styles.lato_regular]}>{tlcLicenseNumber}</Text>
                                        {/* <Icon2 style={styles.text_icon} name='upload' /> */}
                                    </View>
                                </TouchableOpacity>
                                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_tlc_license}</Text>
                            </View>
                            <View>
                                <Text style={[styles.labelStyleAcount, global_styles.lato_medium]}>{translate("tlc_exp_date")}</Text>
                                <View style={styles.license_info_container}>
                                    <Text style={[styles.license_info, global_styles.lato_regular]}>{tlcExpDate}</Text>
                                </View>
                                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_tlc_exp_date}</Text>
                            </View>
                            <View style={styles.button_container}>
                                <TouchableOpacity onPress={cancel}>
                                    <View style={global_styles.button_style_white_wrapper}>
                                        <Text style={global_styles.button_style_white_text}>{translate("cancel")}</Text>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ marginStart: 16 }} />
                                <TouchableOpacity onPress={save}>
                                    <View style={global_styles.bottom_style_orange_wrapper}>
                                        <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate("save")}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    }
                </>
            </KeyboardAwareScrollView>
        </View >
    );

}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1
    },
    container: {
        margin: 22
    },
    labelStyleAcount: {
        fontSize: 12,
        color: '#000000',
        opacity: 0.6
    },
    license_info_container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 4,
        borderWidth: 1,
        borderRadius: 6,
        paddingHorizontal: 12,
        paddingVertical: 10,
        borderColor: '#d4d4d4',
        backgroundColor: '#ffffff'
    },
    license_info: {
        fontSize: 14,
        color: 'rgba(0, 0, 0, 0.6)',
    },
    button_container: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 22
    },
    error_label: {
        color: 'red',
        fontSize: 12,
    },
});
