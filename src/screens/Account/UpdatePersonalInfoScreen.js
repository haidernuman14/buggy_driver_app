import React, { useState, useEffect, useContext } from 'react';
import { TextField } from 'react-native-material-textfield';
import { useMutation, useQuery } from '@apollo/react-hooks';
import {
  StyleSheet,
  ScrollView,
  Text,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  Platform,
  Button,
  TouchableOpacity,
} from 'react-native';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { CheckConnectivity } from '../Helper/NetInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput } from 'react-native-gesture-handler';
import { UpdateDriverProfile } from '../../graphql/driverUpdateProfile';
import { checkIfDriverExists } from '../../graphql/checkIfDriverExists';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome from  'react-native-vector-icons/FontAwesome';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Modal from "react-native-modal";
import Ionicons from  'react-native-vector-icons/Ionicons';

const win = Dimensions.get('window');

/**
 * This class intended to update driver's personla info.
 * @param {*} props 
 */
export default function UpdatePersonalInfoScreen(props) {

  // Set update driver personal info mutation
  const [AUpdateDriverProfile] = useMutation(UpdateDriverProfile);

  let platformStyle = Platform.OS === 'ios' ? styles.iosStyle : styles.androidStyle;

  // Declare states
  const [state, setState] = useState({
    isLoading: false,
    data: {},
    state_list: [],
    first_name: '',
    last_name: '',
    nick_name: '',
    streetAddress: '',
    address_line_2: '',
    city: '',
    driverstate: '',
    zip: '',
    phone: '',
    email: '',
    errors: {
      error_first_name: '',
      error_last_name: '',
      error_nick_name: '',
      error_address: '',
      error_address_line2: '',
      error_city: '',
      error_state: '',
      error_zip: '',
      error_phone: '',
      error_email: '',
    }
  });

  const { isLoading, data, state_list, first_name, last_name, nick_name, streetAddress, address_line_2, city, driverstate, zip, phone, email, errors } = state

  const [user, setUser] = useState({});
  const [driverId, setDriverId] = useState("");
  const { setStatusColor } = useContext(langContext);
  const [regModel, setRegModel] = useState(false);
  const [autoCompelete,setAutoComplete]=useState(false);
  const [checkPhoneExist, setPhoneExist] = useState(false)

  // Check internet connection
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  const setParent = async () => {
    let parentClass = await AsyncStorage.getItem('parent');
    if (parentClass == 'Dashboard')
      parentClass = 'Dashboard'
    props.navigation.setParams({ parent: parentClass })
  }

  // Setting up user detail
  const setUserDetail = async () => {

    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    // console.log(user)
    setDriverId(user.id);
    setUser(user);

    let phone_val = user.phone;
    phone_val = phone_val.split("+1");
    if (phone_val.length == 1) {
      phone_val = user.phone;
    } else {
      phone_val = phone_val[1];
    }

    setState({
      ...state,
      first_name: user.firstName,
      last_name: user.lastName,
      nick_name: user.nickName,
      streetAddress: user.streetAddress,
      address_line_2: user.addressLine2,
      city: user.city,
      driverstate: user.state,
      zip: user.zipCode,
      phone: phone_val,
      email: user.email,
    })

  }

  const setStateList = () => {
    setState({ ...state, state_list: statelist })
  }

  // Set user details from the storage
  useEffect(() => {

    checkNet();
    setStatusColor(Colors.account_status_bar_color);
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setUserDetail();
      setParent();
      setStateList();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setUserDetail();
      setParent();
      setStateList();
    });

    return () => {
      console.log("here ...remove")
      navFocusListener.remove();
    };
  }, []);


  const setStateUpdate = (val) => {
    setState({ ...state, driverstate: val });
  }

  const updateTextInput = (value, field) => {
    setState({ ...state, [field]: value });
  }

  const { data: userData, loading: isCheckingUserExist, error: user_error } = useQuery(checkIfDriverExists, {
    variables: { phone: phone },
    fetchPolicy: "network-only",
    skip: (!checkPhoneExist)
  });

  useEffect(() => {
    setPhoneExist(false)
    if (userData && userData.hasOwnProperty('checkIfDriverExists')) {
      if (userData.checkIfDriverExists.driverExists) {
        let error = { ...state, errors };
        error.error_phone = "This phone number already exists.";
        setState({ ...state, errors: error });
      } else {
        updateData()
      }
    }
  }, [userData])

  // Update personal info
  const updatePersonalInfo = async () => {

    Keyboard.dismiss()

    let flag_val = 0;
    let error = { ...state, errors };

    if (first_name == '') {
      error.error_first_name = translate("error_first_name");
      flag_val = 1;
    } else {
      error.error_first_name = ''
    }

    if (last_name == '') {
      error.error_last_name = translate("error_last_name");
      flag_val = 1;
    } else {
      error.error_last_name = ''
    }

    // if (nick_name == '') {
    //   error.error_nick_name = translate("error_nick_name");
    //   flag_val = 1;
    // } else {
    //   error.error_nick_name = ''
    // }

    if (streetAddress == '') {
      error.error_address = translate("error_address");
      flag_val = 1;
    } else {
      error.error_address = ''
    }

    // if (address_line_2 == '') {
    //   error.error_address_line2 = translate("error_address_line2");
    //   flag_val = 1;
    // } else {
    //   error.error_address_line2 = ''
    // }

    if (city == '') {
      error.error_city = translate("error_city");
      flag_val = 1;
    } else {
      error.error_city = ''
    }

    if (state == '') {
      error.error_state = translate("error_state");
      flag_val = 1;
    } else {
      error.error_state = ''
    }

    /* if (zip == '') {
       error.error_zip = translate("error_zip_code");
       flag_val = 1;
     } else {
       error.error_zip = ''
     } */

    const expressioZip = /^(?:\d{5})$/;
    if (zip == '') {
      error.error_zip = "Please enter your Zip Code.";
      flag_val = 1;
    } else if (expressioZip.test(zip) === false) {
      error.error_zip = "Please enter valid Zip Code.";
      flag_val = 1;
    } else {
      error.error_zip = "";
    }

    const expressionPhone = /^(?:\d{10})$/;
    if (phone == '') {
      error.error_phone = translate("error_phone");
      flag_val = 1;
    } else if (expressionPhone.test(phone) === false) {
      error.error_phone = translate("error_valid_phone");
      flag_val = 1;
    } else {
      error.error_phone = "";
    }

    const expressionEmail = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    if (email == '') {
      error.error_email = translate("error_email");
      flag_val = 1;
    } else if (expressionEmail.test(email) === false) {
      error.error_email = translate("error_valid_email");
      flag_val = 1;
    } else {
      error.error_email = "";
    }

    if (flag_val == 1) {
      setState({ ...state, isLoading: false, errors: error });
      return;
    }

    let userInfo = { ...user }

    if (
      userInfo.firstName == first_name &&
      userInfo.lastName == last_name &&
      userInfo.nick_name == nick_name &&
      userInfo.streetAddress == streetAddress &&
      userInfo.address_line_2 == address_line_2 &&
      userInfo.city == city &&
      userInfo.state == driverstate &&
      userInfo.zipCode == zip &&
      (userInfo.phone == phone || userInfo.phone == "+1" + phone) &&
      userInfo.email == email
    ) {
      setState({ ...state, errors: errors, isBasicDetailEdit: false });
      return
    }

    if ((userInfo.phone != phone && userInfo.phone != "+1" + phone)) {
      setPhoneExist(true)
      return
    } else {
      updateData()
    }

  }

  const updateData = async () => {

    setState({ isLoading: true });

    let input = {
      id: driverId,
      firstName: first_name,
      lastName: last_name,
      nickName: nick_name,
      streetAddress: streetAddress,
      addressLine2: address_line_2,
      city: city,
      state: driverstate,
      zipCode: zip,
      phone: "+1" + phone,
      email: email,
    }

    // console.log("Parameters")
    // console.log(input)
    // console.log("Updating driver profile")

    AUpdateDriverProfile({ variables: { input } }).then((response) => {

      setState({ ...state, isLoading: false });

      errors.error_first_name = '';
      errors.error_last_name = '';
      errors.error_nick_name = '';
      errors.error_address = '';
      errors.error_address_line2 = '';
      errors.error_city = '';
      errors.error_state = '';
      errors.error_zip = '';
      errors.error_phone = '';
      errors.error_email = '';

      if (response.data.updateDriver.ok) {
        // console.log("Profile updated Successfully")
        let user_info = response.data.updateDriver;
        // console.log('User info', user_info)
        // Store upadated user personal data
        let userInfo = { ...user }
        userInfo.firstName = user_info.firstName;
        userInfo.lastName = user_info.lastName;
        userInfo.nickName = user_info.nickName;
        userInfo.streetAddress = user_info.streetAddress;
        userInfo.addressLine2 = user_info.addressLine2;
        userInfo.city = user_info.city;
        userInfo.state = user_info.state;
        userInfo.zipCode = user_info.zipCode;
        userInfo.phone = user_info.phone;
        userInfo.email = user_info.email;
        AsyncStorage.setItem('user', JSON.stringify(userInfo));
        // console.log(user_info)
        setUserDetail();
        // setRegModel(true);
        setState({ ...state, isLoading: false });
        cancel()
      } else {
        setState({ ...state, isLoading: false });
        // console.log("Failed to update profile")
        // console.log(response.data.updateDriver.errors[0].messages[0])
        // alert(response.data.updateDriver.errors[0].messages[0])
      }
    }).catch((error) => {
      // console.log("Error while updating profile")
      // console.log(error)
      setState({ ...state, isLoading: false });
      // alert("Error while updating profile")
    })

  }

  const cancel = () => {
    props.navigation.goBack(null)
  }

  let statelist = [
    { value: 0, label: "Select" },
    { value: 'AL', label: 'Alabama' },
    { value: 'AK', label: 'Alaska' },
    { value: 'AZ', label: 'Arizona' },
    { value: 'AR', label: 'Arkansas' },
    { value: 'CA', label: 'California' },
    { value: 'CO', label: 'Colorado' },
    { value: 'CT', label: 'Connecticut' },
    { value: 'DE', label: 'Delaware' },
    { value: 'DC', label: 'Dist. Of Columbia' },
    { value: 'FL', label: 'Florida' },
    { value: 'GA', label: 'Georgia' },
    { value: 'HI', label: 'Hawaii' },
    { value: 'ID', label: 'Idaho' },
    { value: 'IL', label: 'Illinois' },
    { value: 'IN', label: 'Indiana' },
    { value: 'IA', label: 'Iowa' },
    { value: 'KS', label: 'Kansas' },
    { value: 'KY', label: 'Kentucky' },
    { value: 'LA', label: 'Louisiana' },
    { value: 'ME', label: 'Maine' },
    { value: 'MD', label: 'Maryland' },
    { value: 'MA', label: 'Massachusetts' },
    { value: 'MI', label: 'Michigan' },
    { value: 'MN', label: 'Minnesota' },
    { value: 'MS', label: 'Mississippi' },
    { value: 'MO', label: 'Missouri' },
    { value: 'MT', label: 'Montana' },
    { value: 'NE', label: 'Nebraska' },
    { value: 'NV', label: 'Nevada' },
    { value: 'NH', label: 'New Hampshire' },
    { value: 'NJ', label: 'New Jersey' },
    { value: 'NM', label: 'New Mexico' },
    { value: 'NY', label: 'New York' },
    { value: 'NC', label: 'North Carolina' },
    { value: 'ND', label: 'North Dakota' },
    { value: 'OH', label: 'Ohio' },
    { value: 'OK', label: 'Oklahoma' },
    { value: 'OR', label: 'Oregon' },
    { value: 'PA', label: 'Pennsylvania' },
    { value: 'RI', label: 'Rhode Island' },
    { value: 'SC', label: 'South Carolina' },
    { value: 'SD', label: 'South Dakota' },
    { value: 'TN', label: 'Tennessee' },
    { value: 'TX', label: 'Texas' },
    { value: 'UT', label: 'Utah' },
    { value: 'VT', label: 'Vermont' },
    { value: 'VA', label: 'Virginia' },
    { value: 'WA', label: 'Washington' },
    { value: 'WV', label: 'West Virginia' },
    { value: 'WI', label: 'Wisconsin' },
    { value: 'WY', label: 'Wyoming' },
  ];

  const getDataFromAutoComplete=(data,detail)=>{
    
    let postal_code = detail.address_components.filter(component => component.types.includes("postal_code"))[0];
    let newcity = detail.address_components.filter(component => component.types.includes("locality") || component.types.includes("sublocality_level_1"))[0]; 
    let newState = detail.address_components.filter(component => component.types.includes("administrative_area_level_1"))[0];
      
   // setState({ ...state,streetAddress:data.description,city:newcity.long_name,driverState:newState.long_name})
    

    if(postal_code){
      setState({ ...state,streetAddress:data.description,city:newcity.long_name,zip:postal_code.long_name,driverstate:newState.short_name})
      setAutoComplete(false)}
      
      else{
      setState({ ...state,streetAddress:data.description,city:newcity.long_name,zip:"",driverstate:newState.short_name})
      setAutoComplete(false)
      
  }
}  
  
    
     



  return (
    <>
      <View style={styles.main_container}>
        {(isLoading || isCheckingUserExist) ?
          <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
            <ActivityIndicator size="large" style={{ padding: 60 }} />
          </View> :
          <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'} showsVerticalScrollIndicator={false} contentContainerStyle={(Platform.OS == "web") ? { flexGrow: 1, height: (win.height - 100) } : {}}>
            <View style={styles.container}>
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("first_name")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={translate("place_holder_first_name")}
                  value={first_name}
                  onChangeText={(text) => updateTextInput(text, 'first_name')}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_first_name}</Text>
              </View>
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("last_name")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={translate("place_holder_last_name")}
                  value={last_name}
                  onChangeText={(text) => updateTextInput(text, 'last_name')}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_last_name}</Text>
              </View>
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("nick_name")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={translate("place_holder_nick_name")}
                  value={nick_name}
                  onChangeText={(text) => updateTextInput(text, 'nick_name')}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_nick_name}</Text>
              </View>
              <View>
            
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("address")}</Text>
                
                
                <TouchableOpacity  onPress={()=>setAutoComplete(true)}>
                 <View style={global_styles.autoCompleteContainer}>
                <Text
                  style={[global_styles.lato_regular],[global_styles.textinputContainer]}
                  // placeholder={translate("place_holder_address")}
                  // value={streetAddress}
                  // editable={false}
                  // onFocus={()=>setAutoComplete(true)}
                  // onPress={()=>setAutoComplete(true)}
                  // onChangeText={(text) => updateTextInput(text, 'streetAddress')}
                >{streetAddress}</Text>
                
                </View>
                </TouchableOpacity>
                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_address}</Text>
              </View>
           
              {/* <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("address")}</Text>
                
                {/* <TouchableOpacity onPress={()=>setAutoComplete(true)}>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={translate("place_holder_address")}
                  value={streetAddress}
                  editable={false}
                  onChangeText={(text) => updateTextInput(text, 'streetAddress')}
                />
                </TouchableOpacity> */}
               
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("address_line_2")} (Apt, Suite, etc.)</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={translate("place_holder_address_line_2")}
                  value={address_line_2}
                  onChangeText={(text) => updateTextInput(text, 'address_line_2')}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_address_line2}</Text>
              </View>
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("city")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={translate("place_holder_city")}
                  value={city}
                  onChangeText={(text) => updateTextInput(text, 'city')}
                />
                <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_city}</Text>
              </View>
              <View style={styles.split_container}>
                <View style={styles.half_container_left}>
                  <View>
                    <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("state")}</Text>
                    <View style={[platformStyle]}>
                      <RNPickerSelect
                        placeholder={{}}
                        style={pickerSelectStyles}
                        onValueChange={(value) => setStateUpdate(value)}
                        mode="dropdown"
                        defaultValue={driverstate}
                        value={driverstate}
                        useNativeAndroidPickerStyle={false}
                        items={statelist}
                        Icon={() => {
                          return (
                            <View
                              style={pickerSelectStyles.icon}
                            />
                          );
                        }}
                      />
                    </View>
                  </View>
                </View>
                <View style={styles.half_container_right}>
                  <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("zipcode")}</Text>
                  <TextInput
                    style={[global_styles.textInput, global_styles.lato_regular]}
                    placeholder={translate("place_holder_zip_code")}
                    value={zip}
                    onChangeText={(text) => updateTextInput(text, 'zip')}
                  />
                  <Text style={[styles.error_label, global_styles.lato_regular]}>{errors.error_zip}</Text>
                </View>
              </View>
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("Phone")}</Text>
                <TextInput
                  keyboardType="number-pad"
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={translate("place_holder_phone_number")}
                  value={phone}
                  onChangeText={(text) => updateTextInput(text, 'phone')}
                />
                <Text style={[styles.error_label, global_styles.lato_medium]}>{errors.error_phone}</Text>
              </View>
              <View>
                <Text style={[global_styles.labelStyleAcount, global_styles.lato_medium]}>{translate("Email")}</Text>
                <TextInput
                  style={[global_styles.textInput, global_styles.lato_regular]}
                  placeholder={translate("place_holder_email_address")}
                  value={email}
                  onChangeText={(text) => updateTextInput(text, 'email')}
                />
                <Text style={[styles.error_label, global_styles.lato_medium]}>{errors.error_email}</Text>
              </View>
              <View style={global_styles.button_container}>
                <TouchableOpacity onPress={cancel}>
                  <View style={global_styles.button_style_white_wrapper}>
                    <Text style={global_styles.button_style_white_text}>{translate("cancel")}</Text>
                  </View>
                </TouchableOpacity>
                <View style={{ marginStart: 16 }} />
                <TouchableOpacity onPress={updatePersonalInfo}>
                  <View style={global_styles.bottom_style_orange_wrapper}>
                    <Text style={[global_styles.bottom_style_orange_text, global_styles.lato_medium]}>{translate("save")}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAwareScrollView>}
      </View>

      <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={regModel} >
        <>
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => { setRegModel(false) }}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Text style={modal_style.modal_heading}>Success</Text>
              <Text style={modal_style.modalText}>Profile updated successfully</Text>
              <View style={modal_style.modal_textbuttonWrap_center}>
                <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={() => { setRegModel(false) }}>
                  <Text style={modal_style.upload_document_text_modal}>OK</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </>
      </Modal>

      <Modal backdropOpacity={0.5} backdropColor={"#c4c4c4"} isVisible={autoCompelete}>
              <View style={{ flex: 1,backgroundColor: '#ecf0f1'}}>
                <View style={{flexDirection:"row",alignItems:"center",backgroundColor:"white",height:"7%",alignItems:"center",}}>
                <TouchableOpacity onPress={()=>{setAutoComplete(false)}}>
                  <Ionicons name="arrow-back" style={{alignItems:"center",fontSize:24,marginRight:10,marginLeft:10}}/>

                </TouchableOpacity>
                <Text style={{fontSize:16,textAlign:"center",justifyContent:"center",alignItems:"center",width:"75%"}}>Address</Text>
                </View>
                <View style={{padding: 10,fontWeight:"bold",flex:1}}>
              <GooglePlacesAutocomplete
         placeholder={streetAddress}
         fetchDetails= {true}
         query={{
          key: "AIzaSyBbOdZTwL_yX5P0kTNHgBx0yv_v5nQW4Ak",
          components: "country:us",
          language: 'us',
          type:'address',
          types: '(cities)',
          
           // language of the results
        }}
        
        onPress={(data, details = null) => {getDataFromAutoComplete(data, details)}}
        onFail={(error) => console.error(error)}
        requestUrl={{
          url:
            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
          useOnPlatform: 'web',
        }} // this in only required for use on the web. See https://git.io/JflFv more for details.
      />
      </View>
    
      </View>
                </Modal>
    </>
  );
}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1
  },
  container: {
    margin: 22
  },
  labelStyleAcount: {
    marginTop: 4,
    fontSize: 12,
    color: '#000000',
    letterSpacing: 1,
    opacity: 0.6
  },
  button_container: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 22
  },
  error_label: {
    color: 'red',
    fontSize: 12,
  },
  iosStyle: {
    marginLeft: 0,
    marginTop: -8,
    paddingVertical: 12
  },
  androidStyle: {
    marginLeft: -10,
    marginTop: -13,
    paddingVertical: 12,
    paddingHorizontal: 10,
  },
  split_container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  half_container_left: {
    flex: 1,
  },
  half_container_right: {
    flex: 1,
    marginStart: 16
  }
});