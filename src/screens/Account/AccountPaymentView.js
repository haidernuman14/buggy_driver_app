import React, { useState, useEffect } from 'react';
import {
  Linking, Text, Image, Dimensions, ScrollView, View, StyleSheet, TouchableHighlight, Button, Platform
} from 'react-native';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { translate } from '../../components/Language';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import Icon3 from 'react-native-vector-icons/AntDesign';
import { useLazyQuery, useMutation } from "@apollo/react-hooks";
import useEffectAsync from '../Helper/useEffectAsync';
import PaymentFormView from '../Payment/PaymentFormView';
import { global_styles, modal_style } from '../../constants/Style';
import { CreateCard } from '../../graphql/driverCreateCards'; 
import AsyncStorage from '@react-native-community/async-storage';
import { CheckConnectivity } from '../Helper/NetInfo';
import Colors from '../../constants/Colors';
import StripeCardListScreen from './AccountCardListScreen';
import { ChargeStripe } from '../../graphql/driverChargeStripe';
import Modal from "react-native-modal";
import RadioForm from 'react-native-simple-radio-button'; 
import { WebView } from 'react-native-webview';
import ShowPaymentPopupScreen from '../Payment/ShowPaymentPopupScreen';
import { getPaymentIds } from '../../constants/PaymentIDs';
import { CreditCardInput } from 'react-native-credit-card-input';
const win = Dimensions.get('window');
const SERVER_ERROR = 'Server error. Try again later.';

/*
Stripe payment gateway is used for payment in this application.
This class is responsible to manage payment funcationality.
-Allows Add/Delete credit cards used in stripes.
-Submit card token to stripe api
-Make a payment
-Provides pay later using Zelle and Vanilla.
 */

export default function StripeScreen(props) {

  const [state, setState] = useState({
    errorcard:null,
    payamount: props.amount,
    calculated_amount: props.amount,
    paymenttype:props.paymenttype,
    process: true,
    isLoading: false,
    radioPaymentTypeValue: 0,
    radioPaymentTypeIndex: 0,
    radioCardValue: 0,
    driverId: '',
    pk: '',
    showaddcard: false,
    call_list_card: false,
    isModalDashboard: false,
    isModalSched: false,
    isError: false,
    isModalPayLater: ( props.paymenttype && props.paymenttype == "paylater" )?true:false,
    isVanila: false,
    isZelle: false,
    applyCoupon:false,
    promocode:"",
    deduction_amount:0,
    isModalPayLaterSuccess:false,
    isModalPopup:false,
    cardTokenList:{},
    zelleRemoteCachePopup: false,
    from: (props.hasOwnProperty("from") ? props.from : "profile")
  });

  const { errorcard, zelleRemoteCachePopup, cardTokenList, radioPaymentTypeIndex, isModalPopup, deduction_amount,pk,promocode,isModalPayLaterSuccess, applyCoupon, from, isError,calculated_amount, isVanila, isZelle, isModalPayLater, radioPaymentTypeValue, radioCardValue, payamount, process, isLoading, driverId, showaddcard, call_list_card, isModalDashboard, isModalSched } = state;
  const [createcard] = useMutation(CreateCard);
  const [DriverChargeStripe] = useMutation(ChargeStripe);
  const [key, setKey] = useState(false);

  useEffect(() => {
    checkNet();
    
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();   
    }
    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();   
    });
    return () => {
      navFocusListener.remove();
    };
  }, []);
 
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    //await AsyncStorage.setItem('onSchedToken', '');
    if (!netState) {
      props.screenProps.setNetModal(true)
      return false;
    }
    return true;
  }

  // Set up user id
  const setuserid = async () => {
    const user = await AsyncStorage.getItem('user');
    let data = JSON.parse(user);
    let user_id = JSON.parse(user).id;
    let pk = JSON.parse(user).pk;
    let flg = 0;
    ////////////////////////////////   
    let current_payment_id = await AsyncStorage.getItem('current_payment_id'); 
    let p_value = 0;
    if(current_payment_id && current_payment_id != null) { 
      p_value = parseInt(current_payment_id); 
      flg = 1;
    } else if(data) { 
      if( data.paymentMethod && data.paymentMethod != "" && data.paymentMethod != null ) { 

        if( data.paymentMethod == "Card" ) {
          p_value = getPaymentIds(props,"card");
        }
        if( data.paymentMethod == "Zelle" ) {
          p_value = getPaymentIds(props,"zelle");
        }
        if( data.paymentMethod == "Remote Cash" ) {
          p_value = getPaymentIds(props,"remotecash");
        }  
        // console.log("p_value",p_value); 
      }   
      flg = 1;
    }
    if( flg == 1 ) {
      let radio_index = (p_value==getPaymentIds(props,"card"))?0:(p_value==getPaymentIds(props,"zelle")?1:(p_value==getPaymentIds(props,"remotecash"))?2:"");
      let radio_val = (p_value==getPaymentIds(props,"card"))?0:p_value; 

      if(props.navigation.state.routeName == "CarPayment" || props.navigation.state.routeName == "OnBoardingPassPayment" || props.navigation.state.routeName== "Questionnaire"){
        radio_index=0;
        radio_val=0;
      }
      // console.log("radio_index",radio_index);
      if((props.navigation.state.routeName == "DashboardPaynow") && (p_value == null || p_value==0) ) {
        radio_val=-1;
      }
      ///////////////////////////////

      setState({ ...state, radioPaymentTypeIndex:radio_index, radioPaymentTypeValue:radio_val, pk: pk, driverId: user_id, call_list_card: call_list_card });
     // setKey(!key);
    }

  }
   
  
  const getCreditCardToken = (creditCardData,address) => {
    // console.log("creditCardData---",creditCardData);
    let STRIPE_PUBLISHABLE_KEY='';
    if(props.screenProps.build_type!='live')
    {
      STRIPE_PUBLISHABLE_KEY = 'pk_test_At2oc8mm8RyTgle6alGE2CYf'; 
    }else{
      // Live
      STRIPE_PUBLISHABLE_KEY = "sk_live_BwVfXZfkTFigfTt8cI6Nz3F5008Wi5sJXR";
    }    
    const card = {
      'card[number]': creditCardData.values.number.replace(/ /g, ''),
      'card[exp_month]': creditCardData.values.expiry.split('/')[0],
      'card[exp_year]': creditCardData.values.expiry.split('/')[1],
      'card[cvc]': creditCardData.values.cvc,
      'card[name]': address.name,
     /* 'card[address_line1]': address.address,
      'card[address_line2]': address.address2,
      'card[address_state]':address.state,
      'card[address_city]': address.city,*/
      'card[address_zip]': address.zip
    }; 

    console.log("card------>",card);
    return fetch('https://api.stripe.com/v1/tokens', {
      headers: {
        // Use the correct MIME type for your server
        Accept: 'application/json',
        // Use the correct Content Type to send data in request body
        'Content-Type': 'application/x-www-form-urlencoded',
        // Use the Stripe publishable key as Bearer
        Authorization: `Bearer ${STRIPE_PUBLISHABLE_KEY}`
      },
      // Use a proper HTTP method
      method: 'post',
      // Format the credit card data to a string of key-value pairs
      // divided by &
      body: Object.keys(card)
        .map(key => key + '=' + card[key])
        .join('&')
    }).then(response => response.json());
  }; 

  // Handle Pay later radio button selection change
  const radioPayLetterMethodsButtonChange = (value) => {
    if (value == 1) {
      setState({ ...state, isVanila: true });
    } else {
      setState({ ...state, isZelle: true });
    }
  }

  // Handle card selection
  const radioCardChange = (value) => {
    setState({ ...state, radioCardValue: value });
    if( props.hasOwnProperty("setSelectedCard") ) {
      props.setSelectedCard(value); 
    } 
  }

  // Handle payment on submit button pressed 
  const onSubmit = async (creditCardInput,address,cardToken_List) => {
    // console.log("cs==>",0,cardToken_List);
    checkNet();
    // console.log("cs==>",1);
    const STRIPE_ERROR = 'There was an error with your information. Please enter valid card and billing information.';
    // Disable the Submit button after the request is sent
    if (!creditCardInput.valid) {
      // console.log("cs==>",3,creditCardInput);
      let carderr = 'There was an error with your information. Please enter valid card and billing information.';
      setState({ ...state, isLoading: false, process: false, errorcard: carderr });
      return;
    } 
   /* if (!process) {
      console.log("cs==>",2);
      return false;
    } */

    setState({ ...state, process: false, isLoading: true });

    let creditCardToken;
    try {
      console.log("try==>",3);
      // Create a credit card token 
      creditCardToken = await getCreditCardToken(creditCardInput,address); 
      if (creditCardToken.error) {
        // Reset the state if Stripe responds with an error
        // Set process to false to let the user subscribe again
        // console.log(creditCardToken.error); 
        setState({ ...state, process: true, isLoading: false, errorcard: STRIPE_ERROR }); 
        return;
      } else {
        // console.log("token---", creditCardToken,creditCardToken.id,cardToken_List,cardToken_List.length);

        /* if(cardToken_List.length > 0) {
          cardToken_List.map((itemval)=>{
            console.log(itemval.value+"=="+creditCardToken.id);
             if( itemval.value==creditCardToken.id ){
              setState({ ...state, process: true, isLoading: false, errorcard: "Credit card is already exists." });
              return;
             }
          });
        } */

        // Send a request to your server with the received credit card token
        const result = await subscribeUser(creditCardToken);
        // console.log(result)
        // Handle any errors from your server
        if (result) {
          setState({ ...state,  call_list_card: true, process: true, errorcard: null });
          setKey(!key);
        } else {
          setState({ ...state, process: true, isLoading: false, errorcard: SERVER_ERROR }); 
        }
      }
    } catch (e) { 
      // console.log(e);
      let carderr = 'There was an error with your information. Please enter valid card and billing information.';
      setState({ ...state, isLoading: false, process: true, errorcard: carderr });
     // setKey(!key);
      return;
    }
  };


  const subscribeUser = (creditCardToken) => {
    return new Promise((resolve, reject) => {

      console.log(8);

      // Check for valid credit card token
      if (creditCardToken && creditCardToken.hasOwnProperty('id') && creditCardToken.id) {

        // console.log(9);

        let input = {
          driver: driverId,
          tokenId: creditCardToken.id,
        }
        createcard({
          variables: { input }
        }).then((response) => {
          // console.log("subscribeUser - response-err===", response);
          // console.log(response.data.createCard)
          // console.log("subscribeUser - card-errros>>===", response.data.createCard.errors);
          if (response.data.createCard.errors) {
            // console.log(11);
            reject(false);
          } else {
            // console.log(12);
            resolve(true);
          }
        }).catch((error) => {
          // console.log("subscribeUser - card-err===", error);
          reject(false);
        });
      }
    });
  };

  // Handle pay by card option
  const paybyCard = () => {
    setState({ ...state, isLoading: true });
    // console.log(radioCardValue);
    if (radioCardValue != "" && (props.from == "dashboard" || props.from == "sched")) {

      let input = {
        driver: driverId,
        amount: calculated_amount, //props.amount,
        description: "Payment by card",
        id: radioCardValue
      }

      // console.log("input--", input);
      setState({ ...state, isLoading: true })
      DriverChargeStripe({
        variables: { input }
      }).then((response) => {
          // console.log("payment reponse -->>==", response.data);
        if (!response.data.chargeStripe.ok) {
          setState({ ...state, isLoading: false, isError: true });
          return;
        } else {
          //console.log("paybyCard - response-err===", response);
          //console.log("paybyCard-card-errros>>===", response.data);
          let _isModalDashboard = false;
          let _isModalSched = false;
          if (props.from == "dashboard") {
            _isModalDashboard = true;
          } else {
            _isModalSched = true;
          }
          setState({ ...state, isLoading: false, isModalDashboard: _isModalDashboard, isModalSched: _isModalSched })
          return;
        }
      }).catch((error) => {
        // console.log("card-err===", error);
        setState({ ...state, isLoading: false })
        return
      });

    }

  }

  const addNewCard = () => {
    checkNet(); 
  }

  const setloading = (setflag) => {
    setState({ ...state, isLoading: setflag })
    //setKey(!key);
  }

  const setReloadCards = () => {    
    setState({ ...state, radioCardValue: 0,call_list_card: !call_list_card });
    //setKey(!key);
  } 
  const cancelAddCard = () => {
    setState({ ...state,  errorcard: null, process: true })
  }
  
  const setCardList = (cardlist) => { 
    console.log("cardlist<<<<<<<<<<<<<<<<<",cardlist); 
    if (props && props.hasOwnProperty('setCardTotal') ) {
        props.setCardTotal(cardlist);
    }
    setState({ ...state, cardTokenList: cardlist });
  }
  
  return (
    <>
     <View style={styles.main_container}> 
        <ScrollView   keyboardShouldPersistTaps="always" >
          <View key={key} style={styles.container}>  
            <StripeCardListScreen  {...props} initReload={props.initReload}  cardTokenList={cardTokenList} cancelAddCard={cancelAddCard} onSubmit={onSubmit} error={errorcard} setCardList={setCardList} from_screen={props.navigation.state.routeName} from={from} isLoading={isLoading} radioCardChange={radioCardChange} paybyCard={paybyCard} setReloadCards={setReloadCards} addNewCard={addNewCard}  setloading={setloading} /> 
            <Text>{"\n"}</Text><Text>{"\n"}</Text><Text>{"\n"}</Text>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

// Stylesheet to create Stripe Payment UI
const styles = StyleSheet.create({
  container: {
    flex: 1,  
  },
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    height:"100%"
  },
  button_stripe: {
    fontSize: 14,
    marginRight: 5,
    marginTop: 1,
    position: "absolute",
    left: 0,
  },
  addbutton_payment: {
    paddingVertical: 0,
    paddingHorizontal: 8,
    marginHorizontal: 0,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: Colors.color0,
    borderRadius: 5,
    width:150,
  },
  buttonText_payment: {
    paddingVertical: 6,
    textAlign: "center",
    fontSize: 12,
    color: "#fff"
  },
  img_barcode: {
    width: 300,
    height: 200,
    marginTop: -20,
    resizeMode: "contain",
  },
  mail_box_text: {
    backgroundColor: "#fff",
    fontSize: 12,
  },
  radio_button: {
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "center",
  },
  message_global_text: {
    paddingVertical: 12,
  },
  payment_view_fields: {
    paddingHorizontal: 15,
    marginBottom: 20
  },
  payment_add_cards_text: {
    fontSize: 13,
  },
  payment_view_field_text: {
    fontSize: 12,
    position: "absolute",
    right: 15,
    top: 10
  },
  payment_method_wrap: {
    paddingVertical: 8, 
    paddingHorizontal: 10, 
  },
  card_text: {
    borderColor: "#ccc",
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 10,
    marginHorizontal: 10,
  },
  payment_method_title: {
    fontSize: 14
  },
  payment_method_items: {
    marginHorizontal: 10,
  },
  payment_method_text: {
    fontSize: 13,
    paddingLeft: 20,
  },
  payment_add_cards: { 
    paddingHorizontal:10,
  },
  add_payment_card: {
    marginVertical: 0,
    marginHorizontal: 10,
    borderBottomWidth: 1,
    borderColor: "#f5f5f5",
  },
  add_payment_card_link_wrap: {
    paddingTop: 8,
  },
  textbuttonAddPaymentMethod: {
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderWidth: 1,
    borderColor: Colors.color0,
    borderRadius: 5,
  },
  textbuttonSubmit: {
    backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    alignSelf: "center",
    borderRadius: 5,
  },
  modal_check: {
    color: "green",
    fontSize: 25,
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 7
  },
  modal_check_error: {
    color: "red",
    paddingHorizontal: 20,
    textAlign: "center"
  },
  modal_pay_later: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    height: 290,
    borderRadius: 5,
    borderWidth: 1,
  },
  modal_pay_later_wrap: {
    paddingHorizontal: 0, 
    paddingTop: 20,
    marginBottom:10,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    height: 180,
    borderRadius: 5,
    borderWidth: 1,
  },
  modalText: {
    marginVertical: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  modal_textbuttonWrap: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  apply_coupon_link: { 
    marginLeft: 15,
    padding:20, 
    width:140,
    position:"absolute",
    right:0,
    marginTop:38, 
  },
  apply_coupon_link_text: { 
    color: "#578ee6", 
    fontSize: 12,  
    alignSelf:"flex-end",
    flex:1,
    marginRight:5,
    textDecorationLine:"underline" 
  },
  upload_document_modal: {
    justifyContent: "center",
    marginLeft: 15
  },
  upload_document_text_modal: {
    color: "#FFFFFF",
    fontWeight: "bold",
    backgroundColor: Colors.color0,
    fontSize: 14,
    paddingHorizontal: 30,
    borderRadius: 6,
    paddingVertical: 6,
    marginRight: 7,
  },
  WrapText: {
    flexDirection: "row",
  },
  LinkTextWrap: {
    flexDirection: "row-reverse",
  },
  paddingBottom: {
    paddingBottom: 10,
  },
  pay_later_method_title: {
    fontSize: 15,
    marginBottom: 15,
  },
  pay_later_method_desc: {
    fontSize: 12, 
  },
  webview_desc: { 
    height:200,
    borderWidth:1,
  },
  pay_later_method_desc_link: {
    fontSize: 12,
    color: "#0d73e0",
    textDecorationLine: "underline"
  },
  payment_amount_wrap:{
    paddingHorizontal:10,
    marginVertical:5
  },
  payment_amount:{
     marginVertical:5,
     flex:1,
     flexDirection:"row"
  },
  payment_amount_title:{
     fontSize:12,
     fontWeight:"bold"
  },
  payment_amount_text:{
    marginLeft:5,
    fontSize:12,
  },
  payment_amount_text_promocode:{
    fontSize:10,
    fontStyle:"italic",
    marginLeft:5,
    marginTop:2,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
});