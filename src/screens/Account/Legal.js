import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, View, Text, Dimensions, Platform, TouchableOpacity } from 'react-native';
import { global_styles } from '../../constants/Style';
import Icon from 'react-native-vector-icons/AntDesign';

export default function Legal(props) {

    return (
        <View style={styles.main_container}>
            <View style={styles.outer_container}>
                <TouchableOpacity onPress={() => props.navigation.navigate('TermsOfService')}>
                    <View>
                        <View style={styles.item_container}>
                            <View style={styles.item}>
                                <Text style={[styles.title_text, global_styles.lato_medium]}>Terms of service</Text>
                                <Icon style={styles.vector_icon} name='right' />
                            </View>
                        </View>
                        <View style={styles.saperator} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => props.navigation.navigate('PrivacyPolicy')}>
                    <View style={styles.item_container}>
                        <View style={styles.item}>
                            <Text style={[styles.title_text, global_styles.lato_medium]}>Privacy policy</Text>
                            <Icon style={styles.vector_icon} name='right' />
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1
    },
    outer_container: {
        backgroundColor: "#fff",
        margin: 12,
    },
    item_container: {
        padding: 16,
    },
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    vector_icon: {
        fontSize: 18,
    },
    saperator: {
        backgroundColor: '#ccc',
        height: 0.5,
        alignSelf: 'stretch'
    },
    title_text: {
        fontSize: 16,
    },
})