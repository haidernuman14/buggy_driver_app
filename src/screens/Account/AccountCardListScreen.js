import React, { useState, useEffect } from 'react';
import {
  ActivityIndicator, Text, KeyboardAvoidingView, View, Dimensions, Image, FlatList, SafeAreaView, StyleSheet, TouchableOpacity,
  Platform, ScrollView
} from 'react-native';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { translate } from '../../components/Language';
import { useMutation } from "@apollo/react-hooks";
import { global_styles, modal_style } from '../../constants/Style';
import { GetStripeCards } from '../../graphql/driverAllCards';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Icon2 from 'react-native-vector-icons/EvilIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { CheckConnectivity } from '../Helper/NetInfo';
import Colors from '../../constants/Colors';
import { driverDeleteCard } from '../../graphql/driverDeleteCard';
import Modal from "react-native-modal";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { changeDefaultCard } from '../../graphql/changeDefaultCard';
import CardIcon from 'react-native-vector-icons/AntDesign';
import PaymentFormView from '../Payment/PaymentFormView';

/*
StripeCardListScreen lists the Credit cards of logged in user using "getStripeCards" API.
 */

export default function StripeCardListScreen(props) {

  const [state, setState] = useState({
    isModal: false,
    deleteCardItem: {},
    card_list: {},
    isModalPopup: false,
    ConfirmDefaultCard: false,
    SuccessConfirmDefaultCard: false
  });
  const [driverId, setDriverId] = useState("");
  const [userId, setUserID] = useState("");
  const [activeId, setActiveId] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const { SuccessConfirmDefaultCard, ConfirmDefaultCard, isModal, card_list, deleteCardItem, isModalPopup } = state;
  const [DriverDeleteCard] = useMutation(driverDeleteCard);
  const [ChangeDefaultCard] = useMutation(changeDefaultCard);

  // Set up user id
  const setuserid = async () => {
    const user = await AsyncStorage.getItem('user');
    setDriverId(JSON.parse(user).id);
    setUserID(JSON.parse(user).id);
  }

  // Check internet connectivity
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  const { data, loading, error: error_cards } = useQuery(GetStripeCards, {
    variables: { driverId: userId },
    fetchPolicy: "network-only",
    skip: (userId == '')
  });

  useEffect(() => {
    checkNet();
    setuserid();
    setIsLoading(false);
    if (data) {

      if (data && data.hasOwnProperty("getStripeCards") && data.getStripeCards && data.getStripeCards.hasOwnProperty("cards") && data.getStripeCards.cards.length > 0) {
        let card_list1 = [];
        let ik = 0;
        let active_index = 0;
        let first_card = "";
        data.getStripeCards.cards.map((carditem) => {
          if (carditem.default) {
            setActiveId(ik);
            active_index = ik;
            first_card = { card: data.getStripeCards.cards[active_index].id, number: "**** **** **** " + data.getStripeCards.cards[active_index].last4 };
          }
          card_list1.push({ label: (carditem.brand == "MasterCard" ? "Master" : carditem.brand) + " Card **** " + carditem.last4, value: carditem.id });
          ik = ik + 1;
        });

        if (first_card == "") {
          first_card = { card: data.getStripeCards.cards[active_index].id, number: "**** " + data.getStripeCards.cards[active_index].last4 };
        }
        // console.log("card_list1>>>>>>>>>>", card_list1);
        props.setCardList(card_list1);
        setState({ ...state, deleteCardItem: first_card, card_list: card_list1 });
        // setUserID("");
        props.radioCardChange(first_card.card);
      }
    }
  }, [data]);

  if (loading) {
    return <View><ActivityIndicator size="small" style={{ padding: 60 }} /></View>
  }

  const setListCards = (cards, removeditem) => {
    let card_list1 = [];
    cards.getStripeCards.cards.map((carditem) => {
      if (carditem.value != removeditem)
        card_list1.push({ label: "**** " + carditem.last4, value: carditem.id });
    });
    setState({ ...state, card_list: card_list1 });
  }

  const hideModal = (modalstatus) => {
    if (modalstatus) {
      checkNet();
      setIsLoading(true);
      let input = {
        driver: driverId,
        sourceId: deleteCardItem.card,
      }
      props.setloading(true);
      DriverDeleteCard({
        variables: { input }
      }).then((response) => {
        props.setloading(false);
        setIsLoading(false);
        if (response.data) {
          // console.log("removeCard-->", response.data.removeCard.errors);
          //setListCards(card_list,deleteCardItem.card);
          // if (response.data.removeCard.ok) {
          let card_list_temp = card_list;
          card_list_temp.map((citem, ind) => {
            if (citem.value == deleteCardItem.card) {
              card_list_temp.splice(ind, 1);
            }
          });
          setState({ ...state, card_list: card_list_temp, isModal: false });
          //props.setReloadCards();
          // }
          //props.setReloadCards();
        }
      });
    }

    setState({ ...state, isModal: false });
  }

  const deleteCardConfirm = (value, label) => {
    checkNet();
    setState({ ...state, deleteCardItem: { card: value, number: label }, isModal: true });
  }

  const getAddCardForm = () => {
    return (
      <View key={props.initReload} style={styles.add_payment_method_wrap}>
        <Text style={[styles.payment_method_title, global_styles.lato_semibold]} >Add New Card</Text>
        <PaymentFormView cardTokenList={props.cardTokenList} {...props} cancelAddCard={props.cancelAddCard} onSubmit={props.onSubmit} error={props.error} />
      </View>
    );
  }

  const getCardList = () => {
    if (card_list && card_list.length > 0) {
      return card_list.map((cardlistitem, index) => {
        return (<View style={styles.card_list_it} >
          <View style={styles.card_list}  >
            <View style={styles.creditcard_icon_wrap}>
              <Image width="30" style={styles.creditcard_icon} source={require("../../assets/images/credit-card-icon-2.png")} />
            </View>
            <Text style={[styles.cardlist_text, global_styles.lato_semibold]}>{cardlistitem.label}</Text>

            {((props.navigation.state.routeName != "PaymentPaynow") && card_list.length > 1 && (props.navigation.state.routeName != "BillingPage")) &&
              <TouchableOpacity underlayColor="" style={styles.addbutton} onPress={() => deleteCardConfirm(cardlistitem.value, cardlistitem.label)}>
                <Text style={styles.buttonText}>Remove</Text>
              </TouchableOpacity>
            }

          </View>

        </View>)
      })
    } else {
      return <>
        <Text style={[global_styles.lato_regular]}>No card found.</Text>
      </>;
    }
  }

  return (
    <>
      {!isLoading ? <>

        <Modal backdropOpacity={0.7} backdropColor={"#c4c4c4"} isVisible={isModal} >
          <View style={modal_style.modal}>
            <View style={modal_style.modal_wrapper}>
              <TouchableOpacity underlayColor="" style={modal_style.close_icon_img_wrap} onPress={() => hideModal(false)}>
                <Image width="23" style={modal_style.close_icon_img} source={require("../../assets/images/popup-close-icon.png")} />
              </TouchableOpacity>
              <Text style={modal_style.modal_heading}>Confirmation</Text>
              <Text style={modal_style.modalText}>Are you sure to delete card?{"\n"} {deleteCardItem.number}</Text>
              <View style={modal_style.modal_textbuttonWrap}>
                <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={() => hideModal(false)}>
                  <Text style={modal_style.upload_document_text_modal}>Go Back</Text>
                </TouchableOpacity>
                <TouchableOpacity underlayColor="" style={modal_style.upload_document_modal} onPress={() => hideModal(true)}>
                  <Text style={modal_style.upload_document_text_modal}>Accept</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <KeyboardAvoidingView  >
          <View style={styles.container}>
            {(props.navigation.state.routeName == "BillingPage" || props.navigation.state.routeName == "PaymentPaynow") ?
              <>{getCardList()}</>
              : <>
                <View style={styles.payment_method_wrap}>
                  <Text style={[styles.payment_method_title, global_styles.lato_semibold]} >{card_list.length > 1 ? 'Credit Cards' : 'Credit Card'}</Text>
                </View>
                <View key={card_list.length} >
                  {card_list && card_list.length > 0 ?
                    <View key={isModal} style={styles.card_line_item} >
                      {getCardList()}
                      {getAddCardForm()}
                    </View> : <>
                      <Text style={[styles.no_card_found, global_styles.lato_regular]}>No card found.</Text>
                      <View style={styles.card_line_item} >
                        {getAddCardForm()}
                      </View>
                    </>
                  }
                </View>
              </>
            }

            {/* card_list && card_list.length == 1 &&
                <Text style={[styles.security_deposit_message, { marginTop:0, paddingTop:0, fontSize:13}, global_styles.lato_regular]}>If you want to delete or replace this card, please contact customer support for further assistance.</Text>
            */ }
          </View>
        </KeyboardAvoidingView>

      </> : <View><ActivityIndicator size="small" style={{ padding: 40 }} /></View>}
      {props.isLoading && !isLoading && <View><ActivityIndicator size="small" style={{ padding: 40 }} /></View>}
    </>
  );
}

// Stylesheet to create Car List UI
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 18,
    marginTop: 10
  },
  cardlist_text: {
    fontSize: 14,
    marginLeft: 10,
    marginTop: 3,
    color: "#454545"
  },
  creditcard_icon_wrap: {
    alignSelf: "flex-start",
    height: 30
  },
  creditcard_icon: {
    width: 30,
    height: 30,
    resizeMode: "contain"
  },
  payment_method_wrap: {
    paddingVertical: 8,
    paddingHorizontal: 10,
  },
  add_payment_method_wrap: {
    paddingVertical: 8,
  },
  payment_method_title: {
    fontSize: 16
  },
  card_list: {
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  card_list_it: {
    borderBottomWidth: 0,
    borderBottomColor: "#f2f2f2",
    marginBottom: 10,
  },
  button_stripe: {
    fontSize: 14,
    marginRight: 5,
    marginTop: 1,
    position: "absolute",
    left: 0,
  },
  radio_button: {
    marginVertical: 10,
    alignContent: "center",
    justifyContent: "center",
  },
  no_card_found: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    fontSize: 12
  },
  button_card: {
    fontSize: 18,
  },
  buttonTextAddPaymentMethod: {
    fontSize: 12
  },
  delete_button_card: {
    position: "absolute",
    right: 4,
    top: 0,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    width: 40
  },
  delete_button_card_icon: {
    fontSize: 25
  },
  add_card_icon: {
    fontSize: 20,
  },
  payment_view_fields: {
    paddingHorizontal: 15,
    marginBottom: 20
  },
  payment_add_cards_text: {
    fontSize: 13,
  },
  payment_view_field_text: {
    fontSize: 14,
    position: "absolute",
    right: 15,
    top: 15
  },
  payment_method_wrap: {
    paddingVertical: 8,
    paddingHorizontal: 10,
  },
  card_text: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 10,
    marginHorizontal: 10,
  },
  payment_method_title: {
    fontSize: 14
  },
  payment_method_items: {
    marginHorizontal: 15,
    marginTop: 15
  },
  payment_method_text: {
    fontSize: 13,
    paddingLeft: 20,
  },
  payment_add_cards: {
    marginTop: 20,
  },
  add_payment_card: {
    marginVertical: 10,
    marginHorizontal: 0,
    borderBottomWidth: 1,
    borderColor: "#f5f5f5",
    flex: 1,
    flexDirection: "row"
  },
  add_payment_card_link_wrap: {
    paddingTop: 8,
  },
  card_line_item: {
    paddingVertical: 5,
    paddingHorizontal: 0,
    marginVertical: 5,
    marginHorizontal: 10,
    paddingBottom: 0,
    marginTop: 10,
    borderRadius: 5,
    marginBottom: 0,
    width: "90%",
  },
  addbutton: {
    paddingVertical: 0,
    paddingHorizontal: 10,
    marginHorizontal: 0,
    marginRight: 5,
    marginBottom: 10,
    borderRadius: 5,
    position: "absolute",
    top: 1,
    right: 10,
    alignSelf: "flex-end"
  },
  buttonText: {
    fontSize: 13,
    color: "#2dafd3"
  },
  buttonText_payment: {
    paddingVertical: 6,
    textAlign: "center",
    fontSize: 12,
    color: "#fff"
  },
  modal_check: {
    color: "red",
    fontSize: 35,
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 7
  },
  message_global_text: {
    fontSize: 20,
    marginBottom: 0,
    paddingHorizontal: 25,
    color: "#454545",
    textAlign: "center"
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    height: 180,
    borderRadius: 5,
    borderWidth: 1,
  },
  modalText: {
    marginVertical: 10,
    fontSize: 18,
    textAlign: 'center',
  },
  modal_textbuttonWrap: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  upload_document_modal: {
    justifyContent: "center",
    marginLeft: 0,
    paddingHorizontal: 50,
    borderRadius: 6,
    paddingVertical: 8,
    marginRight: 7,
    backgroundColor: Colors.color0,
  },
  upload_document_text_modal: {
    color: "#FFFFFF",
    fontWeight: "bold",
    fontSize: 16,
  },
  security_deposit_message: {
    paddingHorizontal: 12,
    paddingVertical: 12
  }
});