import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { pickerSelectStyles, global_styles } from '../../constants/Style';
import { allNotificationTypes } from '../../graphql/allNotificationTypes';
import { useMutation, useQuery } from '@apollo/react-hooks';
export default function NotificationSettingsScreen(props) {

    const [state, setState] = useState({
        isPauseAll: true,
        isScheduledMaintenance: true,
        isPaymentDue: true,
        isPaymentMade: true,
        isAppointments: true,
        isBuggyOfficeCommunicationNotification: true,
        isGetNotificationsByText: true,
        isMaintenanceEmail: true,
        isBillingEmails: true,
        iSupportEmails: true,
        isNewsEmails: true,
        allNotificationTypes:null,
        isBuggyOfficeCommunicationEmail: true,
        push_notification:null,
        email_sms_notification:null
    });

    const { isPauseAll, push_notification, email_sms_notification, allNotificationTypes, isScheduledMaintenance, isPaymentDue, isPaymentMade, isAppointments,
        isBuggyOfficeCommunicationNotification, isGetNotificationsByText, isMaintenanceEmail, isBillingEmails,
        isSupportEmails, isNewsEmails, isBuggyOfficeCommunicationEmail } = state

    const [isLoading, setIsLoading] = useState(false);

     ///////////// Current Charge ///////////////
    const { data: all_notification_data, loading: loading, error: error } = useQuery(allNotificationTypes); 

    // console.log("error------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",error);
    useEffect(() => {
        if (all_notification_data) { 
            if (all_notification_data && all_notification_data.hasOwnProperty('allNotificationTypes')) { 
            //    console.log("-=-=-=->>>",all_notification_data.allNotificationTypes); 
             //  let push_notification_1 = [];
              // let email_sms_notification_1 = [];
             //  let allNotificationTypes_type = [];
               /*
               if(all_notification_data.allNotificationTypes != null){
              /* 
                // Push Notifications
                all_notification_data.allNotificationTypes.edges.map((ind)=>{
                    
                    if(ind.node.deliveryMethods.indexOf("Device")>=0 && ind.node.isActive == true){
                        let p_item = {
                            "id": ind.node.id,
                            "name":  ind.node.name,
                        };
                        push_notification_1.push(p_item);
                    }

                });

                 // SMS/EMAIL Notifications
                 all_notification_data.allNotificationTypes.edges.map((ind)=>{
                    
                    if((ind.node.deliveryMethods.indexOf("Email")>=0 || ind.node.deliveryMethods.indexOf("Sms")>=0) && ind.node.isActive == true){
                        let p_item = {};
                        if(ind.node.deliveryMethods.indexOf("Email")>=0 ) {
                           p_item = {
                                "id": ind.node.id,
                                "name":  ind.node.name,
                                "type":  "Email",
                            };
                        }  else if(ind.node.deliveryMethods.indexOf("Sms")>=0 ){
                            p_item = {
                                 "id": ind.node.id,
                                 "name":  ind.node.name,
                                 "type":  "Sms",
                             };
                         }

                        email_sms_notification_1.push(p_item);   
                    }

                });
                * / 
               }
               setState({ ...state, email_sms_notification:email_sms_notification_1, push_notification:push_notification_1, allNotificationTypes:allNotificationTypes_type});
            */
            }
        }
    }, [all_notification_data]);
    ///////////// End Current Charge //////////////
    //console.log("email_sms_notification--->>>>>>>>>",email_sms_notification);
    //console.log("push_notification--->>>>>>>>>",push_notification);
    return (
        <View style={styles.main_container}>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}><>
                <Text style={[styles.title, global_styles.lato_medium]}>Push Notifications</Text>
                <View style={styles.setting_group}>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Pause all</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isPauseAll: !isPauseAll }) }}>
                            <Image style={styles.switch_button} source={isPauseAll ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Scheduled Maintenance</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isScheduledMaintenance: !isScheduledMaintenance }) }}>
                            <Image style={styles.switch_button} source={isScheduledMaintenance ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Payment Due</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isPaymentDue: !isPaymentDue }) }}>
                            <Image style={styles.switch_button} source={isPaymentDue ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Payment Made</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isPaymentMade: !isPaymentMade }) }}>
                            <Image style={styles.switch_button} source={isPaymentMade ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Appointments <Image style={{width:18, height:18}} source={require('../../assets/images/q_icon_notification.png')} /></Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isAppointments: !isAppointments }) }}>
                            <Image style={styles.switch_button} source={isAppointments ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Buggy Office Communication</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isBuggyOfficeCommunicationNotification: !isBuggyOfficeCommunicationNotification }) }}>
                            <Image style={styles.switch_button} source={isBuggyOfficeCommunicationNotification ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={[styles.title_2, global_styles.lato_medium]}>Email and SMS</Text>
                <View style={styles.setting_group}>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Get Notifications by Text (SMS)</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isGetNotificationsByText: !isGetNotificationsByText }) }}>
                            <Image style={styles.switch_button} source={isGetNotificationsByText ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Maintenance Emails</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isMaintenanceEmail: !isMaintenanceEmail }) }}>
                            <Image style={styles.switch_button} source={isMaintenanceEmail ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Billing Emails</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isBillingEmails: !isBillingEmails }) }}>
                            <Image style={styles.switch_button} source={isBillingEmails ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Support Emails</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isSupportEmails: !isSupportEmails }) }}>
                            <Image style={styles.switch_button} source={isSupportEmails ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>News Emails</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isNewsEmails: !isNewsEmails }) }}>
                            <Image style={styles.switch_button} source={isNewsEmails ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.setting_item_container}>
                        <Text style={[styles.item_text, global_styles.lato_regular]}>Buggy Office Communication</Text>
                        <TouchableOpacity onPress={() => { setState({ ...state, isBuggyOfficeCommunicationEmail: !isBuggyOfficeCommunicationEmail }) }}>
                            <Image style={styles.switch_button} source={isBuggyOfficeCommunicationEmail ? require('../../assets/images/toggle_on.png') : require('../../assets/images/toggle_off.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </></KeyboardAwareScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#f9f9f4',
        flex: 1,
        padding: 22,
    },
    title: {
        fontSize: 18,
        color: '#000',
    },
    title_2: {
        fontSize: 18,
        color: '#000',
        marginTop: 64,
    },
    item_text: {
        fontSize: 16,
        color: '#000',
    },
    setting_group: {
        marginTop: 16,
    },
    setting_item_container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 4,
        marginHorizontal: 8,
    },
    switch_button: {
        width: 70,
        height: 70 * 0.52,
        marginTop: 4,
    },
})