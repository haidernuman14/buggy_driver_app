import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  ActivityIndicator,
  View,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  Platform
} from 'react-native';
import analytics from '@react-native-firebase/analytics';
import { translate } from '../../components/Language';
import AsyncStorage from '@react-native-community/async-storage';
import { global_styles, modal_style, pickerSelectStyles } from '../../constants/Style';
import moment from "moment";
import Colors from '../../constants/Colors';
import { langContext } from '../Helper/langContext';
import { CheckConnectivity } from '../Helper/NetInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput } from 'react-native-gesture-handler';
import StripeScreen from './AccountPaymentView';

const win = Dimensions.get('window');
export default function AccountPaymentScreen(props) {

  const [state, setState] = useState({
    isLoading: false,
    data: {},
  });
  const [user, setUser] = useState({});
  const [initReload, setInitReload] = useState(false);
  const [driverId, setDriverId] = useState("");
  const { setStatusColor } = useContext(langContext);

  const { isLoading } = state;

  // Check internet connection
  const checkNet = async () => {
    let netState = await CheckConnectivity();
    if (!netState) {
      props.screenProps.setNetModal(true)
      return;
    }
  }

  // Setting up user detail
  const setuserid = async () => {
 
    let storage_val = await AsyncStorage.getItem('lastFlag');
    if(storage_val){
      setInitReload((storage_val=="yes")?true:false);  
      AsyncStorage.setItem('lastFlag', (storage_val=="yes")?"no":"yes");
    } else {
      AsyncStorage.setItem('lastFlag', "yes");
    }

    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    setDriverId(user.id);
    setUser(user);

  }

  // Set the driver id from the storage
  useEffect(() => {

    checkNet();
    setStatusColor(Colors.account_status_bar_color);
    const isFocused = props.navigation.isFocused();
    if (isFocused) {
      setuserid();
    }

    const navFocusListener = props.navigation.addListener('didFocus', () => {
      setuserid();
    });

    return () => {
      navFocusListener.remove();
    };
  }, []); 
  const setCardTotal = (value) => {  
  } 
  const navigateToSuccess = () => { 
  } 
  const setSelectedCard = (val) => {
    // console.log("creadit card", val); 
  }
  const navigateToScreen = () => {
  }
 
  return (
    <>
      {driverId == "" ?
        <View style={[global_styles.activityIndicatorView, { top: (Platform.OS == "web") ? (win.height / 2) - 100 : 0 }]}>
          <ActivityIndicator size="large" style={{ padding: 60 }} />
        </View> :
        <KeyboardAwareScrollView contentContainerStyle={(Platform.OS == "web") ? { flexGrow: 1, height: (win.height - 100) } : {}}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <>
              <View style={styles.info_main} >
                <View style={styles.item_wrap} >
                   <>
                      <View style={styles.add_payment_card}>
                        <View style={styles.add_payment_card_link_wrap}>
                          <StripeScreen initReload={initReload} setCardTotal={setCardTotal} setSelectedCard={setSelectedCard} {...props} navigateToSuccess={navigateToSuccess} navigateToScreen={navigateToScreen} navigation={props.navigation} from={"account"} amount={""} />
                        </View>
                      </View>
                    </> 
                </View>   
              </View>
            </>
          </TouchableWithoutFeedback>
        </KeyboardAwareScrollView>}
    </>
  );
}

// Stylesheet to design Dashboard screen
const styles = StyleSheet.create({
  info_main: {
    backgroundColor: "#fafafa",
    flex: 1, 
  }, 
  add_payment_card: {
    marginVertical: 0,
    marginHorizontal: 0, 
  }, 
  add_payment_card_link_wrap: {
    paddingTop: 8,
  },
});