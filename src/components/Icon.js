import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

import Colors from '../constants/Colors';

/** This screen is used to style sheet the icons which are used on global level throughout the applications 
 * example: User icon, lock icon,
 */
export default function GlobalIcon(props) {
  return (
    <Icon
      name={props.name}
      size={26}
      iconStyle={props.iconStyle}
      style={props.style}
      color={(props.color)?props.color:Colors.IconDefault}
    />
  );
}
