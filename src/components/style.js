import { PixelRatio, StyleSheet } from 'react-native';
import { Platform, Dimensions } from 'react-native';
const button_default_color = "#db9360";
const { width, height } = Dimensions.get('window');
const widthToDp = number => {
  let givenWidth = typeof number === 'number' ? number : parseFloat(number);
  return PixelRatio.roundToNearestPixel((width * givenWidth) / 100);
}
const heightToDp = number => {
  let givenHeight = typeof number === 'number' ? number : parseFloat(number);
  return PixelRatio.roundToNearestPixel((height * givenHeight) / 100);
}
const win = Dimensions.get('window');
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const baseHeight = 896;
const inputBoxVerticalPadding = win.height >= baseHeight ? 10 : 6;
console.log("screen height", screenHeight);
const gridPadding = win.height >= baseHeight ? 16 : 8;
const gridHeight = win.height >= baseHeight ? 200 : 160;
const gridImageHeight = win.height >= baseHeight ? 160 : 120;

const profileImageSize = 128;
const profileImageBackSize = profileImageSize + 10;
export const errorsScreen = StyleSheet.create({

  errorContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  reloadButton: {
    justifyContent: "center",
    backgroundColor: "#db9360",
    width: 130,
    height: 40,
    borderRadius: 5,
    marginVertical: 25
  }
});
export const loadingScreen = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
export const login_style = StyleSheet.create({
  labelStyle: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 1,
    marginVertical: 2,
    opacity: 0.6
  },
  text_input_wrapper:{
      marginVertical: screenHeight/8,//200,
      marginLeft:35,
      marginRight:35,
    },
    btn_wrapper:{
      alignItems:'center',
    },
    good_morning_block:{
      backgroundColor:'red',
      marginLeft:20,
      marginRight:10,
      width:'100%',
      height:40,
    },
      forgot_btn:{
      color:'#2dafd3', 
      fontSize: 14,
      alignSelf: "flex-end"
      }
  });
  export const forgotpass_style = StyleSheet.create({
    labelStyle: {
      fontSize: 12,
      color: 'rgba(0, 0, 0, 0.6)',
      letterSpacing: 1,
      marginVertical: 4,
      opacity: 0.6
    },
    text_input_container: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      height: 44,
      alignSelf:'center',
      fontSize:14,
      width:'100%', 
      borderColor: '#d4d4d4',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      borderRadius: 4,
      color: '#000000',
      },
      enter_phone_text_container:{
        marginTop:74, 
        marginLeft:36, 
        marginRight:36
      },
      enter_phone_text:{
        textAlign:'center', 
        fontSize: 12,
        color: '#000000',
        letterSpacing: 2,
      },
      textinput_spacing:{
        marginLeft:36, 
        marginTop:36, 
        marginRight:36
      },
      enter_code_text_container:{
        marginTop:74, 
        marginLeft:36, 
        marginRight:36
      },
      enter_code_text:{
        textAlign:'center', 
        fontSize: 12, 
        color: '#000000',
        letterSpacing: 2,
      },
      inputs_container:{
        marginLeft:36, 
        marginTop:36, 
        marginRight:36
      },
      btn_container:{
        alignItems:'center', 
        marginTop:66
      },
  });
  export const cardocs_style = StyleSheet.create({
    flat_list:{
      backgroundColor: '#DB9360',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
      borderRadius:8,
},
main_container: {
  backgroundColor: '#f9f9f4',
  flex: 1,
  marginLeft:15,
  marginRight:15,
  // paddingHorizontal: 15,
  // paddingBottom: 12,
},
  });
  
export const global_styles = StyleSheet.create({
  lato_bold: Platform.OS === "web" ? {} : {
    fontFamily: 'Lato-Bold',
    fontWeight: "bold",
    fontStyle: 'normal'
  },
  lato_semibold: Platform.OS === "web" ? {
    fontWeight: '700',
  } : {
      fontFamily: 'Lato-Semibold',
      fontWeight: '600',
      fontStyle: 'normal',

    },
  lato_regular: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal'
  },
  lato_regular_small: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 9,
  },

  lato_capital: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal',
    textTransform: 'uppercase'

  },
  lato_medium: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Medium",
    fontWeight: "bold",
    fontStyle: 'normal',
    letterSpacing: 0.75,

  },
  lato_regular_android: Platform.OS === "web" ? {} : {
    fontFamily: 'Lato-Regular',
  },
  safe_area: {
    flex: 1,
    backgroundColor: '#fafafa',
  },

  activityIndicatorView: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: (Platform.OS == "web") ? 20 : 0,
    flex: 1,
    zIndex: 1,
  },
  headContainer: {
    marginHorizontal: 50,
    marginVertical: 40,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  logo: {
    //width: win.width/1.5,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  header_bell_icon: {
    paddingRight: 10,
    paddingLeft: 12,
    marginRight: 10
  },
  header_notificationText: {
    color: "#FFF", fontSize: 9, fontWeight: "500"
  },
  header_notificationContainer: {
    position: 'absolute',
    top: Platform.OS === "ios" ? -3 : -5,
    right: 15
  },
  bottom_style_orange_wrapper: {
    paddingHorizontal: 30,
    backgroundColor: '#db9360',
    paddingVertical: 12,
    borderRadius: 5,
    alignItems: "center",
    minWidth: 120,
    textAlign: "center"
  },
  bottom_style_orange_text: {
    color: "#fff",
    fontSize: 14
  },
  bottom_style_gray_wrapper: {
    paddingHorizontal: 30,
    backgroundColor: '#dedbdb',
    paddingVertical: 12,
    borderRadius: 5,
    alignItems: "center",
    minWidth: 120,
    textAlign: "center"
  },
  bottom_style_gray_text: {
    color: "#393e5c",
    fontSize: 14
  },
  button_style_white_wrapper: {
    backgroundColor: '#fff',
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: '#d4d4d4',
    alignItems: "center",
    minWidth: 120,
    textAlign: "center",
    borderWidth: 1,
  },
  button_style_orange_wrapper: {
    backgroundColor: '#db9360',
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: '#db9360',
    alignItems: "center",
    minWidth: 120,
    textAlign: "center",
    borderWidth: 1,
  },
  
  dollorSign:{
    color: '#393e5c',
    fontSize:22 
  },
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 15,
    paddingBottom: 12,
  },
  name_container: {
    fontSize: 16,
    color: '#393e5c',
  },
  profile_outer: {
    borderRadius: 8,
  },
  profile_container: {
    backgroundColor: '#fff',
    padding: 23,
    borderRadius: 8,
    // overflow: 'visible',
  },
  picture_text_container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  profile_image_container: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    position: 'absolute',
    top: -12,
    left: 8,
    zIndex: 1,
  },
  profile_image_circle_back: {
    height: profileImageBackSize,
    width: profileImageBackSize,
    borderRadius: profileImageBackSize / 2,
    justifyContent: 'center',
    backgroundColor: '#f9f9f4',
  },
  profile_image: {
    height: profileImageSize,
    width: profileImageSize,
    borderRadius: profileImageSize / 2,
    alignSelf: 'center',
  },
  avatar: {
    height: profileImageSize,
    width: profileImageSize,
    borderRadius: profileImageSize / 2,
    alignSelf: 'center',
    opacity: 0.8,
  },
  camera_container: {
    height: 118,
    width: 130,
    flex: 1,
    justifyContent: 'flex-end',
    position: 'absolute',
  },
  notes: {
    fontSize: 12,
    marginEnd: 56,
    color: 'rgba(51,54,59,0.6)'
},
  camera_back: {
    flex: 1,
    backgroundColor: 'rgb(45,175,211)',
    width: 26,
    height: 26,
    borderRadius: 26 / 2,
    position: 'absolute',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  camera_image: {
    width: 22,
    height: 21,
    alignSelf: 'center',
    marginStart: 5,
    marginTop: 4,
  },
  profile_details_container: {
    flex: 2,
    alignSelf: 'stretch',
    alignItems: 'flex-end',
    marginStart: 8,
  },
  tlc_dmv_main_container: {
    flexDirection: 'row',
    flexWrap: "wrap",
    justifyContent: 'flex-end',
    marginTop: 16,
  },
  tlc_dmv_container: {
    flexDirection: 'row',
  },
  tlc_dmv_info: {
    fontSize: 14,
    color: '#393e5c',
  },
  edit_details_container: {
    marginTop: 4,
    flexDirection: 'row-reverse',
  },
  edit_details: {
    color: '#2dafd3',
    fontSize: 14,
  },
  grid_row_container: {
    flex: 1,
    flexDirection: 'row',
    height: gridHeight,
    marginTop: 16,
  },
  grid_box_left: {
    flex: 1,
    backgroundColor: '#fff',
    marginEnd: 8,
    justifyContent: 'flex-end',
    borderRadius: 8,
    padding: gridPadding,
  },
  grid_box_right: {
    flex: 1,
    backgroundColor: '#fff',
    marginStart: 8,
    justifyContent: 'flex-end',
    borderRadius: 8,
    padding: gridPadding,
  },
  grid_image: {
    alignSelf: 'center',
    marginBottom: 8,
    width: gridImageHeight,
    height: 120
  },
  grid_label: {
    alignSelf: 'center',
    fontSize: 13,
    color: '#222222',
  },
  first_last_name: {
    fontSize: 14,
    alignSelf: 'flex-end',
    marginTop: 24,
    color: '#393e5c',
  },
  address_info: {
    fontSize: 14,
    alignSelf: 'flex-end',
    color: '#393e5c',
    marginTop: 4,
    textAlign: "right",

  },
  address_line_2: {
    fontSize: 14,
    alignSelf: 'flex-end',
    color: '#393e5c',
    marginTop: 4,
    textAlign: "right",
  },
  phone_email_info: {
    fontSize: 14,
    color: '#393e5c',
    flexWrap: 'wrap',
    overflow: 'scroll',
    alignSelf: 'flex-end',
  },
  vector_icon: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#393e5c',
  },
  phone_email_outer_contaier: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: "wrap",
    marginTop: 18,
    justifyContent: 'flex-end',
  },
  phone_contaier: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    alignSelf: "center",
    marginRight: 10
  },
  email_contaier: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  modal_style: {
    backgroundColor: '#fff',
    padding: 24,
  },
  modal_title: {
    justifyContent: 'center',
    fontSize: 12,
  },
  modal_options_container: {

  },
  modal_option: {
    marginTop: 12,
    fontSize: 18,
  },
  modal_option_cancel: {
    marginTop: 12,
    fontSize: 18,
    color: '#ff0000',
    alignSelf: 'flex-end',
  },
  home_white_button_wrapper: {
    paddingHorizontal: 50,
    backgroundColor: '#fff',
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: '#d4d4d4',
    alignItems: "center",
    minWidth: 120,
    textAlign: "center",
    borderWidth: 1,
  },
  button_style_white_text: {
    color: 'rgb(57,62,62)',
    fontSize: 14
  },
  button_container: {
    justifyContent: "center",
    alignItems: "flex-end",
    flexDirection: 'row',
    marginTop: 20
  },
  textInput: {
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 4,
    fontSize: 14,
    paddingHorizontal: 12,
    // paddingVertical: inputBoxVerticalPadding,
    borderColor: '#d4d4d4',
    color: 'rgba(0, 0, 0, 0.6)',
    alignItems: "center",
    width: "100%",
    backgroundColor: '#ffffff',
    height: (Platform.OS === "ios") ? 40 : 42,
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    alignItems: 'stretch',
  },
  text_amount: {
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 4,
    fontSize: 16,
    paddingHorizontal: 12,
    // paddingVertical: inputBoxVerticalPadding,
    borderColor: '#c4c4c4',
    color: 'rgba(0, 0, 0, 0.6)',
    alignItems: "center",

    backgroundColor: '#ffffff',
    height: (Platform.OS === "ios") ? 40 : 42,

    width: "30%",
    marginBottom: 10,
    justifyContent: "center"
  },
  textinputContainer: {
    width: "100%",
    flex: 1,
    color: 'rgba(0, 0, 0, 0.6)',
    backgroundColor: '#ffffff',
  },
  textAutoCompleteInput: {
    alignItems: "center",
    width: "100%",
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: "white",
    paddingHorizontal: 12,
    borderColor: '#d4d4d4',
    color: 'rgba(0, 0, 0, 0.6)',
    height: (Platform.OS === "ios") ? 40 : 42,
    paddingHorizontal: 12,

  },

  autoCompleteContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 12,
    paddingVertical: 10,
    borderColor: '#d4d4d4',
    backgroundColor: '#ffffff'
  },
  textInput_password: {
    borderRadius: 4,
    fontSize: 14,
    paddingHorizontal: 12,
    paddingVertical: inputBoxVerticalPadding,
    color: 'rgba(0, 0, 0, 0.6)',
    width: "80%",
    backgroundColor: '#ffffff'
  },
  inputWrap_password: { // Defined but not used please remove this.
    flexDirection: "row",
    flex: 1,
    alignItems: 'stretch',
    marginBottom: 0,
    borderRadius: 4,
    borderColor: '#d4d4d4',
    borderWidth: 1,
    backgroundColor: "#fff",
    width: "100%",
    height: (Platform.OS === "ios") ? 40 : 42,
  },
  inputWrap_username: { // Defined but not used please remove this.
    flexDirection: "row",
    flex: 1,
    alignItems: "stretch",
    marginBottom: 0,
    borderRadius: 5,
    height: (Platform.OS === "ios") ? 44 : 46,
  },
  touachableButton: {
    paddingTop: Platform.OS === 'ios' ? 8 : 8,
    paddingHorizontal: 5,
  },
  labelStyle: {
    fontSize: 10,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 1,
    marginVertical: 4,
    opacity: 0.6
  },
  labelStyleAcount: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 1,
    opacity: 0.6,
  },
  textInput_password_secure: {
    color: "#2dafd3",
    paddingTop: Platform.OS === 'ios' ? 2 : 2,
    paddingLeft: 5
  },
  greyColorText: {
    color: "rgba(51,54,59,0.6)"
  },
  hyperLinkText: {
    color: "blue",
    fontSize: 14,
    textAlign: "right"
  },
  activityIndicatorView: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: (Platform.OS == "web") ? 20 : 0,
    flex: 1,
    zIndex: 1,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },

  globalButtonColered: {
    justifyContent: "center",
    flexDirection: 'column',
    alignItems: "center",
    backgroundColor: "#db9360",
    width: 130,
    height: 40,
    borderRadius: 5,
  },
  globalButtonSimple: {
    justifyContent: "center",
    flexDirection: 'column',
    alignItems: "center",
    width: 130,
    height: 40,
    backgroundColor: '#dedbdb',
    borderRadius: 5,
  },
  buttonsContainer: {
    height: "40%",
    margin: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  container: {
    marginTop: (Platform.OS === "ios") ? 70 : 40,
    alignItems: 'center',
  },
  logo_image: {
    width: '60%',
    height: 60,
    resizeMode: "contain",
  },
  logo_text: {
    fontSize: 19,
    color: "#393e5c",
  },
  orange_btn_container: {
    paddingHorizontal:30,
    paddingVertical: 12,
    borderRadius: 5,
    backgroundColor: '#DB9360',
    alignItems: 'center',
    minWidth: 120,
    textAlign: 'center',
  },
  orange_btn_text: {
    color: '#fff',
    fontSize: 14,
  },
  grey_btn_container:
  {
    paddingHorizontal:30,
    backgroundColor: '#fff',
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: '#d4d4d4',
    alignItems: "center",
    minWidth: 120,
    textAlign: "center",
    borderWidth: 1,
  },
  grey_btn_text: {
    color: '#393e5c',
    fontSize: 14,
  },
  white_btn_container:
  {
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: 'black',
    borderWidth: 0.5,
    backgroundColor: '#fff',
    alignItems: 'center',
    minWidth: 120,
    marginLeft: 30,
    textAlign: 'center',
  },
  white_btn_text: {
    color: 'black',
    fontSize: 14,
  },
  text_input: {
    height: 44,
    width: '100%',
    fontSize: 14,
    borderColor: '#d4d4d4',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderRadius: 4,
    padding: 10,
    color: 'rgba(0, 0, 0, 0.6)',
  },
  lato_medium: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Medium",
    fontWeight: '400',
    fontStyle: 'normal'
  },
  lato_regular: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal',
  },
  lato_semibold: Platform.OS === "web" ? {
    fontWeight: '700',
  } : {
      fontFamily: "Lato-Semibold",
      fontWeight: '600',
      fontStyle: 'normal',
    },
  // pickerSelectStyles:{
  // },


  boldText: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold'
  },
  textInputBox: {
    marginTop: 10
  },
  pickerContainer: {
    borderWidth: 1,
    height: (Platform.OS === "ios") ? 40 : 42,
    width: 300,
    borderColor: "#D4D4D4",
    backgroundColor: "#fff",
    borderRadius: 6,
    color: "#d4d4d4",
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: "400",
    paddingRight: 0,
    fontStyle: "normal",
    shadowColor: "black",
    padding: (Platform.OS === "ios") ? 10 : 0,
    elevation: 1,
    fontSize: Platform.OS === "ios" ? 13 : 14,
  },
  errorTextContainer: {
    width: widthToDp('75'),
    marginVertical: heightToDp('-1.5'),
  },
  validationTick: {
    width: widthToDp('10'),
    height: heightToDp('5'),
    marginRight: widthToDp('-60'),
    marginTop: heightToDp('-6'),
  },
  validationTickImage: {
    width: widthToDp('10'),
    height: heightToDp('5'),
    overlayColor: 'white',
  },
  loadingIndicatorContainer: {
    flex: 1,
    backgroundColor: '#333',
    justifyContent: 'center',
    alignItems: 'center',

  },

  dashboardMainContainer: {
    height: screenHeight - 40,
    width: screenWidth,
    alignItems: "stretch"
  },
  pickerAndTextContainer: {
    width: "96%",
    height: "31%",
    margin: "2%",
    backgroundColor: "white",
    borderRadius: 8,
    marginTop: 15
  },
  dashboardImagePickerContiner: {
    width: "25%",
    height: "80%",
    alignItems: "center",
    borderRadius: 100,
    marginLeft: 40
  },
  dashboardDriverDetailsContainer: {
    width: "65%",
    height: "90%",
    flexDirection: "column",
    paddingRight: 15,
  },
  mailAndTlcContainer: {
    flexDirection: "column",
    height: "50%",
    paddingRight: 15,
    justifyContent: "center",
    paddingTop: 15
  },
  innerMailAndTlcContainer: {
    flexDirection: "row-reverse",
    alignItems: "center"
  },
  dasboardElementContainer: {
    height: "30%",
    width: "30%",
    backgroundColor: "white",
    borderRadius: 8,
  },
  textAlignDetails: {
    height: "100%",
    flexDirection: "column",
    alignItems: "stretch",
    borderWidth: 1
  },
  billingAndRentalContainer: {
    height: "27%",
    flexDirection: "row",
    justifyContent: "space-evenly"
  },
  carDocsAndSupportContainer: {
    flex: 3.5,
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  billigAndPaymentElement: {
    height: heightToDp('24'),
    width: widthToDp('47'),
    borderRadius: 8,
    backgroundColor: "white",
  },
  yourRentalElement: {
    height: heightToDp('24'),
    width: widthToDp('47'),
    borderRadius: 8,
    backgroundColor: "white",
  },
  carDocumentElement: {
    height: heightToDp('24'),
    width: widthToDp('47'),
    borderRadius: 8,
    backgroundColor: "white",
  },
  helpAndSupportElement: {
    height: heightToDp('24'),
    width: widthToDp('47'),
    borderRadius: 8,
    backgroundColor: "white",
  },
  dashbardGif: {
    height: heightToDp('20'),
    width: widthToDp('43'),
    margin: 5
  },
  dasboardText: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Medium",
    fontSize: 14,
    color: "#393E5C",
    textAlign: "center",
    letterSpacing: 0.25
  },
  MainContainer: {
    height: screenHeight,
    width: screenWidth,
  },
  linearGradient: {
    flex: 1,
  },
  logo: {
    width: 250,
    height: 60,
  },
  avatar: {
    width: 250,
    height: 150,
  },
  dashboardAvatar: {
    width: 200,
    height: 100,
    marginTop: -5,
  },
  carImageR:{
    width: 140,
    resizeMode: "contain",
    height: 140,
    alignSelf: 'center',
  },
  cameraPng: {
    marginTop: -20,
    justifyContent: "center",
    alignItems: "center"
  },
  landing_without_background: {
    height: 320,
    width: 300
  },
  headContainer: {
    height: "20%",
    alignItems: "center",
    flexDirection: "column",
    marginVertical: "5%",
  },
  footContainer: {
    marginVertical: 0,
    marginBottom: (Platform.OS === "ios") ? 40 : 15,
    flex: 1.3,
    alignItems: "center",
    flexDirection: "column",
    marginTop: (Platform.OS === "ios") ? 70 : 40,
  },
  login_heading_text: {
    fontSize: 19,
    color: "#393E5C",
    textAlign: "center",
  },
  login_text: {
    fontSize: 15,
    color: "white",
    textAlign: "center",
  },
  Signup_text: {
    fontSize: 19,
    color: "#393E5C",
    textAlign: "center",
  },
  TextInputContainer: {
    height: "80%"
  },
  loginButton: {
    justifyContent: "center",
    flexDirection: 'column',
    backgroundColor: "#db9360",
    width: 130,
    height: 40,
    borderRadius: 5,
    marginHorizontal: 90,
    marginVertical: 30
  },
  SignupButton: {
    justifyContent: "space-around",
    flexDirection: 'column',
    backgroundColor: "#B2BEB5",
    width: 130,
    height: 35,
    borderRadius: 5,
    marginHorizontal: 130,
    marginVertical: 10,
  },
  ContactInput: {
    backgroundColor: "white",
    width: 300,
    justifyContent: 'flex-start',
    alignContent: 'center',
    // flex:-0.1,
    height: 40,
    borderRadius: 10,
    borderWidth: 1,
    paddingHorizontal: 10,
    marginTop: "5%"
  },
  globalTextInput: {
    backgroundColor: "white",
    width: 300,
    justifyContent: 'flex-start',
    alignContent: 'center',
    borderRadius: 5,
    shadowColor: "black",
    elevation: 1,
    borderWidth: 1,
    borderRadius: 4,
    fontSize: 14,
    paddingHorizontal: 12,
    borderColor: '#d4d4d4',
    color: 'rgba(0, 0, 0, 0.6)',
    backgroundColor: '#ffffff',
    height: (Platform.OS === "ios") ? 40 : 42,
  },
  PasswordInput: {
    backgroundColor: "white",
    width: 300,
    justifyContent: 'space-around',
    // flex:-0.1,
    height: 40,
    marginVertical: 10,
    borderRadius: 10,
    borderWidth: 1,
    paddingHorizontal: 10
  },
  buttonsContainer: {
    flex: 1,
    justifyContent: 'center',
    borderWidth: 1
  },
  logo_text: {
    fontSize: 19,
    color: "#393e5c",
  },
  labelStyle: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 1,
    marginVertical: 4,
    opacity: 0.6
  },
  showhide_btn: {
    padding: 10,
    fontSize: 14,
    color: '#2dafd3'
  },
  error_text: {
    color: "#f59b42",
    fontSize: 12,
  },
  text_input_password_placeholder: {
    padding: 10,
    flex: 1,
    fontSize: 14
  },
  text_input_container_password: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    alignSelf: 'center',
    fontSize: 14,
    width: '100%',
    borderColor: '#d4d4d4',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderRadius: 4,
    color: '#000000',
  },
});
export const landing_style = StyleSheet.create({
  btn_wrapper: {
    marginVertical: screenHeight / 6,//200,'
  },
  landing_gif: {
    maxHeight: screenHeight / 2,//220,
    width: 300
  },
  footContainer: {
    marginTop: -100,
    alignItems: "center",
    flexDirection: "column",
  },
});
export const return_style = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    marginLeft:15,
    marginRight:15,
    marginTop:15,
  },
  main_label:{
    color: '#000',
    fontSize: 16,
  },
  calendarHide:{
    display:"none",
  },
  cal_image: {
    width: 25,
    resizeMode: "contain",
    marginTop: -3,
    alignItems: "center",
  },
  calenderDate: {
    flex: 1,
  },
  calenderIcon: {
    flexDirection: "row-reverse"
  },
})
export const modalStyle = StyleSheet.create({
  modal: Platform.OS === "web" ? {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    paddingVertical: 20,
    borderRadius: 5,
    borderWidth: 1,
    paddingHorizontal: 25,
    width: "100%",
    maxWidth: Platform.OS === "web" ? 480 : "100%",
    alignSelf: "center"
  } : {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: "#FFFFFF",
      paddingVertical: 20,
      borderRadius: 8,
      paddingHorizontal: 35,
    },
  modalText: Platform.OS === "web" ? {
    marginVertical: 10,
    fontSize: 14,
  } : {
      marginVertical: 10,
      fontSize: 14,
      fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
      fontWeight: '400',
      color: "rgba(0,0,0,0.6)",
      fontStyle: 'normal'
    },
  modalTextAmount: {
    marginVertical: 10,
    fontSize: 22,
    textAlign: 'center',
    fontFamily: 'Lato-Bold',
    fontWeight: '400',
    fontStyle: 'normal'
  },
  modal_textbuttonWrap: {
    paddingVertical: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal_wrapper: {
    width: "100%"
  },
  modal_textbuttonWrap_center: {
    paddingVertical: 10,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal_textbuttonWrap_single: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  modal_heading: {
    fontSize: 20,
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    color: "rgba(0, 0, 0, 0.87)",
    fontStyle: 'normal',
    paddingVertical: 15,
    paddingBottom: 10,
    textAlign: "center"
  },
  close_icon_img_wrap: {
    width: 40,
    height: 40,
    position: "absolute",
    right: -35,
    top: -10,
    alignContent: "center",
    flexDirection: "column",
    alignItems: "center"
  },
  close_icon_img: {
    width: 23,
    height: 23,
    resizeMode: "contain"
  },
  upload_document_modal: {
    backgroundColor: "#fff",
  },
  upload_document_text_modal: {
    color: "#2dafd3",
    fontSize: 16,
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal'
  },
  modal_check: {
    color: "green",
    fontSize: 35,
    paddingHorizontal: 10,
    paddingTop: 10,
    alignSelf: "center",
    paddingBottom: 7
  },
  modal_check_red: {
    color: "red",
    alignSelf: "center",
  },
  modal_check_error: {
    color: "red",
    alignSelf: "center",
    paddingHorizontal: 20,
    textAlign: "center"
  },
  modal_title: {
    justifyContent: 'center',
    fontSize: 12,
  },
  modal_option: {
    marginTop: 12,
    fontSize: 18,
  },
  modal_option_cancel: {
    marginTop: 12,
    fontSize: 18,
    color: '#ff0000',
    alignSelf: 'flex-end',
  },
  modal_style: {
    backgroundColor: '#fff',
    padding: 24,
  },
});
export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    paddingVertical: Platform.OS === "ios" ? 11 : 20,
    paddingHorizontal: 8,
    // marginHorizontal: 10,
    borderWidth: 1,
    borderColor: '#d4d4d4',
    backgroundColor: '#fff',
    borderRadius: 6,
    color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    paddingRight: 0,
    fontStyle: 'normal',
    fontSize: Platform.OS === "ios" ? 13 : 14,
  },
  RNPickerStyle: {
    width: "55%",
    margin: 18,
    borderWidth: 0.25,
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    borderRadius: 6,
    borderColor: "#33363b",
    position: "relative"
  },
  icon: {
    backgroundColor: 'transparent',
    borderTopWidth: 6,
    borderTopColor: 'gray',
    borderRightWidth: 6,
    borderRightColor: 'transparent',
    borderLeftWidth: 6,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
  }
});
export const chat_style = StyleSheet.create({
  main_container: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    backgroundColor: 'white',
    flex: 2,
    // height:(screenHeight/100)*60,
  },
  sent_message_container: {
    flexDirection: 'row',
    marginTop: 16,
    alignSelf: 'flex-start',
    flex: 1
  },
  received_image: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    marginTop: 4,
    marginRight: 10
    // marginStart: 16,
  },
  sent_message_box: {
    marginStart: 20,
    marginLeft: 10,
    marginEnd: 1,
    marginTop: 1,
    marginBottom: 1,
    backgroundColor: '#fff',
    borderRadius: 8,
    justifyContent: 'flex-end',
    padding: 16,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 1,
    elevation: 1,
    flexDirection: 'row-reverse',
    flexShrink: 1,
  },
  chat_text: {
    fontSize: 16,
    alignSelf: 'flex-start',
  },
  sent_image: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    marginTop: 4,
    marginStart: 10,
  },
  received_message_box: {
    alignSelf: 'flex-start',
    marginEnd: 1,
    marginTop: 1,
    marginBottom: 1,
    backgroundColor: '#E1A77E',
    borderRadius: 8,
    padding: 16,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    flexDirection: 'row-reverse',
    shadowOpacity: 0.25,
    shadowRadius: 1,
    elevation: 1,
    flexShrink: 1,
  },
  chat_text_box_container: {
    borderColor: '#D4D4D4',
    borderWidth: 1,
    borderRadius: 8,
    backgroundColor: '#fff',
    marginTop: 16,
    marginStart: 8,
    marginEnd: 8,
    paddingEnd: 16,
    paddingStart: 16,
    paddingTop: 8,
    paddingBottom: 8,
    flexDirection: 'row-reverse',
  },
  text_input: {
    width: '100%',
    height: 40,
    borderColor: '#d4d4d4',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderRadius: 4,
    color: '#000000',
    padding: 10,
  },
  received_message_container: {
    flexDirection: 'row-reverse',
    marginTop: 16,
  },
  picker_container: {
    marginTop: 8,
    marginLeft: 10,
    justifyContent: 'center',
    borderTopWidth: 1,
    borderTopColor: 'gray',
    borderRightWidth: 6,
    borderRightColor: 'transparent',
    borderLeftWidth: 6,
    borderLeftColor: 'transparent',
    width: '90%',
  },
  pickerSelectStyles: {
    marginLeft: 10,
    alignSelf: 'center'
  },
  text_input_container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 0.3,
    borderColor: '#000',
    height: 44,
    marginTop: 5,
    alignSelf: 'center',
    borderRadius: 5,
    width: '88%',
    bottom: 15,
  },
  iconStyle: {
    padding: 10,
    alignSelf: "center"
  }

});
export const account_tab = StyleSheet.create({
  logout_button: {
    alignItems: 'flex-end',
    marginTop: 15,
    marginRight: 15,
  },
  item_wrap: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    paddingLeft: 5,
    marginHorizontal: 15,
    marginVertical: 6,
    flex: 1,
    backgroundColor: "#fff",
    borderLeftWidth: 0,
    borderRadius: 5,
    flexDirection: "row",
    // justifyContent: 'center',
  },
  item_image_left: {
    width: 80,
  },
  item_image_right: {
    width: 300,
    position: "absolute",
    marginLeft: 80,
    marginTop: 15,
  },
  icon_input: {
    width: 80,
    height: 80,
    resizeMode: "contain"
  },
  item_content_text: {
    fontSize: 14,
    // fontWeight: "bold",
    flexWrap: "wrap",
    width: 180,
    color: "#33363b",
    opacity: 0.6
  },
  item_heading_text: {
    fontSize: 18,
    color: "#000000",
    opacity: 0.87,
  },
});
export const HadAccident_style = StyleSheet.create({
  main_container: {
    marginLeft: 10,
    marginTop: 10,
    marginRight: 10,
  },
});
export const billing_question_style = StyleSheet.create({
  main_container: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
  },
  block: {
    backgroundColor: 'white',
    marginVertical:5
  },
  descriptionStyling:{
    letterSpacing:0.5,
    fontSize: 18,
    color:"#7c7c7c",
    lineHeight:28,
    paddingLeft:20,
    paddingRight:30
  }
});
export const support_tab = StyleSheet.create({
  main_container: {
    marginLeft: 15,
    marginRight: 15,
  },
  text: {
    textAlign: 'center', fontSize: 20,
  },
  blocks: {
    // height:100,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 8,
    paddingVertical: 5,
    paddingHorizontal: 15,
    paddingLeft: 5,
    marginVertical: 6,
  },
  images_container: {
    width: 70,
    height: 75,
    alignSelf: 'center',
  },
  images: {
    resizeMode: 'contain',
    height: '100%',
    width: '100%',
  },
  btn_wrapper: {
    alignItems: 'center',
    marginTop: 10,
  },
  search_bar_container: {
    marginTop: 8,
    borderWidth: 1,
    borderColor: "#ccc",
    backgroundColor: "#fff",
    flexDirection: "row",
    borderRadius: 6,
    padding: 0,
    paddingBottom: 5,
    marginLeft: 15,
    marginRight: 15,
  },
  image_search_icon: {
    height: 22,
    marginTop: 10,
    marginLeft: 10,
    width: 22,
  },
  textInput: {
    marginTop: 4,
    borderRadius: 6,
    fontSize: 16,
    paddingHorizontal: 12,
    paddingVertical: 6,
    color: '#454545',
    opacity: 0.8,
    backgroundColor: '#ffffff',
    alignSelf: 'stretch',
    flex: 1,
  },
});
export const legal_style = StyleSheet.create({
  main_container: {
    marginTop: 10,
    marginLeft: 15,
    marginRight: 15,
  },
  item_container: {
    padding: 16,
    backgroundColor: 'white',
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  vector_icon: {
    fontSize: 18,
  },
  saperator: {
    backgroundColor: '#ccc',
    height: 0.5,
    alignSelf: 'stretch'
  },
  title_text: {
    fontSize: 16,
  },
});

export const terms_policy_style = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingHorizontal: 8,
    paddingTop: 16,
    paddingBottom: 12,
  },
  container: {
    backgroundColor: '#fff',
    padding: 16,
  },
  heading: {
    fontSize: 20,
    color: '#393e5c',
    alignSelf: 'center',
    marginBottom: 16,
  },
  title: {
    fontSize: 18,
    marginTop: 12,
  },
  content: {
    marginTop: 4,
    color: '#393e5c',
    fontSize: 14,
  },
});

export const notification_settings = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    padding: 22,
  },
  checkboxes_container: {
    flexDirection: "row",
    alignContent: "flex-end",
    alignSelf: "flex-end",
    justifyContent: 'flex-end',
  },
  checkboxes_item: {
    marginLeft: 40
  },
  vector_icon: {
    fontSize: 26,
  },
  title: {
    fontSize: 18,
    color: '#000',
  },
  title_2: {
    fontSize: 18,
    color: '#000',
    marginTop: 64,
  },
  item_text: {
    fontSize: 16,
    color: '#000',
  },
  setting_group: {
    marginTop: 16,
  },
  setting_item_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginVertical: 5,
    marginLeft: 10
  },
  switch_button: {
    width: 70,
    height: 70 * 0.52,
    marginTop: 4,
  },
  setting_title_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 35,
  },
  setting_title_2: {
    fontSize: 18,
    color: '#000',
  },
  switch_title_button: {
    width: 24,
    height: 18
  },
  setting_checkbox_button: {
    width: 22,
    height: 22
  }
});
export const had_an_Accident = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  headerText: {
    alignItems: "center",
    borderRadius: 8,
    backgroundColor: "white",
    margin: 8,
    flex: 1,
    justifyContent: 'center',
  },
  stepsContainer: {
    flex: 5,
    backgroundColor: "white",
    margin: 8,
    borderRadius: 8,
  },
});
export const billing_question = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  questionContainers: {
    flex: 1,
    borderRadius: 8,
    margin: 2,
    backgroundColor: "white",
    justifyContent: "center"
  },
  ansContainer1: {
    flex: 1.5,
    borderRadius: 8,
    margin: 3,
    backgroundColor: "white",
  },

  ansContainer2: {
    flex: 3,
    borderRadius: 8,
    margin: 3,
    backgroundColor: "white",
  },
  ansContainer3: {
    flex: 1.5,
    borderRadius: 8,
    margin: 3,
    backgroundColor: "white",
  },
});
export const need_a_tow = StyleSheet.create({
  main_container: {
    marginLeft: 5,
    marginRight: 5,
    marginTop:10,
    backgroundColor: 'white',
  },
});
export const buggy_hours_location = StyleSheet.create({
  main_container:{
    marginLeft:5,
    marginRight:5,
    marginTop:10,
    backgroundColor:'white',
},
location_text_line1:{
  fontSize: 20,
  color:'rgb(57,62,92)',

},
});
export const subscriptionsScreen = StyleSheet.create({
  main_container: {
    paddingTop: 22,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center'
  },
  sub_content_wrap: {
    flexDirection: "row",
    marginTop: 15,
  },
  sub_content_wrap_left: {
    width: 80,
  },
  sub_content_wrap_right: {
    marginLeft: 10
  },
  icon_input: {
    width: 80,
    height: 80,
    resizeMode: "contain"
  },
  headings: {
    fontSize: 18,
    color: 'black'
  },
  currentsub_text: {
    fontSize: 15,
    color: 'rgba(51, 54, 59, 0.6)'
  },
  view_car_btn: {
    flexDirection: "row",
    flex: 1,
    marginTop: 13,
    marginLeft: 15,
    alignItems: "center",
    alignSelf: "center",
  },
  view_car_btn_2: {
    flexDirection: "row",
    flex: 1,
    marginTop: 13,
    marginLeft: 15,
    alignItems: "flex-start",
    alignSelf: "flex-start",
  },
  cardocumentbutton: {
    backgroundColor: "#fff",
    borderColor: "#DB9360",
    borderWidth: 1,
    marginRight: 20,
    height: 35,
    borderRadius: 4,
  },
  buttonText: {
    color: "#393e5c",
    fontSize: 13,
    alignSelf: "center",
    flexDirection: "row",
    paddingVertical: 8,
    paddingHorizontal: 15,
  },
  list_item_container: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    marginTop: 20,
    // paddingHorizontal: 16,
    // paddingVertical: 8, 
  },
  list_item: {
    flex: 1,
  },
  btn_container:
  {
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: 'red',
    borderWidth: 0.5,
    backgroundColor: '#fff',
    alignItems: 'center',
    minWidth: 120,
    marginLeft: 35,
    textAlign: 'center',
  },
  tran_btn_container:
  {
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: 'black',
    borderWidth: 0.5,
    backgroundColor: '#fff',
    alignItems: 'center',
    width: 150,
    height: 44,
    textAlign: 'center',
  },


  boxMainContainer: {
    height: "100%"
  },
  waiverLogo: {
    width: 80,
    height: 80,
    resizeMode: "contain"
  },
  buttonsContainer: {
    // height:"7%",
    // flexDirection:'row',
    // margin:5
  },
  cancelButton: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    backgroundColor: "white",
    borderColor: button_default_color,
  },
  paymentMethodButton: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    backgroundColor: "white",
    borderColor: button_default_color,
  },
  showMoreButtonContainer: {
    // height:"10%",
    // width:"100%",
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10
  },
  showMoreButton: {
    height: "90%",
    width: "40%",
    borderWidth: 1,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "white",
    borderColor: button_default_color,
  },
  transactionContainer: {
    // height:"65%",
    marginTop: 30,

  },
  // centeredView: {
  //   flex: 2,
  //   justifyContent: "center",
  //   alignItems: "center",
  // },
  modalView: {
    flex: 1,
    justifyContent: "center",
    elevation: 2,
    flexDirection: "column",
    alignItems: "center",
    borderWidth: 1,

  },
  carDocs: {
    width: widthToDp('90'),
    height: heightToDp('100')
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 10,
    textAlign: "center",
    opacity: 1,
  },
  flatList: {
    flex: 1,
    borderColor: button_default_color,
    margin: 5,
    borderRadius: 8,
    height: heightToDp('8'),
    backgroundColor: "#f9f9f4",
  },
  noRecordFoundContainer: {
    height: "50%",
    alignItems: "center",
    justifyContent: "center",
  },
  noRecordFoundText: {
    color: "#676767",
    fontSize: 15
  },
});
export const notificationListScreen_style = StyleSheet.create({
  main_wrapper: {
    backgroundColor: '#fafafa'
  },
  notification_empty: {
    marginHorizontal: 20,
    marginVertical: 20
  },
  notification_wrap: {
    marginHorizontal: 0,
    marginBottom: 5
  },
  notification_item: {
    backgroundColor: '#fff',
    paddingHorizontal: 13,
    paddingVertical: 12,
    marginVertical: 10,
    marginHorizontal: 10,
    marginBottom: 0,
    zIndex: 10,
    shadowColor: "#000",
    zIndex: 10,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.29,
    shadowRadius: 1.0,
    elevation: 1,
    borderRadius: 2,
    borderLeftWidth: 0,
  },
  notification_item_red: {
    borderColor: "#db1a44"
  },
  notification_item_green: {
    borderColor: "#0bba3a"
  },
  notification_title: {
    fontSize: 12,
    marginLeft: 37,
    color: "#454545",
  },
  notification_desc: {
    fontSize: 11,
    marginLeft: 37,
    color: "#454545",
  },
  bell_icon: {
    color: "#adadad",
    position: "absolute",
    // left: 10,
    fontSize: 25,
    top: 13,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#adadad",
    padding: 2,
    // paddingVertical: 5,
    textAlign: "center"
  },
  notification_datetime: {
    color: "#adadad",
    fontSize: 10,
    marginLeft: 37,
  },
  notification_right_image_red: {
    position: "absolute",
    right: 0,
    fontSize: 10,
    // top: 0,
    opacity: 0.4
  },
  notification_right_image_green: {
    position: "absolute",
    right: 0,
    // fontSize: 10,
    // top: 5,
    opacity: 0.2
  },
  right_image_green_on_detail:{
    position: "absolute",
    right: 0,
    fontSize: 10,
    top: 5,
    opacity: 0.2
  },

});
export const signupScreen = StyleSheet.create({
  MainContainer: {
    height: screenHeight,
    width: screenWidth,
    backgroundColor: "#f9f9f4"
  },
  headContainer: {
    alignItems: "center",
    flexDirection: "column",
    marginTop: (Platform.OS === "ios") ? 70 : 80,
  },
  mainTextInputContainer: {
    height: "40%",
    alignItems: "center",
    marginTop: (Platform.OS === "ios") ? 50 : 50,
  },
  textInputContainer: {
    marginTop: 2
  },
  SignupButton: {
    justifyContent: "center",
    flexDirection: 'column',
    alignItems: "center",
    backgroundColor: "#db9360",
    width: 130,
    height: 40,
    borderRadius: 5,
  },
  cancelButton: {
    justifyContent: "center",
    flexDirection: 'column',
    alignItems: "center",
    backgroundColor: "#c4c4c4",
    width: 130,
    height: 40,
    borderRadius: 5,
    marginTop: 10
  },
  buttonsContainer: {
    margin: 40,
  },
  linearGradient: {
    flex: 1,
  },
  logo: {
    width: 250,
    height: 60,
  },
  avatar: {
    width: 250,
    height: 150,
  },
  login_heading_text: {
    fontSize: 19,
    color: "#393E5C",
    textAlign: "center",
  },
  login_text: {
    fontSize: 15,
    color: "white",
    textAlign: "center",
  },
  ContactInput: {
    backgroundColor: "white",
    width: 300,
    justifyContent: 'flex-start',
    alignContent: 'center',
    borderRadius: 10,
    borderColor: "#D2691E",
    borderWidth: 1,
    paddingHorizontal: 10,
    marginBottom: heightToDp('-1')
  },
  PasswordInput: {
    backgroundColor: "white",
    width: 300,
    justifyContent: "space-around",
    marginVertical: heightToDp('2'),
    borderRadius: 10,
    borderColor: "#D2691E",
    borderWidth: 1,
    paddingHorizontal: 10,
    marginBottom: heightToDp('-1')
  },
});
export const rentalTab = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  headerStyle: {
    flex: 0.7,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center"
  },
  rentalsDetailsContainer: {
    flex: 3,
    backgroundColor: "white",
    margin: 10,
    borderRadius: 8,
  },
  transHistoryContainer: {
    flex: 3.5,
    margin: 5,
    borderRadius: 8,
  },
  heading_item_date: {
    fontSize: 12,
    color: "#393E5C"
  },
  rentalCarImage: {
    width: 100,
    height: 110,
    borderRadius: 110 / 2,
    marginTop: 5,
    resizeMode: "contain",
    marginTop: 5,
    marginLeft:10,
    backgroundColor: '#F9F9F4',
    justifyContent: 'center',
    flex: 1,
  },
  directionsButton: {
    borderRadius: 5,
    borderWidth: 0.5,
    backgroundColor: '#fff',
    alignItems: 'center',
    minWidth: 120,
    marginLeft: 30,
    textAlign: 'center',
  },
  cardocumentbutton: {
    borderRadius: 5,
    borderColor: 'black',
    borderWidth: 0.5,
    backgroundColor: '#fff',
    alignItems: 'center',
    minWidth: 120,
    marginLeft: 30,
    textAlign: 'center',
  },
  buttonText: {
    color: "#393e5c",
    fontSize: 14,
    alignSelf: "center",
    flexDirection: "row",
    paddingVertical: 7,
    paddingHorizontal: 8,
    letterSpacing: 0.75,

  },
  view_car_btn: {
    flexDirection: "row",
    // flex: 1,
    justifyContent: 'flex-end',
    marginTop: 16,
    marginEnd: 15
  },
  icon_input: {
    width: 50,
    height: 49,
  },
  rental_list_title_left_icon: {
    width: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
  },
  flatList: {
    flex: 1,
    height: heightToDp('10'),
    backgroundColor:"white",
    margin:5,
    marginTop:1,
    marginBottom:1

  },
  view_detail: {
    paddingTop: 0,
    marginBottom: 5,
    marginLeft: 2
  },
  noRentalFoundView: {
    flex: 1,
    borderWidth: 1
  },
  rental_list_item_wrap: {
    backgroundColor: '#FFFFFF',
    paddingStart: 15,
    paddingEnd: Platform.OS == 'ios' ? 0 : 15,
    paddingVertical: 5,
    zIndex: 10,
  },
  rental_list_item: {
    backgroundColor: '#fff',
    flexDirection: "row",
    width: "100%"
  },
  rental_list_title_left: {
    padding: 0,
    width: 100,
  },
  rental_list_price_right: {
    width: 400,
    position: "absolute",
    alignItems: "flex-end",
    right: 0,
    borderColor: "#333",
  },
  rental_list_title: {
    color: '#393E5C',
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 10,
  },
  rental_date: {
    fontSize: 12,
    paddingTop: 5,
    color: "#393E5C"
  },
  rental_list_price_right_text: {
    fontSize: 14,
    marginBottom: 8,
    color: "#393E5C"
  },
  return_car_wrap: {
    marginTop: 0,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "#FFF",
    backgroundColor: "#fff",
    paddingVertical: 10,
    paddingTop: 0,
    margin: 10,
    marginBottom:5,
    borderRadius:5
  },
  return_car_title: {
    fontSize: 16,
    color: "#393E5C"
  },
  return_car_title_text: {
    fontSize: 14,
    color: "#393E5C"
  },
  return_car_link_text: {
    color: "#2dafd3",
  },
  icon_input_2: {
    width: 36,
    resizeMode: "contain"
  },
  return_car: {
    marginLeft: 10,
  },
});
export const billingTab = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1
  },
  icon_input: {
    width: 25,
    height: 25,
  },
  icon_input_2: {
    width: 38,
    height: 35,
  },
  icon_input_question: {
    width: 23,
    height: 23,
  },
  billing_details_icon: {
    marginRight: 16,
    marginTop: 2,
  },
  billing_details_title: {
    fontSize: 16,
    color: "#393E5C"
  },
  billing_details_title_text: {
    fontSize: 14,
    color: "#393E5C"
  },

  billing_details_wrap: {
    marginTop: 0,
    borderTopColor: "#f5f5f5",
    borderTopWidth: 1,
    flexDirection: "row",
    paddingHorizontal: 32,
    paddingTop: 16,
    width: "100%",
  },
  tool_charge_ammount: {
    fontSize: 15,
    color: 'rgba(51,54,59,0.6)',
    lineHeight:30,
    letterSpacing:0.5,
    fontSize:18
  },
  statement_title_text: {
    marginVertical: 0,
    marginBottom: 10,
    marginTop: 0,
    fontSize: 16,
  },
  statement_card_list: {
    marginLeft: -15,
  },
  notes: {
    fontSize: 12,
    marginEnd: 56,
    color: 'rgba(51,54,59,0.6)'
  },
  statement_wrapper: {
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 5,
    paddingTop: 10
  },
  statement_title: {
    borderBottomWidth: 0,
    borderBottomColor: "#ccc",
    padding: 10,
  },
  statement_contaner: {
    borderBottomWidth: 0,
    borderBottomColor: "#ccc",
    padding: 20,
    paddingLeft: 20,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 0,
    paddingVertical: 10,
    marginTop: 16,
  },
  statement_list_item: {
    flexDirection: "row",
    borderBottomColor: "#f5f5f5",
    borderBottomWidth: 1,

  },
  wrapper_info_main: {
    flex: 1,
    backgroundColor: '#f9f9f4',
  },
  statement_list_item_text: {
    marginLeft: 10,
    color: "#0B0B0C"
  },
  statement_list_item_price: {
    position: "absolute",
    color: "#0B0B0C",
    right: 0
  },
  save_container: {
    width: 130,
    backgroundColor: '#db9360',
    padding: 9,
    borderRadius: 6,
    alignItems: "center",
    marginStart: 16,
  },
  addbutton2: {
    paddingVertical: 5,
    paddingRight: 0,
    marginBottom: 10,
    borderRadius: 5,
    marginLeft: 1,
    position: "absolute",
    right: 0,
    top: 8
  },
  addbutton: {
    paddingVertical: 5,
    paddingRight: 0,
    marginBottom: 10,
    borderRadius: 5,
    marginLeft: 1,
    alignSelf: "flex-end",
  },
  buttonText: {
    fontSize: 15,
    color: "#2dafd3"
  },
  update_cards: {
    fontSize: 13,
    color: "#2dafd3",
    marginTop: 20,
  },
  cancel_container: {
    width: 130,
    backgroundColor: '#fff',
    padding: 9,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    borderColor: '#ccc',
  },
  cancel_container_text: {
    color: "#333333",
    fontSize: 13
  },
  button_container: {
    flexDirection: 'row',
    flex: 1,
    marginTop: 10,
    justifyContent: 'flex-end'
  },
  save_text: {
    color: '#FFFFFF',
    fontSize: 13,
    fontWeight: 'bold'
  },
  profile_field_value_wrapper: {
    paddingHorizontal: 0,
    flexDirection: "row",
    marginBottom: 0,
    paddingTop: 0,
  },
  pickerIos: {
    marginLeft: Platform.OS == 'ios' ? -10 : 0,
    marginTop: Platform.OS == 'ios' ? 5 : 0,
  },
  refunPopUpCotainer: {
    flex: 1,
    padding: 10,
    backgroundColor: "#393e5c",
    marginLeft: 15, marginRight: 15, marginTop: 10, borderRadius: 5,
    flexDirection: "row", alignItems: "center"
  },
  refundPopUpText: {
    color: "white",
    fontSize: 15,
    flex: 1

  },
  headerStyle: {
    flex: 1.3,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center"
  },
  listImage: {
    height: 30,
    width: 30,
  },
  invoiceContainer: {
    height: "25%",
    backgroundColor: "white",
    borderRadius: 8,
    marginLeft: 5,
    flexDirection: "column"
  },
  allTransaction: {
    flex: 5,
    backgroundColor: "white",
    marginTop: 15,
    borderRadius: 8,
  },
  invoiceFlatList: {
    flex: 0.5,
    borderRadius: 8,
    height: heightToDp('5'),
    marginLeft: 10
  },
  creditCardFlatList: {
    borderRadius: 8,
    height: heightToDp('5.5'),
    marginLeft: 20
  },
  showMoreButton: {
    flex: 1,
    borderWidth: 0.7,
    margin: 8,
    height: 35,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  payNowButton: {
    borderColor: button_default_color,
    backgroundColor: button_default_color,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    width: widthToDp('20'),
    height: heightToDp('5'),
    margin: 4
  },
  paymentMethodAndCreditCardContainer: {
    height: "20%",
    backgroundColor: "white",
    marginTop: 20,
    borderRadius: 5,
    marginBottom: 8
  },
  shareAppContainer: {
    height: "15%",
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center"
  },
  shareAppContainerDetails: {
    flex: 0.7,
    flexDirection: "row",
    alignItems: "center",
    margin: 10
  },
  pickerContainer: {
    flex: 0.5,
    flexDirection: "row",
    alignItems: "center",
    margin: 10
  },
  creditCardContainer: {
    height: "20%",
    backgroundColor: "white",
    borderRadius: 8,
    marginBottom: 8,
    marginTop: 5,
  },
  cardDetails: {
    flex: 1,
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  //for referFriendScreen

  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    padding: 16,
  },
  heading: {
    fontSize: 20,
    marginTop: 16,
    marginBottom: 16,
  },
  content: {
    color: '#393e5c',
    fontSize: 14,
  },
  image: {
    width: 160,
    height: 160 * 0.81,
    alignSelf: 'center',
    marginTop: 32,
  },
  step: {
    color: '#393e5c',
    marginTop: 16,
    fontSize: 14,
  },
  terms_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    alignSelf: 'center',
    marginStart: 16,
    borderColor: '#db9360',
    marginTop: 32,
  },
  terms_text: {
    color: '#FFFFFF',
  },
  link_text: {
    color: 'rgb(45,175,211)',
  },
});
export const transDetails = StyleSheet.create({
  mainContainer: {
    flex: 1,
    borderRadius: 8
  },
  headerContainer: {
    flex: 1,
    borderRadius: 8,
    marginTop:30,
    justifyContent: "center",
    alignItems: "center",
  },
  amountContainer: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    paddingTop:19,
    paddingHorizontal:10,
    
    borderBottomColor:"#f4f4f4",
  },
  paymentMethodContainerForReceipt: {
    flex: 0.5,
    flexDirection: "row",
    borderTopWidth: 0.3,
    justifyContent: "center",
    alignItems: "center"
  },
  paymentMethodContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#f5f5f5",
    borderBottomWidth: 1,
    backgroundColor:"white"
  },
  totalAmountContainer: {
    borderBottomWidth: 0.3
  },
  paymentDateContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
  },
  downloadButton: {
      backgroundColor: '#fff',
      paddingVertical: 12,
      borderRadius: 5,
      borderColor: '#d4d4d4',
      alignItems: "center",
      minWidth: 120,
      textAlign: "center",
      borderWidth: 1,
  },
  payNowButton: {
    margin: 10,
    width: widthToDp('40'),
    height: heightToDp('7'),
    backgroundColor: button_default_color,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
  }
});

export const updateCardInformation = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  error_label: {
    color: 'red',
    fontSize: 12,
  },
  save_container: {
    width: 140,
    backgroundColor: '#db9360',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    marginStart: 16,
    borderColor: '#db9360'
  },
  cancel_container: {
    width: 140,
    backgroundColor: '#dedbdb',
    padding: 8,
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    borderColor: '#dedbdb'
  },
  button_container: {
    flexDirection: 'row',
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  save_text: {
    color: '#FFFFFF',
    fontWeight: 'bold'
  },
  alertIconWrapper: {
    padding: 8,
    flex: 0.1,
  },
  info_main: {
    flex: 1,
    paddingTop: 15,
    paddingHorizontal: 0,
  },
  section_title: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 10,
    backgroundColor: '#EEE'
  },
  error_msg: {
    color: "red",
    fontSize: 12,
    paddingVertical: 0,
    paddingTop: 0
  },
  section_title_license: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 10,
    backgroundColor: '#EEE'
  },
  section_title_payment: {
    paddingVertical: 8,
    marginVertical: 8,
    paddingHorizontal: 10,
    backgroundColor: '#EEE'
  },
  section_title_text: {
    fontSize: 12,
  },
  edit_basic_main: {
    paddingVertical: 0,
    marginHorizontal: 0,
    marginBottom: 10
  },
  personal_detail_field_label: {
    color: 'black',
    fontSize: 12,
  },
  profile_field_value_wrapper: {
    paddingHorizontal: 0,
    marginBottom: 10,
    paddingTop: 0,
    marginTop: 0,
    marginLeft: 20,
    marginRight: 20,
  },
  input: {
    flex: 1,
    paddingHorizontal: 0,
  },
  input_cotainer: {
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 6,
    fontSize: 14,
    paddingHorizontal: 12,
    borderColor: '#efefed',
    marginRight: 10,
    color: '#242424',
    marginBottom: 10,
    backgroundColor: '#ffffff'
  },
  input_cotainer_out_input: {
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 6,
    fontSize: 14,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderColor: '#efefed',
    marginRight: 10,
    color: '#242424',
    marginBottom: 10,
    backgroundColor: '#ffffff'
  },
  profile_field_left: {
    flex: 1,
    flexDirection: "row",
    marginHorizontal: 0,
    marginBottom: 10
  },
  profile_field_1: {
    flex: 0.5,
    alignSelf: "flex-start",
    marginRight: 5,
  },
  profile_field_2: {
    flex: 0.5,
    alignSelf: "flex-start",
    marginRight: 5,
  },
  alertText: {
    color: '#c22',
    fontSize: 12,
    fontWeight: '400'
  },
  alertWrapper: {
    backgroundColor: '#ecb7b7',
    borderRadius: 5,
    paddingVertical: 8,
    paddingHorizontal: 14,
    marginBottom: 10,
    marginHorizontal: 0,
  },

  buttonView: {
    marginVertical: 30,
    flexDirection: "row-reverse",
    paddingHorizontal: 10,
  },
  checkoutButton: {
    // backgroundColor: Colors.color0,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    width: 130,
    alignSelf: "center",
    borderRadius: 5,
  },
  buttonActivityIndicator: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    paddingLeft: 60,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    width: 50,
    alignSelf: "flex-end",
  },
  checkoutButtonText: {
    color: "#FFF",
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  cancelButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    marginTop: 0,
    width: 80,
    alignSelf: "center",
    borderRadius: 5,
  },
  cancelButtonText: {
    fontSize: 13,
    fontWeight: "bold",
    alignSelf: "center"
  },
  iosStyle: {
    flex: 1,
    marginLeft: 0,
    marginTop: 0,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  androidStyle: {
    flex: 1,
    flexDirection: "row",
    marginLeft: -15,
    marginTop: -5,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  textInput: {
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 4,
    fontSize: 14,
    paddingHorizontal: 12,
    borderColor: '#d4d4d4',
    color: 'rgba(0, 0, 0, 0.6)',
    alignItems: "center",
    width: "100%",
    backgroundColor: '#ffffff',
    height: (Platform.OS === "ios") ? 40 : 42,
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    alignItems: 'stretch',
  },
  labelStyle: {
    fontSize: 10,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 1,
    marginVertical: 4,
    opacity: 0.6
  },
  bottom_style_orange_text: {
    color: "#fff",
    fontSize: 14
  },
  bottom_style_orange_wrapper: {
    paddingHorizontal: 30,
    backgroundColor: '#db9360',
    paddingVertical: 12,
    borderRadius: 5,
    alignItems: "center",
    minWidth: 120,
    textAlign: "center"
  },
  button_style_white_text: {
    color: 'rgb(57,62,62)',
    fontSize: 14
  },
  button_style_white_wrapper: {
    paddingHorizontal: 30,
    backgroundColor: '#fff',
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: '#d4d4d4',
    alignItems: "center",
    minWidth: 120,
    textAlign: "center",
    borderWidth: 1,
  },
  alertText: {
    color: '#c22',
    fontSize: 12,
    fontWeight: '400'
  },
  alertWrapper: {
    backgroundColor: '#ecb7b7',
    borderRadius: 5,
    paddingVertical: 8,
    paddingHorizontal: 14, 
    marginBottom: 10,
    marginHorizontal: 0
  },
});
export const modal_style = StyleSheet.create({
  modal: Platform.OS === "web" ? {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    paddingVertical: 20,
    borderRadius: 5,
    borderWidth: 1,
    paddingHorizontal: 25,
    width: "100%",
    maxWidth: Platform.OS === "web" ? 480 : "100%",
    alignSelf: "center"
  } : {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: "#FFFFFF",
      paddingVertical: 20,
      borderRadius: 8,
      paddingHorizontal: 35,
    },
  modalText: Platform.OS === "web" ? {
    marginVertical: 10,
    fontSize: 14,
  } : {
      marginVertical: 10,
      fontSize: 14,
      fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
      fontWeight: '400',
      color: "rgba(0,0,0,0.6)",
      fontStyle: 'normal'
    },
  modalTextAmount: {
    marginVertical: 10,
    fontSize: 22,
    textAlign: 'center',
    fontFamily: 'Lato-Bold',
    fontWeight: '400',
    fontStyle: 'normal'
  },
  modal_textbuttonWrap: {
    paddingVertical: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal_wrapper: {
    width: "100%"
  },
  modal_textbuttonWrap_center: {
    paddingVertical: 10,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal_textbuttonWrap: {
    paddingVertical: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal_textbuttonWrap_single: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  modal_heading: {
    fontSize: 20,
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    color: "rgba(0, 0, 0, 0.87)",
    fontStyle: 'normal',
    paddingVertical: 15,
    paddingBottom: 10,
    textAlign: "center"
  },
  close_icon_img_wrap: {
    width: 40,
    height: 40,
    position: "absolute",
    right: -35,
    top: -10,
    alignContent: "center",
    flexDirection: "column",
    alignItems: "center"
  },
  close_icon_img: {
    width: 23,
    height: 23,
    resizeMode: "contain"
  },
  upload_document_modal: {
    backgroundColor: "#fff",
  },
  upload_document_text_modal: {
    color: "#2dafd3",
    fontSize: 16,
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal'
  },
  modal_check: {
    color: "green",
    fontSize: 35,
    paddingHorizontal: 10,
    paddingTop: 10,
    alignSelf: "center",
    paddingBottom: 7
  },
  modalHideAndShowEye: {
    color: "#db9360",
    fontSize: 25,
    paddingHorizontal: 10,
    alignSelf: "center",
    justifyContent:"center",
    paddingBottom: 7,
    paddingTop:7
  },
  modal_check_red: {
    color: "red",
    alignSelf: "center",
  },
  modal_check_error: {
    color: "red",
    alignSelf: "center",
    paddingHorizontal: 20,
    textAlign: "center"
  },
});
export const updatelicenseInfo = StyleSheet.create({
  main_container: {
    paddingTop: 22,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center'
  },
  textInputlabel: {
    fontSize: 12,
    color: '#000000',
    opacity: 0.6
  },
  separator: {
    height: 2,
  },
  textInput_separator: {
    marginTop: 10,
  },
  btn_container: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center'
  },
});
export const updatePersonalInformation = StyleSheet.create({
  main_container:{
    paddingTop:22,
    marginLeft:20, 
    marginRight:20, 
    marginBottom:20,
    
  },
  text:{
  textAlign:'left',
  fontSize:12,
  letterSpacing:2,
  marginVertical: 4,
  // paddingTop:10,
  color:"rgba(51,54,59,0.6)",
  },
  icon: {
    backgroundColor: 'transparent',
    borderTopWidth: 6,
    borderTopColor: 'gray',
    borderRightWidth: 6,
    borderRightColor: 'transparent',
    borderLeftWidth: 6,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
  },
  textinput_spacing: {
    paddingTop: 20,
  },
  split_container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  half_container_left: {
    flex: 1,
  },
  half_container_right: {
    flex: 1,
    marginStart: 16,
    marginTop:12,
  }
});
export const buttonsClass = StyleSheet.create({
  buttonWithBackgronundColor:{

  },
  buttonWithoutBackgroundColor:{

  }
})
export const stripeScreen = StyleSheet.create({
  mainContainer:{
    height:screenHeight,
    width:screenWidth,
    backgroundColor:"#f9f9f4",
  },
  sectionDescription:{
    fontSize: 16,
    textAlign:"center",
    paddingVertical:16,
    lineHeight:25,
    color:"#858b94"
},
amountTextfieldContainer:{
    flexDirection:"row",
    alignSelf:"center",
    alignItems:"center",
    backgroundColor:"#FFF",
    padding:5,
    paddingHorizontal:18,
    borderRadius:6,
    marginBottom:15
},
textInput: {  
  fontSize: 22, 
  paddingHorizontal: 5, 
  paddingBottom:8,
  paddingTop:10,
  color: '#393e5c', 
},
lineZelle:{
  flexDirection:"row",
  borderTopColor:"#858b94",
  borderTopWidth:0.1,
  paddingHorizontal:15,
  backgroundColor:"#fff"
},
lineZelleIcon:{
  backgroundColor:"#f9f9f4",
  borderRadius:70,
  marginVertical:5,
  padding:10,
  paddingVertical:15,
  justifyContent:"center",
  alignContent:"center",
  alignItems:"center",
},line_zelle_content:{
  marginHorizontal:15,
  paddingVertical:15
},  
lineZelleText:{
 fontSize:16,
 color:"#858b94"
},
dollorSign:{
  color: '#393e5c',
  fontSize:22 
},
lineZelleContent:{
  marginHorizontal:15,
  paddingVertical:15
},
lineZelleTextSmall:{
 fontSize:14,
 color:"#858b94"
},
lineZelleAccordion:{
 position: "absolute",
 right:0,
 marginTop:30,
 marginRight:20
},
payLaterMethodDescLink: {
  fontSize: 14,
  color: "#2dafd3", 
},
});
export const toolsAndTicketScreen = StyleSheet.create({
  main_container: {
    backgroundColor: '#f9f9f4',
    flex: 1,
    paddingBottom: 16,
},
title: {
    fontSize: 16,
    margin: 16,
    textAlign: 'center',
},
label_container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingVertical: 12,
},
label_left: {
    flex: 0.6,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 15,
    textAlign: 'left',
},
label_center: {
    flex: 1,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 15,
    textAlign: 'center',
},
label_right: {
    flex: 0.5,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 15,
    textAlign: 'center',
},
content_container_with_no_back_color: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 12,
    paddingHorizontal: 16,
},
content_container_with_back_color: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    paddingVertical: 12,
    paddingHorizontal: 16,
},
content_left: {
    flex: 0.6,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 14,
    textAlign: 'left',
},
content_center: {
    flex: 1,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 14,
    textAlign: 'center',
},
content_right: {
    flex: 0.5,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 14,
    textAlign: 'center',
},
info: {
    margin: 16,
    color: '#000',
    fontSize: 14,
},
toll_label_left: {
    flex: 1,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 15,
    textAlign: 'left',
},
toll_label_right: {
    flex: 1,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 15,
    textAlign: 'center',
},
toll_content_left: {
    flex: 1,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 14,
    textAlign: 'left',
},
toll_content_right: {
    flex: 1,
    color: 'rgba(0,0,0,0.6)',
    fontSize: 14,
    textAlign: 'right',
},
link_text: {
    color: 'rgb(45,175,211)',
},
});