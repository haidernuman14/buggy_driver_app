import React, { useContext } from 'react'; //Unused import function "useContext"

import { I18nManager } from 'react-native'; //Unused import function "I18nManager"
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";

//Constant defined to change the language of the app
const translationGetters = {
    es: () => require("../assets/translations/es.json"),
    en: () => require("../assets/translations/en.json")
};

const setI18nConfig = (lang="en") => {

    // check and validate the language setting and make language changes
    let languageTag = 'en';
    if (RNLocalize.getCountry() == 'ES' || RNLocalize.getCountry() == 'es') {
        languageTag = 'es';
    }  

    languageTag = lang;
    translate.cache.clear();
    //  I18nManager.forceRTL(isRTL);
    i18n.translations = { [languageTag]: translationGetters[languageTag]() };
    i18n.locale = languageTag;
     

};

const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
//export default {translate};
setI18nConfig()
export {translate,setI18nConfig};
