import React from 'react';
import { Text } from 'react-native';

/**
 * Styled text with space-mono font type
 */
export function MonoText(props) {
  return (
    <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />
  );
}
