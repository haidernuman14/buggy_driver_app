import React from 'react';
import { StyleSheet, View, Platform, Text, Image } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon3 from 'react-native-vector-icons/Feather';
import Icon4 from 'react-native-vector-icons/FontAwesome5';
import Icon5 from 'react-native-vector-icons/Fontisto';
import Icon6 from 'react-native-vector-icons/MaterialIcons';
import Colors from '../constants/Colors';
import { global_styles  } from '../constants/Style'; 
/**
 * This class creates Icon for bottom navigation Tab Bar.
 */
export default function TabBarIcon(props) {

  // Render Tab Bar icons by checking icon name
  return (
      <>

        {props.type_name=="billing" && <View style={styles.bottom_tab_icon}>
            {props.focused ? 
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/billing-icon30x30.png")} />
                :
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/billing-icon-blue30x30.png")} />
            }
            <Text style={[(props.focused )?styles.tab_text:styles.tab_text2,global_styles.lato_regular]}>Billing</Text>
          </View>}

          {props.type_name=="profile" && <View style={styles.bottom_tab_icon}>
            {props.focused ? 
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/account-icon30x30.png")} />
                :
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/account-icon-blue30x30.png")} />
            }
            <Text style={[(props.focused )?styles.tab_text:styles.tab_text2,global_styles.lato_regular]}>Account</Text>
          </View>}

          {props.type_name=="car" && <View style={styles.bottom_tab_icon}>
            {props.focused ? 
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/rental-icon30x30.png")} />
                :
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/rental-icon-blue30x30.png")} />
            }
            <Text style={[(props.focused )?styles.tab_text:styles.tab_text2,global_styles.lato_regular]}>Rental</Text>
          </View>}

          {props.type_name=="account" && <View style={styles.bottom_tab_icon}>
            {props.focused ? 
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/dashboard-icon30x30.png")} />
                :
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/dashboard-icon-blue30x30.png")} />
            }
            <Text style={[(props.focused )?styles.tab_text:styles.tab_text2,global_styles.lato_regular]}>Dashboard</Text>
          </View>} 
            
          {props.type_name=="chat" && <View style={styles.bottom_tab_icon}>
            {props.focused ? 
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/chat-icon30x30.png")} />
                :
                <Image width="25" style={styles.icon_input} source={require("../assets/images/bottom_tab_nav/chat-icon-blue30x30.png")} />
            }
            <Text style={[(props.focused )?styles.tab_text:styles.tab_text2,global_styles.lato_regular]}>Support</Text>
          </View>} 
      
 
      </>) 

}

// Stylesheet to design Tabbar
const styles = StyleSheet.create({
  bottom_tab_icon:{ 
    alignContent:"center",
    paddingTop:20,
  },
  tab_text:{
    fontSize:10,
    color:"#db9461",
    marginTop:3,
  },
  tab_text2:{
    fontSize:10,
    marginTop:3,
    color:"#3a3f5d"
  },
  icon_input: {
    width: 25,
    height: 25,
    alignSelf:"center", 
  }, 
  icon_val_ios1:{
    paddingTop:7,
    paddingBottom:10,
    paddingLeft:10,
    paddingRight:10
  },
  icon_val1:{
    paddingHorizontal: 13
  },
  icon_val_ios2:{
    paddingTop:7,
    paddingBottom:10,
    paddingLeft:7,
    paddingRight:10
  },
  icon_val2:{
    paddingHorizontal: 0,
    marginBottom: -10, 
  },
  inactive_icon: {
    marginBottom: -10,
    color:"#CCCCCC"
  }

})
