export default function firstApi(token,phone) {
  let url = 'https://api2.frontapp.com/contacts/alt:phone:'+phone;
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Access-Control-Allow-Origin':'*',
        'Authorization': 'Bearer ' + token
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        resolve(responseData);
      })
      .catch(err => {
        reject(err);
      });
  })
}