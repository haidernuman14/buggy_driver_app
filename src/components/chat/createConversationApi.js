export default function createConversationApi(token,url,channel_id,message,to) {
 
  console.log("create-message",url,token,{
    channel_id:channel_id,
    body: message,
    to: to
  }); 

  if(message==""){
    return;
  }
 

  let options = {
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    method: 'POST',
    body: JSON.stringify({
      "options": {
        "archive": true
      },
      "channel_id":channel_id,
      "body": message,
      "to": [to]
    })
  };

  console.log("CUSTOM ARRAY---",options); 
    return new Promise((resolve, reject) => {
         fetch(url, options)
          .then((response) => response.json())
          .then((responseData) => {
            resolve(responseData);
          })
          .catch(err => {
            resolve(err);
          });
   });

  }