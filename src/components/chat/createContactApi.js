export default function createContactApi(token,url,name,phone) {
  /*console.log("send-message",url,token,{
    conversation_id:conversation_id,
    body: message,
    to: to
  });
  return; */
  if(phone==""){
    return;
  }

   
  let options = {
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    method: 'POST',
    body: JSON.stringify({
      "handles": [{
        "source": "phone",
        "handle": phone
      }],
      "name":name  
    })
  };

  console.log("CUSTOM ARRAY---",options); 
    return new Promise((resolve, reject) => {
         fetch(url, options)
          .then((response) => response.json())
          .then((responseData) => {
            resolve(responseData);
          })
          .catch(err => {
            resolve(err);
          });
   });

}