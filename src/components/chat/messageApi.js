export default function messageApi(token,url) {
    return new Promise((resolve, reject) => {
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      })
        .then((response) => response.json())
        .then((responseData) => {
          resolve(responseData);
        })
        .catch(err => {
          reject(err);
        });
    })
  }