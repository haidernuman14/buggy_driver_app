export default function sendMessageApi(token,url,message,to,conversation_id) {
  /*console.log("send-message",url,token,{
    conversation_id:conversation_id,
    body: message,
    to: to
  });
  return; */
  if(message==""){
    return;
  }

  let options = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer ' + token
    },
    method: 'POST'
  };

  let body = new FormData(); 
  body.append("conversation_id", conversation_id);
  body.append("body", message);
  body.append("to", [to]);
  options.body = body; 
  console.log("CUSTOM ARRAY",body,to); 
    return new Promise((resolve, reject) => {
         fetch(url, options)
          .then((response) => response.json())
          .then((responseData) => {
            resolve(responseData);
          })
          .catch(err => {
            resolve(err);
          });
   });

}