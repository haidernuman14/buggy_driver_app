import React from 'react';
//const tokenUrl = 'https://sandbox-identity.onsched.com/connect/token';
const tokenUrl = 'https://identity.onsched.com/connect/token';
const details = {
    'grant_type': 'client_credentials',
    'scope': 'OnSchedApi',
    //'client_id': 'clientsb1582914226',//sandbox
    //'client_secret': 'N1PQTlr2MnWLAGvjHA3I',//sandbox
    'client_id': 'client1582914308',//Live
    'client_secret': '3SfozWM5Zzfg8VqB7S1W'//Live
};
var formBody = [];
for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
}
formBody = formBody.join("&");

export default function TokenApi(apiMode) {
    let tokenUrl = 'https://identity.onsched.com/connect/token';
    let client_id = 'client1582914308';
    let client_secret= '3SfozWM5Zzfg8VqB7S1W';
    if (apiMode == 'sandbox'){
        tokenUrl = 'https://sandbox-identity.onsched.com/connect/token';
        client_id = 'clientsb1582914226';
        client_secret = 'N1PQTlr2MnWLAGvjHA3I';
    }
    
    const details = {
        'grant_type': 'client_credentials',
        'scope': 'OnSchedApi',
        'client_id': client_id,
        'client_secret': client_secret
    };
    var formBody = [];
    for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    return new Promise((resolve, reject) => {
        fetch(tokenUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then((response) => {
            if (response.ok) {
                return response.json()
            }
            reject(response)
        })
            .then((responseData) => {
                resolve(responseData);
            })
            .catch(err => {
                //console.log(err);
                reject(err);
            });
    })
}