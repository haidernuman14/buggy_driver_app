export const coupon_list = [
    {
      id:1,
      type:"fixed_percentage",
      code:"CD20",
      title:'Get 20% off by "CD20" promo code',
      value:"20",
    },
    {
      id:2,
      type:"fixed_price",
      code:"HY10",
      title:'Get $10 off by "HY10" promo code',
      value:"10",
    },
    {
      id:3,
      type:"fixed_percentage",
      code:"RU30",
      title:'Get 30% off by "RU30" promo code',
      value:"30",
    },
    {
      id:4,
      type:"fixed_percentage",
      code:"RJ5",
      title:'Get 5% off by "RJ5" promo code',
      value:"5",
    },
];
 