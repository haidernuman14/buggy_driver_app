import { StyleSheet } from 'react-native';
import { Platform, Dimensions } from 'react-native';
import Colors from './Colors';

/**
 * Define font style other constants
 */

const win = Dimensions.get('window');

const baseHeight = 896;
const inputBoxVerticalPadding = win.height >= baseHeight ? 10 : 6;

export const global_styles = StyleSheet.create({
  lato_bold: Platform.OS === "web" ? {} : {
    fontFamily: 'Lato-Bold',
    fontWeight: '700',
    fontStyle: 'normal'
  },
  lato_semibold: Platform.OS === "web" ? {
    fontWeight: '700',
  } : {
      fontFamily: 'Lato-Semibold',
      fontWeight: '600',
      fontStyle: 'normal',
      
    },
  lato_regular: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal'
  },

  lato_capital: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal',
    textTransform: 'uppercase'

  },
  lato_medium: Platform.OS === "web" ? {} : {
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Medium",
    fontWeight: '400',
    fontStyle: 'normal'
  },
  lato_regular_android: Platform.OS === "web" ? {} : {
    fontFamily: 'Lato-Regular',
  },
  activityIndicatorView: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: (Platform.OS == "web") ? 20 : 0,
    flex: 1,
    zIndex: 1,
  },
  headContainer: {
    marginHorizontal: 50,
    marginVertical: 40,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  logo: {
    //width: win.width/1.5,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  header_bell_icon: {
    paddingRight: 10,
    paddingLeft: 12,
    marginRight: 10
  },
  header_notificationText: {
    color: "#FFF", fontSize: 9, fontWeight: "500"
  },
  header_notificationContainer: {
    position: 'absolute',
    top: Platform.OS === "ios" ? -3 : -5,
    right: 15
  },
  bottom_style_orange_wrapper: {
    paddingHorizontal: 30,
    backgroundColor: '#db9360',
    paddingVertical: 12,
    borderRadius: 5,
    alignItems: "center",
    minWidth: 120,
    textAlign: "center"
  },
  bottom_style_orange_text: {
    color: "#fff",
    fontSize: 14
  },
  bottom_style_gray_wrapper: {
    paddingHorizontal: 30,
    backgroundColor: '#dedbdb',
    paddingVertical: 12,
    borderRadius: 5,
    alignItems: "center",
    minWidth: 120,
    textAlign: "center"
  },
  bottom_style_gray_text: {
    color: "#393e5c",
    fontSize: 14
  },
  button_style_white_wrapper: {
    paddingHorizontal: 30,
    backgroundColor: '#fff',
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: '#d4d4d4',
    alignItems: "center",
    minWidth: 120,
    textAlign: "center",
    borderWidth: 1,
  },

  home_white_button_wrapper:{
    paddingHorizontal: 50,
    backgroundColor: '#fff',
    paddingVertical: 12,
    borderRadius: 5,
    borderColor: '#d4d4d4',
    alignItems: "center",
    minWidth: 120,
    textAlign: "center",
    borderWidth: 1,
  },
  button_style_white_text: {
    color: 'rgb(57,62,62)',
    fontSize: 14
  },
  button_container: {
    justifyContent: "center",
    alignItems: "flex-end",
    flexDirection: 'row',
    marginTop: 20
  },
  textInput: {
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 4,
    fontSize: 14,
    paddingHorizontal: 12,
    // paddingVertical: inputBoxVerticalPadding,
    borderColor: '#d4d4d4',
    color: 'rgba(0, 0, 0, 0.6)',
    alignItems:"center",
    width: "100%",
    backgroundColor: '#ffffff',
    height: (Platform.OS === "ios") ? 40 : 42,
    flexDirection:"row",
    justifyContent:"space-between",
    flex: 1,
    alignItems: 'stretch',
  },
  text_amount:{
    marginTop: 4,
    borderWidth: 1,
    borderRadius: 4,
    fontSize: 16,
    paddingHorizontal: 12,
    // paddingVertical: inputBoxVerticalPadding,
    borderColor: '#c4c4c4',
    color: 'rgba(0, 0, 0, 0.6)',
    alignItems:"center",
    
    backgroundColor: '#ffffff',
    height: (Platform.OS === "ios") ? 40 : 42, 
  
    width:"30%",
    marginBottom:10,
    justifyContent:"center"
  },
  textinputContainer:{
    width:"100%",
    flex:1,
    color: 'rgba(0, 0, 0, 0.6)',
    backgroundColor: '#ffffff',
  },
    textAutoCompleteInput:{
      alignItems:"center",
      width: "100%",
      borderWidth: 1,
      borderRadius: 4,
    backgroundColor:"white",
    paddingHorizontal: 12,
    borderColor: '#d4d4d4',
    color: 'rgba(0, 0, 0, 0.6)',
    height: (Platform.OS === "ios") ? 40 : 42,
    paddingHorizontal: 12,
    
    },

    autoCompleteContainer:{
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: 4,
      borderWidth: 1,
      borderRadius: 6,
      paddingHorizontal: 12,
      paddingVertical: 10,
      borderColor: '#d4d4d4',
      backgroundColor: '#ffffff'
    },
  textInput_password: {
    borderRadius: 4,
    fontSize: 14,
    paddingHorizontal: 12,
    paddingVertical: inputBoxVerticalPadding,
    color: 'rgba(0, 0, 0, 0.6)',
    width: "80%",
    backgroundColor: '#ffffff'
  },
  inputWrap_password: { // Defined but not used please remove this.
    flexDirection: "row",
    flex: 1,
    alignItems: 'stretch',
    marginBottom: 0,
    borderRadius: 4,
    borderColor: '#d4d4d4',
    borderWidth: 1,
    backgroundColor: "#fff",
    width: "100%",
    height: (Platform.OS === "ios") ? 40 : 42,
  },
  inputWrap_username: { // Defined but not used please remove this.
    flexDirection: "row",
    flex: 1,
    alignItems: "stretch",
    marginBottom: 0,
    borderRadius: 5,
    height: (Platform.OS === "ios") ? 44 : 46,
  },
  touachableButton: {
    paddingTop: Platform.OS === 'ios' ? 8 : 8,
    paddingHorizontal: 5,
  },
  labelStyle: {
    fontSize: 10,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 1,
    marginVertical: 4,
    opacity: 0.6
  },
  labelStyleAcount: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.6)',
    letterSpacing: 1,
    opacity: 0.6,
  },
  textInput_password_secure: {
    color: "#2dafd3",
    paddingTop: Platform.OS === 'ios' ? 2 : 2,
    paddingLeft: 5
  },
});
// Stylesheet to design modal
export const modal_style = StyleSheet.create({
  modal: Platform.OS === "web" ? {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    paddingVertical: 20,
    borderRadius: 5,
    borderWidth: 1,
    paddingHorizontal: 25,
    width: "100%",
    maxWidth: Platform.OS === "web" ? 480 : "100%",
    alignSelf: "center"
  } : {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: "#FFFFFF",
      paddingVertical: 20,
      borderRadius: 8,
      paddingHorizontal: 35,
    },
  modalText: Platform.OS === "web" ? {
    marginVertical: 10,
    fontSize: 14,
  } : {
      marginVertical: 10,
      fontSize: 14,
      fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
      fontWeight: '400',
      color: "rgba(0,0,0,0.6)",
      fontStyle: 'normal'
    },
  modalTextAmount: {
    marginVertical: 10,
    fontSize: 22,
    textAlign: 'center',
    fontFamily: 'Lato-Bold',
    fontWeight: '400',
    fontStyle: 'normal'
  },
  modal_textbuttonWrap: {
    paddingVertical: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal_wrapper: {
    width: "100%"
  },
  modal_textbuttonWrap_center: {
    paddingVertical: 10,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal_textbuttonWrap_single: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  modal_heading: {
    fontSize: 20,
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    color: "rgba(0, 0, 0, 0.87)",
    fontStyle: 'normal',
    paddingVertical: 15,
    paddingBottom: 10,
    textAlign: "center"
  },
  close_icon_img_wrap: {
    width: 40,
    height: 40,
    position: "absolute",
    right: -35,
    top: -10,
    alignContent: "center",
    flexDirection: "column",
    alignItems: "center"
  },
  close_icon_img: {
    width: 23,
    height: 23,
    resizeMode: "contain"
  },
  upload_document_modal: {
    backgroundColor: "#fff",
  },
  upload_document_text_modal: {
    color: "#2dafd3",
    fontSize: 16,
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal'
  },
  modal_check: {
    color: "green",
    fontSize: 35,
    paddingHorizontal: 10,
    paddingTop: 10,
    alignSelf: "center",
    paddingBottom: 7
  },
  modal_check_red: {
    color: "red",
    alignSelf: "center",
  },
  modal_check_error: {
    color: "red",
    alignSelf: "center",
    paddingHorizontal: 20,
    textAlign: "center"
  },
});
// Stylesheet to design drop down picker
export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    paddingVertical: Platform.OS === "ios" ? 11 : 20,
    paddingHorizontal: 8,
    marginTop:5,
    // marginHorizontal: 10,
    borderWidth: 1,
    borderColor: '#E6B390',
    backgroundColor: '#fff',
    borderRadius: 6,
    color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    paddingRight: 0,
    fontStyle: 'normal',
    fontSize: Platform.OS === "ios" ? 13 : 14,
  },
  placeholder: {
    color: 'black',
    fontSize: 14,
  },
  inputAndroid: {
    paddingHorizontal: 8,
    paddingRight: 0,
    paddingVertical: 6,
    // marginHorizontal: 10,
    marginLeft: 0,
    marginTop: Platform.OS === "ios" ? 0 : 4,
    borderWidth: 1,
    borderColor: '#d4d4d4',
    backgroundColor: '#fff',
    borderRadius: 6,
    color: '#393e5c',
    fontFamily: Platform.OS === "ios" ? "Lato" : "Lato-Regular",
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 13,
  },
  iconContainer: {
    top: Platform.OS === "ios" ? 17 : 22,
    right: Platform.OS === "ios" ? 16 : 10,
  },
  icon: {
    backgroundColor: 'transparent',
    borderTopWidth: 6,
    borderTopColor: 'gray',
    borderRightWidth: 6,
    borderRightColor: 'transparent',
    borderLeftWidth: 6,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
  }
});