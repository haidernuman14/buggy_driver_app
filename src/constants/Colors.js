
/**
 * This constant class holds the color codes used in entire application.
 * This class is a single point to access color code through out the application.
 */
export default {
  color0: '#FD8D39',
  color1: '#00B1DA',
  bottonColorReservation:"#DB935F",
  color2: '#3A3D60',
  submitUnderlay: '#FD8D39',
  submit: '#FD8D39',
  submitText: '#FFF',
  cancelUnderlay: '#FFF',
  cancel: '#6c757d',
  cancelText: '#FD8D39',
  transparent: 'transparent',
  header: '#FFFFFF',
  textcolor: '#333',
  loginGradientStart: '#13505b',
  loginGradientEnd: '#0c7489',
  tintColor: '#2F95DC',
  IconDefault: '#000000',
  tabIconDefault: '#FD8D39',
  tabIconSelected: '#FD8D39',
  tabBar: '#FEFEFE',
  errorBackground: 'red',
  errorText: '#FFFFFF',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: '#2F95DC',
  noticeText: '#FFFFFF',
  status_bar_color: '#f9f9f4',
  account_status_bar_color: '#FFF',
};
