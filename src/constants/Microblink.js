import { Platform } from 'react-native';

// Microblink library license keys to scan DMV license

// const licenseKey = Platform.select({
//       ios: "sRwAAAEXY29tLmpvaW5idWdneS5kcml2ZXJhcHBiGE6pbwi3BhUjATh7SIjWNABvA1wgUd4BS7DrHk6GM+pARYqyoN3Tz4be/km2Iffwe2tQpYTiiVratJ9PJ84S24UX27uafOLQeR+FoyLO8zYKitJt3+IIpYvh3EoN/DJiIWuEmIwS+b0ePg2UFipv2Lomo3lS8vt9w/3FQ9rgK5Gw4b5ABWKiB5B8IIBNE+2ZCseN14G9RBOOB/+tBiccz9Ws5Hgsjtpeu/fyvyCigO+xFX0ktuf2+MMinD8vunbNaSLxFCZP" ,
//       android: "sRwAAAAXY29tLmpvaW5idWdneS5kcml2ZXJhcHBcCqm6yfRrxAM6FlmtW4dCTvRbWMMPN84olsqJZfOGtlJtV8cxnvYfFJ4XpBcPznnieq21QzutmqTsewWiUrIWJbo8dR5Lw8U8p61Os9wpDFsWfLKx2O4hLCJVFft3nyj4DcFwOE1dLQdzmZChTxvIiKkVaKx+EBjIh3Wa7CIgS1DEApGIcdbD1KqTxuJvdRcKd9KeF0E7MDXNJyFjNVyDxtQiS5CF/V7hYEnGPM7cRAbEU3nmce/wcaLQloKJWhnfClPuSz4R" 
//   })

const licenseKey = Platform.select({
      ios: "sRwAAAEXY29tLmpvaW5idWdneS5kcml2ZXJhcHBiGE6pbwi3BhUjATh9UIzWbkBTbQlpbMc72LnTIFog6+E+x4LrB2/ta+/0bYyWoO7+ELvQCQIlQJ/s0tJ5CFJTu9H1r0WBTUPkoq+RTA1VHEYap5QqaqawE6LOmO6yaOWsFymfuvfa4DE8jVgygiCJvT4m0Q0AfssitA30MQ1gu0AWbw5pmEKELw7GThh4ZOX30WOX9AP83eMGkjvFETXq6VHEo47MEjafbNzAX1qJEbAjT8IT8l8BsvjjhP//O2Z9+FYxRQ==" ,
      android: "sRwAAAAXY29tLmpvaW5idWdneS5kcml2ZXJhcHBcCqm6yfRrxAM6FlmrQ4NCOIVbMumNpBo6h1vFrm3Zk0J2jSnZibpNBxG/H03TJG0LL+uMFFgL53Sw5PIbNKbGMiZPnYtANPLDfJo24SfU6GZummthztOcQpbuqnufzo6QZNpb1oSsSzH1mDstujT6eUIVzL998xDzOjpXm2EG0B590bsPvSb4Qhav4VBy2DxnjlzQnsHA41gXQF66ylRl2AgLgp/EsCIbE+jLEiqQZMm5O7KDJVR6OWJmmTAFHYP/Oh8r/w==" 
  })

export default licenseKey;