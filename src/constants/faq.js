
/**
 * This class holds the static FAQ details.
 */

export const faq_data = [{
  
  // Application related FAQs
  name: "Application",
  // Application question and answers
  questions: [
    {
      "question": "What is Buggy?",
      "answer": "Buggy is a TLC Car Rental Company. If you have a TLC License and you are looking to driver for Uber, Lyft, Via, or other ride sharding services and don't have a car? Then Buggy is here for you!"
    },
    {
      "question": "How do I sign up to begin renting a vehicle?",
      "answer": "To begin your application, Apply to Rent a Buggy Vehicle"
    },
    {
      "question": "How do I make an appointment to pick up the vehicle?",
      "answer": "After you apply, we will contact you to set up an appointment to pick up the vehicle."
    },
    {
      "question": "What is the location to pick up the vehicle?",
      "answer": "Our Buggy office and pick up spot is located at 445 Empire Blvd Brooklyn, NY 11225."
    },
    {
      "question": "What is the minimum age requirement to rent from Buggy?",
      "answer": "The minimum age requirement is 21 years old."
    },
    {
      "question": "What kind of license do I need?",
      "answer": "You are required to have a TLC License, and valid DMV license."
    },
    {
      "question": "Which documents do I need to bring with me to the pickup location to get into a vehicle?",
      "answer": "Make sure to bring your TLC and DMV Licences, your Credit or Debit Card, and a balance that can cover the Security Deposit."
    },
    {
      "question": "Can I work for more than one company while renting a vehicle from your company?",
      "answer": "You can work for more than one company. For example, you can work for both Uber and Lyft or any other car service company at the same time while using a Buggy vehicle."
    },
    {
      "question": "Can the car be used for personal use?",
      "answer": "Yes, as long as you are the only person driving the vehicle."
    },
    {
      "question": "Can I drive anywhere with the car?",
      "answer": "Yes, as long as you continue paying your rent on time and you are responsible for all charges you may incur while in the vehicle, ie; toll, tickets etc."
    },
    {
      "question": "Is there a limited amount of mileage?",
      "answer": "There is no mileage limit! Drive to your heart's content!  "
    },
    {
      "question": "Do I need to pay a Security Deposit?",
      "answer": "Yes. You are required to pay a  refundable deposit of $399, before picking up a vehicle. (Please note, deposit amount may be subject to change based on your insurance)."
    },
    {
      "question": "Why am i required to pay a deposit?",
      "answer": "We require a deposit as a security, just in case you damage the vehicle or incur any charges due to fees from tickets, tolls etc."
    },
    {
      "question": "When do I get my deposit back?",
      "answer": "You may receive your security deposit 30 days after returning the vehicle. We hold it for 30 days in case of any tickets or toll charges that may take about 30 or more days to process."
    },
    {
      "question": "Does Buggy charge for Towing?",
      "answer": "If your car has a mechanical problem and can't be driven to our Service Center, Buggy will cover the tow cost as long as you are within the five boroughs, (Brooklyn, Queens, Bronx, Manhattan, and Staten Island) or within a 50-mile radius of the Buggy Brooklyn Office. "
    }
  ]
},
{
  // Returns related FAQs
  name: "Returns",
  // Returns question and answers
  questions: [
    {
      "question": "What is the minimum amount of time I can rent a vehicle for?",
      "answer": "We have a one week minimum policy."
    },
    {
      "question": "What is the return policy?",
      "answer": "If you are looking to return the vehicle, you must schedule your return at least one full week, or more, in advance through text only. To schedule your return, text Buggy at 347-334-6315 and request a link to schedule your return. You will then be prompted to choose the exact date that you would like to return the car, and to let us know your reason for returning."
    },
    {
      "question": "What happens if I do not schedule my return through the link at least one full week in advance?",
      "answer": "Anyone who does not schedule their return at least one full week in advance, may be subject to a penalty fee amount equal to a full additional week of rent."
    },
    {
      "question": "What time of day do I need to return the car?",
      "answer": "All drivers with vehicles scheduled to return, must bring the car in at 8 AM - 9 AM on Monday-Friday, and on Sunday from 9 AM - 10 AM."
    },
    {
      "question": "What happens if I bring the car later than 10 AM?",
      "answer": "Anyone who brings the vehicle later than the appointed time, may be subject to a penalty late fee of $150 plus the rental fee up until, and including, that current day."
    },
    {
      "question": "Am I allowed to smoke in the rental vehicle?",
      "answer": "No. There is no smoking in the vehicle for the drivers or the passengers. There is a $250 Fine if the car comes back with a smoke smell."
    },
    {
      "question": "Am I responsible to clean the vehicle?",
      "answer": "Yes. All Buggy vehicles are cleaned before they are rented out. Please make sure to return the vehicle in the same condition received."
    },
    {
      "question": "Do I need to refuel the vehicle?",
      "answer": "Yes. Buggy sends out all their vehicles with a full tank of gas. All vehicles must be returned with a full tank of gas. If your gas tank was not at full when you received the vehicle, please contact Buggy support."
    }
  ]
},
{
  // Maintenance related FAQs
  name: "Maintenance",
  // Maintenance question and answers
  questions:[
    {
      "question": "Does Buggy have a mechanic shop?",
      "answer": "Yes, Buggy has 2 shops you can go to:\n\nBuggy Shop: \n\n391 Empire Blvd. \nBrooklyn NY, 11225 \nMonday - Thursday from 8 AM - 6 PM, \nFriday from 8 AM - 4 PM, \nSunday 9 AM - 5 PM.\n\nBuggy Mechanics: \n\n183 Empire Blvd. \nBrooklyn NY, 11225 \nMonday - Thursday from 8 AM - 10 PM, \nFriday from 8 AM - 4 PM,\nSunday 9 AM - 10 PM."
    },
    {
      "question": "Where can I go if I need any small maintenance done like oil change, light bulbs etc.?",
      "answer": "You can go to either Buggy Shop or Buggy Mechanics or please contact us at: {{phone_number}}"
    },
    {
      "question": "Am I allowed to get maintenance done at a Non-Buggy mechanic shop?",
      "answer": "No, however If you need maintenance done on Saturday, when Buggy shop is closed, you can go to the mechanic shop: \n\nVLC Auto Repair, \n547 Albany Ave, \nBrooklyn, NY 11203."
    },
    {
      "question": "Will Buggy reimburse me for fixing a mechanical issue with the vehicle?",
      "answer": "Buggy does not offer reimbursement for most mechanical issues however, some basic maintenance will be covered. To find out more please contact us: {{phone_number}}"
    },
    {
      "question": "What should I do if I get a flat tire?",
      "answer": "If you get a flat tire you can change it with the spare tire in the trunk of the car. If you cannot find a spare please contact us at: {{phone_number}}"
    },
    {
      "question": "What should I do if my car breaks down?",
      "answer": "Buggy offers 24/7 roadside assistance for all kinds of emergencies as mentioned. There, in any case of breakdown or if your car doesn't start please contact us at: {{phone_number}}"
    }
  ]
},
{
  // Billing related FAQs
  name: "Billing",
  // Billing question and answers
  questions:[
    {
      "question": "When is the rent due?",
      "answer": "Buggy’s weekly billing cycle is very simple to understand. Your first rental payment will be due exactly 1 week from the day you pick up the vehicle, and every 7 days after that. For example, if you picked up the vehicle on a Monday, your first rental payment will be due 7 days later on that Sunday, and every Sunday moving forward. If you picked the car up on a Wednesday, your rent will be due 7 days later on Tuesday, and every Tuesday moving forward, and so forth. You can always contact one of our Buggy representatives to confirm your rental due date if you are not sure."
    },
    {
      "question": "Which payment methods are available to pay for my rent?",
      "answer": "We have a few different methods of payment, we accept:\n\n- Zelle or Quick Pay from participating banks.\n\n- Remote Cash, please see our FAQ on Remote Cash.\n\n- Debit or Credit Card, with a 3% processing fee."
     // We have a few different methods of payment available. We accept direct payments through Zelle or Quick Pay from participating bank accounts, Remote Cash payments at any of our participating locations (Please see our FAQ regarding the details of Remote Cash), or of course you can pay with your debit or credit card. (Please note, additional fees may apply when paying with your credit or debit card)."
    },
    {
      "question": "What is Instant Pay?",
      "answer": "Instant Pay means your earning are available to cash out from your Uber app instantly, and there is no need to wait for a weekly payment. You can cash out with Instant Pay up to 5 times per day. This will be very helpful to you so you can make sure to pay your weekly rent on time. You must be sure to set up Instant Pay with Uber."
    }
  ]
},
{
  // Tickets & Tolls related FAQs
  name: "Tickets & Tolls",
  // Tickets & Tolls question and answers
  questions:[
    {
      "question": "How do I purchase an E-ZPass?",
      "answer": "You can purchase an E-ZPass at your local DMV, or at an E-ZPass Office located closest to you."
    },
    {
      "question": "How do I set up my E-ZPass?",
      "answer": "When you purchase an E-ZPass, all of the directions are written in the provided manual. However, you can always ask the representative at the purchased location to help set up your account. You can also come to the Buggy office location, and a representative will be happy to help you setup your account."
    },
    {
      "question": "How do I pay for my tolls if they did not read correctly while going through the toll booth and my E-ZPass was not charged?",
      "answer": "If your E-ZPass was not charged for any reason while going through the toll booth, Buggy will pay for those tolls on your behalf, and then then add those tolls to your Buggy account balance with an additional $5 fee for each toll."
    },
    {
      "question": "Why is my E-ZPass not reading correctly?",
      "answer": "There are a few reasons that your E-ZPass is not reading correctly. If you notice that your E-ZPass is not reading correctly, make sure to check the following: Make sure your transponder is located right behind the rearview mirror. Make sure you are not speeding through the toll lanes. Make sure that you always have enough money on your E-ZPass at all times. We highly recommend that you get Auto-Replenishment set up on your E-ZPass, and have it connected to a valid bank account. This way your E-ZPass will automatically charge your card to ensure that you always have enough money before your balance gets too low."
    },
    {
      "question": "What happens if I receive a traffic ticket (ie; camera tickets for parking, red light, speeding, bus lanes, etc)?",
      "answer": "For parking and camera violations, Buggy will attempt to transfer the ticket liability to the driver's name and charge the driver a $10 processing fee. Buggy will NOT be paying for the ticket, each driver is responsible to pay for each of their tickets. However, If we are unable to transfer liability to the driver's name, Buggy will give the driver 72 hours to take care of the ticket after which Buggy pay the ticket on your behalf and charge the driver the ticket cost and a $10 processing fee."
    },
    {
      "question": "How do I go about paying for a ticket?",
      "answer": "To pay for a ticket online, {{click here ticket}}. Pay Tickets to NYC Department of Finance, and type in your violation number. You can also go to your local DMV to pay for your ticket."
    },
    {
      "question": "How do I dispute a ticket if I was not at fault?",
      "answer": "You can go to you local DMV to dispute your ticket. You can also download the NYC Parking Pay or Dispute App, or you can download the WinIt App and follow the directions to dispute your ticket."
    }
  ]
},
{
  // Remote Cash related FAQs
  name: "Remote Cash",
  // Remote Cash question and answers
  questions:[
    {
      "question": "What is Remote Cash?",
      "answer": "Once requested we will send you a bar code which can be used in participating stores. There is a $2 payment processing fee and it is quick and easy. Please tell the cashier that you would like to pay a bill, show them the barcode and they will be able to scan it, charge you, and pay Buggy. \n\nParticipating stores:\nDollar General\nFamily Dollar\nSeven Eleven\nKum & Go\nSpeedway\nCVS Pharmacy"
      // You can now make Cash payments for your weekly rent remotely, at your local 7/11, Dollar General, CVS, Family Dollar, Speedway, and more! For only a $2 fee per transaction, you can save a lot of time and gas, without having to come all the way to the office!"
    },
    {
      "question": "How does Remote Cash work?",
      "answer": "We will be sending you a link with a bar code which you can use every time you make payment. You will be able to go to any participating stores closest to you with this bar code. Once at the store, tell the cashier that you would like to make a bill payment. You will have the Cashier scan the bar code on your phone, give the cashier your rental payment, plus a $2 fee, and they will give you a physical receipt. They will then be able to transfer the funds from their store, directly to our bank account, where it will then be applied to your balance at Buggy and your rent will be paid!"
    },
    {
      "question": "How do I get the Bar-Code link?",
      "answer": "Please send Buggy a text message requesting the link so that we can send it to you. You can use the same link every time!"
    },
    {
      "question": "The cashier did not understand what Remote Cash was. What should I do?",
      "answer": "This program is still very new, so although all of these stores should already be trained to understand how this program works, there is a possibility that you may come across a cashier who may not be very familiar with this. Try telling them you would like to make a bill payment with Cash, or you can mention that you are making a Vanilla Direct Pay bill payment. You can always ask for a different cashier to help. Otherwise, try to walk the cashier through it. Tell them that by scanning the bar-code on your phone, you can give them a cash payment to send to your rental company."
    },
    {
      "question": "I tried to troubleshoot and walk the cashier through the process but he still does not understand what I am talking about. What should I do now?",
      "answer": "The first thing you can do is try going to a different location. There are many store locations available, with cashiers that will be more trained in this program. Once you find a location that works, you can continue to go back on a weekly basis to pay your rent. How convenient!"
    },
    {
      "question": "I tried everything, and it still doesn’t seem to work for me. Is there anything else I can do?",
      "answer": "Yes! Call us: {{phone_number}}"
      //Yes! You can always contact our Customer Support Team at any time, by calling in or texting us, and one of our trained representatives will be sure to help you out."
    }
  ]
},{
  name: "Report an Accident",
}

];
 