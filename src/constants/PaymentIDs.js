export const getPaymentIds = (props,type) => { 

  
    if(props.screenProps.build_type!='live') {
      if( type=="zelle" ){
        return 2
      }
      if( type=="remotecash" ){
        return 5
      }
      if( type=="card" ){
        return 3
      }
    } 

    if(props.screenProps.build_type=='live') {
      if( type=="zelle" ){
        return 3
      }
      if( type=="remotecash" ){
        return 4
      }
      if( type=="card" ){
        return 1
      }
    }

}

export const getPaymentMethodNames = (props,pid) => { 

  
  if(props.screenProps.build_type!='live') {
    if( pid==2  ){
      return "zelle";
    }
    if( pid==5  ){
      return "remotecash";
    }
    if( pid==3  ){
      return "card";
    } 
  } 

  if(props.screenProps.build_type=='live') {

    if( pid==3  ){
      return "zelle";
    }
    if( pid==4  ){
      return "remotecash";
    }
    if( pid==1  ){
      return "card";
    }  
    
  }

}

