import React, { useState } from 'react';
import { StatusBar, Platform, AppRegistry } from 'react-native';
import AppNavigator from './navigation/AppNavigator';
import { ApolloProvider } from '@apollo/react-hooks';
import NetChecker from './screens/NetInfo';
import { langContext } from './screens/langContext';
import { setI18nConfig } from './components/Language';
import Colors from './constants/Colors';
import client from './graphql';
console.disableYellowBox = true;

function App(props) {
  const [state, setState] = useState({
    isModal: false,
    lang: "en",
    car_id: 3,
    notification_count: 90,
    status_color: '#DB9630'
  });
  const { isModal, lang, car_id, status_color, notification_count } = state;
  const setNetModal = (flag) => {
    setState({ ...state, isModal: flag })
  }
  const setLanguage = (language) => {
    setI18nConfig(language);
    setState({ ...state, lang: language })
  }
  const setCarID = (id) => {
    setState({ ...state, car_id: id })
  }
  const setStatusColor = (color) => {
    setState({ ...state, status_color: color })
  }
  const setNotificationCount = (ncount) => {
    setState({ ...state, notification_count: ncount })
  }
  return (<>
    <style type="text/css">{`
        @font-face {
          font-family: 'AntDesign';
          src: url(${require('react-native-vector-icons/Fonts/AntDesign.ttf')}) format('truetype');
        }

        @font-face {
          font-family: 'EvilIcons';
          src: url(${require('react-native-vector-icons/Fonts/EvilIcons.ttf')}) format('truetype');
        }

        @font-face {
          font-family: 'Ionicons';
          src: url(${require('react-native-vector-icons/Fonts/Ionicons.ttf')}) format('truetype');
        }

        @font-face {
          font-family: 'MaterialCommunityIcons';
          src: url(${require('react-native-vector-icons/Fonts/MaterialCommunityIcons.ttf')}) format('truetype');
        }

        @font-face {
          font-family: 'MaterialIcons';
          src: url(${require('react-native-vector-icons/Fonts/MaterialIcons.ttf')}) format('truetype');
        }

        @font-face {
          font-family: 'Feather';
          src: url(${require('react-native-vector-icons/Fonts/Feather.ttf')}) format('truetype');
        }

        @font-face {
          font-family: 'Fontisto';
          src: url(${require('react-native-vector-icons/Fonts/Fontisto.ttf')}) format('truetype');
        }

        @font-face {
          font-family: 'FontAwesome';
          src: url(${require('react-native-vector-icons/Fonts/FontAwesome.ttf')}) format('truetype');
        }
        
        @font-face {
          font-family: 'FontAwesome5_Regular';
          src: url(${require('react-native-vector-icons/Fonts/FontAwesome5_Regular.ttf')}) format('truetype');
        }
      `}</style>
    <ApolloProvider client={client}>
      <langContext.Provider value={{ lang, setLanguage, notification_count, setNotificationCount, status_color, car_id, setCarID, setStatusColor }}>
        <AppNavigator lang={lang} screenProps={{ setNetModal, setStatusColor }} />
      </langContext.Provider>
    </ApolloProvider>
    </>
  );
};

AppRegistry.registerComponent('App', () => App);

export default App;