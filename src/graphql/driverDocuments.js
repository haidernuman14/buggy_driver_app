import { gql } from 'apollo-boost';

export const driverDocument = gql`
query Driver($id: ID) {
  driver(id: $id) {
    id
    dmvLicense
    tlcLicense
    driverdocumentSet {
      edges {
        node {
          name
          documentUrl
        }
      }
    }
  }
}
`;  