import { gql } from 'apollo-boost';
/**This class is used to fetch the data of Credit card details of user using GraphQL query. */
export const GetStripeCards = gql`
query GetStripeCards($driverId: ID!) {
  getStripeCards(id: $driverId) {
    id
    cards {
      last4
      fingerprint
      brand
      id
      default
    }
  }
}`;