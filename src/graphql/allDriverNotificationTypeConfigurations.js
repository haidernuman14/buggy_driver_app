import { gql } from 'apollo-boost';

export const allDriverNotificationTypeConfigurations = gql`
query allDriverNotificationTypeConfigurations($driverId: ID) {
  allDriverNotificationTypeConfigurations(driverId: $driverId) {
    edges {
      node {
        id 
        notificationType {
          id
          name
        }
        deliveryMethods
      }
    }
  }
}`;