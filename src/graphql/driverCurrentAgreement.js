import { gql } from 'apollo-boost';

export const driverDetail = gql`
query Driver($id:ID) {
    driver(id:$id){
      id        
      currentAgreement {
        id 
        pk
        stage
        startDate
        endDate
        pickupType
        car {
          id
          model
          year
          color
        }
      }
   }
}
`;