import { gql } from 'apollo-boost';
/**This class  is used to fetch the driver details, driver balance, current agreement that shows the driver current rental cars, transactionSet */
export const Driver = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
      firstName
      lastName
      email
      balance
      currentCharge
         reservationDriver(status:"Open"){
         edges{
          node{
            id
            pickupDate
            priceRange
            pickUpLocation
            agreementType{
              name
            }
            car{
          year
          color
          model
          genericImageLink
          pk
            }
          }
        }
    }
      currentAgreement {
        id
        car {
          year
          color
          model
          genericImageLink
          pk
        }
        startDate
        endDate
        weeklyCharge
        transactionSet {
          edges {
            node {
              amount
              id
              startDate
              endDate
              chargeType
              chargeSuccessful
              dueDate
              dateAdded
            }
          }
        }
      }
   }
}`;  