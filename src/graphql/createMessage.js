import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to send message...///
 */
export const createDriverMessage = gql`
mutation createDriverMessage($driverId: ID!, $body: String!, $isInbound: Boolean!, $subject:String),{
  createDriverMessage(input:{
    driverId:$driverId,
    body:$body,
    isInbound:$isInbound
    subject:$subject
  }) 
  {
    ok
    errors{
      messages
    }
    clientMutationId
  }
}  `;