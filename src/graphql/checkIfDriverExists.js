import { gql } from 'apollo-boost';

export const checkIfDriverExists = gql`
query checkIfDriverExists($phone: String) {
  checkIfDriverExists(phone: $phone) {
  
    driverExists
    driverId
    passwordExists
  }  
}`;