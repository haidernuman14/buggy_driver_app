import { gql } from 'apollo-boost';

export const driverCompleteRegister = gql`
mutation CompleteDriverProfile($firstName: String!, $lastName: String!, $dmvLicense: String!, $tlcLicense: String!, $email: String, $phone: String,$dmvLicensePic: String,$tlcLicensePic: String) {
  completeDriverProfile(input:{
    firstName: $firstName,
    lastName: $lastName,
    tlcLicense: $tlcLicense,
    dmvLicense: $dmvLicense,
    email:$email,
    phone:$phone,
    dmvLicensePic:$dmvLicensePic,
    tlcLicensePic:$tlcLicensePic
  }) {
    ok
    errors {
      field
      messages
    }
  }
}`;