import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API for driver login
 */
export const driverLogin = gql`
  mutation DriverLogin($username: String!, $password: String!) {
    driverLogin(username: $username, password: $password) {
      token
      user {
        id        
        firstName
        lastName
        nickName        
        email
        tlcLicense
        isTlc
        tlcLicenseExpirationDate
        phone
        address
        addressLine2
        streetAddress
        city
        pk
        referralCode
        state
        zipCode
        dmvLicense
        dmvLicenseExpirationDate
        balance
        paymentMethod  
        dateAdded
        dateModified
        currentAgreement {
          id
          pk
          car{
            pk
            color
            model
            year
          }
          startDate
          endDate
          pickupType 
          stage
          carreturn {
            id
            returnReason
            scheduledDate
            returnType
          }
        }
        driverPtr {
          id 
          subscriptionSet{
            edges{
              node{
                id
                paymentMethod 
                dateAdded
                subscriptionType{
                  name 
                  amount 
                } 
                statusDisplay
              }
            }
          }
          currentCharge
          unreadNotificationsCount
          reservationDriver(status: "Open") {
            edges {
              node {
                id
                status
              }
            }
          }
          activeInsurance {
            id
          }
        }
      }
    }
  }`;
