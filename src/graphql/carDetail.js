import { gql } from 'apollo-boost';

export const carDetail = gql`
  query Car($id:Int,$agreementType_Name_Iexact: String) {
    car(id:$id){
      id   
      genericDescription
      color
      model
      weeklyCharge
      year
      location
      pk
      carpicturesSet{
        edges{
          node{
            pictureUrl
          }
        }
      }
      genericImageLink
      mileageSinceLastOilChange,
       prices(visibleToCustomers:true,agreementType_Name_Iexact: $agreementType_Name_Iexact){
        edges{
          node{
             price
             interval
             intervalUnit
              agreementType{
                name
                dateAdded
                requiredDeposit
              }
          }
        }
      }
  }
}`;