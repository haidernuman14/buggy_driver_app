import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to update driver's profile data
 */
export const UpdateDriverProfile = gql`
mutation updateDriverMutation($input: UpdateDriverMutationInput!) {
  updateDriver(input:$input) {
    id
    firstName
    lastName
    nickName    
    email
    tlcLicense
    phone
    addressLine2
    streetAddress
    city
    state
    zipCode
    dmvLicense
    dateAdded
    driver {
      balance
    }
    success:ok
    ok
    errors {
      field
      messages
    }
  }
}`;

/*
export const UpdateDriverProfile = gql`
mutation UpdateDriver($id: ID!, $firstName: String, $lastName: String, $dmvLicense: String,  $email: String) {
  updateDriver(input:{
    id:$id,
    firstName: $firstName,
    lastName: $lastName,
    dmvLicense: $dmvLicense, 
    email:$email,
  }) {
    success:ok 
    errors {
      field
      messages
    }
  }
}`; 
 */