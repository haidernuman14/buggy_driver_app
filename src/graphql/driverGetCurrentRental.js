import { gql } from 'apollo-boost';

export const driverGetCurrentRental = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
      currentAgreement { 
        id 
        car {
          year
          color
          model
          genericImageLink
          pk
        }
      }
   }
}`;  