import { gql } from 'apollo-boost';
export const availableCars = gql`
query availableCars($driverId: ID, $year:Int, $carModel:Int ,$first: Int, $after: String,$agreementType_Name_Iexact: String) {
  availableCars(driverId: $driverId, year:$year, carModel:$carModel, first: $first, after: $after ) {
    pageInfo{
      endCursor
      hasNextPage
      startCursor
    }
    edges{
      node{
        id
        carpicturesSet{
          edges{
            node{
              pictureUrl
            }
          }
        }
        pk
        model
        isReady
        year
        group
        color
        location
        genericDescription
        genericImageLink
        weeklyCharge
        prices(visibleToCustomers:true, agreementType_Name_Iexact: $agreementType_Name_Iexact){
          edges{
            node{
              price
              interval
              intervalUnit
              agreementType{
                name
                dateAdded
                requiredDeposit
              }
            }
          }
        }
      }
    }
  }
}
`;
