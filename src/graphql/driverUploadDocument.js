import { gql } from 'apollo-boost';

export const driverUploadDocument = gql`
  mutation uploadDocument($input: UploadDocumentMutationInput!) {
    uploadDocument(input:$input) {
      ok
      errors {
        field
        messages
      }
    }
}`;