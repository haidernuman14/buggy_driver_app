import { gql } from 'apollo-boost';

export const allSubscriptions = gql`
 query allSubscriptions($driverId: ID) {
   allSubscriptions(driverId:$driverId){
      edges{
        node{
          id
          paymentMethod 
          dateAdded
          dateModified
          subscriptionType{
            name 
            amount 
          } 
          statusDisplay
        }
      }
    }
}`;  