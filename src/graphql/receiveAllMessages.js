import { gql } from 'apollo-boost';
/**This class is used to fetch the Driver details using query from GraphQL */
export const getAllDriverMessages = gql`
  query allDriverMessages($driverId:ID) {
    allDriverMessages(driverId:$driverId){  
        edges {
            node {
              id
              dateAdded
              body
              isInbound
            }
          }
        }
      }`;

