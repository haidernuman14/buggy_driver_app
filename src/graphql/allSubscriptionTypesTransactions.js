import { gql } from 'apollo-boost';

export const allSubscriptions = gql`
query allSubscriptions ($driverId: ID, $first: Int, $after: String) {
  allSubscriptions ( driverId: $driverId, first:$first, orderBy: ["date_added"], after: $after) {
    pageInfo{
      hasNextPage
      startCursor
      endCursor
    }
    edges {
      node {
        id
        statusDisplay
        status
        dateAdded
        subscriptionPayments {
          edges {
            node {
              id
              sub{
                id
                dateAdded
              } 
              dateAdded
              transaction {
                id  
                amount
                chargeType
                chargeTypeDisplay
                chargeSuccessful
                dueDate
                dateAdded
              }
            }
          }
        }
      }
    }
  }
}
`;