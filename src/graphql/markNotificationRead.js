import { gql } from 'apollo-boost';

export const markNotificationRead = gql`
mutation MarkNotificationRead($input: MarkNotificationReadMutationInput!) {
  markNotificationRead(input: $input) {
    ok
    errors {
      field
      messages
    }
  }
}`;