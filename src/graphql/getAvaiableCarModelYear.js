import { gql } from 'apollo-boost';
export const getAvaiableCarModelYear = gql`
query availableCars($driverId: ID) {
  availableCars(driverId: $driverId) {
    edges{
      node{
        year
      }
    }
  }
}`;