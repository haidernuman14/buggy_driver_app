import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to delete driver's stored debit/credit cards
 */
export const createDriverNotificationTypeConfiguration = gql`
mutation createDriverNotificationTypeConfiguration($input: CreateDriverNotificationTypeConfigurationMutationInput!) {
  createDriverNotificationTypeConfiguration(input:$input) { 
    ok
    errors {
      field
      messages
    }
  }
}`;