import { gql } from 'apollo-boost';

export const driverUpdatePayment = gql`
mutation updateDriver($input: UpdateDriverMutationInput!) {
  updateDriver(input:$input) {
    id
    ok
    errors{
      messages
      field
    }
  }
}`;