import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to delete driver's stored debit/credit cards
 */
export const UpdateDriverNotificationTypeConfiguration = gql`
mutation updateDriverNotificationTypeConfiguration($input:UpdateDriverNotificationTypeConfigurationMutationInput!) {
  updateDriverNotificationTypeConfiguration(input:$input) { 
    ok
    errors {
      field
      messages
    }
  }
}`;