import { gql } from 'apollo-boost';

export const driverProfileImages = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id      
      driverdocumentSet {
        edges {
          node {            
            name
            documentUrl
            dateAdded
            dateModified
            documentType {
              id
              typeName
              documentCategory {
                name
              }
            }
          }
        }
      }      
   }
}`;  