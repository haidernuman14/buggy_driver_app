import { gql } from 'apollo-boost';

export const driverCompleteRegisterWeb = gql`
mutation CompleteDriverProfile($firstName: String!, $lastName: String!, $dmvLicense: String!, $tlcLicense: String!, $email: String, $phone: String,$dmvLicensePic: String,$tlcLicensePic: String,$referralCode: String,$tlcLicenseExpirationDate: Date, $dmvLicenseExpirationDate: Date) {
  completeDriverProfile(input:{
    firstName: $firstName,
    lastName: $lastName,
    tlcLicense: $tlcLicense,
    dmvLicense: $dmvLicense,
    referralCode: $referralCode,
    email:$email,
    phone:$phone,
    dmvLicensePic:$dmvLicensePic,
    tlcLicensePic:$tlcLicensePic,
    tlcLicenseExpirationDate:$tlcLicenseExpirationDate,
    dmvLicenseExpirationDate:$dmvLicenseExpirationDate
  }) {
    ok
    errors {
      field
      messages
    }
  }
}`;