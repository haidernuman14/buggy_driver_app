import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to reset Password
 */
export const appResetPassword = gql`
  mutation ResetPassword($input: ResetPasswordMutationInput!) {
    resetPassword(input:$input) {
      ok
      errors {
        field
        messages
      }
    }
}`;
