import { gql } from 'apollo-boost';
/**This class  is used to fetch the driver details, driver balance, current agreement that shows the driver current rental cars, transactionSet */
export const checkOpenReservation = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
    reservationDriver(status:"Open"){
      edges{
        node{
          id
          priceRange
           pickupDate
          pickUpLocation
          car{
            year
            model
            color
          }
          reservationpriceSet{
            edges{
              node{
                interval
                intervalUnit
                price
                maximumPrice
              
              }
            }
          }
          
        }
      }
    }
      }  
  }`;