import { gql } from 'apollo-boost';

export const setDriverPassword = gql`
  mutation setDriverPassword($driverId: ID!,$newPassword: String!,$confirmPassword: String!) {
    setDriverPassword(input: {
        driverId: $driverId,newPassword: $newPassword,confirmPassword: $confirmPassword
    }) {
      ok
      errors {
        field
        messages
      }
    }
  }  
`;