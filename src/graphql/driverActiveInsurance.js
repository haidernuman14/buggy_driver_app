import { gql } from 'apollo-boost';

export const driverActiveInsurance = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
      dmvLicense
      tlcLicense
      driverdocumentSet {
        edges {
          node {
            name
            documentType {
              id
              typeName
            }
          }
        }
      }
      reservationDriver(status: "Open") {
        edges {
          node {
            id
            car {
              id
              pk
              carModel {
                id
                name
              }
              year
              color
            }
            dateAdded
            pickupDate
            pickupType
            status
            agreementType {
              id
              paymentType
              name
            }
          }
        }
      }
      currentAgreement {
        id
      }
      activeInsurance {
        id
      } 
   }
}`;  