import { gql } from 'apollo-boost';

export const all_DriverNotifications = gql`
  query AllDriverNotifications($driverId:ID) {
    allDriverNotifications(driverId:$driverId){
      edges {
        node {
          id
          title
          message
          dateAdded
          isRead
          pk
        }
      }
    }
}`;