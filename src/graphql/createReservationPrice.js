import { gql } from 'apollo-boost';

export const createReservationPrice = gql`
mutation createReservationPrice($input: CreateReservationPriceMutationInput!) {
    createReservationPrice(input:$input){
        ok
        errors{
            field
            messages
        }
    }  
}`;