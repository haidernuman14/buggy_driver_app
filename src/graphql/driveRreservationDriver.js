import { gql } from 'apollo-boost';

export const reservationDriver = gql`
  query Driver($id:ID!) {
    driver(id:$id){
      reservationDriver(status: "Open") {
        edges {
          node {
            id
            car {
              id
            }
            status
          }
        }
      }
   }
}`;