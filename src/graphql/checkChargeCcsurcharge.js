import { gql } from 'apollo-boost';
/**This class is used to fetch the Driver details using query from GraphQL */
export const checkChargeCcsurcharge = gql`
query Driver($id:ID) {
    driver(id:$id){
        id
        chargeCcsurcharge
    }
}`;
