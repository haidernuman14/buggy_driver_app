import { gql } from 'apollo-boost';
/**This class is used to manage the GraphQL query that allows to maintain the registration process step 2,
 *  Driver details are stors using this query */
export const driverCompleteRegister = gql`
mutation CompleteDriverProfile($firstName: String!, $lastName: String!, $dmvLicense: String!, $tlcLicense: String!, $email: String, $phone: String,$dmvLicensePic: String,$tlcLicensePic: String, $tlcLicenseExpirationDate: Date, $dmvLicenseExpirationDate: Date) {
  completeDriverProfile(input:{
    firstName: $firstName,
    lastName: $lastName,
    tlcLicense: $tlcLicense,
    dmvLicense: $dmvLicense,
    email:$email,
    phone:$phone,
    dmvLicensePic:$dmvLicensePic,
    tlcLicensePic:$tlcLicensePic,
    tlcLicenseExpirationDate:$tlcLicenseExpirationDate,
    dmvLicenseExpirationDate:$dmvLicenseExpirationDate
  }) {
    ok
    errors {
      field
      messages
    }
  }
}`;