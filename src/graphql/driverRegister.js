import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API for Driver Registration
 */
export const driverRegister = gql`
mutation RegisterDriver($tlcLicense: String, $dmvLicense: String, $emailOrPhone: String!, $password: String!, $activeDeviceId: String, $activeDeviceModel: String, $activeDeviceOs: String) {
 registerDriver(input:{tlcLicense: $tlcLicense, dmvLicense: $dmvLicense, emailOrPhone: $emailOrPhone, password: $password, activeDeviceId: $activeDeviceId, activeDeviceModel: $activeDeviceModel, activeDeviceOs: $activeDeviceOs}) {
  ok
  type
  errors {
    field
    messages
  }
}
}`;
