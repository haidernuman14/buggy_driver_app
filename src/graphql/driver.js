import { gql } from 'apollo-boost';
/**This class is used to fetch the Driver details using query from GraphQL */
export const driverDetail = gql`
  query Driver($id:ID!) {
    driver(id:$id){
      id
      isTlc
      firstName
      lastName
      email
      tlcLicense
      phone
      streetAddress
      city
      state
      zipCode
      dmvLicense
      balance
      dateAdded
      dateModified
      currentCharge
      activeCards {
        id
        last4 
      }
      reservationDriver(status: "Open") {
        edges {
          node {
            id
            status 
          }
        }
      }
      activeInsurance {
        id
      } 
   }
}`;