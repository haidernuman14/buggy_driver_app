import { gql } from 'apollo-boost';

export const changeReturn = gql`
mutation changeReturn($input: ChangeReturnInput!) {
  changeReturn(input: $input) {
    ok
    errors {
      messages
    }
  }
}`;
