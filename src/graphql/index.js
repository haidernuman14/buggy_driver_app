import { production } from '../app.json';
import ApolloClient from 'apollo-boost';
import AsyncStorage from '@react-native-community/async-storage';
// import perf from '@react-native-firebase/perf';
const client = new ApolloClient({
  uri: production == 'live' ? 'https://buggy-174216.appspot.com/mobileql' : 'https://buggy-staging-env.appspot.com/mobileql',
  request: async (operation) => {
    
    let token = await AsyncStorage.getItem('token');
    operation.setContext({
      headers: {
        authorization: token != "null" ? `Bearer ${token}` : ''
      }
    })
  },
})

export default client;