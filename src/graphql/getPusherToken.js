import { gql } from 'apollo-boost';

export const getPusherToken = gql`
mutation getPusherToken($input: GetPusherTokenMutationInput!) {
  getPusherToken(input: $input) {
    ok
    errors {
      field
      messages
    }
    token
  }
}
`;