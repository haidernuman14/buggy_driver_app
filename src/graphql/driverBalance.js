import { gql } from 'apollo-boost';

export const driverBalance = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
      firstName
      lastName
      email
      balance
      currentCharge
      dmvLicense
      tlcLicense
      referralCount
      docsMissingApplication {
        id
        typeName
      }
      driverdocumentSet {
        edges {
          node {
            name
            documentType {
              id
              typeName
            }
          }
        }
      }
      reservationDriver(status: "Open") {
        edges {
          node {
            id
            car {
              id
              pk
              carModel {
                id
                name
              }
              year
              color
            } 
            dateAdded
            paymentMethod
            pickupDate
            pickupType
            status
            agreementType {
              id
              paymentType
              name
            }
          }
        }
      }
      activeInsurance {
        id
      }
      currentAgreement {
        id
        car {
          year
          color
          model
          genericImageLink
          pk
          cardocumentSet{
            edges{
              node{
                id
                name
                documentUrl
              }
            }
          }
        } 
        reservation{
          id
          pickupDate 
          paymentMethod
        }
        stage
        startDate
        endDate
        weeklyCharge 
      }
   }
}`;  

/* 
  reservationDriver(status: "Open") {
        edges {
          node {
            id
            car {
              id
              pk
              carModel {
                id
                name
              }
              year
              color
            }
            paymentMethod {
              name
              id
            }
            dateAdded
            pickupDate
            pickupType
            status
            agreementType {
              id
              paymentType
              name
            }
          }
        }
      }
*/