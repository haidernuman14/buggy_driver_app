import { gql } from 'apollo-boost';

export const driverReturnCarDate = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
      currentAgreement {
        id
        carreturn {
          id
          scheduledDate
          returnReason
          returnType
        } 
        car {
          year
          color
          model
          weeklyCharge
          genericImageLink
          pk
          cardocumentSet{
            edges{
              node{
                id
                name
                documentUrl
              }
            }
          }
        } 
        reservation{
          id
          pickupDate 
          paymentMethod
        }
        stage
        startDate
        endDate
        weeklyCharge 
      }
   }
}`;  