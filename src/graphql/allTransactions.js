import { gql } from 'apollo-boost';

export const allTransactions = gql`
query allTransactions($driverId: ID, $chargeTypeIntIn:[Int], $dueDate_Lte:Date, $dueDate_Gte:Date, $amount_Lte:Float, $amount_Gte:Float, $first: Int, $before: String, $after: String,$last:Int,$amount_Lt:Float,$orderBy:[String]) {
  allTransactions(driverId: $driverId, chargeTypeIntIn:$chargeTypeIntIn, dueDate_Lte:$dueDate_Lte, dueDate_Gte:$dueDate_Gte, amount_Lte:$amount_Lte, amount_Gte:$amount_Gte, first: $first, before: $before, after: $after,last:$last,amount_Lt:$amount_Lt,orderBy:$orderBy) {
    pageInfo{
      hasNextPage
      startCursor
      endCursor
    }
    edges {
      cursor
      node {
        id
        amount
        chargeType
        chargeTypeDisplay
        chargeSuccessful
        dueDate
        dateAdded
      }
    }
  }
}
`;