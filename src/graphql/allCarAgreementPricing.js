import { gql } from 'apollo-boost';

export const allCarAgreementPricing = gql`
query AllCarAgreementPricing($carPk:Int!){
  allCarAgreementPricing(carPk:$carPk){
    edges{
      node{
        id
        carModelGroupType{
          name
          description
        }
        agreementType{
          name
          requiredDeposit
        }
        isActive
        dailyPrice
        agreementTypeName
        pk
        weeklyPrice
      }
    }
  }
} `;