import { gql } from 'apollo-boost';

export const updateReservation = gql`
mutation updateReservation($input:  UpdateReservationMutationInput!) {
    updateReservation(input:$input){
        ok
        errors{
            field
            messages
        }
        reservation{
            id
            dateAdded
            dateModified
            pickupDate
            pickUpLocation
            pickupType
            notes
            preferredCar
            agreementType{
                name
            }
            priceRange
            discount
            surcharge
            deposit
            status
            sendMessage
            cancelReason
            isSplit
            returnDate
        }
    }  
}
`;