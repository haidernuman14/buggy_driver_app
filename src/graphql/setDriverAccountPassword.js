import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to send Forgot password request
 */
export const SetDriverPassword = gql`
  mutation setDriverPassword($input: SetDriverPasswordMutationInput!) {
    setDriverPassword(input:$input) {
      ok
      errors {
        field
        messages
      }
    }
}`;