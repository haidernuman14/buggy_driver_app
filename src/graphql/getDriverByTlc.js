import { gql } from 'apollo-boost';

export const getDriverByTlc = gql`
query Driver($tlcLicense:String,$email:String,$phone:String) {
  driver(tlcLicense:$tlcLicense,email:$email,phone:$phone){
    id 
 }
}`;