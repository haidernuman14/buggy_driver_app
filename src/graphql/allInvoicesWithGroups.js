import { gql } from 'apollo-boost';

export const allInvoicesWithGroups = gql`
query allInvoices($driverId:ID){
    allInvoices(driverId: $driverId) {
      pageInfo{
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        cursor
        node {
          id
          totalBalance
          invoiceItemsTotal
          startDate
          endDate
          previousBalance
          invoiceItemsGroup {
            balance
            chargeType
            chargeTypeDisplay
            totalCharge
            totalPaid
            totalCount
          }
        }
      }
    }
  }
`;
