import { gql } from 'apollo-boost';
export const allBusinessHours = gql`
query allBusinessHours{
  allBusinessHours{
    edges{
        node{
          id
          openTime
          closeTime
          dayOfWeek
          dayOfWeekDisplay
          pk
        }
      }
    }
}
`;