import { gql } from 'apollo-boost';

export const driverActiveInsurance = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
      dmvLicense
      tlcLicense
      driverdocumentSet {
        edges {
          node {
            name
            documentType {
              id
              typeName
            }
          }
        }
      }
      reservationDriver(status: "Open") {
        edges {
          node {
            id
            car {
              id
              pk
            }
            pickupDate
            pickupType
            status
          }
        }
      }
      currentAgreement {
        id
      }
      activeInsurance {
        id
      } 
   }
}`;  