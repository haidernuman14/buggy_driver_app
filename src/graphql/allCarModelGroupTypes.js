import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to get all car models with group types 
 */
export const allCarModelGroupTypes = gql`
query allCarModelGroupTypes($before: String, $after: String, $first: Int, $last: Int, $id: ID, $name_Icontains: String){
  allCarModelGroupTypes(before:$before, after:$after, first:$first, last:$last, id:$id, name_Icontains:$name_Icontains)
  {
    pageInfo{
      hasNextPage
      hasPreviousPage
    }
    edges
    {
      node
      {
        id
        pk
        name
        dateAdded
        dateModified
      }
    }
  }
}
`;