import { gql } from 'apollo-boost';

export const cancelReturn = gql`
  mutation cancelReturn($input: CancelReturnInput!) {
    cancelReturn(input:$input) {
      ok
      errors {
        field
        messages
      }
    }
}`;