import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to delete driver's stored debit/credit cards
 */
export const driverDeleteCard = gql`
mutation RemoveCard($input: RemoveCardInput!) {
  removeCard(input: $input) {
    ok
    errors {
      messages
    }
  }
}`;