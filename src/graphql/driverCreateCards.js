import { gql } from 'apollo-boost';
/**This DriverCreateCard class is used to store the newly added cards details in GraphQL database. */
export const CreateCard = gql`
mutation createCard($input: CreateCardInput!) {
  createCard(input: $input) {
    ok
    errors {
      field
      messages
    }
  }
}`;