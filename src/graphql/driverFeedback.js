import { gql } from 'apollo-boost';

export const createDriverFeedback = gql`
mutation createDriverFeedback($input: CreateDriverFeedbackMutationInput!) {
  createDriverFeedback(input: $input) {
    ok 
    errors {
      field
      messages
    }
  }
}`;
