import { gql } from 'apollo-boost';

export const allCarModels = gql`
query allCarModels($isAvailable:Boolean){
  allCarModels(isAvailable:$isAvailable){
    edges{
      node{
        id
        name
        pk
     
      }
    }
}
}
`;