import { gql } from 'apollo-boost';

export const changeDefaultCard = gql`
mutation changeDefaultCard($input: ChangeDefaultCardInput!) {
  changeDefaultCard(input: $input) {
    ok 
    errors {
      field
      messages
    }
  }
}`;