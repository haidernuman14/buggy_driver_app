import { gql } from 'apollo-boost';

export const allTransactionsTicketsAndTolls = gql`
query allTransactions($driverId: ID, $chargeTypeIntIn:[Int], $dueDate_Lte:Date, $dueDate_Gte:Date) {
  allTransactions(driverId: $driverId, chargeTypeIntIn:$chargeTypeIntIn, dueDate_Lte:$dueDate_Lte, dueDate_Gte:$dueDate_Gte) {    
    edges {
      cursor
      node {
        chargeType
        chargeTypeDisplay
        dueDate
        notes
        checkNo
        amount
        confirmationId
      }
    }
  }
}
`;