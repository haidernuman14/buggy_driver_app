import { gql } from 'apollo-boost';

export const driverLicenseExpirationDateUpdate = gql`
mutation UpdateDriver($input: UpdateDriverMutationInput!) {
  updateDriver(input:$input) {
    ok
    driver{
        tlcLicenseExpirationDate
        dmvLicenseExpirationDate
    }
    errors {
      field
      messages
    }
  }
}`;