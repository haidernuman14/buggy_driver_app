import { gql } from 'apollo-boost';

export const updateSubscription = gql`
  mutation updateSubscription($input: UpdateSubscriptionMutationInput!) {
    updateSubscription(input: $input) {
      ok
      errors {
        field
        messages
      }
    }
  }  
`;