import { gql } from 'apollo-boost';

export const driverActiveInsuranceCarList = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
      dmvLicense
      tlcLicense
      activeInsurance {
        id
      } 
   }
}`;  