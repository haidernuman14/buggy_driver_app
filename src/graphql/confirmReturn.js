import { gql } from 'apollo-boost';

export const confirmReturn = gql`
mutation confirmReturn($input: ConfirmReturnMutationInput!) {
  confirmReturn(input: $input) {
    ok
    errors {
      messages
    }
  }
}`;
