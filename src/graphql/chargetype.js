import { gql } from 'apollo-boost';
/**This class is used to get the charge type like (Buggy Ticket Charge, Car Wash, Cash, Check Refund #2276)
 *  status using GraphQL query*/
export const Chargetype = gql`
query EnumQuery {
  chargeTypes: __type(name: "TransactionChargeType") {
    states: enumValues {
      name
      description
    }
  }
}`;