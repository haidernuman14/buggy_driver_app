import { gql } from 'apollo-boost';

export const getRecepitPdf = gql`
query DownloadPdf($transactionId:ID!) {
    getRecieptPdf(transactionId:$transactionId)
}`;