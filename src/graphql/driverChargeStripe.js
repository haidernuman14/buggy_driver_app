import { gql } from 'apollo-boost';
/**This class is intended to fetch the details of debited price from the account for this we have to send and validate the user 
 * token, driver id and other required parameters*/
export const ChargeStripe = gql`
mutation ChargeStripe($input: ChargeStripeInput!) {
  chargeStripe(input: $input) {
    ok
    errors {
      field
      messages
    }
  }
}`;