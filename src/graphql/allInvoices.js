import { gql } from 'apollo-boost';

export const allInvoices = gql`
query allInvoices($driverId:ID){
    allInvoices(driverId: $driverId) {
      pageInfo{
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        cursor
        node {
          id
          totalBalance
          startDate
          endDate
          previousBalance
          invoiceItems {
            id
            amount
            chargeType
            chargeTypeDisplay
            startDate
            endDate
            dueDate
            status
            chargeSuccessful
            notes
          }
        }
      }
    }
  }
`;
