import { gql } from 'apollo-boost';

export const allNotificationTypes = gql`
query allNotificationTypes {
  allNotificationTypes  {
     edges{
      node {
        id
        name
        description
        deliveryMethods
        isActive
      }
    }
  }
}`;