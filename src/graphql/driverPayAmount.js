import { gql } from 'apollo-boost';

export const driverBalance = gql`
  query Driver($id:ID) {
    driver(id:$id){
      id
      currentCharge
    }
}`;