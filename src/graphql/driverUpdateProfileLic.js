import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to update driver's information
 */
export const UpdateDriverProfileLic = gql`
mutation updateDriverMutation($input: UpdateDriverMutationInput!) {
  updateDriver(input:$input) {
    id
    firstName
    lastName
    email
    tlcLicense
    phone
    streetAddress
    city
    state
    zipCode
    dmvLicense
    dateAdded
    driver {
      balance
    }
    success:ok
    ok
    errors {
      field
      messages
    }
  }
}`;
 