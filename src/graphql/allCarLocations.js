
import { gql } from 'apollo-boost';

export const allCarLocations = gql`
query allCarLocations($isPickupLocation:Boolean){
    allCarLocations(isPickupLocation:$isPickupLocation){
    edges{
      node{
        name
        pk
      }
    }
}
}`;