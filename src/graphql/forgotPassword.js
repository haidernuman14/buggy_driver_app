import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to send Forgot password request
 */
export const appRequestPasswordReset = gql`
  mutation RequestPasswordReset($input: RequestPasswordResetMutationInput!) {
    requestPasswordReset(input:$input) {
      ok
      errors {
        field
        messages
      }
    }
}`;