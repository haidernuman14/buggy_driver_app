import { gql } from 'apollo-boost';

export const driverNotificationCounts = gql`
query Driver($id:ID) {
    driver(id:$id){
       id 
       unreadNotificationsCount
    }
}`;