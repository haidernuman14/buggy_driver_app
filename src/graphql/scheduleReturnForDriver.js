import { gql } from 'apollo-boost';

export const scheduleReturnForDriver = gql`
mutation scheduleReturnForDriver($input: ScheduleReturnInput!) {
  scheduleReturnForDriver(input: $input) {
    ok
    errors {
      messages
    }
  }
}`;
