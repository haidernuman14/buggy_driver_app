import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to get all available car models
 */
export const driverDocuments = gql`
query driverDocuments($id:ID){
    driver(id:$id){
        id
        currentAgreement{
            id
            car{
                cardocumentSet{
                    pageInfo{
                        hasNextPage
                        hasPreviousPage
                        startCursor
                        endCursor
                    }
                    edges{
                        node{
                            id
                            documentUrl
                            name
                            makeVisible
                            documentType{
                                id
                                typeName
                                visibleInDriverApp
                            }
                        }
                    }
                }
            }
        }

        driverdocumentSet{
            pageInfo{
                startCursor
                hasNextPage
                hasPreviousPage
                endCursor
            }
            edges{
                node{
                    id
                    documentUrl
                    name
                    makeVisible
                    documentType{
                        id
                        typeName
                        visibleInDriverApp
                    }
                }
            }
        }
    }
}`;