import { gql } from 'apollo-boost';

export const createSubscription = gql`
  mutation createSubscription($input: CreateSubscriptionMutationInput!) {
    createSubscription(input: $input) {
      ok
      errors {
        field
        messages
      }
    }
  }  
`;