import { gql } from 'apollo-boost';

export const getInvoicePdf = gql`
query DownloadPdf($invoiceId:ID!) {
    getInvoicePdf(invoiceId:$invoiceId)
}`;