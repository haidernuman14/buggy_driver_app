import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to update driver's TLC license data
 */
export const driverTLCupdate = gql`
mutation UpdateDriver($id: ID!,$tlcLicense: String!) {
  updateDriver(input:{ 
    id: $id,
    tlcLicense: $tlcLicense
  }) {
    ok
    driver{
      id        
      firstName
      lastName
      email
      tlcLicense
      phone
      streetAddress
      city
      state
      zipCode
      dmvLicense
      balance
      dateAdded
      dateModified
    }
    errors {
      field
      messages
    }
  }
}`;