import { gql } from 'apollo-boost';

export const createReservation = gql`
mutation createReservation($input: CreateReservationMutationInput!) {
    createReservation(input:$input){
        ok
        errors{
            field
            messages
        }
        reservation{
            id
            dateAdded
            dateModified
            pickupDate
            pickUpLocation
            pickupType
            notes
            agreementType{
                name
            }
            preferredCar
            priceRange
            discount
            surcharge
            deposit
            status
            sendMessage
            cancelReason
            isSplit
            returnDate
        }
    }  
}
`;
/*export const allReservationsQuery = gql`
query allReservationsQuery($cursor: String, $searchTerm: String, $orderBy: [String], $assignedTo: String, $fetchReservations: String, $pickupDateRange: String, $pickupType: String, $pickUpLocation: String) {
    reservations: allReservations(first: 30, orderBy: $orderBy, after: $cursor, assignedTo: $assignedTo, searchTerm: $searchTerm, fetchReservations: $fetchReservations, pickupDateRange: $pickupDateRange, pickupType: $pickupType, pickUpLocation: $pickUpLocation) {
        edges {
            node {
                id
                status
                pickupDate
                pickupType
                dateAdded
                weeklyCharge
                preferredCar
                priceRange
                pickUpLocation
                discount
                deposit
                sendMessage
                isSplit
                notes
                requiredPaymentForPickup
                previousRentalFutureReturnDate
                history {
                    user
                    comment
                    date
                    __typename
                }
                rep {
                    username
                    __typename
                }
                driver {
                    id
                    name
                    firstName
                    lastName
                    phone
                    tlcLicense
                    deposit
                    balance
                    baseAdditionalCharge
                    assignedTo
                    activeCards {
                        last4
                        __typename
                    }
                    driverbasestatusSet {
                        edges {
                            node {
                                status
                                base
                                otherDetails
                                id
                                __typename
                            }
                            __typename
                        }
                        __typename
                    }
                    __typename
                }
                car {
                    id
                    pk
                    model
                    year
                    color
                    readyAt
                    activePolicy {
                        id
                        insurancePolicy {
                            id
                            policyNumber
                            broker {
                                id
                                name
                                __typename
                            }
                            __typename
                        }
                        __typename
                    }
                    isReady
                    weeklyCharge
                    resourceId
                    dbTurnover
                    dmvPlate
                    vin
                    stage
                    currentAgreement {
                        stage
                        driver {
                            name
                            id
                            __typename
                        }
                        carreturn {
                            returnType
                            returnReason
                            scheduledDate
                            dateAdded
                            __typename
                        }
                        __typename
                    }
                    __typename
                }
                __typename
            }
            __typename
        }
        pageInfo {
            endCursor
            hasNextPage
            __typename
        }
        __typename
    }
}`;*/