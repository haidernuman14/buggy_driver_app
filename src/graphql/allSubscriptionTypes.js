import { gql } from 'apollo-boost';

export const allSubscriptionTypes = gql`
query AllSubscriptionTypes($name_Icontains: String) {
  allSubscriptionTypes(name_Icontains: $name_Icontains) {
    edges {
      node {
        id
        name
        pk
        amount
        deductibleAmount
      }
    }
  }
}`;