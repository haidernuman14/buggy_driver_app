import { gql } from 'apollo-boost';

export const carDocument = gql`
  query Car($id:Int) {
   car(id:$id){
     id   
     cardocumentSet{
      edges{
        node{
          id
          name
          documentUrl
          documentType{
            typeName
            documentCategory{
              name
            }
          }
          documentSlug
        }
      }
    }
  }
}`;