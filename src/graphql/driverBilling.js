import { gql } from 'apollo-boost';
/** This class is used to fetch the all transaction done by driver */
export const driverBilling = gql`
  query allTransactions($driverId:ID) {
     allTransactions(driverId:$driverId){
        pageInfo{
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
        edges{
          node{
            id
            driver{
              firstName
            }
          }
        }
         
      }
}`;