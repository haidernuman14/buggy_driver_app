import { gql } from 'apollo-boost';
/**This class is used to fetch the Driver car rental history like (transation details, Car details, agreementType) using query from GraphQL */
export const DriverAllRentals = gql`
  query allRentals($orderBy: [String], $driverId: ID, $first: Int, $before: String, $after: String) {
    allRentals(orderBy: $orderBy, driverId:$driverId, first: $first, before: $before, after: $after) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          startDate
          id
          endDate
          pickupType
          weeklyCharge
          agreementType
          car {
            year
            model
            color
            pk
          } 
        }
      }
    }
}`;  