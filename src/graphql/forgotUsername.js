import { gql } from 'apollo-boost';

/**
 * Buggy Graphql API to send Forgot Username request 
 */
export const appRequestUsername = gql`
  mutation RequestUsername($input: RequestUsernameMutationInput!) {
    requestUsername(input:$input) {
      ok
      errors {
        field
        messages
      }
    }
}`;