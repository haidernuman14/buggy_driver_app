import { gql } from 'apollo-boost';
export const allAgreementTypes = gql`
query AllAgreementTypes($isActive:Boolean){
  allAgreementTypes(isActive:$isActive){
    edges{
      node{
        name
        forTlc
        requiredDeposit
      }
    }
  }
}`;