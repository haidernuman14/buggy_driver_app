import { gql } from 'apollo-boost';

export const downloadFromDrive = gql`
  mutation downloadFromDrive($input: DownloadFromDriveMutationInput!) {
    downloadFromDrive(input: $input) {
      ok
      fileData
      errors {
        field
        messages
      }
      clientMutationId
    }
  }  
`;