import React, { useState } from 'react';
import { StatusBar, Platform } from 'react-native';
import AppNavigator from './src/navigation/AppNavigator';
import { ApolloProvider } from '@apollo/react-hooks';
import NetChecker from './src/screens/Helper/NetInfo';
import { langContext } from './src/screens/Helper/langContext';
import { setI18nConfig } from './src/components/Language';
import Colors from './src/constants/Colors';
import client from './src/graphql';
import { production } from './app.json';
import analytics from '@react-native-firebase/analytics';
import AsyncStorage from '@react-native-community/async-storage';
console.disableYellowBox = true;

export default function App(props) {
  const [state, setState] = useState({
    isModal: false,
    lang: "en",
    car_id: 3,
    notification_count: 90,
    status_color: '#f9f9f4'
  });

  const { isModal, lang, car_id, status_color, notification_count } = state;
  const setNetModal = (flag) => {
    setState({ ...state, isModal: flag })
  }
  const setLanguage = (language) => {
    setI18nConfig(language);
    setState({ ...state, lang: language })
  }
  const setCarID = (id) => {
    setState({ ...state, car_id: id })
  }
  const setStatusColor = (color) => {
   // if (production == 'live')
      setState({ ...state, status_color: color })
  }
  const setNotificationCount = (ncount) => {
    setState({ ...state, notification_count: ncount })
  }

  // gets the current screen from navigation state
  const getActiveRouteName = (navigationState) => {
    if (!navigationState) {
      console.log('getActiveRouteName', 'null')
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
      return getActiveRouteName(route);
    }
    return route.routeName;
  }
  return (
    <ApolloProvider client={client}>
      <langContext.Provider value={{ lang, setLanguage, notification_count, setNotificationCount, status_color, car_id, setCarID, setStatusColor }}>

        {Platform.OS === 'ios' ? <StatusBar barStyle={production == 'live' ? "default" : "light-content"} /> :
          <StatusBar
            barStyle={"dark-content"}
            // dark-content, light-content and default
            hidden={false}
            //To hide statusBar
            backgroundColor={status_color}
            //Background color of statusBar only works for Android
            translucent={false}
            //allowing light, but not detailed shapes
            networkActivityIndicatorVisible={true}
          />}

        <NetChecker isModal={isModal} setModal={setNetModal} />
        <AppNavigator onNavigationStateChange={async(prevState, currentState, action) => {
          const currentRouteName = getActiveRouteName(currentState);
          const previousRouteName = getActiveRouteName(prevState);

          if (previousRouteName !== currentRouteName) {
            console.log('currentRouteName', currentRouteName);

            let user = await AsyncStorage.getItem('user');
            if(user && user != null) {
              user = JSON.parse(user);  
              await analytics().setUserId(user.id);
              console.log("Set UID:",user.id);
            }
            // the line below uses the @react-native-firebase/analytics tracker
            // change the tracker here to use other Mobile analytics SDK.
            analytics().setCurrentScreen(currentRouteName, currentRouteName);
          }
        }}
          lang={lang} screenProps={{ setNetModal, setStatusColor, build_type: production }} />
      </langContext.Provider>
    </ApolloProvider>
  );
};
